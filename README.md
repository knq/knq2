# KNQ2 - Computerised Kinship Network Questionaire

KNQ2 (Kinship Network Questionnaire 2) is a software system that has been designed, and implemented by the KNQ2 programming team, headed by Dr Kong Jing at the Chinese Academy of Social Sciences, Beijing. It builds on and extends the original KNQ, developed for the KASS (Kinship and Social Security in Europe) project by Gordon Milligan and Christian Kieser – incorporating major new capabilities requested by the KUV team at Max Planck Institute for Social Anthropology Halle (Saale). The software system has been tested and refined jointly by the Beijing and Halle teams. This process in still on-going, so the software system itself is still at a “beta” stage.
One of our main approaches is to combine ethnography with formal network interviews – applying a version of Rivers’ genealogical method and collecting detailed information about mutual assistance and other interactions among family network members. (The ‘genealogical method’ has been much criticised by socio-cultural anthropologists in recent decades for supposedly imposing western notions of biological kinship on other cultures. We think this criticism is unfair, and if taken literally would rule out a very powerful and flexible data-collection and analytic method.)

Our tool for conducting network interviews is the newly developed Kinship Network Questionnaire (KNQ2) – a software program for laptop computers. Originally developed at Max Planck Institute for Social Anthropology Halle (Saale) in conjunction with the EU-funded KASS-project the latest version incorporates a number of new features.

*  The interviewer and informant look at this screen together during the interview
*  The diagram can be read two ways: either as people connected by family ties or as family unions connected by the individual people they exchange. (The interview collects data on both people and unions)
*  There is also a space (on the right of the screen) for recording information about “important others” – people who are not genealogical relatives but are considered to be kin or kin-like.

The new KNQ2  consists of four subsystems:

* Questionnaire Management System enables researchers to design their own kinship questionnaire
* Interview Management System guides the interviewer and informant through the questionnaire
* Download and Variable Derivation (DAVD) – see right-hand panel
* Internationalisation enables the researcher to produce equivalent versions of the questionnaire in different languages

Data can be downloaded in three kinds of matrix formats:

* Data on individual people and their characteristics (including their interactions with the informant)
* Data on family unions and their characteristics
* Person-by-union data which can be used to work out network relationships

The data are downloaded as EXCEL-readable “txt” and “csv” files, which can be read by statistical packages such as SPSS and R – and also by network analysis packages.
For more information about the KNQ software system see the documentation.

For further information about the KNQ2 software system have a look at our wiki: https://gitlab.gwdg.de/knq/knq2/wikis/home

The KNQ2 software system is available as a windows binary: 
https://gitlab.gwdg.de/knq/knq2/blob/master/binaries/KNQ2Beta3.3Setup.zip

A JAR version of the program is available as well:
https://gitlab.gwdg.de/knq/knq2/blob/master/binaries/KNQ2beta3.3-Jar.zip

