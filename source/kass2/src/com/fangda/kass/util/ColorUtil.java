package com.fangda.kass.util;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.Random;

/**
 * This Class for the setting of color for the UI of the KNQ2.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class ColorUtil {

	/**
	 * Generate the Random Color Code
	 * 
	 * @return
	 */
	public static synchronized String getRandomColorCode() {
		// Color 6 bit code
		int colorLength = 6;

		// The code number of the color code
		char[] codeSequence = { 'A', 'B', 'C', 'D', 'E', 'F', '0', '1', '2',
				'3', '4', '5', '6', '7', '8', '9' };

		// StringBuffer sb = new StringBuffer("#");
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < colorLength; i++) {
			sb.append(codeSequence[random.nextInt(16)]);
		}
		return sb.toString();
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For HTML ,
	 * 
	 * @return String
	 */
	public static String getRandColorCode() {
		String r, g, b;
		Random random = new Random();
		r = Integer.toHexString(random.nextInt(256)).toUpperCase();
		g = Integer.toHexString(random.nextInt(256)).toUpperCase();
		b = Integer.toHexString(random.nextInt(256)).toUpperCase();

		r = r.length() == 1 ? "0" + r : r;
		g = g.length() == 1 ? "0" + g : g;
		b = b.length() == 1 ? "0" + b : b;

		return r + g + b;
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For Question Progress,
	 * Index=Completed Index=0,unCompleted; Index=1,completed; Index=2, some
	 * completed; Index=3, DNA.
	 * 
	 * @return String
	 */
	public static String getProgressColor(int index) {
		if (index == 0) {
			return "#FF0000";
		} else if (index == 1) {
			return "#00CD00";
		} else if (index == 2) {
			return "#0000FF";
		} else if (index == 3) {
			return "#757575";
		}
		return "";
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For Question Progress status,
	 * Index= question's Value. Index="NA",unCompleted; Index="OK",completed;
	 * Index="Mark", be marked; Index="BNM", Be not marked; Index="DNA",do not
	 * apply.
	 * 
	 * @return String
	 */

	public static String getProgressStatusColor(String index) {
		if (index.equals("NA")) {
			return "#FFFFFF";
		} else if (index.equals("OK")) {
			return "#25C031";
		} else if (index.equals("Mark")) {
			return "#FF00FF";
		} else if (index.equals("BNM")) {
			return "#909090";
		} else if (index.equals("DNA")) {
			return "#D3D3D3";
		}
		return "";
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For Person Follow-up Question
	 * Answer Index=0, if Value="NA"; Index=1, if Value="DNA"; Index=2, if not
	 * qid Type ="C" and Value not ="NA" or "DNA"
	 * 
	 * @return String
	 */
	public static String getFollowUpColor(int index) {
		if (index == 0) {
			return "#FFFFFF";
		} else if (index == 1) {
			return "#25C031";
		} else if (index == 2) {
			return "#8040C0";
		}
		return "#FFFFFF";
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For Person Lead Question
	 * Progress Index=0, unCompleted: Completed=False & isMarked=False; Index=1,
	 * completed1: Completed=True & isMarked=True; Index=2, some completed:
	 * Completed=False & isMarked=True; Index=3, DNA: Don't apply. Index=4,
	 * Selected: When click this signal of person Index=5, completed2:
	 * Completed=True & isMarked=False;
	 * 
	 * @return String
	 */
	public static String getPersonProgressColor(int index) {
		if (index == 0) {
			return "#FFFFFF";
		} else if (index == 1) {
			return "#25C031";
		} else if (index == 2) {
			return "#8040C0";
		} else if (index == 3) {
			return "#757575";
		} else if (index == 4) {
			return "#FFAFAF";
		} else if (index == 5) {
			return "#C0C0C0";
		}
		return "";
	}

	public static Color getPersonProgressColorO(int index) {
		String color = getPersonProgressColor(index);
		int r = Integer.parseInt(color.substring(1, 3), 16);
		int g = Integer.parseInt(color.substring(3, 5), 16);
		int b = Integer.parseInt(color.substring(5, 7), 16);
		return new Color(r, g, b);
	}

	/**
	 * Hexadecimal code of the color. "#6E36B4" , For Answer Options,
	 * 
	 * @return String
	 */
	public static String geWebColor(int num, int index) {
		if (index == num - 1 && num < 7) {
			return "#FFFF00";
		} else {
			if (index == 0) {
				return "#7B88EE";
			} else if (index == 1) {
				return "#99DAFF";
			} else if (index == 2) {
				return "#CD96CD";
			} else if (index == 3) {
				return "#4EEE94";
			} else if (index == 4) {
				return "#7FFF00";
			} else if (index == 5) {
				return "#D9F665";
			} else if (index == 6) {
				return "#FFFF00";
			} else if (index == 7) {
				return "#EEC900";
			} else if (index == 8) {
				return "#CC9966";
			} else if (index == 9) {
				return "#FFB416";
			} else if (index == 10) {
				return "#FA7816";
			} else if (index == 11) {
				return "#FF4060";
			} else if (index == 12) {
				return "#FF0000";
			} else if (index == 13) {
				return "#FF00FF";
			} else if (index == 14) {
				return "#00FFFF";
			} else if (index == 15) {
				return "#0000FF";
			}
		}
		return "";
	}

	/**
	 * Get the Index by Hexadecimal code of the color: "#6E36B4" , For Answer
	 * Options,
	 * 
	 * @return String "#25C031"
	 */
	public static int geWebColor(int num, String colorCode) {
		if (colorCode.equalsIgnoreCase("#FFFF00") && num < 7) {
			return num - 1;
		} else {
			if (colorCode.equals("#7B88EE")) {
				return 0;
			} else if (colorCode.equals("#99DAFF")) {
				return 1;
			} else if (colorCode.equals("#CD96CD")) {
				return 2;
			} else if (colorCode.equals("#4EEE94")) {
				return 3;
			} else if (colorCode.equals("#7FFF00")) {
				return 4;
			} else if (colorCode.equals("#D9F665")) {
				return 5;
			} else if (colorCode.equals("#FFFF00")) {
				return 6;
			} else if (colorCode.equals("#EEC900")) {
				return 7;
			} else if (colorCode.equals("#CC9966")) {
				return 8;
			} else if (colorCode.equals("#FFB416")) {
				return 9;
			} else if (colorCode.equals("#FA7816")) {
				return 10;
			} else if (colorCode.equals("#FF4060")) {
				return 11;
			} else if (colorCode.equals("#FF0000")) {
				return 12;
			} else if (colorCode.equals("#FF00FF")) {
				return 13;
			} else if (colorCode.equals("#00FFFF")) {
				return 14;
			} else if (colorCode.equals("#0000FF")) {
				return 15;
			}
		}
		return -1;
	}

	/**
	 * Get the color
	 * 
	 * @param num
	 * @param index
	 * @return
	 */
	public static Color geColor(int num, int index) {
		String color = geWebColor(num, index);
		int r = Integer.parseInt(color.substring(1, 3), 16);
		int g = Integer.parseInt(color.substring(3, 5), 16);
		int b = Integer.parseInt(color.substring(5, 7), 16);
		return new Color(r, g, b);
	}

	/**
	 * RGB
	 * 
	 * @param color
	 * @return
	 */
	public static String geWebColor(Color color) {
		String a = Judge0T00(Integer.toHexString(color.getRed()));
		String b = Judge0T00(Integer.toHexString(color.getGreen()));
		String c = Judge0T00(Integer.toHexString(color.getBlue()));

		return "#" + a + b + c;
	}

	public static String Judge0T00(String a) {
		if (a.equals("0")) {
			a = "00";
		}
		return a;
	}

	public static String getStopRuleColor() {
		return "#FFFF00";
	}

	public static String getCompletePersonColor() {
		return "#00FF00";
	}

	public static void main(String[] args) {
		// System.out.println(getRandomColorCode());
		// System.out.println(getRandColorCode());
		System.out.println(geColor(10, 2));
	}
}
