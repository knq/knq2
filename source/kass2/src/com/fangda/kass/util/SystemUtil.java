package com.fangda.kass.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 * This class for the System Utils in the KNQ2.
 * 
 * @author Fangfang Zhao
 * 
 */
public class SystemUtil {

	private static String PATH = Constants.getUserPath() + File.separator
			+ "language" + File.separator + "system.properties";

	private static Properties props = new Properties();

	static {
		try {
			props.load(new FileInputStream(PATH));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			System.exit(-1);
		}
	}

	/**
	 * Get the key - value from the properties file
	 * 
	 * @param key
	 * 
	 * @return String
	 */
	public static String get(String key, String defaultValue) {
		String result = props.getProperty(key);
		if (result == null) {
			return defaultValue;
		}
		return result;
	}

	/**
	 * Set the key - value into the properties file
	 * 
	 * @param keyname
	 * @param keyvalue
	 * 
	 */
	public static void set(String key, String value) {
		try {
			props.load(new FileInputStream(PATH));
			OutputStream fos = new FileOutputStream(PATH);
			props.setProperty(key, value);
			props.store(fos, "Update '" + key + "' value");
		} catch (IOException e) {
			System.err.println("Update the porperties file error !");
		}
	}
}
