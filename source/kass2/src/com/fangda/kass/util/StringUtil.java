package com.fangda.kass.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * This class for the String Utils in the KNQ2.
 * 
 * @author Fangfang Zhao
 * 
 */
public class StringUtil {
	
	 public static final String EMPTY = "";

	public static String textToHtml(String longString) {
		String result = "";
		result = longString.replace("\r\n", "<br/>");
		result = longString.replace("•", "<br/>•");
		result = "<html>" + result;
		result += ("</html>");
		System.out.println(result);
		return result;
	}

	public static int lastIndexOf(String str, char searchChar) {
		if (isEmpty(str)) {
			return -1;
		}
		return str.lastIndexOf(searchChar);
	}

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isBlank(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	public static String substringBeforeLast(String str, String separator) {
		if (isEmpty(str) || isEmpty(separator)) {
			return str;
		}
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return str;
		}
		return str.substring(0, pos);
	}
	
	public static String substringAfterLast(String str, String separator) {
		if (isEmpty(str) || isEmpty(separator)) {
			return str;
		}
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return str;
		}
		return str.substring(pos + 1, str.length());
	}

	/**
	 * Get the number of str2 in str1
	 * 
	 * @param str1
	 * @param str2
	 * @return counter
	 */
	public static int countStr(String str1, String str2) {
		String reg = str2;
		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher(str1);
		int index = 0;
		while (matcher.find()) {
			index++;
		}
		return index;
	}
	
	public static String join(Object[] array, String separator) {
        if (array == null) {
            return null;
        }
        if (separator == null) {
            separator = EMPTY;
        }
        int arraySize = array.length;

        // ArraySize ==  0: Len = 0
        // ArraySize > 0:   Len = NofStrings *(len(firstString) + len(separator))
        //           (Assuming that all Strings are roughly equally long)
        int bufSize =
            ((arraySize == 0)
                ? 0
                : arraySize
                    * ((array[0] == null ? 16 : array[0].toString().length())
                        + separator.length()));

        StringBuffer buf = new StringBuffer(bufSize);

        for (int i = 0; i < arraySize; i++) {
            if (i > 0) {
                buf.append(separator);
            }
            if (array[i] != null) {
                buf.append(array[i]);
            }
        }
        return buf.toString();
    }
	
	public static String trimToNull(String str) {
		String s = StringUtils.trim(str);
		if(StringUtils.isBlank(s)) {
			return null;
		}
		return s;
	}

	public static void main(String[] args) {
		int count = countStr("absabc abc", "a");
		System.out.println(count);
	}
}
