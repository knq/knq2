package com.fangda.kass.util;

/**
 * This class for the definition of Route of the kinship path.
 * 
 * @author Fangfang Zhao
 * 
 */
public class Route {
	public int index;
	public int oneway;
	public int fnode;
	public int tnode;
	public int length;
	public int type;

	@Override
	public String toString() {
		return "";
	}
}
