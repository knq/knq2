package com.fangda.kass.util;

import java.io.File;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.Project;
import com.fangda.kass.model.questionnaire.Questionnaire;

/**
 * This Class for the constants of the KNQ2.
 * 
 * @author Fangfang Zhao, Shuang Ma, Jing Kong
 * 
 */
public class Constants {

	public final static String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

	/**
	 * Questionnaire open xml path now
	 */
	public static String QUESTIONNAIRE_XML = "";

	public static String QUESTIONNAIRE_TEMPLATE_XML = "Questionnaire"
			+ File.separator + "template" + File.separator
			+ "questions_v2.properties";

	/**
	 * QUESTIONNAIRE_STATUS: 0 = questionnaireview, 11= revise, 12 = temprevise,
	 * 13=localize
	 */
	public static int QUESTIONNAIRE_STATUS = 0;

	public static String PROJECT_XML = "";

	public static String KASS_XML = "";

	public static int isInterviewCancel = 0;

	public static int isQuestionnaireCancel = 0;

	// Internationalize the source code.

	public static String CONFIG_PATH = "language" + File.separator
			+ "config.properties";
	public static String MESSAGE_PATH = "language" + File.separator
			+ "message.properties";

	/**
	 * static store the questionnaire
	 */
	public static Questionnaire QUESTIONNAIRE;
	public static Project PROJECT;
	public static KASS KASS;

	public static String SPLIT_OBJECT = "#";

	public static String SPLIT_PROPERTY = "\\|";

	public static String JOIN_PROPERTY = "|";

	public static String JOIN_OBJECT = "#";

	public static String DEFAULT_NA = "NA";

	public static String VERSION = "V2.0-Beta3.3";

	public static String DEFAULT_DNA = "DNA";

	public static String DEFAULT_OK = "OK";

	public static String DEFAULT_BNM = "BNM";

	public static String DEFAULT_MARK = "Mark";

	public static String XML_EXT = ".xml";
	public static String TMP_EXT = ".tmp";
	public static String PROPERTIES_EXT = ".properties";

	// Question modes
	public static String QUESTION_TYPE_DRAW = "draw";
	public static String QUESTION_TYPE_QUESTION = "question";
	public static String QUESTION_TYPE_MAP = "map";
	public static String QUESTION_TYPE_MOREDETAIL = "moreDetail";

	// The following question IDs in questionnaire are required.
	public static String QUESTION_SEX_MALE = "K_08_00_D_0";
	public static String QUESTION_SEX_FEMALE = "K_08_00_D_1";
	public static String QUESTION_SEX = "K_08_00_D";
	public static String QUESTION_DEATH = "K_08_00_C";
	public static String QUESTION_GENERAL = "K_08_00";
	public static String QUESTION_DRAW = "K_01_01";
	public static String SUBSECTION_GENERAL = "SS_008_01";

	public static String PANELFLAG = "";

	private static String USER_PATH = "";

	public static String getUserPath() {
		return USER_PATH;
	}

	public static void setUserPath(String path) {
		USER_PATH = path;
	}

	public static String getUserLogPath() {
		String userPath = getUserPath();
		String path = userPath + File.separator + "logs";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
		return path;
	}

	public static String getUserDataPath() {
		String userPath = getUserPath();
		String path = userPath + File.separator + "data";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
		return path;
	}
}
