package com.fangda.kass.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;

/**
 * This class for the File operations in the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public class FileUtil {

	/**
	 * Move the Folder and its files
	 * 
	 * @param from
	 * @param to
	 * @throws Exception
	 */
	public static void MoveFolderAndFileWithSelf(String from, String to)
			throws Exception {
		try {
			File dir = new File(from);
			to += File.separator + dir.getName();
			File moveDir = new File(to);
			if (dir.isDirectory()) {
				if (!moveDir.exists()) {
					moveDir.mkdirs();
				}
			} else {
				File tofile = new File(to);
				dir.renameTo(tofile);
				return;
			}

			// System.out.println("dir.isDirectory()"+dir.isDirectory());
			// System.out.println("dir.isFile():"+dir.isFile());

			// List files
			File[] files = dir.listFiles();
			if (files == null)
				return;

			// Move files
			for (int i = 0; i < files.length; i++) {
				System.out.println("文件名：" + files[i].getName());
				if (files[i].isDirectory()) {
					MoveFolderAndFileWithSelf(files[i].getPath(), to);
					// Delete the old files
					files[i].delete();
				}
				File moveFile = new File(moveDir.getPath() + File.separator
						+ files[i].getName());
				// Delete the existed files in the destination folder
				if (moveFile.exists()) {
					moveFile.delete();
				}
				files[i].renameTo(moveFile);
			}
			dir.delete();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Copy a file
	 * 
	 * @param oldPathFile
	 * @param newPathFile
	 * 
	 * @return
	 */
	public static void copySingleFile(String sourceFile, String destFile) {

		if (StringUtils.equals(sourceFile, destFile)) {
			throw new RuntimeException("file format error");
		}
		InputStream inStream = null;
		FileOutputStream fs = null;
		try {
			int bytesum = 0;
			int byteread = 0;
			File sFile = new File(sourceFile);
			if (sFile.exists()) {
				inStream = new FileInputStream(sFile);
				fs = new FileOutputStream(destFile);
				byte[] buffer = new byte[1024];
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread;
					fs.write(buffer, 0, byteread);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("exception : " + e.getMessage());
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void copySingleFile(InputStream inStream, String destFile) {
		FileOutputStream fs = null;
		try {
			int bytesum = 0;
			int byteread = 0;
			fs = new FileOutputStream(destFile);
			byte[] buffer = new byte[1024];
			while ((byteread = inStream.read(buffer)) != -1) {
				bytesum += byteread; // 字节数 文件大小
				fs.write(buffer, 0, byteread);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (fs != null) {
				try {
					fs.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void copyDirectory(String src, String des) throws Exception {

		File file1 = new File(src);
		File[] fs = file1.listFiles();
		File file2 = new File(des);
		if (!file2.exists()) {
			file2.mkdirs();
		}
		// Traverse files and folders
		for (File f : fs) {
			if (f.isFile()) {
				// Files
				copySingleFile(f.getPath(), des + File.separator + f.getName()); // 调用文件拷贝的方法
			} else if (f.isDirectory()) {
				// Folders
				copyDirectory(f.getPath(), des + File.separator + f.getName());
			}
		}

	}

	/**
	 * The main method
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

	}

}
