package com.fangda.kass.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Utils Class for the date
 * 
 * @author Fangfang Zhao
 * 
 */
public class DateUtil {

	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

	private DateUtil() {

	}

	/**
	 * Get Today
	 * 
	 * @return
	 */
	public static String getToday() {
		Date today = new Date();
		return format(today);
	}

	/**
	 * Format Date to string
	 * 
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return date == null ? "" : format(date, Constants.DEFAULT_DATE_PATTERN);
	}

	/**
	 * Format Date to string
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date, String pattern) {
		return date == null ? "" : new SimpleDateFormat(pattern).format(date);
	}

	/**
	 * From String to Date
	 * 
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String strDate) throws ParseException {
		return StringUtils.isBlank(strDate) ? null : parse(strDate,
				Constants.DEFAULT_DATE_PATTERN);
	}

	/**
	 * From String to Date
	 * 
	 * @param strDate
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String strDate, String pattern)
			throws ParseException {
		return StringUtils.isBlank(strDate) ? null : new SimpleDateFormat(
				pattern).parse(strDate);
	}

	/**
	 * Add month to Date
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}

	/**
	 * Add day to Date
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addDay(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, n);
		return cal.getTime();
	}

	/**
	 * Add hours to date
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addHour(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, n);
		return cal.getTime();
	}

	/**
	 * Add minute to date
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addMinute(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, n);
		return cal.getTime();
	}

	/**
	 * Calculate the duration time between two date
	 * 
	 * @param date
	 * @param otherDate
	 * @return
	 */
	public static long betDate(Date date, Date otherDate) {
		return date.getTime() - otherDate.getTime();
	}

	/**
	 * Generate a date by Year, Month and day
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static Date makeDate(int year, int month, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1, year);
		calendar.set(2, month - 1);
		calendar.set(5, day);
		calendar.set(11, 0);
		calendar.set(12, 0);
		calendar.set(13, 0);
		calendar.set(14, 0);
		return calendar.getTime();
	}

	/**
	 * Generate a date by Year, Month, day, hour, minute and second
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 */
	public static Date makeDate(int year, int month, int day, int hour,
			int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(1, year);
		calendar.set(2, month - 1);
		calendar.set(5, day);
		calendar.set(11, hour);
		calendar.set(12, minute);
		calendar.set(13, second);
		calendar.set(14, 0);
		return calendar.getTime();
	}

	/**
	 * Get month
	 * 
	 * @param date
	 * @return
	 */
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(2) + 1;
	}

	/**
	 * Get Year
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(1);
	}

	/**
	 * Get day
	 * 
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(5);
	}

	/**
	 * Get Week
	 * 
	 * @param date
	 * @return
	 */
	public static int getWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int week = calendar.get(Calendar.DAY_OF_WEEK);

		if (week == 1)
			return 7;
		else
			return week - 1;

	}

	/**
	 * Get Hour
	 * 
	 * @param date
	 * @return
	 */
	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);

	}

	/**
	 * Get the last date of the Month
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastDateOfMonth(Date date) {
		int year = DateUtil.getYear(date);
		int month = DateUtil.getMonth(date);
		return DateUtil.addDay(
				DateUtil.addMonth(DateUtil.makeDate(year, month, 1), 1), -1);

	}

	/**
	 * Get the first day of the Month
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDateOfMonth(Date date) {
		int year = DateUtil.getYear(date);
		int month = DateUtil.getMonth(date);
		return DateUtil.makeDate(year, month, 1);
	}

	/**
	 * Get the day of week
	 * 
	 * @param date
	 * @param resultWeek
	 * @return
	 */
	public static Date getDayOfWeek(Date date, int resultWeek) {
		if (resultWeek < 1 || resultWeek > 7)
			throw new IllegalArgumentException("resultWeek must in 1-7");
		int week = DateUtil.getWeek(date);
		return DateUtil.addDay(date, resultWeek - week);
	}

	/**
	 * Get the Time
	 * 
	 * @return
	 */
	public static Long getTime() {
		Long result = new Long(DateUtil.format(new Date(), "yyyyMMddHHmmssS"));
		return result;
	}
}