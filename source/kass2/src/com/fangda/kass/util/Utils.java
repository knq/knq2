package com.fangda.kass.util;

/**
 * This Class for the Utils for getting the Name of the OS
 * 
 * @author Fangfang Zhao
 * 
 */
public class Utils {

	public static String getOs() {
		String os = System.getProperty("os.name");
		return os;
	}

	public static boolean isMac() {
		String os = getOs();
		if (os.contains("Mac")) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		getOs();
	}
}
