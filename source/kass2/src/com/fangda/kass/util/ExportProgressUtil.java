package com.fangda.kass.util;

import javax.swing.JProgressBar;

/**
 * This class for exporting the data of the progress bar of the completed
 * questions.
 * 
 * @author Fangfang Zhao
 * 
 */
public class ExportProgressUtil {

	private static ExportProgressUtil instance = new ExportProgressUtil();

	private ExportProgressUtil() {

	}

	public static ExportProgressUtil getInstance() {
		return instance;
	}

	private JProgressBar progressBar;
	/**
	 * 1 Person 2 Union
	 */
	private int type = 0;

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
