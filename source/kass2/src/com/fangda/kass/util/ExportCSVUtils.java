package com.fangda.kass.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * This class for exporting data tables in the cvs or text format file
 * 
 * @author Yu Zhang
 * @param CSV
 *            The escape characters in CSV file 
 *            Year,Manufacture,ModelNumber,Note,Price 
 *            1997,Ford,E350,"ac, abs, moon",3000.00
 *            1999,Chevy,"Venture ""Extended Edition""","",4900.00
 *            1999,Chevy,"Venture ""Extended Edition, Very Large""","",5000.00
 *            1996,Jeep,Grand
 *            Cherokee,"MUST SELL!air, moon roof, loaded",4799.00
 * 
 *            This is a CSV format example RFC 4180
 */

public class ExportCSVUtils {
	public static final String CSV_DATAITEM_SPLITER_COMMA = ",";
	public static final String CSV_LINE_END_COMMA = System
			.getProperty("line.separator");
	public static final String CSV_NULL_DATAITEM_COMMA = "\"\"";

	public static final String TXT_DATAITEM_SPLITER_COMMA = "\t";

	/*
	 * 2016-01-17 Add the escape characters in CVS file
	 *  1 "" 
	 *  2 "," """
	 */
	private static String escapeCVSChar(String srcString) {
		String result = srcString;
		if (!StringUtil.isEmpty(srcString)) {
			if (srcString.contains(CSV_DATAITEM_SPLITER_COMMA)
					|| srcString.contains(CSV_LINE_END_COMMA)
					|| srcString.contains("\"")) {
				String temp = srcString.replace("\"", "\"\"");
				result = "\"" + temp + "\"";
			}
		}
		return result;
	}

	/*
	 * Tab and Line break 1 'tab' to " "; 2 'line break' to " "
	 */

	private static String escapeTabChar(String srcString) {
		String result = srcString;
		if (!StringUtil.isEmpty(srcString)) {
			if (srcString.contains(TXT_DATAITEM_SPLITER_COMMA)
					|| srcString.contains("\n\r")
					|| srcString.contains(CSV_LINE_END_COMMA)) {
				String tempTab = srcString.replace(TXT_DATAITEM_SPLITER_COMMA,
						"");
				String tempLine = tempTab.replace("\n\r", "");
				String tempLineResult = tempLine
						.replace(CSV_LINE_END_COMMA, "");
				result = tempLineResult;
				;
			}
		}
		return result;
	}

	public static boolean exportCSV(File file, String[] headArray,
			String[][] exportArrayData) {
		if (file != null && !StringUtil.isEmpty(file.getName())) {
			if (file.getName().endsWith(".csv")) {
				return exportCSVData(file, headArray, exportArrayData);
			} else if (file.getName().endsWith(".txt")) {
				return exportTXTData(file, headArray, exportArrayData);
			}
		}
		return false;
	}

	// Export the TXT file
	private static boolean exportTXTData(File file, String[] headArray,
			String[][] exportArrayData) {

		boolean result = false;

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out);
			bw = new BufferedWriter(osw);

			if (headArray != null) {
				int headArrayLength = headArray.length;
				for (int i = 0; i < headArrayLength; i++) {
					String curHeadColValue = escapeTabChar(headArray[i]);

					if (StringUtil.isEmpty(curHeadColValue)) {
						curHeadColValue = CSV_NULL_DATAITEM_COMMA;
					}

					if (i == headArrayLength - 1) {
						bw.append(curHeadColValue).append(CSV_LINE_END_COMMA);
					} else {
						bw.append(curHeadColValue).append(
								TXT_DATAITEM_SPLITER_COMMA);
					}
				}
			}

			if (exportArrayData != null) {
				int rowLength = exportArrayData.length;
				int colLength = exportArrayData[0].length;
				for (int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
					for (int colIndex = 0; colIndex < colLength; colIndex++) {
						String curValue = escapeTabChar(exportArrayData[rowIndex][colIndex]);

						if (StringUtil.isEmpty(curValue)) {
							curValue = CSV_NULL_DATAITEM_COMMA;
						}

						if (colIndex == colLength - 1) {
							bw.append(curValue).append(CSV_LINE_END_COMMA);
						} else {
							bw.append(curValue).append(
									TXT_DATAITEM_SPLITER_COMMA);
						}
					}
				}
			}

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			file.deleteOnExit();
			result = false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	/**
	 * Export the CSV Data
	 * 
	 * @param file
	 * @param exportArrayData
	 * @return
	 */

	public static boolean exportCSVData(File file, String[] headArray,
			String[][] exportArrayData) {
		boolean result = false;

		if (file.exists()) {
			file.delete();
		}

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out, "UTF-8");
			bw = new BufferedWriter(osw);

			if (headArray != null) {
				int headArrayLength = headArray.length;
				for (int i = 0; i < headArrayLength; i++) {
					String curHeadColValue = escapeCVSChar(headArray[i]);

					if (StringUtil.isEmpty(curHeadColValue)) {
						curHeadColValue = CSV_NULL_DATAITEM_COMMA;
					}

					if (i == headArrayLength - 1) {
						bw.append(curHeadColValue).append(CSV_LINE_END_COMMA);
					} else {
						bw.append(curHeadColValue).append(
								CSV_DATAITEM_SPLITER_COMMA);
					}
				}
			}

			if (exportArrayData != null) {
				int rowLength = exportArrayData.length;
				int colLength = exportArrayData[0].length;
				for (int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
					for (int colIndex = 0; colIndex < colLength; colIndex++) {
						String curValue = escapeCVSChar(exportArrayData[rowIndex][colIndex]);

						if (StringUtil.isEmpty(curValue)) {
							curValue = CSV_NULL_DATAITEM_COMMA;
						}

						if (colIndex == colLength - 1) {
							bw.append(curValue).append(CSV_LINE_END_COMMA);
						} else {
							bw.append(curValue).append(
									CSV_DATAITEM_SPLITER_COMMA);
						}
					}
				}
			}

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			file.deleteOnExit();
			result = false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					file.deleteOnExit();
					result = false;
					e.printStackTrace();
				}
			}
		}

		return result;
	}
}
