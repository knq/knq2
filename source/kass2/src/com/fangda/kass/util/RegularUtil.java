package com.fangda.kass.util;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.model.support.Relevance;
import com.fangda.kass.model.support.RelevanceCondition;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;

/**
 * This class for the Regular Rule of the questions for a person.
 * 
 * @author Fangfang Zhao
 * 
 */
public class RegularUtil {

	private static KassService kassService;
	private static QuestionnaireService qService;

	/**
	 * Get the default value
	 * 
	 * @param question
	 * @param person
	 * @return
	 */
	public static String getDefaultValue(Question question, KPPerson person) {
		init();
		String result = "";
		
		String name = QuestionConstants.getRelevantCondition(question
				.getRelevance());
		if (StringUtils.isBlank(name))
			return Constants.DEFAULT_NA;
		Relevance r = new Relevance();
		r.string2obj(name);
		boolean flag = checkPersonByRelevance(r, person, question);
		if(flag) {
			result = Constants.DEFAULT_NA;
		} else {
			result = Constants.DEFAULT_DNA;
		}
		//System.out.println(person.getPid() + "  " + question.getRelevance() + " :   " +question.getQid() + " : " + result);
		return result;
	}
	
	public static String getDefaultValue(Question question, KUUnion union) {
		init();
		String result = "";
		
		String name = QuestionConstants.getRelevantCondition(question
				.getRelevance());
		if (StringUtils.isBlank(name))
			return "";
		Relevance r = new Relevance();
		r.string2obj(name);
		boolean flag = checkUnionByRelevance(r, union, question);
		if(flag) {
			result = Constants.DEFAULT_NA;
		} else {
			result = Constants.DEFAULT_DNA;
		}
		return result;
	}

	private static void init() {
		if (kassService == null) {
			kassService = new KassService();
		}
		if (qService == null) {
			qService = new QuestionnaireService();
		}
	}

	/**
	 * Get the type of a question
	 * 
	 * @param question
	 * @return String
	 */
	public static String getType(Question question) {
		init();
		String sid = question.getSubsection();
		Subsection subsection = qService.searchSubsection(sid);
		return subsection.getSectionMode();
	}

	/**
	 * Check if it is a question in the More Detail Mode by qid of a question
	 * 
	 * @param question
	 * @return boolean
	 */
	public static boolean isPersonOrUnion(Question question) {
		init();
		String type = getType(question);
		return Constants.QUESTION_TYPE_MOREDETAIL.equals(type);
	}

	/**
	 * Check if it is a question in the More Detail Mode by sid of a section
	 * 
	 * @param sid
	 * @return boolean
	 */
	public static boolean isPersonOrUnion(String sid) {
		init();
		Subsection subsection = qService.searchSubsection(sid);
		String type = subsection.getSectionMode();
		return Constants.QUESTION_TYPE_MOREDETAIL.equals(type);
	}

	/**
	 * Check if it is a union question
	 * 
	 * @param question
	 * @return boolean
	 */
	public static boolean isUnion(Question question) {
		return "U".equals(question.getType());
	}

	public static boolean isPerson(Question question) {
		return "P".equals(question.getType());
	}

	/**
	 * Check if it is a question in draw mode
	 * 
	 * @param question
	 * @return boolean
	 */
	public static boolean isDraw(Question question) {
		init();
		String type = getType(question);
		return Constants.QUESTION_TYPE_DRAW.equals(type);
	}

	public static boolean isDraw(String sid) {
		init();
		Subsection subsection = qService.searchSubsection(sid);
		return Constants.QUESTION_TYPE_DRAW.equals(subsection.getSectionMode());
	}

	public static boolean isMap(Question question) {
		init();
		String type = getType(question);
		return Constants.QUESTION_TYPE_MAP.equals(type);
	}

	public static boolean isMap(String sid) {
		init();
		Subsection subsection = qService.searchSubsection(sid);
		return Constants.QUESTION_TYPE_MAP.equals(subsection.getSectionMode());
	}
	
	public static boolean isMoreDetailP(Question question) {
		init();
		String type = getType(question);
		return "P".equals(question.getType()) && Constants.QUESTION_TYPE_MOREDETAIL.equals(type);
	}
	public static boolean isMoreDetailU(Question question) {
		init();
		String type = getType(question);
		return "U".equals(question.getType()) && Constants.QUESTION_TYPE_MOREDETAIL.equals(type);
	}
	public static boolean isMoreDetail(String sid) {
		init();
		Subsection subsection = qService.searchSubsection(sid);
		return Constants.QUESTION_TYPE_MOREDETAIL.equals(subsection.getSectionMode());
	}
	
	public static boolean checkPersonByRelevance(Relevance r, KPPerson person, Question question) {
		List<RelevanceCondition> conditions = r.getConditions();
		boolean result = true;
		for (RelevanceCondition condition : conditions) {
			boolean flag = checkByRelevanceCondition(condition, person,  null, question);
			result = result && flag;
		}
		return result;
	}
	
	public static boolean checkUnionByRelevance(Relevance r, KUUnion union, Question question) {
		List<RelevanceCondition> conditions = r.getConditions();
		boolean result = true;
		for (RelevanceCondition condition : conditions) {
			boolean flag = checkByRelevanceCondition(condition, null,  union, question);
			result = result && flag;
		}
		return result;
	}

	public static boolean checkByRelevanceCondition(RelevanceCondition condition,
			KPPerson person, KUUnion union, Question question) {
		if (StringUtils.equals(condition.getType(), Relevance.TYPE_PERSON)) {
			String v = "";
			if (StringUtils.equals(condition.getTypeAttribute(), "pid")) {
				v = person.getPid() + "";
			} else if (StringUtils.equals(condition.getTypeAttribute(),
					"isOther")) {
				v = person.getIsOther() + "";
			} else if (StringUtils.equals(condition.getTypeAttribute(), "sex")) {
				v = person.getSex() + "";
			}
			return matchResult(v, condition.getOp(), condition.getValue());
		} else if (StringUtils.equals(condition.getType(),
				Relevance.TYPE_PERSON_FIELD)) {
			String v = "";
			boolean isConstain = false;
			for (KQQuestionField field : person.getFields()) {
				if (StringUtils.equals(condition.getTypeAttribute(), field.getQid())) {
					v = field.getValue();
					isConstain = true;
					break;
				}
			}
			if(isConstain) {
				return matchResult(v, condition.getOp(), condition.getValue());
			} else {
				return true;
			}
			
		} else if (StringUtils
				.equals(condition.getType(), Relevance.TYPE_UNION)) {
			String v = "";
			if (StringUtils.equals(condition.getTypeAttribute(), "uid")) {
				v = union.getId() + "";
			} else if (StringUtils.equals(condition.getTypeAttribute(),
					"isOther")) {
				v = union.getIsOther() + "";
			}
			return matchResult(v, condition.getOp(), condition.getValue());
		} else if (StringUtils.equals(condition.getType(),
				Relevance.TYPE_UNION_FIELD)) {
			String v = "";
			boolean isConstain = false;
			for (KQQuestionField field : union.getFields()) {
				if (StringUtils.equals(condition.getTypeAttribute(), field.getQid())) {
					v = field.getValue();
					isConstain = true;
					break;
				}
			}
			if(isConstain) {
				return matchResult(v, condition.getOp(), condition.getValue());
			} else {
				return true;
			}
		} else if (StringUtils.equals(condition.getType(),
				Relevance.TYPE_FUNCTION)) {
			String v = "";
			if (StringUtils.equals(condition.getTypeAttribute(),
					Relevance.FUNCTION_MERRIED)) {
				v = kassService.getPersonPartner(person.getPid()) + "";
			} else if (StringUtils.equals(condition.getTypeAttribute(),
					Relevance.FUNCTION_HOUSEHOUD)) {
				v = kassService.getNumByPersonLabel("mapResDistance", "1") + "";
			} else if (StringUtils.equals(condition.getTypeAttribute(),
					Relevance.FUNCTION_STEPKINSHIP)) {
				Routing routing = new Routing();
				routing.initRoute();
				int count = Integer.MAX_VALUE;
				try {
					count = routing.comput_routing(1, person.getPid() + 1);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				v = count + "";
			}
			return matchResult(v, condition.getOp(), condition.getValue());
		}
		return false;
	}

	private static boolean matchResult(String value1, String op, String value2) {
		if (StringUtils.equals(op, Relevance.OP_EQUALS)) {
			return StringUtils.equals(value1, value2);
		} else if (StringUtils.equals(op, Relevance.OP_NO_EQUALS)) {
			return !StringUtils.equals(value1, value2);
		} else if (StringUtils.equals(op, Relevance.OP_GREAT)) {
			if (NumberUtils.isDigits(value1) && NumberUtils.isDigits(value2)) {
				return Double.parseDouble(value1) > Double.parseDouble(value2);
			} else {
				if (value1.compareToIgnoreCase(value2) > 0) {
					return true;
				} else {
					return false;
				}
			}
		} else if (StringUtils.equals(op, Relevance.OP_LESS)) {
			if (NumberUtils.isDigits(value1) && NumberUtils.isDigits(value2)) {
				return Double.parseDouble(value1) < Double.parseDouble(value2);
			} else {
				if (value1.compareToIgnoreCase(value2) < 0) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
}
