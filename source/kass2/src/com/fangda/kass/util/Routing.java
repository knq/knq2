package com.fangda.kass.util;

import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.service.interview.KassService;

/**
 * This class for the model of Route of the kinship path in the KNQ2.
 * 
 * @author Fangfang Zhao
 * 
 */
public class Routing {
	List<Route> routes;
	HeadNode[] G;
	int nodeNum;
	List<Integer> routing_node_path;
	List<Integer> routing_xh;
	private int start_node;
	private int to_node;
	private KassService kassService;

	public Routing() {
		if (kassService == null) {
			kassService = new KassService();
		}
		int maxNode = 0;
		List<Route> routes = new ArrayList<Route>();
		List<KUUnion> list = Constants.KASS.getUnions().getUnions();
		for (KUUnion union : list) {
			List<KUUMember> members = union.getMembers();
			int index = 0;
			for (int i = 0; i < members.size(); i++) {
				KUUMember m1 = members.get(i);
				for (int j = i + 1; j < members.size(); j++) {
					KUUMember m2 = members.get(j);
					Route route = new Route();
					route.fnode = m1.getId() + 1;
					route.tnode = m2.getId() + 1;
					route.index = index;
					route.length = 1;
					route.oneway = 2;
					routes.add(route);
					index++;
					int max = Math.max(route.fnode, route.tnode);
					nodeNum = Math.max(max, nodeNum);
				}

			}
		}

		this.nodeNum = this.nodeNum + 1;
		this.routes = routes;
	}

	public void initRoute() {
		G = new HeadNode[nodeNum];
		for (int i = 0; i < nodeNum; i++) {
			G[i] = new HeadNode();
			G[i].nodeName = i + 1;
			G[i].inDegree = 0;
			G[i].link = null;
		}
		for (int j = 0; j < routes.size(); j++) {
			Route route = routes.get(j);
			int begin = route.fnode;
			int end = route.tnode;
			int weight = route.length;
			if (route.oneway == 2) {
				{
					Node node = new Node();
					node.adjvex = end - 1;
					node.weight = weight;
					++G[end - 1].inDegree;
					node.next = G[begin - 1].link;
					G[begin - 1].link = node;
				}

				{
					Node node = new Node();
					node.adjvex = begin - 1;
					node.weight = weight;
					// ++G[begin - 1].inDegree;
					node.next = G[end - 1].link;
					G[end - 1].link = node;
				}

			}
		}

	}

	/**
	 * Calculate the steps of the path
	 * 
	 * @param startNode
	 * @param endNode
	 * @return
	 */
	public int comput_routing(int startNode, int endNode) {
		start_node = startNode;
		to_node = endNode;
		if (start_node == to_node) {
			return 0;
		}
		Dijkstra(start_node);
		routing_node_path = new ArrayList<Integer>();
		routing_xh = new ArrayList<Integer>();
		save_route_path(to_node);
		int weight = 0;
		for (int i = 0; i < routing_node_path.size() - 1; i++) {
			int start = routing_node_path.get(i);
			int end = routing_node_path.get(i + 1);
			weight = weight + this.getWeight(start, end);
		}
		return weight;
	}

	void save_route_path(int end) {
		routing_xh.add(end);
		if (G[end - 1].d == Integer.MAX_VALUE
				|| G[end - 1].d == Integer.MIN_VALUE) {
			return;
		}
		if (routing_xh.size() > 20) {
			int z = routing_xh.size();
			if ((routing_xh.get(z - 1) == routing_xh.get(z - 3))
					&& (routing_xh.get(z - 3) == routing_xh.get(z - 5))
					&& (routing_xh.get(z - 5) == routing_xh.get(z - 7))
					&& (routing_xh.get(z - 2) == routing_xh.get(z - 4))
					&& (routing_xh.get(z - 4) == routing_xh.get(z - 6))
					&& (routing_xh.get(z - 6) == routing_xh.get(z - 8))) {
				routing_node_path.add(end);
				return;
			}
		}
		if (G[end - 1].parent == -1) {
			// System.out.println("==========1  " + end);
			routing_node_path.add(end);
		} else if (end != 0) {
			save_route_path(G[end - 1].parent + 1);
			// System.out.println("==========2  " + end);
			routing_node_path.add(end);
		}
	}

	int getWeight(int begin, int end) {
		Node node = G[begin - 1].link;
		while (node != null) {
			if (node.adjvex == end - 1) {
				return node.weight;
			}
			node = node.next;
		}
		return Integer.MAX_VALUE;
	}

	void Dijkstra(int start) {
		for (int i = 0; i < nodeNum; i++) {
			G[i].d = Integer.MAX_VALUE;
			G[i].isKnown = false;
		}
		G[start - 1].d = 0;
		G[start - 1].parent = -1;
		while (true) {
			int k;
			boolean ok = true;
			for (k = 0; k < nodeNum; k++) {
				if (!G[k].isKnown) {
					ok = false;
					break;
				}
			}
			if (ok)
				return;
			int i;
			int minIndex = -1;
			for (i = 0; i < nodeNum; i++) {
				if (!G[i].isKnown) {
					if (minIndex == -1)
						minIndex = i;
					else if (G[minIndex].d > G[i].d)
						minIndex = i;
				}
			}
			G[minIndex].isKnown = true;
			Node node = G[minIndex].link;
			while (node != null) {
				int begin = minIndex + 1;
				int end = node.adjvex + 1;
				int weight = getWeight(begin, end);
				if (G[minIndex].d + weight < G[end - 1].d) {
					G[end - 1].d = G[minIndex].d + weight;
					G[end - 1].parent = minIndex;
				}
				node = node.next;
			}
		}
	}

	class HeadNode {// The definition of head node
		int nodeName;
		int inDegree;// the path depth
		int d;// the Path from the start node to this node
		boolean isKnown;// true = The shortest path is known, false = The
						// shortest path is unknown
		int parent;// The parent node
		Node link;// The node of the edge linked to this node
	}

	class Node {// The definition of node
		int adjvex;// the end of the edge
		int weight;// the weight of the edge
		Node next;// the node of the next edge
	}
}
