package com.fangda.kass.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 * This Class for the key and value of sys.properties
 * 
 * @author Fangfang Zhao
 * 
 */
public class ConfigUtil {

	private static String PATH = "sys.properties";

	private static Properties props = new Properties();

	static {
		try {
			props.load(new FileInputStream(PATH));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			System.exit(-1);
		}
	}

	/**
	 * Get the value by the key from the properties file
	 * 
	 * @param key
	 * 
	 * @return String
	 */
	public static String get(String key, String defaultValue) {
		String result = props.getProperty(key);
		if (result == null) {
			return defaultValue;
		}
		return result;
	}

	/**
	 * Set the key - value from the properties file
	 * 
	 * @param keyname
	 * @param keyvalue
	 * 
	 */
	public static void set(String key, String value) {
		try {
			props.load(new FileInputStream(PATH));
			// Call Hashtable.put， getProperty
			OutputStream fos = new FileOutputStream(PATH);
			props.setProperty(key, value);
			// load to Properties
			// write the key-value to output
			props.store(fos, "Update '" + key + "' value");
		} catch (IOException e) {
			System.err.println(".preperties file error");
		}
	}
}
