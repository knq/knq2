package com.fangda.kass.service.interview;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.fangda.kass.model.interview.Project;
import com.fangda.kass.service.BaseService;
import com.fangda.kass.util.Constants;

/**
 * This Class is the service class of an project XML file.
 * 
 * @author Fangfang Zhao
 * 
 */

public class ProjectService extends BaseService {
	
	public static final String PROJECT_NAME = "project.xml";

	/**
	 * Get project
	 * 
	 * @return
	 */
	public Project get() {
		if (Constants.PROJECT != null) {
			return Constants.PROJECT;
		}
		Project q = read(Constants.PROJECT_XML);
		Constants.PROJECT = q;
		return q;
	}

	/**
	 * Save project
	 */
	public void save() {
		saveFile(Constants.PROJECT);
	}

	/**
	 * Save project
	 * 
	 * @param p
	 */
	public void saveFile(Project p) {
		this.saveFile(p, Constants.PROJECT_XML);
	}

	public void saveFile(Project p, String filePath) {
		try {
			JAXBContext context = JAXBContext.newInstance(Project.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");// 编码格式
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);// 是否格式化生成的xml串
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);// 是否省略xml头信息
			marshaller.marshal(p, new File(filePath));
		} catch (JAXBException e) {
			throw new RuntimeException(" read xml file error ");
		}
	}

	/**
	 * Read project file
	 * 
	 * @param rpath
	 * @return
	 */
	public Project read(String rpath) {
		Project project = null;
		try {
			JAXBContext cxt = JAXBContext.newInstance(Project.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			project = (Project) unmarshaller.unmarshal(new File(rpath));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" read xml file error ");
		}
		return project;
	}
}
