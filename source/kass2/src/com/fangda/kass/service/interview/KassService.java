package com.fangda.kass.service.interview;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.davd.support.KnnRela;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.Toast;
import com.fangda.kass.ui.common.Toast.Style;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.support.CalTimer;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class is the service class of an interview XML file.
 * 
 * @author Fangfang Zhao, Shuang Ma, Jing Kong
 * 
 */
public class KassService implements java.io.Serializable {

	public KASS get() {
		if (Constants.KASS != null) {
			return Constants.KASS;
		}
		KASS q = read(Constants.KASS_XML);
		Constants.KASS = q;
		return q;
	}

	public void save() {
		saveFile(Constants.KASS);
	}

	public void saveFile(KASS p) {
		this.saveFile(p, Constants.KASS_XML);
	}

	/**
	 * Saving the interview XML file.
	 * 
	 * @param p
	 */
	public void saveRealFile(KASS p) {
		this.saveFile(p, Constants.KASS_XML);
		FileUtil.copySingleFile(Constants.KASS_XML, Constants.KASS_XML.replace(
				Constants.TMP_EXT, Constants.XML_EXT));
	}

	/**
	 * Saving as the interview XML file.
	 * 
	 * @param p
	 */
	public void saveAsFile(KASS p, JFileChooser chooser) {
		this.saveFile(p, Constants.KASS_XML);
		FileUtil.copySingleFile(Constants.KASS_XML, chooser.getSelectedFile()
				.toString());
	}

	/**
	 * Deleting the temporary interview XML file
	 */
	public void deleteTmpFile() {
		if (Constants.KASS_XML.contains(Constants.TMP_EXT)) {
			File f = new File(Constants.KASS_XML);
			if (f.exists()) {
				f.delete();
			}
		}
	}

	/**
	 * Open an interview XML file and generate a temporary interview XML file
	 * with .tmp file suffix which is a copy of the interview XML file.
	 * 
	 * @param path
	 */
	public String openRealXmlFile(String path) {
		String tmpfile = path.replace(Constants.XML_EXT, Constants.TMP_EXT);
		FileUtil.copySingleFile(path, tmpfile);
		return tmpfile;
	}


	/**
	 * Validation before exit
	 * 
	 * @param km
	 * @param isExit
	 *            0 = not close the main window, 1 = close the main window
	 * @param isCloseInterview
	 *            0 = not close the interview, 1 = close the Interview
	 * @param isCloseProject
	 *            0 = not close the Project, 1 = close the Project
	 * @param isSave
	 */
	public void exitValid(KnqMain km, int isExit, int isCloseInterview,
			int isCloseProject, boolean isSave) {

		if (Constants.KASS_XML != null && Constants.KASS_XML != "") {
			File tmpFile = new File(Constants.KASS_XML);
			File xmlFile = new File(Constants.KASS_XML.replace(
					Constants.TMP_EXT, Constants.XML_EXT));
			if (CalTimer.getInstance().isRuning()) {
				CalTimer.getInstance().stop();
				if (isSave) {
					CalTimer.getInstance().saveLog();
					KassService kassService = new KassService();
					kassService.save();
					kassService.saveRealFile(kassService.get());
					kassService.deleteTmpFile();
				} else {
					KassService kassService = new KassService();
					kassService.deleteTmpFile();
				}
				CalTimer.getInstance().reset();
				IvMainPanel.startBtn.setIcon(UIHelper.getImage("startBtn.png"));
				IvMainPanel.startBtn.updateUI();
			} else if (tmpFile.lastModified() / 1000 * 1000 == xmlFile
					.lastModified() / 1000 * 1000) {
				KassService kassService = new KassService();
				kassService.deleteTmpFile();
			} else {

			}
		} else {

		}
		if (isExit == 1) {
			System.exit(0);
		} else if (isCloseInterview == 1 && Constants.KASS_XML != null
				&& Constants.KASS_XML != "") {
			km.ivMainPanel.removeAll();
			km.ivMainPanel.repaint();
			Constants.KASS_XML = null;
			Constants.KASS = null;
			km.setTitle(null);
			km.ivMainPanel = null;
		} else if (isCloseProject == 1) {
			Constants.PROJECT = null;
			Constants.PROJECT_XML = null;
			Constants.QUESTIONNAIRE = null;
			Constants.QUESTIONNAIRE_XML = null;
			if (Constants.KASS_XML != null && Constants.KASS_XML != "") {
				km.ivMainPanel.removeAll();
				km.ivMainPanel.repaint();
				Constants.KASS_XML = null;
				Constants.KASS = null;
				km.setTitle(null);
			}
			if (km.qnMainPanel != null) {
				km.qnMainPanel.removeAll();
				km.qnMainPanel.repaint();
			}
		} else {
		}
	}


	/**
	 * Validation for the project
	 * 
	 * @param km
	 * @param isProjectNull
	 *            0 = isProjectNotNullAlert (project is not null), 1 =
	 *            isProjectNullAlert_1 (project is null), 2 =
	 *            isProjectNullAlert_2 (pr0ject is null)
	 */
	public void isProjectNullValid(KnqMain km, int isProjectNull) {
		String alert = null;
		if (isProjectNull == 0) {
			if (Constants.PROJECT_XML != null && Constants.PROJECT_XML != "") {
				alert = mxResources.get("isProjectNotNullAlert");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		}
		if (isProjectNull == 1) {
			if (Constants.PROJECT_XML == null || Constants.PROJECT_XML == "") {
				alert = mxResources.get("isProjectNullAlert_1");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (isProjectNull == 2) {
			if (Constants.PROJECT_XML == null || Constants.PROJECT_XML == "") {
				alert = mxResources.get("isProjectNullAlert_2");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}


	/**
	 * Validation for the interview
	 * 
	 * @param km
	 * @param isInterviewNull
	 *            1 Interview is NULL (isInterviewNullAlert), 0 = Interview is
	 *            not NULL (isInterviewNotNullAlert)
	 * 
	 * @return
	 */
	public int isInterviewNullValid(KnqMain km, int isInterviewNull) {
		String alert = null;
		if (isInterviewNull == 1) {
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				alert = mxResources.get("isInterviewNullAlert");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (isInterviewNull == 0) {
			if (Constants.KASS_XML != null && Constants.KASS_XML != "") {
				alert = mxResources.get("isInterviewNotNullAlert");
				int n = JOptionPane.showInternalConfirmDialog(
						km.frame.getContentPane(), alert,
						mxResources.get("KNQ2"),
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.INFORMATION_MESSAGE);
				KassService kassService = new KassService();
				switch (n) {
				case 0:
					kassService.save();
					kassService.saveRealFile(kassService.get());
					kassService.deleteTmpFile();
					Constants.isInterviewCancel = 0;
					break;
				case 1:
					Constants.isInterviewCancel = 0;
					return 1;
				case 2:
					Constants.isInterviewCancel = 1;
					return 0;
				}
			}
		}
		return 0;
	}

	public void saveFile(KASS p, String filePath) {
		try {
			JAXBContext context = JAXBContext.newInstance(KASS.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");// 编码格式
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);// 是否格式化生成的xml串
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);// 是否省略xml头信息
			marshaller.marshal(p, new File(filePath));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" Save xml file error ");
		}
	}

	public KASS read(String rpath) {
		KASS kass = null;
		try {
			JAXBContext cxt = JAXBContext.newInstance(KASS.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			kass = (KASS) unmarshaller.unmarshal(new File(rpath));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" Read xml file error ");
		}
		return kass;
	}

	// ====================================\
	/**
	 * Find Person by his pid.
	 * 
	 * @param pid
	 * @return
	 */
	public KPPerson getPerson(int pid) {
		get();
		KASS kass = Constants.KASS;
		if (kass.getPeople() == null) {
			return null;
		}
		List<KPPerson> persons = kass.getPeople().getPersons();
		if (persons == null || persons.size() <= 0) {
			return null;
		}
		for (KPPerson person : persons) {
			if (person.getPid() == pid) {
				return person;
			}
		}
		return null;
	}

	/**
	 * Get the field by Label of the person question
	 * 
	 * @param pid
	 * @param fieldName
	 * @return
	 */
	public KQQuestionField getPersonField(int pid, String fieldName) {
		KPPerson person = getPerson(pid);
		if (person == null) {
			return null;
		}
		List<KQQuestionField> fields = person.getFields();
		if (fields != null && fields.size() > 0) {
			for (KQQuestionField field : fields) {
				if (StringUtils.equals(field.getLabel(), fieldName)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * Get the field by Label of the union question
	 * 
	 * @param pid
	 * @param fieldName
	 * @return
	 */
	public KQQuestionField getUnionField(int uid, String fieldName) {
		KUUnion union = getUnion(uid);
		if (union == null) {
			return null;
		}
		List<KQQuestionField> fields = union.getFields();
		if (fields != null && fields.size() > 0) {
			for (KQQuestionField field : fields) {
				if (StringUtils.equals(field.getLabel(), fieldName)) {
					return field;
				}
			}
		}
		return null;
	}

	public KQQuestion getPersonQuestion(int pid, String qid) {
		KPPerson person = getPerson(pid);
		if (person == null) {
			return null;
		}
		List<KQQuestion> kqquestions = person.getQuestions();
		if (kqquestions != null && kqquestions.size() > 0) {
			for (KQQuestion question : kqquestions) {
				if (question.getQid().equals(qid)) {
					return question;
				}
			}
		}
		return null;
	}

	public String getPersonQuestionValue(int pid, Question question) {
		if (question == null) {
			return "";
		}
		KQQuestion kquestion = getPersonQuestion(pid, question.getQid());
		if (kquestion != null) {
			List<KQQuestionField> fields = kquestion.getFields();
			List<String> l = new ArrayList<String>();
			for (KQQuestionField field : fields) {
				if (!StringUtils
						.equals(field.getValue(), Constants.DEFAULT_DNA)
						&& !StringUtils.equals(field.getValue(),
								Constants.DEFAULT_NA)) {
					l.add(field.getValue());
				}
			}
			return StringUtils.join(l.toArray(), ",");
		} else {
			KQQuestionField field = this.getPersonField(pid,
					question.getLabel());
			if (field != null) {
				return field.getValue();
			}
			return "";
		}
	}

	/**
	 * Get KQQuestionField by pid, qid and qid of field
	 * 
	 * @param pid
	 * @param qid
	 * @param fname
	 * @return
	 */
	public KQQuestionField getPersonQuestionField(int pid, String qid,
			String fname) {
		KQQuestion kqquestion = getPersonQuestion(pid, qid);
		List<KQQuestionField> fields = kqquestion.getFields();
		if (fields != null && fields.size() > 0) {
			for (KQQuestionField field : fields) {
				if (field.getQid().equals(fname)) {
					return field;
				}
			}
		}
		return null;
	}

	public Map<String, Integer> getAnswerPercentage() {
		List<KQuestion> kquestions = Constants.KASS.getQuestions()
				.getQuestions();
		int count = kquestions.size();
		int answerCount = 0;
		int noanswerCount = 0;
		for (KQuestion kquestion : kquestions) {
			if (kquestion.getCompleted().equals("1")) {
				answerCount++;
			} else {
				if (!kquestion.getIsMarked()) {
					noanswerCount++;
				}
			}
		}
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("total", count);
		map.put("inprocess", count - answerCount - noanswerCount);
		map.put("completed", answerCount);
		map.put("noprocess", noanswerCount);
		return map;
	}

	public KUUnion getUnion(int uid) {
		get();
		KASS kass = Constants.KASS;
		if (kass.getUnions() == null) {
			return null;
		}
		List<KUUnion> unions = kass.getUnions().getUnions();
		if (unions == null || unions.size() <= 0) {
			return null;
		}
		for (KUUnion union : unions) {
			if (union.getId() == uid) {
				return union;
			}
		}
		return null;
	}

	public KUUMember getUnionMember(int uid, int pid) {
		KUUnion union = getUnion(uid);
		List<KUUMember> members = union.getMembers();
		for (KUUMember member : members) {
			if (member.getId() == pid) {
				return member;
			}
		}
		return null;
	}

	/**
	 * Get the all unions of a person by pid
	 * 
	 * @param pid
	 * @return
	 */
	public List<KUUnion> getUnionByPid(int pid) {
		List<KUUnion> us = new ArrayList<KUUnion>();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		if (unions == null || unions.size() <= 0) {
			return us;
		}
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == pid) {
					us.add(union);
					break;
				}
			}
		}
		return us;
	}


	/**
	 * Get the union by pid
	 * 
	 * @param pid
	 * @param type
	 *            Type = offspring or partner
	 * @return
	 */
	public List<KUUnion> getUnionByPid(int pid, String type) {
		List<KUUnion> us = new ArrayList<KUUnion>();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		if (unions == null || unions.size() <= 0) {
			return us;
		}
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == pid
						&& StringUtils.equals(type, member.getType())) {
					us.add(union);
					break;
				}
			}
		}
		return us;
	}

	/**
	 * Get the Person Name by pid
	 * 
	 * @param pid
	 * @return
	 */
	public String getPersonName(int pid) {
		try {
			String sname = getPersonField(pid, "Surname").getValue();
			String oname = getPersonField(pid, "Othernames").getValue();
			return sname + " " + oname;
		} catch (Exception e) {
			Toast.makeText(KnqMain.kmain.frame,
					"Fill person name first in left question!", Style.SUCCESS)
					.display();
			return null;
		}
	}

	/**
	 * Get the Person Birth Year by pid
	 * 
	 * @param pid
	 * @return
	 */
	public String getPersonBirthYear(int pid) {
		String year = getPersonField(pid, "Year_of_birth").getValue();
		if (StringUtils.equals(Constants.DEFAULT_DNA, year)
				|| StringUtils.equals(Constants.DEFAULT_NA, year)) {
			return "";
		}
		return year;
	}

	public String getPersonDeath(int pid) {
		String isDeath = getPersonField(pid, "cbIsDeceased").getValue();
		return isDeath;
	}

	/**
	 * Get the parents of a person by pid
	 * 
	 * @param pid
	 * @return
	 */
	public Map<String, Boolean> getPersonParent(int pid) {
		get();
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == pid
						&& "offspring".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						KPPerson person = this.getPerson(member.getId());
						if (person.getSex() == 0) {
							result.put("father", true);
						}
						if (person.getSex() == 1) {
							result.put("mother", true);
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Get the kinship between person 1 and person 2. ( 1 M mother 2 F father 3
	 * Z sister 4 B brother 5 W wife 6 H husband 7 D daughter 8 S son )
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public List<KnnRela> getUnionBetween(int p1, int p2) {
		List<KnnRela> list = new ArrayList<KnnRela>();
		if (p1 == p2)
			return list;
		get();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();

		String type1 = "";
		String type2 = "";
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == p1) {
					type1 = member.getType();
				}
				if (member.getId() == p2) {
					type2 = member.getType();
				}
			}
			if (StringUtils.isNotBlank(type1) && StringUtils.isNotBlank(type2)) {
				break;
			}
			type1 = "";
			type2 = "";
		}
		if (StringUtils.isNotBlank(type1) && StringUtils.isNotBlank(type2)) {
			KPPerson person1 = this.getPerson(p1);
			KPPerson person2 = this.getPerson(p2);
			if ("partner".equals(type1) && "partner".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("H"));
					list.add(new KnnRela("W"));
				} else {
					list.add(new KnnRela("W"));
					list.add(new KnnRela("H"));
				}
			} else if ("partner".equals(type1) && "offspring".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("F"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("S"));
					} else {
						list.add(new KnnRela("D"));
					}
				} else {
					list.add(new KnnRela("M"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("S"));
					} else {
						list.add(new KnnRela("D"));
					}
				}
			} else if ("offspring".equals(type1) && "partner".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("S"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("F"));
					} else {
						list.add(new KnnRela("M"));
					}
				} else {
					list.add(new KnnRela("D"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("F"));
					} else {
						list.add(new KnnRela("M"));
					}
				}
			} else if ("offspring".equals(type1) && "offspring".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("B"));
				} else {
					list.add(new KnnRela("Z"));
				}
				if (person2.getSex() == 0) {
					list.add(new KnnRela("B"));
				} else {
					list.add(new KnnRela("Z"));
				}
			}
		}
		return list;
	}

	/**
	 * Get the partners of a person
	 * 
	 * @param pid
	 * @return
	 */
	public boolean getPersonPartner(int pid) {
		get();
		boolean has = false;
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == pid && "partner".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				int num = 0;
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						num++;
					}
				}
				if (num >= 2) {
					has = true;
					break;
				}
			}
		}
		return has;
	}

	/**
	 * The number of the partners of a person
	 * 
	 * @param uid
	 * @return
	 */
	public int getCountUnionById(int uid) {
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		int count = 0;
		for (KUUnion union : unions) {
			if (union.getId() == uid) {
				List<KUUMember> members = union.getMembers();
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						count++;
					}
				}
			}
		}
		return count;
	}

	/**
	 * If the two person are partner, return union
	 * 
	 * @param pid1
	 * @param pid2
	 * @return
	 */	
	public KUUnion isPartner(int pid1, int pid2) {
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			List<Integer> pids = new ArrayList<Integer>();
			for (KUUMember member : members) {
				if ("partner".equals(member.getType())) {
					pids.add(member.getId());
				}
				if (pids.contains(pid1) && pids.contains(pid2)) {
					return union;
				}
			}
		}
		return null;
	}

	/**
	 * Get the siblings of a person
	 * 
	 * @param pid
	 * @return
	 */
	public boolean getPersonBrother(int pid) {
		get();
		boolean has = false;
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == pid
						&& "offspring".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				int num = 0;
				for (KUUMember member : members) {
					if ("offspring".equals(member.getType())) {
						num++;
					}
				}
				if (num >= 2) {
					has = true;
					break;
				}
			}
		}
		return has;
	}

	public KUUnion getUnionByPid(int pid1, int pid2) {
		get();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			List<Integer> pids = new ArrayList<Integer>();
			for (KUUMember member : members) {
				if ("partner".equals(member.getType())) {
					pids.add(member.getId());
				}
			}
			String ids = StringUtils.join(pids.toArray(), ",");
			String pidStr1 = pid1 + "," + pid2;
			String pidStr2 = pid2 + "," + pid1;
			if (StringUtils.equals(ids, pidStr1)
					|| StringUtils.equals(pidStr2, ids)) {
				return union;
			}
		}
		return null;
	}

	public KUUnion getPersonUnion(int pid, String unionType) {
		get();
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == pid && unionType.equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				return union;
			}
		}
		return null;
	}

	/**
	 * Get the question by qid
	 * 
	 * @param qid
	 * @return
	 */
	public KQuestion getQuestion(String qid) {
		KASS kass = Constants.KASS;
		if (kass == null || kass.getQuestions() == null) {
			return null;
		}
		List<KQuestion> qustions = kass.getQuestions().getQuestions();
		if (qustions == null || qustions.size() <= 0) {
			return null;
		}
		for (KQuestion qustion : qustions) {
			if (qustion.getId().equals(qid)) {
				return qustion;
			}
		}
		return null;
	}

	/**
	 * Get person
	 * 
	 * @return
	 */
	public List<KPPerson> getKPersons() {
		List<KPPerson> kpeoples = null;
		kpeoples = Constants.KASS.getPeople().getPersons();
		return kpeoples;
	}
	
	/**
	 * Get Unions
	 * 
	 * @return
	 */
	public List<KUUnion> getKUUnions() {
		List<KUUnion> kuunions = null;
		kuunions = Constants.KASS.getUnions().getUnions();
		return kuunions;
	}

	/**
	 * Get the question by qid
	 * 
	 * @param qid
	 * @return
	 */
	public KQuestion getCompleteField(String qid) {
		List<KQuestion> list = Constants.KASS.getQuestions().getQuestions();
		for (KQuestion kquestion : list) {
			if (qid.equals(kquestion.getId())) {
				return kquestion;
			}
		}
		return null;
	}

	/**
	 * Deleting the person by pid
	 * 
	 * @param pid
	 */
	public void deletePersonById(int pid) {
		List<KPPerson> persons = Constants.KASS.getPeople().getPersons();
		if (persons == null || persons.size() <= 0) {
			return;
		}
		KPPerson p = null;
		for (KPPerson person : persons) {
			if (person.getPid() == pid) {
				p = person;
				break;
			}
		}
		if (p != null)
			persons.remove(p);
		List<KUUnion> unions = Constants.KASS.getUnions().getUnions();
		if (unions == null || unions.size() <= 0) {
			return;
		}
		// Deleting this person from its unions
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			KUUMember m = null;
			for (KUUMember member : members) {
				if (member.getId() == pid) {
					m = member;
					break;
				}
			}
			if (m != null) {
				members.remove(m);
			}
		}
		this.save();
	}

	public List<Integer> getEgoParent(boolean isMother) {
		get();
		List<Integer> ids = new ArrayList<Integer>();
		List<KUUnion> unions = this.getUnionByPid(0);
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if ("offspring".equals(member.getType()) && member.getId() == 0) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						KPPerson person = this.getPerson(member.getId());
						if (person.getSex() == 0 && !isMother) {
							ids.add(person.getPid());
						}
						if (person.getSex() == 1 && isMother) {
							ids.add(person.getPid());
						}
					}
				}
			}
		}
		return ids;
	}

	/**
	 * Deleting the union by uid
	 * 
	 * @param uid
	 */
	public void deleteUnionById(int uid) {
		List<KUUnion> unions = Constants.KASS.getUnions().getUnions();
		if (unions == null || unions.size() <= 0) {
			return;
		}
		// Deleting the union
		KUUnion u = null;
		for (KUUnion union : unions) {
			if (uid == union.getId()) {
				u = union;
				break;
			}
		}
		if (u != null)
			unions.remove(u);
		this.save();
	}

	public int getNumByPersonLabel(String label, String value) {
		List<KPPerson> persons = Constants.KASS.getPeople().getPersons();
		if (persons == null || persons.size() <= 0) {
			return 0;
		}
		int num = 0;
		for (KPPerson person : persons) {
			for (KQQuestionField field : person.getFields()) {
				if (StringUtils.equalsIgnoreCase(label, field.getLabel())
						&& StringUtils
								.equalsIgnoreCase(value, field.getValue())) {
					num++;
				}
			}
		}
		return num;
	}
}
