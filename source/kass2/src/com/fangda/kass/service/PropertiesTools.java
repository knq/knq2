package com.fangda.kass.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;

/**
 * The Class for importing the multi-language config and message files of KNQ1.
 * 
 * @author Fangfang Zhao
 * 
 */
public class PropertiesTools {

	public static Integer readProperties(String messageFile, String questionFile, String xmlPath) {

		Questionnaire tQuestionnaire = XmlHelper.getInstance().readFileProperty(xmlPath);
		setEnProperty(tQuestionnaire);
		setMessage(tQuestionnaire, messageFile);		
		Integer ifError=0;
		ifError = setQuestion(tQuestionnaire, questionFile);
		Constants.QUESTIONNAIRE_XML = xmlPath;
		Constants.QUESTIONNAIRE = tQuestionnaire;
		return ifError;
	}
	
	public static void setEnProperty(Questionnaire q) {
		q.setEn_title(q.getTitle());
		q.setEn_cultureArea(q.getCultureArea());
		q.setEn_note(q.getNote());
		
		List<CompleteStep> steps = q.getDraw().getCompleteProcedure().getCompleteStep();
		for(CompleteStep step : steps) {
			step.setEn_title(step.getTitle());
			List<CompleteSubStep> substeps = step.getSubSteps();
			for(CompleteSubStep substep : substeps) {
				substep.setEn_kinshipTerm(substep.getKinshipTerm());
			}
		}
		q.getDraw().getStopRule().setEn_note(q.getDraw().getStopRule().getNote());
		
		List<Subsection> subsections = q.getSection().getSubsections();
		for(Subsection subsection : subsections) {
			subsection.setEn_detail(subsection.getDetail());
			subsection.setEn_title(subsection.getTitle());
			setDefaultQuestion(subsection);
			for(Subsection ss : subsection.getSubSections()) {
				ss.setEn_detail(ss.getDetail());
				ss.setEn_title(ss.getTitle());
				setDefaultQuestion(ss);
			}
		}
	}
	
	public static void setDefaultQuestion(Subsection ss) {
		if(ss.getQuestions() != null) {
			for(Question q : ss.getQuestions()) {
				q.setEn_detail(q.getDetail());
				q.setEn_lead(q.getLead());
				q.setEn_title(q.getTitle());
				if(q.getFollowupQuestions() != null) {
					for(FollowupQuestion fq : q.getFollowupQuestions()) {
						fq.setEn_detail(fq.getDetail());
						fq.setEn_groupName(fq.getGroupName());
						setDefaultOption(fq);
					}
				}
				setDefaultOption(q);
			}
		}
	}
	
	public static void setDefaultOption(Question q) {
		if(q.getOptions() != null) {
			for(Option o : q.getOptions()) {
				o.setEn_detail(o.getDetail());
				o.setEn_note(o.getNote());
			}
		}
	}

	public static void setMessage(Questionnaire q, String messageFile) {
		try {
			InputStream is = PropertiesTools.class.getResourceAsStream("/resources/convert.properties");
			Properties p = loadProperties(is);
			String sections = p.getProperty("Sections");
			String[] sectionArray = sections.split(",");
			Properties mp = loadProperties(new FileInputStream(messageFile));
			for (String s : sectionArray) {
				Subsection subsection = searchSubsection(q, s);
				String subsectionTD = p.getProperty(s);
				String[] tds = subsectionTD.split(",");
				String subsectionTitle = tds[0];
				String subsectionDetail = tds[1];
				subsection.setTitle(mp.getProperty(subsectionTitle));
				subsection.setDetail(mp.getProperty(subsectionDetail));
				
				String qlist = p.getProperty(s + "_QList");
				if(StringUtil.isNotBlank(qlist)) {
					String[] qArray = qlist.split(",");
					for(String qid : qArray) {
						Question question = searchQuestion(q, qid, s);
						if(question == null) {
							question = searchQuestion(q, qid);
						}
						if(question != null) {
							if(p.get(qid + "_title") != null) {
								String title = mp.getProperty(p.getProperty(qid + "_title"));
								question.setTitle(title);
							}
							
							if(p.get(qid + "_detail") != null) {
								String detail = mp.getProperty(p.getProperty(qid + "_detail"));
								question.setDetail(detail);
							}
							if(p.get(qid + "_lead") != null) {
								String lead = mp.getProperty(p.getProperty(qid + "_lead"));
								question.setLead(lead);
							}
							if(p.get(qid + "_label") != null) {
								String label = mp.getProperty(p.getProperty(qid + "_label"));
								question.setLabel(label);
							}
							if(p.get(qid + "_groupName") != null) {
								if(question instanceof FollowupQuestion) {
									FollowupQuestion fquestion = (FollowupQuestion)question;
									String groupName = mp.getProperty(p.getProperty(qid + "_groupName"));
									fquestion.setGroupName(groupName);
								}
							}
							if(p.get(qid + "_option") != null) {
								String option = mp.getProperty(p.getProperty(qid + "_option"));
								if(StringUtil.isNotBlank(option)) {
									String[] optionArray = option.split(";");
									if(question.getOptions() != null) {
										int max = Math.min(optionArray.length, question.getOptions().size());
										for(int i = 0; i < max; i++) {
											question.getOptions().get(i).setDetail(optionArray[i]);
										}
									}
								}
							}
							if(p.get(qid + "_options") != null) {
								String numStr = p.getProperty(qid + "_options");
								int num = Integer.parseInt(numStr);
								for(int i = 0; i < num; i++) {
									String note1 = null;
									String detail1 = null;
									if(p.getProperty(qid + "_" + i + "_detail") != null) {
										String detail = p.getProperty(qid + "_" + i + "_detail");
										detail1 = mp.getProperty(detail);
									}
									if(p.getProperty(qid + "_" + i + "_note") != null) {
										String note = p.getProperty(qid + "_" + i + "_note");
										note1 = mp.getProperty(note);
									}
									setOption(question, qid + "_" + i, detail1, note1);
								}
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Integer setQuestion(Questionnaire q, String questionFile) {
		try {
			Properties p = loadProperties(new FileInputStream(questionFile));
			String sections = p.getProperty("Sections");
			if(sections==null){//无section 文件格式不对
				
				return 1;
			}
			String[] sectionArray = sections.split(";");
			for (String s : sectionArray) {
				String sectionId = sectionId1To2(s);
				Subsection subsection = searchSubsection(q, sectionId);
				String subsectionDetail = p.getProperty(s);
				subsection.setDetail(subsectionDetail);
				String questions = p.getProperty(s + "QList");
				String[] questionArray = questions.split(";");

				for (String questionId : questionArray) {
					String qTitle = p.getProperty(questionId + "Title");
					String qLead = p.getProperty(questionId + "Lead");
					String qDetail = p.getProperty(questionId + "Detail");
					String qSubsection = p.getProperty(questionId + "Subsection");

					Subsection subsection1 = searchSubsection(q, qSubsection);
					String subsectionDetail1 = p.getProperty(qSubsection);
					subsection1.setDetail(subsectionDetail1);

					Question question = searchQuestion(q, questionId.replace("KASS_", "K_"), sectionId);
					if (question != null) {
						question.setTitle(qTitle);
						question.setLead(qLead);
						question.setDetail(qDetail);
					}
					// follow question
					for (int i = 65; i <= 70; i++) {//
						char c = (char) i;
						String fquestionId = questionId + "_" + c;
						if (p.get(fquestionId) != null) {
							String fquestionDetail = (String) p.get(fquestionId);
							Question fq = searchQuestion(q, fquestionId.replace("KASS_", "K_"), sectionId);
							fq.setDetail(fquestionDetail);
							String fquestionNum = fquestionId + "x";
							if (p.get(fquestionNum) != null) {
								int onum = Integer.parseInt((String) p.get(fquestionNum));
								for (int j = 0; j < onum; j++) {
									String optionId = fquestionId + "_" + j;
									setOption(fq, optionId.replace("KASS_", "K_"), p.getProperty(optionId), null);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private static String sectionId1To2(String sectionId) {
		// Section04 = SS_004
		return sectionId.replace("Section", "SS_0");
	}

	private static Question searchQuestion(Questionnaire q, String qid, String sid) {
		Subsection subsection = searchSubsection(q, sid);
		List<Question> questions = subsection.getQuestions();
		if (questions != null && questions.size() > 0) {
			for (Question question : questions) {
				if (question.getQid().equals(qid)) {
					return question;
				}
				if (question.getFollowupQuestions() != null) {
					for (Question fq : question.getFollowupQuestions()) {
						if (fq.getQid().equals(qid)) {
							return fq;
						}
					}
				}
			}
		}
		List<Subsection> ss = subsection.getSubSections();
		if (ss != null && ss.size() > 0) {
			for (Subsection s : ss) {
				List<Question> qs = s.getQuestions();
				if (qs != null && qs.size() > 0) {
					for (Question question : qs) {
						if (question.getQid().equals(qid)) {
							return question;
						}
						if (question.getFollowupQuestions() != null) {
							for (Question fq : question.getFollowupQuestions()) {
								if (fq.getQid().equals(qid)) {
									return fq;
								}
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	private static Question searchQuestion(Questionnaire q, String qid) {
		List<Subsection> sections = q.getSection().getSubsections();
		for(Subsection subsection : sections) {
			Question q1 = searchQuestion(q, qid, subsection.getSid());
			if(q1 != null) {
				return q1;
			}
		}
		return null;
	}

	private static void setOption(Question q, String oid, String detail, String note) {
		if (q.getOptions() != null) {
			for (Option option : q.getOptions()) {
				if (option.getOid().equals(oid)) {
					option.setDetail(detail);
					if(StringUtils.isNotBlank(note)) {
						option.setNote(note);
					}
				}
			}
		}
	}

	private static Subsection searchSubsection(Questionnaire q, String id) {
		List<Subsection> subSectionList = q.getSection().getSubsections();
		for (Subsection ss : subSectionList) {
			if (ss.getSid().equals(id)) {
				return ss;
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					if (subsection.getSid().equals(id)) {
						return subsection;
					}
				}
			}
		}
		return null;
	}

	private static Properties loadProperties(InputStream in) {
		Properties p = new Properties();
		try {
			p.load(new InputStreamReader(in, "UTF-8"));
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return p;
	}
}
