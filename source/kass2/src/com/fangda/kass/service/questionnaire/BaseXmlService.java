package com.fangda.kass.service.questionnaire;

import java.util.List;

import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.BaseService;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.util.Constants;

/**
 * The class for XML files
 * 
 * @author Fangfang Zhao
 * 
 */
public class BaseXmlService extends BaseService {

	public void save(Questionnaire q) {
		// TODO Auto-generated method stub
		resetSortNo(q);
		XmlHelper.getInstance().saveFile(q);
	}

	public Questionnaire get() {
		if (Constants.QUESTIONNAIRE != null) {
			return Constants.QUESTIONNAIRE;
		}
		Questionnaire q = XmlHelper.getInstance().readFile();
		Constants.QUESTIONNAIRE = q;
		return q;
	}

	/**
	 * Reset the Sorting Number
	 * 
	 * @param q
	 */
	private void resetSortNo(Questionnaire q) {
		List<Subsection> subsections = q.getSection().getSubsections();
		if (subsections != null && subsections.size() > 0) {
			for (int i = 0; i < subsections.size(); i++) {
				Subsection s1 = subsections.get(i);
				s1.setSortNo(i + 1);
				int no = resetQuestionSortNo(s1, 0);
				if (s1.getSubSections() != null
						&& s1.getSubSections().size() > 0) {
					for (int j = 0; j < s1.getSubSections().size(); j++) {
						Subsection s2 = s1.getSubSections().get(j);
						s2.setSortNo(j + 1);
						no = resetQuestionSortNo(s2, no);
					}
				}

			}
		}
	}

	private int resetQuestionSortNo(Subsection subsection, int startNo) {
		if (subsection.getQuestions() != null
				&& subsection.getQuestions().size() > 0) {
			for (int i = 0; i < subsection.getQuestions().size(); i++) {
				Question q1 = subsection.getQuestions().get(i);
				q1.setSortNo(startNo + i + 1);
				if (q1.getFollowupQuestions() != null
						&& q1.getFollowupQuestions().size() > 0) {
					for (int j = 0; j < q1.getFollowupQuestions().size(); j++) {
						FollowupQuestion q2 = q1.getFollowupQuestions().get(j);
						q2.setSortNo(j + 1);
					}
				}
			}
			return startNo + subsection.getQuestions().size();
		}
		return 0;
	}
}
