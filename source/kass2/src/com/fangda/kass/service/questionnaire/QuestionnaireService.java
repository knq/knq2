package com.fangda.kass.service.questionnaire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.ui.questionnaire.support.QnTreeNode;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * The class for the Questionnaire XML files
 * 
 * @author Fangfang Zhao
 * 
 */
public class QuestionnaireService extends BaseXmlService {

	static LinkedHashMap<String, String> groupMap = new LinkedHashMap<String, String>();

	/**
	 * The navigation tree of the questions in the left windows
	 * 
	 * @param ifInterview
	 *            1 for interview, 0 for questionnaire
	 * 
	 * @return
	 */

	public DefaultMutableTreeNode getLeftTree(int ifInterview) {
		Questionnaire q = Constants.QUESTIONNAIRE;
		DefaultMutableTreeNode top = new DefaultMutableTreeNode(
				mxResources.get("preface"));
		List<Subsection> subSectionList = q.getSection().getSubsections();
		int sectionIndex = 1;
		for (Subsection ss : subSectionList) {
			QnTreeNode treeNode = new QnTreeNode(0, "" + sectionIndex, ss,
					null, ifInterview);
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeNode);
			// question of subsection.
			if (ss.getQuestions() != null) {
				for (Question question : ss.getQuestions()) {
					if ((question.getTitle() != null && !"".equals(question
							.getTitle()))
							|| (question.getEn_title() != null && !""
									.equals(question.getEn_title()))) {
						QnTreeNode questionNode = new QnTreeNode(2,
								question.getSortNo() + ")", ss, question,
								ifInterview);
						DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(
								questionNode);
						node.add(node1);
					}
				}
			}
			// subsections of subsection
			if (ss.getSubSections() != null) {
				int ssIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					if ((subsection.getTitle() != null && !"".equals(subsection
							.getTitle()))
							|| (subsection.getEn_title() != null && !""
									.equals(subsection.getEn_title()))) {
						QnTreeNode ssNode = new QnTreeNode(1, sectionIndex
								+ "." + ssIndex, subsection, null, ifInterview);
						DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(
								ssNode);
						// questions of subsection
						for (Question question : subsection.getQuestions()) {
							if (StringUtils.isNotBlank(question.getTitle())
									|| StringUtils.isNotBlank(question
											.getEn_title())) {
								QnTreeNode questionNode = new QnTreeNode(2,
										question.getSortNo() + ")", subsection,
										question, ifInterview);
								DefaultMutableTreeNode node2 = new DefaultMutableTreeNode(
										questionNode);
								node1.add(node2);
							}
						}
						node.add(node1);
						ssIndex++;
					}
				}
			}
			top.add(node);
			sectionIndex++;
		}
		return top;
	}

	public Questionnaire getQuestionnaire() {
		return Constants.QUESTIONNAIRE;
	}

	public void saveQuestionnaireFile(Questionnaire q, String filePath) {
		XmlHelper.getInstance().saveFile(q, filePath);
	}

	/*
	 * isQuestionnaireNull=0, Questionnaire is not Null,
	 * isQuestionnaireNotNullAlert isQuestionnaireNull=1, Questionnaire is Null,
	 * isQuestionnaireNullAlert_1 isQuestionnaireNull=2, Questionnaire is Null,
	 * isQuestionnaireNullAlert_2 isQuestionnaireNull=3, Questionnaire is Null,
	 * isQuestionnaireNullAlert_3 isQuestionnaireNull=4, Questionnaire is Null,
	 * isQuestionnaireNullAlert_4
	 */
	public void isQuestionnaireNullValid(KnqMain km, int isQuestionnaireNull) {
		String alert = null;
		if (isQuestionnaireNull == 0) {
			if (Constants.QUESTIONNAIRE_STATUS == 11) {
				System.out.println("Save the revise");
				XmlHelper.getInstance().alertExitQn(km, "revise");
			}
			if (Constants.QUESTIONNAIRE_STATUS == 12) {
				System.out.println("Save the temp revise");
				XmlHelper.getInstance().alertExitQn(km, "temprevise");
			}
			if (Constants.QUESTIONNAIRE_STATUS == 13) {
				System.out.println("Saving the localize");
				XmlHelper.getInstance().alertExitQn(km, "local");
			}
		}
		if (isQuestionnaireNull == 1) {
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				alert = mxResources.get("isQuestionnaireNullAlert_1");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (isQuestionnaireNull == 2) {
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == ""
					|| Constants.QUESTIONNAIRE.getLanguage().equals("en")) {
				alert = mxResources.get("isQuestionnaireNullAlert_2");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (isQuestionnaireNull == 3) {
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				alert = mxResources.get("isQuestionnaireNullAlert_3");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (isQuestionnaireNull == 4) {
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == ""
					|| Constants.QUESTIONNAIRE.getStatus().equals("Formal")) {
				alert = mxResources.get("isQuestionnaireNullAlert_4");
				JOptionPane.showMessageDialog(null, alert, "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	/**
	 * Retrieval the subsection
	 * 
	 * @param id
	 */
	public Subsection searchSubsection(String id) {
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subSectionList = q.getSection().getSubsections();
		for (Subsection ss : subSectionList) {
			if (ss.getSid().equals(id)) {
				return ss;
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					if (subsection.getSid().equals(id)) {
						return subsection;
					}
				}
			}
		}
		return null;
	}

	public List<Question> searchAllQuestion() {
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subSectionList = q.getSection().getSubsections();
		List<Question> questions = new ArrayList<Question>();
		for (Subsection ss : subSectionList) {
			for (Question question : ss.getQuestions()) {
				questions.add(question);
				questions.addAll(question.getFollowupQuestions());
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					for (Question question : subsection.getQuestions()) {
						questions.add(question);
						questions.addAll(question.getFollowupQuestions());
					}
				}
			}
		}
		return questions;
	}

	/**
	 * Retrieval the all unions questions
	 * 
	 * @return
	 */
	public List<Question> searchUnionQuestion() {
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subSectionList = q.getSection().getSubsections();
		List<Question> list = new ArrayList<Question>();
		for (Subsection ss : subSectionList) {
			for (Question qst : ss.getQuestions()) {
				if ("U".equals(qst.getType())) {
					list.add(qst);
				}
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					for (Question qst : subsection.getQuestions()) {
						if ("U".equals(qst.getType())) {
							list.add(qst);
						}
					}
				}
			}
		}
		return list;
	}

	/**
	 * Retrieval the all person questions
	 * 
	 * @return
	 */
	public List<Question> searchPersonQuestion() {
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subSectionList = q.getSection().getSubsections();
		List<Question> list = new ArrayList<Question>();
		for (Subsection ss : subSectionList) {
			for (Question qst : ss.getQuestions()) {
				if ("P".equals(qst.getType())) {
					list.add(qst);
				}
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					for (Question qst : subsection.getQuestions()) {
						if ("P".equals(qst.getType())) {
							list.add(qst);
						}
					}
				}
			}
		}
		return list;
	}

	public Subsection searchParentSubsection(String id) {
		String sid = StringUtil.substringBeforeLast(id, "_");
		return searchSubsection(sid);
	}

	/**
	 * Get the question by qid and sid
	 * 
	 * @param qid
	 * @param sid
	 */
	public Question searchQuestion(String qid, String sid) {
		Subsection subsection = searchSubsection(sid);
		List<Question> questions = subsection.getQuestions();
		if (questions != null && questions.size() > 0) {
			for (Question q : questions) {
				if (q.getQid().equals(qid)) {
					return q;
				}
				if (q.getFollowupQuestions() != null) {
					for (Question fq : q.getFollowupQuestions()) {
						if (fq.getQid().equals(qid)) {
							return fq;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Get the sub questions of a question by qid
	 * 
	 * @param qid
	 * @return
	 */
	public List<FollowupQuestion> listFollowupQuestion(String qid) {
		Question question = searchQuestion(qid);
		return question.getFollowupQuestions();
	}

	public Question searchQuestion(String qid) {
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subsections = q.getSection().getSubsections();
		if (subsections != null) {
			for (Subsection s1 : subsections) {
				Question question = searchQuestion(qid, s1.getSid());
				if (question != null) {
					return question;
				}
				if (s1.getSubSections() != null) {
					for (Subsection s2 : s1.getSubSections()) {
						Question q2 = searchQuestion(qid, s2.getSid());
						if (q2 != null) {
							return q2;
						}
					}
				}
			}
		}
		return null;
	}

	public Option searchQuestionOption(String qid, String oid) {
		Question question = searchQuestion(qid);
		if (question.getOptions() != null) {
			for (Option option : question.getOptions()) {
				if (StringUtils.equals(oid, option.getOid())) {
					return option;
				}
			}
		}
		return null;
	}

	/**
	 * Get the next question by qid, the qid is a qid of leadQuestion
	 * 
	 * @param qid
	 * @return
	 */
	public Question getNextQuestion(String qid) {
		Question question = this.searchQuestion(qid);
		Subsection subsection = this.searchSubsection(question.getSubsection());
		boolean flag = false;
		for (int i = 0; i < subsection.getQuestions().size(); i++) {
			Question q = subsection.getQuestions().get(i);
			if (StringUtils.equals(qid, q.getQid())) {
				if (i == subsection.getQuestions().size() - 1) {
					flag = true;
					break;
				} else {
					return subsection.getQuestions().get(i + 1);
				}
			}
		}
		if (flag) {// Meet the end of a subsection, get the next subsection
			if (subsection.getSubSections() != null
					&& subsection.getSubSections().size() > 0) {
				Subsection ss = subsection.getSubSections().get(0);
				if (ss.getQuestions() != null && ss.getQuestions().size() > 0) {
					return ss.getQuestions().get(0);
				}
			}
			Subsection ss = this.getNextSubsection(subsection.getSid());
			if (ss == null) {
				return null;
			} else {
				if (ss.getQuestions() != null && ss.getQuestions().size() > 0) {
					return ss.getQuestions().get(0);
				} else {
					if (ss.getSubSections() != null
							&& ss.getSubSections().size() > 0) {
						Subsection s = ss.getSubSections().get(0);
						if (s.getQuestions() != null
								&& s.getQuestions().size() > 0) {
							return s.getQuestions().get(0);
						}
					}
				}
			}
		}
		return null;
	}

	public Subsection getNextSubsection(String sid) {
		Subsection ss = this.searchParentSubsection(sid);
		boolean flag = false;
		String rootId = "";
		if (ss != null) {
			rootId = ss.getSid();
			for (int i = 0; i < ss.getSubSections().size(); i++) {
				Subsection s = ss.getSubSections().get(i);
				if (StringUtils.equals(s.getSid(), sid)) {
					if (i == ss.getSubSections().size() - 1) {
						flag = true;
						break;
					} else {
						return ss.getSubSections().get(i + 1);
					}
				}
			}
		} else {
			rootId = sid;
			flag = true;
		}
		if (flag && rootId.length() == 6) {
			Questionnaire q = Constants.QUESTIONNAIRE;
			List<Subsection> subsections = q.getSection().getSubsections();
			int index = -1;
			for (int i = 0; i < subsections.size(); i++) {
				Subsection subsection = subsections.get(i);
				if (rootId.equals(subsection.getSid())) {
					index = i;
					break;
				}
			}
			if (index > -1 && index < subsections.size() - 1) {
				Subsection section = subsections.get(index + 1);
				return section;
			} else {
				return null;
			}
			// rootId = rootId.replace("SS_", "");
			// int i = Integer.parseInt(rootId);
			// String nextId = "";
			// i = i + 1;
			// if(i <= 9) {
			// nextId = "00" + i;
			// } else if(i > 9 && i <= 99) {
			// nextId = "0" + i;
			// } else {
			// nextId = "" + i;
			// }
			// return this.searchSubsection("SS_" + nextId);
		}
		return null;
	}

	/**
	 * Get the previous question by qid, the qid is a qid of leadQuestion
	 * 
	 * @param qid
	 * @return
	 */
	public Question getPrevQuestion(String qid) {
		Question question = this.searchQuestion(qid);
		Subsection subsection = this.searchSubsection(question.getSubsection());
		boolean flag = false;
		for (int i = 0; i < subsection.getQuestions().size(); i++) {
			Question q = subsection.getQuestions().get(i);
			if (StringUtils.equals(qid, q.getQid())) {
				if (i == 0) {
					flag = true;
					break;
				} else {
					return subsection.getQuestions().get(i - 1);
				}
			}
		}
		if (flag) {// Meet the beginning of a subsection, get the previous
					// subsection

			Subsection ss = this.getPrevSubsection(subsection.getSid());
			if (ss == null) {
				return null;
			} else {
				if (ss.getQuestions() != null && ss.getQuestions().size() > 0) {
					return ss.getQuestions().get(ss.getQuestions().size() - 1);
				} else {
					if (ss.getSubSections() != null
							&& ss.getSubSections().size() > 0) {
						Subsection s = ss.getSubSections().get(
								ss.getSubSections().size() - 1);
						if (s.getQuestions() != null
								&& s.getQuestions().size() > 0) {
							return s.getQuestions().get(
									s.getQuestions().size() - 1);
						}
					}
				}
			}
		}
		return null;
	}

	public Subsection getPrevSubsection(String sid) {
		Subsection ss = this.searchParentSubsection(sid);
		boolean flag = false;
		String rootId = "";
		if (ss != null) {
			rootId = ss.getSid();
			for (int i = 0; i < ss.getSubSections().size(); i++) {
				Subsection s = ss.getSubSections().get(i);
				if (StringUtils.equals(s.getSid(), sid)) {
					if (i == 0) {
						flag = true;
						break;
					} else {
						return ss.getSubSections().get(i - 1);
					}
				}
			}
		} else {
			rootId = sid;
			flag = true;
		}
		if (flag && rootId.length() == 6) {
			Questionnaire q = Constants.QUESTIONNAIRE;
			List<Subsection> subsections = q.getSection().getSubsections();
			int index = -1;
			for (int i = 0; i < subsections.size(); i++) {
				Subsection subsection = subsections.get(i);
				if (rootId.equals(subsection.getSid())) {
					index = i;
					break;
				}
			}
			if (index > 0 && index <= subsections.size() - 1) {
				Subsection section = subsections.get(index - 1);
				return section;
			} else {
				return null;
			}
		}
		return null;
	}

	public Question searchParanetQuestion(String qid) {
		String fqid = StringUtil.substringBeforeLast(qid, "_");
		return searchQuestion(fqid);
	}

	public Map<String, String> genSubsectionId(String sid) {
		Map<String, String> map = new HashMap<String, String>();
		String sectionId = "";
		int sortNo = 0;
		if (StringUtils.isBlank(sid)) {
			List<Subsection> subsections = get().getSection().getSubsections();
			if (subsections == null || subsections.size() <= 0) {
				sectionId = "SS_001";
				sortNo = 1;
			} else {
				int max = 0;
				int maxNo = 0;
				for (Subsection ss : subsections) {
					String no = StringUtil.substringAfterLast(ss.getSid(), "_");
					max = Math.max(max, Integer.parseInt(no));
					maxNo = Math.max(maxNo, ss.getSortNo());
				}
				String num = "";
				if (max + 1 <= 9) {
					num = "00" + (max + 1);
				} else if ((max + 1) > 9 && (max + 1) <= 99) {
					num = "0" + (max + 1);
				} else {
					num = "" + (max + 1);
				}
				sectionId = "SS_" + num;
				sortNo = maxNo + 1;
			}
		} else {
			Subsection subsection = this.searchSubsection(sid);
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections == null || subsections.size() <= 0) {
				sectionId = sid + "_01";
				sortNo = 1;
			} else {
				int max = 0;
				int maxNo = 0;
				for (Subsection ss : subsections) {
					String no = StringUtil.substringAfterLast(ss.getSid(), "_");
					max = Math.max(max, Integer.parseInt(no));
					maxNo = Math.max(maxNo, ss.getSortNo());
				}
				String num = "";
				if (max + 1 > 9) {
					num = "" + (max + 1);
				} else {
					num = "0" + (max + 1);
				}
				sectionId = sid + "_" + num;
				sortNo = maxNo + 1;
			}
		}
		map.put("sortNo", "" + sortNo);
		map.put("sectionId", sectionId);
		return map;
	}

	public Map<String, String> genQuestionId(String qid, String sid, String type) {
		Map<String, String> map = new HashMap<String, String>();
		String qestionId = "";
		int sortNo = 0;
		if (StringUtils.isNotBlank(qid)) {
			Question question = searchQuestion(qid);
			int maxSortNo = 0;
			int maxNo = 0;
			if (question.getFollowupQuestions() != null
					&& question.getFollowupQuestions().size() > 0) {
				for (Question fq : question.getFollowupQuestions()) {
					maxSortNo = Math.max(fq.getSortNo(), maxSortNo);
					char c = StringUtil.substringAfterLast(fq.getQid(), "_")
							.toCharArray()[0];
					maxNo = Math.max(maxNo, Integer.valueOf(c));
				}
				qestionId = qid + "_" + (char) (maxNo + 1);
				sortNo = maxSortNo + 1;
			} else {
				qestionId = qid + "_A";
				sortNo = 1;
			}
		} else {
			Subsection parentSection = this.searchParentSubsection(sid);
			if (parentSection == null) {
				parentSection = this.searchSubsection(sid);
			}
			String parent_sid = parentSection.getSid();
			int maxSortNo = 0;
			int maxNo = 0;
			if (parentSection.getQuestions() != null
					&& parentSection.getQuestions().size() > 0) {
				for (Question q : parentSection.getQuestions()) {
					maxSortNo = Math.max(q.getSortNo(), maxSortNo);
					maxNo = Math.max(
							maxNo,
							Integer.parseInt(StringUtil.substringAfterLast(
									q.getQid(), "_")));
				}
			}
			if (parentSection.getSubSections() != null
					&& parentSection.getSubSections().size() > 0) {
				for (Subsection ss : parentSection.getSubSections()) {
					if (ss.getQuestions() != null
							&& ss.getQuestions().size() > 0) {
						for (Question q : ss.getQuestions()) {
							maxSortNo = Math.max(q.getSortNo(), maxSortNo);
							maxNo = Math.max(maxNo, Integer.parseInt(StringUtil
									.substringAfterLast(q.getQid(), "_")));
						}
					}
				}
			}
			if (maxSortNo == 0) {
				qestionId = parent_sid + "_01";
				sortNo = 1;
			} else {
				String num = "";
				if (maxNo + 1 > 9) {
					num = "" + (maxNo + 1);
				} else {
					num = "0" + (maxNo + 1);
				}
				qestionId = parent_sid + "_" + num;
				sortNo = maxSortNo + 1;
			}
		}
		qestionId = qestionId.replace("SS_00", "K_0");

		map.put("sortNo", "" + sortNo);
		map.put("qestionId", qestionId);
		return map;
	}

	public StopRule getStopRule() {
		Questionnaire q = this.get();
		return q.getDraw().getStopRule();
	}

	/**
	 * Get the question by label
	 * 
	 * @param label
	 * @return
	 */
	public boolean getSameByLabel(String label) {
		if (StringUtils.isBlank(label)) {
			return false;
		}
		Questionnaire qn = this.get();
		List<Subsection> sections = qn.getSection().getSubsections();
		for (Subsection section : sections) {
			for (Question q : section.getQuestions()) {
				if (StringUtils.equals(q.getLabel(), label)) {
					return true;
				}
				if (q.getOptions() != null && q.getOptions().size() > 0) {
					for (Option option : q.getOptions()) {
						if (StringUtils.equals(option.getLabel(), label)) {
							return true;
						}
					}
				}
				if (q.getFollowupQuestions() != null
						&& q.getFollowupQuestions().size() > 0) {
					for (FollowupQuestion fq : q.getFollowupQuestions()) {
						if (StringUtils.equals(fq.getLabel(), label)) {
							return true;
						}
						if (fq.getOptions() != null
								&& fq.getOptions().size() > 0) {
							for (Option option : fq.getOptions()) {
								if (StringUtils
										.equals(option.getLabel(), label)) {
									return true;
								}
							}
						}
					}
				}
			}
			if (section.getSubSections() != null
					&& section.getSubSections().size() > 0) {
				for (Subsection ss : section.getSubSections()) {
					if (ss.getQuestions() != null
							&& ss.getQuestions().size() > 0) {
						for (Question q : ss.getQuestions()) {
							if (StringUtils.equals(q.getLabel(), label)) {
								return true;
							}
							if (q.getOptions() != null
									&& q.getOptions().size() > 0) {
								for (Option option : q.getOptions()) {
									if (StringUtils.equals(option.getLabel(),
											label)) {
										return true;
									}
								}
							}
							if (q.getFollowupQuestions() != null
									&& q.getFollowupQuestions().size() > 0) {
								for (FollowupQuestion fq : q
										.getFollowupQuestions()) {
									if (StringUtils
											.equals(fq.getLabel(), label)) {
										return true;
									}
									if (fq.getOptions() != null
											&& fq.getOptions().size() > 0) {
										for (Option option : fq.getOptions()) {
											if (StringUtils.equals(
													option.getLabel(), label)) {
												return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Saving as the questionnaire
	 * 
	 * @param p
	 */
	public void saveAsFile(Questionnaire q, String fileName) {
		this.save(q);
		FileUtil.copySingleFile(Constants.QUESTIONNAIRE_XML, fileName);
	}
}
