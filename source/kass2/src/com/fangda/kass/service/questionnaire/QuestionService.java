package com.fangda.kass.service.questionnaire;

import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;

/**
 * The class for the questions of the questionnaire XML file
 * 
 * @author Fangfang Zhao
 * 
 */
public class QuestionService extends BaseXmlService {

	public List<Question> listAllQuestion(Subsection subsection) {
		List<Question> questions = new ArrayList<Question>();
		for (int i = 0; i < subsection.getQuestions().size(); i++) {
			Question q = subsection.getQuestions().get(i);
			questions.add(q);
			addFollowQuestion(q, questions);
		}
		for (int i = 0; i < subsection.getSubSections().size(); i++) {
			Subsection ss = subsection.getSubSections().get(i);
			for (int j = 0; j < ss.getQuestions().size(); j++) {
				Question q = ss.getQuestions().get(i);
				questions.add(q);
				addFollowQuestion(q, questions);
			}
		}
		return questions;
	}

	private void addFollowQuestion(Question question, List<Question> questions) {
		if (question.getFollowupQuestions() != null) {
			for (Question q : question.getFollowupQuestions()) {
				questions.add(q);
			}
		}
	}

}
