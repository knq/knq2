package com.fangda.kass.service;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.style.RtfParagraphStyle;

/**
 * The Class for exporting the Microsoft Word document (.doc) format of
 * questionnaire
 * 
 * @author Jianguo Zhou
 * 
 */
public class WordTools {
	public static Boolean ifShowDetail;
	static LinkedHashMap<String, String> groupMap = new LinkedHashMap<String, String>();

	/**
	 * @param path
	 */
	public static void saveWord(String path) {
		QuestionnaireService service = new QuestionnaireService();
		Questionnaire q = service.get();
		genWord(q, path);
	}

	private static void genWord(Questionnaire q, String path) {
		Document document = new Document(PageSize.A4.rotate());
		try {
			RtfWriter2.getInstance(document, new FileOutputStream(path));
			document.open();
			// The title
			Font titleFont = new Font(Font.NORMAL, 16, Font.BOLD);
			// The sub title
			Font subTitleFont = new Font(Font.NORMAL, 16, Font.BOLD);

			/* Setting the Heading 1 style */
			RtfParagraphStyle rtfGsBt1 = getTitle1Style();
			/* Setting the Heading 2 style  */
			RtfParagraphStyle rtfGsBt2 = getTitle2Style();

			// The title
			Paragraph title = new Paragraph(q.getTitle());
			title.setAlignment(Element.ALIGN_CENTER);
			title.setFont(titleFont);
			document.add(title);

			// The sub title
			String substring = q.getLanguage();
			substring = substring + " " + q.getVersion();
			Paragraph subTitle = new Paragraph(substring);
			title.setAlignment(Element.ALIGN_CENTER);
			title.setFont(subTitleFont);
			document.add(subTitle);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (document != null)
				document.close();
		}
	}

	/**
	 * Font Setting: Color = Black </BR> 修改日期：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param family
	 *            font type
	 * @param size
	 *            font type，22f = 二号, 18f = 小二号, 16f = 三号
	 * @param style
	 *            font style
	 * @return
	 */
	public static Font setFontStyle(String family, float size, int style) {
		return setFontStyle(family, Color.BLACK, size, style);
	}

	/**
	 * Font Setting </BR> 修改日期：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param family
	 *            font type
	 * @param color
	 *            font color
	 * @param size
	 *            font type，22f为二号，18f为小二号，16f为三号 13.5为四号 12为小四
	 * @param style
	 *            font style
	 * @return
	 */
	public static Font setFontStyle(String family, Color color, float size,
			int style) {
		Font font = new Font();
		font.setFamily(family);
		font.setColor(color);
		font.setSize(size);
		font.setStyle(style);
		return font;
	}

	/**
	 * Add the background color for the text </BR> 修改日期：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 *            The content with background color
	 * @param appendStr
	 *            The content without background color
	 * @return
	 */
	private static Phrase setPhraseStyle(String content, String appendStr) {
		Chunk chunk = new Chunk(content);
		// The background color is light gray
		chunk.setBackground(Color.LIGHT_GRAY);
		Phrase phrase = new Phrase(chunk);
		phrase.add(appendStr);
		return phrase;
	}

	/**
	 * The Style Setting of Paragraph</BR> Revise date：2014-08-07 The previous
	 * part of paragraph with the background color, the remainder of paragraph
	 * without the background color.
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 *            with the background color
	 * @param font
	 * @param firstLineIndent
	 * @param appendStr
	 *            without the background color
	 * @return
	 */
	public static Paragraph setParagraphStyle(String content, Font font,
			float firstLineIndent, String appendStr) {
		Paragraph par = setParagraphStyle(content, font, 0f, 12f);
		Phrase phrase = new Phrase();
		phrase.add(par);
		phrase.add(appendStr);
		Paragraph paragraph = new Paragraph(phrase);
		paragraph.setFirstLineIndent(firstLineIndent);
		paragraph.setAlignment(Paragraph.ALIGN_JUSTIFIED_ALL);
		return paragraph;
	}

	/**
	 * The Style Setting of Paragraph</BR> Revise date：2014-08-07 The previous
	 * part of paragraph with the background color, the remainder of paragraph
	 * without the background color.
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 *            with the background color
	 * @param font
	 * @param firstLineIndent
	 * @param leading
	 * @param appendStr
	 *            without the background color
	 * @return
	 */
	public static Paragraph setParagraphStyle(String content, Font font,
			float firstLineIndent, float leading, String appendStr) {
		Phrase phrase = setPhraseStyle(content, appendStr);
		Paragraph par = new Paragraph(phrase);
		par.setFont(font);
		par.setFirstLineIndent(firstLineIndent);
		par.setLeading(leading);
		par.setAlignment(Paragraph.ALIGN_JUSTIFIED_ALL);
		return par;
	}

	/**
	 * The Style Setting of Paragraph</BR> Revise date：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 * @param font
	 * @param leading
	 * @param alignment
	 * @return
	 */
	public static Paragraph setParagraphStyle(String content, Font font,
			float leading, int alignment) {
		return setParagraphStyle(content, font, 0f, leading, 0f, alignment);
	}

	/**
	 * The Style Setting of Paragraph</BR> Revise date：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 * @param font
	 * @param firstLineIndent
	 * @param leading
	 * @return
	 */
	public static Paragraph setParagraphStyle(String content, Font font,
			float firstLineIndent, float leading) {
		return setParagraphStyle(content, font, firstLineIndent, leading, 0.6f,
				Paragraph.ALIGN_JUSTIFIED_ALL);
	}

	/**
	 * The Style Setting of Paragraph </BR> Revise date：2014-08-07
	 * 
	 * @author Jianguo Zhou
	 * @param content
	 * @param font
	 * @param firstLineIndent
	 * @param leading
	 * @param indentationRight
	 * @param alignment
	 * @return
	 */
	public static Paragraph setParagraphStyle(String content, Font font,
			float firstLineIndent, float leading, float indentationRight,
			int alignment) {
		Paragraph par = new Paragraph(content, font);
		par.setFirstLineIndent(firstLineIndent);
		par.setLeading(leading);
		par.setIndentationRight(indentationRight);
		par.setAlignment(alignment);
		return par;
	}

	public static void exportDoc(String fileName) {
		try {
			Document doc = new Document();
			RtfWriter2.getInstance(doc, new FileOutputStream(fileName));
			doc.open();
			// Page Margins: 25.4 mm =72f, 31.8 mm= 90f (left and right)
			doc.setMargins(90f, 90f, 72f, 72f);

			// Title font style
			Font tfont = setFontStyle("华文中宋", 22f, Font.BOLD);
			
			// Body font style
			Font bfont = setFontStyle("仿宋_GB2312", 16f, Font.NORMAL);

			Paragraph title = setParagraphStyle("测试Itext导出Word文档", tfont, 12f,
					Paragraph.ALIGN_CENTER);

			StringBuffer contentSb = new StringBuffer();
			contentSb.append("最近项目很忙，这个是项目中使用到的，所以现在总结一下，以便今后可以参考使用，");
			contentSb.append("2011年4月27日 — 2011年5月20日，对以下技术进行使用，");
			contentSb.append("Itext、");
			contentSb.append("Excel、");
			contentSb.append("Word、");
			contentSb.append("PPT。");

			Paragraph bodyPar = setParagraphStyle(contentSb.toString(), bfont,
					32f, 18f);
			Paragraph bodyEndPar = setParagraphStyle(
					"截至2014年4月28日，各种技术已经完全实现。", bfont, 32f, 18f);
			Paragraph blankRow = new Paragraph(18f, " ", bfont);
			Paragraph deptPar = setParagraphStyle("（技术开发部盖章）", bfont, 12f,
					Paragraph.ALIGN_RIGHT);
			Paragraph datePar = setParagraphStyle("2011-04-30", bfont, 12f,
					Paragraph.ALIGN_RIGHT);

			doc.add(title);
			doc.add(blankRow);
			doc.add(bodyPar);
			doc.add(bodyEndPar);
			doc.add(blankRow);
			doc.add(blankRow);
			doc.add(blankRow);
			doc.add(deptPar);
			doc.add(datePar);
			
			doc.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * //////////////////////////////////////////////////////////////////// read file
	 * 
	 * @param qid
	 * @param sid
	 */
	public static Questionnaire readproperty(String rpath) {
		Questionnaire q = XmlHelper.getInstance().readFileProperty(rpath);
		if (q != null) {
			return q;
		}

		return null;
	}

	/**
	 * The title style 1
	 * 
	 * @return
	 */
	public static RtfParagraphStyle getTitle1Style() {
		RtfParagraphStyle rtfGsBt1 = RtfParagraphStyle.STYLE_HEADING_1;
		rtfGsBt1.setAlignment(Element.ALIGN_LEFT);
		rtfGsBt1.setStyle(Font.BOLD);
		rtfGsBt1.setSize(14);
		return rtfGsBt1;
	}

	/**
	 * The title style 2
	 * 
	 * @return
	 */
	public static RtfParagraphStyle getTitle2Style() {
		RtfParagraphStyle rtfGsBt2 = RtfParagraphStyle.STYLE_HEADING_2;
		rtfGsBt2.setAlignment(Element.ALIGN_LEFT);
		rtfGsBt2.setStyle(Font.BOLD);
		rtfGsBt2.setSize(12);
		rtfGsBt2.setIndentLeft(18);
		return rtfGsBt2;
	}

	/**
	 * The title style 3
	 * 
	 * @return
	 */
	public static RtfParagraphStyle getTitle3Style() {
		RtfParagraphStyle rtfGsBt2 = RtfParagraphStyle.STYLE_HEADING_3;
		rtfGsBt2.setAlignment(Element.ALIGN_LEFT);
		rtfGsBt2.setStyle(Font.BOLD);
		rtfGsBt2.setSize(12);
		rtfGsBt2.setIndentLeft(24);
		return rtfGsBt2;
	}

	/**
	 * The content style
	 * 
	 * @return
	 */
	public static RtfParagraphStyle getContextStyle() {
		RtfParagraphStyle rtfGsBt2 = RtfParagraphStyle.STYLE_NORMAL;
		rtfGsBt2.setAlignment(Element.ALIGN_LEFT);
		rtfGsBt2.setStyle(Font.NORMAL);
		rtfGsBt2.setSize(18);
		return rtfGsBt2;
	}

	/**
	 * Add the Preface of questionnaire
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addFistChapter(Document document, String text)
			throws DocumentException {
		Paragraph content = WordTools.setParagraphStyle(text, getTitle1Style(),
				32f, 18f);
		content.setFont(getTitle1Style());
		document.add(content);
	}

	/**
	 * Add the content
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addByFont(Document document, String text, Font font)
			throws DocumentException {
		Paragraph content = WordTools.setParagraphStyle(text, font, 32f, 18f);
		content.setFont(font);
		document.add(content);
	}

	/**
	 * Add the section
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addFistSection(Document document, String text)
			throws DocumentException {
		Paragraph content = WordTools.setParagraphStyle(text, getTitle2Style(),
				32f, 18f);
		content.setIndentationLeft(12f);
		document.add(content);
	}

	/**
	 * Add the sub section of section
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addFistSectionChild(Document document, String text)
			throws DocumentException {

		Paragraph content = WordTools.setParagraphStyle(text, getTitle3Style(),
				32f, 18f);
		content.setIndentationLeft(24f);
		document.add(content);
	}

	/**
	 * Add the content
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContent(Document document, String text)
			throws DocumentException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle(text, tfont, 18f,
				Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(18f);
		document.add(ptitle);
	}

	/**
	 * Add the content
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContent2(Document document, String text)
			throws DocumentException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle(text, tfont, 18f,
				Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(24f);
		document.add(ptitle);
	}

	/**
	 * Add the content
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContent2Relevant(Document document, String text)
			throws DocumentException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 11f, Font.ITALIC);
		Paragraph ptitle = WordTools.setParagraphStyle(text, tfont, 18f,
				Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(24f);
		document.add(ptitle);
	}

	/**
	 * Add the content
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContent2PanelOption(Document document, String text)
			throws DocumentException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle(text
				+ " _________________", tfont, 18f, Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(48f);
		document.add(ptitle);
	}

	/**
	 * Add the content with the indentation
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContent3(Document document, String text)
			throws DocumentException {
		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle(text, tfont, 18f,
				Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(12f);
		document.add(ptitle);
	}

	/**
	 * Add the content in the center
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addContentImage(Document document, String text)
			throws DocumentException {
		Font tfont = WordTools.setFontStyle("Arial", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle(text, tfont, 18f,
				Paragraph.ALIGN_CENTER);
		document.add(ptitle);
	}

	/**
	 * Add a blank row
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addKonghang(Document document) throws DocumentException {
		Font bfont = WordTools.setFontStyle("仿宋_GB2312", 16f, Font.NORMAL);
		Paragraph blankRow = new Paragraph(18f, "", bfont);
		document.add(blankRow);
	}

	/**
	 * Add a blank row in half height
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addKonghang0d5(Document document)
			throws DocumentException {
		Font bfont = WordTools.setFontStyle("仿宋_GB2312", 8f, Font.NORMAL);
		Paragraph blankRow = new Paragraph(9f, "", bfont);
		document.add(blankRow);
	}

	/**
	 * Add a blank row in the center
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addKonghangCenter(Document document)
			throws DocumentException {
		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle("", tfont, 16f,
				Paragraph.ALIGN_CENTER);
		document.add(ptitle);
	}

	/**
	 * Add a title
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addZhengBiaoti(Document document, String title)
			throws DocumentException {
		// 设置标题字体样式，粗体、二号、华文中宋
		Font tfont = WordTools.setFontStyle("Times New Roman", 20f, Font.BOLD);
		Paragraph ptitle = WordTools.setParagraphStyle(title, tfont, 30f,
				Paragraph.ALIGN_CENTER);
		document.add(ptitle);
	}

	/**
	 * Add a sub title
	 * 
	 * @return
	 * @throws DocumentException
	 */
	public static void addFuBiaoti(Document document, String title)
			throws DocumentException {
		// 设置标题字体样式，粗体、二号、华文中宋
		Font tfont = WordTools.setFontStyle("Times New Roman", 13f, Font.BOLD);
		Paragraph ptitle = WordTools.setParagraphStyle(title, tfont, 30f,
				Paragraph.ALIGN_CENTER);
		document.add(ptitle);
		addKonghang(document);

	}

	/**
	 * Add a picture
	 * 
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws DocumentException
	 * @throws DocumentException
	 */
	public static void addImage(Document document, String path)
			throws MalformedURLException, IOException, DocumentException {

		addKonghangCenter(document);
		// add a picture
		Image png = Image.getInstance(path);
		png.setAlignment(Image.ALIGN_CENTER);
		png.scaleAbsolute(436, 217);
		document.add(png);

		// the note of the picture
		addContentImage(document,
				"Figure1: A simplified example of diagram of NKK.");
	}

	public static void addContentTextSingle(Document document, String str)
			throws DocumentException, MalformedURLException, IOException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle("____________________",
				tfont, 18f, Paragraph.ALIGN_JUSTIFIED);

		ptitle.setIndentationLeft(30f);

		document.add(ptitle);
	}

	public static void addContentTextSinglePanel(Document document, String str)
			throws DocumentException, MalformedURLException, IOException {

		Font tfont = WordTools
				.setFontStyle("Times New Roman", 12f, Font.NORMAL);
		Paragraph ptitle = WordTools.setParagraphStyle("    _________________",
				tfont, 18f, Paragraph.ALIGN_JUSTIFIED);
		ptitle.setIndentationLeft(30f);

		document.add(ptitle);
	}

	/**
	 * @param args
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws DocumentException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException, DocumentException {
		// Questionnaire q =
		// XmlHelper.getInstance().readFileProperty("H:\\2014-08-03\\questions_en_EN_v2.properties");
		// writedoc(q, "H:\\a.doc");
	}
}
