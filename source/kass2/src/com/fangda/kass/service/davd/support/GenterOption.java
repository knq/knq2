package com.fangda.kass.service.davd.support;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class for the setting of DAVD. The abbreviation DAVD stands for ‘download
 * and variable’, which generates three different types of data matrices for
 * analysis: A set of network matrices, the person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GenterOption {

	/**
	 * The list of the interview xml file which is the source file used for
	 * generates three different types of data matrices.
	 */
	private List<String> ivFileNames = new ArrayList<String>();
	
	/**
	 * The filename including path of questionnaire applied to interview, which
	 * can be empty.
	 */
	private String questionFileName;
	
	/**
	 *  The directory path of the output data files.
	 */
	private String outFileName;
	
	/**
	 * The generating rule of the field name of the table of the data matrices
	 * 1 stand for using 'qid' as the field name.
	 * 2 stand for using 'label' as the field name.
	 * 3 stand for using 'qid + " " + label' as the field name.
	 */
	private Integer varType;
	
	/**
	 * The file form of the output file of the data matrices, which provides two
	 * file types: .cvs and .txt
	 */
	private List<String> exts = new ArrayList<String>();

	/**
	 * 1 - 10 stand for generating network the matrices tables.
	 */
	private Integer generType;

	public List<String> getIvFileNames() {
		return ivFileNames;
	}

	public void setIvFileNames(List<String> ivFileNames) {
		this.ivFileNames = ivFileNames;
	}

	public String getOutFileName() {
		return outFileName;
	}

	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	public Integer getVarType() {
		return varType;
	}

	public void setVarType(Integer varType) {
		this.varType = varType;
	}

	public List<String> getExts() {
		return exts;
	}

	public void setExts(List<String> exts) {
		this.exts = exts;
	}

	public Integer getGenerType() {
		return generType;
	}

	public void setGenerType(Integer generType) {
		this.generType = generType;
	}

	public String getQuestionFileName() {
		return questionFileName;
	}

	public void setQuestionFileName(String questionFileName) {
		this.questionFileName = questionFileName;
	}

}
