package com.fangda.kass.service.davd.support;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class for the node of the kinship path of NKK for DAVD. The abbreviation
 * DAVD stands for ‘download and variable’, which generates three different
 * types of data matrices for analysis: A set of network matrices, the person
 * matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class Node {
	// ID fnode tnode
	public int adjvex;
	public boolean isKnown = false;
	public int weight;
	public List<KnnOneLine> lines = new ArrayList<KnnOneLine>();
}
