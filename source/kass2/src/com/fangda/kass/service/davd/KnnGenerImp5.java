package com.fangda.kass.service.davd;

import java.util.List;

import com.fangda.kass.service.davd.support.KnnOneLine;
import com.fangda.kass.service.davd.support.KnnRoute;
import com.fangda.kass.service.davd.support.KnnRouting;
import com.fangda.kass.util.Constants;

/**
 * Generating the Matrix Table 5 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImp5 extends KnnGener {

	@Override
	public String[][] genResult() {

		// TODO Auto-generated method stub
		String path = Constants.KASS_XML;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			path = option.getIvFileNames().get(0);
		}
		KnnRouting r = new KnnRouting(path, 0);
		r.cal(0);
		List<KnnOneLine> ones = r.getSortingResult3();
		
		int max = 0;
		for(KnnOneLine one : ones) {
			max = Math.max(one.getRoutes().size(), max);
			columns.add(" ");
		}
		for(int i = 0; i < max; i++) {
			header.add("uid");
			header.add("pid");
		}
		
		String[][] result = new String[ones.size()][max * 2];
		for(int i  = 0; i < ones.size(); i++) {
			KnnOneLine one = ones.get(i);
			for(int j = 0; j < one.getRoutes().size(); j++) {
				KnnRoute route = one.getRoutes().get(j);
				result[i][2*j] = route.uid + "";
				result[i][2*j + 1] = route.tnode + "";
			}
		}
		return result;
	}

	@Override
	public String getFileName() {
		// TODO Auto-generated method stub
		return "m5";
	}

}
