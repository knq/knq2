package com.fangda.kass.service.davd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.davd.support.KnnOneLine;
import com.fangda.kass.service.davd.support.KnnRela;
import com.fangda.kass.service.davd.support.KnnRoute;
import com.fangda.kass.service.davd.support.KnnRouting;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.ExportProgressUtil;

/**
 * Generating the the person matrix which contains the characteristics of the
 * individual persons as well as their interactions with the informant and among
 * themselves.
 * 
 * @author zhaofangfang
 * 
 */
public class knnGenerImpPerson extends KnnGener {

	List<String> questionHeaders = new ArrayList<String>();
	List<String> questionIds = new LinkedList<String>();
	Questionnaire q = null;

	@Override
	public String[][] genResult() {

		String questionaireFileName = option.getQuestionFileName();
		List<String> fileNames = option.getIvFileNames();
		if (StringUtils.isNotBlank(questionaireFileName)) {
			q = XmlHelper.getInstance().readFileProperty(questionaireFileName);
		}

		List<KnnGenterPerson> kps = new ArrayList<KnnGenterPerson>();
		for (String fileName : fileNames) {
			KASS k = kassService.read(fileName);
			List<KnnGenterPerson> ps = handle(k, fileName);
			kps.addAll(ps);
		}
		int maxPartnerUidSize = 0;
		int maxOffspringUidSize = 0;
		int maxStep = 0;
		for (KnnGenterPerson kp : kps) {
			maxPartnerUidSize = Math.max(maxPartnerUidSize,
					kp.partnerUids.size());
			maxOffspringUidSize = Math.max(maxOffspringUidSize,
					kp.offspringUids.size());
			maxStep = Math.max(maxStep, kp.steps.size());
		}
		header.add("serialNumber");
		header.add("pid");
		header.add("sex");
		header.add("isOther");
		String[][] result = new String[kps.size()][5 + questionHeaders.size()
				+ maxPartnerUidSize + maxOffspringUidSize + maxStep + 2];

		for (int i = 0; i < kps.size(); i++) {
			KnnGenterPerson kp = kps.get(i);
			result[i][0] = kp.serialNumber;
			result[i][1] = kp.person.getPid() + "";
			result[i][2] = kp.person.getSex() + "";
			result[i][3] = kp.person.getIsOther() + "";
			// question
			for (int j = 0; j < questionHeaders.size(); j++) {
				String t = questionHeaders.get(j);
				if (kp.questionValues.get(t) == null) {
					result[i][4 + j] = QuestionConstants.getAnswerCode("DCR");
				} else {
					result[i][4 + j] = kp.questionValues.get(t);
				}
				if (i == 0) {
					header.add(t);
				}

			}
			int index = 4 + questionHeaders.size();

			// uid partner
			if (i == 0) {
				header.add("num_P_unions");
			}

			index = index + 1;
			// uid partner
			int numP = 0;
			for (int j = 0; j < maxPartnerUidSize; j++) {
				if (i == 0) {
					header.add("partner_union_" + (j + 1));
				}
				if (j < kp.partnerUids.size()) {
					result[i][index + j] = kp.partnerUids.get(j).toString();
					numP++;
				} else {
					result[i][index + j] = QuestionConstants
							.getAnswerCode("partner");
				}
			}
			result[i][index - 1] = numP + "";

			index = 4 + questionHeaders.size() + 1 + maxPartnerUidSize;
			// uid offspring
			if (i == 0) {
				header.add("num_CH_unions");
			}
			// uid offspring
			int numCH = 0;
			index = index + 1;
			for (int j = 0; j < maxOffspringUidSize; j++) {
				if (i == 0) {
					header.add("childhood_union_" + (j + 1));
				}
				if (j < kp.offspringUids.size()) {
					result[i][index + j] = kp.offspringUids.get(j).toString();
					numCH++;
				} else {
					result[i][index + j] = QuestionConstants
							.getAnswerCode("offspring");
				}
			}
			result[i][index - 1] = numCH + "";

			index = 4 + questionHeaders.size() + 1 + maxPartnerUidSize + 1
					+ maxOffspringUidSize;
			// dist
			if (i == 0) {
				header.add("kindist");
			}
			if (kp.person.getIsOther() == 1) {
				result[i][index] = (QuestionConstants.getAnswerCode("kindist"))
						+ "";
			} else {
				result[i][index] = (kp.steps.size()) + "";
			}

			index = index + 1;
			// step
			for (int j = 0; j < maxStep; j++) {
				if (i == 0) {
					header.add("kinstep" + (j + 1));
				}
				if (j < kp.steps.size()) {
					result[i][index + j] = kp.steps.get(j) + "";
				} else {
					result[i][index + j] = QuestionConstants
							.getAnswerCode("kinstep");
				}
			}
		}

		return result;
	}

	private List<KnnGenterPerson> handle(KASS k, String fileName) {
		List<KnnGenterPerson> kps = new ArrayList<KnnGenterPerson>();
		List<KPPerson> persons = k.getPeople().getPersons();
		if (persons != null && persons.size() > 0) {
			int i = 0;
			for (KPPerson person : persons) {
				int va = (int) ((i * 1.0 / persons.size()) * 45);
				if (ExportProgressUtil.getInstance().getProgressBar() != null) {
					ExportProgressUtil.getInstance().getProgressBar()
							.setValue(va);
				}
				KnnGenterPerson kp = new KnnGenterPerson();
				kp.person = person;
				kp.serialNumber = k.getInterviewInfo().getSerialNumber();

				for (KQQuestionField field : person.getFields()) {
					String value = getFieldValue(person, field.getLabel());
					String t = getHeaderByOption(field.getLabel(),
							field.getQid());
					if (!questionHeaders.contains(t)) {
						addBySorting(field.getQid(), t);
						// questionHeaders.add(t);
					}
					kp.questionValues.put(t, value);
				}
				for (KQQuestion ques : person.getQuestions()) {
					//
					String value1 = QuestionConstants.getAnswerCode(ques
							.getValue());
					String t1 = getHeaderByOption(ques.getLabel(),
							ques.getQid());
					if (!questionHeaders.contains(t1)) {
						addBySorting(ques.getQid(), t1);
						// questionHeaders.add(t1);
					}
					kp.questionValues.put(t1, value1);

					if (ques.getFields() != null) {
						for (KQQuestionField field : ques.getFields()) {
							String value = getQuestionValue(person,
									field.getLabel());
							String t = getHeaderByOption(field.getLabel(),
									field.getQid());
							if (!questionHeaders.contains(t)) {
								addBySorting(field.getQid(), t);
								// questionHeaders.add(t);
							}
							kp.questionValues.put(t, value);
						}
					} else {
						String value = getQuestionValue(person, ques.getLabel());
						String t = getHeaderByOption(ques.getLabel(),
								ques.getQid());
						if (!questionHeaders.contains(t)) {
							addBySorting(ques.getQid(), t);
							// questionHeaders.add(t);
						}
						kp.questionValues.put(t, value);
					}
				}
				addQRC(kp, person, persons);
				// UID
				kp.partnerUids = getPersonPartener(k, person);
				kp.offspringUids = getPersonOffspring(k, person);
				// ROUTING
				List<Integer> steps = getSorting(k, person, fileName);
				kp.steps = steps;
				kps.add(kp);
				i++;
			}
		}

		return kps;
	}

	private void addQRC(KnnGenterPerson kp, KPPerson person,
			List<KPPerson> persons) {
		if (q != null) {
			for (int i = 0; i < questionIds.size(); i++) {
				Question question = this.searchQuestion(q, questionIds.get(i));
				if (question != null
						&& StringUtils.equals(question.getType(), "R")) {
					int index = getQRCIndex(i, question.getQid(),
							question.getLabel());
					if (index == -1) {
						continue;
					}
					String aKey = question.getQid() + "_" + question.getLabel()
							+ "-Sum-NKK";
					String bKey = question.getQid() + "_" + question.getLabel()
							+ "-Sum-Other";
					if (option.getVarType() == 1) {
						aKey = question.getQid() + "-Sum-NKK";
						bKey = question.getQid() + "-Sum-Other";
					} else if (option.getVarType() == 2) {
						aKey = question.getLabel() + "-Sum-NKK";
						bKey = question.getLabel() + "-Sum-Other";
					}

					if (!questionHeaders.contains(aKey)) {
						this.questionHeaders.add(index, aKey);
						this.questionHeaders.add(index + 1, bKey);
					}
					KQQuestion ques = searchKQQuestion(person,
							question.getQid());

					if (ques.getFields() != null) {
						int nkkNum = 0;
						int otherNum = 0;
						for (KQQuestionField field : ques.getFields()) {
							String pid = field.getValue();
							KPPerson per = this.searchPersonByPid(persons, pid);
							if (per != null) {
								if (per.getIsOther() == 1) {
									otherNum++;
								} else {
									nkkNum++;
								}
							}
						}

						kp.questionValues.put(aKey, nkkNum + "");
						kp.questionValues.put(bKey, otherNum + "");
					} else {
						kp.questionValues.put(aKey, 0 + "");
						kp.questionValues.put(bKey, 0 + "");
					}
				}

				// if(question != null && StringUtils.equals(question.getType(),
				// "R")) {
				// System.out.println(question.getQid());

				// }
			}
		}
	}

	private KQQuestion searchKQQuestion(KPPerson person, String qid) {

		List<KQQuestion> questions = person.getQuestions();
		for (KQQuestion question : questions) {
			if (StringUtils.equals(qid, question.getQid())) {
				return question;
			}
		}
		return null;
	}

	private int getQRCIndex(int index, String qid, String label) {
		String key = qid + "_" + label + "-Sum-NKK";
		if (option.getVarType() == 1) {
			key = qid + "-Sum-NKK";
		} else if (option.getVarType() == 2) {
			key = label + "-Sum-NKK";
		}
		if (questionIds.contains(key)) {
			return -1;
		}
		for (int i = index + 1; i < questionIds.size(); i++) {
			if (questionIds.get(i).startsWith(qid)) {
				continue;
			} else {
				return i;
			}
		}
		return questionIds.size();
	}

	@Override
	public String getFileName() {
		return "mperson";
	}

	private String getFieldValue(KPPerson person, String label) {
		List<KQQuestionField> fields = person.getFields();
		for (KQQuestionField field : fields) {
			if (StringUtils.equals(field.getLabel(), label)) {
				return QuestionConstants.getAnswerCode(field.getValue());
			}
		}
		return null;
	}

	private String getQuestionValue(KPPerson person, String label) {
		List<KQQuestion> qs = person.getQuestions();
		for (KQQuestion q : qs) {
			if (q.getFields() != null) {
				for (KQQuestionField field : q.getFields()) {
					if (StringUtils.equals(field.getLabel(), label)) {
						return QuestionConstants
								.getAnswerCode(field.getValue());
					}
				}
			} else {
				if (StringUtils.equals(q.getLabel(), label)) {
					return QuestionConstants.getAnswerCode(q.getValue());
				}
			}
		}
		return null;
	}

	private String getHeaderByOption(String label, String qid) {
		if (option.getVarType() == 1) {
			return qid;
		} else if (option.getVarType() == 2) {
			return label;
		} else {
			return qid + "_" + label;
		}
	}

	/**
	 * Get all partners of this person
	 * 
	 * @param k
	 * @param person
	 * @return
	 */
	private List<Integer> getPersonPartener(KASS k, KPPerson person) {
		List<KUUnion> unions = k.getUnions().getUnions();
		List<Integer> uids = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == person.getPid()
						&& "partner".equals(member.getType())) {
					uids.add(union.getId());
				}
			}
		}
		return uids;
	}

	private List<Integer> getPersonOffspring(KASS k, KPPerson person) {
		List<KUUnion> unions = k.getUnions().getUnions();
		List<Integer> uids = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == person.getPid()
						&& "offspring".equals(member.getType())) {
					uids.add(union.getId());
				}
			}
		}
		return uids;
	}

	private List<Integer> getSorting(KASS kass, KPPerson person, String path) {
		KnnRouting r = new KnnRouting(path, 1);
		r.cal(0);
		List<KnnOneLine> ones = r.getSortingResult3();

		List<Integer> b = new ArrayList<Integer>();

		for (int i = 0; i < ones.size(); i++) {
			KnnOneLine one = ones.get(i);
			KnnRoute kr = one.getRoutes().get(one.getRoutes().size() - 1);
			if (kr.tnode == person.getPid()) {
				for (int j = 0; j < one.getRoutes().size(); j++) {
					KnnRoute r1 = one.getRoutes().get(j);
					List<KnnRela> list = getUnionBetween(kass, r1.fnode,
							r1.tnode);
					KnnRela rela = list.get(1);
					b.add(rela.getNum());
				}
			}
		}
		return b;
	}

	private class KnnGenterPerson {

		String serialNumber;
		KPPerson person;
		Map<String, String> questionValues = new HashMap<String, String>();
		List<Integer> partnerUids = new ArrayList<Integer>();
		List<Integer> offspringUids = new ArrayList<Integer>();
		List<Integer> steps = new ArrayList<Integer>();
	}

	private void addBySorting(String qid, String headerName) {
		int a = addQidBySorting(qid);
		if (a >= questionHeaders.size()) {
			this.questionHeaders.add(headerName);
		} else {
			this.questionHeaders.add(a, headerName);
		}
	}

	private int addQidBySorting(String qid) {
		if (questionIds.size() <= 0) {
			questionIds.add(qid);
			return 0;
		} else {
			for (int i = 0; i < questionIds.size(); i++) {
				String id = questionIds.get(i);
				if (qid.compareTo(id) < 0) {
					questionIds.add(i, qid);
					return i;
				}
			}
		}
		questionIds.add(qid);
		return questionIds.size();
	}

	private Question searchQuestion(Questionnaire q, String qid) {
		List<Subsection> subsections = q.getSection().getSubsections();
		if (subsections != null) {
			for (Subsection s1 : subsections) {
				Question question = searchQuestion(q, qid, s1.getSid());
				if (question != null) {
					return question;
				}
				if (s1.getSubSections() != null) {
					for (Subsection s2 : s1.getSubSections()) {
						Question q2 = searchQuestion(q, qid, s2.getSid());
						if (q2 != null) {
							return q2;
						}
					}
				}
			}
		}
		return null;
	}

	private Question searchQuestion(Questionnaire ques, String qid, String sid) {
		Subsection subsection = searchSubsection(ques, sid);
		List<Question> questions = subsection.getQuestions();
		if (questions != null && questions.size() > 0) {
			for (Question q : questions) {
				if (q.getQid().equals(qid)) {
					return q;
				}
				if (q.getFollowupQuestions() != null) {
					for (Question fq : q.getFollowupQuestions()) {
						if (fq.getQid().equals(qid)) {
							return fq;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Get subsection
	 * 
	 * @param id
	 */
	public Subsection searchSubsection(Questionnaire q, String id) {
		List<Subsection> subSectionList = q.getSection().getSubsections();
		for (Subsection ss : subSectionList) {
			if (ss.getSid().equals(id)) {
				return ss;
			}
			if (ss.getSubSections() != null) {
				for (Subsection subsection : ss.getSubSections()) {
					if (subsection.getSid().equals(id)) {
						return subsection;
					}
				}
			}
		}
		return null;
	}

	private KPPerson searchPersonByPid(List<KPPerson> persons, String pid) {
		for (KPPerson person : persons) {
			if (StringUtils.equals(pid, person.getPid() + "")) {
				return person;
			}
		}
		return null;
	}
}
