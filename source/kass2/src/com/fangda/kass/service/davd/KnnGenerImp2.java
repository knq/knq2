package com.fangda.kass.service.davd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;

/**
 * Generating the Matrix Table 2 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author  Fangfang Zhao
 * 
 */
public class KnnGenerImp2 extends KnnGener {

	@Override
	public String[][] genResult() {
		KASS kass = null;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			kass = kassService.read(option.getIvFileNames().get(0));
		} else {
			kass = kassService.get();
		}
		List<KPPerson> kps = kass.getPeople().getPersons();
		Map<Integer, Integer> pidIndexMap = new HashMap<Integer, Integer>();
		int index = 0;
		for(KPPerson person : kps ) {
			if(person.getIsOther() == 0) {
				columns.add(person.getPid() + "");
				pidIndexMap.put(person.getPid(), index);
				index ++;
			}
		}
		List<KUUnion> uns = kass.getUnions().getUnions();
		List<KUUnion> removes = new ArrayList<KUUnion>();
		for(KUUnion union : uns) {
			if(union.getIsOther() == 0) {
				header.add(union.getId() + "");
			} else {
				removes.add(union);
			}
		}
		uns.removeAll(removes);
		String[][] result = new String[columns.size()][header.size()];
		for(int j = 0; j < uns.size(); j++) {
			KUUnion union = uns.get(j);
			List<KUUMember> kms = union.getMembers();
			for(KUUMember m : kms) {
				int pindex = pidIndexMap.get(m.getId());
				String type = m.getType();
				KPPerson person = getPerson(kass, m.getId());
				if("partner".equals(type)) {
					if(person.getSex() == 0) {
						result[pindex][j] = "2";
					} else {
						result[pindex][j] = "1";
					}
				} else {
					if(person.getSex() == 0) {
						result[pindex][j] = "4";
					} else {
						result[pindex][j] = "3";
					}
				}
			}
		}
		return result;
	}

	@Override
	public String getFileName() {
		return "m2";
	}
	
	public KPPerson getPerson(KASS kass, int pid) {
		if (kass.getPeople() == null) {
			return null;
		}
		List<KPPerson> persons = kass.getPeople().getPersons();
		if (persons == null || persons.size() <= 0) {
			return null;
		}
		for (KPPerson person : persons) {
			if (person.getPid() == pid) {
				return person;
			}
		}
		return null;
	}

}
