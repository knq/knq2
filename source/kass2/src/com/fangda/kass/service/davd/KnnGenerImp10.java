package com.fangda.kass.service.davd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;

/**
 * Generating the Matrix Table 10 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImp10 extends KnnGener {

	@Override
	public String[][] genResult() {
		KASS kass = null;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			kass = kassService.read(option.getIvFileNames().get(0));
		} else {
			kass = kassService.get();
		}
		List<KPPerson> kps = kass.getPeople().getPersons();
		Map<Integer, Integer> pidIndexMap = new HashMap<Integer, Integer>();
		int index = 0;
		for (KPPerson person : kps) {
			columns.add(person.getPid() + "");
			pidIndexMap.put(person.getPid(), index);
			index++;
		}
		List<KUUnion> uns = kass.getUnions().getUnions();
		for (KUUnion union : uns) {
			header.add(union.getId() + "");
		}
		String[][] result = new String[columns.size()][header.size()];
		for (int j = 0; j < uns.size(); j++) {
			KUUnion union = uns.get(j);
			List<KUUMember> kms = union.getMembers();
			for (KUUMember m : kms) {
				int pindex = pidIndexMap.get(m.getId());
				String type = m.getType();
				if ("partner".equals(type)) {
					result[pindex][j] = "1";
				} else {
					result[pindex][j] = "2";
				}
			}
		}
		return result;
	}

	@Override
	public String getFileName() {
		return "m10";
	}

}
