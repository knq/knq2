package com.fangda.kass.service.davd.support;

import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.interview.KPPerson;

/**
 * The Class for the result of the data matrices for DAVD. The abbreviation DAVD
 * stands for ‘download and variable’, which generates three different types of
 * data matrices for analysis: A set of network matrices, the person matrix and
 * the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnResult {

	/**
	 * The person of the NKK diagram
	 */
	private KPPerson person;
	/**
	 * The lines of route
	 */
	private List<KnnOneLine> list = new ArrayList<KnnOneLine>();
	
	public KPPerson getPerson() {
		return person;
	}
	public void setPerson(KPPerson person) {
		this.person = person;
	}
	public List<KnnOneLine> getList() {
		return list;
	}
	public void setList(List<KnnOneLine> list) {
		this.list = list;
	}
}
