package com.fangda.kass.service.davd.support;

/**
 * The Class for initializing the routes of the kinship path of NKK for DAVD.
 * The abbreviation DAVD stands for ‘download and variable’, which generates
 * three different types of data matrices for analysis: A set of network
 * matrices, the person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnRoute {

	public int index;
	public int fnode;
	public int tnode;
	public int length = 1;
	public int oneway = 2;
	public int uid;
	
	@Override
	public String toString() {
		return fnode + "->" + tnode;
	}
	
}
