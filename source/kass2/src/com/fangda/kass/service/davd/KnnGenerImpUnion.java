package com.fangda.kass.service.davd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.ExportProgressUtil;

/**
 * Generating the the union matrix which contains all the information about the
 * unions and their members.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImpUnion extends KnnGener {

	List<String> questionHeaders = new ArrayList<String>();
	List<String> questionIds = new ArrayList<String>();

	@Override
	public String[][] genResult() {
		List<String> fileNames = option.getIvFileNames();

		List<KnnGenterUnion> kpu = new ArrayList<KnnGenterUnion>();
		for (String fileName : fileNames) {
			KASS k = kassService.read(fileName);
			List<KnnGenterUnion> ps = handle(k, fileName);
			kpu.addAll(ps);
		}
		int maxPartnerUidSize = 0;
		int maxOffspringUidSize = 0;
		
		for(KnnGenterUnion kp : kpu) {
			maxPartnerUidSize = Math.max(maxPartnerUidSize, kp.partnerPids.size());
			maxOffspringUidSize = Math.max(maxOffspringUidSize, kp.offspringPids.size());
		}
		header.add("serialNumber");
		header.add("uid");
		header.add("isOther");
		String[][] result = new String[kpu.size()][3 + questionHeaders.size() + maxPartnerUidSize + maxOffspringUidSize + 2];
		
		for(int i = 0; i < kpu.size(); i++) {
			KnnGenterUnion kp = kpu.get(i);
			result[i][0] = kp.serialNumber;
			result[i][1] = kp.union.getId() + "";
			result[i][2] = kp.union.getIsOther() + "";
			//question
			for(int j = 0; j < questionHeaders.size(); j++) {
				String t = questionHeaders.get(j);
				if(kp.questionValues.get(t) == null) {
					result[i][3 + j] = QuestionConstants.getAnswerCode("DCR");
				} else {
					result[i][3 + j] = kp.questionValues.get(t);
				}
				if(i == 0)
					header.add(t);
			}
			int index = 3 + questionHeaders.size();
			//uid partner
			if(i == 0) {
				header.add("num_partners");
			}
			index = index + 1;
			int numP = 0;
			for(int j = 0; j < maxPartnerUidSize; j++) {
				if(i == 0)
					header.add("partner_" + (j+1));
				if(j < kp.partnerPids.size()) {
					result[i][index + j] = kp.partnerPids.get(j).toString();
					numP++;
				} else {
					result[i][index + j] = QuestionConstants.getAnswerCode("partner_union");
				}
 			}
			result[i][index - 1] = numP + "";
			
			index = 3 + questionHeaders.size() + 1 + maxPartnerUidSize;
			//uid offspring
			if(i == 0) {
				header.add("num_children");
			}
			int numCH = 0;
			index = index + 1;
			for(int j = 0; j < maxOffspringUidSize; j++) {
				if(i == 0)
					header.add("child_" + (j+1));
				if(j < kp.offspringPids.size()) {
					result[i][index + j] = kp.offspringPids.get(j).toString();
					numCH++;
				} else {
					result[i][index + j] = QuestionConstants.getAnswerCode("childhood_union");
				}
 			}
			result[i][index - 1] = numCH + "";
		}
		
		return result;
	}
	
	private List<KnnGenterUnion> handle(KASS k, String fileName) {
		List<KnnGenterUnion> kps = new ArrayList<KnnGenterUnion>();
		List<KUUnion> unions = k.getUnions().getUnions();
		if(unions != null && unions.size() > 0) {
			int i = 0;
			for(KUUnion union : unions) {
				int va = (int)((i * 1.0 / unions.size()) * 45);
				if(ExportProgressUtil.getInstance().getProgressBar() != null) {
					ExportProgressUtil.getInstance().getProgressBar().setValue(50 + va);
				}
				
				KnnGenterUnion kp = new KnnGenterUnion();
				kp.union = union;
				kp.serialNumber = k.getInterviewInfo().getSerialNumber();
				
				for(KQQuestionField field : union.getFields()) {
					String value = getFieldValue(union, field.getLabel());
					String t = getHeaderByOption(field.getLabel(), field.getQid());
					if(!questionHeaders.contains(t)) {
						addBySorting(field.getQid(), t);
						//questionHeaders.add(t);
					}
					kp.questionValues.put(t, value);
				}
				
				List<KUUMember> members = union.getMembers();
				for(KUUMember member : members) {
					if("partner".equals(member.getType())) {
						kp.partnerPids.add(member.getId());
					} else if("offspring".equals(member.getType())) {
						kp.offspringPids.add(member.getId());
					}
				}
				kps.add(kp);
				i++;
			}
			
		}
		return kps;
	}

	@Override
	public String getFileName() {
		return "munion";
	}
	
	class KnnGenterUnion {
		
		String serialNumber;
		KUUnion union;
		Map<String, String> questionValues = new HashMap<String, String>();
		List<Integer> partnerPids = new ArrayList<Integer>();
		List<Integer> offspringPids = new ArrayList<Integer>();
	}

	private String getFieldValue(KUUnion union, String label) {
		List<KQQuestionField> fields = union.getFields();
		for(KQQuestionField field : fields) {
			if(StringUtils.equals(field.getLabel(), label)) {
				return QuestionConstants.getAnswerCode(field.getValue());
			}
		}
		return null;
	}
	
	private String getHeaderByOption(String label, String qid) {
		if(option.getVarType() == 1) {
			return qid;
		} else if(option.getVarType() == 2) {
			return label;
		} else {
			return qid + "_" + label;
		}
	}
	
	private void addBySorting(String qid, String headerName) {
		int a = addQidBySorting(qid);
		if(a >= questionHeaders.size()) {
			this.questionHeaders.add(headerName);
		} else {
			this.questionHeaders.add(a, headerName);
		}
	}
	
	private int addQidBySorting(String qid) {
		if(questionIds.size() <= 0) {
			questionIds.add(qid);
			return 0;
		} else {
			for(int i = 0; i < questionIds.size(); i++) {
				String id = questionIds.get(i);
				if(qid.compareTo(id) < 0) {
					questionIds.add(i, qid);
					return i;
				}
			}
		}
		questionIds.add(qid);
		return questionIds.size();
	}

}
