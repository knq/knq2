package com.fangda.kass.service.davd;

import java.util.List;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.service.davd.support.KnnRela;

/**
 * Generating the Matrix Table 6 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImp6 extends KnnGener {

	@Override
	public String[][] genResult() {
		//String path = option.getIvFileNames().get(0);
		KASS kass = null;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			kass = kassService.read(option.getIvFileNames().get(0));
		} else {
			kass = kassService.get();
		}
		List<KPPerson> kps = kass.getPeople().getPersons();
		for (KPPerson person : kps) {
			columns.add(person.getPid() + "");
			header.add(person.getPid() + "");
		}
		String[][] result = new String[columns.size()][header.size()];
		for (int i = 0; i < kps.size(); i++) {
			KPPerson p1 = kps.get(i);
			for (int j = 0; j < kps.size(); j++) {
				KPPerson p2 = kps.get(j);
				List<KnnRela> list = getUnionBetween(kass, p1.getPid(), p2.getPid());
				if(list != null && list.size() > 0) {
					KnnRela rela = list.get(1);
					result[i][j] = rela.getCode();
				}
			}
		}
		return result;
	}

	@Override
	public String getFileName() {
		return "m6";
	}
	

}
