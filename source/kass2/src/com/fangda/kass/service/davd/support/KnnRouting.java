package com.fangda.kass.service.davd.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.service.interview.KassService;

/**
 * The Class for generating the routes of the kinship path of NKK for DAVD. The
 * abbreviation DAVD stands for ‘download and variable’, which generates three
 * different types of data matrices for analysis: A set of network matrices, the
 * person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnRouting {

	KassService kassService;
	List<KnnRoute> routes;
	int type = 0;//1, 0, 1-8
	Map<Integer, List<Integer>> nodeMap = new HashMap<Integer, List<Integer>>();
	Map<Integer, Node> idNodeMap = new HashMap<Integer, Node>();
	Node[] G;
	private KASS kass;

	public KnnRouting(String path, int t) {
		type = t;
		if (kassService == null) {
			kassService = new KassService();
		}
		kass = kassService.read(path);
		routes = new ArrayList<KnnRoute>();
		List<KUUnion> list = kass.getUnions().getUnions();
		for (KUUnion union : list) {
			if(union.getIsOther() == 1) {
				continue;
			}
			List<KUUMember> members = union.getMembers();
			int index = 0;
			for (int i = 0; i < members.size(); i++) {
				KUUMember m1 = members.get(i);
				for (int j = i + 1; j < members.size(); j++) {
					KUUMember m2 = members.get(j);
					KnnRoute route = new KnnRoute();
					route.fnode = m1.getId();
					route.tnode = m2.getId();
					route.index = index;
					route.length = 1;
					route.oneway = 2;
					route.uid = union.getId();
					routes.add(route);
					index++;

					KnnRoute route1 = new KnnRoute();
					route1.fnode = m2.getId();
					route1.tnode = m1.getId();
					route1.index = index;
					route1.length = 1;
					route1.oneway = 2;
					route1.uid = union.getId();
					routes.add(route1);
					index++;

					if (nodeMap.get(route.fnode) != null) {
						List<Integer> l = nodeMap.get(route.fnode);
						if (!l.contains(route.tnode)) {
							l.add(route.tnode);
						}
					} else {
						List<Integer> l = new ArrayList<Integer>();
						l.add(route.tnode);
						nodeMap.put(route.fnode, l);
					}
					if (nodeMap.get(route.tnode) != null) {
						List<Integer> l = nodeMap.get(route.tnode);
						if (!l.contains(route.fnode)) {
							l.add(route.fnode);
						}
					} else {
						List<Integer> l = new ArrayList<Integer>();
						l.add(route.fnode);
						nodeMap.put(route.tnode, l);
					}
				}
			}
		}
	}

	public void cal(int start) {
		
		Set<Integer> set = nodeMap.keySet();
		G = new Node[set.size()];
		Iterator<Integer> iter = set.iterator();
		int i = 0;
		while (iter.hasNext()) {
			Node node = new Node();
			node.adjvex = iter.next();
			node.isKnown = false;
			if (node.adjvex == start) {
				node.weight = 0;
			} else {
				node.weight = Integer.MAX_VALUE;
			}
			idNodeMap.put(node.adjvex, node);
			G[i] = node;
			i++;
		}
		while (true) {
			int k;
			boolean ok = true;
			for (k = 0; k < G.length; k++) {
				if (!G[k].isKnown) {
					ok = false;
					break;
				}
			}
			if (ok)
				return;

			int j;
			int minIndex = -1;
			for (j = 0; j < G.length; j++) {
				if (!G[j].isKnown) {
					if (minIndex == -1)
						minIndex = j;
					else if (G[minIndex].weight > G[j].weight)
						minIndex = j;
				}
			}
			G[minIndex].isKnown = true;
			Node node = G[minIndex];
			List<Integer> links = nodeMap.get(node.adjvex);
			//System.out.println(" 1 : " + node.adjvex);
			for (Integer z = 0; z < links.size(); z++) {
				Integer l1 = links.get(z);
				// Find the node of l1
				Node n = idNodeMap.get(l1);
				//if(n.isKnown) continue;
				if(node.adjvex == n.adjvex) {
					continue;
				}
				KnnRoute r = getKnnRoute(node.adjvex, n.adjvex);
				if (node.lines == null || node.lines.size() == 0) {
					KnnOneLine line = new KnnOneLine();
					line.getRoutes().add(r);
					n.lines.add(line);
					//System.out.println(" 1 new : " + n.adjvex);
				} else {
					for (int m = 0; m < node.lines.size(); m++) {
						
						KnnOneLine line = node.lines.get(m);
						
						List<KnnRoute> rs = new ArrayList<KnnRoute>();
						Collections.addAll(rs,  new KnnRoute[line.getRoutes().size()]);  
						Collections.copy(rs, line.getRoutes());  
						
						KnnOneLine line1 = new KnnOneLine();
						line1.setRoutes(rs);
						line1.getRoutes().add(r);
						n.lines.add(line1);
						//System.out.println(" 1 old : " + n.adjvex);
					}
					removeKnn(n.lines);
				}
				n.weight = 0;
			}
			
		}
		
	}
	
	public void addAgain() {
		Iterator<Entry<Integer, Node>> iter = idNodeMap.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<Integer, Node> entry = iter.next();
			Node node = entry.getValue();
			//Traversing the relation nodes.
			List<Integer> links = nodeMap.get(node.adjvex);
			//System.out.println(node.adjvex + ", " + links.size());
			for (Integer z = 0; z < links.size(); z++) {
				Integer l1 = links.get(z);
				// Find the node of l1
				Node n = idNodeMap.get(l1);
				KnnRoute r = getKnnRoute(node.adjvex, n.adjvex);
				//System.out.println(node.adjvex + "," + n.adjvex +" : " + node.lines.size());
				for (int m = 0; m < node.lines.size(); m++) {
					KnnOneLine line = node.lines.get(m);
					
					List<KnnRoute> rs = new ArrayList<KnnRoute>();
					for(KnnRoute kr : line.getRoutes()) {
						rs.add(kr);
					}
					KnnOneLine line1 = new KnnOneLine();
					line1.setRoutes(rs);
					line1.getRoutes().add(r);
					n.lines.add(line1);
					removeKnn(n.lines);
					//System.out.println(" 1 old : " + n.adjvex);
				}
			}
		}
	}

	public Map<Integer, Node> getResult() {

		//Add Again
		addAgain();
		//Deleting
		Iterator<Entry<Integer, Node>> it = idNodeMap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, Node> entry = it.next();
			Node node = entry.getValue();
			//System.out.println(node.lines.size());
			List<KnnOneLine> lines = node.lines;
			removeKnn(lines);
		}
		return idNodeMap;
	}
	
	public List<KnnOneLine> getSortingResult() {
		Map<Integer, Node> map = getResult();
		Iterator<Entry<Integer, Node>> it = map.entrySet().iterator();
		List<KnnOneLine> ones = new ArrayList<KnnOneLine>();
		while (it.hasNext()) {
			Entry<Integer, Node> entry = it.next();
			Node node = entry.getValue();
			if(node.adjvex != 0) {
				List<KnnOneLine> lines = node.lines;
				ones.addAll(lines);
			}
		}
		Collections.sort(ones, new MyCompare());
		return ones;
	}
	//for the matrix table 4
	public List<KnnOneLine> getSortingResult2() {
		List<KnnOneLine> ones = getSortingResult();
		List<KnnOneLine> removes = new ArrayList<KnnOneLine>();
		for(int i = 0; i < ones.size(); i++) {
			KnnOneLine one1 = ones.get(i);
			KnnRoute r1 = one1.getRoutes().get(one1.getRoutes().size() - 1);
			for(int j = i + 1; j < ones.size(); j++) {
				KnnOneLine one2 = ones.get(j);
				KnnRoute r2 = one2.getRoutes().get(one2.getRoutes().size() - 1);
				if(r1.tnode == r2.tnode) {
					if(one1.containOther(one2)) {
						removes.add(one2);
					}
				}
			}
		}
		ones.removeAll(removes);
		return ones;
	}
	
	public List<KnnOneLine> getSortingResult3() {
		List<KnnOneLine> ones = getSortingResult2();
		List<KnnOneLine> removes = new ArrayList<KnnOneLine>();
		for(int i = 0; i < ones.size(); i++) {
			KnnOneLine one1 = ones.get(i);
			KnnRoute r1 = one1.getRoutes().get(one1.getRoutes().size() - 1);
			for(int j = i + 1; j < ones.size(); j++) {
				KnnOneLine one2 = ones.get(j);
				KnnRoute r2 = one2.getRoutes().get(one2.getRoutes().size() - 1);
				if(r1.tnode == r2.tnode) {
					removes.add(one2);
				}
			}
		}
		ones.removeAll(removes);
		return ones;
	}
	
	
	/**
	 * Deleting the same data
	 */
	private void removeKnn(List<KnnOneLine> lines) {
		//Containing the same node
		List<KnnOneLine> removes = new ArrayList<KnnOneLine>();
		for (int i = 0; i < lines.size(); i++) {
			KnnOneLine line = lines.get(i);
			if(line.hasSameUnion()) {
				removes.add(line);
			}
		}
		lines.removeAll(removes);
		
		//Deleting the repeated data
		removes.clear();
		for (int i = 0; i < lines.size(); i++) {
			KnnOneLine line = lines.get(i);
			for (int j = i + 1; j < lines.size(); j++) {
				KnnOneLine line1 = lines.get(j);
				if(line1.equalOther(line)) {
					removes.add(line1);
				}
			}
		}
		lines.removeAll(removes);
		
	}

	KnnRoute getKnnRoute(int start, int end) {
		for (KnnRoute route : routes) {
			if (start == route.fnode && end == route.tnode) {
				return route;
			}
		}
		return null;
	}
}
