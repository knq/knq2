package com.fangda.kass.service.davd.support;

/**
 * The Class for Coding the relationship in the data matrices for DAVD. The
 * abbreviation DAVD stands for ‘download and variable’, which generates three
 * different types of data matrices for analysis: A set of network matrices, the
 * person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnRela {

	public KnnRela() {

	}

	public KnnRela(String code) {
		this.code = code;
	}

	private String code;

	private int num;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNum() {
		if("M".equals(code)) {
			num = 1;
		} else if("F".equals(code)) {
			num = 2;
		} else if("Z".equals(code)) {
			num = 3;
		} else if("B".equals(code)) {
			num = 4;
		} else if("W".equals(code)) {
			num = 5;
		} else if("H".equals(code)) {
			num = 6;
		} else if("D".equals(code)) {
			num = 7;
		} else if("S".equals(code)) {
			num = 8;
		}
		return num;
	}

}
