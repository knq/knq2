package com.fangda.kass.service.davd.support;

import java.util.Comparator;

/**
 * The Class for comparing the routes of the kinship path of NKK for DAVD. The
 * abbreviation DAVD stands for ‘download and variable’, which generates three
 * different types of data matrices for analysis: A set of network matrices, the
 * person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class MyCompare implements Comparator<KnnOneLine> {

	@Override
	public int compare(KnnOneLine o1, KnnOneLine o2) {
		KnnRoute r1 = o1.getRoutes().get(o1.getRoutes().size() - 1);
		KnnRoute r2 = o2.getRoutes().get(o2.getRoutes().size() - 1);
		if(r1.tnode > r2.tnode) {
			return 4;
		} else {
			if(r1.tnode == r2.tnode) {
				if(o1.getRoutes().size() > o2.getRoutes().size()) {
					return -2;
				} else {
					return -1;
				}
			} else {
				return 0;
			}
		}
	}

}
