package com.fangda.kass.service.davd;

import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.service.davd.support.KnnOneLine;
import com.fangda.kass.service.davd.support.KnnRela;
import com.fangda.kass.service.davd.support.KnnRoute;
import com.fangda.kass.service.davd.support.KnnRouting;
import com.fangda.kass.util.Constants;

/**
 * Generating the Matrix Table 7 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImp7 extends KnnGener {

	@Override
	public String[][] genResult() {
		//String path = option.getIvFileNames().get(0);
		KASS kass = null;
		String path = Constants.KASS_XML;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			path = option.getIvFileNames().get(0);
			kass = kassService.read(path);
		} else {
			kass = kassService.get();
		}
		KnnRouting r = new KnnRouting(path, 0);
		r.cal(0);
		List<KnnOneLine> ones = r.getSortingResult3();
		int max = 0;
		for(KnnOneLine one : ones) {
			max = Math.max(one.getRoutes().size(), max);
		}
		for(int i = 0; i < max; i++) {
			header.add("" + (i+1));
		}
		for(int i = 0; i < max; i++) {
			header.add("" + (i+1));
		}
		String[][] result = new String[ones.size()][max * 2];
		//循环列表
		for(int i = 0; i < ones.size(); i++) {
			KnnOneLine one = ones.get(i);
			KnnRoute kr = one.getRoutes().get(one.getRoutes().size() - 1);
			columns.add(kr.tnode + "");
			
			List<String> a = new ArrayList<String>();
			List<String> b = new ArrayList<String>();
			for(int j = 0; j < one.getRoutes().size(); j++) {
				KnnRoute r1 = one.getRoutes().get(j);
				List<KnnRela> list = getUnionBetween(kass, r1.fnode, r1.tnode);
				KnnRela rela = list.get(1);
				a.add(rela.getCode());
				b.add(rela.getNum() + "");
			}
			for(int z = 0; z < max; z ++) {
				if(z < a.size()) {
					result[i][z] = a.get(z);
				}
				if(z < b.size()) {
					result[i][z + max] = b.get(z);
				}
			}
		}
		
		return result;
	}

	@Override
	public String getFileName() {
		return "m7";
	}

}
