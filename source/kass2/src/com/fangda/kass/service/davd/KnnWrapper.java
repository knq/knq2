package com.fangda.kass.service.davd;

import java.util.List;

import com.fangda.kass.service.davd.support.GenterOption;

/**
 * The extension of class for DAVD
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnWrapper {

	private GenterOption option;

	/**
	 * The column heading of the matrix table
	 */
	protected List<String> header;

	/**
	 * The row heading of the matrix table
	 */
	protected List<String> columns;

	private KnnGener gener;

	public KnnWrapper() {

	}

	public KnnWrapper(GenterOption option) {
		this.option = option;
	}

	public String[][] getResult() {
		if (option.getGenerType() == 1) {
			gener = new KnnGenerImp1();
			gener.setOption(option);
		} else if (option.getGenerType() == 2) {
			gener = new KnnGenerImp2();
			gener.setOption(option);
		} else if (option.getGenerType() == 3) {
			gener = new KnnGenerImp3();
			gener.setOption(option);
		} else if (option.getGenerType() == 4) {
			gener = new KnnGenerImp4();
			gener.setOption(option);
		} else if (option.getGenerType() == 5) {
			gener = new KnnGenerImp5();
			gener.setOption(option);
		} else if (option.getGenerType() == 6) {
			gener = new KnnGenerImp6();
			gener.setOption(option);
		} else if (option.getGenerType() == 7) {
			gener = new KnnGenerImp7();
			gener.setOption(option);
		} else if (option.getGenerType() == 8) {
			gener = new KnnGenerImp8();
			gener.setOption(option);
		} else if (option.getGenerType() == 10) {
			gener = new KnnGenerImp10();
			gener.setOption(option);
		} else if (option.getGenerType() == 21) {
			gener = new knnGenerImpPerson();
			gener.setOption(option);
		} else if (option.getGenerType() == 22) {
			gener = new KnnGenerImpUnion();
			gener.setOption(option);
		}
		System.out.println(gener.getClass().getSimpleName());
		this.header = gener.getHeader();
		this.columns = gener.getColumns();
		return gener.getResult();
	}

	public GenterOption getOption() {
		return option;
	}

	public void setOption(GenterOption option) {
		this.option = option;
	}

	public List<String> getHeader() {
		return header;
	}

	public List<String> getColumns() {
		return columns;
	}

	/**
	 * Add the XML path
	 * 
	 * @param path
	 */
	public void addXmlPath(String path) {
		if (option == null) {
			option = new GenterOption();
		}
		option.getIvFileNames().add(path);
	}

	/**
	 * Setting the type of 1- 10
	 * 
	 * @param type
	 */
	public void setGenType(int type) {
		if (option == null) {
			option = new GenterOption();
		}
		option.setGenerType(type);
	}

	/**
	 * Add the file type of the output file
	 */
	public void addExportExt(String ext) {
		if (option == null) {
			option = new GenterOption();
		}
		option.getExts().add(ext);
	}

	/**
	 * Setting the destination directory of output files
	 * 
	 * @param path
	 */
	public void setOutPath(String path) {
		if (option == null) {
			option = new GenterOption();
		}
		option.setOutFileName(path);
	}

	public void print() {
		this.getResult();
		gener.print();
	}
}
