package com.fangda.kass.service.davd;

import java.util.List;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.service.davd.support.KnnOneLine;
import com.fangda.kass.service.davd.support.KnnRoute;
import com.fangda.kass.service.davd.support.KnnRouting;
import com.fangda.kass.util.Constants;

/**
 * Generating the Matrix Table 8 of Network matrices which contain information
 * about the general structure of the informant's network. It consists of PID
 * and UID.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnGenerImp8 extends KnnGener {

	@Override
	public String[][] genResult() {
		//String path = option.getIvFileNames().get(0);
		KASS kass = null;
		String path = Constants.KASS_XML;
		if(option != null && option.getIvFileNames() != null && option.getIvFileNames().size() > 0) {
			path = option.getIvFileNames().get(0);
			kass = kassService.read(path);
		} else {
			kass = kassService.get();
		}
		KnnRouting r = new KnnRouting(path, 0);
		r.cal(0);
		List<KnnOneLine> mat5 = r.getSortingResult3();
		List<KnnOneLine> mat4 = r.getSortingResult2();
		List<KnnOneLine> mat3 = r.getSortingResult();
		
		for(int i = 0; i < mat5.size(); i++) {
			KnnOneLine one = mat5.get(i);
			KnnRoute kr = one.getRoutes().get(one.getRoutes().size() - 1);
			columns.add(kr.tnode + "");
		}
		header.add("Kinship distance = number of person-to-person steps in shortest path");
		header.add("Number of simple paths");
		header.add("Number of independent simple paths");
		
		String[][] result = new String[columns.size()][header.size()];
		for(int i = 0; i < mat5.size(); i++) {
			KnnOneLine one = mat5.get(i);
			KnnRoute kr = one.getRoutes().get(one.getRoutes().size() - 1);
			
			result[i][0] = one.getRoutes().size() + "";
			int a = getNumByEnd(mat3, kr.tnode);
			int b = getNumByEnd(mat4, kr.tnode);
			result[i][1] = a + "";
			result[i][2] = b + "";
			
		}
		return result;
	}
	
	private int getNumByEnd(List<KnnOneLine> ones, int end) {
		int size = 0;
		for(int i = 0; i < ones.size(); i++) {
			KnnOneLine one = ones.get(i);
			KnnRoute kr = one.getRoutes().get(one.getRoutes().size() - 1);
			if(kr.tnode == end) {
				size++;
			}
		}
		return size;
	}

	@Override
	public String getFileName() {
		return "m8";
	}

}
