package com.fangda.kass.service.davd.support;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class for the routes for DAVD. The abbreviation DAVD stands for ‘download
 * and variable’, which generates three different types of data matrices for
 * analysis: A set of network matrices, the person matrix and the union matrix.
 * 
 * @author Fangfang Zhao
 * 
 */
public class KnnOneLine {

	private List<KnnRoute> routes = new ArrayList<KnnRoute>();

	public List<KnnRoute> getRoutes() {
		return routes;
	}

	public void setRoutes(List<KnnRoute> routes) {
		this.routes = routes;
	}
	
	public boolean hasSameUnion() {
		boolean flag = false;
		for(int i = 0; i < routes.size(); i++) {
			KnnRoute k1 = routes.get(i);
			for(int j = i + 1; j < routes.size(); j++) {
				KnnRoute k2 = routes.get(j);
				if(k1.uid == k2.uid) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * Checking for equaling to other
	 * @param route
	 * @return
	 */
	public boolean equalOther(KnnOneLine oneLine) {
		if(this.routes.size() == oneLine.routes.size()) {
			boolean flag = true;
			for(int i = 0; i < this.routes.size(); i++) {
				KnnRoute r1 = this.routes.get(i);
				KnnRoute r2 = oneLine.routes.get(i);
				if(r1.fnode == r2.fnode && r1.tnode == r2.tnode) {
				} else {
					flag = false;
					break;
				}
			}
			return flag;
		}
		return false;
	}
	
	/**
	 * Checking for containing other
	 * @param oneLine
	 * @return
	 */
	public boolean containOther(KnnOneLine oneLine) {
		boolean flag = false;
		for(int i = 0; i < this.routes.size(); i++) {
			KnnRoute r1 = routes.get(i);
			for(int j = 0; j < oneLine.routes.size(); j++) {
				KnnRoute r2 = oneLine.routes.get(j);
				if(r1.uid == r2.uid && r1.tnode == r2.tnode) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(KnnRoute route : routes) {
			sb.append(route.fnode).append("->").append(route.tnode);
			sb.append("   ");
		}
		return sb.toString();
	}
	
}
