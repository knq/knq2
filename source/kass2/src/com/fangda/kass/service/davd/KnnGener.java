package com.fangda.kass.service.davd;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.service.davd.support.GenterOption;
import com.fangda.kass.service.davd.support.KnnRela;
import com.fangda.kass.service.interview.KassService;

/**
 * Generating the superclass
 * 
 * @author Fangfang Zhao
 * 
 */
public abstract class KnnGener {

	protected KassService kassService = new KassService();

	/**
	 * The column heading of the matrix table
	 */
	protected List<String> header = new ArrayList<String>();

	/**
	 * The row heading of the matrix table
	 */
	protected List<String> columns = new ArrayList<String>();

	protected GenterOption option;

	/**
	 * Get the result of the matrix table
	 * 
	 * @return
	 */
	public String[][] getResult() {
		// if(option == null) {
		// throw new RuntimeException("option must init");
		// }
		return genResult();
	}

	public abstract String[][] genResult();

	/**
	 * Get the filename without the filename suffixes，
	 * 
	 * @return
	 */
	public abstract String getFileName();

	/**
	 * Setting the value of parameter setting of DAVD ( download and variable ).
	 * 
	 * @param option
	 */
	public void setOption(GenterOption option) {
		this.option = option;
	}

	public void print() {
		String[][] result = getResult();
		System.out.print(formatString(""));
		for (String h : header) {
			System.out.print(formatString(h));
		}
		System.out.println("");
		for (int i = 0; i < columns.size(); i++) {
			String c = columns.get(i);
			System.out.print(formatString(c));

			int length = result[i].length;
			for (int j = 0; j < length; j++) {
				System.out.print(formatString(result[i][j]));
			}
			System.out.println("");
		}
	}

	private String formatString(String s) {
		if (s == null)
			return "   ";
		if (s.length() == 0) {
			return "   ";
		} else if (s.length() == 1) {
			return "  " + s;
		} else if (s.length() == 2) {
			return " " + s;
		} else if (s.length() == 3) {
			return s;
		}
		return "";
	}

	public List<String> getHeader() {
		return header;
	}

	public List<String> getColumns() {
		return columns;
	}

	protected List<KnnRela> getUnionBetween(KASS kass, int p1, int p2) {
		List<KnnRela> list = new ArrayList<KnnRela>();
		if (p1 == p2)
			return list;
		List<KUUnion> unions = kass.getUnions().getUnions();

		String type1 = "";
		String type2 = "";
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == p1) {
					type1 = member.getType();
				}
				if (member.getId() == p2) {
					type2 = member.getType();
				}
			}
			if (StringUtils.isNotBlank(type1) && StringUtils.isNotBlank(type2)) {
				break;
			}
			type1 = "";
			type2 = "";
		}

		if (StringUtils.isNotBlank(type1) && StringUtils.isNotBlank(type2)) {
			KPPerson person1 = this.getPerson(kass, p1);
			KPPerson person2 = this.getPerson(kass, p2);
			if ("partner".equals(type1) && "partner".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("H"));
					list.add(new KnnRela("W"));
				} else {
					list.add(new KnnRela("W"));
					list.add(new KnnRela("H"));
				}
			} else if ("partner".equals(type1) && "offspring".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("F"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("S"));
					} else {
						list.add(new KnnRela("D"));
					}
				} else {
					list.add(new KnnRela("M"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("S"));
					} else {
						list.add(new KnnRela("D"));
					}
				}
			} else if ("offspring".equals(type1) && "partner".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("S"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("F"));
					} else {
						list.add(new KnnRela("M"));
					}
				} else {
					list.add(new KnnRela("D"));
					if (person2.getSex() == 0) {
						list.add(new KnnRela("F"));
					} else {
						list.add(new KnnRela("M"));
					}
				}
			} else if ("offspring".equals(type1) && "offspring".equals(type2)) {
				if (person1.getSex() == 0) {
					list.add(new KnnRela("B"));
				} else {
					list.add(new KnnRela("Z"));
				}
				if (person2.getSex() == 0) {
					list.add(new KnnRela("B"));
				} else {
					list.add(new KnnRela("Z"));
				}
			}
		}
		return list;
	}

	public KPPerson getPerson(KASS kass, int pid) {
		if (kass.getPeople() == null) {
			return null;
		}
		List<KPPerson> persons = kass.getPeople().getPersons();
		if (persons == null || persons.size() <= 0) {
			return null;
		}
		for (KPPerson person : persons) {
			if (person.getPid() == pid) {
				return person;
			}
		}
		return null;
	}
}
