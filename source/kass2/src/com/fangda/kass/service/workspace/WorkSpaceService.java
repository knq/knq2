package com.fangda.kass.service.workspace;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import com.fangda.kass.service.BaseService;
import com.fangda.kass.util.Constants;

/**
 * The Class for the project and the directory path of the project .
 * 
 * @author Fangfang Zhao
 * 
 */
public class WorkSpaceService extends BaseService {

	/**
	 * Recent projects
	 * 
	 * @return
	 */
	public String getRecentProjectPath() {
		Preferences pref = Preferences.userRoot().node("project");
		String recentProjectPath = pref.get(String.valueOf(1), "");
		System.out.println("^^^" + recentProjectPath);
		return recentProjectPath;
	}

	/**
	 * Recent questionnaires
	 * 
	 * @return
	 */
	public String getRecentQuePath() {

		return null;
	}

	/**
	 * Recent interviews
	 * 
	 * @return
	 */
	public String getRecentInterviewPath() {
		Preferences pref = Preferences.userRoot().node("interview");
		String recentInterviewPath = pref.get(String.valueOf(1), "");
		System.out.println("^^^" + recentInterviewPath);
		return recentInterviewPath;
	}

	/**
	 * The list of recent projects
	 * 
	 * @return
	 */
	public static List<String> getRecentProjectPathList() {

		ArrayList<String> list = new ArrayList<String>();
		Preferences pref = Preferences.userRoot().node("project");
		try {
			for (int j = 0; j < pref.keys().length + 1; j++) {
				if (pref.get(String.valueOf(j), "") != "") {
					list.add(pref.get(String.valueOf(j), ""));
					/* System.out.println("!!!"+pref.get(String.valueOf(j),"")); */
				} else {
				}

			}
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// list.add(new
		// String[]{"E:\\work\\kass\\project.xml","C:\\Users\\richard\\Desktop\\project.xml"});

		return list;
	}

	/**
	 * The list of recent interviews
	 * 
	 * @return
	 */
	public static List<String> getRecentInterviewPathList() {
		ArrayList<String> list = new ArrayList<String>();
		Preferences pref = Preferences.userRoot().node("interview");
		try {
			for (int j = 0; j < pref.keys().length + 1; j++) {
				if (pref.get(String.valueOf(j), "") != "") {
					list.add(pref.get(String.valueOf(j), ""));
					/* System.out.println("!!!"+pref.get(String.valueOf(j),"")); */
				} else {
				}

			}
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// list.add(new
		// String[]{"E:\\work\\kass\\project.xml","C:\\Users\\richard\\Desktop\\project.xml"});

		return list;
	}

	public String saveRecentProjectPath() {

		return "";
	}

	public static String SetProjectDefaultDir() {
		String defaultPath = "";
		WorkSpaceService spaceService = new WorkSpaceService();
		String dir = spaceService.getRecentProjectPath();
		if (dir == null) {
			defaultPath = Constants.getUserPath() + File.separator + "data";
			System.out.println("@@@" + defaultPath);
			File foder = new File(defaultPath);
			if (foder.exists() == false) {
				foder.mkdirs();
			}
		} else {
			defaultPath = dir;
			System.out.println("###" + dir);
		}
		return defaultPath;
	}

	public static String SetSystemDefaultDir() {
		String defaultPath = Constants.getUserPath() + File.separator
				+ "language";
		File foder = new File(defaultPath);
		if (foder.exists() == false) {
			foder.mkdirs();
		}

		return defaultPath;
	}

	public static String SetQueDefaultDir() {
		String defaultPath = "";
		WorkSpaceService spaceService = new WorkSpaceService();
		String dir = spaceService.getRecentQuePath();
		if (dir == null) {
			String defaultTempPath = Constants.getUserPath() + File.separator
					+ "Questionnaire";
			// For the installation package(it is invalid now): defaultPath =
			// new File(defaultTempPath).getParent()+"\\Questionnaire";
			File foder = new File(defaultTempPath);
			if (foder.exists() == false) {
				foder.mkdirs();
			}
		} else {
			defaultPath = dir;
			System.out.println("###" + dir);
		}
		return defaultPath;
	}

	public static String SetInterviewDefaultDir() {
		String defaultPath = "";
		WorkSpaceService spaceService = new WorkSpaceService();
		String dir = spaceService.getRecentInterviewPath();
		if (dir == null) {
			String defaultTempPath = Constants.getUserPath() + File.separator
					+ "data";
			defaultPath = defaultTempPath;
			File foder = new File(defaultPath);
			if (foder.exists() == false) {
				foder.mkdirs();
			}
		} else {
			defaultPath = dir;
			System.out.println("###" + dir);
		}
		return defaultPath;
	}
}
