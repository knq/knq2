package com.fangda.kass.service;

import java.io.File;

import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.mxgraph.util.mxResources;

/**
 * The Class for the XML file.
 * 
 * @author Fangfang Zhao
 * 
 */
public class XmlHelper {

	private static XmlHelper xmlHelper;

	public static XmlHelper getInstance() {
		if (xmlHelper != null) {
			return xmlHelper;
		} else {
			return new XmlHelper();
		}
	}
	
	public String openRealXmlFile(String path) {
		String tmpfile = "";
		if(path.endsWith(Constants.TMP_EXT)) {
			tmpfile = path;
		} else {
			tmpfile = path.replace(Constants.PROPERTIES_EXT, Constants.TMP_EXT);
			FileUtil.copySingleFile(path, tmpfile);
		}
		return tmpfile;
	}
	
	/**
	 * Save the file of questionnaire
	 * @param p
	 */
	public void saveRealFile(Questionnaire p) {
		this.saveFile(p, Constants.QUESTIONNAIRE_XML);
		FileUtil.copySingleFile(Constants.QUESTIONNAIRE_XML, Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT));
	}
	
	/**
	 * Delete the temporary file of questionnaire
	 */
	public void deleteTmpFile() {
		if(Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
			File f = new File(Constants.QUESTIONNAIRE_XML);
			if(f.exists()) {
				f.delete();
			}
		}
	}

	public Questionnaire readFile() {
		validXml();
		Questionnaire q = null;
		try {
			JAXBContext cxt = JAXBContext.newInstance(Questionnaire.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			//unmarshaller.setProperty(Unmarshaller, value);
			//unmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);// 是否格式化生成的xml串
			q = (Questionnaire) unmarshaller.unmarshal(new File(
					Constants.QUESTIONNAIRE_XML));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" read xml file error ");
		}
		return q;
	}

	public Questionnaire readFileProperty(String rpath) {
		//validXml();
		Questionnaire q = null;
		try {
			JAXBContext cxt = JAXBContext.newInstance(Questionnaire.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			q = (Questionnaire) unmarshaller.unmarshal(new File(rpath));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" read xml file error ");
		}
		return q;
	}
	
	public void saveFile(Questionnaire q) {
		validXml();
		try {
			JAXBContext context = JAXBContext.newInstance(Questionnaire.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
			marshaller.marshal(q, new File(Constants.QUESTIONNAIRE_XML));
		} catch (JAXBException e) {
			throw new RuntimeException(" read xml file error ");
		}
	}
	
	public void saveFile(Questionnaire q, String filePath) {
		validXml();
		try {
			JAXBContext context = JAXBContext.newInstance(Questionnaire.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
			marshaller.marshal(q, new File(filePath));
		} catch (JAXBException e) {
			throw new RuntimeException(" read xml file error ");
		}
	}

	public void validXml() {
		if (Constants.QUESTIONNAIRE_XML == null
				|| "".equals(Constants.QUESTIONNAIRE_XML)) {
			throw new RuntimeException(" not set xml file");
		}
	}
	
	public KASS readFilePropertyInterview(String rpath) {
		//validXml();
		KASS k = null;
		try {
			JAXBContext cxt = JAXBContext.newInstance(KASS.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			k = (KASS) unmarshaller.unmarshal(new File(rpath));
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(" read xml file error ");
		}
		return k;
	}
	

	public void saveFileKASS(KASS q, String filePath) {
		//validXml();
		try {
			JAXBContext context = JAXBContext.newInstance(KASS.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
			marshaller.marshal(q, new File(filePath));
			
		} catch (JAXBException e) {
			throw new RuntimeException(e+" save xml file error ");
		}
	}
	
	
	public void alertExitQn(KnqMain km) {
	}
	
	public void alertExitQn(KnqMain km, String type) {
		String alertQn = mxResources.get("confirmSaveQn");
		int n = JOptionPane.showInternalConfirmDialog(km.frame.getContentPane(), alertQn,mxResources.get("KNQ2"),
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);

		switch(n) {
         case 0:
        	 if("local".equals(type)) {
        		 if(Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
        			 String path = Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT);
        			 Constants.QUESTIONNAIRE.setStatus("Formal");
        			 saveFile(Constants.QUESTIONNAIRE, path);
        			 deleteTmpFile();
        			 Constants.QUESTIONNAIRE_XML = path;
        		 }
        	 } else if("revise".equals(type)) {
        		 if(Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
        			 String path = Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT);
        			 Constants.QUESTIONNAIRE.setStatus("Formal");
        			 saveFile(Constants.QUESTIONNAIRE, path);
        			 deleteTmpFile();
        			 Constants.QUESTIONNAIRE_XML = path;
        		 }
        	 } else if("temprevise".equals(type)) {
        		 if(Constants.QUESTIONNAIRE_TEMPLATE_XML.contains(Constants.TMP_EXT)) {
        			 String path = Constants.QUESTIONNAIRE_TEMPLATE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT);
        			 Constants.QUESTIONNAIRE.setStatus("Formal");
        			 saveFile(Constants.QUESTIONNAIRE, path);
        			 deleteTmpFile();
        			 File f = new File(Constants.QUESTIONNAIRE_TEMPLATE_XML);
        			 if(f.exists()) {
        				 f.delete();
        			 }
        			 Constants.QUESTIONNAIRE_TEMPLATE_XML = path;
        		 }
        	 }
        	 Constants.isQuestionnaireCancel=0;
             break;
         case 1:
        	 if("local".equals(type) || "revise".equals(type)) {
        		 if(Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
        			 String path = Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT);
        			 Constants.QUESTIONNAIRE_XML = path;
        			 Questionnaire q = XmlHelper.getInstance().readFile();
        			 Constants.QUESTIONNAIRE = q;
        			 deleteTmpFile();
        		 }
        	 } else if("temprevise".equals(type)) {
        		 if(Constants.QUESTIONNAIRE_TEMPLATE_XML.contains(Constants.TMP_EXT)) {
        			 String path = Constants.QUESTIONNAIRE_TEMPLATE_XML.replace(Constants.TMP_EXT, Constants.PROPERTIES_EXT);
        			 Constants.QUESTIONNAIRE_TEMPLATE_XML = path;
        			 Questionnaire q = XmlHelper.getInstance().readFile();
        			 Constants.QUESTIONNAIRE = q;
        			 File f = new File(Constants.QUESTIONNAIRE_TEMPLATE_XML);
        			 if(f.exists()) {
        				 f.delete();
        			 }
        		 }
        	 }
        	 Constants.isQuestionnaireCancel=0;
             break;
         case 2:
        	 Constants.isQuestionnaireCancel=1;
        	 return;
		}
	}
}
