package com.fangda.kass.ui.interview.graph.action;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.CustomGraph;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.mxgraph.model.mxCell;

/**
 * The Class for the delete of the graph.
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomDeleteAction extends CustomBaseAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2473344347612727339L;

	@Override
	public void action(ActionEvent e) {
		CustomGraph graph = (CustomGraph)getGraph(e);
		if(graph.graphPanel.getDisable() > GraphPanel.DISABLE_NEW) {
			return;
		}
		
		Object[] el = { "Yes", "No" };
		int result = JOptionPane.showOptionDialog(null, "Are you sure you want to remove this cell for graph?",
				"Select Option", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, el, el[1]);
		if (result == JOptionPane.YES_OPTION) {
			if(graph != null && graph.getSelectionCells() != null) {
				Object[] objs = graph.getSelectionCells();
				for(Object o : objs) {
					if(o instanceof mxCell) {
						mxCell cell = (mxCell)o;
						if(cell.getValue() instanceof GraphPerson) {
							GraphPerson graphPerson = (GraphPerson)cell.getValue();
							if(graphPerson.getId() == 0) {
								return;
							}
							this.getService().deletePersonById(graphPerson.getId());
						} else if(cell.getValue() instanceof GraphUnion) {
							GraphUnion graphUnion = (GraphUnion)cell.getValue();
							this.getService().deleteUnionById(graphUnion.getId());
						}
						graph.removeCells(new mxCell[]{cell});
					}
				}
			}
		} else if (result == JOptionPane.NO_OPTION) {

		}
	}

}
