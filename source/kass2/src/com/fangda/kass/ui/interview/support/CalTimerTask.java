package com.fangda.kass.ui.interview.support;

import java.util.TimerTask;

import javax.swing.JLabel;

/**
 * This class for the TimerTask used for calculating the duration time of an
 * interview in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class CalTimerTask extends TimerTask {

	private JLabel timerLabel;

	public CalTimerTask(JLabel timerLabel) {
		this.timerLabel = timerLabel;
	}

	@Override
	public void run() {
		if (CalTimer.getInstance().isRuning()) {
			String a = CalTimer.getInstance().addOne();
			timerLabel.setText(a);
		}
	}

}