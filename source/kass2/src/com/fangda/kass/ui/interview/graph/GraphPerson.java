package com.fangda.kass.ui.interview.graph;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.service.interview.KassService;

/**
 * This class for the person basic information of a person icon, such as name,
 * sex, and so on.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class GraphPerson extends GraphElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5974255916813980232L;
	/**
	 * Name
	 */
	private String name;
	/**
	 * Sex : 0 = male , 1 = female
	 */
	private int sex;
	/**
	 * The Indicator for the other important person. 1 = other important, 0 =
	 * relatives
	 */
	private int isOther;
	/**
	 * The Indicator for death and alive: 1 = death, 2 = alive
	 */
	private int isDeath = 2;
	/**
	 * The birthday
	 */
	private String birthDay;
	/**
	 * The Indicator for the mark status of a person in a question answer
	 */
	private String remark;
	/**
	 * The tip of the person icon when the mouse is over it
	 */
	private String tip;

	private KPPerson kpperson;

	private KassService kassService;

	public GraphPerson() {

	}

	public GraphPerson(int id, String name, int sex) {
		this.setId(id);
		this.name = name;
		this.sex = sex;
	}

	public GraphPerson(int id, String name, int sex, int isDeath) {
		this.setId(id);
		this.name = name;
		this.sex = sex;
		this.isDeath = isDeath;
	}

	private void initService() {
		if (kassService == null) {
			kassService = new KassService();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getIsOther() {
		return isOther;
	}

	public void setIsOther(int isOther) {
		this.isOther = isOther;
	}

	public int getIsDeath() {
		return isDeath;
	}

	public void setIsDeath(int isDeath) {
		this.isDeath = isDeath;
	}

	@Override
	public String toString() {
		String n = "";
		if (this.getId() == 0) {
			n = "E\n";
		} else {
			n = "" + this.getId() + "\n";
		}
		if (StringUtils.isNotBlank(this.getName())) {
			if (this.getName().length() > 10) {
				n = n + this.getName().replace(" ", "\n");
			} else {
				n = n + this.getName();
			}
		}
		if (StringUtils.isNotBlank(this.getBirthDay())) {
			initService();
			int showBirthday = kassService.get().getPageConfig()
					.getShowBirthday();
			if (showBirthday == 1) {
				n = n + "\n" + this.getBirthDay();
			}
		}
		if (StringUtils.isNotBlank(this.getRemark())) {
			n = n + "\n" + this.getRemark();
		}
		return n;
	}

	public KPPerson getKpperson() {
		return kpperson;
	}

	public void setKpperson(KPPerson kpperson) {
		this.kpperson = kpperson;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
}
