package com.fangda.kass.ui.interview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KInterviewInfo;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KPeople;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.CompleteProcedure;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.PqdQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.Toast;
import com.fangda.kass.ui.common.Toast.Style;
import com.fangda.kass.ui.interview.graph.GraphElement;
import com.fangda.kass.ui.interview.graph.GraphEventListener;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.ui.interview.project.AutoCompletePanel;
import com.fangda.kass.ui.interview.project.QuestionPanel;
import com.fangda.kass.ui.interview.project.SearchPanel;
import com.fangda.kass.ui.interview.project.TabQuestionPanel;
import com.fangda.kass.ui.interview.support.CalTimer;
import com.fangda.kass.ui.interview.support.CalTimerTask;
import com.fangda.kass.ui.interview.support.QuestionMap;
import com.fangda.kass.ui.questionnaire.support.QnTreeNode;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.RegularUtil;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the UI of the Interview in the IMS of KNQ2
 * 
 * @author Fangfang Zhao, Shuang Ma, Jianguo Zhou, Jing Kong
 * 
 */
public class IvMainPanel extends JPanel {

	private static final long serialVersionUID = -4601740824088314699L;
	public KnqMain kmain;
	public static IvMainPanel IVPANEL;
	JPanel statusBar;
	JLabel statusLabel;
	JLabel sampleNumberLabel;
	JToolBar topToolBar;
	public JTabbedPane leftOuterPanel;
	public JTree treePanel;// left menu
	JSplitPane rightOuterPanel;
	JPanel rightGraphPanel;// border panel
	protected ProjectService projectService;
	public KassService kassService;
	public QuestionnaireService questionnaireService;
	boolean modified = false;
	public JScrollPane leftQuestionPanel;
	public JScrollPane searchPanel;
	public GraphPanel graphPanel;
	public GraphPanel ographPanel;
	public boolean sectionType1238;
	public QuestionPanel jqustionpanel;
	public CalTimerTask calTimerTask;
	public JLabel timerLabel;
	public Timer t;
	JLabel locationLable;
	public static JButton startBtn;
	public JPanel percentPanel;
	public JToolBar statusToolBar;
	public int opGraphPanel = 1;// 1=graphPanel(NKK Window)
								// 2=ographPanel(Other Important Window)

	public IvMainPanel(KnqMain kmain) {
		Constants.PANELFLAG = "IV";
		this.kmain = kmain;
		projectService = new ProjectService();
		questionnaireService = new QuestionnaireService();

		setLayout(new BorderLayout());

		statusBar = createStatusBar();
		createCenterPanel();
		add(statusBar, BorderLayout.SOUTH);
		createToolBar();

		KASS kass = new KASS();
		kassService = new KassService();
		kass = kassService.get();

		initGraph(kass);
		KInterviewInfo interviewInfo = new KInterviewInfo();
		interviewInfo = kass.getInterviewInfo();
		String interviewer = interviewInfo.getInterviewer();
		String interviewee = interviewInfo.getInterviewee();
		if (interviewInfo.getSerialNumber() != null
				&& interviewInfo.getInterviewer() != null
				& interviewInfo.getInterviewee() != null) {
			kmain.setTitle("|" + interviewInfo.getSerialNumber()
					+ ".xml - interviewer: " + interviewer + " / interviewee: "
					+ interviewee);
		}
		sectionType1238 = true;
		IVPANEL = this;
		setOrientation(kass.getPageConfig().getPageFormat());
		this.addHierarchyListener(new HierarchyListener() {

			@Override
			public void hierarchyChanged(HierarchyEvent e) {

				if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0
						&& Constants.PROJECT_XML != null
						&& Constants.KASS_XML != null) {
					if (e.getComponent().isShowing()) {
						new Thread() {

							@Override
							public void run() {
								try {
									Thread.sleep(500);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								int result = JOptionPane.showConfirmDialog(
										IVPANEL, "Do you want to start timer?",
										"Care!", JOptionPane.YES_OPTION,
										JOptionPane.INFORMATION_MESSAGE);
								if (result == JOptionPane.YES_OPTION) {
									startBtn.doClick();
								}
							}
						}.start();
					}
				}
			}
		});
	}

	/**
	 * Create a status bar
	 * 
	 * @return
	 */
	protected JPanel createStatusBar() {
		JPanel statusBar = new JPanel(new BorderLayout(50, 0));
		KASS kass = new KASS();
		kassService = new KassService();
		kass = kassService.get();
		KInterviewInfo interviewInfo = kass.getInterviewInfo();
		JLabel serialNumber = new JLabel(interviewInfo.getSerialNumber());
		// statusLabel = new JLabel(mxResources.get("ready"));
		statusToolBar = new JToolBar();
		startBtn = new JButton(UIHelper.getImage("startBtn.png"));
		final JButton stopBtn = new JButton(UIHelper.getImage("stopBtn.png"));
		statusToolBar.add(startBtn);
		statusToolBar.add(stopBtn);
		statusToolBar.setFloatable(false);
		timerLabel = new JLabel(CalTimer.getInstance().getNowTimeStr());
		calTimerTask = new CalTimerTask(timerLabel);

		JPanel timeControl = new JPanel(new BorderLayout(10, 0));
		timeControl.add(statusToolBar, BorderLayout.WEST);
		timeControl.add(timerLabel, BorderLayout.CENTER);
		timeControl.setBorder(BorderFactory.createEtchedBorder());
		statusBar.add(serialNumber, BorderLayout.WEST);
		statusBar.add(timeControl, BorderLayout.EAST);
		percentPanel = new JPanel();
		percentPanel.setLayout(new GridBagLayout());
		percentPanel.setBorder(BorderFactory.createEtchedBorder());
		statusBar.add(percentPanel, BorderLayout.CENTER);
		statusBar.setBorder(BorderFactory.createEtchedBorder());
		t = new Timer();
		t.schedule(calTimerTask, 0, 1000);
		startBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (CalTimer.getInstance().isRuning()) {
					CalTimer.getInstance().stop();
					CalTimer.getInstance().saveLog();
					CalTimer.getInstance().reset();
					startBtn.setIcon(UIHelper.getImage("startBtn.png"));
					startBtn.updateUI();
				} else {
					CalTimer.getInstance().start();
					startBtn.setIcon(UIHelper.getImage("pauseBtn.png"));
					startBtn.updateUI();
				}
			}
		});
		stopBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (CalTimer.getInstance().isRuning()) {
					CalTimer.getInstance().stop();
					CalTimer.getInstance().saveLog();
					CalTimer.getInstance().reset();
					startBtn.setIcon(UIHelper.getImage("startBtn.png"));
					startBtn.updateUI();
				}
			}
		});
		updatePercent();
		return statusBar;
	}

	/**
	 * Create a tool bar
	 */
	protected void createToolBar() {
		add(topToolBar = new IvToolBar(this, JToolBar.HORIZONTAL),
				BorderLayout.NORTH);
		// JButton b0 = (JButton) topToolBar.getComponentAtIndex(0);
		// b0.setToolTipText(mxResources.get("open"));

		JButton b1 = (JButton) topToolBar.getComponentAtIndex(0);
		b1.setDisabledIcon(UIHelper.getImage("save_disable.png"));
		b1.setEnabled(true);
		b1.setToolTipText(mxResources.get("save"));

		/*
		 * JButton b2 = (JButton) topToolBar.getComponentAtIndex(3);
		 * b2.setDisabledIcon(UIHelper.getImage("print_disable.png"));
		 * b2.setToolTipText(mxResources.get("print"));
		 * 
		 * JButton b3 = (JButton) topToolBar.getComponentAtIndex(5);
		 * b3.setDisabledIcon(UIHelper.getImage("cut_disable.png"));
		 * b3.setEnabled(false); b3.setToolTipText(mxResources.get("cut"));
		 * 
		 * JButton b4 = (JButton) topToolBar.getComponentAtIndex(6);
		 * b4.setDisabledIcon(UIHelper.getImage("copy_disable.png"));
		 * b4.setEnabled(false); b4.setToolTipText(mxResources.get("copy"));
		 * 
		 * JButton b5 = (JButton) topToolBar.getComponentAtIndex(7);
		 * b5.setDisabledIcon(UIHelper.getImage("paste_disable.png"));
		 * b5.setEnabled(false); b5.setToolTipText(mxResources.get("paste"));
		 * 
		 * JButton b6 = (JButton) topToolBar.getComponentAtIndex(9);
		 * b6.setToolTipText(mxResources.get("view"));
		 * 
		 * JButton b7 = (JButton) topToolBar.getComponentAtIndex(10);
		 * b7.setDisabledIcon(UIHelper.getImage("pagesetup_disable.png"));
		 * b7.setToolTipText(mxResources.get("reversionRelease"));
		 * b7.setEnabled(false); setEnabled(false); JButton b8 = (JButton)
		 * topToolBar.getComponentAtIndex(12);
		 * b8.setDisabledIcon(UIHelper.getImage("import_disable.png"));
		 * b8.setToolTipText(mxResources.get("questionImport"));
		 * b8.setEnabled(false);
		 * 
		 * JButton b9 = (JButton) topToolBar.getComponentAtIndex(13);
		 * b9.setToolTipText(mxResources.get("questionExport"));
		 * b9.setDisabledIcon(UIHelper.getImage("expotDoc_disable.png"));
		 * b9.setEnabled(false);
		 */

		JButton b2 = (JButton) topToolBar.getComponentAtIndex(2);
		b2.setDisabledIcon(UIHelper.getImage("delete.gif"));
		b2.setToolTipText(mxResources.get("delete"));

		JButton b3 = (JButton) topToolBar.getComponentAtIndex(4);
		b3.setDisabledIcon(UIHelper.getImage("undo.gif"));
		b3.setToolTipText(mxResources.get("undo"));

		JButton b4 = (JButton) topToolBar.getComponentAtIndex(5);
		b4.setDisabledIcon(UIHelper.getImage("redo.gif"));
		b4.setToolTipText(mxResources.get("redo"));

		JButton b10 = (JButton) topToolBar.getComponentAtIndex(7);
		b10.setDisabledIcon(UIHelper.getImage("zoomin.gif"));
		b10.setEnabled(true);
		b10.setToolTipText(mxResources.get("zoomIn"));

		JButton b11 = (JButton) topToolBar.getComponentAtIndex(8);
		b11.setDisabledIcon(UIHelper.getImage("zoomout.gif"));
		b11.setEnabled(true);
		b11.setToolTipText(mxResources.get("zoomOut"));

		JButton b12 = (JButton) topToolBar.getComponentAtIndex(9);
		b12.setDisabledIcon(UIHelper.getImage("zoom.gif"));
		b12.setEnabled(true);
		b12.setToolTipText(mxResources.get("fitInWindow"));

		JButton b13 = (JButton) topToolBar.getComponentAtIndex(10);
		// b13.setDisabledIcon(UIHelper.getImage("zoom.gif"));
		b13.setEnabled(true);
		b13.setToolTipText(mxResources.get("zoomactual"));

		JButton b14 = (JButton) topToolBar.getComponentAtIndex(12);
		// b13.setDisabledIcon(UIHelper.getImage("zoom.gif"));
		b14.setEnabled(true);
		b14.setToolTipText(mxResources.get("pageLayout"));

		JButton b15 = (JButton) topToolBar.getComponentAtIndex(13);
		// b13.setDisabledIcon(UIHelper.getImage("zoom.gif"));
		b15.setEnabled(true);
		b15.setToolTipText(mxResources.get("autoArrange"));

		JButton b16 = (JButton) topToolBar.getComponentAtIndex(14);
		// b13.setDisabledIcon(UIHelper.getImage("zoom.gif"));
		b16.setEnabled(true);
		b16.setToolTipText(mxResources.get("moveDiagram"));

	}

	protected void createCenterPanel() {

		getLeftPanel();

		getRightPanel();

		JSplitPane inner = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				leftOuterPanel, rightGraphPanel);
		inner.setOneTouchExpandable(true);
		inner.setDividerLocation(300);
		inner.setDividerSize(6);
		inner.setBorder(null);
		this.add(inner, BorderLayout.CENTER);

	}

	protected void getLeftPanel() {
		leftOuterPanel = new JTabbedPane();
		JScrollPane treeScroll = genLeftTreePanel();
		leftOuterPanel.add(mxResources.get("navigation"), treeScroll);

		leftQuestionPanel = new JScrollPane();
		leftQuestionPanel
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		leftQuestionPanel
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		leftQuestionPanel.setBackground(Color.white);
		leftQuestionPanel.setBorder(BorderFactory.createEmptyBorder());
		leftOuterPanel.add(mxResources.get("questions"), leftQuestionPanel);

		searchPanel = new JScrollPane();
		searchPanel
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		searchPanel
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		searchPanel.setBackground(Color.WHITE);
		searchPanel.getViewport().add(new SearchPanel(this));
		leftOuterPanel.add(mxResources.get("search.button"), searchPanel);
	}

	private JScrollPane genLeftTreePanel() {
		final JScrollPane leftPanel = new JScrollPane();
		leftPanel
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		leftPanel
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// Left for the navigation tree of the questions
		DefaultMutableTreeNode top = questionnaireService.getLeftTree(1);
		treePanel = new JTree(top);

		treePanel.addTreeSelectionListener(new TreeSelectionListener() {

			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) e
						.getPath().getLastPathComponent();
				if (treeNode.getUserObject() instanceof QnTreeNode) {
					QnTreeNode qntn = (QnTreeNode) treeNode.getUserObject();
					if (e.getOldLeadSelectionPath() != null) {
						DefaultMutableTreeNode oldtreeNode = (DefaultMutableTreeNode) e
								.getOldLeadSelectionPath()
								.getLastPathComponent();
						QnTreeNode oldqntn = (QnTreeNode) oldtreeNode
								.getUserObject();
						if (!oldqntn.getSubsection().getDetail()
								.equals(qntn.getSubsection().getDetail())) {
							JLabel label = new JLabel(qntn.getSubsection()
									.getDetail());
							UIHelper.createMultiLabel(label, 600);
							JOptionPane.showMessageDialog(null, label);
						}
					} else {
						Subsection subsection = questionnaireService
								.searchSubsection("SS_001");
						JLabel label = new JLabel(subsection.getDetail());
						UIHelper.createMultiLabel(label, 600);
						JOptionPane.showMessageDialog(null, label);
					}
					clickTree(qntn);
				} else {

				}
			}
		});
		treePanel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource() == treePanel) {
					TreePath selPath = treePanel.getPathForLocation(e.getX(),
							e.getY());
					if (selPath != null) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath
								.getLastPathComponent();
						QnTreeNode qntn = (QnTreeNode) node.getUserObject();
						clickTree(qntn);
					}
				}
			}
		});

		DefaultTreeCellRenderer cellRenderer = (DefaultTreeCellRenderer) treePanel
				.getCellRenderer();
		UIHelper.setTreeRender(cellRenderer);
		treePanel.setShowsRootHandles(false);
		treePanel.setRootVisible(true);
		leftPanel.getViewport().removeAll();
		leftPanel.getViewport().add(treePanel);
		leftPanel.repaint();
		return leftPanel;
	}

	protected void getRightPanel() {

		graphPanel = new GraphPanel(this, 1);
		ographPanel = new GraphPanel(this, 2);
		rightGraphPanel = new JPanel();
		rightGraphPanel.setLayout(new BorderLayout());
		locationLable = new JLabel("KNQ2>>");
		rightGraphPanel.add(locationLable, BorderLayout.NORTH);
		rightOuterPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true,
				graphPanel, ographPanel);
		rightOuterPanel.setOneTouchExpandable(true);
		rightOuterPanel.setDividerLocation(0.6);
		rightOuterPanel.setResizeWeight(0.6);
		rightOuterPanel.setDividerSize(6);
		rightOuterPanel.setBorder(null);

		rightGraphPanel.add(rightOuterPanel, BorderLayout.CENTER);
		graphPanel.setGraphListener(new GraphEventListener(this));
		ographPanel.setGraphListener(new GraphEventListener(this));
		graphPanel.setDisable(GraphPanel.DISABLE_NOCLICK);
		ographPanel.setDisable(GraphPanel.DISABLE_NOCLICK);
	}

	public void clickTreeAction(QnTreeNode qntn) {

	}

	public void clickTree(QnTreeNode qntn) {

		leftOuterPanel.setSelectedIndex(1);
		leftQuestionPanel.getViewport().removeAll();
		String subSection;
		Question que;
		if (qntn.getType() == 2) {
			subSection = qntn.getQuestion().getSubsection();
			que = qntn.getQuestion();
		} else {
			if (qntn.getSubsection().getSubSections() != null
					&& !qntn.getSubsection().getSubSections().isEmpty()) {
				subSection = qntn.getSubsection().getSubSections().get(0)
						.getQuestions().get(0).getSubsection();
				que = qntn.getSubsection().getSubSections().get(0)
						.getQuestions().get(0);
			} else {
				subSection = qntn.getSubsection().getQuestions().get(0)
						.getSubsection();
				que = qntn.getSubsection().getQuestions().get(0);
			}
			collAndSelectTreeNode(que);
		}

		String label = this.getLabel(que);
		locationLable.setText(label);
		String leftLabel = this.getLeftLabel(que);
		leftOuterPanel.setTitleAt(1, leftLabel);
		if (subSection.startsWith("SS_001")) {
			// Hide right or bottom
			rightOuterPanel.getRightComponent().setMinimumSize(new Dimension());
			rightOuterPanel.setDividerLocation(1.0d);
		} else if (subSection.startsWith("SS_002")) {
			rightOuterPanel.setDividerLocation(0);
		} else if (StringUtils.isNotBlank(que.getRelevance())
				&& que.getRelevance().equals("REL_I")) {
			rightOuterPanel.setDividerLocation(0);
		} else {
			rightOuterPanel.setDividerLocation(0.6);
		}
		if (RegularUtil.isDraw(subSection) || RegularUtil.isMap(subSection)
				|| RegularUtil.isPersonOrUnion(subSection)) {
			List<String> qids = null;
			if (RegularUtil.isPersonOrUnion(subSection)) {
				qids = genSS008(que.getQid());
				graphPanel.clearSelected();
				ographPanel.clearSelected();
			} else {
				qids = new ArrayList<String>();
				qids.add(que.getQid());
			}
			setDefaultColor();
			TabQuestionPanel jpanel = new TabQuestionPanel(this, graphPanel,
					qids, 1);
			jpanel.showTabIndex(qntn.getQuestion());
			graphPanel.selectColor = "";
			graphPanel.completeSelectPersonMode();
			ographPanel.selectColor = "";
			ographPanel.completeSelectPersonMode();
			leftQuestionPanel.getViewport().add(jpanel);
			sectionType1238 = true;
			saveQuestionRunning(que.getQid());
			if (RegularUtil.isDraw(subSection)) {
				graphPanel.completePerson();
			}
			if (subSection.startsWith("SS_001")) {
				graphPanel.changeColorByStopRule();
			} else if (RegularUtil.isMap(subSection)) {
				jpanel.initSelectPerson3();
			} else if (RegularUtil.isMoreDetail(subSection)) {
				if (subSection.startsWith("SS_008")) {
					jpanel.initAllCompletePerson8();
				}
				if (subSection.startsWith("SS_009")) {
					jpanel.initAllCompleteUnion9();
				}
			}
		} else {
			clickTreeQuestion47(que);
		}
	}

	private void clickTreeQuestion47(Question question) {
		setDefaultColor();
		GraphPanel ographP, graphP;
		if (this.ifShowGraphMain(question.getQid())) {
			graphP = graphPanel;
		} else {
			graphPanel.minimumSize();
			graphP = null;
		}
		if (this.ifShowGraphO(question.getQid())) {
			ographP = ographPanel;
		} else {
			ographPanel.minimumSize();
			ographP = null;
		}
		this.repaint();
		jqustionpanel = new QuestionPanel(this, question, graphPanel,
				ographPanel);
		leftQuestionPanel.getViewport().add(jqustionpanel);
		sectionType1238 = false;
	}

	/**
	 * Next Page (Next Question)
	 * 
	 * @param qid
	 */
	public void nextPage(String qid) {
		Question question = questionnaireService.getNextQuestion(qid);
		setDefaultColor();
		if (question != null) {
			// saveQuestionRunning(question.getQid());
			goToQuestion(question);
			collAndSelectTreeNode(question);
			updatePercent();
		} else {
			Toast.makeText(kmain.frame, "Can't go to next Question",
					Style.SUCCESS).display();
		}
	}

	public void prevPage(String qid) {
		// if(questionnaireService.getPrevQuestion(qid)!=null){
		Question question = questionnaireService.getPrevQuestion(qid);
		setDefaultColor();
		if (question != null) {
			// saveQuestionRunning(question.getQid());
			goToQuestion(question);
			collAndSelectTreeNode(question);
			updatePercent();
		} else {
			Toast.makeText(kmain.frame, "Can't go to prev Question",
					Style.SUCCESS).display();
		}
		// }

	}

	public void goToQuestion(Question question) {
		String label = this.getLabel(question);
		locationLable.setText(label);
		String leftLabel = this.getLeftLabel(question);
		leftOuterPanel.setTitleAt(1, leftLabel);

		leftQuestionPanel.getViewport().removeAll();
		leftQuestionPanel.getViewport().repaint();
		String subSection = question.getSubsection();
		if (RegularUtil.isDraw(subSection) || RegularUtil.isMap(subSection)
				|| RegularUtil.isPersonOrUnion(question)) {
			List<String> qids = null;
			if (RegularUtil.isPersonOrUnion(question)) {
				qids = this.genSS008(question.getQid());
				graphPanel.clearSelected();
				ographPanel.clearSelected();
			} else {
				qids = new ArrayList<String>();
				qids.add(question.getQid());
			}
			graphPanel.setAllDefaultColor();
			ographPanel.setAllDefaultColor();
			TabQuestionPanel jpanel = new TabQuestionPanel(this, graphPanel,
					qids, 1);
			graphPanel.selectColor = "";
			ographPanel.selectColor = "";
			leftQuestionPanel.getViewport().add(jpanel);
			sectionType1238 = true;
		} else {
			// Questions in Section 4-7
			clickTreeQuestion47(question);
		}
	}

	public void status(String msg) {
		if (StringUtils.isBlank(msg))
			msg = "";
		if (statusLabel != null)
			statusLabel.setText(msg);
	}

	public void addNewPerson(GraphPanel gpanel, mxCell cell) {
		GraphElement element = (GraphElement) cell.getValue();
		if (element instanceof GraphPerson) {
			String prevQid = getLeftQuestionId();
			leftOuterPanel.setSelectedIndex(1);
			leftQuestionPanel.getViewport().removeAll();
			Questionnaire q = questionnaireService.get();
			List<PqdQuestion> questions = q.getDraw().getPerson()
					.getPquestion();
			List<String> qids = new ArrayList<String>();
			for (PqdQuestion pqd : questions) {
				qids.add(pqd.getQid());
			}
			TabQuestionPanel jpanel = new TabQuestionPanel(this, gpanel, qids,
					0);
			jpanel.setCell(cell);
			jpanel.setPrevQid(prevQid);
			leftQuestionPanel.getViewport().add(jpanel);
			Toast.makeText(kmain.frame,
					mxResources.get("interview.fill.person.name"),
					Style.SUCCESS).display();
		} else if (element instanceof GraphUnion) {
			QuestionMap qm = new QuestionMap();
			qm.saveNewUnion(cell, kassService);
		}
	}

	/**
	 * Auto draw the family member of a person
	 * 
	 * @param cell
	 */
	public void startAutocomplete(GraphPanel gpanel, mxCell cell) {
		if (cell != null && cell.getValue() instanceof GraphPerson) {
			leftOuterPanel.setSelectedIndex(1);
			leftQuestionPanel.getViewport().removeAll();
			Questionnaire q = questionnaireService.get();
			CompleteProcedure cps = q.getDraw().getCompleteProcedure();
			int qid = ((GraphPerson) (cell.getValue())).getId();
			AutoCompletePanel panel = new AutoCompletePanel(this, gpanel, qid,
					cell, cps);
			leftQuestionPanel.getViewport().add(panel);
		}
	}

	public void singleClickQuestion(GraphPanel gPanel, mxCell cell) {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				this.singleClickCheck(cell, jpanel.qst.getQid(), gPanel);
			} else if (component instanceof QuestionPanel) {
				QuestionPanel jpanel = (QuestionPanel) leftQuestionPanel
						.getViewport().getComponent(0);
				this.singleClickCheck(cell, jpanel.curqid, gPanel);
				// jpanel.clearAllValue();
			}
			upDateLeftNavPanel(cell);
		}
	}

	public void dbClickQuestion(GraphPanel gPanel, mxCell cell) {
		this.clearOtherGraph(gPanel);
		if (cell != null && cell.getValue() instanceof GraphElement) {
			GraphElement element = (GraphElement) cell.getValue();

			String prevQid = this.getLeftQuestionId();
			if (element instanceof GraphPerson) {
				if (!sectionType1238) {
					jqustionpanel.dbClickQuestion(cell);
				} else {
					if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
						Component component = leftQuestionPanel.getViewport()
								.getComponent(0);
						if (component instanceof TabQuestionPanel) {
							TabQuestionPanel panel = (TabQuestionPanel) component;
							String subSection = panel.qst.getSubsection();
							if (RegularUtil.isDraw(subSection)
									|| RegularUtil.isPersonOrUnion(panel.qst)) {
							} else {
								return;
							}
						}
					}

					GraphPerson person = (GraphPerson) element;
					if (StringUtils.isBlank(person.getName())) {
						return;
					}
					leftQuestionPanel.getViewport().removeAll();
					Subsection subsection = questionnaireService
							.searchSubsection(Constants.SUBSECTION_GENERAL);
					List<Question> questions = subsection.getQuestions();
					List<String> qids = new ArrayList<String>();
					for (Question pqd : questions) {
						qids.add(pqd.getQid());
					}
					QuestionMap qm = new QuestionMap();
					qm.initDNAPerson(person.getId(), qids);
					TabQuestionPanel jpanel = new TabQuestionPanel(this,
							gPanel, qids, 1);
					jpanel.setCell(cell);
					jpanel.setPrevQid(prevQid);
					leftQuestionPanel.getViewport().add(jpanel);

					String label = this.getLabel(questions.get(0));
					locationLable.setText(label);
					String leftLabel = this.getLeftLabel(questions.get(0));
					leftOuterPanel.setTitleAt(1, leftLabel);
					leftOuterPanel.setSelectedIndex(1);
				}
			} else if (element instanceof GraphUnion) {
				if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
					Component component = leftQuestionPanel.getViewport()
							.getComponent(0);
					if (component instanceof TabQuestionPanel) {
						TabQuestionPanel panel = (TabQuestionPanel) component;
						String subSection = panel.qst.getSubsection();
						if (RegularUtil.isDraw(panel.qst)
								|| RegularUtil.isPersonOrUnion(panel.qst)) {
						} else {
							return;
						}
					}
				}
				leftQuestionPanel.getViewport().removeAll();
				List<String> qids = new ArrayList<String>();
				List<Question> questions = questionnaireService
						.searchUnionQuestion();
				for (Question pqd : questions) {
					qids.add(pqd.getQid());
				}
				// List<PqdQuestion> uQuestions =
				// questionnaireService.get().getDraw().getUnion().getuQuestion();
				// if(uQuestions != null && uQuestions.size() > 0) {
				// for(PqdQuestion uqs : uQuestions) {
				// qids.add(uqs.getQid());
				// }
				// } else {
				//
				// }
				TabQuestionPanel jpanel = new TabQuestionPanel(this, gPanel,
						qids, 1);
				jpanel.setCell(cell);
				jpanel.setPrevQid(prevQid);
				leftQuestionPanel.getViewport().add(jpanel);
				leftOuterPanel.setSelectedIndex(1);
			}
		}
	}

	public void afterClickEmpty(GraphPanel gPanel) {
		this.clearOtherGraph(gPanel);
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				jpanel.clearAllValue();
				jpanel.setCell(null);
			}
			if (component instanceof QuestionPanel) {
				QuestionPanel jpanel = (QuestionPanel) leftQuestionPanel
						.getViewport().getComponent(0);
				jpanel.clearAllValue();
				jpanel.setCell(null, gPanel);
			}
		}
	}

	/**
	 * Save the selected persons
	 * 
	 * @param grapPanel
	 */
	public void afterSelectCell(GraphPanel grapPanel, mxCell cell) {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				if (RegularUtil.isMap(jpanel.qst)
						&& cell.getValue() instanceof GraphPerson) {
					String a = cell.getStyle();
					int index = a.indexOf("fillColor");
					if (index > 0) {
						String color = a.substring(index + 10, index + 17);
						if (StringUtils.isNotBlank(color)) {
							String tip = jpanel.getSelectPersonTip3(color);
							GraphPerson gp = (GraphPerson) cell.getValue();
							gp.setTip(tip);
						}
					}
				}
			} else if (component instanceof QuestionPanel) {
				QuestionPanel jpanel = (QuestionPanel) leftQuestionPanel
						.getViewport().getComponent(0);
				jpanel.afterSelectCell(grapPanel);
				jpanel.setCell(null, grapPanel);
			}
		}
	}

	/**
	 * Unselected a mxCell (a graph)
	 * 
	 * @param grapPanel
	 * @param mxCell
	 */
	public void afterUnSelectCell(GraphPanel grapPanel, mxCell mxCell) {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				GraphElement element = (GraphElement) mxCell.getValue();
				if (element instanceof GraphPerson) {
					GraphPerson gperson = (GraphPerson) element;
					jpanel.saveUnSelectPerson(grapPanel, gperson.getId());
				}
			} else if (component instanceof QuestionPanel) {
				QuestionPanel jpanel = (QuestionPanel) leftQuestionPanel
						.getViewport().getComponent(0);
				jpanel.afterUnSelectCell(grapPanel);
			}
		}
	}

	public void initGraph(KASS kass) {
		KPeople people = kass.getPeople();
		mxCell mxCell = null;
		if (people != null) {
			List<KPPerson> persons = people.getPersons();
			if (persons != null && persons.size() > 0) {
				for (KPPerson person : persons) {
					mxCell cell = null;
					if (person.getIsOther() == 1) {
						cell = ographPanel.drawPerson(person);
					} else {
						cell = graphPanel.drawPerson(person);
					}
					if (person.getPid() == 0) {
						mxCell = cell;
					}
				}
			}
		}
		if (kass.getUnions() != null) {
			List<KUUnion> unions = kass.getUnions().getUnions();
			if (unions != null && unions.size() > 0) {
				for (KUUnion union : unions) {
					if (union.getIsOther() != null && union.getIsOther() == 1) {
						ographPanel.drawUnion(union);
					} else {
						graphPanel.drawUnion(union);
					}
				}
			}
		}
		//
		graphPanel.zoomToCell(mxCell);
		mxCell root = (mxCell) ographPanel.graphComponent.getGraph().getModel()
				.getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			mxCell = (mxCell) c.getChildAt(0);
		}
		ographPanel.zoomToCell(mxCell);
	}

	/**
	 * Get the pid of the next person
	 * 
	 * @return
	 */
	public int getNextPersonId() {
		int maxGid = this.graphPanel.getMaxPersonId();
		int maxOid = this.ographPanel.getMaxPersonId();
		int nextId = Math.max(maxGid, maxOid);
		if (nextId == -1) {
			nextId = 0;
		} else {
			nextId = nextId + 1;
		}
		return nextId;
	}

	public int getNextUnionId() {
		int maxGid = this.graphPanel.getMaxUnionId();
		int maxOid = this.ographPanel.getMaxUnionId();
		int nextId = Math.max(maxGid, maxOid);
		if (nextId == -1) {
			nextId = 0;
		} else {
			nextId = nextId + 1;
		}
		return nextId;
	}

	/**
	 * Get the color of the person icon by the pid of a person
	 * 
	 * @return
	 */
	public String getPersonColor(int pid) {
		String color = this.graphPanel.getColorByPerson(pid);
		if (StringUtils.isNotBlank(color)) {
			color = this.ographPanel.getColorByPerson(pid);
		}
		return color;
	}

	public void upDateLeftNavPanel(mxCell cell) {
		if (cell != null) {
			if (cell.getValue() instanceof GraphElement) {
				GraphElement one = (GraphElement) cell.getValue();
				Integer pid = one.getId();

			}
		} else {

		}
	}

	public void openDefaultQuestionPanel() {
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePanel
				.getLastSelectedPathComponent();
		if (treeNode == null) {
			return;
		}
		QnTreeNode qntn = (QnTreeNode) treeNode.getUserObject();
		if (qntn != null && qntn.getType() == 2) {
			Question q = qntn.getQuestion();
			List<String> qs = new ArrayList<String>();
			qs.add(q.getQid());
			TabQuestionPanel jpanel = new TabQuestionPanel(this, graphPanel,
					qs, 1);
			leftQuestionPanel.getViewport().add(jpanel);
			leftQuestionPanel.getViewport().repaint();
			graphPanel.setDisable(GraphPanel.DISABLE_NEW);
			ographPanel.setDisable(GraphPanel.DISABLE_NEW);
		}
	}

	public void openSpecialQuestionPanel(String qid) {
		if (StringUtils.isNotBlank(qid)) {
			Question q = questionnaireService.searchQuestion(qid);
			openSpecialQuestionPanel(q);
		} else {
			leftQuestionPanel.getViewport().removeAll();
			leftOuterPanel.setSelectedIndex(0);
		}
	}

	public void openSpecialQuestionPanel(Question q) {
		List<String> qids = null;
		if (RegularUtil.isPersonOrUnion(q)) {
			qids = genSS008(q.getQid());
			graphPanel.clearSelected();
			ographPanel.clearSelected();
		} else {
			qids = new ArrayList<String>();
			qids.add(q.getQid());
		}

		String leftLabel = this.getLeftLabel(q);
		leftOuterPanel.setTitleAt(1, leftLabel);
		leftOuterPanel.setSelectedIndex(1);
		leftQuestionPanel.getViewport().removeAll();
		TabQuestionPanel jpanel = new TabQuestionPanel(this, graphPanel, qids,
				1);
		leftQuestionPanel.getViewport().add(jpanel);
		leftQuestionPanel.getViewport().repaint();
		graphPanel.setDisable(GraphPanel.DISABLE_NEW);
		ographPanel.setDisable(GraphPanel.DISABLE_NEW);
	}

	private String getLabel(Question question) {
		String label = "KNQ2>>";
		Subsection subsection = questionnaireService.searchSubsection(question
				.getSubsection());
		Subsection pSubSection = questionnaireService
				.searchParentSubsection(subsection.getSid());
		if (pSubSection == null) {
			pSubSection = subsection;
		}
		int sortNo = 1;
		List<Subsection> sections = Constants.QUESTIONNAIRE.getSection()
				.getSubsections();
		for (int i = 0; i < sections.size(); i++) {
			if (pSubSection.getSid().equals(sections.get(i).getSid())) {
				sortNo = i + 1;
				break;
			}
		}
		label = label + "Section " + sortNo + " " + pSubSection.getTitle()
				+ ">>";
		if (!subsection.getSid().equals(pSubSection.getSid())) {
			List<Subsection> subsections = pSubSection.getSubSections();
			int subSortNo = 1;
			for (int i = 0; i < subsections.size(); i++) {
				if (subsection.getSid().equals(subsections.get(i).getSid())) {
					subSortNo = i + 1;
					break;
				}
			}
			label = label + "Section " + sortNo + "." + subSortNo + " "
					+ subsection.getTitle() + ">>";
		}
		label = label + question.getSortNo() + "." + question.getTitle();
		return "<html>" + label + "</html>";
	}

	public String getLeftLabel(Question question) {
		Subsection subsection = questionnaireService.searchSubsection(question
				.getSubsection());
		String result = "Questions";
		if (RegularUtil.isDraw(subsection.getSid())) {
			result = "Draw";
		} else if (RegularUtil.isMap(subsection.getSid())) {
			result = "Map";
		} else if (RegularUtil.isPersonOrUnion(question)) {
			result = "More Detail";
		}
		return result;
	}

	/**
	 * Check the question if it need to answer
	 */
	private String checkNeedQuestion(int pid, Question question) {
		QuestionMap qm = new QuestionMap();
		String qid = qm.getNoCompleteQuestion(pid, question.getQid());
		if (StringUtils.isNotBlank(qid)) {
			Question fquestion = questionnaireService.searchQuestion(qid);
			Question parent = questionnaireService.searchParanetQuestion(qid);
			if (parent == null) {
				parent = fquestion;
			}
			return parent.getQid();
			// String tip = fquestion.getTitle() +
			// " not complete, please complete first?";
		}
		return "";
	}

	/**
	 * Calculate the percent of the completed questions
	 */
	public void updatePercent() {
		percentPanel.removeAll();
		GridBagConstraints c = new GridBagConstraints();
		Map<String, Integer> map = kassService.getAnswerPercentage();
		Integer total = map.get("total");
		Integer inprocess = map.get("inprocess");
		Integer completed = map.get("completed");
		Integer noprocess = map.get("noprocess");
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		float completePercent = (completed * 100.0f) / (total * 1.0f);
		float inprocessPercent = (inprocess * 100.0f) / (total * 1.0f);
		float noprocessPercent = (noprocess * 100.0f) / (total * 1.0f);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = completed;
		c.weighty = 0;
		JPanel p1 = new JPanel();
		p1.setBackground(ColorUtil.getPersonProgressColorO(1));
		p1.add(new JLabel(df.format(completePercent) + "%"));
		p1.setToolTipText("Completed : " + completed);
		percentPanel.add(p1, c);

		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = inprocess;
		c.weighty = 0;
		JPanel p2 = new JPanel();
		p2.setBackground(ColorUtil.getPersonProgressColorO(2));
		p2.add(new JLabel(df.format(inprocessPercent) + "%"));
		p2.setToolTipText("Some Completed : " + inprocess);
		percentPanel.add(p2, c);

		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = noprocess;
		c.weighty = 0;
		JPanel p3 = new JPanel();
		p3.setBackground(ColorUtil.getPersonProgressColorO(4));
		p3.add(new JLabel(df.format(noprocessPercent) + "%"));
		p3.setToolTipText("Uncompleted : " + noprocess);
		percentPanel.add(p3, c);

		percentPanel.updateUI();
	}

	/**
	 * Get the person of the NKK Diagram
	 */
	public List<KPPerson> getKPersons() {
		List<KPPerson> kpersons = new ArrayList<KPPerson>();
		List<KPPerson> persons = kassService.getKPersons();

		for (KPPerson kperson : persons) {
			if (kperson.getIsOther().equals(0)) {
				kpersons.add(kperson);
			}
		}
		return kpersons;
	}

	/**
	 * Get the person of the Other Important Diagram
	 */
	public List<KPPerson> getKPersonsO() {
		List<KPPerson> kpersons = new ArrayList<KPPerson>();
		List<KPPerson> persons = kassService.getKPersons();

		for (KPPerson kperson : persons) {
			if (kperson.getIsOther().equals(1)) {
				kpersons.add(kperson);
			}
		}
		return kpersons;
	}

	/**
	 * The flag to display or not display the NKK Diagram Window
	 */
	public Boolean ifShowGraphMain(String curqid) {
		Boolean flag = false;
		flag = ifPersonsValeNA(getKPersons(), curqid);
		return flag;
	}

	/**
	 * The flag to display or not display the Other Important Diagram Window
	 */
	public Boolean ifShowGraphO(String curqid) {
		Boolean flag = false;
		flag = ifPersonsValeNA(getKPersonsO(), curqid);
		return flag;
	}

	public Boolean ifPersonsValeNA(List<KPPerson> persons, String curqid) {
		Boolean flag = false;
		for (KPPerson person : persons) {
			KQQuestion kquestion = kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kquestion != null) {
				if (kquestion.getValue().equals(Constants.DEFAULT_NA)) {
					flag = true;
					return flag;
				}
			}
		}
		return flag;
	}

	/**
	 * Check the answer condition of the question
	 * 
	 * @param cell
	 * @param qid
	 */
	public void singleClickCheck(mxCell cell, String qid, GraphPanel gPanel) {

		clearOtherGraph(gPanel);
		if (cell == null)
			return;
		if (cell.getValue() instanceof GraphPerson) {
			Question q12 = questionnaireService.searchQuestion(qid);
			if (RegularUtil.isDraw(q12)) {
				this.graphPanel.setAllDefaultColor();
				this.ographPanel.setAllDefaultColor();
				graphPanel.changeColorByStopRule();
				graphPanel.completePerson();
				ographPanel.completePerson();
				return;
			}

			QuestionMap qm = new QuestionMap();
			GraphPerson gp = (GraphPerson) cell.getValue();
			if (StringUtils.isBlank(gp.getName())) {
				return;
			}
			String skipQid = qm.getNoCompleteQuestion(gp.getId(), qid);
			Question question = null;
			if (StringUtils.isNotBlank(skipQid)) {
				question = questionnaireService.searchQuestion(skipQid);
				if (question instanceof FollowupQuestion) {
					question = questionnaireService
							.searchParanetQuestion(skipQid);
				}
				Toast.makeText(kmain.frame,
						"Please complete " + question.getTitle() + " first.",
						Style.SUCCESS).display();
				this.openSpecialQuestionPanel(question);
			} else {
				Component component = leftQuestionPanel.getViewport()
						.getComponent(0);
				if (component instanceof TabQuestionPanel) {
					TabQuestionPanel jpanel = (TabQuestionPanel) component;
					this.graphPanel.setAllDefaultColor();
					this.ographPanel.setAllDefaultColor();
					Question q = jpanel.qst;
					if (RegularUtil.isPersonOrUnion(q)) {
						leftQuestionPanel.getViewport().removeAll();
						Subsection subsection = questionnaireService
								.searchSubsection(Constants.SUBSECTION_GENERAL);
						List<Question> questions = subsection.getQuestions();
						List<String> qids = new ArrayList<String>();
						for (Question q1 : questions) {
							qids.add(q1.getQid());
						}
						qm.initDNAPerson(gp.getId(), qids);
						jpanel = new TabQuestionPanel(this, gPanel, qids, 1);
						jpanel.setCell(cell);
						jpanel.showTabIndex(question);
						leftQuestionPanel.getViewport().add(jpanel);
					} else {
						leftQuestionPanel.getViewport().removeAll();
						List<String> qids = new ArrayList<String>();
						qids.add(q.getQid());
						jpanel = new TabQuestionPanel(this, gPanel, qids, 1);
						jpanel.setCell(cell);
						jpanel.showTabIndex(question);
						leftQuestionPanel.getViewport().add(jpanel);
					}
				} else if (component instanceof QuestionPanel) {
					QuestionPanel jpanel = (QuestionPanel) leftQuestionPanel
							.getViewport().getComponent(0);
					// jpanel.clearAllValue();
					jpanel.setCell(cell, gPanel);
				}
			}
		} else if (cell.getValue() instanceof GraphUnion) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				Question q = jpanel.qst;
				String prevQid = jpanel.getPrevQid();
				if (RegularUtil.isPersonOrUnion(q)) {
					this.graphPanel.setAllDefaultColor();
					this.ographPanel.setAllDefaultColor();
					leftQuestionPanel.getViewport().removeAll();
					List<String> qids = new ArrayList<String>();
					// List<PqdQuestion> uQuestions =
					// questionnaireService.get().getDraw().getUnion().getuQuestion();
					// if(uQuestions != null && uQuestions.size() > 0) {
					// for(PqdQuestion uqs : uQuestions) {
					// qids.add(uqs.getQid());
					// }
					// } else {
					List<Question> questions = questionnaireService
							.searchUnionQuestion();
					for (Question pqd : questions) {
						qids.add(pqd.getQid());
					}
					// }
					jpanel = new TabQuestionPanel(this, graphPanel, qids, 1);
					jpanel.setCell(cell);
					jpanel.setPrevQid(prevQid);
					leftQuestionPanel.getViewport().add(jpanel);
				} else if (RegularUtil.isMap(q)) {

				} else {
					this.graphPanel.setAllDefaultColor();
					this.ographPanel.setAllDefaultColor();
					graphPanel.changeColorByStopRule();
				}
			}
		}
	}

	public void setDefaultColor() {
		graphPanel.clearLockPerson();
		graphPanel.setAllDefaultColor();
		ographPanel.clearLockPerson();
		ographPanel.setAllDefaultColor();
		graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
		ographPanel.setDisable(GraphPanel.DISABLE_CLICK);
		graphPanel.setAllTipNull();
		ographPanel.setAllTipNull();
		graphPanel.setAllRemarkNone();
		ographPanel.setAllRemarkNone();
	}

	/**
	 * Running the Save Operation
	 * 
	 * @param qid
	 */
	public void saveQuestionRunning(String qid) {
		KQuestion kquestion = kassService.getCompleteField(qid);
		if ("0".equals(kquestion.getCompleted())) {
			kquestion.setCompleted("2");
			kassService.save();
		}
	}

	/**
	 * Get the qid list of the questions in section 8
	 * 
	 * @param qid
	 * @return
	 */
	public List<String> genSS008(String qid) {
		List<String> qids = new ArrayList<String>();
		if (StringUtils.isNotBlank(qid)) {
			Question question = questionnaireService.searchQuestion(qid);
			Subsection subsection = questionnaireService
					.searchSubsection(question.getSubsection());
			List<Question> questions = subsection.getQuestions();
			for (int i = 0; i < questions.size(); i++) {
				qids.add(questions.get(i).getQid());
			}
		} else {
			Subsection subsection = questionnaireService
					.searchSubsection(Constants.SUBSECTION_GENERAL);
			List<Question> questions = subsection.getQuestions();
			for (Question pqd : questions) {
				qids.add(pqd.getQid());
			}
		}
		return qids;
	}

	/**
	 * Get the qid of the question in the navigation tree of questions
	 */
	public String getLeftQuestionId() {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel panel = (TabQuestionPanel) component;
				Question q = panel.qst;
				return q.getQid();
			}
		}
		return "";
	}

	/**
	 * Display the navigation tree of questions
	 * 
	 * @param question
	 * @return
	 */
	public QnTreeNode collAndSelectTreeNode(Question question) {
		DefaultTreeModel model = (DefaultTreeModel) treePanel.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		return collAndSelectTreeNode(root, question);
	}

	private QnTreeNode collAndSelectTreeNode(DefaultMutableTreeNode node,
			Question question) {
		if (node.isLeaf()) {
			return null;
		}
		for (int i = 0; i < node.getChildCount(); i++) {
			DefaultMutableTreeNode tn = (DefaultMutableTreeNode) node
					.getChildAt(i);
			QnTreeNode item = (QnTreeNode) tn.getUserObject();
			if (item.getQuestion() != null
					&& question.equals(item.getQuestion())) {
				// expand
				TreePath path = new TreePath(
						((DefaultMutableTreeNode) tn).getPath());
				treePanel.collapsePath(path);
				treePanel.setSelectionPath(path);
				DefaultTreeCellRenderer cellRender = new DefaultTreeCellRenderer();
				cellRender.setBackground(Color.GRAY);
				// treePanel.setCellRenderer(cellRender);
				return item;
			} else {
				QnTreeNode n = collAndSelectTreeNode(tn, question);
				if (n == null) {
					continue;
				} else {
					return n;
				}
			}
		}
		return null;
	}

	public void clearOtherGraph(GraphPanel gPanel) {
		if (gPanel.type == 1) {// graphPanel
			ographPanel.clearSelected();
		} else if (gPanel.type == 2) {// ographPanel
			graphPanel.clearSelected();
		}
	}

	public void addANewPerosnIOIFinish(String pid) {
		jqustionpanel.paelq.addANewPerosnIOIFinish(pid);
	}

	public void secondSelectPerosns(List<Integer> pids) {
		jqustionpanel.paelq.secondSelectPerosns(pids);
	}

	public void showPersonAnswers(String qid) {
		graphPanel.showRemark(qid, true);
		ographPanel.showRemark(qid, true);
	}

	public void clearPersonAnswers(String qid) {
		graphPanel.showRemark(qid, false);
		ographPanel.showRemark(qid, false);
	}

	/**
	 * Search a peron by pid, name and sex 0 = not find, 1 = find in GraphPanel,
	 * 2 = find in the OgrapPanel
	 * 
	 * @param name
	 * @return
	 */
	private int searchByName(int pid, String name, int sex) {
		mxCell mxCell = graphPanel.searchByName(pid, name, sex);
		if (mxCell != null) {
			int x = (int) mxCell.getGeometry().getCenterX();
			int y = (int) mxCell.getGeometry().getCenterY();
			// graphPanel.graphComponent(mxCell);
			// graphPanel.graphComponent.getGraph().refresh();
			return 1;
		}
		mxCell = ographPanel.searchByName(pid, name, sex);
		if (mxCell != null) {
			int x = (int) mxCell.getGeometry().getCenterX();
			int y = (int) mxCell.getGeometry().getCenterY();
			// ographPanel.graphComponent.scrollCellToVisible(mxCell);
			// ographPanel.graphComponent.getGraph().refresh();
			return 2;
		}
		return 0;
	}

	/**
	 * check if there is same name
	 * 
	 * @param cell
	 */
	public boolean checkSameName(int pid, int sex, QuestionMap qm, mxCell cell) {

		String first = qm.getLeftValue("Surname");
		String second = qm.getLeftValue("Othernames");
		int state = searchByName(pid, first + " " + second, sex);
		if (state == 1 || state == 2) {
			Object[] el = { "Yes", "No" };
			mxCell mxCell = graphPanel.searchByName(pid, first + " " + second,
					sex);
			GraphPerson gp = (GraphPerson) mxCell.getValue();
			int result = JOptionPane.showOptionDialog(null,
					"Is this person the pid " + gp.getId() + "-" + first + " "
							+ second, "Select Option",
					JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
					null, el, el[1]);
			if (result == JOptionPane.NO_OPTION) {// 新增一个
				return true;
			} else if (result == JOptionPane.YES_OPTION) {
				KPPerson person = kassService.getPerson(pid);
				if (person == null) {
					mxCell[] cells = new mxCell[] { cell };
					graphPanel.graphComponent.getGraph().removeCells(cells);
				}
				if (cell != null)
					openDefaultQuestionPanel();
				return false;
			}
		}
		return true;
	}

	/**
	 * Get the person icon by pid, sex
	 * 
	 * @param pid
	 * @param sex
	 * @param qm
	 * @param cell
	 * @return
	 */
	public mxCell getSameCellByName(int pid, int sex, QuestionMap qm,
			mxCell cell) {
		String first = qm.getLeftValue("Surname");
		String second = qm.getLeftValue("Othernames");
		mxCell mxCell = graphPanel.searchByName(pid, first + " " + second, sex);
		return mxCell;
	}

	/**
	 * Click event on the view mode
	 * 
	 * @param gPanel
	 * @param cell
	 * @return
	 */
	public void clickOnViewModel(GraphPanel gPanel, mxCell cell) {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				if (RegularUtil.isMap(jpanel.qst)) {
					if (cell.getValue() instanceof GraphPerson) {
						String a = cell.getStyle();
						if (a.contains("fillColor")) {
							int index = a.indexOf("fillColor");
							String r = a.substring(index + 10, index + 17);
							jpanel.clickSelectPerson(r);
						}
					}
				}
			}
		}
	}

	public void pressedOnViewModel(GraphPanel gPanel, mxCell cell) {
		if (leftQuestionPanel.getViewport().getComponentCount() > 0) {
			Component component = leftQuestionPanel.getViewport().getComponent(
					0);
			if (component instanceof TabQuestionPanel) {
				TabQuestionPanel jpanel = (TabQuestionPanel) component;
				if (RegularUtil.isDraw(jpanel.qst)) {
					Toast.makeText(
							kmain.frame,
							"Click the “modify” button on the left panel to modify the diagram",
							Style.SUCCESS).display();
				}
			}
		}
	}

	/**
	 * Page Layout 0 = portrait, 1 = landscape
	 * 
	 * @param orientation
	 */
	public void setOrientation(int orientation) {
		if (orientation == 0) {
			rightOuterPanel.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
			rightOuterPanel.repaint();
			graphPanel.graphComponent.getPageFormat().setOrientation(
					PageFormat.PORTRAIT);
			graphPanel.zoomToEge();
			graphPanel.graphComponent.refresh();
			ographPanel.graphComponent.getPageFormat().setOrientation(
					PageFormat.PORTRAIT);
			ographPanel.zoomToFirst();
			ographPanel.graphComponent.refresh();
		} else {
			rightOuterPanel.setOrientation(JSplitPane.VERTICAL_SPLIT);
			rightOuterPanel.repaint();
			graphPanel.graphComponent.getPageFormat().setOrientation(
					PageFormat.LANDSCAPE);
			graphPanel.zoomToEge();
			graphPanel.graphComponent.refresh();
			ographPanel.graphComponent.getPageFormat().setOrientation(
					PageFormat.LANDSCAPE);
			ographPanel.zoomToFirst();
			ographPanel.graphComponent.refresh();
		}
	}

}
