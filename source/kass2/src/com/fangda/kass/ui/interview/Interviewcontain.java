package com.fangda.kass.ui.interview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.fangda.kass.model.interview.InterviewInfo;
import com.fangda.kass.model.interview.InterviewPage;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KPeople;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.questionnaire.QuestionnaireService;

/**
 * This class for the component contained in the interview in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class Interviewcontain {

	public LinkedHashMap<String, Object> radioGroupMap;
	public LinkedHashMap<String, Object> keyValueMap;
	public LinkedHashMap<String, Object> radioMap;
	public LinkedHashMap<String, Object> parentIdQId;
	public LinkedHashMap<String, Object> checkboxMap;
	public LinkedHashMap<String, Object> questionMap;
	public LinkedHashMap<String, Object> optionId;
	public LinkedHashMap<String, Object> textField;
	public LinkedHashMap<String, Object> noteField;
	public LinkedHashMap<String, Object> pageQuestion;
	public LinkedHashMap<String, Object> layoutMap;
	public LinkedHashMap<String, Object> groupMap;
	public LinkedHashMap<String, Object> noteFieldLabel;
	public LinkedHashMap<String, Object> qLabel;
	public Question questionsMap;
	public KQQuestion kqquestions;
	public InterviewPage page;
	public String qtType;
	public String pageType;
	protected QuestionnaireService questionnaireService;

	public Interviewcontain() {
		// TODO Auto-generated constructor stub
		questionnaireService = new QuestionnaireService();
		radioGroupMap = new LinkedHashMap<String, Object>();
		keyValueMap = new LinkedHashMap<String, Object>();
		radioMap = new LinkedHashMap<String, Object>();
		parentIdQId = new LinkedHashMap<String, Object>();
		questionMap = new LinkedHashMap<String, Object>();
		checkboxMap = new LinkedHashMap<String, Object>();
		optionId = new LinkedHashMap<String, Object>();
		questionsMap = new Question();
		page = new InterviewPage();
		textField = new LinkedHashMap<String, Object>();
		pageQuestion = new LinkedHashMap<String, Object>();
		qtType = "1";
		layoutMap = new LinkedHashMap<String, Object>();
		groupMap = new LinkedHashMap<String, Object>();
		noteField = new LinkedHashMap<String, Object>();
		noteFieldLabel = new LinkedHashMap<String, Object>();
		qLabel = new LinkedHashMap<String, Object>();
	}

	public void outxml() {
		KASS kass = new KASS();
		KPeople people = new KPeople();
		ArrayList<KPPerson> persons = new ArrayList<KPPerson>();
		KPPerson person = new KPPerson();
		KQQuestion question = new KQQuestion();

		ArrayList<KQQuestion> questions = new ArrayList<KQQuestion>();

		java.util.List<KQQuestionField> fields = new ArrayList<KQQuestionField>();
		if (questionsMap != null) {
			// question.setType(questionsMap.getType());
			// question.setAskOthers(questionsMap.getAskOthers());
			question.setValue("Ok");
			// question.setDirection(questionsMap.getDirection());
			// question.setIsauto(questionsMap.getIsauto());
			question.setQid(questionsMap.getQid());
			// question.setRelevance(questionsMap.getRelevance());
			// question.setSubsection(questionsMap.getSubsection());
		}

		for (Iterator iterato = questionMap.entrySet().iterator(); iterato
				.hasNext();) {

			Map.Entry<String, Object> els = (Map.Entry<String, Object>) iterato
					.next();
			String key = els.getKey();
			// inques.setQid(key);

			InterviewInfo info = (InterviewInfo) els.getValue();
			String value = "";
			if (info.getQtype().equals("A")) {
				JTextField field = (JTextField) textField.get(key);
				if (field != null) {
					value = field.getText();
					// 写入xml field
					KQQuestionField fd = new KQQuestionField();
					fd.setQid(key);
					Question qus = questionnaireService.searchQuestion(fd
							.getQid());
					if (qus != null) {
						fd.setLabel(qus.getLabel());
					}
					fd.setValue(value);
					fields.add(fd);
				}

			} else if (info.getQtype().equals("C")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) radioMap.get(st);
					if (jrb.isSelected()) {
						value = (String) keyValueMap.get(st);
						System.out.println(value);
						// Write to XML field
						KQQuestionField fd = new KQQuestionField();
						fd.setQid(key);
						Question qus = questionnaireService.searchQuestion(key);
						if (qus != null) {
							fd.setLabel(qus.getLabel());
						}
						fd.setValue(value);
						fields.add(fd);
					}
				}
			} else if (info.getQtype().equals("T")) {
				JTextArea note = (JTextArea) noteField.get(key);
				if (note != null) {
					String name = (String) noteFieldLabel.get(key);
					value = note.getText();
					KQQuestionField fd = new KQQuestionField();
					fd.setQid(key);
					Question qus = questionnaireService.searchQuestion(fd
							.getQid());
					if (qus != null) {
						fd.setLabel(qus.getLabel());
					}
					fd.setValue(value);
					fields.add(fd);
				}
			} else if (info.getQtype().equals("Q")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) radioMap.get(st);

					if (jrb.isSelected()) {
						value = (String) keyValueMap.get(st);
						KQQuestionField fd = new KQQuestionField();
						fd.setQid(key);
						Question qus = questionnaireService.searchQuestion(fd
								.getQid());
						if (qus != null) {
							fd.setLabel(qus.getLabel());
						}
						fd.setValue(value);
						fields.add(fd);
						break;
					}
				}
			} else if (info.getQtype().equals("D")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) checkboxMap.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";
					KQQuestionField fd = new KQQuestionField();
					// fd.setName((String)qLabel.get(key));
					fd.setQid(key);
					Question qus = questionnaireService.searchQuestion(fd
							.getQid());
					if (qus != null) {
						fd.setLabel(qus.getLabel());
					}
					fd.setValue(value);
					fields.add(fd);
				}
			} else {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) checkboxMap.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";
					KQQuestionField fd = new KQQuestionField();
					// fd.setName(st);
					fd.setQid(st);
					Option option = questionnaireService.searchQuestionOption(
							info.getQid(), st);
					if (option != null) {
						fd.setLabel(option.getLabel());
					}
					fd.setValue(value);
					fields.add(fd);
				}
			}
		}

		question.setFields(fields);
		questions.add(question);
		person.setQuestions(questions);
		persons.add(person);
		people.setPersons(persons);
		kass.setPeople(people);

		XmlHelper.getInstance().saveFileKASS(kass, "E:\\w\\e\\tmp\\t.xml");
	}

	public void clean() {
		// TODO Auto-generated method stub
		groupMap.clear();

		questionMap.clear();
		radioGroupMap.clear();
		keyValueMap.clear();
		radioMap.clear();
		parentIdQId.clear();
		checkboxMap.clear();
		questionMap.clear();
		optionId.clear();
		textField.clear();
		noteField.clear();
		pageQuestion.clear();
		layoutMap.clear();
		noteFieldLabel.clear();
		qLabel.clear();

	}

	public void setNextPage() {
		// TODO Auto-generated method stub
		doPage(page.getCurpage() + 1);
	}

	public boolean hasNextPage() {
		boolean flag = false;
		Integer pageNum = page.getCurpage() + 1;
		if (pageNum > 0 && pageNum <= page.getCount()) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	public boolean hasPrePage() {
		boolean flag = false;
		Integer pageNum = page.getCurpage() - 1;
		if (pageNum > 0 && pageNum <= page.getCount()) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	public void doPage(Integer pageNum) {
		if (pageNum > 0 && pageNum <= page.getCount()) {
			page.setCurpage(pageNum);
		} else {
			// Tip: Out of Range
		}
	}

	public void setPrePage() {
		// TODO Auto-generated method stub
		doPage(page.getCurpage() - 1);
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getQtType() {
		return qtType;
	}

	public void setQtType(String qtType) {
		this.qtType = qtType;
	}

	public InterviewPage getPage() {
		return page;
	}

	public void setPage(InterviewPage page) {
		this.page = page;
	}

	public Question getQuestionsMap() {
		return questionsMap;
	}

	public void setQuestionsMap(Question questionsMap) {
		this.questionsMap = questionsMap;
	}

	public KQQuestion getKqquestions() {
		return kqquestions;
	}

	public void setKqquestions(KQQuestion kqquestions) {
		this.kqquestions = kqquestions;
	}
}
