package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KFieldSite;
import com.fangda.kass.model.interview.KInterviewInfo;
import com.fangda.kass.model.interview.Project;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.support.CalTimer;
import com.fangda.kass.ui.interview.support.InitUtil;
import com.fangda.kass.ui.questionnaire.support.QnTreeNode;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.DateUtil;
import com.fangda.kass.util.FileUtil;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of the interview properties edit
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class InterviewEditDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6598299868427454283L;
	JPanel panel;
	protected JTextField sampleNumField;
	protected JTextField serialNumField;
	protected JTextField interviewerField;
	protected JTextField intervieweeField;
	protected JTextField startDateField;
	protected JTextField endDateField;
	protected JTextField ProjectTitle;
	protected JTextField ProjectDirector;
	protected JTextField Country;
	protected JTextField Province;
	protected JTextField District;
	protected JTextField County;
	protected JTextField Town;
	protected JTextField Village;
	protected JTextField PlaceName;

	KassService service;
	QuestionnaireService qservice;

	public InterviewEditDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {
		// TODO Auto-generated method stub
		super.initDialog();
		service = new KassService();
		qservice = new QuestionnaireService();
		super.setTitle(mxResources.get("interviewProperties"));
	}

	@Override
	public JPanel createPanel() {
		sampleNumField = new JTextField(40);
		serialNumField = new JTextField(30);
		interviewerField = new JTextField(15);
		intervieweeField = new JTextField(15);
		startDateField = new JTextField(DateUtil.format(new Date(),
				"yyyy-MM-dd HH:mm:ss"), 15);
		startDateField.setEnabled(false);
		endDateField = new JTextField(15);
		endDateField.setEnabled(false);
		ProjectTitle = new JTextField(40);
		ProjectTitle.setEnabled(false);
		ProjectDirector = new JTextField(40);
		ProjectDirector.setEnabled(false);
		Country = new JTextField(40);
		Country.setEnabled(false);
		Province = new JTextField(40);
		Province.setEnabled(false);
		District = new JTextField(40);
		District.setEnabled(false);
		County = new JTextField(40);
		County.setEnabled(false);
		Town = new JTextField(40);
		Town.setEnabled(false);
		Village = new JTextField(40);
		Village.setEnabled(false);
		PlaceName = new JTextField(40);
		PlaceName.setEnabled(false);

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel fieldSitePanel = new JPanel();

		fieldSitePanel.setBackground(Color.white);
		fieldSitePanel
				.setLayout(new BoxLayout(fieldSitePanel, BoxLayout.X_AXIS));
		fieldSitePanel.add(new JLabel("<html><strong>"
				+ mxResources.get("fieldSite") + "</strong></html>"));
		fieldSitePanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(fieldSitePanel);

		GridBagLayout bagLayout = new GridBagLayout();

		JPanel fieldSiteInfoPanel = new JPanel(bagLayout);
		GridBagConstraints c = new GridBagConstraints();
		fieldSiteInfoPanel.setBackground(Color.white);
		int fieldSiteInfo = 0;
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("projectTitle") + ":</HTML>"), c, 0,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, ProjectTitle,
				c, 1, fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("projectDirectory") + ":</HTML>"),
				c, 2, fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white,
				ProjectDirector, c, 3, fieldSiteInfo, 1, 1,
				GridBagConstraints.WEST);
		fieldSiteInfo++;

		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("country") + ":</HTML>"), c, 0,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, Country, c, 1,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("province") + ":</HTML>"), c, 2,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, Province, c, 3,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		fieldSiteInfo++;

		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("district") + ":</HTML>"), c, 0,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, District, c, 1,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("county") + ":</HTML>"), c, 2,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, County, c, 3,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		fieldSiteInfo++;

		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("town") + ":</HTML>"), c, 0,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, Town, c, 1,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("village") + ":</HTML>"), c, 2,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, Village, c, 3,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		fieldSiteInfo++;

		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("placeName") + ":</HTML>"), c, 0,
				fieldSiteInfo, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldSiteInfoPanel, Color.white, PlaceName, c,
				1, fieldSiteInfo, 1, 1, GridBagConstraints.WEST);

		panel.add(fieldSiteInfoPanel);

		JPanel propPanel = new JPanel();

		propPanel.setBackground(Color.white);
		propPanel.setLayout(new BoxLayout(propPanel, BoxLayout.X_AXIS));
		propPanel.add(new JLabel("<html><strong>" + mxResources.get("sample")
				+ "</strong></html>"));
		propPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(propPanel);

		JPanel projectPanel = new JPanel(bagLayout);
		projectPanel.setBackground(Color.white);
		int index = 0;
		UIHelper.addToBagpanel(
				projectPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("sampleNumber") + ":</HTML>"), c, 0,
				index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(projectPanel, Color.white, sampleNumField, c, 1,
				index, 1, 1, GridBagConstraints.WEST);
		index++;
		UIHelper.addToBagpanel(
				projectPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("serialNumber") + ":</HTML>"), c, 0,
				index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(projectPanel, Color.white, serialNumField, c, 1,
				index, 1, 1, GridBagConstraints.WEST);
		panel.add(projectPanel);

		JPanel fieldPanel = new JPanel();
		fieldPanel.setBackground(Color.white);
		fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.X_AXIS));
		fieldPanel.add(new JLabel("<html><strong>"
				+ mxResources.get("interview") + "</strong></html>"));
		fieldPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(fieldPanel);

		JPanel fieldContentPanel = new JPanel(bagLayout);
		fieldContentPanel.setBackground(Color.white);
		int fieldIndex = 0;
		UIHelper.addToBagpanel(
				fieldContentPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("interviewerName") + ":</HTML>"), c,
				0, fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				interviewerField, c, 1, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(
				fieldContentPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("intervieweeName") + ":</HTML>"), c,
				2, fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				intervieweeField, c, 3, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldIndex++;

		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("startDate") + ":</HTML>"), c, 0,
				fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, startDateField,
				c, 1, fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				"<HTML>" + mxResources.get("endDate") + ":</HTML>"), c, 2,
				fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, endDateField, c,
				3, fieldIndex, 1, 1, GridBagConstraints.WEST);
		fieldIndex++;
		panel.add(fieldContentPanel);

		return panel;
	}

	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		String sampleNum = sampleNumField.getText();
		String serialNumF = serialNumField.getText();
		String interviewer = interviewerField.getText();
		String interviewee = intervieweeField.getText();

		if ("".equals(sampleNum) || "".equals(serialNumF)
				|| "".equals(interviewer) || "".equals(interviewee)) {
			JOptionPane.showMessageDialog(
					container,
					"<HTML><font color='red'>(*)</font>"
							+ mxResources.get("missInformation") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	@Override
	public void afterSubmit() {
		// TODO Auto-generated method stub
		super.afterSubmit();
		km.ivMainPanel = new IvMainPanel(km);
		km.setMainPanel(km.ivMainPanel);
		//
		String op = (String) params.get("op");
		if ("add".equals(op)) {
			CalTimer.getInstance().start();
			km.ivMainPanel.startBtn.setIcon(UIHelper.getImage("pauseBtn.png"));
			km.ivMainPanel.startBtn.updateUI();
			Question question = qservice
					.searchQuestion(Constants.QUESTION_DRAW);
			QnTreeNode node = km.ivMainPanel.collAndSelectTreeNode(question);
			km.ivMainPanel.clickTree(node);
		}
	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		String op = (String) params.get("op");

		serialNumField.setEditable(false);

		if ("edit".equals(op)) {
			KassService kassService = new KassService();
			KASS kass = new KASS();
			kass = kassService.get();
			KFieldSite fieldSite = new KFieldSite();
			fieldSite = kass.getFieldSite();
			ProjectTitle.setText(fieldSite.getProjectTitle());
			ProjectDirector.setText(fieldSite.getProjectDirector());
			Country.setText(fieldSite.getCountry());
			Province.setText(fieldSite.getProvince());
			District.setText(fieldSite.getDistrict());
			County.setText(fieldSite.getCounty());
			Town.setText(fieldSite.getTown());
			Village.setText(fieldSite.getVillage());
			PlaceName.setText(fieldSite.getPlaceName());
			KInterviewInfo interviewInfo = new KInterviewInfo();
			interviewInfo = kass.getInterviewInfo();
			sampleNumField.setText(interviewInfo.getSampleNumber());
			serialNumField.setText(interviewInfo.getSerialNumber());
			interviewerField.setText(interviewInfo.getInterviewer());
			intervieweeField.setText(interviewInfo.getInterviewee());
			startDateField.setText(interviewInfo.getStartDate());
			endDateField.setText(interviewInfo.getEndDate());
			intervieweeField.setEditable(false);
			sampleNumField.setEditable(false);
		} else {
			Project project = Constants.PROJECT;

			ProjectTitle.setText(project.getTitle());
			ProjectDirector.setText(project.getDirectory());
			Country.setText(project.getCountry());
			Province.setText(project.getProvince());
			District.setText(project.getDistrict());
			County.setText(project.getCounty());
			Town.setText(project.getTown());
			Village.setText(project.getVillage());
			PlaceName.setText(project.getPlaceName());

			Document dt1 = intervieweeField.getDocument();
			Document dt2 = sampleNumField.getDocument();
			final String placename = project.getPlaceName();
			serialNumField.setText(placename);
			dt1.addDocumentListener(new javax.swing.event.DocumentListener() {
				public void insertUpdate(DocumentEvent e) {
					System.out.println("insertUpdate");
					Document dt = e.getDocument();
					try {
						String s = dt.getText(0, dt.getLength());
						serialNumField.setText(placename + "-"
								+ sampleNumField.getText() + s);
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

				public void removeUpdate(DocumentEvent e) {
					System.out.println("removeUpdate");
					Document dt = e.getDocument();
					try {
						String s = dt.getText(0, dt.getLength());
						serialNumField.setText(placename + "-"
								+ sampleNumField.getText() + s);
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				public void changedUpdate(DocumentEvent e) {
					System.out.println("changedUpdate");
				}

			});
			dt2.addDocumentListener(new javax.swing.event.DocumentListener() {
				public void insertUpdate(DocumentEvent e) {
					System.out.println("insertUpdate");
					Document dt = e.getDocument();
					try {
						String s = dt.getText(0, dt.getLength());
						serialNumField.setText(placename + "-" + s
								+ intervieweeField.getText());
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

				public void removeUpdate(DocumentEvent e) {
					System.out.println("removeUpdate");
					Document dt = e.getDocument();
					try {
						String s = dt.getText(0, dt.getLength());
						serialNumField.setText(placename + "-" + s
								+ intervieweeField.getText());
					} catch (BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				public void changedUpdate(DocumentEvent e) {
					System.out.println("changedUpdate");
				}

			});
			// serialNumField.setText(fieldSite.getPlaceName());
		}
	}

	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		String sampleNum = sampleNumField.getText();
		String serialNum = serialNumField.getText();
		String interviewer = interviewerField.getText();
		String interviewee = intervieweeField.getText();
		KInterviewInfo interviewInfo = null;
		KASS kass = null;
		KFieldSite fieldSite = null;
		if ("add".equals(params.get("op"))) {
			interviewInfo = new KInterviewInfo();
			kass = new KASS();
			fieldSite = new KFieldSite();
			String startDate = DateUtil.format(new Date(),
					"yyyy-MM-dd HH:mm:ss");
			interviewInfo.setStartDate(startDate);
		} else {
			interviewInfo = service.get().getInterviewInfo();
			kass = service.get();
			fieldSite = service.get().getFieldSite();
		}
		interviewInfo.setSampleNumber(sampleNum);
		interviewInfo.setSerialNumber(serialNum);
		interviewInfo.setInterviewer(interviewer);
		interviewInfo.setInterviewee(interviewee);
		interviewInfo.setProgress(0);
		interviewInfo.setVersion(Constants.VERSION);

		Project project = Constants.PROJECT;

		// if getfieldSite is Null, setFieldSite; else don't setFieldSite
		fieldSite.setProjectTitle(ProjectTitle.getText());
		fieldSite.setProjectDirector(ProjectDirector.getText());
		fieldSite.setCountry(Country.getText());
		fieldSite.setProvince(Province.getText());
		fieldSite.setDistrict(District.getText());
		fieldSite.setCounty(County.getText());
		fieldSite.setTown(Town.getText());
		fieldSite.setVillage(Village.getText());
		fieldSite.setPlaceName(PlaceName.getText());

		kass.setFieldSite(fieldSite);

		String QuestionnaireDirectory = project.getQuestionnaireDirectory();
		File tempFile = new File(QuestionnaireDirectory.trim());
		String fileName = tempFile.getName();
		interviewInfo.setQVersion(fileName);
		kass.setInterviewInfo(interviewInfo);
		kass.setLanguage(project.getLan());

		Constants.KASS = kass;
		Constants.QUESTIONNAIRE_XML = project.getQuestionnaireDirectory();
		Constants.QUESTIONNAIRE = qservice.get();

		if ("add".equals(params.get("op"))) {
			String kassFile = project.getDirectory() + File.separator
					+ serialNum + Constants.TMP_EXT;
			String kassFile1 = project.getDirectory() + File.separator
					+ serialNum + Constants.XML_EXT;
			Constants.KASS_XML = kassFile;

			InitUtil.saveDefaultComplete(qservice, service);

			service.saveFile(kass, kassFile);
			FileUtil.copySingleFile(kassFile, kassFile1);

			Preferences pref = Preferences.userRoot().node("interview");
			int result = 0;
			try {
				for (int j = 0; j < pref.keys().length + 1; j++) {
					// An interview file has been opened.
					if (kassFile.equals(pref.get(String.valueOf(j), ""))) {
						String tempPath = pref.get(String.valueOf(j), "");
						for (int k = j; k > 1; k--) {
							pref.put(String.valueOf(k),
									pref.get(String.valueOf(k - 1), ""));
						}
						pref.put(String.valueOf(1), tempPath);
						result = 1;
						break;
					}
				}
			} catch (BackingStoreException e1) {
				e1.printStackTrace();
			}
			int i;
			if (result == 0) {
				try {
					// The number of files in the list of the recent interview
					if (pref.keys().length < 10) {
						for (i = pref.keys().length + 1; i > 1; i--) {
							pref.put(String.valueOf(i),
									pref.get(String.valueOf(i - 1), ""));
						}
						pref.put(String.valueOf(1), kassFile1);
					} else {
						for (i = pref.keys().length; i > 1; i--) {
							pref.put(String.valueOf(i),
									pref.get(String.valueOf(i - 1), ""));
						}
						pref.put(String.valueOf(1), kassFile1);
					}
				} catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				// service.save();
			}
			try {
				String path = Constants.getUserLogPath() + File.separator
						+ "interviewLog.xml";
				FileOutputStream fos = new FileOutputStream(path);
				pref.exportSubtree(fos);
			} catch (Exception e1) {
				System.err.println(" Cannot export nodes:  " + e1);
			}
		}
		return new MessageModel(true, "Submit success!");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object o = e.getSource();

	}
}
