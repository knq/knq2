package com.fangda.kass.ui.interview.graph.action;

import java.awt.event.ActionEvent;

import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.CustomGraph;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.mxgraph.util.mxUndoManager;

/**
 * The Class for undoing a step of drawing NKK diagram.
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomUndoAction extends CustomBaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9025351313120924863L;

	@Override
	public void action(ActionEvent e) {
		CustomGraph mxGraph = (CustomGraph)super.getGraph(e);
		GraphPanel graphPanel = mxGraph.graphPanel;
		mxUndoManager undoManager = graphPanel.getUndoManager();
		undoManager.undo();
	}

}
