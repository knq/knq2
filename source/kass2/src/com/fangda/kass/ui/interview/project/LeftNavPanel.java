package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.interview.IvMainPanel;

/**
 * This class for the questions navigation tree in the left windows in the IMS
 * (interview management system)
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class LeftNavPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3068808666830201204L;
	public QuestionnaireService questionnaireService;
	public IvMainPanel ivMainPanel;

	public LeftNavPanel(QuestionnaireService questionnaireService) {
		this.questionnaireService = questionnaireService;
	}

	public void paint(Graphics g) {
		super.paint(g);
		ImageIcon navImg = UIHelper.getImage("leftNavhead.jpg");
		g.drawImage(navImg.getImage(), 5, 5, null);
		drawPersonInfo(g);
	}

	public void drawPersonInfo(Graphics g) {
		Font f = new Font("宋体", Font.BOLD, 12);
		g.setFont(f);
		g.drawString("21%", 12, 25);

		g.setColor(Color.black);
		g.drawLine(5, 40, 37, 40);
		int sx = 20, sy = 40, ex = 20, ey = 48;
		int index = 0;
		questionnaireService.getQuestionnaire();
		List<Subsection> subsections = null;
		subsections = questionnaireService.get().getSection().getSubsections();
		for (Subsection subsection : subsections) {
			List<Question> questions = subsection.getQuestions();
			for (Question question : questions) {
				String qid = question.getQid();

			}
		}

		g.drawLine(20, 40, 20, 48);
		g.setColor(Color.gray);
		// g.drawRect(5, 41, 32, 43);
		g.fillRect(21, 41, 17, 8);

		g.setColor(Color.black);
		g.drawLine(20, 48, 27, 48);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame jframe = new JFrame();
		jframe.setSize(800, 600);
		QuestionnaireService aireService = new QuestionnaireService();
		LeftNavPanel lp = new LeftNavPanel(aireService);
		jframe.add(lp);

		jframe.setVisible(true);

	}

}
