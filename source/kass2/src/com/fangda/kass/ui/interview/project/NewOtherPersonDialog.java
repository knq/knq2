/**
 * 
 */
package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.JRadioGroup;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.support.QuestionMap;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.RegularUtil;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of the DCR question in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class NewOtherPersonDialog extends BaseDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -583142353685062847L;
	public KassService kassService;
	public QuestionnaireService qService;
	public IvMainPanel ivMainPanel;
	private QuestionMap qm;
	private LinkedHashMap<String, Object> groupMap;
	private int sex = -1;

	public NewOtherPersonDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {
		super.initDialog();
		kassService = new KassService();
		qService = new QuestionnaireService();
		super.setTitle(mxResources.get("addNewPerson"));
		qm = new QuestionMap();
		groupMap = new LinkedHashMap<String, Object>();
	}

	@Override
	public JPanel createPanel() {
		JPanel jpanel = new JPanel();
		jpanel.setBackground(Color.white);
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		GridBagConstraints c = new GridBagConstraints();

		int index = 0;
		JPanel questionPanel = createQuestionPanel();
		UIHelper.addToBagpanel(panel, Color.white, questionPanel, c, 0, index,
				1, 1, GridBagConstraints.WEST);

		jpanel.add(panel);
		return jpanel;
	}

	private JPanel createQuestionPanel() {
		String leadQid = Constants.QUESTION_GENERAL;

		int width = 300;
		Question qst = qService.searchQuestion(leadQid);
		JPanel boxPanel = new JPanel();
		boxPanel.setLayout(new VFlowLayout());
		boxPanel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();
		qm.removeAll();
		if (qst.getFollowupQuestions() != null
				&& qst.getFollowupQuestions().size() > 0) {
			for (int i = 0; i < qst.getFollowupQuestions().size(); i++) {
				FollowupQuestion fquestion = qst.getFollowupQuestions().get(i);
				GridBagLayout bagLayout1 = new GridBagLayout();
				JPanel followupPanel = new JPanel(bagLayout1);
				followupPanel.setBackground(Color.white);
				int index = 0;
				if (StringUtils.isNotBlank(fquestion.getGroupName())) {
					String key = qst.getQid() + "_" + fquestion.getGroupName();
					JPanel jpanel = (JPanel) groupMap.get(key);
					if (jpanel == null) {
						jpanel = new JPanel();
						jpanel.setBackground(Color.white);
						jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));
						jpanel.setBorder(BorderFactory
								.createTitledBorder(fquestion.getGroupName()));
						jpanel.setName("group_" + fquestion.getGroupName());
						groupMap.put(key, jpanel);
						UIHelper.addToBagpanel(followupPanel, Color.white,
								jpanel, c, 0, index, 1, 1,
								GridBagConstraints.WEST);
						index++;
					}
					JPanel qPanel = new JPanel();
					qPanel.setBackground(Color.white);

					qPanel.setLayout(new BoxLayout(qPanel, BoxLayout.Y_AXIS));
					qPanel.setName("question_" + fquestion.getQid());
					qPanel.setBorder(BorderFactory.createEmptyBorder());
					// qPanel.setBorder(BorderFactory.createTitledBorder(fquestion.getQid()));
					if (StringUtil.isNotBlank(fquestion.getDetail())) {
						JLabel titleLabel = new JLabel(""
								+ fquestion.getDetail());
						titleLabel.setPreferredSize(new Dimension(width - 80,
								30));
						qPanel.add(titleLabel);
					}
					this.addPanelOption(qst, qPanel, fquestion, width - 60);
					jpanel.add(qPanel);
				} else {
					followupPanel.setName("question_" + fquestion.getQid());
					String t = fquestion.getDetail();
					if (StringUtils.isNotBlank(t)) {
						JLabel detailLabel = new JLabel(t);
						UIHelper.createMultiLabel(detailLabel, Font.NORMAL, 13,
								width - 60);
						UIHelper.addToBagpanel(followupPanel, Color.white,
								detailLabel, c, 0, index, 1, 1,
								GridBagConstraints.WEST);
						index++;
					}
					index = addOption(qst, followupPanel, fquestion,
							width - 60, c, index);
					index++;
				}
				boxPanel.add(followupPanel);
			}
		}
		return boxPanel;
	}

	public int addOption(Question qst, JPanel panel, Question question,
			int width, GridBagConstraints c, int index) {

		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField();
			field.setName(question.getQid());
			field.setPreferredSize(new Dimension(width, 30));
			UIHelper.addToBagpanel(panel, Color.white, field, c, 0, index, 1,
					1, GridBagConstraints.WEST);
			qm.addQuestion(question, field);
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setName(question.getQid());
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			notePane.setPreferredSize(new Dimension(width, 60));
			UIHelper.addToBagpanel(panel, Color.white, notePane, c, 0, index,
					1, 1, GridBagConstraints.WEST);
			index++;
			qm.addQuestion(question, noteField);
		} else if ("B".equals(question.getType())
				|| "D".equals(question.getType())) {//
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					JCheckBox cb = new JCheckBox();
					cb.setName(option.getOid());
					cb.setBackground(Color.white);
					cb.setText("<html>" + option.getDetail() + "</html>");
					cb.setPreferredSize(new Dimension(width, 30));
					UIHelper.addToBagpanel(panel, Color.white, cb, c, 0, index,
							1, 1, GridBagConstraints.WEST);
					index++;
					qm.addQuestion(question, cb);
				}
			}
		} else if ("C".equals(question.getType())
				|| "Q".equals(question.getType())) {
			if (question.getOptions() != null) {
				JRadioGroup bg = new JRadioGroup();
				for (Option option : question.getOptions()) {
					JRadioButton rb = new JRadioButton();
					rb.setActionCommand(option.getValue());
					rb.setName(option.getOid());
					rb.setBackground(Color.white);
					rb.setText("<html>" + option.getDetail() + "</html>");
					rb.setPreferredSize(new Dimension(width, 30));
					UIHelper.addToBagpanel(panel, Color.white, rb, c, 0, index,
							1, 1, GridBagConstraints.WEST);
					index++;
					bg.add(rb);
				}
				qm.addQuestion(question, bg);
			}
		}
		return index;
	}

	public void addPanelOption(Question qst, JPanel panel,
			final Question question, int width) {
		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField();
			field.setName(question.getQid());
			field.setPreferredSize(new Dimension(15, 30));
			panel.add(field);
			qm.addQuestion(question, field);
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setName(question.getQid());
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(notePane);
			qm.addQuestion(question, noteField);
		} else if ("B".equals(question.getType())
				|| "D".equals(question.getType())) {//
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					JCheckBox cb = new JCheckBox();
					cb.setName(option.getOid());
					cb.setBackground(Color.white);
					cb.setText("<html>" + option.getDetail() + "</html>");
					cb.setPreferredSize(new Dimension(width - 20, 30));
					if (RegularUtil.isPersonOrUnion(qst)) {
						cb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
							}
						});
					}
					panel.add(cb);
					qm.addQuestion(question, cb);
				}
			}
		} else if ("C".equals(question.getType())
				|| "Q".equals(question.getType())) {
			if (question.getOptions() != null) {
				JRadioGroup bg = new JRadioGroup();
				bg.setName(question.getLabel());
				int i = 0;
				for (Option option : question.getOptions()) {
					if (option.getLayout() != null
							&& option.getLayout().equals("R")) {
						panel.setLayout(new BoxLayout(panel,
								BoxLayout.LINE_AXIS));
					}
					JRadioButton rb = new JRadioButton();
					rb.setBackground(Color.white);
					rb.setText("<html>" + option.getDetail() + "</html>");
					rb.setActionCommand(option.getValue());
					rb.setName(option.getOid());
					if (RegularUtil.isPersonOrUnion(qst)) {
						rb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JRadioButton jb = (JRadioButton) e.getSource();
								if (Constants.QUESTION_SEX_MALE.equals(jb
										.getName())) {
									sex = 0;
								} else if (Constants.QUESTION_SEX_FEMALE
										.equals(jb.getName())) {
									sex = 1;
								}
							}
						});
					}
					panel.add(rb);
					bg.add(rb);
					i++;
				}
				qm.addQuestion(question, bg);
			}
		}
	}

	@Override
	public boolean beforeValid() {
		boolean flag = IvMainPanel.IVPANEL.checkSameName(-1, sex, qm, null);
		return flag;
	}

	@Override
	public MessageModel submit() {
		// Add a new person in the canvas
		int pid = IvMainPanel.IVPANEL.getNextPersonId();

		double[] xy = getXY();
		int x = (int) xy[0];
		int y = (int) xy[1];
		mxCell cell = IvMainPanel.IVPANEL.ographPanel.addNewPerson(pid, x, y,
				sex, false);
		IvMainPanel.IVPANEL.ographPanel.graphComponent.getGraph().refresh();
		qm.savePersonValue(IvMainPanel.IVPANEL.ographPanel, cell, qService,
				kassService, null, null);
		// Setting the color
		changeStyle(pid, sex, cell);
		GraphPerson gp = (GraphPerson) cell.getValue();
		gp.setName(kassService.getPersonName(gp.getId()));
		gp.setBirthDay(kassService.getPersonBirthYear(gp.getId()));
		gp.setSex(sex);
		IvMainPanel.IVPANEL.ographPanel.graphComponent.getGraph().refresh();
		IvMainPanel.IVPANEL.addANewPerosnIOIFinish(pid + "");
		return new MessageModel(true, "Submit success!");
	}

	public int cal(int x, int y) {
		mxCell mxCell = IvMainPanel.IVPANEL.ographPanel.getCellByXY(x, y);
		if (mxCell != null) {
			int x1 = x + 60;
			return cal(x1, y);
		} else {
			return x;
		}
	}

	public void changeStyle(int pid, int sex, mxCell cell) {
		KPPerson person = kassService.getPerson(pid);
		int isLiving = IvMainPanel.IVPANEL.ographPanel.isLiving(person);
		if (person.getSex() == 0) {
			if (isLiving == 2) {
				cell.setStyle("maleO");
			} else {
				cell.setStyle("maleDeathO");
			}
		} else {
			if (isLiving == 2) {
				cell.setStyle("femaleO");
			} else {
				cell.setStyle("femaleDeathO");
			}
		}
		IvMainPanel.IVPANEL.ographPanel.graphComponent.getGraph().refresh();
	}

	public double[] getXY() {
		double maxx = 0;
		double maxy = 0;
		mxGraphComponent graphComponent = IvMainPanel.IVPANEL.ographPanel.graphComponent;
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (maxx < child.getGeometry().getX()) {
					maxx = child.getGeometry().getX();
				}
				if (maxy < child.getGeometry().getY()) {
					maxy = child.getGeometry().getY();
				}
			}
		}

		if (maxy == 0) {
			maxx = 500;
			maxy = 500;
		} else {
			maxy = maxy + 100;
		}
		return new double[] { maxx, maxy };
	}
}
