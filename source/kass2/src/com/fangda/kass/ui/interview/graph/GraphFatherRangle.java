package com.fangda.kass.ui.interview.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.interview.PageConfig;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.interview.graph.GraphAutoRangle.PersonG;
import com.fangda.kass.ui.interview.graph.GraphAutoRangle.UnionG;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DPerson;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DUnion;

/**
 * The Class for the auto arrange of the unions and their members on the left of
 * the NKK diagram (the network kinship diagram).
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class GraphFatherRangle {

	/**
	 * The set of unions on the left of the NKK diagram (the network kinship
	 * diagram).
	 */
	private TreeMap<Integer, List<UnionG>> ufs = null;

	/**
	 * The set of arranged unions on the left of the NKK diagram (the network
	 * kinship diagram).
	 */
	private TreeMap<Integer, List<UnionG>> aleadyUfs = null;

	/**
	 * The set of all person on the left of the NKK diagram (the network kinship
	 * diagram).
	 */
	private TreeMap<Integer, List<PersonG>> pfs = null;

	/**
	 * The set of arranged all person on the left of the NKK diagram (the
	 * network kinship diagram).
	 */
	private TreeMap<Integer, List<PersonG>> aleadyPfs = null;

	private GraphAutoRangle range;
	private KassService kassService;
	private GraphMotherRangle gmr;

	private Double x0 = 600d;
	private Double p = 100d;
	private Double h = 100d;

	public GraphFatherRangle(GraphAutoRangle range, KassService kassService,
			GraphMotherRangle gmr) {
		this.range = range;
		this.kassService = kassService;
		this.gmr = gmr;
		this.x0 = this.range.getX0();
		PageConfig pageConfig = kassService.get().getPageConfig();
		this.p = (double) pageConfig.getCellWith();
		this.h = (double) pageConfig.getCellHeight();
	}

	public void initRangle() {
		this.init();
		this.fillData();
	}

	public void range1(int g, int s, int m) {
		List<UnionG> gs = ufs.get(g);
		if (gs != null && gs.size() > 0) {
			UnionG g0 = gs.get(0);
			KUUnion egoParentUnion = kassService.getPersonUnion(0, "offspring");
			if (egoParentUnion.getId() == g0.getUnion().getId()) {// ego父母Union\
				arrangeEgoParentU(g0, g, s, m);
			}
			this.arrangeUfOffspringU(s, m, g);
			this.arrangeUfOther(s, m, g);
			// System.out.println("-----f-m-: "+m+"-------");
			// printCoor() ;
		}
	}

	private void arrangeUfOther(int s, int m, int g) {
		List<UnionG> unions = this.ufs.get(g);
		while (unions.size() > 0) {
			UnionG u0 = unions.get(0);
			int n = -1;
			List<UnionG> alreadyUnions = this.aleadyUfs.get(g);
			if (alreadyUnions == null || alreadyUnions.size() == 0) {
				n = 0;
			} else {
				// Sort the unions when these unions are the same kinship
				// distance from ego and calculate the position number: n
				List<UnionG> unionGs = new ArrayList<UnionG>();
				for (int i = 0; i < alreadyUnions.size(); i++) {
					UnionG ug0 = alreadyUnions.get(i);
					if (ug0.getDis() == u0.getDis()) {
						unionGs.add(ug0);
					}
				}
				if (unionGs.size() == 0) {
					for (int i = 0; i < alreadyUnions.size(); i++) {
						UnionG ug0 = alreadyUnions.get(i);
						if (u0.getDis() < ug0.getDis()) {
							n = i;
							break;
						} else {
							if (i == alreadyUnions.size() - 1) {
								n = i + 1;
								break;
							}
						}
					}
				} else if (unionGs.size() > 0) {
					for (int i = 0; i < unionGs.size(); i++) {
						UnionG unionG = unionGs.get(i);
						Object[] objs = getEffectiveUnion(unionG, u0);
						int index = this.getIndexFromAlreadyUfs(g, unionG
								.getUnion().getId());
						if (objs == null) {
							String firstUid = u0.getPath()[0];
							KUUMember ku = kassService.getUnionMember(
									Integer.parseInt(firstUid), 0);
							if ("partner".equals(ku.getType())) {
								n = index + 1;
							} else {
								n = index;
								break;
							}
							break;
						} else {
							KUUnion kuunion = (KUUnion) objs[0];
							Integer kuIndex = (Integer) objs[1];
							Integer kuIndex2 = (Integer) objs[2];
							String pid = u0.getPath()[kuIndex + 1];
							KPPerson p1 = kassService.getPerson(Integer
									.parseInt(pid));
							KUUMember ku = kassService.getUnionMember(
									kuunion.getId(), Integer.parseInt(pid));
							if ("partner".equals(ku.getType())) {
								String pid2 = unionG.getPath()[kuIndex2 + 1];
								KUUMember ku2 = kassService
										.getUnionMember(kuunion.getId(),
												Integer.parseInt(pid2));
								if ("partner".equals(ku2.getType())) {
									if (p1.getSex() == 0) {
										n = index + 1;
									} else {
										n = index;
										break;
									}
									break;
								} else if ("offspring".equals(ku2.getType())) {
									n = index;
									break;
								}
							} else if ("offspring".equals(ku.getType())) {
								String pid2 = unionG.getPath()[kuIndex2 + 1];
								KUUMember ku2 = kassService
										.getUnionMember(kuunion.getId(),
												Integer.parseInt(pid2));
								if ("partner".equals(ku2.getType())) {
									n = index + 1;
								} else if ("offspring".equals(ku2.getType())) {
									String yob2 = kassService
											.getPersonBirthYear(Integer
													.parseInt(pid2));
									String yob1 = kassService
											.getPersonBirthYear(Integer
													.parseInt(pid));
									if (StringUtils.isBlank(yob1)
											|| StringUtils.isBlank(yob2)) {
										if (Integer.parseInt(pid) > Integer
												.parseInt(pid2)) {
											n = index + 1;
										} else {
											n = index;
											break;
										}
									} else {
										if (yob1.compareTo(yob2) > 0) {
											n = index + 1;
										} else {
											n = index;
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			this.addToAlreadyUfs(g, u0, n);
			this.delFromUfs(g, u0);
			if (n == 0) {
				u0.setX(x0 - 2 * p);
				u0.setXl(x0 - 4 * p);
				u0.setXr(x0 + p);
				u0.setY(2 * h * (s - m));
				List<KPPerson> parnters = this.getParnters(u0.getUnion()
						.getId());
				for (int i = 0; i < parnters.size(); i++) {
					KPPerson parnter = parnters.get(i);
					PersonG pg = this
							.getFromAlreadyPfs(g + 1, parnter.getPid());
					if (pg == null) {
						pg = this.getFromPfs(g + 1, parnter.getPid());
						if (pg == null) {
							continue;
						}
						if (parnter.getSex() == 1) {
							this.addToAlreadyPfs(g + 1, pg, 0);
							if (!this.isMRangePerson(g + 1, pg.getPerson()
									.getPid())) {
								pg.setX(x0 - p);
								pg.setY(2 * h * (s - m) - h);
							}
						} else {
							if (i == 0) {
								this.addToAlreadyPfs(g + 1, pg, 0);
								if (!this.isMRangePerson(g + 1, pg.getPerson()
										.getPid())) {
									pg.setX(x0 - 3 * p);
									pg.setY(2 * h * (s - m) - h);
								}
							} else {
								List<PersonG> list = this.getAleadyPfs().get(
										g + 1);
								if (list == null || list.size() == 0) {
									this.addToAlreadyPfs(g + 1, pg, 0);
								} else {
									this.addToAlreadyPfs(g + 1, pg, 1);
								}
								if (!this.isMRangePerson(g + 1, pg.getPerson()
										.getPid())) {
									pg.setX(x0 - 3 * p);
									pg.setY(2 * h * (s - m) - h);
								}
							}
						}
						this.delFromPfs(g + 1, pg);
					}
				}
				this.moveUnionMethod3(u0, s, m);
			} else if (n > 0) {
				UnionG beforeUG = this.getFromAlreadyUfsByIndex(g, n - 1);
				int r = -1;
				List<KPPerson> list = this.getParnters(beforeUG.getUnion()
						.getId());
				for (int i = 0; i < list.size(); i++) {
					Integer pid = list.get(i).getPid();
					int indexTmp = this.getIndexFromAlreadyPfs(g + 1, pid);
					if (indexTmp < 0) {
						list.remove(i);
					}
				}
				if (list.size() == 1) {
					Integer pid = list.get(0).getPid();
					r = this.getIndexFromAlreadyPfs(g + 1, pid);
				} else if (list.size() == 2) {
					Integer pid1 = list.get(0).getPid();
					Integer pid2 = list.get(1).getPid();
					int index1 = this.getIndexFromAlreadyPfs(g + 1, pid1);
					int index2 = this.getIndexFromAlreadyPfs(g + 1, pid2);
					r = Math.max(index1, index2);
				}
				PersonG personG = this.getFromAlreadyPfsByIndex(g + 1, r);
				if (!this.hasParentUnion(personG.getPerson().getPid())) {
					if (personG.getX() > beforeUG.getXl()) {
						this.setUnionCoord6(u0, s, m);
						this.addPartnerMethod4(u0.getUnion().getId(), r, g, s,
								m);
					} else {
						this.setUnionCoord7(u0, personG, s, m);
						this.addPartnerMethod4(u0.getUnion().getId(), r, g, s,
								m);
					}
				}
				if (list.size() == 1
						&& this.hasParentUnion(list.get(0).getPid())) {
					Integer pid = list.get(0).getPid();
					PersonG pg = this.getFromAlreadyPfs(g + 1, pid);
					if (pg != null) {
						Integer uid = this.getParentUnion(pid);// Parent-Union
						int rr = this.getLeftOffspringIndex(uid, g + 1);
						int i = rr + 1;
						while (isChildParnter(g + 1, i, uid)) {
							int childUnionId = getChildParnter(g + 1, i, uid);
							UnionG ug = this.getFromAlreadyUfs(g, childUnionId);
							PersonG p1 = this
									.getFromAlreadyPfsByIndex(g + 1, i);
							if (ug.getX() > beforeUG.getX()
									&& !this.hasParentUnion(p1.getPerson()
											.getPid())) {
								rr = i;
							}
							i = i + 1;
						}
						// If (Pm'[m,r].Xr+P<= Um'[m,n-1].Xr){
						PersonG p1 = this.getFromAlreadyPfsByIndex(g + 1, rr);
						if (p1.getX() - p > beforeUG.getXl()) {
							this.setUnionCoord6(u0, s, m);
							this.addPartnerMethod4(u0.getUnion().getId(), r, g,
									s, m);
						} else {
							this.setUnionCoord7(u0, p1, s, m);
							this.addPartnerMethod4(u0.getUnion().getId(), r, g,
									s, m);
						}
					}
				}
				//
				if (list.size() == 2
						&& this.hasParentUnion(list.get(0).getPid())
						&& this.hasParentUnion(list.get(1).getPid())) {
					Integer pid1 = list.get(0).getPid();
					Integer pid2 = list.get(1).getPid();
					PersonG pg = this.getFromAlreadyPfs(g + 1, pid1);
					if (pg != null) {
						Integer uid1 = this.getParentUnion(pid1);// Parent-Union
						int rr = this.getLeftOffspringIndex(uid1, g + 1);
						int i = rr + 1;
						while (isChildParnter(g + 1, i, uid1)) {
							int childUnionId = getChildParnter(g + 1, i, uid1);
							UnionG ug = this.getFromAlreadyUfs(g, childUnionId);
							PersonG p1 = this
									.getFromAlreadyPfsByIndex(g + 1, i);
							if (ug.getX() > beforeUG.getX()
									&& !this.hasParentUnion(p1.getPerson()
											.getPid())) {
								rr = i;
							}
							i = i + 1;
						}

						// PersonG pg2 = this.getFromAlreadyPms(g, pid2);
						Integer uid2 = this.getParentUnion(pid2);// Parent-Union
						UnionG ug2 = this.getFromAlreadyUfs(g + 1, uid2);
						int rr2 = this.getLeftOffspringIndex(uid2, g + 1);
						// PersonG rightPg = this.getFromAlreadyPmsByIndex(g,
						// rr2);
						// (Um'[m+1,b].X<=Um'[m,n-1].Xr+P){
						if (ug2.getX() >= beforeUG.getXl() - p) {
							// if (getOnlyOneChildUnion(uid2, pid2) == 1) {
							r = rr2;
							if (ug2.getXl() >= beforeUG.getXl()) {
								this.setUnionCoord6(u0, s, m);
								this.addPartnerMethod4(u0.getUnion().getId(),
										rr2, g, s, m);
							} else {
								this.setUnionCoord8(u0, ug2, s, m);
								this.addPartnerMethod4(u0.getUnion().getId(),
										rr2, g, s, m);
							}
							// } else if (getOnlyOneChildUnion(uid2, pid2) == 2)
							// {
							// r = rr;
							// }
						} else {
							r = rr;
						}
						if (r == rr) {
							PersonG p1 = this
									.getFromAlreadyPfsByIndex(g + 1, r);
							if (p1.getX() - p >= beforeUG.getXl()) {
								this.setUnionCoord6(u0, s, m);
								this.addPartnerMethod4(u0.getUnion().getId(),
										r, g, s, m);
							} else {
								this.setUnionCoord7(u0, p1, s, m);
							}
						}
					}
				}
				this.moveUnionMethod3(u0, s, m);
			}
			this.rangeUnionOffspring(u0.getUnion().getId(), g, s, m);
		}
	}

	/**
	 * Arrange the offsprings of a union
	 * 
	 * @param uid
	 * @param g
	 * @param s
	 * @param m
	 */
	private void rangeUnionOffspring(int uid, int g, int s, int m) {
		List<KPPerson> persons = this.getChildren(uid);
		UnionG ug = this.getFromAlreadyUfs(g, uid);
		int n = this.getIndexFromAlreadyUfs(g, uid);
		Collections.sort(persons, new Comparator<KPPerson>() {

			@Override
			public int compare(KPPerson o1, KPPerson o2) {
				String year1 = kassService.getPersonBirthYear(o1.getPid());
				String year2 = kassService.getPersonBirthYear(o2.getPid());
				if (StringUtils.isBlank(year1) || StringUtils.isBlank(year2)) {
					return o1.getPid() - o2.getPid();
				}
				return year1.compareTo(year2);
			}
		});
		if (persons.size() == 1) {
			int pid = persons.get(0).getPid();
			PersonG pg = this.getFromPfs(g, pid);
			if (pg == null) {
				return;
			}
			List<PersonG> gs = this.getAleadyPfs().get(g);
			int k = 0;
			PersonG BP = null;
			if (gs != null) {
				for (int i = 0; i < gs.size(); i++) {
					PersonG p1 = gs.get(i);
					BP = p1;
					// PersonG p2 = gs.get(i + 1);
					if (ug.getX() < p1.getX()) {
						k = i + 1;
					} else {
						break;
					}
				}
			}
			this.addToAlreadyPfs(g, pg, k);
			this.delFromPfs(g, pg);
			if (!this.isMRangePerson(g + 1, pg.getPerson().getPid())) {
				pg.setX(ug.getX());
				pg.setY(2 * h * (s - m) + h);
			}
		} else {
			int k = -1;
			int len = persons.size();
			for (int i = len - 1; i >= 0; i--) {
				int pid = persons.get(i).getPid();
				PersonG pg = this.getFromPfs(g, pid);
				if (pg == null) {
					continue;
				}
				if (i == len - 1) {
					if (n == 0) {
						this.addToAlreadyPfs(g, pg, 0);
						this.delFromPfs(g, pg);
						if (!this.isMRangePerson(g, pg.getPerson().getPid())) {
							pg.setX(ug.getX() + p);
							pg.setY(2 * h * (s - m) + h);
						}
						k = -1;
					} else if (n > 0) {
						List<PersonG> gs = this.getAleadyPfs().get(g);
						if (gs == null) {
							k = -1;
						} else {
							for (int j = 0; j < gs.size(); j++) {
								PersonG p = gs.get(j);
								if (ug.getX() < p.getX()) {
									k = j;
								} else {
									break;
								}
							}
						}
						this.addToAlreadyPfs(g, pg, k + 1);
						this.delFromPfs(g, pg);
						if (!this.isMRangePerson(g, pg.getPerson().getPid())) {
							pg.setX(ug.getX() + p);
							pg.setY(2 * h * (s - m) + h);
						}
					}
				} else {
					this.addToAlreadyPfs(g, pg, k + 1);
					this.delFromPfs(g, pg);
					PersonG beforePG = this.getFromAlreadyPfsByIndex(g, k);
					if (!this.isMRangePerson(g, pg.getPerson().getPid())) {
						pg.setX(beforePG.getX() - p);
						pg.setY(2 * h * (s - m) + h);
					}
				}
				k = k + 1;
			}
			if (persons.size() > 3) {
				PersonG pg = this.getFromAlreadyPfsByIndex(g, k);
				ug.setXl(pg.getX() - p);
				this.moveUnionMethod6(ug, pg, s, m);
			}
		}
	}

	private void arrangeEgoParentU(UnionG g0, int g, int s, int m) {
		UnionG motherG0 = this.gmr.getFromAlreadyUms(g, g0.getUnion().getId());
		if (motherG0 != null) {
			g0.setX(motherG0.getX());
			g0.setY(motherG0.getY());
			g0.setXr(motherG0.getXr());
			g0.setXl(motherG0.getXl());
			this.addToAlreadyUfs(g, g0, 0);
			this.delFromUfs(g, g0);
		}
		// Get ego's father
		List<Integer> pids = kassService.getEgoParent(false);

		if (pids.size() > 0) {
			Integer fatherId = pids.get(0);
			Integer uid = this.getParentUnion(fatherId);
			if (uid == -1) {
				PersonG personG = getFromPfs(g + 1, fatherId);
				if (personG != null) {
					if (!this.isMRangePerson(g + 1, personG.getPerson()
							.getPid())) {
						personG.setX(x0 - p);
						personG.setY(2 * h * (s - m) - h);
					}
					this.addToAlreadyPfs(g + 1, personG, 0);
					this.moveUnionMethod0(m);// Step4.1.1.3.3
					this.delFromPfs(g + 1, personG);
				}
			}
		}
	}

	/**
	 * Arrange the child unions of offsprings in the last arranged generation
	 * (aleadyUfs[g+1])
	 * 
	 * @param s
	 * @param m
	 * @param g
	 */
	private void arrangeUfOffspringU(int s, int m, int g) {
		List<UnionG> lastUnions = this.aleadyUfs.get(g + 1);
		if (lastUnions != null && lastUnions.size() > 0) {
			for (int i = 0; i < lastUnions.size(); i++) {
				UnionG unionG = lastUnions.get(i);
				// 寻找子person
				List<UnionG> up = new ArrayList<UnionG>();
				UnionG minDisUnionG = null;
				int minDis = 10000;
				List<KPPerson> offsprings = this.getChildren(unionG.getUnion()
						.getId());
				int d = offsprings.size();
				List<Integer> offspringIds = new ArrayList<Integer>();
				KUUnion egoParentUnion = kassService.getPersonUnion(0,
						"offspring");
				if (egoParentUnion.getId() == unionG.getUnion().getId()) {
					// the parent Union of ego\
					for (int c = 0; c < d; c++) {
						Integer pid = offsprings.get(c).getPid();
						int index = gmr.getIndexFromAlreadyPms(g + 1, pid);
						if (offspringIds.size() == 0) {
							offspringIds.add(0, pid);
						} else {
							boolean flag = false;
							for (int f = 0; f < offspringIds.size(); f++) {
								int index1 = gmr.getIndexFromAlreadyPms(g + 1,
										offspringIds.get(f));
								if (index > index1) {
									offspringIds.add(f, pid);
									flag = true;
									break;
								}
							}
							if (!flag) {
								offspringIds.add(pid);
							}
						}
					}
				} else {
					for (int c = 0; c < d; c++) {
						Integer pid = offsprings.get(c).getPid();
						int index = this.getIndexFromAlreadyPfs(g + 1, pid);
						if (offspringIds.size() == 0) {
							offspringIds.add(pid);
						} else {
							boolean flag = false;
							for (int f = 0; f < offspringIds.size(); f++) {
								int index1 = this.getIndexFromAlreadyPfs(g + 1,
										offspringIds.get(f));
								if (index < index1) {
									offspringIds.add(f, pid);
									flag = true;
									break;
								}
							}
							if (!flag) {
								offspringIds.add(pid);
							}
						}
					}
				}
				for (int c = 0; c < d; c++) {
					Integer pid = offspringIds.get(c);
					List<KUUnion> list = kassService.getUnionByPid(pid,
							"partner");
					for (KUUnion ku : list) {
						UnionG ug = this.getFromUfs(g, ku.getId());
						if (ug != null) {
							if (ug.getDis() < minDis) {
								minDis = ug.getDis();
								minDisUnionG = ug;
							}
							up.add(ug);
							this.delFromUfs(g, ug);
						}
					}
				}
				if (minDisUnionG != null) {
					up.remove(minDisUnionG);
					up.add(0, minDisUnionG);
				}
				//
				int k = getLeftOffspringIndex(unionG.getUnion().getId(), g + 1);
				if (unionG.getUnion().getId() == egoParentUnion.getId()) {
					k = -1;
				}
				k = k + 1;
				for (int b = 0; b < up.size(); b++) {
					UnionG ug = up.get(b);
					this.addToAlreadyUfs(g, ug, -1);
					List<KUUMember> ms = ug.getUnion().getMembers();
					int personId = 0;
					for (KUUMember member : ms) {
						if (offspringIds.contains(member.getId())) {
							personId = member.getId();
							break;
						}
					}
					PersonG pg = this.getFromAlreadyPfs(g + 1, personId);// s5
					int n = this.getIndexFromAlreadyUfs(g, ug.getUnion()
							.getId());
					if (n == 0) {
						if (pg == null) {
							ug.setX(x0 - 2 * p);
							ug.setY(2 * h * (s - m));
							ug.setXl(ug.getX() - 2 * p);
							ug.setXr(ug.getX() + p);
						} else {
							if (pg.getX() - p >= x0 - 2 * p) {
								ug.setX(x0 - 2 * p);
								ug.setY(2 * h * (s - m));
								ug.setXl(ug.getX() - 2 * p);
								ug.setXr(ug.getX() + p);
							} else {
								this.setUnionCoord9(ug, pg, s, m);
							}
						}
						for (KUUMember member : ms) {
							if (member.getId() != personId) {
								PersonG otherPg = this.getFromPfs(g + 1,
										member.getId());
								if (otherPg != null) {
									this.addToAlreadyPfs(g + 1, otherPg, k);
									this.delFromPfs(g + 1, otherPg);
									if (!this.isMRangePerson(g + 1, otherPg
											.getPerson().getPid())) {
										this.addPartnerMethod5(ug, otherPg, s,
												m);
									}
									if (aleadyPfs.get(g + 1).size() > k) {
										PersonG p = this
												.getFromAlreadyPfsByIndex(
														g + 1, k + 1);
										if (p != null
												&& p.getX() <= otherPg.getX()) {
											this.moveUnionMethod4(ug, otherPg,
													s, m);
										}
									}
									k = k + 1;
								}
							}
						}
					} else if (n > 0) {
						UnionG ug2 = this.getFromAlreadyUfsByIndex(g, n - 1);
						if (pg == null) {
							setUnionCoord6(ug, s, m);
						} else {
							if (pg.getX() - p < ug2.getXl()) {
								setUnionCoord9(ug, pg, s, m);
							} else {
								setUnionCoord6(ug, s, m);
							}
						}
						for (KUUMember member : ms) {
							if (member.getId() != personId) {
								PersonG otherPg = this.getFromPfs(g + 1,
										member.getId());
								if (otherPg != null) {
									this.addToAlreadyPfs(g + 1, otherPg, k);
									this.delFromPfs(g + 1, otherPg);
									this.addPartnerMethod6(ug, otherPg, s, m);
									PersonG ppg = this
											.getFromAlreadyPfsByIndex(g + 1,
													k + 1);
									if (ppg != null
											&& ppg.getX() >= otherPg.getX()) {
										this.moveUnionMethod4(ug, otherPg, s, m);
									}
									k = k + 1;
								}
							}
						}
					}
					this.rangeUnionOffspring(ug.getUnion().getId(), g, s, m);
				}
			}
		}
	}

	private void moveUnionMethod0(int m) {
		int a = this.range.getMaxG();
		int b = this.range.getMinG();
		int s = a - b + 1;
		for (Entry<Integer, List<UnionG>> entry : this.aleadyUfs.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);// 根据代数求m行号
			if (i > m) {
				List<UnionG> list = entry.getValue();
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					thisUG.setX(thisUG.getX() - p);
					for (PersonG pg : thisUG.getChildrenPersons()) {
						PersonG personG = this.getFromAlreadyPfs(g + 1, pg
								.getPerson().getPid());
						if (personG != null) {
							if (!this.isMRangePerson(g + 1, personG.getPerson()
									.getPid())) {
								personG.setX(personG.getX() - p);
							}
						}
					}
					for (PersonG pg : thisUG.getParentPersons()) {
						List<KUUnion> unions = kassService.getUnionByPid(pg
								.getPerson().getPid(), "offspring");
						if (unions == null || unions.size() == 0) {
							PersonG personG = this.getFromAlreadyPfs(g + 1, pg
									.getPerson().getPid());
							if (personG != null) {
								if (!this.isMRangePerson(g + 1, personG
										.getPerson().getPid())) {
									personG.setX(personG.getX() - p);
								}
							}
						}
					}
				}
			}
		}
	}

	private void init() {
		ufs = new TreeMap<Integer, List<UnionG>>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		aleadyUfs = new TreeMap<Integer, List<UnionG>>(
				new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
		pfs = new TreeMap<Integer, List<PersonG>>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		aleadyPfs = new TreeMap<Integer, List<PersonG>>(
				new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
	}

	private void fillData() {
		List<DUnion> dunions = range.getDunions();
		for (DUnion union : dunions) {
			boolean isFather = range.isFatherUnion(union);
			if (isFather) {
				List<UnionG> list = this.ufs.get(union.getG());
				if (list == null) {
					list = new ArrayList<UnionG>();
					this.ufs.put(union.getG(), list);
				}
				UnionG ug = new UnionG();
				ug.setDis(union.getDis());
				ug.setG(union.getG());
				ug.setUnion(kassService.getUnion(union.getUid()));
				ug.setPath(union.getPath());
				list.add(ug);
			}
		}
		// Sorting
		for (Entry<Integer, List<UnionG>> entry : ufs.entrySet()) {
			List<UnionG> list = entry.getValue();
			Collections.sort(list, new Comparator<UnionG>() {

				@Override
				public int compare(UnionG o1, UnionG o2) {
					if (o1.getDis() > o2.getDis()) {
						return 1;
					} else if (o1.getDis() < o2.getDis()) {
						return -1;
					} else if (o1.getDis() == o2.getDis()) {
						return o1.getUnion().getId() - o2.getUnion().getId();
					}
					return 0;
				}

			});
		}
		// Add Person
		for (Entry<Integer, List<UnionG>> entry : ufs.entrySet()) {
			List<UnionG> list = entry.getValue();
			for (UnionG ug : list) {
				KUUnion union = ug.getUnion();
				List<KUUMember> members = union.getMembers();
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						DPerson dperson = range.getDPerson(member.getId());
						PersonG pg = this.getFromPfs(dperson.getG(),
								member.getId());
						if (pg == null) {
							pg = new PersonG();
							pg.setDis(dperson.getDis());
							pg.setG(dperson.getG());
							pg.setPerson(kassService.getPerson(dperson.getPid()));
							ug.getParentPersons().add(pg);
							addToPfs(pg);
						}

					} else {
						DPerson dperson = range.getDPerson(member.getId());
						PersonG pg = this.getFromPfs(dperson.getG(),
								member.getId());
						if (pg == null) {
							pg = new PersonG();
							pg.setDis(dperson.getDis());
							pg.setG(dperson.getG());
							pg.setPerson(kassService.getPerson(dperson.getPid()));
							ug.getChildrenPersons().add(pg);
							pg.getUpUnions().add(ug);
							addToPfs(pg);
						}
					}
				}
			}
		}
	}

	private void setUnionCoord6(UnionG u, int s, int m) {
		int index = this.getIndexFromAlreadyUfs(u.getG(), u.getUnion().getId());
		UnionG beforeUnion = this.getFromAlreadyUfsByIndex(u.getG(), index - 1);
		u.setX(beforeUnion.getXl() - p);
		u.setXl(u.getX() - 2 * p);
		u.setXr(u.getX() + p);
		u.setY(2 * h * (s - m));
	}

	private void setUnionCoord7(UnionG u, PersonG personG, int s, int m) {
		u.setX(personG.getX() - 2 * p);
		u.setXl(u.getX() - 2 * p);
		u.setXr(u.getX() + p);
		u.setY(2 * h * (s - m));
	}

	private void setUnionCoord8(UnionG u1, UnionG u2, int s, int m) {
		u1.setX(u2.getXl() - p);
		u1.setXl(u1.getX() - 2 * p);
		u1.setXr(u1.getX() + p);
		u1.setY(2 * h * (s - m));
	}

	private void setUnionCoord9(UnionG ug, PersonG pg, int s, int m) {
		ug.setX(pg.getX() - p);
		ug.setXl(ug.getX() - 2 * p);
		ug.setXr(ug.getX() + p);
		ug.setY(2 * h * (s - m));

	}

	private void addPartnerMethod4(int uid, int r, int g, int s, int m) {
		List<KPPerson> list = this.getParnters(uid);
		UnionG ug = this.getFromAlreadyUfs(g, uid);
		for (KPPerson person : list) {
			PersonG pg = this.getFromAlreadyPfs(g + 1, person.getPid());
			if (pg == null) {
				pg = this.getFromPfs(g + 1, person.getPid());
				if (person.getSex() == 0) {
					this.addToAlreadyPfs(g + 1, pg, r + 1);
					if (!this.isMRangePerson(g + 1, pg.getPerson().getPid())) {
						pg.setX(ug.getX() - p);
						pg.setY(2 * h * (s - m) - h);
					}
					r = r + 1;
				} else {
					this.addToAlreadyPfs(g + 1, pg, r + 1);
					if (!this.isMRangePerson(g + 1, pg.getPerson().getPid())) {
						pg.setX(ug.getX() + p);
						pg.setY(2 * h * (s - m) - h);
					}
				}
				this.delFromPfs(g + 1, pg);
			}
		}
	}

	private void addPartnerMethod5(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPfs(personG.getG(), personG.getPerson()
				.getPid());
		if (k != 0) {
			PersonG pg = getFromAlreadyPfsByIndex(personG.getG(), k - 1);
			if (u.getX() - p >= pg.getX()) {
				personG.setX(pg.getX() - p);
			} else {
				personG.setX(u.getX() - p);
			}
		} else {
			personG.setX(u.getX() - p);
		}
		personG.setY(2 * h * (s - m) - h);
	}

	private void addPartnerMethod6(UnionG u, PersonG personG, int s, int m) {
		int n = this.getIndexFromAlreadyUfs(u.getG(), u.getUnion().getId());
		UnionG beforeU = this.getFromAlreadyUfsByIndex(u.getG(), n - 1);

		int k = this.getIndexFromAlreadyPfs(personG.getG(), personG.getPerson()
				.getPid());
		PersonG beforeP = this.getFromAlreadyPfsByIndex(personG.getG(), k - 1);
		if (beforeU.getXl() <= beforeP.getX() - p) {
			personG.setX(u.getX() - p);
		} else {
			if (beforeP.getX() - p < u.getX()) {
				personG.setX(beforeP.getX() - p);
			} else {
				personG.setX(u.getX() - p);
			}
		}
		personG.setY(2 * h * (s - m) - h);

	}

	private void moveUnionMethod3(UnionG u, int s, int m) {
		int a = Math.abs(this.range.getMinG()) + 1;
		for (int i = s; i >= m; i--) {
			int g = i - a;// Calculate the generation value from the generation
							// row number
			List<UnionG> list = this.aleadyUfs.get(g);
			if (list == null) {
				continue;
			}
			for (int j = 0; j < list.size(); j++) {
				UnionG thisUG = list.get(j);
				KUUnion egoParentUnion = kassService.getPersonUnion(0,
						"offspring");
				if (thisUG.getXr() <= u.getXr()
						&& thisUG.getUnion().getId() != egoParentUnion.getId()
						&& thisUG.getUnion().getId() != u.getUnion().getId()) {
					thisUG.setX(thisUG.getX() - 3 * p);
					thisUG.setXl(thisUG.getXl() - 3 * p);
					thisUG.setXr(thisUG.getXr() - 3 * p);
					for (PersonG pg : thisUG.getChildrenPersons()) {
						if (!this
								.isMRangePerson(g + 1, pg.getPerson().getPid())) {
							pg.setX(pg.getX() - 3 * p);
						}
					}
					for (PersonG pg : thisUG.getParentPersons()) {
						List<KUUnion> unions = kassService.getUnionByPid(pg
								.getPerson().getPid(), "offspring");
						if (unions == null || unions.size() == 0) {
							if (!this.isMRangePerson(g + 1, pg.getPerson()
									.getPid())) {
								pg.setX(pg.getX() - 3 * p);
							}
						}
					}
				}
			}
		}
		int cg = m - a;
		int n = getIndexFromAlreadyUfs(cg, u.getUnion().getId());
		List<UnionG> cUnions = this.aleadyUfs.get(cg);
		if (n < cUnions.size() - 1) {
			UnionG afterUG = this.getFromAlreadyUfsByIndex(cg, n + 1);
			if (afterUG.getXr() > u.getXr()) {
				double v = u.getX() - afterUG.getX() + 3 * p;
				afterUG.setX(afterUG.getX() + v);
				afterUG.setXl(afterUG.getXl() + v);
				afterUG.setXr(afterUG.getXr() + v);
				for (PersonG pg : afterUG.getChildrenPersons()) {
					pg.setX(pg.getX() + v);
				}
				for (PersonG pg : afterUG.getParentPersons()) {
					List<KUUnion> unions = kassService.getUnionByPid(pg
							.getPerson().getPid(), "offspring");
					if (unions == null || unions.size() == 0) {
						pg.setX(pg.getX() + v);
					}
				}
			}
		}
	}

	private void moveUnionMethod4(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPfs(u.getG() + 1, personG.getPerson()
				.getPid());
		PersonG nextPG = getFromAlreadyPfsByIndex(personG.getG(), k + 1);
		if (nextPG == null) {
			return;
		}
		List<KUUnion> nextPunion = kassService.getUnionByPid(nextPG.getPerson()
				.getPid(), "offspring");
		int nextPuid = -1;
		if (nextPunion != null) {
			nextPuid = nextPunion.get(0).getId();
		}
		double v = nextPG.getX() - personG.getX() + p;
		double d = nextPG.getX();
		int a = this.range.getMaxG();
		for (Entry<Integer, List<UnionG>> entry : this.aleadyUfs.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);//
			if (i >= m) {
				List<UnionG> list = entry.getValue();
				KUUnion egoParentUnion = kassService.getPersonUnion(0,
						"offspring");
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					if ((thisUG.getXr() <= d
							&& thisUG.getUnion().getId() != egoParentUnion
									.getId() && thisUG.getUnion().getId() != u
							.getUnion().getId())
							|| (thisUG.getUnion().getId() == nextPuid)) {
						thisUG.setX(thisUG.getX() - v);
						thisUG.setXl(thisUG.getXl() - v);
						thisUG.setXr(thisUG.getXr() - v);
						for (PersonG pg : thisUG.getChildrenPersons()) {
							if (!this.isMRangePerson(g + 1, pg.getPerson()
									.getPid())) {
								pg.setX(pg.getX() - v);
							}
						}
						for (PersonG pg : thisUG.getParentPersons()) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0) {
								if (!this.isMRangePerson(g + 1, pg.getPerson()
										.getPid())) {
									pg.setX(pg.getX() - v);
								}
							}
						}
					}
				}
			}
		}
	}

	private void moveUnionMethod6(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPfs(u.getG(), personG.getPerson()
				.getPid());
		PersonG nextPG = getFromAlreadyPfsByIndex(personG.getG(), k + 1);
		if (nextPG == null) {
			return;
		}
		double v = nextPG.getX() - personG.getX() + p;
		double d = nextPG.getX();
		int a = this.range.getMaxG();
		for (Entry<Integer, List<UnionG>> entry : this.aleadyUfs.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);//
			if (i >= m) {
				List<UnionG> list = entry.getValue();
				KUUnion egoParentUnion = kassService.getPersonUnion(0,
						"offspring");
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					if (thisUG.getXr() <= d
							&& thisUG.getUnion().getId() != egoParentUnion
									.getId()
							&& thisUG.getUnion().getId() != u.getUnion()
									.getId()) {
						thisUG.setX(thisUG.getX() - v);
						thisUG.setXl(thisUG.getXl() - v);
						thisUG.setXr(thisUG.getXr() - v);
						for (PersonG pg : thisUG.getChildrenPersons()) {
							if (!this.isMRangePerson(g + 1, pg.getPerson()
									.getPid())) {
								pg.setX(pg.getX() - v);
							}
						}
						for (PersonG pg : thisUG.getParentPersons()) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0) {
								if (!this.isMRangePerson(g + 1, pg.getPerson()
										.getPid())) {
									pg.setX(pg.getX() - v);
								}
							}
						}
					}
				}
			}
		}
	}

	private void addToPfs(PersonG pg) {
		List<PersonG> persons = pfs.get(pg.getG());
		if (persons == null) {
			persons = new ArrayList<PersonG>();
			pfs.put(pg.getG(), persons);
		}
		persons.add(pg);
	}

	private PersonG getFromPfs(int m, int pid) {
		List<PersonG> list = pfs.get(m);
		if (list == null) {
			return null;
		}
		for (PersonG g : list) {
			if (g.getPerson().getPid() == pid) {
				return g;
			}
		}
		return null;
	}

	private PersonG getFromAlreadyPfs(int g, int pid) {
		List<PersonG> list = this.aleadyPfs.get(g);
		if (list == null) {
			return null;
		}
		for (PersonG pg : list) {
			if (pg.getPerson().getPid() == pid) {
				return pg;
			}
		}
		return null;
	}

	private int getIndexFromAlreadyPfs(int g, int pid) {
		List<PersonG> list = this.aleadyPfs.get(g);
		if (list == null) {
			return -1;
		}
		for (int i = 0; i < list.size(); i++) {
			PersonG p = list.get(i);
			if (p.getPerson().getPid() == pid) {
				return i;
			}
		}
		return -1;
	}

	private int getIndexFromAlreadyUfs(int g, int uid) {
		List<UnionG> list = this.aleadyUfs.get(g);
		if (list == null) {
			return -1;
		}
		for (int i = 0; i < list.size(); i++) {
			UnionG u = list.get(i);
			if (u.getUnion().getId() == uid) {
				return i;
			}
		}
		return -1;
	}

	private PersonG getFromAlreadyPfsByIndex(int g, int index) {
		List<PersonG> list = this.aleadyPfs.get(g);
		if (list == null) {
			return null;
		}
		if (index >= list.size()) {
			return null;
		}
		return list.get(index);
	}

	private UnionG getFromAlreadyUfsByIndex(int g, int index) {
		List<UnionG> list = this.aleadyUfs.get(g);
		if (list == null) {
			return null;
		}
		return list.get(index);
	}

	private UnionG getFromAlreadyUfs(int g, int uid) {
		List<UnionG> list = this.aleadyUfs.get(g);
		if (list == null) {
			return null;
		}
		for (int i = 0; i < list.size(); i++) {
			UnionG u = list.get(i);
			if (u.getUnion().getId() == uid) {
				return u;
			}
		}
		return null;
	}

	private UnionG getFromUfs(int g, int uid) {
		List<UnionG> list = ufs.get(g);
		for (UnionG ug : list) {
			if (ug.getUnion().getId() == uid) {
				return ug;
			}
		}
		return null;
	}

	private void addToAlreadyPfs(int g, PersonG pg, int index) {
		List<PersonG> alreadyList = this.aleadyPfs.get(g);
		if (alreadyList == null) {
			alreadyList = new ArrayList<PersonG>();
			aleadyPfs.put(g, alreadyList);
		}
		if (index == -1) {
			alreadyList.add(pg);
		} else {
			alreadyList.add(index, pg);
		}
	}

	private void delFromPfs(int g, PersonG pg) {
		List<PersonG> list = pfs.get(g);
		list.remove(pg);
	}

	private void addToAlreadyUfs(int g, UnionG ug, int index) {
		List<UnionG> alreadyList = this.aleadyUfs.get(g);
		if (alreadyList == null) {
			alreadyList = new ArrayList<UnionG>();
			aleadyUfs.put(g, alreadyList);
		}
		if (index == -1) {
			if (!alreadyList.contains(ug)) {
				alreadyList.add(ug);
			}
		} else {
			if (!alreadyList.contains(ug)) {
				alreadyList.add(index, ug);
			}
		}
	}

	private void delFromUfs(int g, UnionG ug) {
		List<UnionG> list = ufs.get(g);
		list.remove(ug);
	}

	/**
	 * Check the parent Union
	 * 
	 * @param pid
	 * @return
	 */
	private boolean hasParentUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "offspring");
		if (unions == null || unions.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Check the child union
	 * 
	 * @param pid
	 * @return
	 */
	private boolean hasChildUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "partner");
		if (unions == null || unions.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Get the parent union id
	 * 
	 * @param pid
	 * @return
	 */
	private Integer getParentUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "offspring");
		if (unions != null && unions.size() > 0) {
			return unions.get(0).getId();
		}
		return -1;
	}

	/**
	 * Get the left-most of offspring
	 * 
	 * @param uid
	 * @param g
	 */
	private int getLeftOffspringIndex(int uid, int g) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		int k = -1;
		for (KUUMember member : members) {
			if ("offspring".equals(member.getType())) {
				int index = this.getIndexFromAlreadyPfs(g, member.getId());
				if (k < index) {
					k = index;
				}
			}
		}
		if (k == -1) {
			k = 0;
		}
		return k;
	}

	/**
	 * Get the list of the partner unions
	 * 
	 * @param uid
	 * @return
	 */
	private List<KPPerson> getParnters(int uid) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		List<KPPerson> persons = new ArrayList<KPPerson>();
		for (KUUMember member : members) {
			if ("partner".equals(member.getType())) {
				persons.add(kassService.getPerson(member.getId()));
			}
		}
		Collections.sort(persons, new Comparator<KPPerson>() {

			@Override
			public int compare(KPPerson o1, KPPerson o2) {
				if (o1.getSex() == 0) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		return persons;
	}

	private List<KPPerson> getChildren(int uid) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		List<KPPerson> persons = new ArrayList<KPPerson>();
		for (KUUMember member : members) {
			if ("offspring".equals(member.getType())) {
				persons.add(kassService.getPerson(member.getId()));
			}
		}
		return persons;
	}

	/**
	 * Get the Child-Union if just only one child union
	 * 
	 * @param uid
	 * @param pid
	 * @return
	 */
	private int getOnlyOneChildUnion(int uid, int pid) {
		List<KPPerson> list = this.getChildren(uid);
		int pidSize = 0;
		int othSize = 0;
		for (KPPerson p : list) {
			if (p.getPid() == pid && hasChildUnion(pid)) {
				pidSize++;
			}
			if (p.getPid() != pid && hasChildUnion(pid)) {
				othSize++;
			}
		}
		if (pidSize == 0 && othSize == 0) {
			return 0;
		} else if (pidSize == 1 && othSize == 0) {
			return 1;
		} else {
			return 2;
		}
	}

	private boolean isChildParnter(int g, int index, int uid) {
		if (index >= this.aleadyPfs.size()) {
			return false;
		}
		PersonG pg = this.getFromAlreadyPfsByIndex(g, index);
		List<KPPerson> children = this.getChildren(uid);
		boolean flag = false;
		for (KPPerson child : children) {
			KUUnion u = kassService.getUnionByPid(pg.getPerson().getPid(),
					child.getPid());
			if (u != null) {// 夫妻关系
				flag = true;
				break;
			}
		}
		return flag;
	}

	private Integer getChildParnter(int g, int index, int uid) {
		PersonG pg = this.getFromAlreadyPfsByIndex(g, index);
		List<KPPerson> children = this.getChildren(uid);
		for (KPPerson child : children) {
			KUUnion u = kassService.getUnionByPid(pg.getPerson().getPid(),
					child.getPid());
			if (u != null) {// 夫妻关系
				return u.getId();
			}
		}
		return -1;
	}

	private Object[] getEffectiveUnion(UnionG unionG, UnionG u0) {
		String id = "";
		int index = -1;
		int index2 = -1;
		boolean flag = false;
		for (int i = u0.getPath().length - 1; i >= 0; i = i - 2) {
			String uid = u0.getPath()[i];
			if (uid != null) {
				for (int j = unionG.getPath().length - 1; j >= 0; j = j - 2) {
					String uid2 = unionG.getPath()[j];
					if (uid2 == null) {
						continue;
					}
					if (StringUtils.equals(uid, uid2)) {
						id = uid;
						index = i;
						index2 = j;
						flag = true;
						break;
					}
				}
			}
			if (flag) {
				break;
			}
		}
		if (StringUtils.isNotBlank(id)) {
			return new Object[] { kassService.getUnion(Integer.parseInt(id)),
					index, index2 };
		}
		return null;
	}

	/**
	 * Check if it is on the right of the NKK diagram. ( Already arranged)
	 * 
	 * @param g
	 * @param pid
	 * @return
	 */
	private boolean isMRangePerson(int g, int pid) {
		List<PersonG> list = gmr.getAleadyPms().get(g);
		if (list == null) {
			return false;
		}
		for (PersonG pg : list) {// mother
			if (pg.getPerson().getPid() == pid) {
				PersonG personG = this.getFromAlreadyPfs(g, pid);
				if (personG == null) {
					personG = this.getFromPfs(g, pid);
				}
				personG.setX(pg.getX());
				personG.setY(pg.getY());
				return true;
			}
		}
		return false;
	}

	public TreeMap<Integer, List<UnionG>> getAleadyUfs() {
		return aleadyUfs;
	}

	public TreeMap<Integer, List<PersonG>> getAleadyPfs() {
		return aleadyPfs;
	}

	public TreeMap<Integer, List<UnionG>> getUfs() {
		return ufs;
	}

	public Double getX0() {
		return x0;
	}

	public void setX0(Double x0) {
		this.x0 = x0;
	}

	public void printCoor() {
		for (Map.Entry<Integer, List<PersonG>> entry : this.aleadyPfs
				.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			for (PersonG pg : gs) {
				System.out.println(key + "" + pg.getPerson().getPid() + " "
						+ pg.getX() + "," + pg.getY());
			}
		}
		System.out.println("==========printCoor");
	}

	public void print() {
		System.out.println("==========Father U===========");
		for (Map.Entry<Integer, List<UnionG>> entry : ufs.entrySet()) {
			String key = entry.getKey() + " : ";
			List<UnionG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Father U'===========");
		for (Map.Entry<Integer, List<UnionG>> entry : this.aleadyUfs.entrySet()) {
			String key = entry.getKey() + " : ";
			List<UnionG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Father P===========");
		for (Map.Entry<Integer, List<PersonG>> entry : pfs.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Father P'===========");
		for (Map.Entry<Integer, List<PersonG>> entry : this.aleadyPfs
				.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
	}

}
