package com.fangda.kass.ui.interview.project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.CompleteProcedure;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.PqdQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.JRadioGroup;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.ui.interview.support.InitUtil;
import com.fangda.kass.ui.interview.support.QuestionMap;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Auto Complete Panel
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class AutoCompletePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7120586547946998904L;
	public IvMainPanel ivMainPanel;
	public int pid;// person id
	public mxCell mxCell;
	public CompleteProcedure cps;

	private QuestionnaireService qService;
	private KassService kassService;
	protected JPanel container;
	private Question leadQuestion;
	private String leadQid = Constants.QUESTION_GENERAL;
	public JScrollPane questionScroll;
	public int curStepNo = 1;
	public int page = 0;
	private QuestionMap qm;
	private JButton sureBtn;
	JRadioGroup bg;
	public int cx = 60;
	public int cy = 60;
	public boolean hasPartern = false;
	public GraphPanel graphPanel;
	public KUUnion nowUnion;
	private LinkedHashMap<String, Object> groupMap = new LinkedHashMap<String, Object>();
	List<JPanel> panels = new ArrayList<JPanel>();

	public AutoCompletePanel(IvMainPanel ivMainPanel, GraphPanel graphPanel,
			int pid, mxCell mxCell, CompleteProcedure cps) {
		this.ivMainPanel = ivMainPanel;
		this.graphPanel = graphPanel;
		this.pid = pid;
		this.mxCell = mxCell;
		this.cps = cps;
		qService = new QuestionnaireService();
		kassService = new KassService();
		qm = new QuestionMap();
		initPanel();
	}

	public void initPanel() {
		String title = kassService.getPersonName(pid);
		this.setBackground(Color.white);
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(title),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		int width = ivMainPanel.leftQuestionPanel.getWidth();
		int height = ivMainPanel.leftQuestionPanel.getHeight();
		this.setPreferredSize(new Dimension(width - 15, height - 20));
		container = new JPanel(new BorderLayout());
		container.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		container.add(createPanel(), BorderLayout.CENTER);
		container.setBackground(Color.white);
		container.setPreferredSize(new Dimension(width - 15, height - 20));
		this.add(container);
		this.setVisible(true);
	}

	public JComponent createPanel() {
		leadQuestion = qService.searchQuestion(leadQid);
		JPanel jpanel = createPanel(leadQuestion);
		return jpanel;
	}

	public JPanel createPanel(Question qst) {
		JPanel jpanel = new JPanel();
		int width1 = ivMainPanel.leftQuestionPanel.getWidth();
		int height1 = ivMainPanel.leftQuestionPanel.getHeight();
		jpanel.setPreferredSize(new Dimension(width1 - 15, height1 - 100));
		jpanel.setBackground(Color.white);
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		GridBagConstraints c = new GridBagConstraints();

		int index = 0;
		int width = ivMainPanel.leftQuestionPanel.getWidth() - 15;

		JLabel detailLabel = new JLabel(
				mxResources.get("autoCompleteDetailNote"));
		UIHelper.createMultiLabel(detailLabel, Font.NORMAL, 12, width);
		UIHelper.addToBagpanel(panel, Color.white, detailLabel, c, 0, index, 1,
				1, GridBagConstraints.NORTHWEST);

		index++;
		createQuestionPanel();
		UIHelper.addToBagpanel(panel, Color.white, questionScroll, c, 0, index,
				1, 1, GridBagConstraints.NORTHWEST);

		index++;
		index = createQuestionButton(panel, c, index);

		jpanel.add(panel);
		return jpanel;
	}

	private void createQuestionPanel() {
		questionScroll = new JScrollPane();
		// int width2 = ivMainPanel.leftQuestionPanel.getWidth() - 15;
		int height2 = ivMainPanel.leftQuestionPanel.getHeight() - 140;
		// questionScroll.setPreferredSize(new Dimension(width2+20, height2));
		questionScroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		questionScroll
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		questionScroll.setBackground(Color.white);
		// questionScroll.setBorder(BorderFactory
		// .createEmptyBorder(0, 0, 0, 0));
		questionScroll.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Add Mother"),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		questionScroll.setPreferredSize(new Dimension(
				ivMainPanel.leftQuestionPanel.getWidth() - 15, height2));

		Map<String, Boolean> map = kassService.getPersonParent(pid);
		if (map.get("mother") != null && map.get("father") == null) {
			createQuestionPanel(1, 1);
		} else if (map.get("mother") != null && map.get("father") != null) {
			boolean hasBrother = kassService.getPersonBrother(pid);
			if (hasBrother) {
				createQuestionPanel(3, 0);
			} else {
				createQuestionPanel(2, 0);
			}
		} else {
			createQuestionPanel(1, 0);
		}
	}

	private void createQuestionPanel(int setpNo, int p) {
		if (setpNo == 1) {
			Map<String, Boolean> map = kassService.getPersonParent(pid);
			if (p == 0) {
				if (map.get("mother") != null && map.get("mother")) {
					setNext();
					this.createQuestionPanel(curStepNo, page);
					return;
				}
			}
			if (p == 1) {
				if (map.get("father") != null && map.get("father")) {
					setNext();
					this.createQuestionPanel(curStepNo, page);
					return;
				}
			}
		}
		int width = ivMainPanel.leftQuestionPanel.getWidth();
		// Question qst = qService.searchQuestion(leadQid);
		Questionnaire q = qService.get();
		List<PqdQuestion> questions = q.getDraw().getPerson().getPquestion();

		// JPanel boxPanel = new JPanel();
		// boxPanel.setLayout(new VFlowLayout());
		// boxPanel.setBackground(Color.white);

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel followupPanel = new JPanel(bagLayout);
		followupPanel.setBackground(Color.white);

		int index = 0;
		index = this.addStep(followupPanel, c, index, setpNo, p);
		qm.removeAll();
		panels.clear();
		groupMap.clear();
		for (PqdQuestion pq : questions) {
			Question qst = qService.searchQuestion(pq.getQid());
			if (qst.getFollowupQuestions() != null
					&& qst.getFollowupQuestions().size() > 0) {
				for (int i = 0; i < qst.getFollowupQuestions().size(); i++) {
					FollowupQuestion fquestion = qst.getFollowupQuestions()
							.get(i);
					if (Constants.QUESTION_SEX.equals(fquestion.getQid())) {
						continue;
					}
					GridBagLayout bagLayout1 = new GridBagLayout();
					JPanel fPanel = new JPanel(bagLayout1);
					fPanel.setBackground(Color.white);
					if (StringUtils.isNotBlank(fquestion.getGroupName())) {

						String key = qst.getQid() + "_"
								+ fquestion.getGroupName();
						JPanel jpanel = (JPanel) groupMap.get(key);
						if (jpanel == null) {
							jpanel = new JPanel();
							jpanel.setBackground(Color.white);
							jpanel.setLayout(new BoxLayout(jpanel,
									BoxLayout.Y_AXIS));
							jpanel.setBorder(BorderFactory
									.createTitledBorder(fquestion
											.getGroupName()));
							jpanel.setName("group_" + fquestion.getGroupName());
							groupMap.put(key, jpanel);
							UIHelper.addToBagpanel(fPanel, Color.white, jpanel,
									c, 0, index, 1, 1,
									GridBagConstraints.NORTHWEST);
							index++;
						}
						JPanel qPanel = new JPanel();
						qPanel.setBackground(Color.white);

						qPanel.setLayout(new BoxLayout(qPanel, BoxLayout.Y_AXIS));
						qPanel.setName("question_" + fquestion.getQid());
						qPanel.setBorder(BorderFactory.createEmptyBorder());
						// qPanel.setBorder(BorderFactory.createTitledBorder(fquestion.getQid()));
						if (StringUtil.isNotBlank(fquestion.getDetail())) {
							JLabel titleLabel = new JLabel(""
									+ fquestion.getDetail());
							titleLabel.setPreferredSize(new Dimension(
									width - 130, 30));
							qPanel.add(titleLabel);
						}
						addOption(qPanel, c, fquestion, index, width - 130);
						jpanel.add(qPanel);
						panels.add(qPanel);
					} else {
						fPanel.setName("question_" + fquestion.getQid());
						String t = fquestion.getDetail();
						if (StringUtils.isNotBlank(t)) {
							JLabel detailLabel = new JLabel(t);
							UIHelper.createMultiLabel(detailLabel, Font.NORMAL,
									13, width - 130);
							UIHelper.addToBagpanel(fPanel, Color.white,
									detailLabel, c, 0, index, 1, 1,
									GridBagConstraints.NORTHWEST);
							index++;
						}
						addOption(fPanel, c, fquestion, index, width - 130);
						panels.add(fPanel);
						index++;
					}
					UIHelper.addToBagpanel(followupPanel, Color.white, fPanel,
							c, 0, index, 1, 1, GridBagConstraints.NORTHWEST);
				}
			}
		}
		questionScroll.getViewport().add(followupPanel);
		questionScroll.getViewport().repaint();
		updateNewPersonVisiable();
	}

	private int addStep(JPanel panel, GridBagConstraints c, int index,
			int stepNo, int page) {
		List<CompleteStep> stemps = cps.getCompleteStep();
		CompleteStep curStep = null;
		for (CompleteStep step : stemps) {
			if (step.getStepNo() == stepNo) {
				curStep = step;
				break;
			}
		}
		curStepNo = curStep.getStepNo();
		if (stepNo == 1) {
			CompleteSubStep subStep = null;
			if (page == 0) {
				subStep = curStep.getSubSteps().get(0);
			} else {
				subStep = curStep.getSubSteps().get(1);
			}
			JRadioButton rb = new JRadioButton();
			rb.setName(subStep.getKinshipTerm());
			rb.setActionCommand(subStep.getSex().toString());
			rb.setBackground(Color.white);
			rb.setText("<html>" + subStep.getKinshipTerm() + "</html>");
			rb.setSelected(true);
			bg = new JRadioGroup();
			bg.add(rb);
			UIHelper.addToBagpanel(panel, Color.white, rb, c, 0, index, 1, 1,
					GridBagConstraints.NORTHWEST);
			index++;
		} else {
			List<CompleteSubStep> subSteps = curStep.getSubSteps();
			bg = new JRadioGroup();
			for (CompleteSubStep subStep : subSteps) {
				JRadioButton rb = new JRadioButton();
				rb.setBackground(Color.white);
				rb.setActionCommand(subStep.getSex().toString());
				if (StringUtils.isNotBlank(subStep.getKinshipTerm())) {
					rb.setName(subStep.getKinshipTerm());
					rb.setText("<html>" + subStep.getKinshipTerm() + "</html>");
				} else {
					if (subStep.getSex() == 1) {
						rb.setName("Female");
						rb.setText("<html>Female</html>");
					} else {
						rb.setName("Male");
						rb.setText("<html>Male</html>");
					}
				}
				// rb.setSelected(true);
				bg.add(rb);
				UIHelper.addToBagpanel(panel, Color.white, rb, c, 0, index, 1,
						1, GridBagConstraints.NORTHWEST);
				index++;
			}
		}
		questionScroll.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(curStep.getTitle()),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		return index;
	}

	public int addOption(JPanel panel, GridBagConstraints c, Question question,
			int index, int width) {

		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField();
			field.setName(question.getQid());
			field.setPreferredSize(new Dimension(width, 30));
			// UIHelper.addToBagpanel(panel, Color.white, field, c, 0, index, 1,
			// 1, GridBagConstraints.WEST);
			panel.add(field);
			qm.addQuestion(question, field);
			index++;
			if (StringUtils.isNotBlank(question.getLabel())
					&& (StringUtils.equals(question.getLabel(), "Surname") || StringUtils
							.equals(question.getLabel(), "Othernames"))) {
				field.getDocument().addDocumentListener(documentListener);
			}
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setName(question.getQid());
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			notePane.setPreferredSize(new Dimension(width, 60));
			// UIHelper.addToBagpanel(panel, Color.white, notePane, c, 0, index,
			// 1, 1, GridBagConstraints.WEST);
			panel.add(notePane);
			index++;
			qm.addQuestion(question, noteField);
		} else if ("B".equals(question.getType())
				|| "D".equals(question.getType())) {//
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					JCheckBox cb = new JCheckBox();
					cb.setName(option.getOid());
					cb.setBackground(Color.white);
					cb.setText("<html>" + option.getDetail() + "</html>");
					cb.setPreferredSize(new Dimension(width, 30));
					// UIHelper.addToBagpanel(panel, Color.white, cb, c, 0,
					// index, 1, 1, GridBagConstraints.WEST);
					panel.add(cb);
					index++;
					qm.addQuestion(question, cb);
				}
			}
		} else if ("C".equals(question.getType())
				|| "Q".equals(question.getType())) {
			if (question.getOptions() != null) {
				JRadioGroup bg = new JRadioGroup();
				for (Option option : question.getOptions()) {
					JRadioButton rb = new JRadioButton();
					rb.setActionCommand(option.getValue());
					rb.setName(option.getOid());
					rb.setBackground(Color.white);
					rb.setText("<html>" + option.getDetail() + "</html>");
					rb.setPreferredSize(new Dimension(width, 30));
					// UIHelper.addToBagpanel(panel, Color.white, rb, c, 0,
					// index, 1, 1, GridBagConstraints.WEST);
					index++;
					bg.add(rb);
					panel.add(rb);
				}
				qm.addQuestion(question, bg);
			}
		}
		return index;
	}

	private int createQuestionButton(JPanel panel, GridBagConstraints c,
			int index) {
		if (curStepNo == 3) {
			sureBtn = new JButton("");
			completeAuto();
		} else {
			sureBtn = new JButton("");
			noSaveGoNext();
		}
		UIHelper.addToBagpanel(panel, Color.white, sureBtn, c, 0, index, 1, 1,
				GridBagConstraints.WEST);
		index++;
		return index;
	}

	/**
	 * The Button for going to next question
	 */
	private void changeQuestionButton() {
		if (sureBtn == null) {
			sureBtn = new JButton("");
		}
		sureBtn.setEnabled(true);
		this.removeListener();
		if (curStepNo == 3) {
			completeAuto();
		} else {
			noSaveGoNext();
		}
	}

	private DocumentListener documentListener = new DocumentListener() {

		@Override
		public void insertUpdate(DocumentEvent e) {
			change(e);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			change(e);
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			change(e);
		}

		private void change(DocumentEvent e) {
			List<Object> objs = qm.getNameFieldTextComponents();
			int vSize = 0;
			for (Object obj : objs) {
				if (obj instanceof JTextField) {
					String t = ((JTextField) obj).getText();
					if (StringUtils.isNotBlank(t)) {
						vSize++;
					}
				} else if (obj instanceof JTextArea) {
					String t = ((JTextArea) obj).getText();
					if (StringUtils.isNotBlank(t)) {
						vSize++;
					}
				}
			}
			if (vSize == 0) {
				removeListener();
				changeQuestionButton();
			} else if (vSize < objs.size()) {
				removeListener();
				noComplete();
			} else if (vSize == objs.size()) {
				removeListener();
				hasComplete();
			}
		}
	};

	private void removeListener() {
		ActionListener[] lis = sureBtn.getActionListeners();
		for (ActionListener l : lis) {
			sureBtn.removeActionListener(l);
		}
	}

	private void noSaveGoNext() {
		sureBtn.setEnabled(true);
		sureBtn.setText(mxResources.get("autoCompleteSkip"));
		sureBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				questionScroll.getViewport().removeAll();
				setNext();
				createQuestionPanel(curStepNo, page);
				changeQuestionButton();
				questionScroll.getViewport().repaint();
			}
		});
	}

	private void noComplete() {
		sureBtn.setEnabled(false);
		sureBtn.setText(mxResources.get("autoCompleteFillBoth"));
	}

	private void hasComplete() {
		sureBtn.setEnabled(true);
		sureBtn.setText(mxResources.get("autoCompleteEnter"));
		sureBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = addPerson();
				if (!flag) {
					return;
				}
				questionScroll.getViewport().removeAll();
				setXunHuanNext();
				createQuestionPanel(curStepNo, page);
				changeQuestionButton();
				questionScroll.getViewport().repaint();
			}
		});
	}

	private void completeAuto() {
		sureBtn.setEnabled(true);
		sureBtn.setText(mxResources.get("finish"));
		sureBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KPPerson person = kassService.getPerson(pid);
				person.setCompleted(1);
				kassService.save();
				graphPanel.completePerson();
				ivMainPanel.openDefaultQuestionPanel();
			}
		});
	}

	public void setNext() {
		if (curStepNo == 1 && page == 0) {
			curStepNo = 1;
			page = 1;
		} else if (curStepNo == 1 && page == 1) {
			curStepNo = 2;
			page = 0;
		} else if (curStepNo == 2) {
			curStepNo = 3;
		} else if (curStepNo == 3) {
			curStepNo = 4;
		} else if (curStepNo == 4) {
			curStepNo = 3;
		}
	}

	public void setXunHuanNext() {
		if (curStepNo == 1 && page == 0) {
			curStepNo = 1;
			page = 1;
		} else if (curStepNo == 1 && page == 1) {
			curStepNo = 2;
			page = 0;
		} else if (curStepNo == 2) {
			curStepNo = 2;
		} else if (curStepNo == 3) {
			curStepNo = 4;
		} else if (curStepNo == 4) {
			curStepNo = 4;
		}
	}

	/**
	 * Add a person
	 */
	public boolean addPerson() {
		int sex = 0;
		Enumeration<AbstractButton> ems = bg.getElements();
		while (ems.hasMoreElements()) {
			AbstractButton button = ems.nextElement();
			if (button.isSelected()) {
				sex = Integer.parseInt(button.getActionCommand());
				break;
			}
		}
		boolean flag = true;
		if (curStepNo == 1) {
			flag = addParent(sex);
		} else if (curStepNo == 2) {//
			flag = this.addSisterAndBrother(sex);
		} else if (curStepNo == 3) {// partner
			flag = this.addParner(sex);
		} else if (curStepNo == 4) {// child
			flag = this.addChild(sex);
		}
		graphPanel.graphComponent.getGraph().refresh();
		return flag;
	}

	public boolean addParent(int sex) {

		double ex = mxCell.getGeometry().getX();
		double ey = mxCell.getGeometry().getY();
		double x = 0;
		double y = 0;
		KUUnion union = kassService.getPersonUnion(pid, "offspring");
		mxCell u1 = null;
		if (union != null) {
			u1 = graphPanel.findUnionById(union.getId());
			if (u1 == null) {
				u1 = graphPanel.drawOnlyUnion(union);
			}
			x = u1.getGeometry().getX();
			y = u1.getGeometry().getY();
		} else {
			int cellId = ivMainPanel.getNextUnionId();
			x = ex;
			y = ey - cy;
			u1 = graphPanel.addNewUnion(cellId, x, y, false);
			union = new KUUnion();
			GraphUnion gu = ((GraphUnion) u1.getValue());
			union.setId(gu.getId());
			union.getCoords().setX(x);
			union.getCoords().setY(y);
			if (graphPanel.type == 1) {
				union.setIsOther(0);
			} else {
				union.setIsOther(1);
			}
			qm.initNewUnionDefault(union);
			kassService.get().getUnions().getUnions().add(union);
		}
		mxCell p1 = null;
		Integer cellId = null;
		if (!ivMainPanel.checkSameName(-1, sex, qm, null)) {// The person is
															// exist, then don't
															// add
			p1 = ivMainPanel.getSameCellByName(-1, sex, qm, null);
			GraphPerson gp = (GraphPerson) p1.getValue();
			cellId = gp.getId();
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		} else {// Add the person
			cellId = ivMainPanel.getNextPersonId();
			if (sex == 0) {
				p1 = graphPanel.addNewPerson(cellId, x - cx, y - 2 * cy, sex,
						false);
			} else {
				p1 = graphPanel.addNewPerson(cellId, x + cx, y - 2 * cy, sex,
						false);
			}
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		}
		this.changeStyle(cellId, sex, p1);
		GraphPerson gp1 = (GraphPerson) p1.getValue();
		gp1.setName(kassService.getPersonName(gp1.getId()));
		gp1.setBirthDay(kassService.getPersonBirthYear(gp1.getId()));
		gp1.setSex(sex);
		if (graphPanel.getEdge(u1, mxCell) == null) {
			graphPanel.drawOffspringEdge(u1, mxCell);
			KUUMember m1 = new KUUMember();
			m1.setId(pid);
			m1.setType("offspring");
			union.getMembers().add(m1);
		}
		graphPanel.drawParentEdge(p1, u1);
		KUUMember m2 = new KUUMember();
		m2.setId(gp1.getId());
		m2.setType("partner");
		union.getMembers().add(m2);
		GraphUnion gu = ((GraphUnion) u1.getValue());
		gu.setKuunion(union.clone());
		kassService.save();
		hanldeCount(cellId);
		KPPerson person = kassService.getPerson(cellId);
		this.saveYear(person);
		InitUtil.modifyPersonDefault(person, qService, kassService);
		return true;
	}

	/**
	 * Draw the sibling
	 */
	public boolean addSisterAndBrother(int sex) {
		if (bg.getSelection() == null) {
			return false;
		}
		double ex = mxCell.getGeometry().getX();
		double ey = mxCell.getGeometry().getY();
		double x = 0;
		double y = 0;
		KUUnion union = kassService.getPersonUnion(pid, "offspring");
		mxCell u1 = null;
		if (union != null) {
			u1 = graphPanel.findUnionById(union.getId());
			if (u1 == null) {
				u1 = graphPanel.drawOnlyUnion(union);
			}
			x = u1.getGeometry().getX();
			y = u1.getGeometry().getY();
		} else {
			x = ex;
			y = ey - cy;
			int cellId = ivMainPanel.getNextUnionId();
			u1 = graphPanel.addNewUnion(cellId, x, y, false);
			union = new KUUnion();
			GraphUnion gu = ((GraphUnion) u1.getValue());
			union.setId(gu.getId());
			union.getCoords().setX(x);
			union.getCoords().setY(y);
			if (graphPanel.type == 1) {
				union.setIsOther(0);
			} else {
				union.setIsOther(1);
			}
			qm.initNewUnionDefault(union);
			kassService.get().getUnions().getUnions().add(union);
		}
		mxCell p1 = null;
		Integer cellId = null;
		if (!ivMainPanel.checkSameName(-1, sex, qm, null)) {// The person is
															// exist, then don't
															// add
			p1 = ivMainPanel.getSameCellByName(-1, sex, qm, null);
			GraphPerson gp = (GraphPerson) p1.getValue();
			cellId = gp.getId();
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		} else {// Add the person
			cellId = ivMainPanel.getNextPersonId();
			int lx = calSisterAndBrother((int) (x - cx), (int) ey);
			p1 = graphPanel.addNewPerson(cellId, lx, ey, sex, false);
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		}

		this.changeStyle(cellId, sex, p1);
		GraphPerson gp1 = (GraphPerson) p1.getValue();
		gp1.setName(kassService.getPersonName(gp1.getId()));
		gp1.setBirthDay(kassService.getPersonBirthYear(gp1.getId()));
		gp1.setSex(sex);
		if (graphPanel.getEdge(u1, mxCell) == null) {
			graphPanel.drawOffspringEdge(u1, mxCell);
			KUUMember m1 = new KUUMember();
			m1.setId(pid);
			m1.setType("offspring");
			union.getMembers().add(m1);
		}
		graphPanel.drawOffspringEdge(u1, p1);
		KUUMember m2 = new KUUMember();
		m2.setId(gp1.getId());
		m2.setType("offspring");
		union.getMembers().add(m2);
		GraphUnion gu = ((GraphUnion) u1.getValue());
		gu.setKuunion(union.clone());
		kassService.save();
		hanldeCount(cellId);
		KPPerson person = kassService.getPerson(cellId);
		this.saveYear(person);
		InitUtil.modifyPersonDefault(person, qService, kassService);
		return true;
	}

	/**
	 * Draw the partner
	 */
	public boolean addParner(int sex) {
		if (bg.getSelection() == null) {
			return false;
		}
		double ex = mxCell.getGeometry().getX();
		double ey = mxCell.getGeometry().getY();
		double x = ex + cx;
		double y = ey + cy;
		mxCell p1 = null;//
		Integer cid = null;
		mxCell u1 = null;
		KUUnion union = null;
		if (!ivMainPanel.checkSameName(-1, sex, qm, null)) {// The person is
															// exist, then don't
															// add
			p1 = ivMainPanel.getSameCellByName(-1, sex, qm, null);
			GraphPerson gp = (GraphPerson) p1.getValue();
			cid = gp.getId();
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);

			union = kassService.isPartner(cid, pid);
		} else {// Add the person
			int lx = this.calPartnerUnion((int) x, (int) y);
			cid = ivMainPanel.getNextPersonId();
			p1 = graphPanel.addNewPerson(cid, lx + cx, ey, sex, false);
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		}
		if (union == null) {
			int lx = this.calPartnerUnion((int) x, (int) y);
			int cellId = ivMainPanel.getNextUnionId();
			u1 = graphPanel.addNewUnion(cellId, lx, y, false);
			union = new KUUnion();
			GraphUnion gu = ((GraphUnion) u1.getValue());
			union.setId(gu.getId());
			union.getCoords().setX(lx);
			union.getCoords().setY(y);
			if (graphPanel.type == 1) {
				union.setIsOther(0);
			} else {
				union.setIsOther(1);
			}
			qm.initNewUnionDefault(union);
			kassService.get().getUnions().getUnions().add(union);
			nowUnion = union;
		} else {
			nowUnion = union;
			u1 = graphPanel.findUnionById(union.getId());
		}
		this.changeStyle(cid, sex, p1);
		GraphPerson gp1 = (GraphPerson) p1.getValue();
		gp1.setName(kassService.getPersonName(gp1.getId()));
		gp1.setBirthDay(kassService.getPersonBirthYear(gp1.getId()));
		gp1.setSex(sex);
		if (graphPanel.getEdge(u1, mxCell) == null) {
			graphPanel.drawParentEdge(u1, mxCell);
			KUUMember m1 = new KUUMember();
			m1.setId(pid);
			m1.setType("partner");
			union.getMembers().add(m1);
		}
		if (graphPanel.getEdge(u1, p1) == null) {
			graphPanel.drawParentEdge(u1, p1);
			KUUMember m1 = new KUUMember();
			m1.setId(gp1.getId());
			m1.setType("partner");
			union.getMembers().add(m1);
		}
		GraphUnion gu = ((GraphUnion) u1.getValue());
		gu.setKuunion(union);
		kassService.save();
		hanldeCount(cid);
		KPPerson person = kassService.getPerson(cid);
		this.saveYear(person);
		InitUtil.modifyPersonDefault(person, qService, kassService);
		return true;
	}

	/**
	 * Check the stopRule and change the color of the person with the largest
	 * kinship distance to yellow.
	 * 
	 * @param u
	 * @param p
	 */
	private void hanldeCount(int pid) {
		int rcount = graphPanel.getRoutingCount(pid);
		StopRule sr = qService.getStopRule();
		int step = sr.getStep();
		if (rcount == step) {
			mxCell mxCell = graphPanel.findPersonById(pid);
			graphPanel.setColor(mxCell, ColorUtil.getStopRuleColor());
			graphPanel.graphComponent.refresh();
		}
	}

	/**
	 * Draw the children
	 */
	public boolean addChild(int sex) {
		if (bg.getSelection() == null) {
			return false;
		}
		double ex = mxCell.getGeometry().getX();
		double ey = mxCell.getGeometry().getY();
		double x = 0;
		double y = 0;
		// KUUnion union = kassService.getPersonUnion(pid, "partner");
		KUUnion union = nowUnion;
		mxCell u1 = null;
		if (union != null) {
			u1 = graphPanel.findUnionById(union.getId());
			if (u1 == null) {
				u1 = graphPanel.drawOnlyUnion(union);
			}
			x = u1.getGeometry().getX();
			y = u1.getGeometry().getY();
		} else {
			x = ex + cx;
			y = ey + cy;
			int cellId = ivMainPanel.getNextUnionId();
			u1 = graphPanel.addNewUnion(cellId, x, y, false);
			union = new KUUnion();
			GraphUnion gu = ((GraphUnion) u1.getValue());
			union.setId(gu.getId());
			union.getCoords().setX(x);
			union.getCoords().setY(y);
			if (graphPanel.type == 1) {
				union.setIsOther(0);
			} else {
				union.setIsOther(1);
			}
			qm.initNewUnionDefault(union);
		}
		mxCell p1 = null;
		Integer cellId = null;
		if (!ivMainPanel.checkSameName(-1, sex, qm, null)) {// The person is
															// exist, then don't
															// add
			p1 = ivMainPanel.getSameCellByName(-1, sex, qm, null);
			GraphPerson gp = (GraphPerson) p1.getValue();
			cellId = gp.getId();
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		} else {// Add the person
			cellId = ivMainPanel.getNextPersonId();
			int lx = this.calChild((int) x, (int) (y + cy));
			p1 = graphPanel.addNewPerson(cellId, lx, y + cy, sex, false);
			qm.savePersonValue(graphPanel, p1, qService, kassService, null,
					null);
		}
		this.changeStyle(cellId, sex, p1);
		GraphPerson gp1 = (GraphPerson) p1.getValue();
		gp1.setName(kassService.getPersonName(gp1.getId()));
		gp1.setBirthDay(kassService.getPersonBirthYear(gp1.getId()));
		gp1.setSex(sex);
		if (graphPanel.getEdge(u1, mxCell) == null) {
			graphPanel.drawParentEdge(u1, mxCell);
			KUUMember m1 = new KUUMember();
			m1.setId(pid);
			m1.setType("partner");
			union.getMembers().add(m1);
		}
		graphPanel.drawOffspringEdge(u1, p1);
		KUUMember m2 = new KUUMember();
		m2.setId(gp1.getId());
		m2.setType("offspring");
		union.getMembers().add(m2);
		GraphUnion gu = ((GraphUnion) u1.getValue());
		gu.setKuunion(union.clone());
		kassService.save();
		hanldeCount(cellId);
		KPPerson person = kassService.getPerson(cellId);
		this.saveYear(person);
		InitUtil.modifyPersonDefault(person, qService, kassService);
		return true;
	}

	/**
	 * The original coordinates
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int calSisterAndBrother(int x, int y) {
		mxCell mxCell = graphPanel.getCellByXY(x, y);
		if (mxCell != null) {
			int x1 = x - cx;
			return calSisterAndBrother(x1, y);
		} else {
			return x;
		}
	}

	public int calChild(int x, int y) {
		mxCell mxCell = graphPanel.getCellByXY(x, y);
		if (mxCell != null) {
			int x1 = x + cx;
			return calChild(x1, y);
		} else {
			return x;
		}
	}

	public int calPartnerUnion(int x, int y) {
		mxCell mxCell = graphPanel.getCellByXY(x, y);
		if (mxCell != null) {
			int x1 = x + cx;
			return calPartnerUnion(x1, y);
		} else {
			return x;
		}
	}

	public void changeStyle(int pid, int sex, mxCell cell) {
		KPPerson person = kassService.getPerson(pid);
		int isLiving = graphPanel.isLiving(person);
		if (person.getSex() == 0) {
			if (isLiving == 2) {
				cell.setStyle("maleO");
			} else {
				cell.setStyle("maleDeathO");
			}
		} else {
			if (isLiving == 2) {
				cell.setStyle("femaleO");
			} else {
				cell.setStyle("femaleDeathO");
			}
		}
		graphPanel.graphComponent.getGraph().refresh();
	}

	JPanel deathYearParentPanel = null;// death Panel 父类
	JPanel cbIsYODUnknownPanel = null;//
	JCheckBox cbIsYODUnknownCb = null;//
	JPanel deathYearPanel = null;
	JTextField deathYearText = null;
	JPanel cbIsYODApproxPanel = null;
	JCheckBox cbIsYODApproxCb = null;

	JPanel birthYearParentPanel = null;// birth Panel 父类
	JPanel cbIsYOBUnknownPanel = null;//
	JCheckBox cbIsYOBUnknownCb = null;//
	JPanel birthYearPanel = null;
	JTextField birthYearText = null;
	JPanel cbIsYOBApproxPanel = null;
	JCheckBox cbIsYOBApproxCb = null;

	JCheckBox cbIsDeceasedCb = null;

	public void updateNewPersonVisiable() {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();

			String id = name.replace("question_", "");
			Question question = qService.searchQuestion(id);
			if ("cbIsYODUnknown".equals(question.getLabel())) {
				cbIsYODUnknownPanel = jpanel;
				Container c = jpanel.getParent();
				if (c instanceof JPanel) {
					JPanel parent = (JPanel) c;
					if (StringUtils.isNotBlank(parent.getName())
							&& parent.getName().startsWith("group_")) {
						deathYearParentPanel = parent;
					}
				}
				if (jpanel.getComponentCount() > 0) {
					cbIsYODUnknownCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("Year_of_death".equals(question.getLabel())) {
				deathYearPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					deathYearText = (JTextField) jpanel.getComponent(0);
				}
			}
			if ("cbIsYODApprox".equals(question.getLabel())) {
				cbIsYODApproxPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					cbIsYODApproxCb = (JCheckBox) jpanel.getComponent(0);
				}
			}

			if ("cbIsYOBUnknown".equals(question.getLabel())) {
				cbIsYOBUnknownPanel = jpanel;
				Container c = jpanel.getParent();
				if (c instanceof JPanel) {
					JPanel parent = (JPanel) c;
					if (StringUtils.isNotBlank(parent.getName())
							&& parent.getName().startsWith("group_")) {
						birthYearParentPanel = parent;
					}
				}
				if (jpanel.getComponentCount() > 0) {
					cbIsYOBUnknownCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("Year_of_birth".equals(question.getLabel())) {
				birthYearPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					birthYearText = (JTextField) jpanel.getComponent(0);
				}
			}
			if ("cbIsYOBApprox".equals(question.getLabel())) {
				cbIsYOBApproxPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					cbIsYOBApproxCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("cbIsDeceased".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					cbIsDeceasedCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
		}
		if (deathYearParentPanel != null) {
			deathYearParentPanel.setVisible(false);
		}
		if (cbIsYOBApproxPanel != null) {
			cbIsYOBApproxPanel.setVisible(false);
		}
		if (cbIsYODApproxPanel != null) {
			cbIsYODApproxPanel.setVisible(false);
		}
		if (cbIsDeceasedCb != null) {
			cbIsDeceasedCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						if (deathYearParentPanel != null) {
							deathYearParentPanel.setVisible(true);
						}
						if (cbIsYODUnknownPanel != null) {
							cbIsYODUnknownPanel.setVisible(true);
						}
						if (deathYearPanel != null) {
							deathYearPanel.setVisible(true);
						}
						if (cbIsYODApproxPanel != null) {
							cbIsYODApproxPanel.setVisible(false);
						}
						if (cbIsYODUnknownCb != null) {
							cbIsYODUnknownCb.setSelected(false);
						}
						if (deathYearText != null) {
							deathYearText.setText("");
						}
						if (cbIsYODApproxCb != null) {
							cbIsYODApproxCb.setSelected(false);
						}
					} else {
						if (deathYearParentPanel != null) {
							deathYearParentPanel.setVisible(false);
						}
						if (cbIsYODUnknownCb != null) {
							cbIsYODUnknownCb.setSelected(false);
						}
						if (deathYearText != null) {
							deathYearText.setText("");
						}
						if (cbIsYODApproxCb != null) {
							cbIsYODApproxCb.setSelected(false);
						}
					}
				}
			});
		}
		if (cbIsYOBUnknownCb != null) {
			cbIsYOBUnknownCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						birthYearText.setText("");
						birthYearPanel.setVisible(false);
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					} else {
						birthYearText.setText("");
						birthYearPanel.setVisible(true);
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					}
				}
			});
		}
		if (cbIsYODUnknownCb != null) {
			cbIsYODUnknownCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						deathYearText.setText("");
						deathYearPanel.setVisible(false);
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					} else {
						deathYearText.setText("");
						deathYearPanel.setVisible(true);
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					}
				}
			});
		}
		if (birthYearText != null) {
			birthYearText.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent event) {
					if (StringUtils.isNotBlank(birthYearText.getText())) {
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(true);
					} else {
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					}
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
			});
		}
		if (deathYearText != null) {
			deathYearText.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent event) {
					if (StringUtils.isNotBlank(deathYearText.getText())) {
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(true);
					} else {
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					}
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
			});
		}
	}

	private void saveYear(KPPerson person) {
		if (deathYearParentPanel != null) {
			if (deathYearParentPanel.isVisible()) {
				if (cbIsYODUnknownCb != null) {
					if (cbIsYODUnknownCb.isSelected()) {
						setPersonField(person, "cbIsYODUnknown", "true");
					} else {
						setPersonField(person, "cbIsYODUnknown", "false");
					}
				}
				if (deathYearText != null) {
					setPersonField(person, "Year_of_death",
							deathYearText.getText());
				}
				if (cbIsYODApproxCb != null) {
					if (cbIsYODApproxCb.isSelected()) {
						setPersonField(person, "cbIsYODApprox", "true");
					} else {
						setPersonField(person, "cbIsYODApprox", "false");
					}
				}
			} else {
				setPersonField(person, "cbIsYODUnknown", "false");
				setPersonField(person, "Year_of_death", "");
				setPersonField(person, "cbIsYODApprox", "false");
			}
		}
		if (birthYearParentPanel != null) {
			if (birthYearParentPanel.isVisible()) {
				if (cbIsYOBUnknownCb != null) {
					if (cbIsYOBUnknownCb.isSelected()) {
						setPersonField(person, "cbIsYOBUnknown", "true");
					} else {
						setPersonField(person, "cbIsYOBUnknown", "false");
					}
				}
				if (birthYearText != null) {
					setPersonField(person, "Year_of_death",
							birthYearText.getText());
				}
				if (cbIsYOBApproxCb != null) {
					if (cbIsYOBApproxCb.isSelected()) {
						setPersonField(person, "cbIsYODApprox", "true");
					} else {
						setPersonField(person, "cbIsYODApprox", "false");
					}
				}
			} else {
				setPersonField(person, "cbIsYOBUnknown", "false");
				setPersonField(person, "Year_of_birth", "");
				setPersonField(person, "cbIsYOBApprox", "false");
			}
		}
	}

	private void setPersonField(KPPerson person, String fieldName, String v) {
		List<KQQuestionField> fields = person.getFields();
		for (KQQuestionField field : fields) {
			if (StringUtils.equals(field.getLabel(), fieldName)) {
				field.setValue(v);
				break;
			}
		}
	}
}
