package com.fangda.kass.ui.interview.graph;

import com.mxgraph.model.mxCell;

/**
 * The Class for the listener of the graph operation
 * 
 * @author Fangfang Zhao
 * 
 */
public abstract class GraphListener {
	/**
	 * Add a graphic cell
	 */
	public abstract void afterAddedCell(GraphPanel grapPanel, mxCell cell);

	/**
	 * Double click a graphic cell
	 * 
	 * @param cell
	 */
	public abstract void afterDbClickCell(GraphPanel grapPanel, mxCell cell);

	/**
	 * Single click a graphic cell
	 * 
	 * @param cell
	 */
	public abstract void afterSingleClickCell(GraphPanel grapPanel, mxCell cell);

	/**
	 * Click Empty
	 */
	public abstract void afterClickEmpty(GraphPanel grapPanel);

	/**
	 * Select a graphic cell
	 * 
	 * @param grapPanel
	 */
	public abstract void afterSelectCell(GraphPanel grapPanel, mxCell cell);

	/**
	 * Unselect graphic cell
	 * 
	 * @param grapPanel
	 * @param cell
	 */
	public abstract void afterUnSelectCell(GraphPanel grapPanel, mxCell cell);

	/**
	 * Click on the view model
	 * 
	 * @param grapPanel
	 * @param cell
	 */
	public abstract void clickOnViewModel(GraphPanel grapPanel, mxCell cell);

	/**
	 * Pressed on the view model
	 * 
	 * @param grapPanel
	 * @param cell
	 */
	public abstract void pressedOnViewModel(GraphPanel grapPanel, mxCell cell);
}
