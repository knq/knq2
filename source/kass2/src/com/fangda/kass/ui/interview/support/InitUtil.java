package com.fangda.kass.ui.interview.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.interview.KQuestions;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.RegularUtil;

/**
 * This class for initializing the question answers of an interview in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class InitUtil {

	private static KassService kassService;
	private static QuestionnaireService qService;
	static {
		if (kassService == null) {
			kassService = new KassService();
		}
		if (qService == null) {
			qService = new QuestionnaireService();
		}
	}

	/**
	 * Generating the default value, if a person is dead, check the skip
	 * condition in the ifGo element in the questions
	 * 
	 * @param question
	 * @param person
	 */
	public static void saveDefaultQuestion(Question question, KPPerson person) {
		if (RegularUtil.isDraw(question) || RegularUtil.isUnion(question)) {
			return;
		}
		String defaultValue = RegularUtil.getDefaultValue(question, person);
		List<String> labels = new ArrayList<String>();
		if (RegularUtil.isMap(question)) {
			if (StringUtils.isNotBlank(question.getLabel())) {
				if (!labels.contains(question.getLabel())) {
					KQQuestionField field = new KQQuestionField();
					field.setLabel(question.getLabel());
					field.setQid(question.getQid());
					field.setValue(defaultValue);
					person.getFields().add(field);
					labels.add(question.getLabel());
				}
			}
		} else if (RegularUtil.isPersonOrUnion(question)) {
			List<FollowupQuestion> fquestions = question.getFollowupQuestions();
			if (fquestions != null && fquestions.size() > 0) {

				for (FollowupQuestion fquestion : fquestions) {
					if ("sex".equals(fquestion.getLabel())) {
						continue;
					}
					if ("B".equals(fquestion.getType())) {
						List<Option> options = fquestion.getOptions();
						for (Option option : options) {
							if (!labels.contains(option.getLabel())) {
								KQQuestionField field = new KQQuestionField();
								field.setLabel(option.getLabel());
								field.setQid(option.getOid());
								field.setValue(defaultValue);
								if (Constants.QUESTION_GENERAL.equals(question
										.getQid())) {
									person.getFields().add(
											fquestion.getSortNo() - 1, field);
								} else {
									person.getFields().add(field);
								}
								labels.add(option.getLabel());
							}
						}
					} else {
						if (!labels.contains(fquestion.getLabel())) {
							KQQuestionField field = new KQQuestionField();
							field.setLabel(fquestion.getLabel());
							field.setQid(fquestion.getQid());
							field.setValue(defaultValue);
							if (Constants.QUESTION_GENERAL.equals(question
									.getQid())) {
								person.getFields().add(
										fquestion.getSortNo() - 1, field);
							} else {
								person.getFields().add(field);
							}
							labels.add(fquestion.getLabel());
						}
					}
				}
			}
		} else {
			KQQuestion kqq = new KQQuestion();
			kqq.setQid(question.getQid());
			kqq.setValue(defaultValue);
			kqq.setTitle(question.getTitle());
			kqq.setLabel(question.getLabel());
			person.getQuestions().add(kqq);
			// System.out.println(kqq.getQid() + "  : " + defaultValue + "  : "
			// + question.getType());
			if ("F".equals(question.getType())) {
				if (Constants.DEFAULT_DNA.equals(defaultValue)) {
					kqq.setValue(Constants.DEFAULT_DNA);
				} else {
					kqq.setValue(Constants.DEFAULT_MARK);
				}
			}
			if ("A".equals(question.getType())
					|| "R".equals(question.getType())) {

			} else {
				List<FollowupQuestion> fquestions = question
						.getFollowupQuestions();
				if (fquestions != null && fquestions.size() > 0) {
					for (FollowupQuestion fquestion : fquestions) {
						if ("B".equals(fquestion.getType())) {
							List<Option> options = fquestion.getOptions();
							for (Option option : options) {
								KQQuestionField field = new KQQuestionField();
								field.setLabel(option.getLabel());
								field.setQid(option.getOid());
								field.setValue(defaultValue);
								kqq.getFields().add(field);
							}
						} else {
							KQQuestionField field = new KQQuestionField();
							field.setLabel(fquestion.getLabel());
							field.setQid(fquestion.getQid());
							field.setValue(defaultValue);
							kqq.getFields().add(field);
						}
					}
				} else {
					KQQuestionField field = new KQQuestionField();
					field.setQid(question.getQid());
					field.setLabel(question.getLabel());
					field.setValue(defaultValue);
					kqq.getFields().add(field);
				}
			}
		}
	}

	/**
	 * Modify the default value of the person
	 * 
	 * @param qService
	 * @param kassService
	 */
	public static void modifyPersonDefault(QuestionnaireService qService,
			KassService kassService) {
		List<KPPerson> persons = kassService.getKPersons();
		for (KPPerson person : persons) {
			InitUtil.modifyPersonDefault(person, qService, kassService);
		}
	}

	/**
	 * Modify the default value of the union
	 * 
	 * @param qService
	 * @param kassService
	 */
	public static void modifyUnionDefault(QuestionnaireService qService,
			KassService kassService) {
		List<KUUnion> unions = kassService.get().getUnions().getUnions();
		for (KUUnion union : unions) {
			InitUtil.modifyUnionDefault(union, qService, kassService);
		}
	}

	/**
	 * Modify the default value of the person
	 * 
	 * @param person
	 * @param qService
	 * @param kassService
	 */
	public static void modifyPersonDefault(KPPerson person,
			QuestionnaireService qService, KassService kassService) {
		List<Subsection> list = qService.getQuestionnaire().getSection()
				.getSubsections();
		for (Subsection subsection : list) {
			List<Question> questions = subsection.getQuestions();
			if (questions != null && questions.size() > 0) {
				for (Question question : questions) {
					modifyDefaultValue(question, person, kassService);
				}
			}
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections != null && subsections.size() > 0) {
				for (Subsection ss : subsections) {
					List<Question> qs = ss.getQuestions();
					if (qs != null && qs.size() > 0) {
						for (Question question : qs) {
							modifyDefaultValue(question, person, kassService);
						}
					}
				}
			}
		}
		// kassService.save();
		handlePersonResDistance();//
		handlePersonDeath(person);//
		// kassService.save();
	}

	public static void modifyUnionDefault(KUUnion union,
			QuestionnaireService qService, KassService kassService) {
		List<Subsection> list = qService.getQuestionnaire().getSection()
				.getSubsections();
		for (Subsection subsection : list) {
			List<Question> questions = subsection.getQuestions();
			if (questions != null && questions.size() > 0) {
				for (Question question : questions) {
					modifyUnionDefaultValue(question, union, kassService);
				}
			}
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections != null && subsections.size() > 0) {
				for (Subsection ss : subsections) {
					List<Question> qs = ss.getQuestions();
					if (qs != null && qs.size() > 0) {
						for (Question question : qs) {
							modifyUnionDefaultValue(question, union,
									kassService);
						}
					}
				}
			}
		}
		kassService.save();
	}

	/**
	 * Modify the default value of the person
	 * 
	 * @param question
	 * @param person
	 * @param kassService
	 */
	public static void modifyDefaultValue(Question question, KPPerson person,
			KassService kassService) {
		if (RegularUtil.isDraw(question) || RegularUtil.isUnion(question)) {
			return;
		}
		String defaultValue = RegularUtil.getDefaultValue(question, person);
		List<String> labels = new ArrayList<String>();
		if (RegularUtil.isMap(question)) {
			if (StringUtils.isNotBlank(question.getLabel())) {
				if (!labels.contains(question.getLabel())) {
					KQQuestionField field = kassService.getPersonField(
							person.getPid(), question.getLabel());
					if (field != null) {
						if (Constants.DEFAULT_DNA.equals(field.getValue())
								|| Constants.DEFAULT_NA
										.equals(field.getValue())) {
							field.setValue(defaultValue);
						}
					}
					labels.add(question.getLabel());
				}
			}
		} else if (RegularUtil.isPersonOrUnion(question)) {
			List<FollowupQuestion> fquestions = question.getFollowupQuestions();
			if (fquestions != null && fquestions.size() > 0) {
				for (FollowupQuestion fquestion : fquestions) {
					if ("B".equals(fquestion.getType())) {
						List<Option> options = fquestion.getOptions();
						for (Option option : options) {
							if (!labels.contains(option.getLabel())) {
								KQQuestionField field = kassService
										.getPersonField(person.getPid(),
												option.getLabel());
								if (field != null) {
									if (Constants.DEFAULT_DNA.equals(field
											.getValue())
											|| Constants.DEFAULT_NA
													.equals(field.getValue())) {
										field.setValue(defaultValue);
									}
								}
								labels.add(option.getLabel());
							}
						}
					} else {
						if (!labels.contains(fquestion.getLabel())) {
							KQQuestionField field = kassService.getPersonField(
									person.getPid(), fquestion.getLabel());
							if (field != null) {
								if (Constants.DEFAULT_DNA.equals(field
										.getValue())
										|| Constants.DEFAULT_NA.equals(field
												.getValue())) {
									field.setValue(defaultValue);
								}
							}
							labels.add(fquestion.getLabel());
						}
					}
				}
			}
		} else {
			KQQuestion kqq = kassService.getPersonQuestion(person.getPid(),
					question.getQid());
			if (kqq != null) {
				if (Constants.DEFAULT_DNA.equals(kqq.getValue())
						|| Constants.DEFAULT_NA.equals(kqq.getValue())) {
					kqq.setValue(defaultValue);
				}
				if (Constants.DEFAULT_DNA.equals(kqq.getValue())) {
					// kqq.setIsCompleted(true);
					kqq.setValue(Constants.DEFAULT_DNA);
				}
				if ("F".equals(question.getType())) {
					if (Constants.DEFAULT_DNA.equals(defaultValue)) {
						kqq.setValue(Constants.DEFAULT_DNA);
					} else {
						kqq.setValue(Constants.DEFAULT_MARK);
					}
				}

				List<KQQuestionField> fquestions = kqq.getFields();
				if (fquestions != null && fquestions.size() > 0) {
					for (KQQuestionField field : fquestions) {
						if (Constants.DEFAULT_DNA.equals(field.getValue())
								|| Constants.DEFAULT_NA
										.equals(field.getValue())) {
							field.setValue(defaultValue);
						}
					}
				}
			}
		}
	}

	/**
	 * Modify the default value of the union
	 * 
	 * @param question
	 * @param person
	 * @param kassService
	 */
	public static void modifyUnionDefaultValue(Question question,
			KUUnion union, KassService kassService) {
		if (RegularUtil.isUnion(question)) {
			return;
			// String defaultValue = RegularUtil.getDefaultValue(question,
			// union);
			// if(StringUtils.isBlank(defaultValue)) {
			// return;
			// }
			// List<KQQuestionField> fields = union.getFields();
			// if (fields != null && fields.size() > 0) {
			// for (KQQuestionField field : fields) {
			// if (Constants.DEFAULT_DNA.equals(field.getValue())
			// || Constants.DEFAULT_NA
			// .equals(field.getValue())) {
			// field.setValue(defaultValue);
			// }
			// }
			// }
		}
	}

	/**
	 * Check the dead person
	 * 
	 * @param person
	 */
	public static List<String> handlePersonDeath(KPPerson person) {
		// 人员情况cbIsDeceased KASS_P_08_00_C
		Question question = qService.searchQuestion(Constants.QUESTION_DEATH);
		List<String> dnaList = new ArrayList<String>();
		if (StringUtils.isNotBlank(question.getIfGo())) {
			Map<String, List<String>> map = getValue(question.getIfGo());
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					question.getLabel());
			if (field != null) {
				String value = field.getValue();
				Iterator<Entry<String, List<String>>> iter = map.entrySet()
						.iterator();
				while (iter.hasNext()) {
					Entry<String, List<String>> entry = iter.next();
					String key = entry.getKey();
					List<String> vals = entry.getValue();
					if (StringUtils.equals(key, value)) {
						for (String l : vals) {
							Question fquestion = qService.searchQuestion(l);
							KQQuestionField f = kassService.getPersonField(
									person.getPid(), fquestion.getLabel());
							if (f != null) {
								f.setValue(Constants.DEFAULT_DNA);
								dnaList.add(l);
							}
						}
					}
				}
				iter = map.entrySet().iterator();
				while (iter.hasNext()) {
					Entry<String, List<String>> entry = iter.next();
					String key = entry.getKey();
					List<String> vals = entry.getValue();
					if (!StringUtils.equals(key, value)) {
						for (String l : vals) {
							if (dnaList.contains(l)) {
								continue;
							}
							Question fquestion = qService.searchQuestion(l);
							KQQuestionField f = kassService.getPersonField(
									person.getPid(), fquestion.getLabel());
							if (f != null) {
								if (Constants.DEFAULT_DNA.equals(f.getValue())) {
									f.setValue(Constants.DEFAULT_NA);
								}
							}
						}
					}
				}
			}
		}
		return dnaList;
	}

	/**
	 * Check the household member
	 * 
	 * @param person
	 * @return
	 */
	public static void handlePersonResDistance() {
		List<KPPerson> persons = kassService.get().getPeople().getPersons();
		boolean allIsOk = true;
		for (KPPerson person : persons) {
			if (person.getPid() == 0) {
				continue;
			}
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					"mapResDistance");
			if (field == null || Constants.DEFAULT_NA.equals(field.getValue())) {
				allIsOk = false;
				break;
			}
		}
		if (!allIsOk) {
			return;
		}
		List<KPPerson> ps = new ArrayList<KPPerson>();
		KPPerson ego = null;
		for (KPPerson person : persons) {
			if (person.getPid() == 0) {
				ego = person;
				continue;
			}
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					"mapResDistance");
			if (field != null && "1".equals(field.getValue())) {
				ps.add(person);
			}
		}
		if (ps.size() > 0) {
			ps.add(ego);
			for (KPPerson person : ps) {
				modifyRelG(person, Constants.DEFAULT_NA);
			}
			for (KPPerson person : persons) {
				if (!ps.contains(person)) {
					modifyRelG(person, Constants.DEFAULT_DNA);
				}
			}
		} else {
			for (KPPerson person : persons) {
				modifyRelG(person, Constants.DEFAULT_DNA);
			}
		}
	}

	/**
	 * Modify the value of REL_G
	 * 
	 * @param question
	 * @param person
	 * @param kassService
	 */
	public static void modifyRelG(KPPerson person, String na) {
		List<Subsection> list = qService.getQuestionnaire().getSection()
				.getSubsections();
		for (Subsection subsection : list) {
			List<Question> questions = subsection.getQuestions();
			if (questions != null && questions.size() > 0) {
				for (Question question : questions) {
					modifyRelG(question, person, na);
				}
			}
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections != null && subsections.size() > 0) {
				for (Subsection ss : subsections) {
					List<Question> qs = ss.getQuestions();
					if (qs != null && qs.size() > 0) {
						for (Question question : qs) {
							modifyRelG(question, person, na);
						}
					}
				}
			}
		}
	}

	public static void modifyRelG(Question question, KPPerson person, String na) {
		if ("REL_G".equals(question.getRelevance())) {
			KQQuestion kqquestion = kassService.getPersonQuestion(
					person.getPid(), question.getQid());
			if (kqquestion != null) {
				List<KQQuestionField> fields = kqquestion.getFields();
				for (KQQuestionField field : fields) {
					if (Constants.DEFAULT_DNA.equals(na)) {
						field.setValue(na);
					}
					if (Constants.DEFAULT_NA.equals(na)) {
						if (Constants.DEFAULT_DNA.equals(field.getValue())) {
							field.setValue(na);
						}
					}
				}
				if (Constants.DEFAULT_DNA.equals(na)) {
					kqquestion.setValue(na);
				}
				if (Constants.DEFAULT_NA.equals(na)) {
					if (Constants.DEFAULT_DNA.equals(kqquestion.getValue())) {
						kqquestion.setValue(na);
						KQuestion kquestion = kassService
								.getCompleteField(kqquestion.getQid());
						if ("1".equals(kquestion.getCompleted())) {// 设置已经结束的回复
							kquestion.setCompleted("2");
						}
					}
				}
			}
		}
	}

	/**
	 * Get the value of the ifGo element (The skip condition)
	 * 
	 * @param obj
	 * @return
	 */
	public static Map<String, List<String>> getValue(String ifGo) {
		String label = ifGo.replace("labelSame#", "");
		String[] ls = label.split(Constants.SPLIT_OBJECT);
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String l : ls) {
			String[] vs = l.split(Constants.SPLIT_PROPERTY);
			String value = vs[0];
			String qids = vs[1];
			String[] qidArray = qids.split(",");
			map.put(value, Arrays.asList(qidArray));
		}
		return map;
	}

	/**
	 * Save the completed status
	 * 
	 * @param qService
	 * @param kassService
	 */
	public static void saveDefaultComplete(QuestionnaireService qService,
			KassService kassService) {
		if (kassService.get().getQuestions() == null) {
			kassService.get().setQuestions(new KQuestions());
		}
		List<Subsection> list = qService.getQuestionnaire().getSection()
				.getSubsections();
		for (Subsection subsection : list) {
			List<Question> questions = subsection.getQuestions();
			if (questions != null && questions.size() > 0) {
				for (Question question : questions) {
					KQuestion kq = new KQuestion();
					kq.setCompleted("0");
					kq.setId(question.getQid());
					if ("F".equals(question.getType())) {
						kq.setIsMarked(true);
					} else {
						kq.setIsMarked(false);
					}
					kassService.get().getQuestions().getQuestions().add(kq);
				}
			}
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections != null && subsections.size() > 0) {
				for (Subsection ss : subsections) {
					List<Question> qs = ss.getQuestions();
					if (qs != null && qs.size() > 0) {
						for (Question question : qs) {
							KQuestion kq = new KQuestion();
							kq.setCompleted("0");
							kq.setId(question.getQid());
							if ("F".equals(question.getType())) {
								kq.setIsMarked(true);
							} else {
								kq.setIsMarked(false);
							}
							kassService.get().getQuestions().getQuestions()
									.add(kq);
						}
					}
				}
			}
		}
		kassService.save();
	}
}
