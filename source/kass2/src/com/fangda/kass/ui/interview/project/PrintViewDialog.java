package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of printing the diagram in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class PrintViewDialog extends BaseDialog implements Printable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8273351054387675453L;
	JButton printButton = null;
	JButton zoomInButton = null;
	JButton zoomOutButton = null;
	JButton nkkButton = null;
	JButton otherButton = null;

	Image image = null;
	Image oimage = null;
	JScrollPane mainScrollPanel = null;
	JPanel imagePanel = null;
	float imageWidth = 0;
	float imageHeight = 0;
	private int type = 1;// 1 = NKK, 2 = other
	private PrinterJob job;

	public PrintViewDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {

	}

	@Override
	public JPanel createPanel() {
		this.initPrinterJob();
		image = IvMainPanel.IVPANEL.graphPanel.exportPNG();
		oimage = IvMainPanel.IVPANEL.ographPanel.exportPNG();

		JPanel panel = new JPanel();
		mainScrollPanel = new JScrollPane();
		mainScrollPanel
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mainScrollPanel
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		final ImageIcon a = new ImageIcon(image);
		// final ImageIcon b = new ImageIcon(oimage);
		type = 1;
		imagePanel = new JPanel() {
			public void paintComponent(Graphics gs) {
				Graphics2D g = (Graphics2D) gs;
				super.paintComponent(g);
				this.setPreferredSize(new Dimension(a.getIconWidth(), a
						.getIconHeight()));
				g.drawImage(image, 20, 20, a.getIconWidth(), a.getIconHeight(),
						this);
				imageWidth = a.getIconWidth();
				imageHeight = a.getIconHeight();
			}
		};
		imagePanel.setBackground(Color.white);
		imagePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		mainScrollPanel.setPreferredSize(new Dimension(500, 500));
		mainScrollPanel.setViewportView(imagePanel);
		panel.add(mainScrollPanel);

		return panel;
	}

	/**
	 * Create a panel for the button
	 * 
	 * @return
	 */
	public JPanel createButtonPanel() {
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		closeButton = new JButton(mxResources.get("cancel"));
		printButton = new JButton(mxResources.get("print"));
		nkkButton = new JButton(mxResources.get("print.nkk"));
		otherButton = new JButton(mxResources.get("print.other"));
		zoomInButton = new JButton(mxResources.get("zoomIn"));
		zoomOutButton = new JButton(mxResources.get("zoomOut"));

		buttonPanel.add(closeButton);
		buttonPanel.add(printButton);
		buttonPanel.add(otherButton);
		buttonPanel.add(nkkButton);
		buttonPanel.add(zoomInButton);
		buttonPanel.add(zoomOutButton);
		closeButton.addActionListener(this);
		printButton.addActionListener(this);
		nkkButton.addActionListener(this);
		otherButton.addActionListener(this);
		zoomInButton.addActionListener(this);
		zoomOutButton.addActionListener(this);
		return buttonPanel;
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		final Object o = e.getSource();
		if (o == closeButton) {
			this.setVisible(false);
			this.dispose();
		}
		if (o == zoomInButton) {
			drawImage(false, 0.8f);
		}
		if (o == zoomOutButton) {
			drawImage(false, 1.2f);
		}
		if (o == nkkButton) {// Print the NKK diagram
			if (type == 2 && image != null) {
				type = 1;
				drawImage(true, 1f);
			}
		}
		if (o == otherButton) {// Print the other important person diagram
			if (type == 1 && oimage != null) {
				type = 2;
				drawImage(true, 1f);
			}
		}
		if (o == printButton) {
			this.printActionPerformed(e);
		}
	}

	private void drawImage(boolean reload, float scale) {
		mainScrollPanel.remove(imagePanel);
		if (reload) {
			if (type == 1) {
				final ImageIcon a = new ImageIcon(image);
				imageWidth = a.getIconWidth();
				imageHeight = a.getIconHeight();
			} else {
				final ImageIcon a = new ImageIcon(oimage);
				imageWidth = a.getIconWidth();
				imageHeight = a.getIconHeight();
			}
		}
		imageWidth = imageWidth * scale;
		imageHeight = imageHeight * scale;
		imagePanel = new JPanel() {
			public void paintComponent(Graphics gs) {
				Graphics2D g = (Graphics2D) gs;
				super.paintComponent(g);
				this.setPreferredSize(new Dimension((int) imageWidth,
						(int) imageHeight));
				if (type == 1) {
					g.drawImage(image, 20, 20, (int) imageWidth,
							(int) imageHeight, this);
				} else {
					g.drawImage(oimage, 20, 20, (int) imageWidth,
							(int) imageHeight, this);
				}
			}
		};
		imagePanel.setBackground(Color.white);
		mainScrollPanel.setViewportView(imagePanel);
	}

	private void initPrinterJob() {
		job = PrinterJob.getPrinterJob();
		job.setJobName("Print TextArea");
		job.setPrintable(this);
	}

	private void printActionPerformed(ActionEvent evt) {

		PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
		pras.add(javax.print.attribute.standard.MediaSizeName.ISO_A4);
		pras.add(javax.print.attribute.standard.OrientationRequested.PORTRAIT);

		boolean flag = job.printDialog();
		if (flag) {
			try {
				job.print();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public int print(Graphics g, PageFormat pageFormat, int pageIndex)
			throws PrinterException {
		if (pageIndex > 0) {
			return Printable.NO_SUCH_PAGE;
		}
		Graphics2D g2 = (Graphics2D) g.create();
		g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		imagePanel.print(g2);
		g2.dispose();

		return Printable.PAGE_EXISTS;
	}
}
