package com.fangda.kass.ui.interview;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.fangda.kass.model.interview.InterviewInfo;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.util.mxResources;

/**
 * This class for the main panel of the interview in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class InterviewMainPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5335173471417557544L;
	static JFrame frame = null;
	static InterviewShow interviewShow;
	private QuestionnaireService service;
	static Questionnaire q;
	static String quidName;
	private static LinkedHashMap<String, Object> layoutMap;
	private static LinkedHashMap<String, Object> groupMap;
	private static Interviewcontain inerviewcont;

	/**
	 * The frame container defined
	 */
	public InterviewMainPanel(String labelName) {
		inerviewcont = new Interviewcontain();
		layoutMap = new LinkedHashMap<String, Object>();
		groupMap = new LinkedHashMap<String, Object>();
		quidName = labelName;
		service = new QuestionnaireService();
		q = new Questionnaire();
		q = service.get();
		// TODO Auto-generated constructor stub
		show();
	}

	public void show() {

		JFrame jFrame = createFrame();

		Container contentPane = jFrame.getContentPane();

		JPanel panel = getScrollPanel();

		contentPane.add(panel);
		jFrame.setVisible(true);
	}

	public static JFrame createFrame() {
		frame = new JFrame();
		// frame.getContentPane().add(panel);//
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// frame.setJMenuBar(menuBar);// menu
		frame.setSize(1000, 750);// size defined
		frame.setLocationRelativeTo(null);
		String version = Constants.VERSION;
		frame.setTitle(mxResources.get("KNQ2") + version);
		// frame.addComponentListener(componentListener);
		return frame;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame jFrame = createFrame();

		// interviewShow = new InterviewShow();
		Container contentPane = jFrame.getContentPane();

		JPanel panel = getScrollPanel();

		contentPane.add(panel);
		jFrame.setVisible(true);

	}

	private static JPanel getScrollPanel() {

		final JPanel jPanel11 = new JPanel();

		jPanel11.setLayout(null);

		JScrollPane jScrollPane = new JScrollPane();

		jScrollPane.setBounds(100, 30, 720, 600);

		JPanel qv = getTreePanel();
		jScrollPane.getViewport().add(qv);

		// jScrollPane.getViewport().add(jList);

		jPanel11.add(jScrollPane);

		JButton jButton1 = new JButton(mxResources.get("sure"));
		jButton1.setBounds(0, 0, 90, 30);

		jButton1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				inerviewcont.outxml();

			}

		});
		jPanel11.add(jButton1);
		return jPanel11;

	}

	private static JPanel getTreePanel() {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel jPanel7 = new JPanel(bagLayout);

		if (q == null)
			return jPanel7;
		List<Subsection> subsections = q.getSection().getSubsections();
		int index = 3;
		int sectionIndex = 1;
		for (Subsection ss : subsections) {
			String sectionTitle = mxResources.get("section") + sectionIndex
					+ " " + ss.getTitle();

			JLabel jt = new JLabel(sectionTitle);
			jt.setName(mxResources.get("section") + "_" + ss.getSid());
			index++;
			// subsection's question
			if (ss.getQuestions() != null) {
				int questionIndex = 1;
				for (Question question : ss.getQuestions()) {
					index = addQuestion(jPanel7, c, question, index,
							questionIndex, question.getQid());
					questionIndex++;
				}
			}
			if (ss.getSubSections() != null) {
				int subsectionIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					String showTitle = "  " + sectionIndex + "."
							+ subsectionIndex + " ";
					showTitle = showTitle
							+ (subsection.getTitle() == null ? "" : subsection
									.getTitle());
					// JLabel subsectionTitleLabel =
					// UIHelper.genLabel(showTitle, true, 11);
					JLabel jt1 = new JLabel(showTitle);
					jt1.setName(mxResources.get("section") + "_"
							+ subsection.getSid());
					UIHelper.createMultiLabel(jt1, Font.BOLD, 11, 700);
					UIHelper.addToBagpanel(jPanel7, Color.white, jt1, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
					if (subsection.getDetail() != null) {
						JLabel snodeJta = new JLabel(subsection.getDetail());
						UIHelper.createMultiLabel(snodeJta, 700);
						UIHelper.addToBagpanel(jPanel7, Color.white, snodeJta,
								c, 0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;
					}

					int questionIndex = 1;
					for (Question question : subsection.getQuestions()) {
						index = addQuestion(jPanel7, c, question, index,
								questionIndex, question.getQid());
						questionIndex++;
					}
					subsectionIndex++;
				}
			}
			sectionIndex++;
		}

		jPanel7.setBackground(Color.white);
		jPanel7.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		return jPanel7;

	}

	/**
	 * Base Panel
	 * 
	 * @param panel
	 * @param c
	 *            GridBagConstraints
	 * @param gridx
	 * @param gridy
	 * @param gridwidth
	 * @param gridheight
	 * @param anthor
	 */
	public static void addToBagpanel(JPanel panel, Color color,
			JComponent component, GridBagConstraints c, int gridx, int gridy,
			int gridwidth, int gridheight, int anthor) {
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = gridwidth;
		c.gridheight = gridheight;
		c.anchor = anthor;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5, 5, 5, 5);

		panel.add(component, c);
	}

	public static int addQuestion(JPanel panel, GridBagConstraints c,
			Question question, int index, int questionIndex, String qname) {

		if (!quidName.contains(qname)) {
			return index;
		}

		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		boolean otherCondition = false;
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())) {
				String parentQid = StringUtil.substringBeforeLast(
						question.getQid(), "_");
				String key = parentQid + fquestion.getGroupName();// groupMap
				JPanel jpanel = (JPanel) groupMap.get(key);
				if (jpanel == null) {
					jpanel = new JPanel();
					jpanel.setBackground(Color.white);
					jpanel.setLayout(new FlowLayout());
					jpanel.setBorder(BorderFactory.createTitledBorder(fquestion
							.getGroupName()));
					groupMap.put(key, jpanel);
					UIHelper.addToBagpanel(panel, Color.white, jpanel, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
				if (StringUtil.isNotBlank(question.getDetail())) {
					JLabel titleLabel = UIHelper.genLabel("&nbsp;"
							+ question.getDetail());
					jpanel.add(titleLabel);
				}
				addPanelOption(jpanel, question);
			} else {
				if (isFollowup && fquestion.getLayout() != null) {
					String parentQid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					String key = parentQid + fquestion.getLayout();
					JPanel jpanel = (JPanel) layoutMap.get(key);
					if (jpanel == null) {
						jpanel = new JPanel();
						jpanel.setBackground(Color.white);
						jpanel.setLayout(new FlowLayout());
						layoutMap.put(key, jpanel);
						UIHelper.addToBagpanel(panel, Color.white, jpanel, c,
								0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;
						JLabel titleLabel = UIHelper.genLabel("&nbsp;"
								+ question.getDetail());
						jpanel.add(titleLabel);
						addPanelOption(jpanel, question);
					} else {
						if (question.getDetail() != null
								&& !"".equals(question.getDetail())) {
							JLabel titleLabel = UIHelper.genLabel("&nbsp;"
									+ question.getDetail());
							jpanel.add(titleLabel);
						}
						addPanelOption(jpanel, question);
					}
				} else {
					otherCondition = true;
				}
			}
		} else {
			otherCondition = true;
		}

		if (otherCondition) {
			if (question.getTitle() != null) {
				String title = "";
				if (isFollowup) {
					title = question.getSortNo() + "." + question.getTitle();
				} else {
					title = question.getSortNo() + "." + question.getTitle();
					if ("G".equals(question.getDirection())) {
						title = title + "(Help given)";
					} else if ("R".equals(question.getDirection())) {
						title = title + "(Help received)";
					}
				}
				JLabel jta = new JLabel(title);
				jta.setName("section_" + question.getQid());
				UIHelper.createMultiLabel(jta, Font.BOLD, 11, 700);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
			if (question.getLead() != null) {
				JLabel jta = new JLabel(" " + question.getLead());
				UIHelper.createMultiLabel(jta, 680);
				// JTextArea jta = UIHelper.genTextArea(" " +
				// question.getLead(), 680);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
			if (question.getDetail() != null
					&& !question.getDetail().equals(question.getLead())) {
				JLabel jta = new JLabel(" " + question.getDetail());
				UIHelper.createMultiLabel(jta, 680);
				// JTextArea jta = UIHelper.genTextArea(" " +
				// question.getDetail(), 680);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
			index = addOption(panel, c, question, index);
			if (question.getFollowupQuestions() != null) {
				int followupIndex = 1;
				for (FollowupQuestion fqestion : question
						.getFollowupQuestions()) {
					String qid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					index = addQuestion(panel, c, fqestion, index,
							followupIndex, qid);
					followupIndex++;
				}
			}
		}
		return index;
	}

	public static int addOption(JPanel panel, GridBagConstraints c,
			Question question, int index) {
		// Save the question id and option id
		InterviewInfo info = (InterviewInfo) inerviewcont.questionMap
				.get(question.getQid());
		if (info == null) {
			InterviewInfo tinfo = new InterviewInfo();
			tinfo.setQtype(question.getType());
			inerviewcont.questionMap.put(question.getQid(), tinfo);
			info = tinfo;
		}
		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField(50);
			UIHelper.addToBagpanel(panel, Color.white, field, c, 0, 1 + index,
					1, 1, GridBagConstraints.WEST);
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			UIHelper.addToBagpanel(panel, Color.white, notePane, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;

						String parentQid = StringUtil.substringBeforeLast(
								question.getQid(), "_");
						String key = parentQid;
						ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
								.get(key);
						if (jbtn == null) {
							jbtn = new ButtonGroup();
							inerviewcont.radioGroupMap.put(key, jbtn);
							inerviewcont.parentIdQId
									.put(key, question.getQid());
						}
						JRadioButton radioButton = new JRadioButton(option
								.getDetail().toString());
						radioButton.setBackground(Color.gray);
						// String color = option.getColor();
						UIHelper.addToBagpanel(panel, Color.white, radioButton,
								c, 0, 1 + index, 1, 1, GridBagConstraints.WEST);
						String t = question.getQid()
								+ option.getDetail().toString();

						key = question.getQid() + "_" + option.getDetail();
						String value = option.getValue() == null ? "" : option
								.getValue();

						info.getOptionid().add(key);
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.radioMap.put(key, radioButton);

						jbtn.add(radioButton);

					} else {
						JCheckBox chin = new JCheckBox(optionTitle);
						optionTitle = "      □ " + optionTitle;
						JLabel optionLabel = new JLabel("");
						UIHelper.createMultiLabel(optionLabel, 650);
						UIHelper.addToBagpanel(panel, Color.white, chin, c, 0,
								1 + index, 1, 1, GridBagConstraints.WEST);
						String parentQid = StringUtil.substringBeforeLast(
								question.getQid(), "_");

						String key = question.getQid() + "_"
								+ option.getDetail();
						String value = option.getValue() == null ? "" : option
								.getValue();
						info.getOptionid().add(key);
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.checkboxMap.put(key, chin);
					}

					JLabel optionLabel = new JLabel("");
					UIHelper.createMultiLabel(optionLabel, 650);
					UIHelper.addToBagpanel(panel, Color.white, optionLabel, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);

					index++;
				}
			}
		}
		return index;
	}

	public static void addPanelOption(JPanel panel, Question question) {

		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField(10);
			panel.add(field);
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(noteField);
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;
					} else {
						optionTitle = "      □ " + optionTitle;
					}
					JLabel optionLabel = new JLabel(optionTitle);
					panel.add(optionLabel);
				}
			}
		}
	}
}
