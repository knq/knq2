package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KPeople;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphEventListener;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.util.ColorUtil;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of selecting person in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class SelectPersonDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8571889287370068893L;
	public KassService kassService;
	public QuestionnaireService qService;
	public GraphPanel graphPanel;
	public GraphPanel ographPanel;
	public IvMainPanel ivMainPanel;
	public String color;

	public SelectPersonDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {
		super.initDialog();
		kassService = new KassService();
		qService = new QuestionnaireService();
		ivMainPanel = (IvMainPanel) params.get("ivMainPanel");
		color = ColorUtil.getProgressStatusColor("Mark");
		super.setTitle(mxResources.get("selectInDiagram"));

	}

	@Override
	public JPanel createPanel() {
		JPanel jpanel = new JPanel();
		jpanel.setBackground(Color.white);
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		GridBagConstraints c = new GridBagConstraints();

		int index = 0;
		JPanel questionPanel = createQuestionPanel();
		UIHelper.addToBagpanel(panel, Color.white, questionPanel, c, 0, index,
				1, 1, GridBagConstraints.WEST);

		jpanel.add(panel);
		return jpanel;
	}

	private JPanel createQuestionPanel() {

		JPanel boxPanel = new JPanel();
		boxPanel.setLayout(new VFlowLayout());
		boxPanel.setBackground(Color.white);
		graphPanel = new GraphPanel(this.ivMainPanel, 1);
		graphPanel.setPreferredSize(new Dimension(400, 600));
		graphPanel.setGraphListener(new GraphEventListener(this.ivMainPanel));
		graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
		graphPanel.setSelectPersonMode(color);

		ographPanel = new GraphPanel(this.ivMainPanel, 2);
		ographPanel.setPreferredSize(new Dimension(400, 600));
		ographPanel.setGraphListener(new GraphEventListener(this.ivMainPanel));
		ographPanel.setDisable(GraphPanel.DISABLE_CLICK);
		ographPanel.setSelectPersonMode(color);

		JSplitPane inner = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				graphPanel, ographPanel);
		inner.setOneTouchExpandable(true);
		inner.setDividerLocation(500);
		inner.setDividerSize(6);
		inner.setBorder(null);

		boxPanel.add(inner);
		KASS kass = kassService.get();
		initGraph(kass);
		return boxPanel;
	}

	public void initGraph(KASS kass) {
		KPeople people = kass.getPeople();
		mxCell mxCell = null;
		if (people != null) {
			List<KPPerson> persons = people.getPersons();
			if (persons != null && persons.size() > 0) {
				for (KPPerson person : persons) {
					mxCell cell = null;
					if (person.getIsOther() == 1) {
						cell = ographPanel.drawPerson(person);
					} else {
						cell = graphPanel.drawPerson(person);
					}
					if (person.getPid() == 0) {
						mxCell = cell;
					}
				}
			}
		}
		if (kass.getUnions() != null) {
			List<KUUnion> unions = kass.getUnions().getUnions();
			if (unions != null && unions.size() > 0) {
				for (KUUnion union : unions) {
					if (union.getIsOther() != null && union.getIsOther() == 1) {
						ographPanel.drawUnion(union);
					} else {
						graphPanel.drawUnion(union);
					}
				}
			}
		}
		graphPanel.setAllDefaultColor();
		ographPanel.setAllDefaultColor();
		graphPanel.zoomToCell(mxCell);

		mxCell root = (mxCell) ographPanel.graphComponent.getGraph().getModel()
				.getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		mxCell ocell = null;
		if (c.getChildCount() > 0) {
			ocell = (mxCell) c.getChildAt(0);
		}
		ographPanel.zoomToCell(ocell);
	}

	@Override
	public MessageModel submit() {
		List<Integer> pids = graphPanel.getPersonByColor(color);
		List<Integer> pids1 = ographPanel.getPersonByColor(color);
		pids.addAll(pids1);
		ivMainPanel.secondSelectPerosns(pids);
		return new MessageModel(true, "Submit success!");
	}
}
