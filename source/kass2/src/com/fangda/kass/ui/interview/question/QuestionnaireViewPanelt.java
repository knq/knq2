package com.fangda.kass.ui.interview.question;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.mxgraph.util.mxResources;

/**
 * This class for the Panel of viewing the questionnaire applied to the current
 * interview in the IMS
 * 
 * @author Shuang Ma, Fangfang Zhao
 * 
 */
public class QuestionnaireViewPanelt extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4958106621271951568L;

	public QuestionnaireViewPanelt() {
		// TODO Auto-generated constructor stub

	}

	// private LinkedHashMap<String, Object> layoutMap;
	// private LinkedHashMap<String, Object> groupMap;
	// private QuestionnaireService service;

	public JPanel createPanel() {
		// TODO Auto-generated method stub

		GridBagLayout bagLayout = new GridBagLayout();
		// GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);

		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		// JLabel titleLabel1 = UIHelper.genLabel("getTitle()", true, 13);
		// JLabel titleLabel2 = new JLabel(mxResources.get("language") + ":");
		return panel;

	}
}
