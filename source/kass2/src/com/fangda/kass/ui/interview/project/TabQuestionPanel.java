package com.fangda.kass.ui.interview.project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.JRadioGroup;
import com.fangda.kass.ui.common.MultiLabel;
import com.fangda.kass.ui.common.Toast;
import com.fangda.kass.ui.common.Toast.Style;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.CustomGraph;
import com.fangda.kass.ui.interview.graph.GraphElement;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.ui.interview.support.InitUtil;
import com.fangda.kass.ui.interview.support.QuestionMap;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.RegularUtil;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the Panel for the questions of the section 1, 2, 3, 8 and 9 in
 * the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class TabQuestionPanel extends JPanel {

	private static final long serialVersionUID = -5887002415537111392L;
	protected JPanel container;
	protected IvMainPanel ivMainPanel;
	protected GraphPanel graphPanel;
	private List<String> qids;
	private Question leadQuestion;
	public int type = 0;
	private mxCell cell;
	private QuestionnaireService qService;
	private KassService kassService;

	private LinkedHashMap<String, Object> groupMap = new LinkedHashMap<String, Object>();
	private QuestionMap qm;
	public Question qst;
	private JRadioGroup jradioGroup;
	public JButton cancelBtn, sureBtn, modifyBtn, allBtn, resetBtn;
	private List<JButton> sureBtns = new ArrayList<JButton>();
	// private int buttonSize = 110;
	// private int buttonSizeSmall = 100;
	// private int buttonHeight = 25;
	JTabbedPane tabPanel;
	private String prevQid = "";
	List<JPanel> panels = new ArrayList<JPanel>();
	private int selectPid = -1;
	private int selectUid = -1;
	List<String> dedupLabels = new ArrayList<String>();
	//
	JPanel leadQuestionPanel = null;

	public TabQuestionPanel(IvMainPanel ivMainPanel, GraphPanel graphPanel,
			List<String> qids, int type) {
		this.ivMainPanel = ivMainPanel;
		this.graphPanel = graphPanel;
		this.qids = qids;
		this.type = type;
		qService = new QuestionnaireService();
		kassService = new KassService();
		qm = new QuestionMap();
		initPanel();
	}

	public void initPanel() {

		this.setBackground(Color.white);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		String title = "";
		if (type == 0) {
			title = "New Person";
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder(title),
					BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		}
		container = new JPanel(new BorderLayout());
		container.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		container.add(createPanel(), BorderLayout.CENTER);
		container.setBackground(Color.white);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.add(container);
		this.setVisible(true);

	}

	public JComponent createPanel() {
		if (qids.size() > 1) {

			tabPanel = new JTabbedPane();
			CustomGraph graph = (CustomGraph) graphPanel.graphComponent
					.getGraph();
			mxCell cell = (mxCell) graph.getSelectionCell();

			if (cell != null && cell.getValue() instanceof GraphPerson) {
				GraphPerson person = (GraphPerson) cell.getValue();
				selectPid = person.getId();
			}
			if (cell != null && cell.getValue() instanceof GraphUnion) {
				GraphUnion union = (GraphUnion) cell.getValue();
				selectUid = union.getId();
			}

			if (selectUid > -1) {
				for (String id : qids) {
					Question question = qService.searchQuestion(id);
					JPanel questionPanel = createPanel(question);
					if (selectUid >= 0) {
						int index = qm.checkUnionComplete(question, selectUid);
						String color = ColorUtil.getProgressColor(index);
						tabPanel.add("<html><font color='" + color + "'>"
								+ question.getLabel() + "</font></html>",
								questionPanel);
					} else {
						tabPanel.add(question.getLabel(), questionPanel);
					}
				}
			} else {
				for (String id : qids) {
					Question question = qService.searchQuestion(id);
					JPanel questionPanel = createPanel(question);
					if (selectPid >= 0) {
						int index = qm.checkPersonComplete(question, selectPid);
						String color = ColorUtil.getProgressColor(index);
						tabPanel.add("<html><font color='" + color + "'>"
								+ question.getLabel() + "</font></html>",
								questionPanel);
					} else {
						tabPanel.add(question.getLabel(), questionPanel);
					}
				}
			}

			tabPanel.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					CustomGraph graph = (CustomGraph) graphPanel.graphComponent
							.getGraph();
					mxCell cell = (mxCell) graph.getSelectionCell();
					if (cell != null && cell.getValue() instanceof GraphPerson) {
						// qm.savePersonValue(cell, qService, kassService);
						GraphPerson person = (GraphPerson) cell.getValue();
						selectPid = person.getId();
						// qm.initPersonValue(person.getId(), kassService);
						person.setName(kassService.getPersonName(person.getId()));
						person.setBirthDay(kassService
								.getPersonBirthYear(person.getId()));
						if ("true".equals(kassService.getPersonDeath(person
								.getId()))) {
							person.setIsDeath(1);
							cell.setStyle(graphPanel.getPersonStyle(person));
						} else {
							person.setIsDeath(2);
							cell.setStyle(graphPanel.getPersonStyle(person));
						}
						graph.refresh();
						updateVisiable(person.getId());
						changeTitleColor(selectPid);
					} else if (cell != null
							&& cell.getValue() instanceof GraphUnion) {
						GraphUnion union = (GraphUnion) cell.getValue();
						selectUid = union.getId();
						updateUnionVisiable(union.getId());
						changeTitleColor(selectUid);
					}
				}
			});
			return tabPanel;
		} else if (qids.size() == 1) {
			String qid = qids.get(0);
			leadQuestion = qService.searchQuestion(qid);
			JPanel jpanel = createPanel(leadQuestion);
			if (RegularUtil.isMap(qst)) {
				initAllSelectPerson(graphPanel, jradioGroup);
			}
			return jpanel;
		}
		return null;
	}

	public void initSelectPerson3() {
		if (RegularUtil.isMap(qst)) {
			initAllSelectPerson(graphPanel, jradioGroup);
		}
	}

	public void setCell(mxCell cell) {
		if (cell == null) {
			this.cell = null;
		} else {
			this.cell = cell;
			if (cell.getValue() instanceof GraphElement) {
				GraphElement one = (GraphElement) cell.getValue();
				if (type == 0) {
					if (cell.getValue() instanceof GraphPerson) {
						qm.initPersonSex(cell, kassService);
						updateNewPersonVisiable();
					}

				} else {
					if (cell.getValue() instanceof GraphPerson) {
						qm.initPersonValue(one.getId(), kassService);
						this.selectPid = one.getId();
						this.initAllCompletePerson8();
					} else {
						qm.initUnionValue(one.getId(), kassService);
						this.selectUid = one.getId();
						updateUnionVisiable(selectUid);
						this.initAllCompleteUnion9();
					}
				}
			}
		}
	}

	/**
	 * Question Panel-frame + title
	 * 
	 * @param qst
	 * @return
	 */
	public JPanel createPanel(Question qst) {
		this.qst = qst;
		int width = ivMainPanel.leftQuestionPanel.getWidth() - 50;
		// mainjpanel.setPreferredSize(new Dimension(width+20, 1000));
		// reload
		JPanel panel = new JPanel(new VFlowLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		// panel.setBorder(BorderFactory.createLineBorder(Color.red));
		panel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();

		int index = 0;
		final MultiLabel multiLabel = new MultiLabel(qst.getDetail(),
				Font.BOLD, 13);
		multiLabel.resize(width);
		panel.add(multiLabel.getLabel());
		// UIHelper.addToBagpanel(panel, Color.white, multiLabel.getLabel(), c,
		// 0, index, 1, 1, GridBagConstraints.NORTH);

		index++;
		if (qids.size() == 1) {
			index = createQuestionButtonPreNext(panel, c, index);
		}
		index++;
		JScrollPane questionScroll = createQuestionPanel();

		panel.add(questionScroll);
		// UIHelper.addToBagpanel(panel, Color.white, questionScroll, c, 0,
		// index,
		// 1, 1, GridBagConstraints.NORTHWEST);

		index++;
		index = createQuestionButtonSave(panel, c, index);
		updateQuestionPanel(questionScroll, qst);
		panel.addComponentListener(new ComponentListener() {

			@Override
			public void componentResized(ComponentEvent e) {
				JPanel panel = (JPanel) e.getSource();
				// Question Detail
				multiLabel.resize(panel.getWidth() - 30);
				// questionScroll
				final int heightLeft = ivMainPanel.leftQuestionPanel
						.getHeight();
				int labelHeight = multiLabel.getLabel().getHeight();
				int height = heightLeft - labelHeight - 90;
				for (int i = 0; i < panel.getComponents().length; i++) {
					Component component = panel.getComponent(i);
					if (component instanceof JScrollPane) {
						if (qids.size() > 1) {
							if (qids.size() <= 4) {
								component.setPreferredSize(new Dimension(panel
										.getWidth() - 30, height));
							} else {
								component.setPreferredSize(new Dimension(panel
										.getWidth() - 30, height - 25));
							}
						} else {
							component.setPreferredSize(new Dimension(panel
									.getWidth() - 30, height));
						}
					}
				}
				panel.doLayout();
				if (leadQuestionPanel != null) {
					for (int i = 0; i < leadQuestionPanel.getComponents().length; i++) {
						Component component = leadQuestionPanel.getComponents()[i];
						if (component instanceof JRadioButton) {
							JRadioButton rb = (JRadioButton) component;
							String text = rb.getText();
							text = text.replace("<html>", "");
							text = text.replace("</html>", "");

							int lineHeight = UIHelper.getLineHeight(text,
									panel.getWidth() - 65);
							component.setPreferredSize(new Dimension(panel
									.getWidth() - 65, lineHeight));
							component.doLayout();
						}
					}
					leadQuestionPanel.doLayout();
				}
			}

			@Override
			public void componentMoved(ComponentEvent e) {

			}

			@Override
			public void componentShown(ComponentEvent e) {

			}

			@Override
			public void componentHidden(ComponentEvent e) {

			}

		});
		ivMainPanel.graphPanel.graphComponent.refresh();
		return panel;
	}

	/**
	 * FollowQuestion showing , adapt to Scroll
	 * 
	 * @return
	 */
	private JScrollPane createQuestionPanel() {
		JScrollPane questionScroll = new JScrollPane();
		questionScroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		questionScroll
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		questionScroll.setBackground(Color.gray);
		questionScroll.setBorder(BorderFactory.createEmptyBorder());
		return questionScroll;
	}

	public void updateQuestionPanel(JScrollPane questionScroll, Question qst) {

		questionScroll.getViewport().removeAll();
		int width = ivMainPanel.leftQuestionPanel.getWidth();
		// lead question
		JPanel leadPanel = new JPanel();
		leadPanel
				.setLayout(new VFlowLayout(VFlowLayout.TOP, 0, 0, true, false));
		leadPanel.setBackground(Color.white);
		leadPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		GridBagConstraints c = new GridBagConstraints();
		GridBagLayout bagLayout = new GridBagLayout();

		leadQuestionPanel = new JPanel(bagLayout);
		leadQuestionPanel.setName("question_" + qst.getQid());
		leadQuestionPanel.setBackground(Color.white);
		leadQuestionPanel
				.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		if (qst.getOptions() != null && qst.getOptions().size() > 0) {
			this.addOption(qst, leadQuestionPanel, qst, width - 60, c, 0);
		}
		leadPanel.add(leadQuestionPanel);
		questionScroll.getViewport().add(leadPanel);

		// followQuestion
		JPanel boxPanel = new JPanel();
		// boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
		boxPanel.setLayout(new VFlowLayout(VFlowLayout.TOP, 0, 0, true, false));
		boxPanel.setBackground(Color.white);
		List<String> labels = new ArrayList<String>();
		if (qst.getFollowupQuestions() != null
				&& qst.getFollowupQuestions().size() > 0) {
			for (int i = 0; i < qst.getFollowupQuestions().size(); i++) {
				FollowupQuestion fquestion = qst.getFollowupQuestions().get(i);
				GridBagLayout bagLayout1 = new GridBagLayout();
				JPanel followupPanel = new JPanel(bagLayout1);
				// followupPanel.setBorder(BorderFactory.createTitledBorder(fquestion.getQid()));
				followupPanel.setBackground(Color.white);
				int index = 0;
				if (StringUtils.isNotBlank(fquestion.getGroupName())) {
					String key = qst.getQid() + "_" + fquestion.getGroupName();
					JPanel jpanel = (JPanel) groupMap.get(key);
					if (jpanel == null) {
						jpanel = new JPanel();
						jpanel.setBackground(Color.white);
						jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));
						jpanel.setBorder(BorderFactory
								.createTitledBorder(fquestion.getGroupName()));
						jpanel.setName("group_" + fquestion.getGroupName());
						groupMap.put(key, jpanel);
						UIHelper.addToBagpanel(followupPanel, Color.white,
								jpanel, c, 0, index, 1, 1,
								GridBagConstraints.WEST);
						index++;
					}
					JPanel qPanel = new JPanel();
					qPanel.setBackground(Color.white);

					qPanel.setLayout(new BoxLayout(qPanel, BoxLayout.Y_AXIS));
					qPanel.setName("question_" + fquestion.getQid());
					qPanel.setBorder(BorderFactory.createEmptyBorder());
					// qPanel.setBorder(BorderFactory.createTitledBorder(fquestion.getQid()));
					if (StringUtil.isNotBlank(fquestion.getDetail())) {
						JLabel titleLabel = new JLabel(""
								+ fquestion.getDetail());
						titleLabel.setPreferredSize(new Dimension(width - 90,
								30));
						titleLabel.setBorder(BorderFactory.createEmptyBorder());
						qPanel.add(titleLabel);
					}
					this.addPanelOption(qst, qPanel, fquestion, width - 50);
					jpanel.add(qPanel);
					panels.add(qPanel);
					if (labels.contains(fquestion.getLabel())) {
						dedupLabels.add(fquestion.getLabel());
					} else {
						labels.add(fquestion.getLabel());
					}
				} else {
					followupPanel.setName("question_" + fquestion.getQid());
					String t = fquestion.getDetail();
					if (StringUtils.isNotBlank(t)) {
						JLabel detailLabel = new JLabel(t);
						UIHelper.createMultiLabel(detailLabel, Font.NORMAL, 13,
								width - 60);
						UIHelper.addToBagpanel(followupPanel, Color.white,
								detailLabel, c, 0, index, 1, 1,
								GridBagConstraints.WEST);
						index++;
					}
					index = addOption(qst, followupPanel, fquestion,
							width - 60, c, index);
					index++;
					panels.add(followupPanel);
					if (labels.contains(fquestion.getLabel())) {
						dedupLabels.add(fquestion.getLabel());
					} else {
						labels.add(fquestion.getLabel());
					}
				}
				boxPanel.add(followupPanel);
			}
			questionScroll.getViewport().add(boxPanel);
		}
		questionScroll.getViewport().repaint();
	}

	public void addPanelOption(Question qst, JPanel panel,
			final Question question, int width) {
		FollowupQuestion fquestion = (FollowupQuestion) question;
		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField();
			field.setBorder(BorderFactory.createLineBorder(Color.gray));
			field.setName(question.getQid());
			int w = 15;
			if (fquestion.getLength() > 0) {
				w = fquestion.getLength();
			}
			field.setPreferredSize(new Dimension(w, 30));
			panel.add(field);
			qm.addQuestion(question, field);
			if (StringUtils.equals(fquestion.getDataType(), "int")) {
				field.addKeyListener(intKeylistener);
			} else if (StringUtils.equals(fquestion.getDataType(), "decimal")) {
				field.addKeyListener(decimalKeylistener);
			}
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setName(question.getQid());
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(notePane);
			qm.addQuestion(question, noteField);
		} else if ("B".equals(question.getType())
				|| "D".equals(question.getType())) {//
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					JCheckBox cb = new JCheckBox();
					cb.setName(option.getOid());
					cb.setBackground(Color.white);
					cb.setText("<html>" + option.getDetail() + "</html>");
					cb.setPreferredSize(new Dimension(width - 20, 30));
					if (RegularUtil.isPersonOrUnion(qst)) {
						cb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JCheckBox jb = (JCheckBox) e.getSource();
								// Show or hide
								if (jb.isSelected()) {
									if (selectUid > -1) {
										qm.isCheckShow(selectUid, question,
												"true", panels, 2);
									} else {
										qm.isCheckShow(selectPid, question,
												"true", panels, 1);
									}
								} else {
									if (selectUid > -1) {
										qm.isCheckShow(selectUid, question,
												"false", panels, 2);
									} else {
										qm.isCheckShow(selectPid, question,
												"false", panels, 1);
									}
								}
							}
						});
					}
					panel.add(cb);
					qm.addQuestion(question, cb);
				}
			}
		} else if ("C".equals(question.getType())
				|| "Q".equals(question.getType())) {
			if (question.getOptions() != null) {
				JRadioGroup bg = new JRadioGroup();
				bg.setName(question.getLabel());
				int i = 0;
				for (Option option : question.getOptions()) {
					if (option.getLayout() != null
							&& option.getLayout().equals("R")) {
						panel.setLayout(new BoxLayout(panel,
								BoxLayout.LINE_AXIS));
					}
					JRadioButton rb = new JRadioButton();
					rb.setBackground(this.getColor(qst, question.getOptions()
							.size(), i));
					rb.setText("<html>" + option.getDetail() + "</html>");
					rb.setActionCommand(option.getValue());
					rb.setName(option.getOid());
					if (RegularUtil.isPersonOrUnion(qst)) {
						rb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JRadioButton jb = (JRadioButton) e.getSource();
								// Show or hide
								if (selectUid > -1) {
									qm.isCheckShow(selectUid, question,
											jb.getActionCommand(), panels, 2);
								} else {
									qm.isCheckShow(selectPid, question,
											jb.getActionCommand(), panels, 1);
								}
							}
						});
					}
					panel.add(rb);
					bg.add(rb);
					i++;
				}
				qm.addQuestion(question, bg);
			}
		}
	}

	/**
	 * Add the option
	 * 
	 * @param panel
	 * @param c
	 * @param question
	 * @param index
	 * @return
	 */
	public int addOption(Question qst, JPanel panel, final Question question,
			int width, GridBagConstraints c, int index) {

		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField();
			field.setBorder(BorderFactory.createLineBorder(Color.gray));
			field.setName(question.getQid());
			int w = width - 20;
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (fquestion.getLength() > 0) {
				w = fquestion.getLength() * 10;
				if (w > width - 20) {
					w = width - 20;
				}
				field.setPreferredSize(new Dimension(w, 30));
				panel.add(field, new GridBagConstraints(0, index, 1, 1, 1.0,
						1.0, GridBagConstraints.WEST, GridBagConstraints.WEST,
						new Insets(0, 0, 0, 0), 0, 0));
			} else {
				field.setPreferredSize(new Dimension(w, 30));
				UIHelper.addToBagpanel(panel, Color.white, field, c, 0, index,
						1, 1, GridBagConstraints.WEST);
			}
			qm.addQuestion(question, field);
			if (StringUtils.equals(fquestion.getDataType(), "int")) {
				field.addKeyListener(intKeylistener);
			} else if (StringUtils.equals(fquestion.getDataType(), "decimal")) {
				field.addKeyListener(decimalKeylistener);
			}
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setName(question.getQid());
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			notePane.setPreferredSize(new Dimension(width - 20, 200));
			UIHelper.addToBagpanel(panel, Color.white, notePane, c, 0, index,
					1, 1, GridBagConstraints.WEST);
			qm.addQuestion(question, noteField);
			index++;
		} else if ("B".equals(question.getType())
				|| "D".equals(question.getType())) {//
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					JCheckBox cb = new JCheckBox();
					cb.setName(option.getOid());
					cb.setBackground(Color.white);
					cb.setText("<html>" + option.getDetail() + "</html>");
					cb.setPreferredSize(new Dimension(width - 20, 30));
					if (option.getNote() != null
							&& !"".equals(option.getNote())) {
						cb.setToolTipText(option.getNote());
					}
					UIHelper.addToBagpanel(panel, Color.white, cb, c, 0, index,
							1, 1, GridBagConstraints.WEST);
					qm.addQuestion(question, cb);
					index++;
					// if (cb.isSelected()) {
					// if(selectUid > -1) {
					// qm.isCheckShow(selectUid, question, "true",
					// panels, 2);
					// } else {
					// qm.isCheckShow(selectPid, question, "true",
					// panels, 1);
					// }
					// } else {
					// if(selectUid > -1) {
					// qm.isCheckShow(selectUid, question, "false",
					// panels, 2);
					// } else {
					// qm.isCheckShow(selectPid, question, "false",
					// panels, 1);
					// }
					// }
					if (RegularUtil.isPersonOrUnion(qst)) {
						cb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JCheckBox jb = (JCheckBox) e.getSource();
								// 设置是否显示
								if (jb.isSelected()) {
									if (selectUid > -1) {
										qm.isCheckShow(selectUid, question,
												"true", panels, 2);
									} else {
										qm.isCheckShow(selectPid, question,
												"true", panels, 1);
									}
								} else {
									if (selectUid > -1) {
										qm.isCheckShow(selectUid, question,
												"false", panels, 2);
									} else {
										qm.isCheckShow(selectPid, question,
												"false", panels, 1);
									}
								}
							}
						});
					}
				}
			}
		} else if ("C".equals(question.getType())
				|| "Q".equals(question.getType())) {
			if (question.getOptions() != null) {
				final JRadioGroup bg = new JRadioGroup();
				int i = 0;
				for (Option option : question.getOptions()) {
					JRadioButton rb = new JRadioButton();
					rb.setActionCommand(option.getValue());
					rb.setName(option.getOid());
					rb.setBackground(this.getColor(qst, question.getOptions()
							.size(), i));
					rb.setText("<html>" + option.getDetail() + "</html>");
					rb.setPreferredSize(new Dimension(width, UIHelper
							.getLineHeight(option.getDetail(), width)));

					if (option.getNote() != null
							&& !"".equals(option.getNote())) {
						rb.setToolTipText(option.getNote());
					}
					if (option.getDefaultValue() != null
							&& "true".equals(option.getDefaultValue())) {
						rb.setSelected(true);
					}
					UIHelper.addToBagpanel(panel, Color.white, rb, c, 0, index,
							1, 1, GridBagConstraints.WEST);
					if (RegularUtil.isMap(qst)) {
						rb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JRadioButton jb = (JRadioButton) e.getSource();
								// highline same person
								if (ivMainPanel.graphPanel.getDisable() == GraphPanel.DISABLE_NOCLICK) {
									ivMainPanel.graphPanel.highLine(ColorUtil
											.geWebColor(jb.getBackground()));
									ivMainPanel.ographPanel.highLine(ColorUtil
											.geWebColor(jb.getBackground()));
								} else {
									ivMainPanel.graphPanel
											.setSelectPersonMode(ColorUtil
													.geWebColor(jb
															.getBackground()));
									ivMainPanel.ographPanel
											.setSelectPersonMode(ColorUtil
													.geWebColor(jb
															.getBackground()));
								}
							}
						});
					} else if (RegularUtil.isPersonOrUnion(qst)) {
						rb.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								JRadioButton jb = (JRadioButton) e.getSource();
								// Show or hide
								if (selectUid > -1) {
									qm.isCheckShow(selectUid, question,
											jb.getActionCommand(), panels, 2);
								} else {
									qm.isCheckShow(selectPid, question,
											jb.getActionCommand(), panels, 1);
								}
							}
						});
					}
					bg.add(rb);
					i++;
					index++;
				}
				qm.addQuestion(question, bg);
				if (RegularUtil.isMap(qst)) {
					jradioGroup = bg;
					// initAllSelectPerson(graphPanel, bg);
				}
			}
		}
		return index;
	}

	public Color getColor(Question question, int size, int index) {
		if (RegularUtil.isMap(question)) {
			Color color = ColorUtil.geColor(size, index);
			return color;
		}
		return Color.white;
	}

	/**
	 * Button
	 * 
	 * @return
	 */
	private int createQuestionButtonSave(JPanel panel, GridBagConstraints c,
			int index) {
		JPanel jpanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpanel.setPreferredSize(new Dimension(150, 28));
		jpanel.setBackground(Color.white);
		// cancel button is use in add new person
		cancelBtn = new JButton(mxResources.get("cancel"));
		cancelBtn.setToolTipText(mxResources.get("cancel"));
		// cancelBtn.setPreferredSize(new Dimension(buttonSizeSmall,
		// buttonHeight));
		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (RegularUtil.isMap(qst)) {
					modifyBtn.setVisible(true);
					allBtn.setVisible(true);
					cancelBtn.setVisible(false);
					resetBtn.setVisible(false);
					ivMainPanel.graphPanel
							.setDisable(GraphPanel.DISABLE_NOCLICK);
					ivMainPanel.ographPanel
							.setDisable(GraphPanel.DISABLE_NOCLICK);
					TabQuestionPanel.this.initAllSelectPerson(graphPanel,
							jradioGroup);
				} else {
					if (TabQuestionPanel.this.cell != null) {
						Object o = TabQuestionPanel.this.cell.getValue();
						if (o instanceof GraphPerson) {
							GraphPerson one = (GraphPerson) o;
							KPPerson person = kassService.getPerson(one.getId());
							if (person == null) {
								mxCell[] cells = new mxCell[] { TabQuestionPanel.this.cell };
								graphPanel.graphComponent.getGraph()
										.removeCells(cells);
							}
						}
					}
					graphPanel.clearAllCell();
					ivMainPanel.leftQuestionPanel.getViewport().removeAll();
					ivMainPanel.leftQuestionPanel.getViewport().repaint();
					// Add a new page

					if (StringUtils.isBlank(prevQid)) {
						if (RegularUtil.isPersonOrUnion(qst)) {
							ivMainPanel.openSpecialQuestionPanel(qst.getQid());
						}
					} else {
						ivMainPanel.openSpecialQuestionPanel(prevQid);
					}
				}
			}
		});
		// section 3 section8 is need this button to save question
		sureBtn = new JButton(mxResources.get("sure"));
		sureBtns.add(sureBtn);
		sureBtn.setToolTipText(mxResources.get("sure"));
		// sureBtn.setPreferredSize(new Dimension(buttonSizeSmall, 20));
		sureBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (RegularUtil.isMap(qst)) {
					saveSelectPerson();
					return;
				}
				if (cell == null) {
					Toast.makeText(ivMainPanel.kmain.frame,
							"Please select graph first.", Style.SUCCESS)
							.display();
					return;
				}
				if (cell.getValue() instanceof GraphPerson) {
					// checked first name is same
					if (type == 0) {
						GraphPerson one = (GraphPerson) cell.getValue();
						if (!ivMainPanel.checkSameName(one.getId(),
								one.getSex(), qm, cell)) {
							return;
						}
					}

					int s = qm.savePersonValue(graphPanel, cell, qService,
							kassService, panels, dedupLabels);
					if (s == -1) {
						Toast.makeText(ivMainPanel.kmain.frame,
								"Ego can't deceased", Style.SUCCESS).display();
						return;
					}
					if (type == 0) {
						GraphPerson one = (GraphPerson) cell.getValue();
						KPPerson person = kassService.getPerson(one.getId());
						GraphPerson gp = (GraphPerson) cell.getValue();
						gp.setName(kassService.getPersonName(person.getPid()));
						gp.setBirthDay(kassService.getPersonBirthYear(person
								.getPid()));
						if ("true".equals(kassService.getPersonDeath(gp.getId()))) {
							gp.setIsDeath(1);
							cell.setStyle(graphPanel.getPersonStyle(gp));
						} else {
							gp.setIsDeath(2);
							cell.setStyle(graphPanel.getPersonStyle(gp));
						}
						graphPanel.graphComponent.getGraph().refresh();
						ivMainPanel.leftQuestionPanel.getViewport().removeAll();

						// Add a new page
						ivMainPanel.openDefaultQuestionPanel();
					} else {
						GraphPerson one = (GraphPerson) cell.getValue();
						one.setName(kassService.getPersonName(one.getId()));
						one.setBirthDay(kassService.getPersonBirthYear(one
								.getId()));
						KPPerson person = kassService.getPerson(one.getId());
						one.setSex(person.getSex());
						if ("true".equals(kassService.getPersonDeath(one
								.getId()))) {
							one.setIsDeath(1);
							cell.setStyle(graphPanel.getPersonStyle(one));
						} else {
							one.setIsDeath(2);
							cell.setStyle(graphPanel.getPersonStyle(one));
						}
						graphPanel.graphComponent.getGraph().refresh();
					}
					TabQuestionPanel.this.initAllCompletePerson8();
				} else {
					qm.saveUnionValue(cell, qService, kassService, panels);
					TabQuestionPanel.this.initAllCompleteUnion9();
				}
				ivMainPanel.updatePercent();
				Toast.makeText(ivMainPanel.kmain.frame, "Save successfully.",
						Style.SUCCESS).display();
				if (StringUtils.isBlank(prevQid)) {
					if (RegularUtil.isPersonOrUnion(qst)) {
						ivMainPanel.openSpecialQuestionPanel(qst.getQid());
					}
				} else {
					ivMainPanel.openSpecialQuestionPanel(prevQid);
				}
			}
		});
		allBtn = new JButton(mxResources.get("all"));
		allBtn.setToolTipText(mxResources.get("all"));
		// allBtn.setPreferredSize(new Dimension(buttonSizeSmall, 20));
		allBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// ivMainPanel.graphPanel.showRemark(qst.getQid(), true);
				// ivMainPanel.ographPanel.showRemark(qst.getQid(), true);
				ivMainPanel.graphPanel.clearHighLine();
				ivMainPanel.ographPanel.clearHighLine();
			}
		});
		resetBtn = new JButton(mxResources.get("reset"));
		resetBtn.setToolTipText(mxResources.get("reset"));
		// resetBtn.setPreferredSize(new Dimension(buttonSizeSmall, 20));
		resetBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ivMainPanel.graphPanel.clearSelected();
				ivMainPanel.ographPanel.clearSelected();
				ivMainPanel.graphPanel.setAllDefaultColor();
				ivMainPanel.ographPanel.setAllDefaultColor();
			}
		});
		jpanel.add(cancelBtn);
		jpanel.add(sureBtn);
		jpanel.add(allBtn);
		jpanel.add(resetBtn);

		panel.add(jpanel);
		index++;
		initQuestionButton();
		return index;
	}

	/**
	 * Initialize the UI
	 * 
	 * @return
	 */
	private void initQuestionButton() {
		if (modifyBtn == null) {
			this.createModifyBtn();
		}
		resetBtn.setVisible(false);
		allBtn.setVisible(false);
		cancelBtn.setVisible(false);
		sureBtn.setVisible(false);
		modifyBtn.setVisible(false);
		if (type == 0) {
			cancelBtn.setVisible(true);
			sureBtn.setVisible(true);
			sureBtn.setEnabled(false);
			ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_NEW);
			ivMainPanel.ographPanel.setDisable(GraphPanel.DISABLE_NEW);
			modifyBtn.setVisible(false);
		} else {
			KQuestion kquestion = kassService.getCompleteField(qst.getQid());
			if (kquestion != null && "1".equals(kquestion.getCompleted())) {// complete
				sureBtn.setVisible(false);
				modifyBtn.setVisible(true);
				if (RegularUtil.isMap(qst)) {
					allBtn.setVisible(true);
				}
				ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_NOCLICK);
				ivMainPanel.ographPanel.setDisable(GraphPanel.DISABLE_NOCLICK);
			} else {
				if (RegularUtil.isMap(qst)) {
					resetBtn.setVisible(true);
					cancelBtn.setVisible(true);
				}
				if (type == 0) {
					sureBtn.setVisible(true);
				}
				// if uncompleted, then draw
				if (RegularUtil.isDraw(qst)) {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_NEW);
					ivMainPanel.ographPanel.setDisable(GraphPanel.DISABLE_NEW);
				} else if (RegularUtil.isMap(qst)) {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
					ivMainPanel.ographPanel
							.setDisable(GraphPanel.DISABLE_CLICK);
				} else {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
					ivMainPanel.ographPanel
							.setDisable(GraphPanel.DISABLE_CLICK);
				}
			}
			if (RegularUtil.isPersonOrUnion(qst)) {
				cancelBtn.setVisible(true);
				sureBtn.setVisible(true);
				// If completed, then disable
				ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
				ivMainPanel.ographPanel.setDisable(GraphPanel.DISABLE_CLICK);
			}
		}
	}

	/**
	 * Previous or Next
	 * 
	 * @return
	 */
	private int createQuestionButtonPreNext(JPanel panel, GridBagConstraints c,
			int index) {
		if (type == 0) {
			return index;
		}
		JPanel jpanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpanel.setPreferredSize(new Dimension(200, 28));
		jpanel.setBackground(Color.white);
		JButton previousBtn = new JButton("<<");
		JButton nextBtn = new JButton(">>");
		// previousBtn.setPreferredSize(new Dimension(buttonSizeSmall, 20));
		previousBtn.setToolTipText(mxResources.get("previous"));
		// nextBtn.setPreferredSize(new Dimension(buttonSizeSmall, 20));
		nextBtn.setToolTipText(mxResources.get("next"));
		previousBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				previousPage();
			}
		});
		nextBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextPage();
			}
		});

		jpanel.add(previousBtn);
		if (modifyBtn == null) {
			this.createModifyBtn();
		}
		jpanel.add(modifyBtn);
		jpanel.add(nextBtn);

		// UIHelper.addToBagpanel(panel, Color.white, jpanel, c, 0, index, 1, 1,
		// GridBagConstraints.WEST);
		panel.add(jpanel);
		index++;
		return index;
	}

	private void createModifyBtn() {
		if (modifyBtn != null) {
			return;
		}
		modifyBtn = new JButton(mxResources.get("markOpen"));
		// modifyBtn.setPreferredSize(new Dimension(buttonSize, 20));
		modifyBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (RegularUtil.isDraw(qst)) {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_NEW);
					ivMainPanel.ographPanel.setDisable(GraphPanel.DISABLE_NEW);
				} else if (RegularUtil.isMap(qst)) {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
					ivMainPanel.ographPanel
							.setDisable(GraphPanel.DISABLE_CLICK);
				} else {
					ivMainPanel.graphPanel.setDisable(GraphPanel.DISABLE_CLICK);
					ivMainPanel.ographPanel
							.setDisable(GraphPanel.DISABLE_CLICK);
				}
				modifyBtn.setVisible(false);
				if (RegularUtil.isDraw(qst)) {
					sureBtn.setVisible(false);
				} else if (RegularUtil.isMap(qst)) {
					sureBtn.setVisible(false);
					allBtn.setVisible(false);
					cancelBtn.setVisible(true);
					resetBtn.setVisible(true);
				} else {
					sureBtn.setVisible(true);
				}
				KQuestion kquestion = kassService.getCompleteField(qst.getQid());
				kquestion.setCompleted("2");
				kassService.save();
			}
		});
	}

	public void previousPage() {
		if (qst.getSubsection().startsWith("SS_001")
				|| qst.getSubsection().startsWith("SS_002")) {
			if (beforeGotoSection12()) {// complete
				ivMainPanel.prevPage(qst.getQid());
			} else {// no complete
				ivMainPanel.prevPage(qst.getQid());
			}
		} else {
			if (beforeGotoSection3()) {
				ivMainPanel.prevPage(qst.getQid());
			}
		}
	}

	public void nextPage() {
		if (qst.getSubsection().startsWith("SS_001")
				|| qst.getSubsection().startsWith("SS_002")) {
			beforeGotoSection12();
			ivMainPanel.nextPage(qst.getQid());
		} else {
			if (beforeGotoSection3()) {
				ivMainPanel.nextPage(qst.getQid());
			}
		}
	}

	/**
	 * In section 1-2, Check for the Previous and Next
	 * 
	 * @return
	 */
	private boolean beforeGotoSection12() {
		KQuestion kquestion = kassService.getCompleteField(qst.getQid());
		if ("1".equals(kquestion.getCompleted())) {// 已经完成的
			return true;
		}
		String tip = "The question is completed?";
		Object[] el = { "Yes", "No" };
		int result = JOptionPane.showOptionDialog(null, tip, "Select Option",
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				el, el[1]);

		if (result == JOptionPane.YES_OPTION) {
			kquestion.setCompleted("1");
			kquestion.setIsMarked(true);
			kassService.save();
			return true;
		} else if (result == JOptionPane.NO_OPTION) {
			return false;
		}
		return true;
	}

	/**
	 * In the section 3, check for the Previous and Next
	 * 
	 * @return
	 */
	private boolean beforeGotoSection3() {
		this.saveSelectPerson();
		KQuestion kquestion = kassService.getCompleteField(qst.getQid());
		if ("1".equals(kquestion.getCompleted())) {// if completed
			return true;
		}
		int state = qm.checkComplete(qst.getQid());
		if (state == 1) {
			kquestion.setCompleted("1");
			kquestion.setIsMarked(true);
			kassService.save();
			return true;
		} else {
			kquestion.setCompleted(state + "");
			kassService.save();
		}
		String tip = "The question has not been completed, do you want to leave away this question?";
		Object[] el = { "Yes", "No" };
		int result = JOptionPane.showOptionDialog(null, tip, "Select Option",
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				el, el[1]);

		if (result == JOptionPane.YES_OPTION) {
			return true;
		} else if (result == JOptionPane.NO_OPTION) {
			return false;
		}
		return true;
	}

	/**
	 * Clear all values
	 */
	public void clearAllValue() {
		qm.clearComponent();
	}

	/**
	 * Save in the section 3
	 * 
	 * @param graphPanel
	 */
	public void saveSelectPerson() {
		if (jradioGroup == null)
			return;
		Enumeration<AbstractButton> radios = jradioGroup.getElements();
		while (radios.hasMoreElements()) {
			AbstractButton ab = radios.nextElement();
			String color = ColorUtil.geWebColor(ab.getBackground());
			List<Integer> pids = ivMainPanel.graphPanel.getPersonByColor(color);
			List<Integer> pids2 = ivMainPanel.ographPanel
					.getPersonByColor(color);
			pids.addAll(pids2);

			for (int i = 0; i < pids.size(); i++) {
				int a = pids.get(i);
				KQQuestionField field = kassService.getPersonField(a,
						qst.getLabel());
				if (field != null) {
					String value = ab.getActionCommand();
					field.setValue(value);
				}
			}
		}
		// kassService.save();
		InitUtil.modifyPersonDefault(qService, kassService);
		kassService.save();
	}

	/**
	 * Unselected in the section 3
	 * 
	 * @param graphPanel
	 */
	public void saveUnSelectPerson(GraphPanel graphPanel, int qid) {
		KQQuestionField field = kassService.getPersonField(qid, qst.getLabel());
		field.setValue(Constants.DEFAULT_NA);
		kassService.save();
	}

	/**
	 * Initialize the selected person
	 * 
	 * @param graphPanel
	 * @param value
	 */
	public void initSelectPerson(GraphPanel graphPanel, String value) {
		List<KPPerson> persons = kassService.get().getPeople().getPersons();
		List<Integer> pids = new ArrayList<Integer>();
		for (KPPerson person : persons) {
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					qst.getLabel());
			if (field != null && StringUtils.equals(value, field.getValue())) {
				pids.add(person.getPid());
			}
		}
		graphPanel.initSelectPerson(pids);
	}

	/**
	 * 
	 * @param graphPanel
	 * @param value
	 */
	public void initAllSelectPerson(GraphPanel graphPanel, JRadioGroup bg) {
		//
		ivMainPanel.graphPanel.setAllDefaultColor();
		ivMainPanel.ographPanel.setAllDefaultColor();
		initDNAPerson();// DNA
		List<KPPerson> persons = kassService.get().getPeople().getPersons();
		Enumeration<AbstractButton> buttons = bg.getElements();
		while (buttons.hasMoreElements()) {
			JRadioButton button = (JRadioButton) buttons.nextElement();
			String color = ColorUtil.geWebColor(button.getBackground());
			String value = button.getActionCommand();

			List<Integer> pids = new ArrayList<Integer>();
			for (KPPerson person : persons) {
				KQQuestionField field = kassService.getPersonField(
						person.getPid(), qst.getLabel());
				if (field != null
						&& StringUtils.equals(value, field.getValue())) {
					pids.add(person.getPid());
				}
			}
			ivMainPanel.graphPanel.initSelectPerson(pids, color,
					button.getText());
			ivMainPanel.ographPanel.initSelectPerson(pids, color,
					button.getText());
		}
	}

	public void initAllCompletePerson8() {
		//
		ivMainPanel.graphPanel.setAllDefaultColor();
		ivMainPanel.ographPanel.setAllDefaultColor();
		Subsection subsection = qService.searchSubsection(qst.getSubsection());
		List<KPPerson> persons = kassService.getKPersons();
		if (persons == null)
			return;
		if (subsection.getQuestions() == null)
			return;
		List<Integer> pids = new ArrayList<Integer>();
		for (KPPerson person : persons) {
			boolean isComplete = true;
			for (Question question : subsection.getQuestions()) {
				int state = qm.checkPersonComplete(question, person.getPid());
				if (state != 1) {
					isComplete = false;
					break;
				}
			}
			if (isComplete)
				pids.add(person.getPid());
		}
		String color = ColorUtil.getProgressColor(1);
		ivMainPanel.graphPanel.initSelectPerson(pids, color, "");
		ivMainPanel.ographPanel.initSelectPerson(pids, color, "");
	}

	public void initAllCompleteUnion9() {
		//
		ivMainPanel.graphPanel.setAllDefaultColor();
		ivMainPanel.ographPanel.setAllDefaultColor();
		Subsection subsection = qService.searchSubsection(qst.getSubsection());
		List<KUUnion> unions = kassService.getKUUnions();
		if (unions == null)
			return;
		if (subsection.getQuestions() == null)
			return;
		List<Integer> pids = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			boolean isComplete = true;
			for (Question question : subsection.getQuestions()) {
				int state = qm.checkUnionComplete(question, union.getId());
				if (state != 1) {
					isComplete = false;
					break;
				}
			}
			if (isComplete)
				pids.add(union.getId());
		}
		String color = ColorUtil.getProgressColor(1);
		ivMainPanel.graphPanel.initSelectUnion(pids, color, "");
		ivMainPanel.ographPanel.initSelectUnion(pids, color, "");
	}

	/**
	 * Selected a person in the view mode
	 * 
	 * @param graphPanel
	 * @param bg
	 */
	public void clickSelectPerson(String c) {
		Enumeration<AbstractButton> buttons = jradioGroup.getElements();
		while (buttons.hasMoreElements()) {
			JRadioButton button = (JRadioButton) buttons.nextElement();
			String color = ColorUtil.geWebColor(button.getBackground());
			if (StringUtils.equalsIgnoreCase(c, color)) {
				button.setSelected(true);
			}
		}
		graphPanel.highLine(c);
	}

	/**
	 * Initialize all person
	 */
	public void initAllPersonValue3() {
		if (jradioGroup == null)
			return;

	}

	public String getSelectPersonTip3(String c) {
		Enumeration<AbstractButton> buttons = jradioGroup.getElements();
		while (buttons.hasMoreElements()) {
			JRadioButton button = (JRadioButton) buttons.nextElement();
			String color = ColorUtil.geWebColor(button.getBackground());
			if (StringUtils.equalsIgnoreCase(c, color)) {
				String value = button.getText();
				return value;
			}
		}
		return "";
	}

	// Section 3
	public void initDNAPerson() {
		List<KPPerson> persons = kassService.get().getPeople().getPersons();
		List<Integer> pids = new ArrayList<Integer>();
		for (KPPerson person : persons) {
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					qst.getLabel());
			if (Constants.DEFAULT_DNA.equals(field.getValue())) {
				pids.add(person.getPid());
			}
		}
		ivMainPanel.graphPanel.lockPerson(pids);
		ivMainPanel.ographPanel.lockPerson(pids);
	}

	/**
	 * Tab
	 * 
	 * @param q
	 */
	public void showTabIndex(Question q) {
		if (q == null) {
			return;
		}
		if (this.qids.size() > 1 && tabPanel != null) {
			for (int i = 0; i < qids.size(); i++) {
				String qid = qids.get(i);
				if (StringUtils.equals(qid, q.getQid())) {
					tabPanel.setSelectedIndex(i);
				}
			}
		}
	}

	public void changeTab() {
		if (cell == null) {
			return;
		}
		if (qids.size() > 1) {
			if (cell != null && cell.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) cell.getValue();
				qm.initDNAPerson(gp.getId(), qids);
				tabPanel.removeAll();
				for (String id : qids) {
					Question question = qService.searchQuestion(id);
					JPanel questionPanel = createPanel(question);
					tabPanel.add(question.getLabel(), questionPanel);
				}
				tabPanel.repaint();
			}
		}
	}

	public String getPrevQid() {
		return prevQid;
	}

	public void setPrevQid(String prevQid) {
		this.prevQid = prevQid;
	}

	JPanel deathYearParentPanel = null;// death Panel 父类
	JPanel cbIsYODUnknownPanel = null;//
	JCheckBox cbIsYODUnknownCb = null;//
	JPanel deathYearPanel = null;
	JTextField deathYearText = null;
	JPanel cbIsYODApproxPanel = null;
	JCheckBox cbIsYODApproxCb = null;

	JPanel birthYearParentPanel = null;// birth Panel 父类
	JPanel cbIsYOBUnknownPanel = null;//
	JCheckBox cbIsYOBUnknownCb = null;//
	JPanel birthYearPanel = null;
	JTextField birthYearText = null;
	JPanel cbIsYOBApproxPanel = null;
	JCheckBox cbIsYOBApproxCb = null;

	JCheckBox cbIsDeceasedCb = null;

	private void updateSureEnable() {
		boolean flag1 = false;
		boolean flag2 = false;
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();
			String id = name.replace("question_", "");
			Question question = qService.searchQuestion(id);
			if ("Surname".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					JTextField surenameTextField = (JTextField) jpanel
							.getComponent(1);
					if (!"".equals(surenameTextField.getText())) {
						flag1 = true;
					}
				}
			}
			if ("Othernames".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					JTextField othernameTextField = (JTextField) jpanel
							.getComponent(1);
					if (!"".equals(othernameTextField.getText())) {
						flag2 = true;
					}
				}
			}
		}
		if (flag1 && flag2) {
			for (JButton btn : sureBtns) {
				btn.setEnabled(true);
			}
		} else {
			for (JButton btn : sureBtns) {
				btn.setEnabled(false);
			}
		}

	}

	public void updateNewPersonVisiable() {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();
			String id = name.replace("question_", "");
			Question question = qService.searchQuestion(id);
			if ("Surname".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					final JTextField surenameTextField = (JTextField) jpanel
							.getComponent(1);
					surenameTextField.addKeyListener(new KeyListener() {
						@Override
						public void keyPressed(KeyEvent arg0) {
						}

						@Override
						public void keyReleased(KeyEvent event) {
							updateSureEnable();
						}

						@Override
						public void keyTyped(KeyEvent arg0) {
						}
					});
				}
			}
			if ("Othernames".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					final JTextField surenameTextField = (JTextField) jpanel
							.getComponent(1);
					surenameTextField.addKeyListener(new KeyListener() {
						@Override
						public void keyPressed(KeyEvent arg0) {
						}

						@Override
						public void keyReleased(KeyEvent event) {
							updateSureEnable();
						}

						@Override
						public void keyTyped(KeyEvent arg0) {
						}
					});
				}
			}
			if ("cbIsYODUnknown".equals(question.getLabel())) {
				cbIsYODUnknownPanel = jpanel;
				Container c = jpanel.getParent();
				if (c instanceof JPanel) {
					JPanel parent = (JPanel) c;
					if (StringUtils.isNotBlank(parent.getName())
							&& parent.getName().startsWith("group_")) {
						deathYearParentPanel = parent;
					}
				}
				if (jpanel.getComponentCount() > 0) {
					cbIsYODUnknownCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("Year_of_death".equals(question.getLabel())) {
				deathYearPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					deathYearText = (JTextField) jpanel.getComponent(0);
				}
			}
			if ("cbIsYODApprox".equals(question.getLabel())) {
				cbIsYODApproxPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					cbIsYODApproxCb = (JCheckBox) jpanel.getComponent(0);
				}
			}

			if ("cbIsYOBUnknown".equals(question.getLabel())) {
				cbIsYOBUnknownPanel = jpanel;
				Container c = jpanel.getParent();
				if (c instanceof JPanel) {
					JPanel parent = (JPanel) c;
					if (StringUtils.isNotBlank(parent.getName())
							&& parent.getName().startsWith("group_")) {
						birthYearParentPanel = parent;
					}
				}
				if (jpanel.getComponentCount() > 0) {
					cbIsYOBUnknownCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("Year_of_birth".equals(question.getLabel())) {
				birthYearPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					birthYearText = (JTextField) jpanel.getComponent(0);
				}
			}
			if ("cbIsYOBApprox".equals(question.getLabel())) {
				cbIsYOBApproxPanel = jpanel;
				if (jpanel.getComponentCount() > 0) {
					cbIsYOBApproxCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
			if ("cbIsDeceased".equals(question.getLabel())) {
				if (jpanel.getComponentCount() > 0) {
					cbIsDeceasedCb = (JCheckBox) jpanel.getComponent(0);
				}
			}
		}
		if (deathYearParentPanel != null) {
			deathYearParentPanel.setVisible(false);
		}
		if (cbIsYOBApproxPanel != null) {
			cbIsYOBApproxPanel.setVisible(false);
		}
		if (cbIsYODApproxPanel != null) {
			cbIsYODApproxPanel.setVisible(false);
		}
		if (cbIsDeceasedCb != null) {
			cbIsDeceasedCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						deathYearParentPanel.setVisible(true);
						cbIsYODUnknownPanel.setVisible(true);
						deathYearPanel.setVisible(true);
						cbIsYODApproxPanel.setVisible(false);
						cbIsYODUnknownCb.setSelected(false);
						deathYearText.setText("");
						cbIsYODApproxCb.setSelected(false);
					} else {
						deathYearParentPanel.setVisible(false);
						cbIsYODUnknownCb.setSelected(false);
						deathYearText.setText("");
						cbIsYODApproxCb.setSelected(false);
					}
				}
			});
		}
		if (cbIsYOBUnknownCb != null) {
			cbIsYOBUnknownCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						birthYearText.setText("");
						birthYearPanel.setVisible(false);
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					} else {
						birthYearText.setText("");
						birthYearPanel.setVisible(true);
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					}
				}
			});
		}
		if (cbIsYODUnknownCb != null) {
			cbIsYODUnknownCb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JCheckBox jb = (JCheckBox) e.getSource();
					if (jb.isSelected()) {
						deathYearText.setText("");
						deathYearPanel.setVisible(false);
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					} else {
						deathYearText.setText("");
						deathYearPanel.setVisible(true);
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					}
				}
			});
		}
		if (birthYearText != null) {
			birthYearText.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent event) {
					if (StringUtils.isNotBlank(birthYearText.getText())) {
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(true);
					} else {
						cbIsYOBApproxCb.setSelected(false);
						cbIsYOBApproxPanel.setVisible(false);
					}
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
			});
		}
		if (deathYearText != null) {
			deathYearText.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent event) {
					if (StringUtils.isNotBlank(deathYearText.getText())) {
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(true);
					} else {
						cbIsYODApproxCb.setSelected(false);
						cbIsYODApproxPanel.setVisible(false);
					}
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
			});
		}
	}

	/**
	 * Check for Display. If 1. DNA =, 2. ifgo 3. labelSame
	 * 
	 * @param pid
	 */
	public void updateVisiable(int pid) {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();
			String id = name.replace("question_", "");

			Question question = qService.searchQuestion(id);
			KQQuestionField field = kassService.getPersonField(pid,
					question.getLabel());
			if (field != null && Constants.DEFAULT_DNA.equals(field.getValue())) {
				jpanel.setVisible(false);
				qm.hidePanel(jpanel);
			} else {
				// qm.isCheckShow(pid, question, field.getValue(), panels);

				if (field != null) {
					// if only one answer Option, = defaultValue
					if (question instanceof FollowupQuestion) {
						FollowupQuestion fquestion = (FollowupQuestion) question;
						if (fquestion.getOptions() != null
								&& fquestion.getOptions().size() == 1) {
							Option option = fquestion.getOptions().get(0);
							if (StringUtils
									.isNotBlank(option.getDefaultValue())
									&& Constants.DEFAULT_NA.equals(field
											.getValue())) {
								field.setValue(option.getDefaultValue());
							}
						}
					}
					qm.initPersonShow(pid, question, field.getValue(), panels);
				}
			}
			// if labelSame，just display the first question
			for (int j = i + 1; j < panels.size(); j++) {
				JPanel jpanel1 = panels.get(j);
				String name1 = jpanel1.getName();
				String id1 = name1.replace("question_", "");
				Question question1 = qService.searchQuestion(id1);
				if (StringUtils.equals(question.getLabel(),
						question1.getLabel())) {
					JPanel panel1 = getPanelById(id1);
					if (panel1.isVisible() && jpanel.isVisible()) {
						if (jpanel.isVisible()) {
							jpanel1.setVisible(false);
						} else {
							jpanel1.setVisible(true);
						}
					}
				}
			}
		}
	}

	public JPanel getPanelById(String qid) {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String id = jpanel.getName().replace("question_", "");
			if (StringUtils.equals(id, qid)) {
				return jpanel;
			}
		}
		return null;
	}

	/**
	 * Check for the display
	 * 
	 * @param pid
	 */
	public void updateUnionVisiable(int uid) {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();
			String id = name.replace("question_", "");
			Question question = qService.searchQuestion(id);
			KQQuestionField field = kassService.getUnionField(uid,
					question.getLabel());
			if (field != null && Constants.DEFAULT_DNA.equals(field.getValue())) {
				jpanel.setVisible(false);
				qm.hidePanel(jpanel);
			} else {
				// qm.isCheckShow(pid, question, field.getValue(), panels);
				if (field != null) {
					qm.initUnionShow(uid, question, field.getValue(), panels);
				}
			}

			// if labelSame，just display the first question
			for (int j = i + 1; j < panels.size(); j++) {
				JPanel jpanel1 = panels.get(j);
				String name1 = jpanel1.getName();
				String id1 = name1.replace("question_", "");
				Question question1 = qService.searchQuestion(id1);
				if (StringUtils.equals(question.getLabel(),
						question1.getLabel())) {
					if (jpanel.isVisible()) {
						jpanel1.setVisible(false);
					} else {
						jpanel1.setVisible(true);
					}
				}
			}
		}
	}

	QuestionKeyAdapter intKeylistener = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_INT, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					Object obj = evt.getSource();
					JTextField field = ((JTextField) obj);
					String text = field.getText();
					if (StringUtils.isBlank(text)) {
						text = Constants.DEFAULT_NA;
					}
					if (field.getParent() instanceof JPanel
							&& StringUtils.isNotBlank(field.getParent()
									.getParent().getName())) {
						String name = field.getParent().getName();
						String id = name.replace("question_", "");
						Question question = qService.searchQuestion(id);
						if (selectUid > -1) {
							qm.isCheckShow(selectUid, question, text, panels, 2);
						} else {
							qm.isCheckShow(selectPid, question, text, panels, 1);
						}
					}
				}
			});

	QuestionKeyAdapter decimalKeylistener = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_DECIMAL, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					Object obj = evt.getSource();
					JTextField field = ((JTextField) obj);
					String text = field.getText();
					if (StringUtils.isBlank(text)) {
						text = Constants.DEFAULT_NA;
					}
					if (field.getParent() instanceof JPanel
							&& StringUtils.isNotBlank(field.getParent()
									.getName())) {
						String name = field.getParent().getName();
						String id = name.replace("question_", "");
						Question question = qService.searchQuestion(id);
						if (selectUid > -1) {
							qm.isCheckShow(selectUid, question, text, panels, 2);
						} else {
							qm.isCheckShow(selectPid, question, text, panels, 1);
						}
					}
				}
			});

	/**
	 * Check the stopRule, change the color of the icon of the person with the
	 * largest kinship distance to yellow.
	 * 
	 * @param u
	 * @param p
	 */
	public void hanldeCount(int pid) {
		int rcount = graphPanel.getRoutingCount(pid);
		StopRule sr = qService.getStopRule();
		int step = sr.getStep();
		if (rcount == step) {
			mxCell mxCell = graphPanel.findPersonById(pid);
			graphPanel.setColor(mxCell, ColorUtil.getStopRuleColor());
			graphPanel.graphComponent.refresh();
		}
	}

	private void changeTitleColor(int pid) {
		if (selectUid > -1) {
			for (int i = 0; i < qids.size(); i++) {
				String id = qids.get(i);
				Question question = qService.searchQuestion(id);
				if (pid >= 0) {
					int index = qm.checkUnionComplete(question, pid);
					String color = ColorUtil.getProgressColor(index);
					tabPanel.setTitleAt(i, "<html><font color='" + color + "'>"
							+ question.getLabel() + "</font></html>");
				} else {
					tabPanel.setTitleAt(i, question.getLabel());
				}
			}
		} else {
			for (int i = 0; i < qids.size(); i++) {
				String id = qids.get(i);
				Question question = qService.searchQuestion(id);
				if (pid >= 0) {
					int index = qm.checkPersonComplete(question, pid);
					String color = ColorUtil.getProgressColor(index);
					tabPanel.setTitleAt(i, "<html><font color='" + color + "'>"
							+ question.getLabel() + "</font></html>");
				} else {
					tabPanel.setTitleAt(i, question.getLabel());
				}
			}
		}
	}

}
