package com.fangda.kass.ui.interview.graph;

import javax.swing.JPopupMenu;

import com.fangda.kass.ui.interview.IvAction.CompletePersonAction;
import com.fangda.kass.ui.interview.IvAction.DrawAction;
import com.fangda.kass.ui.interview.IvAction.DrawUnionAction;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.mxgraph.model.mxCell;

/**
 * This class for the pop-up menu when right click on the person icon, union
 * icon or blank canvas.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphPopupMenu extends JPopupMenu {

	private static final long serialVersionUID = 261479532719541951L;

	public GraphPopupMenu(IvMainPanel ivMainPanel) {
		this(ivMainPanel, null, 1, 0, 0, 0);
	}

	public GraphPopupMenu(IvMainPanel ivMainPanel, mxCell cell, int type,
			int menuType, double x, double y) {
		if (menuType == 0) {
			add(ivMainPanel.kmain.bind("Male", new DrawAction("male", type, x,
					y), "/images/male.png"));
			add(ivMainPanel.kmain.bind("Female", new DrawAction("female", type,
					x, y), "/images/female.png"));
			add(ivMainPanel.kmain.bind("Union", new DrawAction("union", type,
					x, y), "/images/straight.png"));
		} else if (menuType == 1) {// Selected icon is a person
			add(ivMainPanel.kmain.bind("Complete", new CompletePersonAction(
					cell, type), "/images/gear.png"));
		} else if (menuType == 2) {// Selected icon is a union
			add(ivMainPanel.kmain.bind("Partner", new DrawUnionAction(cell, 0,
					type), "/images/straight.png"));
			add(ivMainPanel.kmain.bind("Offspring", new DrawUnionAction(cell,
					1, type), "/images/vertical.png"));
		}
	}
}
