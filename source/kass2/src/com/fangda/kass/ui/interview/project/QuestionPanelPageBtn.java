package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.Toast;
import com.fangda.kass.ui.common.Toast.Style;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphElement;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Constants;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the buttons of answering questions in the IMS
 * 
 * @author Jianguo Zhou
 * 
 */
@SuppressWarnings(value = { "deprecation" })
public class QuestionPanelPageBtn extends JPanel {

	private static final long serialVersionUID = 1L;
	protected IvMainPanel ivMainPanel;
	protected Question question;
	protected static QuestionnaireService questionnaireService;
	public JButton selectPBtn;
	protected JButton finishPBtn;
	public String curqid;
	protected QuestionPanel questionPanel;
	public String selectState;
	public String selectcolor;
	List<Integer> selectPerson = new ArrayList<Integer>();
	List<Integer> secondselectPerson = new ArrayList<Integer>();
	public String followupAnswerState;
	List<Integer> lockPersons = new ArrayList<Integer>();
	List<KPPerson> kpersons = new ArrayList<KPPerson>();
	public String markAnswerState;

	public QuestionPanelPageBtn(IvMainPanel ivMainPanel,
			QuestionPanel questionpanel, Question question,
			QuestionPanelPage container) {
		selectcolor = "#6699CC";
		followupAnswerState = "0";
		questionnaireService = new QuestionnaireService();
		this.ivMainPanel = ivMainPanel;
		questionPanel = questionpanel;
		markAnswerState = "0";
		this.question = question;
		this.initPanel();
	}

	public void initPanel() {
		createPanel();
		this.setBackground(Color.white);
		this.setVisible(true);
	}

	/**
	 * The buttons of mark, previous and next
	 * 
	 * @return
	 */
	private void createPanel() {
		curqid = question.getQid();
		InitLockPerson();

		GridBagConstraints c = new GridBagConstraints();
		int index = 0;
		index++;
		JButton previousBtn = new JButton("<<");
		previousBtn.setToolTipText(mxResources.get("previous"));
		previousBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prevPage();

			}
		});
		UIHelper.addToBagpanel(this, Color.gray, previousBtn, c, 0, index, 1,
				1, GridBagConstraints.EAST);

		selectPBtn = new JButton(mxResources.get("finishMark"));
		selectPBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				finishBtnAction0();
			}
		});
		UIHelper.addToBagpanel(this, Color.gray, selectPBtn, c, 1, index, 1, 1,
				GridBagConstraints.EAST);

		JButton nextBtn = new JButton(">>");
		nextBtn.setToolTipText(mxResources.get("next"));
		nextBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextPage();

			}
		});
		UIHelper.addToBagpanel(this, Color.gray, nextBtn, c, 2, index, 1, 1,
				GridBagConstraints.EAST);

		// Check the answer completed status
		jianchaQuestionifComplete();
		searchQuestionIfNaSetFalseAndComplete();
		initMarkBtnState();

	}

	/**
	 * if NA: isMarked = false , complete = 2 for the question
	 */
	private void searchQuestionIfNaSetFalseAndComplete() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		boolean flag = false;
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_NA)) {
				flag = true;
				break;
			}
		}
		if (flag) {
			this.setQuestionIsMarked(false);
			setCompleted("2");
		}
	}

	private void finishPBtnAction() {
		if (selectState.equals("1")) {
			finishPBtn.setText(mxResources.get("allCompleted"));
			selectPBtn.setEnabled(true);
			selectState = "2";
			setAllPersonFalse();
			selectPBtn.setText(mxResources.get("modifyLead"));
			if (this.ifInitMarkedPeopleREL_Z()) {
				questionPanel.paelq.setFolloupQuestionPageShow(true);
			}
			this.ivMainPanel.jqustionpanel.paelq.saveBtn.setEnabled(true);
			InitPersonAnswer();
		} else {
			if (finishPBtnIfcomplete()) {
				finishPBtn.setText(mxResources.get("uncompleted"));
				setCompleted("1");
				setAllPersonTrue();
				selectState = "1";
				selectPBtn.setEnabled(false);
				questionPanel.paelq.setFolloupQuestionPageShow(false);
				followupAnswerState = "0";

			} else {
				Toast.makeText(ivMainPanel.kmain.frame,
						mxResources.get("CompletePromptAnswerQuestion"),
						Style.SUCCESS).display();
			}
			InitSelectPerson();
		}

		SaveQuestionState();
		ivMainPanel.updatePercent();
	}

	public void leadFollowupQuestionIfcomplete() {
		if (finishPBtnIfcomplete()) {
			followupAnswerState = "0";
			KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
			kquestion.setCompleted("1");
		}
	}

	public void setFollowupQuestionHide() {
		questionPanel.paelq.setFolloupQuestionPageShow(false);
		followupAnswerState = "0";
	}

	private void selectPBtnAction() {
		if (selectState.equals("0")) {
			selectState0();
		} else if (selectState.equals("1")) {
			selectState1();
		} else if (selectState.equals("2")) {
			selectState2();
		}
		ivMainPanel.kassService.save();

	}

	/**
	 * The Mark Button: 1 = mark, 2 = modify
	 */
	private void finishBtnAction0() {
		if (ivMainPanel.kassService.getQuestion(curqid).getIsMarked()) {
			modifyCommit();
		} else {
			markFinishCommit();
		}
	}

	/**
	 * The finish mark
	 */
	private void markFinishCommit() {
		if (question.getType().equals("A")) {
			// setPQuestionBnmAndNa(); Selected = OK, unSelected = BNM
			completeSelectPersonMode();
			setPQuestionBnmAndOk();
			// If completed, the button label is change as modify
			setQuestionIsMarked(true);
			setCompleted("1");
			selectPBtn.setText(mxResources.get("markOpen"));
		} else {
			completeSelectPersonMode();
			setPQuestionBnmAndNa();
			selectPBtn.setText(mxResources.get("modifyLead"));
			if (getMarkPersonNum() > 0) {
				setCompleted("2");
				FinishSelectModel();

				followupAnswerState = "1";
				questionPanel.paelq.setFolloupQuestionPageShow(true);
				ivMainPanel.repaint();
				selectPerson.clear();
				ivMainPanel.jqustionpanel
						.updateLeadDetail(ivMainPanel.jqustionpanel.question);
			} else {
				setCompleted("1");
			}
			setQuestionIsMarked(true);
		}

		InitSelectPerson();
	}

	/**
	 * End the selected mode
	 */
	private void completeSelectPersonMode() {
		questionPanel.graphPanel.completeSelectPersonMode();
		questionPanel.ographPanel.completeSelectPersonMode();
	}

	/**
	 * The modify submit
	 */
	private void modifyCommit() {
		if (question.getType().equals("F")) {
			enterFolloupQuestion();
		} else {
			selectPBtn.setText(mxResources.get("finishMark"));
			setCompleted("2");
			setQuestionIsMarked(false);
			followupAnswerState = "0";
			setPQuestionBNMTNA();
			InitSelectModel();
			InitSelectPerson();
			questionPanel.paelq.setFolloupQuestionPageShow(false);
		}
	}

	/**
	 * Click the modify button start to modify the answer, the button label
	 * change to mark
	 */
	private void setPQuestionIsMarkedFalse() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_OK)) {
				kqquestion.setValue(Constants.DEFAULT_MARK);
			}
		}
	}

	/**
	 * Modify initialize the answers: BNM = NA, OK = Mark
	 */
	private void setPQuestionBNMTNA() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_BNM)) {
				kqquestion.setValue(Constants.DEFAULT_NA);
				List<KQQuestionField> fileds = kqquestion.getFields();
				for (KQQuestionField field : fileds) {
					field.setValue(Constants.DEFAULT_NA);
				}
			} else if (kqquestion.getValue().equals(Constants.DEFAULT_OK)) {
				kqquestion.setValue(Constants.DEFAULT_MARK);
			}
		}
	}

	/**
	 * Count the number of the marked persons
	 */
	private int getMarkPersonNum() {
		int count = 0;
		List<KPPerson> persons = getKPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_MARK)
					|| kqquestion.getValue().equals(Constants.DEFAULT_OK)) {
				count++;
			}
		}
		persons = getKPersonsO();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_MARK)
					|| kqquestion.getValue().equals(Constants.DEFAULT_OK)) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Set the answer
	 */
	public void setPQuestionBnmAndNa() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : persons) {
			setPQuestionBnmAndNaQuestion(person.getPid());
		}
	}

	/**
	 * Set the answer: selected = OK, unselected =BNM
	 */
	public void setPQuestionBnmAndOk() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_NA)) {
				kqquestion.setValue(Constants.DEFAULT_BNM);
			} else if (kqquestion.getValue().equals(Constants.DEFAULT_MARK)) {
				kqquestion.setValue(Constants.DEFAULT_OK);
			}
		}
	}

	/**
	 * Set the answer of the question
	 */
	public void setPQuestionBnmAndNaQuestion(int pid) {
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kqquestion.getValue().equals(Constants.DEFAULT_NA)) {
			kqquestion.setValue(Constants.DEFAULT_BNM);
			setFollowupQestionToDNA(kqquestion);
		}
	}

	private void selectState0() {
		if (selectPBtn.getText().equals(mxResources.get("finishMark"))) {
			selectPBtnFinishMark();
		} else if (selectPBtn.getText().equals(mxResources.get("modifyLead"))) {
			selectPBtnOpenMark();
		}
	}

	private void selectState1() {
		selectPBtn.setText(mxResources.get("modifyLead"));
		finishPBtn.show();
		selectState = "2";
		ivMainPanel.graphPanel.completeSelectPersonMode();
	}

	private void selectState2() {
		setOldPerson();
		selectPBtn.setText(mxResources.get("finishMark"));
		selectPBtn.setEnabled(true);
		selectState = "0";
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		kquestion.setIsMarked(false);
		ivMainPanel.jqustionpanel.paelq.flQue.show(false);
		ivMainPanel.jqustionpanel.paelq.pageSave.show(false);
		ivMainPanel.repaint();
	}

	private void selectPBtnOpenMark() {
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		kquestion.setIsMarked(false);
		ivMainPanel.graphPanel.setAllDefaultColor();
		ivMainPanel.ographPanel.setAllDefaultColor();
		setSelectPBtnTextopen(kquestion);

		questionPanel.paelq.setFolloupQuestionPageShow(false);
	}

	public void setFolloupQuestionBnmToNa() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals(Constants.DEFAULT_BNM)) {
				kqquestion.setValue(Constants.DEFAULT_NA);
				List<KQQuestionField> fields = kqquestion.getFields();
				for (KQQuestionField field : fields) {
					field.setValue(Constants.DEFAULT_NA);
				}
				ivMainPanel.kassService.save();
			}
		}
	}

	private void selectPBtnFinishMark() {
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		if (kquestion != null) {
			if (!kquestion.getIsMarked()) {
				if (questionPanel.paelq.type == 3) {
					selectPBtnFinishMarkC(questionPanel.ographPanel,
							this.getKPersonsO());
					selectPBtn.setText(mxResources.get("modifyLead"));

					finishPBtn.setText(mxResources.get("uncompleted"));
					setCompleted("1");
					setAllPersonTrue();
					selectState = "1";
					selectPBtn.setEnabled(false);

					InitSelectPerson();
					kquestion.setIsMarked(true);
				} else {
					setCompleted("2");
					ivMainPanel.graphPanel.completeSelectPersonMode();
					ivMainPanel.graphPanel.setSelectCellNull();
					ivMainPanel.ographPanel.completeSelectPersonMode();
					ivMainPanel.ographPanel.setSelectCellNull();
					selectPBtn.setText(mxResources.get("modifyLead"));
					kquestion.setIsMarked(true);
					singleClickFollowupQuestion();

				}
			} else {
				selectPBtn.setText(mxResources.get("finishMark"));
				InitSelectModel();
				finishPBtn.show(true);
				kquestion.setIsMarked(true);
			}
		}
	}

	private void setQuestionIsMarked(Boolean falg) {
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		if (kquestion != null) {
			kquestion.setIsMarked(falg);
		}
	}

	public void setPersonQut(List<KPPerson> persons) {
		for (KPPerson personid : persons) {
			setPersonQutFinish(personid.getPid());
			if (!lockPersons.contains(personid.getPid())) {
				if (selectPerson.contains(personid.getPid())) {
					setPersonQutMarked(personid.getPid(), true);
					if (ivMainPanel.kassService
							.getPersonQuestion(personid.getPid(), curqid)
							.getValue().equals(Constants.DEFAULT_BNM)) {
						setPersonQutValue(personid.getPid(),
								Constants.DEFAULT_NA);
					}
				} else {
					setPersonQutMarked(personid.getPid(), false);
				}
			}
		}
	}

	private void selectPBtnFinishMarkC(GraphPanel graphPanel,
			List<KPPerson> persons) {
		graphPanel.completeSelectPersonMode();

		for (KPPerson personid : persons) {
			setPersonQutFinish(personid.getPid());
			if (selectPerson.contains(personid.getPid())) {
				setPersonQutMarked(personid.getPid(), true);
				setPersonQutValue(personid.getPid(), Constants.DEFAULT_NA);
			} else {
				setPersonQutMarked(personid.getPid(), false);
				setPersonQutValue(personid.getPid(), Constants.DEFAULT_BNM);
			}
		}
	}

	private Boolean finishPBtnIfcomplete() {
		Boolean flag = false;
		if (!finishPBtnIfcompleteCommon(getKPersons())) {
			return flag;
		}
		if (!finishPBtnIfcompleteCommon(getKPersonsO())) {
			return flag;
		}
		flag = true;
		return flag;
	}

	private Boolean finishPBtnIfcompleteCommon(List<KPPerson> persons) {
		Boolean flag = false;
		for (KPPerson kperson : persons) {
			KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(
					kperson.getPid(), curqid);
			if (!kquestion.getValue().equals(Constants.DEFAULT_DNA)) {
				if (kquestion.getValue().equals(Constants.DEFAULT_NA)) {
					return flag;
				}
				if (kquestion.getValue().equals("Mark")) {
					List<KQQuestionField> fields = kquestion.getFields();
					for (KQQuestionField field : fields) {
						if (field.getValue().equals(Constants.DEFAULT_NA)) {
							return flag;
						}
					}
				}
			}
		}
		flag = true;
		return flag;
	}

	public void SaveQuestionState() {
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		if (kquestion != null) {
			kquestion.setCompleted(selectState);
		}
	}

	private void setCompleted(String completed) {
		KQuestion question = ivMainPanel.kassService.getQuestion(curqid);
		if (question != null) {
			question.setCompleted(completed);
		}
		getSelectState();
	}

	/**
	 * Initialize the mark button
	 */
	private void initMarkBtnState() {
		// Question type = DCR
		ifQuestionIsDCRSetShowMark();
		// subsection = 007_01
		ifsubsection0071();
		FinishSelectModel();
		getSelectState();
		if (selectState.equals("0")) {
			// getType = F, Mark = true , go to the follow up questions
			if (question.getType().equals("F")) {
				enterFolloupQuestion();
			} else {
				selectPBtn.setText(mxResources.get("finishMark"));
				InitSelectPerson();
				InitSelectModel();
			}
		} else if (selectState.equals("1")) {
			selectPBtn.setText(mxResources.get("markOpen"));
			InitSelectPerson();
		} else if (selectState.equals("2")) {
			if (getQuestionIsMarked()) {
				enterFolloupQuestion();
			} else {
				modifyCommit();
			}
		}
	}

	/**
	 * The follow up questions
	 */
	private void enterFolloupQuestion() {
		if (question.getType().equals("R")) {
			// DCR
			followupAnswerState = "1";
			// Follow up question
			questionPanel.paelq.setFolloupQuestionPageShow(true);
			selectPBtn.setText(mxResources.get("modifyLead"));
			InitSelectPerson();
		} else {
			selectPBtn.setText(mxResources.get("modifyLead"));
			showFolloupTip();
			// isMarked = true, then answer the follow up question
			followupAnswerState = "1";
			questionPanel.paelq.setFolloupQuestionPageShow(true);
			InitSelectPerson();
		}
	}

	private Boolean getQuestionIsMarked() {
		Boolean flag = false;
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
		if (kquestion != null) {
			flag = kquestion.getIsMarked();
		}
		return flag;
	}

	private void showFolloupTip() {
		String show = mxResources.get("followupUncompleteTip");
		String pid = "\"";
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();
		int i = 0;
		for (KPPerson person : persons) {
			KQQuestion question = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (question.getValue().equals(Constants.DEFAULT_MARK)) {
				if (i == 0) {
				} else {
					pid += ",";
				}
				pid += String.valueOf(person.getPid());
				i++;
			}
		}
		pid += "\"";
		show += pid;
		Toast.makeText(ivMainPanel.kmain.frame, show, Style.SUCCESS).display();
	}

	/**
	 * The complete status
	 */
	private void getSelectState() {
		KQuestion kquestion = ivMainPanel.kassService.getQuestion(question
				.getQid());
		selectState = kquestion.getCompleted();
	}

	private void setSelectPBtnText() {
		setSelectPBtnTextNormal();
	}

	private void setSelectPBtnTextNormal() {
		getSelectState();
		ivMainPanel.graphPanel.setAllDefaultColor();
		if (selectState.equals("0")) {
			questionPanel.paelq.setFolloupQuestionPageShow(false);
			KQuestion kquestion = ivMainPanel.kassService.getQuestion(curqid);
			if (kquestion != null) {
				if (kquestion.getIsMarked()) {
					selectPBtn.setText(mxResources.get("modifyLead"));
					InitSelectPerson();
				} else {
					if (ifInitMarkedPeopleREL_Z()) {
						setSelectPBtnTextopen_Z();
					} else {
						setSelectPBtnTextopen(kquestion);
					}
				}
			}

		} else if (selectState.equals("1")) {
			selectPBtn.setEnabled(false);
			if (ivMainPanel.kassService.getQuestion(
					questionPanel.paelq.question.getQid()).getIsMarked()) {
				selectPBtn.setText(mxResources.get("modifyLead"));
			}

			finishPBtn.setText(mxResources.get("uncompleted"));
			questionPanel.paelq.setFolloupQuestionPageShow(false);
			InitSelectPerson();
		} else if (selectState.equals("2")) {
			selectPBtn.setText(mxResources.get("modifyLead"));
			InitSelectPerson();
		}
	}

	public Boolean ifInitMarkedPeopleREL_Z() {
		Boolean flag = false;
		if (question.getRelevance().equals("REL_Z")) {
			flag = true;
		}
		return flag;
	}

	public Boolean ifInitMarkedPeopleREL_I() {
		Boolean flag = false;
		if (question.getRelevance().equals("REL_I")) {
			flag = true;
		}
		return flag;
	}

	public void setSelectPBtnTextopen(KQuestion kquestion) {
		selectPBtn.setText(mxResources.get("finishMark"));
		InitSelectModel();
	}

	public void setSelectPBtnTextopen_Z() {
		if (question.getQid().equals("K_07_01")) {
			selectPBtn.setEnabled(false);
			InitSelectPerson();
		} else {
			selectPBtn.setText(mxResources.get("finishMark"));
			InitSelectModel();
			finishPBtn.show(true);
		}

	}

	public void setSelectPBtnTextopen_I(KQuestion kquestion) {
		selectPBtn.setText(mxResources.get("finishMark"));

		InitSelectModel_I();
		InitSelectPerson_I();

		finishPBtn.show(true);

		ivMainPanel.ographPanel.setSelectPersonMode(selectcolor);
	}

	public void InitSelectModel_I() {
		ivMainPanel.ographPanel.setAllDefaultColor();
		ivMainPanel.ographPanel.setSelectPersonMode(selectcolor);
	}

	public void InitSelectPerson_I() {
		List<KPPerson> persons = getKPersons();
		String selcolor = selectcolor;

		for (KPPerson kperson : persons) {
			selcolor = ColorUtil.getPersonProgressColor(3);
			mxCell mcell = ivMainPanel.graphPanel.findPersonById(kperson
					.getPid());
			ivMainPanel.graphPanel.setColor(mcell, selcolor);
			ivMainPanel.graphPanel.graphComponent.refresh();
		}

		List<KPPerson> personso = getKPersonsO();
		for (KPPerson kperson : personso) {
			KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(
					kperson.getPid(), curqid);
			selcolor = selectcolor;
			if (kquestion.getValue().equals(Constants.DEFAULT_DNA)) {
				selcolor = ColorUtil.getPersonProgressColor(3);
			} else if (!kquestion.getValue().equals("OK")
					&& !kquestion.getValue().equals("Mark")) {
				selcolor = ColorUtil.getPersonProgressColor(0);
			} else if (kquestion.getValue().equals("OK")
					&& kquestion.getValue().equals("Mark")) {
				selcolor = ColorUtil.getPersonProgressColor(1);
			} else if (!kquestion.getValue().equals("OK")
					&& kquestion.getValue().equals("Mark")) {
				selcolor = ColorUtil.getPersonProgressColor(2);
			} else if (kquestion.getValue().equals(Constants.DEFAULT_BNM)) {
				selcolor = ColorUtil.getPersonProgressColor(3);
			} else if (kquestion.getValue().equals("OK")
					&& !kquestion.getValue().equals("Mark")) {
				selcolor = ColorUtil.getPersonProgressColor(5);
			}
			mxCell mcell = ivMainPanel.ographPanel.findPersonById(kperson
					.getPid());
			ivMainPanel.ographPanel.setColor(mcell, selectcolor);
			ivMainPanel.ographPanel.graphComponent.refresh();
		}
	}

	public void InitSelectPerson() {
		InitPersons(getKPersons(), questionPanel.graphPanel);
		InitPersons(getKPersonsO(), questionPanel.ographPanel);
	}

	public void InitPersons(List<KPPerson> persons, GraphPanel graphPanel) {
		String selcolor = selectcolor;
		for (KPPerson kperson : persons) {
			KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(
					kperson.getPid(), curqid);

			if (kquestion != null) {
				selcolor = selectcolor;
				selcolor = ColorUtil.getProgressStatusColor(kquestion
						.getValue());

				mxCell mcel = graphPanel.findPersonById(kperson.getPid());
				if (kquestion.getValue().equals("OK")) {
					graphPanel.setColor(mcel, selcolor, selcolor);
				} else {
					graphPanel.setColor(mcel, selcolor);
				}

				graphPanel.graphComponent.refresh();
			}
		}

	}

	private void setPersonQutFinish(Integer pid) {
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kqquestion != null) {
			kqquestion.setValue("OK");

		}

	}

	private void setAllPersonFalse() {
		setAllPersonFalseCommon(getKPersons());
		setAllPersonFalseCommon(getKPersonsO());
	}

	private void setAllPersonFalseCommon(List<KPPerson> persons) {
		for (KPPerson personid : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					personid.getPid(), curqid);
			if (!kqquestion.getValue().equals(Constants.DEFAULT_DNA)
					&& kqquestion != null) {
				kqquestion.setValue("OK");
			}
		}

	}

	private void setAllPersonTrue() {
		setAllPersonTrueCommon(getKPersons());
		setAllPersonTrueCommon(getKPersonsO());
	}

	private void setAllPersonTrueCommon(List<KPPerson> persons) {
		for (KPPerson personid : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					personid.getPid(), curqid);
			if (!kqquestion.getValue().equals(Constants.DEFAULT_DNA)
					&& kqquestion != null) {
				kqquestion.setValue("OK");
			}
		}

	}

	private void setPersonQutMarked(Integer pid, Boolean flag) {
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kqquestion != null) {
			if (flag) {
				kqquestion.setValue("Mark");
			} else {
				kqquestion.setValue("NA");
			}
		}

	}

	private void setPersonQutValue(Integer pid, String value) {
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kqquestion != null) {
			kqquestion.setValue(value);
			List<KQQuestionField> fields = kqquestion.getFields();
			for (KQQuestionField field : fields) {
				if (value.equals(Constants.DEFAULT_BNM)) {
					field.setValue(Constants.DEFAULT_DNA);
				} else {
					field.setValue(value);
				}

			}
		}
		ivMainPanel.kassService.save();
	}

	/**
	 * Select mode
	 */
	public void InitSelectModel() {
		InitSelectM(questionPanel.graphPanel, this.getKPersons());
		InitSelectM(questionPanel.ographPanel, this.getKPersonsO());
	}

	/**
	 * End the select mode
	 */
	public void FinishSelectModel() {
		ivMainPanel.graphPanel.completeSelectPersonMode();
		ivMainPanel.graphPanel.setSelectCellNull();
		ivMainPanel.ographPanel.completeSelectPersonMode();
		ivMainPanel.ographPanel.setSelectCellNull();
	}

	public void InitSelectModel_Z() {
		InitSelectM_Z(questionPanel.graphPanel, this.getKPersons());
		InitSelectM_Z(questionPanel.ographPanel, this.getKPersonsO());
	}

	public void InitPeopleState() {
		selectPerson.clear();
		if (questionPanel.paelq.type == 2) {
			List<KQQuestionField> kqquestionFiedls = ivMainPanel.kassService
					.getPersonQuestion(questionPanel.paelq.dbpersonid,
							questionPanel.paelq.curqid).getFields();
			for (KQQuestionField field : kqquestionFiedls) {
				if (field.getValue().equals(Constants.DEFAULT_DNA)
						|| field.getValue().equals(Constants.DEFAULT_NA)) {
					selectPerson.add(Integer.parseInt(field.getValue()));
				}
			}
		} else {
			List<KPPerson> persons = getKPersons();

			for (KPPerson kperson : persons) {
				if (getPersonQutFinish(kperson.getPid())) {
					selectPerson.add(kperson.getPid());
				}
			}
		}

		ivMainPanel.graphPanel.setSelectCellNull();
		ivMainPanel.graphPanel.setAllDefaultColor();
		ivMainPanel.graphPanel.setSelectPersonMode(selectcolor);
		ivMainPanel.graphPanel.initSelectPerson(selectPerson);
	}

	public void InitSelectM(GraphPanel graphPanel, List<KPPerson> persons) {
		graphPanel.clearSelected();
		graphPanel.setAllDefaultColor();
		for (KPPerson kperson : persons) {
			if (getPersonQutFinish(kperson.getPid())
					&& !selectPerson.contains(kperson.getPid())) {
				selectPerson.add(kperson.getPid());
			}
		}
		graphPanel.setSelectPersonMode(selectcolor);
	}

	public void InitSelectM_Z(GraphPanel graphPanel, List<KPPerson> persons) {
		graphPanel.setAllDefaultColor();
		graphPanel.setSelectPersonMode(selectcolor);

		List<KQQuestionField> fields = ivMainPanel.kassService
				.getPersonQuestion(questionPanel.paelq.dbpersonid, curqid)
				.getFields();
		for (KQQuestionField field : fields) {
			field.getValue();
		}

		graphPanel.setSelectCellNull();
		graphPanel.setAllDefaultColor();
		graphPanel.setSelectPersonMode(selectcolor);
		graphPanel.initSelectPerson(selectPerson);

	}

	public void afterSelectCell(GraphPanel grapPanel) {
		mxCell cell = grapPanel.getSelectCell();
		if (cell != null) {
			GraphPerson gp = (GraphPerson) cell.getValue();

			if (cell.getStyle().contains(selectcolor)) {
				if (!selectPerson.contains(gp.getId())) {
					selectPerson.add(gp.getId());
				}
				KQQuestion kqquestion = ivMainPanel.kassService
						.getPersonQuestion(gp.getId(), curqid);
				kqquestion.setValue("Mark");
			} else {
				removeSelectPerson(gp.getId());
			}
		}
	}

	public void afterUnSelectCell(GraphPanel grapPanel) {
		mxCell cell = grapPanel.getSelectCell();
		if (cell != null) {
			GraphPerson gp = (GraphPerson) cell.getValue();
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					gp.getId(), curqid);
			kqquestion.setValue(Constants.DEFAULT_BNM);
			setFollowupQestionToDNA(kqquestion);
			removeSelectPerson(gp.getId());
		}
	}

	/**
	 * Set the follow up question as NA
	 * 
	 * @param kqquestion
	 */
	public void setFollowupQestionToDNA(KQQuestion kqquestion) {
		List<KQQuestionField> fields = kqquestion.getFields();
		for (KQQuestionField field : fields) {
			field.setValue(Constants.DEFAULT_DNA);
		}
	}

	public void removeSelectPerson(int pid) {
		if (selectPerson.contains(pid)) {
			for (int i = 0; i < selectPerson.size(); i++) {
				if (selectPerson.get(i) == pid) {
					selectPerson.remove(i);
					break;
				}
			}
		}
	}

	public void setOldPerson() {
		setOldPersonC(questionPanel.graphPanel, this.getKPersons());
		setOldPersonC(questionPanel.ographPanel, this.getKPersonsO());
	}

	public void setOldPersonC(GraphPanel graphPanel, List<KPPerson> persons) {

		for (KPPerson kperson : persons) {
			if (getPersonQutFinish(kperson.getPid())) {
				selectPerson.add(kperson.getPid());
			}
		}
		graphPanel.completeSelectPersonMode();
		graphPanel.setSelectPersonMode(selectcolor);
		graphPanel.initSelectPerson(selectPerson);
	}

	private Boolean getPersonQutFinish(Integer pid) {
		Boolean flag = false;
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kqquestion != null) {
			flag = kqquestion.getValue().equals("Mark");
		}
		return flag;
	}

	/**
	 * Get the value when single click the graph
	 * 
	 * @param cell
	 * @param gPanel
	 */
	public void singleClickQuestion(mxCell cell, GraphPanel gPanel) {
		KQuestion question = ivMainPanel.kassService.getQuestion(curqid);
		if (selectPBtn.isEnabled() && question.getIsMarked()) {
			if (questionPanel.paelq.type == 3) {

			} else {
				singleClickQuestiondoQuestion(cell, gPanel);
			}

		} else if (this.ifInitMarkedPeopleREL_Z()) {

		} else if (this.ifInitMarkedPeopleREL_I()) {
			singleClickQuestionREL_I();
		}
	}

	private void singleClickQuestiondoQuestion(mxCell cell, GraphPanel gPanel) {
		if (followupAnswerState.equals("0")) {
			if (questionPanel.paelq.type == 2) {

			} else {
				singleClickShowFolloup(cell);
				InitPersonAnswerCOnePerson(cell, gPanel);
			}
		} else if (followupAnswerState.equals("1")) {
			KQQuestion kquestion = questionPanel.paelq
					.getKQuestionbyqid(questionPanel.paelq.curqid);
			if (kquestion == null) {
				kquestion = questionPanel.paelq
						.getKQuestionbyqidO(questionPanel.paelq.curqid);
			}
			if (kquestion != null) {
				if (kquestion.getValue().equals("Mark")
						|| kquestion.getValue().equals(Constants.DEFAULT_OK)) {
					GraphElement one = (GraphElement) cell.getValue();
					questionPanel.paelq.initPersonValue(one.getId());
				} else {
					JOptionPane.showMessageDialog(null,
							mxResources.get("unMarkPerson"));
				}
			}
		}
	}

	/**
	 * Display the follow up questions without the buttons
	 */
	private void singleClickShowFolloup(mxCell cell) {
		questionPanel.paelq.setFolloupQuestionPageShowExceptBtn(true);
		ivMainPanel.repaint();
	}

	private void singleClickQuestionREL_I() {

	}

	public void singleClickQuestionHintMkOpen() {
		Object[] e1 = { "Cancle", "Mark Open", "FollowUp Question" };
		int result = JOptionPane.showOptionDialog(null,
				"<HTML>" + mxResources.get("tipMarkOpen") + "</HTML>",
				"Select Option", JOptionPane.DEFAULT_OPTION,
				JOptionPane.WARNING_MESSAGE, null, e1, e1[0]);
		if (result == JOptionPane.YES_OPTION) {
			System.out.println("Cancel");
		} else if (result == JOptionPane.NO_OPTION) {
			selectPBtnAction();
		} else if (result == 2) {
			System.out.println("FollowUp Question");
			singleClickFollowupQuestion();
		}
	}

	public void singleClickQuestionHintUnOpen() {

		Object[] e1 = { "Cancle", "Uncomplet Open", "FollowUp Question" };
		int result = JOptionPane.showOptionDialog(null,
				"<HTML>" + mxResources.get("tipMarkOpen") + "</HTML>",
				"Select Option", JOptionPane.DEFAULT_OPTION,
				JOptionPane.WARNING_MESSAGE, null, e1, e1[0]);
		if (result == JOptionPane.YES_OPTION) {
			System.out.println("Cancel");
		} else if (result == JOptionPane.NO_OPTION) {
			System.out.println("Uncomplet Open");
			finishPBtnAction();
		} else if (result == 2) {
			System.out.println("FollowUp Question");
			singleClickFollowupQuestion();
		}
	}

	private void singleClickFollowupQuestion() {
		followupAnswerState = "1";
		questionPanel.paelq.setFolloupQuestionPageShow(true);
		ivMainPanel.repaint();
	}

	/**
	 * The follow up question type = c, 1 = NA, 2 = DNA, 3 = index
	 */
	public void InitPersonAnswer() {
		InitPersonAnswerC(questionPanel.graphPanel, this.getKPersons());
		InitPersonAnswerC(questionPanel.ographPanel, this.getKPersonsO());
	}

	/**
	 * The follow up question type = c, 1 = NA, 2 = DNA, 3 = index
	 */
	public void InitPersonAnswerC(GraphPanel graphPanel, List<KPPerson> persons) {
		FollowupQuestion followupQuestion = questionPanel.paelq.question
				.getFollowupQuestions().get(0);
		if (!followupQuestion.getType().equals("C")) {
			return;
		}
		int num = questionPanel.paelq.getQuestionOptionNum();
		graphPanel.completeSelectPersonMode();
		String selcolor = selectcolor;

		for (KPPerson kperson : persons) {
			KQQuestion kquestion = questionPanel.paelq.getKQuestionByQidPeople(
					kperson.getPid(), curqid);

			String index = kquestion.getFields().get(0).getValue();
			if (index.equals(Constants.DEFAULT_DNA)) {
				selcolor = ColorUtil.getFollowUpColor(1);
			} else if (index.equals(Constants.DEFAULT_NA)) {
				selcolor = ColorUtil.getFollowUpColor(0);
			} else {
				if (followupQuestion.getType().equals("C")) {
					selcolor = ColorUtil
							.geWebColor(num, Integer.valueOf(index));
				} else {
					selcolor = ColorUtil.getFollowUpColor(2);
				}
			}

			mxCell mcel = graphPanel.findPersonById(kperson.getPid());
			graphPanel.setColor(mcel, selcolor);
			graphPanel.graphComponent.refresh();

		}
	}

	/**
	 * 
	 * @return
	 */
	private void InitPersonAnswerCOnePerson(mxCell mcel, GraphPanel graphPanel) {
		int personID = questionPanel.paelq.getKPeopleIdC(graphPanel
				.getSelectCell());
		questionPanel.paelq.InitOporationbyperson(graphPanel, personID);
	}

	private Boolean questionAllNMarked() {
		Boolean flag = false;

		List<KPPerson> persons = getKPersons();

		for (KPPerson kperson : persons) {
			KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(
					kperson.getPid(), curqid);
			if (kquestion.getValue().equals("Mark")) {
				return flag;
			}
		}

		flag = true;
		return flag;
	}

	/**
	 * Go to the next question
	 */
	public void nextPage() {
		if (selectState.equals("0") || selectState.equals("2")) {
			String complete = "1";
			if (finishPBtnIfcomplete()) {

			} else {
				Object[] el = { "Yes", "No" };
				int result = JOptionPane.showOptionDialog(null,
						mxResources.get("questionCompleteNote"),
						mxResources.get("selectOptionTitle"),
						JOptionPane.DEFAULT_OPTION,
						JOptionPane.WARNING_MESSAGE, null, el, el[1]);
				if (result == JOptionPane.YES_OPTION) {
					if (questionAllNMarked()) {
						complete = "0";
					} else {
						complete = "2";
					}
				} else if (result == JOptionPane.NO_OPTION) {
					return;
				}
			}
			setCompleted(complete);

		}

		ivMainPanel.nextPage(question.getQid());
	}

	/**
	 * The OK Button
	 */
	public void okifcomplete() {
		String complete = "1";
		if (finishPBtnIfcomplete()) {

		} else {
			complete = "2";
		}
		setCompleted(complete);
	}

	public void nextPageFlvmain() {
		setSelectPBtnText();
		ivMainPanel.graphPanel.setAllDefaultColor();
		InitPeopleState();
		questionPanel.updateNextCont();
		questionPanel.refresh();
	}

	public void prevPage() {
		if (selectState.equals("0") || selectState.equals("2")) {
			String complete = "1";
			if (finishPBtnIfcomplete()) {

			} else {
				Object[] el = { "Yes", "No" };
				int result = JOptionPane.showOptionDialog(null,
						mxResources.get("questionCompleteNote"),
						mxResources.get("selectOptionTitle"),
						JOptionPane.DEFAULT_OPTION,
						JOptionPane.WARNING_MESSAGE, null, el, el[1]);
				if (result == JOptionPane.YES_OPTION) {
					if (questionAllNMarked()) {
						complete = "0";
					} else {
						complete = "2";
					}
				} else if (result == JOptionPane.NO_OPTION) {
					return;
				}
			}
			setCompleted(complete);
		}
		ivMainPanel.prevPage(question.getQid());

	}

	public void prevPageFlvmain() {
		setSelectPBtnText();
		ivMainPanel.graphPanel.setAllDefaultColor();
		InitPeopleState();

		questionPanel.updateNextCont();
		questionPanel.refresh();
	}

	public void InitLockPerson() {
		questionPanel.graphPanel.clearLockPerson();
		questionPanel.ographPanel.clearLockPerson();

		InitLockPersons(questionPanel.graphPanel, this.getKPersons());
		InitLockPersons(questionPanel.ographPanel, this.getKPersonsO());
	}

	public void InitLockPersons(GraphPanel graphPanel, List<KPPerson> persons) {
		graphPanel.clearLockPerson();
		for (KPPerson kperson : persons) {
			KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(
					kperson.getPid(), curqid);
			if (kquestion != null) {
				if (kquestion.getValue().equals(Constants.DEFAULT_DNA)) {
					lockPersons.add(kperson.getPid());
				}
			}
		}
		graphPanel.lockPerson(lockPersons);
	}

	/**
	 * NKK
	 */
	public List<KPPerson> getKPersons() {
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();

		for (KPPerson kperson : persons) {
			if (kperson.getIsOther().equals(0)) {
				kpersons.add(kperson);
			}
		}
		return kpersons;
	}

	/**
	 * others
	 */
	public List<KPPerson> getKPersonsO() {
		List<KPPerson> kpersons = new ArrayList<KPPerson>();
		List<KPPerson> persons = ivMainPanel.kassService.getKPersons();

		for (KPPerson kperson : persons) {
			if (kperson.getIsOther().equals(1)) {
				kpersons.add(kperson);
			}
		}
		return kpersons;
	}

	/**
	 * Get the question status by the pid of a person
	 */
	public String getPQuestionStateByPid(Integer pid) {
		KQQuestion kquestion = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		if (kquestion != null) {
			return kquestion.getValue();
		}
		return "";
	}

	/**
	 * Set show or hide the tip
	 */
	public void ifshowPersonAnswers() {
		if (followupAnswerState.equals("1")) {
			ivMainPanel.graphPanel.showTip(curqid, false);
			ivMainPanel.ographPanel.showTip(curqid, false);
		} else {
			ivMainPanel.graphPanel.showTip(curqid, true);
			ivMainPanel.ographPanel.showTip(curqid, true);
		}
	}

	/**
	 * subsection = 007_01
	 */
	public void ifsubsection0071() {
		Question question = ivMainPanel.questionnaireService
				.searchQuestion(curqid);
		String subsectionid = question.getSubsection();
		if (subsectionid.equals("SS_007_01")) {
			ivMainPanel.showPersonAnswers(curqid);
		} else {
			ivMainPanel.graphPanel.showTip(curqid, true);
			ivMainPanel.ographPanel.showTip(curqid, true);
		}
	}

	/**
	 * Question type = DCR
	 */
	public void ifQuestionIsDCRSetShowMark() {
		if (questionPanel.paelq.type == 2) {
			ivMainPanel.showPersonAnswers(curqid);
		}
	}

	/**
	 * Check the completed status of the question and follow up question. If
	 * there are not NA, complete = 1
	 */
	private void jianchaQuestionifComplete() {
		if (finishPBtnIfcompleteCommon(ivMainPanel.kassService.getKPersons())) {
			setCompleted("1");
		}
	}
}
