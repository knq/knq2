package com.fangda.kass.ui.interview.support;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KILog;
import com.fangda.kass.model.interview.KInterviewInfo;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.util.DateUtil;

/**
 * This class for the timer of calculating the duration time of an interview in
 * the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class CalTimer {

	private static CalTimer calTimer = new CalTimer();

	private KassService kassService;

	private CalTimer() {
		if (kassService == null) {
			kassService = new KassService();
		}
	}

	public static CalTimer getInstance() {
		return calTimer;
	}

	private Date startTime;

	private Date endTime;

	private boolean isRuning = false;

	public String addOne() {
		return getNowTimeStr();
	}

	/**
	 * 
	 * @return
	 */
	public String getNowTimeStr() {
		if (startTime == null) {
			return "00:00:00";
		}
		int d = getAllDuration() + getBetTime(startTime, new Date());
		int h = d / 3600;
		int m = (d - h * 3600) / 60;
		int s = (d - h * 3600) % 60;
		StringBuffer sb = new StringBuffer();
		if (h <= 9) {
			sb.append("0").append(h);
		} else {
			sb.append(h);
		}
		sb.append(":");
		if (m <= 9) {
			sb.append("0").append(m);
		} else {
			sb.append(m);
		}
		sb.append(":");
		if (s <= 9) {
			sb.append("0").append(s);
		} else {
			sb.append(s);
		}
		return sb.toString();
	}

	public void stop() {
		endTime = new Date();
		isRuning = false;
	}

	public void start() {
		startTime = new Date();
		endTime = null;
		isRuning = true;
	}

	public void reset() {
		startTime = null;
		endTime = null;
		isRuning = false;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public boolean isRuning() {
		return isRuning;
	}

	public void setRuning(boolean isRuning) {
		this.isRuning = isRuning;
	}

	/**
	 * Save the log of interview
	 */
	public void saveLog() {
		if (endTime == null) {
			endTime = new Date();
		}
		String startTimeStr = DateUtil.format(startTime, "yyyy-MM-dd HH:mm:ss");
		String endTimeStr = DateUtil.format(endTime, "yyyy-MM-dd HH:mm:ss");
		KILog kilog = new KILog();
		kilog.setStartTime(startTimeStr);
		kilog.setEndTime(endTimeStr);
		int duration = getBetTime(startTime, endTime);
		kilog.setDuration(String.valueOf(duration));
		KInterviewInfo interview = kassService.get().getInterviewInfo();
		if (interview.getLogs().getKiLog().size() <= 0) {
			interview.setStartDate(startTimeStr);
		}
		interview.getLogs().getKiLog().add(kilog);
		// if(StringUtils.isBlank(interview.getStartDate())) {
		// }
		int d = getAllDuration();
		interview.setDuration(String.valueOf(d + duration));
		interview.setEndDate(endTimeStr);
		kassService.save();
	}

	public int getAllDuration() {
		KInterviewInfo interview = kassService.get().getInterviewInfo();
		int d = 0;
		if (StringUtils.isNotBlank(interview.getDuration())) {
			d = Integer.parseInt(interview.getDuration());
		}
		return d;
	}

	public int getBetTime(Date start, Date end) {
		int duration = (int) ((end.getTime() - start.getTime()) / 1000);
		return duration;
	}
}
