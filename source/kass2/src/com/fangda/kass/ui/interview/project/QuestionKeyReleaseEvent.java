package com.fangda.kass.ui.interview.project;

import java.awt.event.KeyEvent;

public interface QuestionKeyReleaseEvent {

	public void released(KeyEvent e);
}
