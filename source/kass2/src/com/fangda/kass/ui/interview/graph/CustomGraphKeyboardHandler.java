package com.fangda.kass.ui.interview.graph;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.action.CustomDeleteAction;
import com.fangda.kass.ui.interview.graph.action.CustomRedoAction;
import com.fangda.kass.ui.interview.graph.action.CustomUndoAction;
import com.mxgraph.swing.mxGraphComponent;

/**
 * The Class for the keyboard shortcut of the graph operation, such as delete,
 * undo, redo.
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomGraphKeyboardHandler extends GraphKeyboardHandler {
	private IvMainPanel ivMainPanel;

	public CustomGraphKeyboardHandler(mxGraphComponent graphComponent,
			IvMainPanel ivMainPanel) {
		super(graphComponent);
		this.ivMainPanel = ivMainPanel;
	}

	protected InputMap getInputMap(int condition) {
		InputMap map = new InputMap();
		if (condition == JComponent.WHEN_FOCUSED && map != null) {
			map.put(KeyStroke.getKeyStroke("control Z"), "undo");
			map.put(KeyStroke.getKeyStroke("control Y"), "redo");
			map.put(KeyStroke.getKeyStroke("DELETE"), "delete");
		}
		return map;
	}

	protected ActionMap createActionMap() {
		ActionMap map = (ActionMap) UIManager.get("ScrollPane.actionMap");
		map.put("delete", new CustomDeleteAction());
		map.put("undo", new CustomUndoAction());
		map.put("redo", new CustomRedoAction());
		return map;
	}

}
