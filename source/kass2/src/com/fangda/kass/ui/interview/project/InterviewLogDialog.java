package com.fangda.kass.ui.interview.project;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KILog;
import com.fangda.kass.model.interview.KInterviewInfo;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.BaseDialog;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of the interview log
 * 
 * @author Fangfang Zhao
 * 
 */
public class InterviewLogDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4357482833625085056L;
	JPanel panel;
	protected JTable InterviewLogTable;
	KassService service;

	public InterviewLogDialog(Map<String, Object> params) {
		super(params);

	}

	@Override
	public void initDialog() {
		// TODO Auto-generated method stub
		super.initDialog();
		service = new KassService();
		super.setTitle(mxResources.get("InterviewLog"));

	}

	@Override
	public JPanel createPanel() {
		KInterviewInfo interview = service.get().getInterviewInfo();
		List<KILog> kilog = interview.getLogs().getKiLog();
		String[] columnNames = { mxResources.get("No"),
				mxResources.get("startTime"), mxResources.get("endTime"),
				mxResources.get("Duration") };
		String[][] data = new String[kilog.size()][4];
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		for (int i = 0; i < kilog.size(); i++) {
			model.setValueAt(i + 1, i, 0);
			model.setValueAt(kilog.get(i).getStartTime(), i, 1);
			model.setValueAt(kilog.get(i).getEndTime(), i, 2);
			String durationTime = kilog.get(i).getDuration();
			String timeStr = null;
			int hour = 0;
			int minute = 0;
			int second = 0;
			if (Integer.parseInt(durationTime) <= 0)
				timeStr = "00:00";
			else {
				minute = Integer.parseInt(durationTime) / 60;
				if (minute < 60) {
					second = Integer.parseInt(durationTime) % 60;
					timeStr = unitFormat(minute) + ":" + unitFormat(second);
				} else {
					hour = minute / 60;
					if (hour > 99)
						durationTime = "99:59:59";
					minute = minute % 60;
					second = Integer.parseInt(durationTime) - hour * 3600
							- minute * 60;
					timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":"
							+ unitFormat(second);
				}
			}
			durationTime = timeStr;
			model.setValueAt(durationTime, i, 3);
		}
		InterviewLogTable = new JTable(model);
		InterviewLogTable.setAutoResizeMode(4);
		TableColumn column = null;
		for (int i = 0; i < 4; i++) {
			column = InterviewLogTable.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(10);
			} else {
				column.setPreferredWidth(50);
			}
		}
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		InterviewLogTable.getColumn(mxResources.get("No")).setCellRenderer(
				render);
		DefaultTableCellRenderer render2 = new DefaultTableCellRenderer();
		render2.setHorizontalAlignment(SwingConstants.RIGHT);
		InterviewLogTable.getColumn(mxResources.get("Duration"))
				.setCellRenderer(render2);
		JTableHeader tableHeader = InterviewLogTable.getTableHeader();
		tableHeader.setReorderingAllowed(false);
		DefaultTableCellRenderer hr = (DefaultTableCellRenderer) tableHeader
				.getDefaultRenderer();
		hr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

		JScrollPane scrollpane = new JScrollPane(InterviewLogTable);
		InterviewLogTable.setFillsViewportHeight(true);
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(scrollpane);
		return panel;
	}

	public static String unitFormat(int i) {
		String retStr = null;
		if (i >= 0 && i < 10)
			retStr = "0" + Integer.toString(i);
		else
			retStr = "" + i;
		return retStr;
	}

	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub

		return true;
	}

	@Override
	public void afterSubmit() {
		// TODO Auto-generated method stub
		super.afterSubmit();

	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.closeButton.setVisible(false);
		super.sureButton.setText(mxResources.get("exit"));
	}

	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		this.setVisible(false);
		this.dispose();
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);

	}
}
