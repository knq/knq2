package com.fangda.kass.ui.interview.graph;

/**
 * This class for the basic information of a union icon.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphUnionOne {

	private String type;

	private int id;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
