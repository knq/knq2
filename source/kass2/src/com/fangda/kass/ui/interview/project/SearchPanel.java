package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the Panel of searching a person in the diagram in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class SearchPanel extends JPanel {

	private static final long serialVersionUID = 8685833269541714473L;

	protected IvMainPanel ivMainPanel;

	private KassService kassService;

	private JTextField searchField;

	private JButton searchButton;

	private JTable table;

	private JScrollPane tableScrollPane;

	public SearchPanel(IvMainPanel ivMainPanel) {
		this.ivMainPanel = ivMainPanel;
		kassService = new KassService();
		initPanel();
	}

	private void initPanel() {
		this.setBackground(Color.white);
		this.setLayout(new VFlowLayout());
		String title = mxResources.get("search.button");
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(title),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		int width = ivMainPanel.leftQuestionPanel.getWidth();
		int height = ivMainPanel.leftQuestionPanel.getHeight();
		this.setPreferredSize(new Dimension(width - 15, height + 400));

		searchField = new JTextField(12);
		searchButton = new JButton(mxResources.get("search.button"));

		Object[][] cellData = {};
		String[] columnNames = { mxResources.get("search.no"),
				mxResources.get("search.name"),
				mxResources.get("search.isNKK"), mxResources.get("search.sex"),
				mxResources.get("search.isDeath"), "x", "y" };
		DefaultTableModel model = new DefaultTableModel(cellData, columnNames);
		table = new JTable(model);
		tableScrollPane = new JScrollPane(table,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane
				.setPreferredSize(new Dimension(width - 15, height + 400));
		tableScrollPane.setBackground(Color.WHITE);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // 单选
		table.getColumnModel().getColumn(0).setPreferredWidth(60);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(2).setPreferredWidth(80);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(4).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setMinWidth(0);
		table.getColumnModel().getColumn(5).setMaxWidth(0);
		table.getColumnModel().getColumn(6).setMinWidth(0);
		table.getColumnModel().getColumn(6).setMaxWidth(0);

		this.add(createPanel());
		initListener();
	}

	private JPanel createPanel() {
		JPanel panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		GridBagLayout bagLayout = new GridBagLayout();
		JPanel bagpanel = new JPanel(bagLayout);
		GridBagConstraints c = new GridBagConstraints();
		bagpanel.setBackground(Color.white);

		UIHelper.addToBagpanel(bagpanel, Color.white, searchField, c, 0, 0, 1,
				1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(bagpanel, Color.white, searchButton, c, 1, 0, 1,
				1, GridBagConstraints.WEST);

		table.setBackground(Color.WHITE);
		panel.add(bagpanel);
		panel.add(tableScrollPane);
		return panel;
	}

	private void initListener() {
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String val = searchField.getText();
				if (StringUtils.isNotBlank(val)) {
					List<mxCell> cells = ivMainPanel.graphPanel
							.searchByName(val);
					List<mxCell> ocells = ivMainPanel.ographPanel
							.searchByName(val);
					cells.addAll(ocells);
					addToTable(cells);
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				ivMainPanel.graphPanel.clearSelected();
				ivMainPanel.ographPanel.clearSelected();
				int row = table.getSelectedRow();
				String id = table.getValueAt(row, 0).toString();
				String isOther = table.getValueAt(row, 2).toString();
				String no = mxResources.get("base.no");
				if (StringUtils.equals(no, isOther)) {
					ivMainPanel.ographPanel.zoomToCell(Integer.parseInt(id));
				} else {
					ivMainPanel.graphPanel.zoomToCell(Integer.parseInt(id));
				}
			}
		});
	}

	private void addToTable(List<mxCell> cells) {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		for (mxCell cell : cells) {
			GraphPerson gp = (GraphPerson) cell.getValue();

			String isOther = (gp.getIsOther() == 1 ? mxResources.get("base.no")
					: mxResources.get("base.yes"));
			String isDeath = gp.getIsDeath() == 1 ? mxResources.get("base.yes")
					: mxResources.get("base.no");
			String sex = gp.getSex() == 1 ? mxResources.get("femaleLable")
					: mxResources.get("maleLable");

			tableModel.addRow(new Object[] { gp.getId(), gp.getName(), isOther,
					sex, isDeath, cell.getGeometry().getCenterX(),
					cell.getGeometry().getY() });
		}
	}
}
