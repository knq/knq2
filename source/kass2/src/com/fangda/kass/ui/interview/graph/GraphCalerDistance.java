package com.fangda.kass.ui.interview.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.service.davd.KnnWrapper;
import com.fangda.kass.service.davd.support.GenterOption;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.util.Constants;

/**
 * The Class for calculating the Union Generation and Kinship Distance for the
 * auto arrange of the NKK diagram (the network kinship diagram).
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class GraphCalerDistance {

	private KassService kassService;
	private List<KPPerson> persons;
	private List<KUUnion> unions;
	private String[][] knnRouting3;

	private List<PGPathArray> pgPaths;
	private List<UGPathArray> ugPaths;
	private List<DPerson> ps;
	private List<DUnion> us;

	public GraphCalerDistance(KassService kassService) {
		this.kassService = kassService;
	}

	public void init() {
		persons = kassService.getKPersons();
		Collections.sort(persons, new Comparator<KPPerson>() {
			@Override
			public int compare(KPPerson o1, KPPerson o2) {
				return o1.getPid() - o2.getPid();
			}

		});

		unions = kassService.getKUUnions();
		KPPerson ego = persons.get(0);
		// Parent-UINION
		KUUnion union = kassService.getPersonUnion(ego.getPid(), "offspring");
		if (union == null) {
			union = kassService.getPersonUnion(ego.getPid(), "partner");
		}
		KUUnion remove = null;
		for (KUUnion u : unions) {
			if (u.getId() == union.getId()) {
				remove = u;
				break;
			}
		}
		unions.remove(remove);
		Collections.sort(unions, new Comparator<KUUnion>() {
			@Override
			public int compare(KUUnion o1, KUUnion o2) {
				return o1.getId() - o2.getId();
			}

		});
		unions.add(0, union);

		String path = Constants.KASS_XML;
		GenterOption option = new GenterOption();
		option.setGenerType(3);
		List<String> ivFileNames = new ArrayList<String>();
		ivFileNames.add(path);
		option.setIvFileNames(ivFileNames);
		KnnWrapper warpper = new KnnWrapper(option);
		knnRouting3 = warpper.getResult();
		pgPaths = new ArrayList<PGPathArray>();
		for (int i = 0; i < persons.size(); i++) {
			PGPathArray pa = new PGPathArray();
			pa.setPid(persons.get(i).getPid());
			pgPaths.add(pa);
		}
		ugPaths = new ArrayList<UGPathArray>();
		for (int i = 0; i < unions.size(); i++) {
			UGPathArray ug = new UGPathArray();
			ug.setUid(unions.get(i).getId());
			ugPaths.add(ug);
		}
		ps = new ArrayList<DPerson>();
		for (int i = 0; i < persons.size(); i++) {
			DPerson p = new DPerson();
			p.setPid(persons.get(i).getPid());
			ps.add(p);
		}
		us = new ArrayList<DUnion>();
		for (int i = 0; i < unions.size(); i++) {
			DUnion u = new DUnion();
			u.setUid(unions.get(i).getId());
			if (i == 0) {
				List<KPPerson> list1 = this.getPersons(u.getUid(), "offspring");
				boolean flag = false;
				for (KPPerson p1 : list1) {
					if (p1.getPid() == 0) {
						u.setG(0);
						u.setPath(new String[] { "0" });
						u.setDis(0);
						flag = true;
						break;
					}
				}
				if (!flag) {
					List<KPPerson> list2 = this.getPersons(u.getUid(),
							"partern");
					for (KPPerson p1 : list2) {
						if (p1.getPid() == 0) {
							u.setG(-1);
							u.setPath(new String[] { "0" });
							u.setDis(0);
							break;
						}
					}
				}
			}
			us.add(u);
		}
	}

	/**
	 * Main method
	 */
	public void calUGPGPathMain() {

		calUGPGPath();
		this.distinctPGPath();
		this.distinctUGPath();
		this.calibrateG(this.us.get(0));
		reCalUG();
		// print();
	}

	private void reCalUG() {
		for (DUnion du : this.us) {
			if (du.getG() != null) {
				continue;
			}
			UGPathArray ua = this.getUGPath(du.getUid());
			int k = ua.getPaths().size();
			//
			List<UGPath> removes = new ArrayList<UGPath>();
			for (int a = 0; a < k; a++) {
				for (int b = a + 1; b < k; b++) {
					UGPath apath = ua.getPaths().get(a);
					UGPath bpath = ua.getPaths().get(b);
					if (apath.getUg() > bpath.getUg()) {
						removes.add(bpath);
					} else if (apath.getUg() == bpath.getUg()) {
						if (apath.getDis() <= bpath.getDis()) {
							removes.add(bpath);
						} else {
							removes.add(apath);
						}
					} else {
						removes.add(apath);
					}
				}
			}
			ua.getPaths().removeAll(removes);
			if (ua.getPaths().size() == 1) {
				UGPath apath = ua.getPaths().get(0);
				du.setG(apath.getUg());
				du.setPath(apath.getPath());
				du.setDis(apath.getDis());
				this.calibrateG(du);
			}

		}
	}

	private void calibrateG(DUnion union) {
		// Get the offsprings of the union by its uid.
		union.setMP(true);
		List<KPPerson> persons = getPersons(union.getUid(), "offspring");
		for (int c = 0; c < persons.size(); c++) {
			KPPerson person = persons.get(c);
			DPerson p = getPerson(person.getPid());
			if (p.getG() == null) {
				p.setG(union.getG());
				calPDisPath(p, union);
			}
			if (p.getG() != null && p.isPP() == false) {
				calPartnerU(p);
			}
		}
		persons = getPersons(union.getUid(), "partner");
		int h = persons.size();
		for (int g = 0; g < h; g++) {
			KPPerson person = persons.get(g);
			DPerson dp = this.getPerson(person.getPid());
			if (dp.getG() == null) {
				PGPathArray pa = this.getPGPath(person.getPid());
				int k = pa.getPaths().size();
				if (k > 1) {
					this.deleteUPPath(pa, union);
					if (pa.getPaths().size() > 1) {
						this.trimPGPath(pa);
						this.distinctPGPath(pa);
						this.deleteUPPath(pa, union);
					}
				}
				k = pa.getPaths().size();
				if (k == 1) {
					PGPath path = pa.getPaths().get(0);
					dp.setDis(path.getDis());
					dp.setG(path.getPg());
					dp.setPath(path.getPath());
				}
			}
			KUUnion parentUnion = getParentUnion(dp.getPid());
			if (dp.getG() != null && parentUnion != null) {
				DUnion up = this.getUnion(parentUnion.getId());
				if (up.g == null) {
					up.setG(dp.getG());
					up.setDis(dp.getDis());
					/*
					 * if(dp.getG() != null && dp.isPP == false) {
					 * calPartnerU(dp); }
					 */
					String[] paths = new String[dp.getPath().length + 1];
					for (int i = 0; i < dp.getPath().length; i++) {
						paths[i] = dp.getPath()[i];
					}
					int index = this.getLastColumnIndex(paths);
					paths[index + 1] = up.getUid() + "";
					up.setPath(paths);
					UGPathArray ua = this.getUGPath(up.getUid());
					deletePUGPath(ua, dp);
					// distinctUGPath(ua);
					this.calibrateG(up);
				}
			}
			if (dp.getG() != null && dp.isPP() == false) {
				this.calPartnerU(dp);
			}
		}
	}

	private KUUnion getParentUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid);
		for (KUUnion union : unions) {
			boolean isP = false;
			boolean isO = false;
			for (KUUMember member : union.getMembers()) {
				if ("partner".equals(member.getType()) && member.getId() != pid) {
					isP = true;
				}
				if ("offspring".equals(member.getType())
						&& member.getId() == pid) {
					isO = true;
				}
			}
			if (isP && isO) {
				return union;
			}
		}
		return null;
	}

	private void calPDisPath(DPerson p, DUnion u) {
		if (p.getPid() == 0) {
			p.setDis(0);
			p.setG(0);
			p.setPath(new String[] {});
			return;
		}
		String[] paths = new String[u.getPath().length + 1];
		for (int i = 0; i < u.getPath().length; i++) {
			paths[i] = u.getPath()[i];
		}
		int ind = this.getLastColumnIndex(paths);
		paths[ind + 1] = p.getPid() + "";
		p.setPath(paths);
		p.dis = u.getDis() + 1;

		PGPathArray pa = this.getPGPath(p.getPid());
		int k = pa.getPaths().size();
		List<PGPath> removes = new ArrayList<PGPath>();
		for (int y = 0; y < k; y++) {
			PGPath path = pa.getPaths().get(y);
			removes.add(path);
		}
		pa.getPaths().removeAll(removes);

		PGPath path = new PGPath();
		path.setPg(u.getG());
		path.setPath(p.getPath());
		path.setDis(path.getPath().length / 2);
		pa.getPaths().add(path);
	}

	private void calPartnerU(DPerson p) {
		p.setPP(true);
		List<KUUnion> unions = kassService.getUnionByPid(p.getPid());

		List<KUUnion> uus = new ArrayList<KUUnion>();
		for (KUUnion union : unions) {
			boolean flag = false;
			for (KUUMember km : union.getMembers()) {
				if (StringUtils.equals(km.getType(), "partner")
						&& km.getId() == p.getPid()) {
					flag = true;
					break;
				}
			}
			if (flag) {
				uus.add(union);
			}
		}
		for (KUUnion union : uus) {
			DUnion u = this.getUnion(union.getId());
			if (u.isMP()) {
				continue;
			}
			if (u.getG() == null) {
				UGPathArray ua = this.getUGPath(u.getUid());
				int k = ua.getPaths().size();
				if (k > 1) {
					deleteUGPath(ua, p);
					if (ua.getPaths().size() > 1) {
						this.trimUGPath(ua);
						this.distinctUGPath(ua);
						this.deleteUGPath(ua, p);
					}
				}
				k = ua.getPaths().size();
				if (k == 1) {
					UGPath path = ua.getPaths().get(0);
					u.setG(path.getUg());
					u.setPath(path.getPath());
					u.setDis(path.getDis());
				}
				if (u.getG() != null) {
					this.calibrateG(u);
				}
			}
		}
	}

	private void deleteUGPath(UGPathArray ua, DPerson p) {
		int k = ua.getPaths().size();
		List<UGPath> removes = new ArrayList<UGPath>();
		for (int n = 0; n < k; n++) {
			if (ua.getPaths().get(n).getUg() >= p.getG()) {
				removes.add(ua.getPaths().get(n));
			}
		}
		ua.getPaths().removeAll(removes);
	}

	private void deletePUGPath(UGPathArray ua, DPerson p) {
		int k = ua.getPaths().size();
		List<UGPath> removes = new ArrayList<UGPath>();
		for (int n = 0; n < k; n++) {
			if (ua.getPaths().get(n).getUg() != p.getG()) {
				removes.add(ua.getPaths().get(n));
			}
		}
		ua.getPaths().removeAll(removes);
	}

	private void deleteUPPath(PGPathArray pa, DUnion union) {
		int k = pa.getPaths().size();
		List<PGPath> removes = new ArrayList<PGPath>();
		if (k > 1) {
			for (int y = 0; y < k; y++) {
				PGPath path = pa.getPaths().get(y);
				if (path.getPg() <= union.getG()) {
					removes.add(path);
				}
			}
		}
		pa.getPaths().removeAll(removes);
	}

	private void trimUGPath(UGPathArray ua) {
		int k = ua.getPaths().size();
		for (int y = 0; y < k; y++) {
			UGPath path = ua.getPaths().get(y);
			int index = this.getLastColumnIndex(path.getPath());
			String[] paths = new String[index + 1];
			System.arraycopy(path.getPath(), 0, paths, 0, index + 1);
			for (int m = paths.length - 1; m >= 0; m--) {
				if (m % 2 == 0) {// union
					int uid = Integer.parseInt(path.getPath()[m]);
					DUnion du = this.getUnion(uid);
					if (du.getG() != null) {
						String[] path1 = new String[m + 1];
						String[] path2 = new String[paths.length - m - 1];
						System.arraycopy(paths, 0, path1, 0, path1.length);
						System.arraycopy(paths, m + 1, path2, 0, path2.length);
						//
						int ind = this.getLastColumnIndex(du.getPath());
						String[] pth = new String[ind + 1 + path2.length];
						System.arraycopy(du.getPath(), 0, pth, 0, ind + 1);
						System.arraycopy(path2, 0, pth, ind + 1, path2.length);
						path.setPath(pth);
						path.setUg(this.calUG(pth));
						path.setDis((pth.length + 1) / 2);
						break;
					}
				} else if (m % 2 == 1) {// pid
					int pid = Integer.parseInt(path.getPath()[m]);
					DPerson dp = this.getPerson(pid);
					if (dp.getG() != null) {
						String[] path1 = new String[m + 1];
						String[] path2 = new String[paths.length - m - 1];
						System.arraycopy(paths, 0, path1, 0, path1.length);
						System.arraycopy(paths, m + 1, path2, 0, path2.length);
						//
						int ind = this.getLastColumnIndex(dp.getPath());
						String[] pth = new String[ind + 1 + path2.length];
						System.arraycopy(dp.getPath(), 0, pth, 0, ind + 1);
						System.arraycopy(path2, 0, pth, ind + 1, path2.length);
						path.setPath(pth);
						path.setUg(this.calUG(pth));
						path.setDis((pth.length + 1) / 2);
						break;
					}
				}
			}
		}
	}

	private void trimPGPath(PGPathArray pa) {
		if (pa.getPid() == 9) {
			System.out.println("aaa");
		}
		int k = pa.getPaths().size();
		for (int y = 0; y < k; y++) {
			PGPath path = pa.getPaths().get(y);
			int index = this.getLastColumnIndex(path.getPath());
			String[] paths = new String[index + 1];
			System.arraycopy(path.getPath(), 0, paths, 0, index + 1);
			for (int m = paths.length - 1; m >= 0; m--) {
				if (m % 2 == 0) {// union
					int uid = Integer.parseInt(paths[m]);
					DUnion du = this.getUnion(uid);
					if (du.getG() != null) {
						String[] path1 = new String[m + 1];
						String[] path2 = new String[paths.length - m - 1];
						System.arraycopy(paths, 0, path1, 0, path1.length);
						System.arraycopy(paths, m + 1, path2, 0, path2.length);
						//
						int ind = this.getLastColumnIndex(du.getPath());
						String[] pth = new String[ind + 1 + path2.length];
						System.arraycopy(du.getPath(), 0, pth, 0, ind + 1);
						System.arraycopy(path2, 0, pth, ind + 1, path2.length);
						path.setPath(pth);
						path.setPg(this.calculatePG(pth, pa.getPid()));
						path.setDis(pth.length / 2);
						break;
					}
				} else if (m % 2 == 1) {// pid
					int pid = Integer.parseInt(paths[m]);
					DPerson dp = this.getPerson(pid);
					if (dp.getG() != null) {
						String[] path1 = new String[m + 1];
						String[] path2 = new String[paths.length - m - 1];
						System.arraycopy(paths, 0, path1, 0, path1.length);
						System.arraycopy(paths, m + 1, path2, 0, path2.length);
						//
						int ind = this.getLastColumnIndex(dp.getPath());
						String[] pth = new String[ind + 1 + path2.length];
						System.arraycopy(dp.getPath(), 0, pth, 0, ind + 1);
						System.arraycopy(path2, 0, pth, ind + 1, path2.length);
						path.setPath(pth);
						path.setPg(this.calculatePG(pth, pa.getPid()));
						path.setDis(pth.length / 2);
						break;
					}
				}
			}
		}
	}

	private PGPathArray getPGPath(int pid) {
		for (PGPathArray pa : this.pgPaths) {
			if (pa.getPid() == pid) {
				return pa;
			}
		}
		return null;
	}

	private UGPathArray getUGPath(int uid) {
		for (UGPathArray ua : this.ugPaths) {
			if (ua.getUid() == uid) {
				return ua;
			}
		}
		return null;
	}

	private DPerson getPerson(int pid) {

		for (DPerson dp : this.ps) {
			if (dp.getPid() == pid) {
				return dp;
			}
		}
		return null;
	}

	private DUnion getUnion(int uid) {
		for (DUnion du : this.us) {
			if (du.getUid() == uid) {
				return du;
			}
		}
		return null;
	}

	private List<KPPerson> getPersons(int uid, String type) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		List<KPPerson> persons = new ArrayList<KPPerson>();
		for (KUUMember member : members) {
			if (StringUtils.equals(member.getType(), type)) {
				KPPerson kpperson = kassService.getPerson(member.getId());
				persons.add(kpperson);
			}
		}
		return persons;
	}

	/**
	 * Distinct the kinship path of a person (Removing the repeat paths.)
	 */
	private void distinctPGPath() {
		for (int i = 0; i < pgPaths.size(); i++) {
			PGPathArray pa = pgPaths.get(i);
			distinctPGPath(pa);
		}
	}

	private void distinctPGPath(PGPathArray pa) {
		List<PGPath> list = pa.getPaths();
		List<PGPath> removes = new ArrayList<PGPath>();
		int a = list.size();
		for (int m = 0; m < a; m++) {
			for (int n = m + 1; n < a; n++) {
				PGPath p1 = list.get(m);
				PGPath p2 = list.get(n);
				if (this.equalArray(p1.getPath(), p2.getPath())) {
					removes.add(p2);
				}
			}
		}
		list.removeAll(removes);
	}

	/**
	 * Distinct the kinship path of a union (Removing the repeat paths.)
	 */
	private void distinctUGPath() {
		for (int i = 0; i < ugPaths.size(); i++) {
			UGPathArray pa = ugPaths.get(i);
			distinctUGPath(pa);
		}
	}

	private void distinctUGPath(UGPathArray pa) {
		List<UGPath> list = pa.getPaths();
		List<UGPath> removes = new ArrayList<UGPath>();
		int b = list.size();
		for (int m = 0; m < b; m++) {
			for (int n = m + 1; n < b; n++) {
				UGPath p1 = list.get(m);
				UGPath p2 = list.get(n);
				if (this.equalArray(p1.getPath(), p2.getPath())) {
					removes.add(p2);
				}
			}
		}
		list.removeAll(removes);
	}

	public void calUGPGPath() {
		for (int i = 0; i < persons.size(); i++) {
			List<String[]> paths = getRouting3(persons.get(i));
			PGPathArray pa = pgPaths.get(i);
			for (int a = 0; a < paths.size(); a++) {
				PGPath path = new PGPath();
				path.setPath(paths.get(a));
				int index = this.getLastColumnIndex(path.getPath());
				path.setDis((index + 1) / 2);// union的数量
				path.setPg(this.calculatePG(path.getPath(), persons.get(i)
						.getPid()));
				pa.getPaths().add(path);
			}
		}
	}

	/**
	 * Calculate the generation of a person and the end-union of the path based
	 * the simple path (also see the Matrix Table 3).
	 * 
	 * @param paths
	 * @param person
	 * @return
	 */
	private int calculatePG(String[] paths, int pid) {
		int index = this.getLastColumnIndex(paths);
		int uid = Integer.parseInt(paths[index - 1]);// end uid
		KUUMember kuu = kassService.getUnionMember(uid, pid);
		int pg = 0;
		if ("offspring".equals(kuu.getType())) {
			pg = this.calculateUG(paths);
		} else {
			pg = this.calculateUG(paths) + 1;
		}
		return pg;
	}

	/**
	 * Calculate the generation of the person based the its path.
	 * 
	 * @param paths
	 * @return
	 */
	private int calculateUG(String[] paths) {
		int index = this.getLastColumnIndex(paths);
		// end Union
		int uid = Integer.parseInt(paths[index - 1]);
		int n = (index + 1) / 2;
		for (int i = 0; i < ugPaths.size(); i++) {
			if (uid == ugPaths.get(i).uid) {// get the end union uid == ugPaths
				String[] uPaths = new String[index];// remove the END PID
				for (int j = 0; j < uPaths.length; j++) {
					uPaths[j] = paths[j];
				}
				//
				int k = ugPaths.get(i).getPaths().size();
				if (k == 0) {
					UGPath path = new UGPath();
					path.setDis(n - 1);
					path.setPath(uPaths);
					path.setUg(calUG(uPaths));
					ugPaths.get(i).getPaths().add(path);
					return path.getUg();
				} else {
					for (int m = 0; m < k; m++) {
						UGPath ugPath = ugPaths.get(i).getPaths().get(m);
						if (equalArray(ugPath.getPath(), uPaths)) {
							return ugPath.getUg();
						} else {
							int tempUG = calUG(uPaths);
							if (ugPath.getDis() <= n - 1) {
								UGPath ugPath1 = new UGPath();
								ugPath1.setDis(n - 1);
								ugPath1.setPath(uPaths);
								ugPath1.setUg(tempUG);
								ugPaths.get(i).getPaths().add(ugPath1);
							} else {
								ugPath.setDis(n - 1);
								ugPath.setPath(uPaths);
								ugPath.setUg(tempUG);
							}
							return tempUG;
						}
					}
				}
			}
		}
		return -100;
	}

	private boolean equalArray(String[] array1, String[] array2) {
		if (array1.length != array2.length) {
			return false;
		}
		int index1 = this.getLastColumnIndex(array1);
		int index2 = this.getLastColumnIndex(array2);
		if (index1 != index2) {
			return false;
		}
		for (int i = 0; i < index1; i++) {
			String a = array1[i];
			String b = array2[i];
			if (!StringUtils.equals(a, b)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Calculate the generation of the union based the its path.
	 * 
	 * @param uPaths
	 * @return
	 */
	private int calUG(String[] uPaths) {
		Map<Integer, Integer> uMap = new HashMap<Integer, Integer>();
		int uid00 = Integer.parseInt(uPaths[0]);
		KUUMember km = kassService.getUnionMember(uid00, 0);
		if ("offspring".equals(km.getType())) {
			uMap.put(uid00, 0);
		} else {
			uMap.put(uid00, -1);
		}
		int index = this.getLastColumnIndex(uPaths);
		for (int i = 1; i < index; i = i + 2) {
			int uid0 = Integer.parseInt(uPaths[i - 1]);
			int pid = Integer.parseInt(uPaths[i]);
			int uid1 = Integer.parseInt(uPaths[i + 1]);
			KUUMember kuuMember0 = kassService.getUnionMember(uid0, pid);
			KUUMember kuuMember1 = kassService.getUnionMember(uid1, pid);
			if (kuuMember0 == null || kuuMember1 == null) {
				System.out.println("aaaa");
			}
			if ("partner".equals(kuuMember1.getType())
					&& "offspring".equals(kuuMember0.getType())) {
				Integer a = uMap.get(uid0) - 1;
				uMap.put(uid1, a);
			} else if ("offspring".equals(kuuMember1.getType())
					&& "partner".equals(kuuMember0.getType())) {
				Integer a = uMap.get(uid0) + 1;
				uMap.put(uid1, a);
			} else if ("partner".equals(kuuMember1.getType())
					&& "partner".equals(kuuMember0.getType())) {
				uMap.put(uid1, uMap.get(uid0));
			}
		}
		int uidEnd = Integer.parseInt(uPaths[index]);
		return uMap.get(uidEnd);
	}

	private int calculatePGendU(PGPath path, DUnion endUnion) {
		int index = this.getLastColumnIndex(path.getPath());
		int pid = Integer.parseInt(path.getPath()[index]);
		KUUMember kuu = kassService.getUnionMember(endUnion.getUid(), pid);
		if ("partner".equals(kuu.getType())) {
			return endUnion.getG() + 1;
		} else {
			return endUnion.getG();
		}
	}

	private List<String[]> getRouting3(KPPerson kpperson) {
		int pid = kpperson.getPid();
		List<String[]> paths = new ArrayList<String[]>();
		for (int i = 0; i < knnRouting3.length; i++) {
			String[] path = knnRouting3[i];
			int index = getLastColumnIndex(path);
			if ((path[index]).equals(pid + "")) {
				paths.add(path);
			}
		}
		return paths;
	}

	private int getLastColumnIndex(String[] columns) {
		int length = columns.length;
		for (int i = length - 1; i >= 0; i--) {
			if (StringUtils.isNotBlank(columns[i])) {
				return i;
			}
		}
		return -1;
	}

	private void print() {
		System.out.println("===============PGPath============");
		for (PGPathArray pa : this.pgPaths) {
			int pid = pa.getPid();
			if (pa.getPaths().size() > 0) {
				for (PGPath path : pa.getPaths()) {
					System.out.println("pid = " + pid + " pg: " + path.getPg()
							+ " dis:" + path.getDis() + " path:"
							+ StringUtils.join(path.getPath(), ","));
				}
			} else {
				System.out.println("pid = " + pid + " []");
			}
		}
		System.out.println("===============UGPath============");
		for (UGPathArray ua : this.ugPaths) {
			int uid = ua.getUid();
			for (UGPath path : ua.getPaths()) {
				System.out.println("uid = " + uid + " ug: " + path.getUg()
						+ " dis:" + path.getDis() + " path:"
						+ StringUtils.join(path.getPath(), ","));
			}
		}
		System.out.println("===============p[]============");
		for (DPerson dp : this.ps) {
			System.out.println("pid : " + dp.getPid() + "  dis : "
					+ dp.getDis() + " g : " + dp.getG() + " path : "
					+ StringUtils.join(dp.getPath(), ","));
		}
		System.out.println("===============U[]============");
		for (DUnion du : this.us) {
			System.out.println("uid : " + du.getUid() + "  dis : "
					+ du.getDis() + " g : " + du.getG() + " path : "
					+ StringUtils.join(du.getPath(), ","));
		}
		System.out.println("===============4============");
	}

	public List<DPerson> getPersons() {
		return this.ps;
	}

	public List<DUnion> getUnions() {
		return this.us;
	}

	class PGPathArray {

		private int pid;

		private List<PGPath> paths = new ArrayList<PGPath>();

		public int getPid() {
			return pid;
		}

		public void setPid(int pid) {
			this.pid = pid;
		}

		public List<PGPath> getPaths() {
			return paths;
		}

		public void setPaths(List<PGPath> paths) {
			this.paths = paths;
		}

	}

	class PGPath {

		private String[] path;

		private int dis = 0;

		private int pg = 0;

		public String[] getPath() {
			return path;
		}

		public void setPath(String[] path) {
			this.path = path;
		}

		public int getDis() {
			return dis;
		}

		public void setDis(int dis) {
			this.dis = dis;
		}

		public int getPg() {
			return pg;
		}

		public void setPg(int pg) {
			this.pg = pg;
		}
	}

	class UGPathArray {

		private int uid;

		private List<UGPath> paths = new ArrayList<UGPath>();

		public int getUid() {
			return uid;
		}

		public void setUid(int uid) {
			this.uid = uid;
		}

		public List<UGPath> getPaths() {
			return paths;
		}

		public void setPaths(List<UGPath> paths) {
			this.paths = paths;
		}

	}

	class UGPath {

		private String[] path;

		private int dis = 0;

		private int ug = 0;

		public String[] getPath() {
			return path;
		}

		public void setPath(String[] path) {
			this.path = path;
		}

		public int getDis() {
			return dis;
		}

		public void setDis(int dis) {
			this.dis = dis;
		}

		public int getUg() {
			return ug;
		}

		public void setUg(int ug) {
			this.ug = ug;
		}
	}

	class DPerson {
		private int pid;
		private Integer g;
		private Integer dis;
		private String[] path;
		private boolean isPP = false;

		public int getPid() {
			return pid;
		}

		public void setPid(int pid) {
			this.pid = pid;
		}

		public Integer getG() {
			return g;
		}

		public void setG(Integer g) {
			this.g = g;
		}

		public Integer getDis() {
			return dis;
		}

		public void setDis(Integer dis) {
			this.dis = dis;
		}

		public String[] getPath() {
			return path;
		}

		public void setPath(String[] path) {
			this.path = path;
		}

		public boolean isPP() {
			return isPP;
		}

		public void setPP(boolean isPP) {
			this.isPP = isPP;
		}
	}

	class DUnion {
		private int uid;
		private Integer g;
		private Integer dis;
		private String[] path;
		private boolean isMP = false;

		public Integer getG() {
			return g;
		}

		public void setG(Integer g) {
			this.g = g;
		}

		public Integer getDis() {
			return dis;
		}

		public void setDis(Integer dis) {
			this.dis = dis;
		}

		public String[] getPath() {
			return path;
		}

		public void setPath(String[] path) {
			this.path = path;
		}

		public int getUid() {
			return uid;
		}

		public void setUid(int uid) {
			this.uid = uid;
		}

		public boolean isMP() {
			return isMP;
		}

		public void setMP(boolean isMP) {
			this.isMP = isMP;
		}
	}
}
