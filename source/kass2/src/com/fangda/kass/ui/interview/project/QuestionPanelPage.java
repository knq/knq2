package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.InterviewInfo;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KPeople;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.CustomLabel;
import com.fangda.kass.ui.common.MultiLabel;
import com.fangda.kass.ui.common.Toast;
import com.fangda.kass.ui.common.Toast.Style;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.Interviewcontain;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.CustomGraph;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the display of question in the IMS
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * 
 */
@SuppressWarnings(value = { "unchecked", "unused" })
public class QuestionPanelPage extends JPanel {

	private static final long serialVersionUID = -5496801034414354780L;
	protected JPanel container;
	protected IvMainPanel ivMainPanel;
	public Question question;
	protected QuestionnaireService questionnaireService;
	protected JScrollPane flQue;
	public static Interviewcontain inerviewcont;
	public List<FollowupQuestion> fquestions;
	protected JPanel pageSave;
	public String curqid;
	public JButton saveBtn;
	public JButton viewBtn;
	public JButton sureBtn;
	public JButton selectInDiagram;
	public JButton addNewPerson;
	public int dbpersonid;
	private JPanel followupPanel;

	// Type：
	// 0: Questions in the section 4-6
	// 1: F/J， Questions about the kinship term
	// 2：R, DCR questions
	// 3: A, Questions without follow up question
	public int type = 0;

	public QuestionPanelPage(IvMainPanel ivMainPanel, Question question) {
		questionnaireService = new QuestionnaireService();
		this.ivMainPanel = ivMainPanel;
		this.question = question;
		inerviewcont = new Interviewcontain();
		this.initPanel();
	}

	public void initPanel() {
		this.setBackground(Color.white);
		this.setVisible(true);
		this.setLayout(new VFlowLayout());
		createPanel();

	}

	/**
	 * Initialize when press the buttons of mark, previous, next and follow up
	 * question
	 * 
	 * @return
	 */
	private void createPanel() {
		if ("F".equals(question.getType()) || "J".equals(question.getType())) {
			type = 1;
		} else if ("R".equals(question.getType())) {
			type = 2;
		} else if ("A".equals(question.getType())) {
			type = 3;
		}

		inerviewcont.setQuestionsMap(question);
		curqid = question.getQid();

		int index = 0;
		GridBagConstraints c = new GridBagConstraints();
		getJScrollPane();
		followupQuestionPanel();
		index++;
		UIHelper.addToBagpanel(this, Color.white, flQue, c, index, 0, 1, 1,
				GridBagConstraints.CENTER);
		flQue.setVisible(false);

		index++;
		createPageSave();
		UIHelper.addToBagpanel(this, Color.white, pageSave, c, index, 0, 1, 1,
				GridBagConstraints.CENTER);
	}

	public int addtypeRPersonInfo(int index, JPanel panel, GridBagConstraints c) {
		if (type == 2) {
			JLabel personNum = new JLabel("Person Number:");
			UIHelper.addToBagpanel(panel, Color.white, personNum, c, 0, index,
					1, 1, GridBagConstraints.WEST);
			index++;

			JLabel faminliName = new JLabel("Family Name:");
			UIHelper.addToBagpanel(panel, Color.white, faminliName, c, 0,
					index, 1, 1, GridBagConstraints.WEST);
			index++;

			JLabel otherName = new JLabel("Other Name:");
			UIHelper.addToBagpanel(panel, Color.white, otherName, c, 0, index,
					1, 1, GridBagConstraints.WEST);
			index++;

		}
		return index;
	}

	/**
	 * Create the buttons of OK and cancel
	 */
	public void createPageSave() {
		pageSave = new JPanel();
		pageSave.setBackground(Color.white);
		pageSave.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		sureBtn = new JButton(mxResources.get("cancel"));
		sureBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ivMainPanel.jqustionpanel.pael2.InitLockPerson();
				ivMainPanel.jqustionpanel.pael2.followupAnswerState = "0";
				ivMainPanel.jqustionpanel.pael2.InitSelectPerson();
				ivMainPanel.jqustionpanel.refresh();
				setFolloupQuestionPageShow(false);
				ivMainPanel.jqustionpanel.pael2.ifshowPersonAnswers();
				ivMainPanel.jqustionpanel
						.updateLeadDetail(ivMainPanel.jqustionpanel.question);
			}

		});
		pageSave.add(sureBtn);
		saveBtn = new JButton(mxResources.get("sure"));
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actionSave();
			}
		});
		pageSave.add(saveBtn);

		viewBtn = new JButton(mxResources.get("folloupPageViewAnswers"));
		viewBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewPesonsAnswers();
			}
		});
		pageSave.add(viewBtn);

		pageSave.setVisible(false);
	}

	public void actionSave() {
		saveFollowupQuestion();
		ivMainPanel.kassService.save();
	}

	private void saveFollowupQuestion() {
		if (type == 2) {
			checkQuestionDCRSetOK();
		} else {
			checkQuestionSetOK();
		}

		ivMainPanel.jqustionpanel.pael2.InitLockPerson();
		ivMainPanel.jqustionpanel.pael2.followupAnswerState = "0";
		ivMainPanel.jqustionpanel.pael2.InitSelectPerson();
		ivMainPanel.jqustionpanel.pael2.selectPBtn.setText(mxResources
				.get("markOpen"));
		setFolloupQuestionPageShow(false);
		ivMainPanel.jqustionpanel.pael2.okifcomplete();
		ivMainPanel.jqustionpanel
				.updateLeadDetail(ivMainPanel.jqustionpanel.question);
		ivMainPanel.jqustionpanel.refresh();
	}

	/**
	 * Check the completed status
	 */
	private void checkQuestionSetOK() {
		List<KPPerson> pserons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : pserons) {
			KQQuestion question = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (question.getValue().equals(Constants.DEFAULT_MARK)) {
				Boolean flag = false;
				List<KQQuestionField> fields = question.getFields();
				for (KQQuestionField field : fields) {
					if (field.getValue().equals(Constants.DEFAULT_NA)) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					question.setValue(Constants.DEFAULT_OK);
				}
			}
		}

	}

	/**
	 * Saving the mark person
	 * 
	 * @param qst
	 */
	public void saveAnswerToMarkPerson() {
		List<KPPerson> persons = ivMainPanel.kassService.get().getPeople()
				.getPersons();
		for (KPPerson person : persons) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (kqquestion.getValue().equals("Mark")) {
				saveAnswerToMarkPersonQuestion(kqquestion);
			}
		}
	}

	/**
	 * Save the answer of the mark person
	 */
	public void saveAnswerToMarkPersonQuestion(KQQuestion kqquestion) {
		List<KQQuestionField> fields = kqquestion.getFields();
		for (KQQuestionField field : fields) {
			String fname = field.getQid();
			if (fname.contains("A")) {
				JCheckBox chin = (JCheckBox) inerviewcont.checkboxMap
						.get(fname);
				if (chin != null) {

				}
			} else {
				JRadioButton radioButton = (JRadioButton) inerviewcont.radioMap
						.get(field.getQid());
				if (radioButton != null) {

				}
			}
		}
	}

	public void upDatePageQuestion(Question qst) {
		inerviewcont.clean();
		inerviewcont.setQuestionsMap(qst);
		curqid = qst.getQid();
		followupQuestionPanel();
		if (type == 1) {
			setFolloupQuestionPageShow(true);
		} else {
			setFolloupQuestionPageShow(false);
		}
		ivMainPanel.repaint();
	}

	public void setFolloupQuestionPageShow(Boolean flag) {
		flQue.setVisible(flag);
		pageSave.setVisible(flag);
	}

	public void setFolloupQuestionPageShowExceptBtn(Boolean flag) {
		flQue.setVisible(flag);
		pageSave.setVisible(!flag);
	}

	/**
	 * Create a panel
	 */
	private void getJScrollPane() {
		flQue = new JScrollPane();
		flQue.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		flQue.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flQue.setBackground(Color.white);
		// flQue.setBorder(BorderFactory.createLineBorder(Color.blue));
		flQue.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
	}

	private void followupQuestionPanel() {

		followupPanel = new JPanel(new VFlowLayout());
		followupPanel.setBackground(Color.white);
		int width = ivMainPanel.leftQuestionPanel.getWidth();
		int height = ivMainPanel.leftQuestionPanel.getHeight();

		int index = 0;
		followupQuestionPanel3(followupPanel);

		flQue.getViewport().removeAll();
		flQue.getViewport().add(followupPanel);
		flQue.repaint();
	}

	public void resize(int width) {
		int size = followupPanel.getComponentCount();
		for (int i = 0; i < size; i++) {
			Component component = followupPanel.getComponent(i);
			if (component instanceof JLabel) {
				CustomLabel jlabel = (CustomLabel) component;
				MultiLabel label = (MultiLabel) jlabel.getAttr();
				System.out.println(width);
				label.resize(width);
			}
		}
	}

	public int addOption(JPanel panel, Question question) {
		InterviewInfo info = (InterviewInfo) inerviewcont.questionMap
				.get(question.getQid());
		if (info == null) {
			InterviewInfo tinfo = new InterviewInfo();
			tinfo.setQtype(question.getType());
			inerviewcont.setQuestionsMap(question);
			inerviewcont.questionMap.put(question.getQid(), tinfo);
			inerviewcont.setKqquestions(getKQuestionbyqid(StringUtil
					.substringBeforeLast(question.getQid(), "_")));
			info = tinfo;
		}
		if ("A".equals(question.getType())) {
			String key = question.getQid();
			JTextField field = (JTextField) inerviewcont.textField.get(key);
			if (field == null) {
				field = new JTextField(30);
				inerviewcont.textField.put(key, field);
			}
			field.setName(key);
			if (StringUtils.equals(((FollowupQuestion) question).getDataType(),
					"int")) {
				field.addKeyListener(intKeylistener);
			} else if (StringUtils.equals(
					((FollowupQuestion) question).getDataType(), "decimal")) {
				field.addKeyListener(decimalKeylistener);
			} else if (StringUtils.equals(
					((FollowupQuestion) question).getDataType(), "percentage")) {
				field.addKeyListener(perCentKeylistener);
			} else if (StringUtils.equals(
					((FollowupQuestion) question).getDataType(), "string")) {
				field.addKeyListener(perCentKeylistenerString);
			}
			panel.add(field);
		} else if ("T".equals(question.getType())) {
			String key = question.getQid();
			JTextArea noteField = (JTextArea) inerviewcont.noteField.get(key);
			if (noteField == null) {
				noteField = new JTextArea(5, 30);
				inerviewcont.noteField.put(key, noteField);
				inerviewcont.noteFieldLabel.put(key, question.getLabel());
			}

			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(notePane);
		} else if ("D".equals(question.getType())) {//

		} else if ("Q".equals(question.getType())) {
			addOptionQ(panel, question, info);
		} else {
			addOptionCQ(panel, question, info);
		}
		return 0;
	}

	public void updatePage() {
		followupQuestionPanel();
	}

	private void followupQuestionPanel3(JPanel followupPanel) {
		fquestions = question.getFollowupQuestions();
		for (FollowupQuestion flpqs : fquestions) {
			if (flpqs != null) {
				if (flpqs.getDisable() == 2)
					continue;
				if (flpqs.getDetail() != null) {
					int width = ivMainPanel.leftQuestionPanel.getWidth() - 40;
					MultiLabel multiLabel = new MultiLabel(flpqs.getDetail(),
							Font.BOLD, 13);
					multiLabel.resize(width);

					followupPanel.add(multiLabel.getLabel());
					if (question.getType().equals("R")
							&& flpqs.getType().equals("R")) {
						selectInDiagram = new JButton(
								mxResources.get("selectInDiagram"));
						selectInDiagram.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								// dbClickQuestionStartSecondSelectPerson();
								if (getSelectPerson()) {
									Map<String, Object> map = new HashMap<String, Object>();
									map.put("op", "select");
									map.put("ivMainPanel", ivMainPanel);
									new SelectPersonDialog(map)
											.setMainPanel(ivMainPanel.kmain);
								} else {
									Toast.makeText(ivMainPanel.kmain.frame,
											mxResources.get("selectAPerson"),
											Style.SUCCESS).display();
								}
							}
						});
						followupPanel.add(selectInDiagram);
					}
					if (question.getType().equals("R")
							&& flpqs.getType().equals("N")) {
						addNewPerson = new JButton(
								mxResources.get("addNewPerson"));
						addNewPerson.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Map<String, Object> map = new HashMap<String, Object>();
								map.put("op", "add");
								new NewOtherPersonDialog(map)
										.setMainPanel(ivMainPanel.kmain);
							}

						});
						followupPanel.add(addNewPerson);
					}
				}
				addOption(followupPanel, flpqs);
			}
		}
	}

	public void addPanelOption(JPanel panel, Question question) {
		InterviewInfo info = (InterviewInfo) inerviewcont.questionMap
				.get(question.getQid());
		if (info == null) {
			InterviewInfo tinfo = new InterviewInfo();
			tinfo.setQtype(question.getType());
			inerviewcont.questionMap.put(question.getQid(), tinfo);
			info = tinfo;
		}
		if ("A".equals(question.getType())) {
			String key = question.getQid();
			JTextField field = (JTextField) inerviewcont.textField.get(key);
			if (field == null) {
				field = new JTextField(10);
				inerviewcont.textField.put(key, field);
			}
			panel.add(field);
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(noteField);
		} else if ("Q".equals(question.getType())) {
			addPanelOptionQ(panel, question, info);
		} else if ("D".equals(question.getType())) {
			addPanelOptionD(panel, question, info);
		} else {
			addPanelOptionCQ(panel, question, info);
		}
	}

	private void addPanelOptionCQ(JPanel panel, Question question,
			InterviewInfo info) {
		if (question.getOptions() != null) {
			for (Option option : question.getOptions()) {
				String optionTitle = option.getDetail() == null ? "" : option
						.getDetail();
				String key = StringUtil.substringBeforeLast(question.getQid(),
						"_");
				if ("C".equals(question.getType())) {
					key = question.getQid();
					ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
							.get(key);
					if (jbtn == null) {
						jbtn = new ButtonGroup();
						inerviewcont.radioGroupMap.put(key, jbtn);
						inerviewcont.parentIdQId.put(key, question.getQid());
					}
					String value = option.getValue() == null ? "" : option
							.getValue();
					key = option.getOid();
					JRadioButton radioButton = (JRadioButton) inerviewcont.radioMap
							.get(key);
					if (radioButton == null) {
						radioButton = new JRadioButton(optionTitle);
						info.getOptionid().add(key);
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.radioMap.put(key, radioButton);
						jbtn.add(radioButton);
					}
					panel.add(radioButton);
				} else {
					key = option.getOid();
					String value = option.getValue() == null ? "" : option
							.getValue();

					JCheckBox chin = (JCheckBox) inerviewcont.checkboxMap
							.get(key);
					if (chin == null) {
						chin = new JCheckBox(optionTitle);
						info.getOptionid().add(key);
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.checkboxMap.put(key, chin);
					}
					panel.add(chin);
				}
			}
		}
	}

	private void addPanelOptionD(JPanel panel, Question question,
			InterviewInfo info) {
		if (question.getOptions() != null) {
			String key = question.getQid();
			inerviewcont.qLabel.put(key, question.getLabel());
			for (Option option : question.getOptions()) {
				String optionTitle = option.getDetail() == null ? "" : option
						.getDetail();

				key = option.getOid();
				String value = option.getValue() == null ? "" : option
						.getValue();

				JCheckBox chin = (JCheckBox) inerviewcont.checkboxMap.get(key);
				if (chin == null) {
					chin = new JCheckBox(optionTitle);
					info.getOptionid().add(key);
					inerviewcont.keyValueMap.put(key, value);
					inerviewcont.checkboxMap.put(key, chin);
				}
				panel.add(chin);

			}
		}
	}

	private void addPanelOptionQ(JPanel panel, Question question,
			InterviewInfo info) {
		if (question.getOptions() != null) {
			for (Option option : question.getOptions()) {
				String optionTitle = option.getDetail() == null ? "" : option
						.getDetail();
				String key = question.getQid();
				ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
						.get(key);
				if (jbtn == null) {
					jbtn = new ButtonGroup();
					inerviewcont.radioGroupMap.put(key, jbtn);
					inerviewcont.parentIdQId.put(key, question.getQid());
				}
				String value = option.getValue() == null ? "" : option
						.getValue();
				key = option.getOid();
				JRadioButton radioButton = (JRadioButton) inerviewcont.radioMap
						.get(key);
				if (radioButton == null) {
					radioButton = new JRadioButton(optionTitle);
					info.getOptionid().add(key);
					inerviewcont.keyValueMap.put(key, value);
					inerviewcont.radioMap.put(key, radioButton);
					jbtn.add(radioButton);
				}
				panel.add(radioButton);
			}
		}
	}

	private static int addOptionQ(JPanel panel, Question question,
			InterviewInfo info) {
		if (question.getOptions() != null) {
			String key = question.getQid();
			inerviewcont.qLabel.put(key, question.getLabel());
			for (Option option : question.getOptions()) {
				String optionTitle = option.getDetail() == null ? "" : option
						.getDetail();

				if ("Q".equals(question.getType())) {//
					ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
							.get(key);
					if (jbtn == null) {
						jbtn = new ButtonGroup();
						inerviewcont.radioGroupMap.put(key, jbtn);
						inerviewcont.parentIdQId.put(key, question.getQid());
					}
					String value = option.getValue() == null ? "" : option
							.getValue();
					key = option.getOid();
					JRadioButton radioButton = (JRadioButton) inerviewcont.radioMap
							.get(key);
					if (radioButton == null) {
						radioButton = new JRadioButton(optionTitle);
						info.getOptionid().add(key);

						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.radioMap.put(key, radioButton);
						jbtn.add(radioButton);
					}
					panel.add(radioButton);
				}
			}
		}
		return 0;
	}

	private int addOptionCQ(JPanel panel, Question question, InterviewInfo info) {
		if (question.getOptions() != null) {
			int i = 0;
			for (Option option : question.getOptions()) {
				String optionTitle = option.getDetail() == null ? "" : option
						.getDetail();
				String key = StringUtil.substringBeforeLast(question.getQid(),
						"_");

				if (option.getDisable() == 2)
					continue;

				if ("C".equals(question.getType())) {
					key = question.getQid();
					ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
							.get(key);
					if (jbtn == null) {
						jbtn = new ButtonGroup();
						inerviewcont.radioGroupMap.put(key, jbtn);
						inerviewcont.parentIdQId.put(key, question.getQid());
					}
					String value = option.getValue() == null ? "" : option
							.getValue();
					key = option.getOid();
					JRadioButton radioButton = (JRadioButton) inerviewcont.radioMap
							.get(key);
					if (radioButton == null) {
						radioButton = new JRadioButton(optionTitle);
						radioButton.setActionCommand(option.getValue() + ","
								+ key);
						radioButton.setBackground(getColor(question
								.getOptions().size(), i));
						radioButton.addActionListener(radiobuttonListener);
						if (option.getNote() != null
								&& !"".equals(option.getNote())) {
							radioButton.setToolTipText(option.getNote());
						}
						if (option.getDefaultValue() != null
								&& "true".equals(option.getDefaultValue())) {
							radioButton.setSelected(true);
						}
						info.getOptionid().add(key);
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.radioMap.put(key, radioButton);
						jbtn.add(radioButton);
					}
					panel.add(radioButton);
					i++;
				} else {
					key = option.getOid();
					String value = option.getValue() == null ? "" : option
							.getValue();

					JCheckBox chin = (JCheckBox) inerviewcont.checkboxMap
							.get(key);
					if (chin == null) {
						chin = new JCheckBox(optionTitle);
						info.getOptionid().add(key);
						chin.addActionListener(checkbuttonListener);
						chin.setActionCommand(key);
						if (option.getNote() != null
								&& !"".equals(option.getNote())) {
							chin.setToolTipText(option.getNote());
						}
						inerviewcont.keyValueMap.put(key, value);
						inerviewcont.checkboxMap.put(key, chin);
					}
					panel.add(chin);
				}
			}
		}
		return 0;
	}

	/**
	 * ==========Radio button =============
	 */
	public ActionListener radiobuttonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (getKSelectPid() >= 0) {
				if (ivMainPanel.jqustionpanel.pael2.followupAnswerState == "1") {
					JRadioButton jb = (JRadioButton) e.getSource();
					radiobuttonListenerFillzhi(jb, ivMainPanel.graphPanel,
							getKPeopleId());
					radiobuttonListenerFillzhi(jb, ivMainPanel.ographPanel,
							getKPeopleIdO());
				}
			} else {
				Toast.makeText(ivMainPanel.kmain.frame,
						mxResources.get("selectAPerson"), Style.SUCCESS)
						.display();
			}
		}
	};

	/**
	 * =========Check button=========
	 */
	public ActionListener checkbuttonListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (getKSelectPid() >= 0) {
				if (ivMainPanel.jqustionpanel.pael2.followupAnswerState == "1") {
					JCheckBox jcb = (JCheckBox) e.getSource();
					boolean flag = false;
					String value = jcb.getActionCommand();
					if (jcb.isSelected()) {
						flag = true;
					} else {
					}
					checkbuttonListenerFillzhi(value, flag, getKPeopleId());
					checkbuttonListenerFillzhi(value, flag, getKPeopleIdO());
				}
			} else {
				Toast.makeText(ivMainPanel.kmain.frame,
						mxResources.get("selectAPerson"), Style.SUCCESS)
						.display();
			}
		}
	};

	/**
	 * Check box answer
	 */
	private void checkbtnifHTrue(String opid, int pid) {
		String opidpre = StringUtil.substringBeforeLast(opid, "_");
		KQQuestion question = ivMainPanel.kassService.getPersonQuestion(pid,
				curqid);
		List<KQQuestionField> fields = question.getFields();
		Boolean checkfalg = false;
		for (KQQuestionField field : fields) {
			String opidName = field.getQid();
			if (opidName.contains(opidpre)) {
				JCheckBox chin = (JCheckBox) inerviewcont.checkboxMap
						.get(opidName);
				if (chin != null && chin.isSelected()) {
					checkfalg = true;
					break;
				}

			}
		}
		Boolean allOptfalg = true;
		if (checkfalg) {
			for (KQQuestionField field : fields) {
				String opidName = field.getQid();
				if (!opidName.contains(opidpre)) {
					if (field.getValue().equals("NA")) {
						allOptfalg = false;
						break;
					}
				} else {
					if (field.getValue().equals(Constants.DEFAULT_NA)) {
						field.setValue("false");
					}
				}
			}
			if (allOptfalg) {

			}
		} else {
			for (KQQuestionField field : fields) {
				String opidName = field.getQid();
				if (opidName.contains(opidpre)) {
					field.setValue(Constants.DEFAULT_NA);
				}
			}
			question.setValue("Mark");

		}
	}

	/**
	 * the default answer of the check box
	 * 
	 * @param fqoId
	 * @param flag
	 * @param pid
	 */
	public void checkbuttonListenerFillzhi(String fqoId, boolean flag, int pid) {
		if (pid >= 0) {
			KQQuestionField kfield = ivMainPanel.kassService
					.getPersonQuestionField(pid, curqid, fqoId);
			if (kfield != null) {
				if (flag) {
					kfield.setValue("true");
				} else {
					kfield.setValue("false");
				}
			}

			checkbtnifHTrue(fqoId, pid);
		}
	}

	/**
	 * Set the color of the person icon as the color of the answer option
	 * 
	 * @param jb
	 * @param graphPanel
	 * @param pid
	 */
	public void radiobuttonListenerFillzhi(JRadioButton jb,
			GraphPanel graphPanel, int pid) {
		mxCell mcel = graphPanel.findPersonById(pid);
		if (mcel != null) {
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					pid, curqid);
			if (kqquestion.getValue().equals("Mark")) {
				String value = jb.getActionCommand();
				String[] valueArray = value.split(",");
				String color = ColorUtil.geWebColor(jb.getBackground());
				graphPanel.setColor(mcel, color, color);
				graphPanel.graphComponent.refresh();
				String opidName = StringUtil.substringBeforeLast(valueArray[1],
						"_");
				ivMainPanel.kassService.getPersonQuestionField(pid, curqid,
						opidName).setValue(valueArray[0]);
			}
		}
	}

	/**
	 * Initialize the selected person
	 * 
	 * @param graphPanel
	 * @param value
	 */
	public void initSelectPerson(String value) {
		List<Integer> pids = new ArrayList<Integer>();
		int kpeopleid = getKPeopleId();
		if (kpeopleid == -1)
			return;
		else {
			pids.add(kpeopleid);
			ivMainPanel.graphPanel.initSelectPerson(pids);
		}
	}

	public static String addOptionCQInitValue(String qid) {
		String str = "";
		List<KQQuestionField> fields = inerviewcont.kqquestions.getFields();
		for (KQQuestionField kqquestionField : fields) {
			if (kqquestionField.getQid().equals(qid)) {
				str = kqquestionField.getValue();
				return str;
			}
		}

		return str;
	}

	/**
	 * save
	 */
	public void savetoperson() {
		int kpeopleid = getKPeopleId();
		if (kpeopleid == -1)
			kpeopleid = getKPeopleIdO();
		if (kpeopleid == -1)
			return;

		savetopersonById(kpeopleid);
	}

	public void savetopersonById(int pid) {
		for (Iterator<?> iterato = inerviewcont.questionMap.entrySet()
				.iterator(); iterato.hasNext();) {
			Map.Entry<String, Object> els = (Map.Entry<String, Object>) iterato
					.next();
			String key = els.getKey();
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					pid, StringUtil.substringBeforeLast(key, "_"));
			if (null == kqquestion) {
				kqquestion = getKQuestionbyqid(key);
			}
			if (kqquestion == null)
				continue;

			InterviewInfo info = (InterviewInfo) els.getValue();
			String value = "";
			if (info.getQtype().equals("A")) {
				JTextField field = (JTextField) inerviewcont.textField.get(key);
				if (field != null) {
					value = field.getText();
					System.out.println(value);
					savetopersontextA(value, key);
				}

			} else if (info.getQtype().equals("C")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) inerviewcont.radioMap
							.get(st);
					if (jrb.isSelected()) {
						value = (String) inerviewcont.keyValueMap.get(st);
						System.out.println(value);
						KQQuestionField field = (KQQuestionField) ivMainPanel.kassService
								.getPersonQuestionField(pid, StringUtil
										.substringBeforeLast(key, "_"), key);
						if (null != field) {
							field.setValue(value);
							break;
						}

					}
				}
			} else if (info.getQtype().equals("T")) {
				JTextArea note = (JTextArea) inerviewcont.noteField.get(key);
				if (note != null) {
					value = note.getText();
				}
			} else if (info.getQtype().equals("Q")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) inerviewcont.radioMap
							.get(st);

					if (jrb.isSelected()) {
						value = (String) inerviewcont.keyValueMap.get(st);
						KQQuestionField fd = new KQQuestionField();
						fd.setQid(key);
						Question qus = questionnaireService.searchQuestion(fd
								.getQid());
						if (qus != null) {
							fd.setLabel(qus.getLabel());
						}
						fd.setValue(value);
						// fields.add(createKQQuestionField((String)inerviewcont.qLabel.get(key),value));
						break;
					}
				}
			} else if (info.getQtype().equals("D")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";
				}
			} else {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";

					KQQuestionField field = (KQQuestionField) ivMainPanel.kassService
							.getPersonQuestionField(pid, StringUtil
									.substringBeforeLast(
											inerviewcont.questionsMap.getQid(),
											"_"), st);
					if (null != field) {
						field.setValue(value);
					}
				}
			}
			if (getkqquestionFieldValue(kqquestion)) {
				kqquestion.setValue("OK");
			}
			kqquestion.setValue("OK");
			;
			ivMainPanel.kassService.save();
		}
	}

	public void savetopersontextA(String value, String key) {
		if (value.equals("")) {
			Object[] el = { "No answer", "Not Known" };
			int result = JOptionPane.showOptionDialog(null,
					mxResources.get("questionCompleteNote"),
					mxResources.get("selectOptionTitle"),
					JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
					null, el, el[1]);
			if (result == JOptionPane.YES_OPTION) {
				return;
			}
		}
		KQQuestionField questionfield = getKQuestionFieldbyqid(key);
		if (questionfield != null) {
			questionfield.setValue(value);
		}
	}

	public Boolean getkqquestionFieldValue(KQQuestion kqquestion) {
		Boolean flag = false;
		List<KQQuestionField> fields = kqquestion.getFields();
		for (KQQuestionField field : fields) {
			if (field.getValue().equals("")) {
				return flag;
			}
		}
		flag = true;
		return flag;
	}

	public KQQuestionField createKQQuestionField(String name, String value,
			String label) {
		KQQuestionField fd = new KQQuestionField();
		fd.setQid(name);
		fd.setLabel(label);
		fd.setValue(value);
		return fd;
	}

	public KQQuestion getKQuestionbyqid(String qid) {
		int kpeopleid = getKPeopleId();
		if (kpeopleid == -1)
			return null;
		KPeople people = ivMainPanel.kassService.get().getPeople();
		if (people != null) {
			List<KPPerson> persons = people.getPersons();
			if (persons != null && persons.size() > 0) {
				for (KPPerson person : persons) {
					if (person.getPid() == kpeopleid) {
						List<KQQuestion> questions = person.getQuestions();
						if (questions != null && questions.size() > 0) {
							for (KQQuestion question : questions) {
								if (question.getQid().equals(qid))
									return question;

							}
						}
					}

				}
			}
		}

		return null;
	}

	public KQQuestion getKQuestionbyqidO(String qid) {
		int kpeopleid = getKPeopleIdO();
		if (kpeopleid == -1)
			return null;
		KPeople people = ivMainPanel.kassService.get().getPeople();
		if (people != null) {
			List<KPPerson> persons = people.getPersons();
			if (persons != null && persons.size() > 0) {
				for (KPPerson person : persons) {
					if (person.getPid() == kpeopleid) {
						List<KQQuestion> questions = person.getQuestions();
						if (questions != null && questions.size() > 0) {
							for (KQQuestion question : questions) {
								if (question.getQid().equals(qid))
									return question;

							}
						}
					}

				}
			}
		}

		return null;
	}

	public KQQuestion getKQuestionByQidPeople(Integer pid, String qid) {
		KPeople people = ivMainPanel.kassService.get().getPeople();
		if (people != null) {
			List<KPPerson> persons = people.getPersons();
			if (persons != null && persons.size() > 0) {
				for (KPPerson person : persons) {
					if (person.getPid() == pid) {
						List<KQQuestion> questions = person.getQuestions();
						if (questions != null && questions.size() > 0) {
							for (KQQuestion question : questions) {
								if (question.getQid().equals(qid))
									return question;

							}
						}
					}

				}
			}
		}

		return null;
	}

	public KQQuestionField getKQuestionFieldbyqid(String fid) {
		KQQuestion kqquestion = getKQuestionbyqid(StringUtil
				.substringBeforeLast(fid, "_"));
		if (kqquestion == null) {
			kqquestion = getKQuestionbyqidO(StringUtil.substringBeforeLast(fid,
					"_"));
		}
		KQQuestionField kqquestionfield = null;
		if (kqquestion != null) {
			List<KQQuestionField> fields = kqquestion.getFields();
			for (KQQuestionField field : fields) {
				if (field.getQid().equals(fid)) {
					kqquestionfield = field;
					return kqquestionfield;
				}
			}
		}

		return kqquestionfield;
	}

	public KQQuestionField getKQuestionFieldbyqidO(String fid) {
		KQQuestion kqquestion = getKQuestionbyqidO(StringUtil
				.substringBeforeLast(fid, "_"));
		KQQuestionField kqquestionfield = null;
		List<KQQuestionField> fields = kqquestion.getFields();
		for (KQQuestionField field : fields) {
			if (field.getQid().equals(fid)) {
				kqquestionfield = field;
				return kqquestionfield;
			}
		}

		return kqquestionfield;
	}

	public int getKPeopleId() {
		return getKPeopleIdC((CustomGraph) ivMainPanel.graphPanel.graphComponent
				.getGraph());
	}

	public int getKPeopleIdO() {
		return getKPeopleIdC((CustomGraph) ivMainPanel.ographPanel.graphComponent
				.getGraph());
	}

	public int getKPeopleIdC(CustomGraph graph) {
		int kpeopleid = -1;
		mxCell cell = (mxCell) graph.getSelectionCell();
		if (cell != null && cell.getValue() instanceof GraphPerson) {
			GraphPerson person = (GraphPerson) cell.getValue();
			kpeopleid = person.getId();
		} else if (cell != null && cell.getValue() instanceof GraphUnion) {

		}
		return kpeopleid;
	}

	/**
	 * Get the pid of the selected person
	 * 
	 * @param graph
	 * @return
	 */
	public int getKPeopleIdC(mxCell cell) {
		int kpeopleid = -1;
		if (cell != null && cell.getValue() instanceof GraphPerson) {
			GraphPerson person = (GraphPerson) cell.getValue();
			kpeopleid = person.getId();
		}
		return kpeopleid;
	}

	/**
	 * Clear the value of the person
	 */
	public void clearPersonValue() {
		ClearOporationbyperson();
	}

	public void ClearOporationbyperson() {
		for (Iterator iterato = inerviewcont.questionMap.entrySet().iterator(); iterato
				.hasNext();) {

			Map.Entry<String, Object> els = (Map.Entry<String, Object>) iterato
					.next();
			String key = els.getKey();

			InterviewInfo info = (InterviewInfo) els.getValue();
			String value = "";
			if (info.getQtype().equals("A")) {
				JTextField field = (JTextField) inerviewcont.textField.get(key);
				if (field != null) {
					field.setText("");
				}

			} else if (info.getQtype().equals("C")) {
				ButtonGroup jbtn = (ButtonGroup) inerviewcont.radioGroupMap
						.get(key);
				if (null != jbtn) {
					jbtn.clearSelection();
				}
			} else if (info.getQtype().equals("T")) {
				JTextArea note = (JTextArea) inerviewcont.noteField.get(key);
				if (note != null) {
					value = note.getText();
				}
			} else if (info.getQtype().equals("Q")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) inerviewcont.radioMap
							.get(st);

					if (jrb.isSelected()) {
						value = (String) inerviewcont.keyValueMap.get(st);
						KQQuestionField fd = new KQQuestionField();
						fd.setQid(key);
						Question qus = questionnaireService.searchQuestion(fd
								.getQid());
						if (qus != null) {
							fd.setLabel(qus.getLabel());
						}
						fd.setValue(value);

						break;
					}
				}
			} else if (info.getQtype().equals("D")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";

				}
			} else {
				for (String st : info.getOptionid()) {
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);
					if (null != jrb) {
						jrb.setSelected(false);
					}
				}
			}
		}

	}

	/**
	 * 初始值
	 */
	public void initPersonValue(int pid) {
		if (ifPersonsContainPid(ivMainPanel.jqustionpanel.pael2.getKPersons(),
				pid)) {
			InitOporationbyperson(ivMainPanel.graphPanel, pid);
		}
		if (ifPersonsContainPid(ivMainPanel.jqustionpanel.pael2.getKPersonsO(),
				pid)) {
			InitOporationbyperson(ivMainPanel.ographPanel, pid);
		}
	}

	public Boolean ifPersonsContainPid(List<KPPerson> persons, int pid) {
		Boolean flag = false;
		for (KPPerson person : persons) {
			if (person.getPid().equals(pid)) {
				flag = true;
				break;
			}
		}
		return flag;

	}

	public void initPersonValue_Z(int pid) {
		if (pid > 0) {
			InitOporationbyperson(ivMainPanel.graphPanel, pid);
		}
	}

	/**
	 * Fill the person icon with the color of the question answer option of this
	 * person
	 * 
	 * @param graphPanel
	 * @param pid
	 */
	public void InitOporationbyperson(GraphPanel graphPanel, int pid) {

		for (Iterator iterato = inerviewcont.questionMap.entrySet().iterator(); iterato
				.hasNext();) {

			Map.Entry<String, Object> els = (Map.Entry<String, Object>) iterato
					.next();
			String key = els.getKey();

			KQQuestion kqquestion = getKQuestionbyqid(StringUtil
					.substringBeforeLast(key, "_"));
			if (null == kqquestion) {
				kqquestion = getKQuestionbyqid(key);
			}
			InterviewInfo info = (InterviewInfo) els.getValue();
			String value = "";
			if (info.getQtype().equals("A")) {
				JTextField field = (JTextField) inerviewcont.textField.get(key);
				if (field != null) {
					value = getKQuestionFieldbyqid(key).getValue();
					if (StringUtils.equals(Constants.DEFAULT_NA, value)
							|| StringUtils.equals(Constants.DEFAULT_DNA, value)) {
						value = "";
					}
					field.setText(value);
				}

			} else if (info.getQtype().equals("C")) {
				KQQuestionField field = ivMainPanel.kassService
						.getPersonQuestionField(pid,
								StringUtil.substringBeforeLast(key, "_"), key);
				if (field != null) {
					ButtonGroup btngroup = (ButtonGroup) inerviewcont.radioGroupMap
							.get(key);
					if (btngroup != null) {
						btngroup.clearSelection();
					}

					String pvalue = field.getValue();
					if (!pvalue.equals("")) {
						for (String st : info.getOptionid()) {
							value = (String) inerviewcont.keyValueMap.get(st);
							if (pvalue.equals(value)) {
								JRadioButton jrb = (JRadioButton) inerviewcont.radioMap
										.get(st);
								if (jrb != null) {
									jrb.setSelected(true);
								}
							}
						}
					}
					int num = getQuestionOptionNum();
					String selcolor = ivMainPanel.jqustionpanel.pael2.selectcolor;
					if (pvalue.equals(Constants.DEFAULT_NA)) {
					} else if (pvalue.equals(Constants.DEFAULT_DNA)) {
					} else {
						selcolor = ColorUtil.geWebColor(num,
								Integer.valueOf(pvalue));
						mxCell mcell = graphPanel.findPersonById(pid);
						graphPanel.setColor(mcell, selcolor, selcolor);
						graphPanel.graphComponent.refresh();
					}
				}
			} else if (info.getQtype().equals("T")) {
				JTextArea note = (JTextArea) inerviewcont.noteField.get(key);
				if (note != null) {

					value = note.getText();
				}
			} else if (info.getQtype().equals("Q")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JRadioButton jrb = (JRadioButton) inerviewcont.radioMap
							.get(st);

					if (jrb.isSelected()) {
						value = (String) inerviewcont.keyValueMap.get(st);
						KQQuestionField fd = new KQQuestionField();
						fd.setQid(key);
						Question qus = questionnaireService.searchQuestion(fd
								.getQid());
						if (qus != null) {
							fd.setLabel(qus.getLabel());
						}
						fd.setValue(value);
						break;
					}
				}
			} else if (info.getQtype().equals("D")) {
				for (String st : info.getOptionid()) {
					System.out.println(st);
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);

					if (jrb.isSelected()) {
						value = "true";
					} else
						value = "false";
				}
			} else {
				for (String st : info.getOptionid()) {
					KQQuestionField field = ivMainPanel.kassService
							.getPersonQuestionField(pid,
									StringUtil.substringBeforeLast(key, "_"),
									st);
					JCheckBox jrb = (JCheckBox) inerviewcont.checkboxMap
							.get(st);
					value = field.getValue();
					if (value.equals("true")) {
						jrb.setSelected(true);
					} else {
						jrb.setSelected(false);
					}
				}
			}

		}
	}

	public String getOptionDetailbyOID(String oid, Question question) {
		String str = "";
		for (Option option : question.getOptions()) {
			if (option.getOid().startsWith(oid)) {
				return option.getDetail();
			}

		}

		return str;
	}

	public void dbClickQuestion(mxCell cell) {
		System.out.println("db clicked!!!");
		if (type == 1) {
			setFolloupQuestionPageShow(true);
		} else if (type == 2) {

		} else if (type == 3) {

		}
		ivMainPanel.repaint();
		ivMainPanel.repaint();
	}

	public void dbClickQuestionStartSecondSelectPerson() {
		dbpersonid = getKPeopleId();
		if (dbpersonid == -1) {
			Toast.makeText(ivMainPanel.kmain.frame,
					mxResources.get("selectAPerson"), Style.SUCCESS).display();
			return;
		}
		KPPerson kpperson = ivMainPanel.kassService.getPerson(dbpersonid);
		if (kpperson != null) {
		}
		ivMainPanel.graphPanel.clearLockPerson();
		ivMainPanel.ographPanel.clearLockPerson();
		ivMainPanel.jqustionpanel.pael2.selectPerson.clear();
		ivMainPanel.jqustionpanel.pael2.InitSelectModel_Z();

		setFolloupQuestionPageShow(true);
	}

	public static Color getColor(int size, int index) {
		Color color = ColorUtil.geColor(size, index);
		return color;
	}

	public void SaveSecondSelectPeople() {
		int i = 0;
		KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
				dbpersonid, curqid);
		List<KQQuestionField> fields = new ArrayList<KQQuestionField>();

		for (Integer pid : ivMainPanel.jqustionpanel.pael2.selectPerson) {
			String value = String.valueOf(pid);
			String name = ivMainPanel.questionnaireService.searchQuestion(
					curqid).getQid()
					+ "_" + i;
			String label = kqquestion.getLabel() + "_" + i;
			KQQuestionField kqquestionField = createKQQuestionField(name,
					value, label);
			fields.add(kqquestionField);
			i++;
		}

		if (kqquestion != null) {
			kqquestion.setFields(fields);
			kqquestion.setValue("OK");
		}
		ivMainPanel.jqustionpanel.pael2.secondselectPerson.clear();
	}

	public Boolean selectPersonifMarked(int pid) {
		Boolean flag = false;

		KQQuestion kquestion = getKQuestionByQidPeople(pid, curqid);
		if (kquestion.getValue().equals("Mark")) {
			flag = true;
		} else {
			flag = false;
		}

		return flag;
	}

	public Integer getQuestionOptionNum() {
		Integer num = 0;

		for (FollowupQuestion followupQuestion : question
				.getFollowupQuestions()) {
			if (followupQuestion.getType().equals("C")) {
				num = followupQuestion.getOptions().size();
			}
		}

		return num;
	}

	public void saveMaptoperson() {
		saveMaptopersonCommon(ivMainPanel.graphPanel,
				ivMainPanel.jqustionpanel.pael2.getKPersons());
		saveMaptopersonCommon(ivMainPanel.ographPanel,
				ivMainPanel.jqustionpanel.pael2.getKPersonsO());

	}

	public void saveMaptopersonCommon(GraphPanel graphPanel,
			List<KPPerson> persons) {
		for (KPPerson person : persons) {
			KQQuestion kquestion = this.getKQuestionByQidPeople(
					person.getPid(), curqid);
			if (kquestion.getValue().equals("Mark")) {
				String color = graphPanel.getColorByPerson(person.getPid())
						.toUpperCase();
				System.out.println("person id = " + person.getPid()
						+ " color = " + color);
				int index = ColorUtil.geWebColor(getQuestionOptionNum(), color);
				if (index > 0) {
					kquestion.getFields().get(0)
							.setValue(String.valueOf(index));
				}
				saveMapToPersonQuestionValue(kquestion);
			}
		}
	}

	/**
	 * Check the completed status of this question of a person
	 * 
	 * @param kquestion
	 */
	public void saveMapToPersonQuestionValue(KQQuestion kquestion) {
		List<KQQuestionField> fields = kquestion.getFields();
		for (KQQuestionField field : fields) {
			if (field.getValue().equals(Constants.DEFAULT_NA)) {
				kquestion.setValue(Constants.DEFAULT_NA);
				return;
			}
		}
		kquestion.setValue(Constants.DEFAULT_OK);
	}

	/**
	 * Check the input data type: Int
	 */
	QuestionKeyAdapter intKeylistener = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_INT, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					if (getSelectPerson()) {
						Object obj = evt.getSource();
						JTextField field = ((JTextField) obj);
						String text = field.getText();

						if (StringUtils.isBlank(text)) {
							text = Constants.DEFAULT_NA;
						} else if (field.getParent() instanceof JPanel
								&& StringUtils.isNotBlank(field.getName())) {
							String name = field.getName();

							if (!text.equals("")) {
								TextListenerFillzhi(name, getKPeopleId(), text);
								TextListenerFillzhi(name, getKPeopleIdO(), text);
							}
						}

					} else {
						Toast.makeText(ivMainPanel.kmain.frame,
								mxResources.get("TextFocusalarm"),
								Style.SUCCESS).display();
					}

				}
			});

	/**
	 * Get the selected persons
	 * 
	 * @return
	 */
	private Boolean getSelectPerson() {
		Boolean flag = false;
		if (getKPeopleId() > -1) {
			return true;
		} else if (getKPeopleIdO() > -1) {
			return true;
		}
		return flag;
	}

	/**
	 * Check the input data type: Decimal
	 */
	QuestionKeyAdapter decimalKeylistener = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_DECIMAL, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					if (getKSelectPid() >= 0) {
						Object obj = evt.getSource();
						JTextField field = ((JTextField) obj);
						String text = field.getText();
						if (StringUtils.isBlank(text)) {
							text = Constants.DEFAULT_NA;
						} else if (field.getParent() instanceof JPanel
								&& StringUtils.isNotBlank(field.getName())) {
							String name = field.getName();

							if (!text.equals("")) {
								TextListenerFillzhi(name, getKPeopleId(), text);
								TextListenerFillzhi(name, getKPeopleIdO(), text);
							}
						}
					} else {
						Toast.makeText(ivMainPanel.kmain.frame,
								mxResources.get("selectAPerson"), Style.SUCCESS)
								.display();
					}

				}
			});

	/**
	 * Check the input data type: Percent
	 */
	QuestionKeyAdapter perCentKeylistener = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_PERCENT, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					if (ivMainPanel.jqustionpanel.pael2.followupAnswerState == "1") {
						if (getSelectPerson()) {
							Object obj = evt.getSource();
							JTextField field = ((JTextField) obj);
							String text = field.getText();

							if (StringUtils.isBlank(text)) {
								text = Constants.DEFAULT_NA;
							} else if (field.getParent() instanceof JPanel
									&& StringUtils.isNotBlank(field.getName())) {
								String name = field.getName();

								if (!text.equals("")) {
									TextListenerFillzhi(name, getKPeopleId(),
											text);
									TextListenerFillzhi(name, getKPeopleIdO(),
											text);
								}
							}

						} else {
							Toast.makeText(ivMainPanel.kmain.frame,
									mxResources.get("TextFocusalarm"),
									Style.SUCCESS).display();
						}
					}
				}

			});

	/**
	 * Check the input data type: String
	 */
	QuestionKeyAdapter perCentKeylistenerString = new QuestionKeyAdapter(
			QuestionKeyAdapter.TYPE_STRING, new QuestionKeyReleaseEvent() {
				@Override
				public void released(KeyEvent evt) {
					if (ivMainPanel.jqustionpanel.pael2.followupAnswerState == "1") {
						if (getSelectPerson()) {
							Object obj = evt.getSource();
							JTextField field = ((JTextField) obj);
							String text = field.getText();

							if (StringUtils.isBlank(text)) {
								text = Constants.DEFAULT_NA;
							} else if (field.getParent() instanceof JPanel
									&& StringUtils.isNotBlank(field.getName())) {
								String name = field.getName();

								if (!text.equals("")) {
									TextListenerFillzhi(name, getKPeopleId(),
											text);
									TextListenerFillzhi(name, getKPeopleIdO(),
											text);
								}
							}

						} else {
							Toast.makeText(ivMainPanel.kmain.frame,
									mxResources.get("TextFocusalarm"),
									Style.SUCCESS).display();
						}
					}
				}
			});

	/**
	 * The text box for the question answer
	 * 
	 * @param fqoId
	 * @param flag
	 * @param pid
	 */
	public void TextListenerFillzhi(String fieldId, int pid, String text) {
		if (pid >= 0) {
			KQQuestionField kfield = ivMainPanel.kassService
					.getPersonQuestionField(pid, curqid, fieldId);
			if (kfield != null) {
				kfield.setValue(text);
				ivMainPanel.jqustionpanel.pael2.ifsubsection0071();
			}
		} else {
		}
	}

	/**
	 * Person select again
	 * 
	 * @param pids
	 */
	public void secondSelectPerosns(List<Integer> pids) {
		Integer kpid = getKSelectPid();
		if (kpid >= 0) {
			int i = 0;
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					kpid, curqid);
			List<KQQuestionField> fields = new ArrayList<KQQuestionField>();
			for (Integer pid : pids) {
				String value = String.valueOf(pid);
				String name = ivMainPanel.questionnaireService.searchQuestion(
						curqid).getQid()
						+ "_" + i;
				String label = kqquestion.getLabel() + "_" + i;
				KQQuestionField kqquestionField = createKQQuestionField(name,
						value, label);
				fields.add(kqquestionField);
				i++;
			}

			if (kqquestion != null) {
				kqquestion.setFields(fields);
				kqquestion.setValue("OK");
			}
			ivMainPanel.showPersonAnswers(curqid);
		}

	}

	/**
	 * Add new person to the follow up question
	 * 
	 * @param pid
	 */
	public void addANewPerosnIOIFinish(String pid) {
		Integer kpid = getKSelectPid();
		if (kpid > 0) {
			int i = 0;
			KQQuestion kqquestion = ivMainPanel.kassService.getPersonQuestion(
					kpid, curqid);
			if (kqquestion != null) {
				List<KQQuestionField> fields = kqquestion.getFields();
				for (KQQuestionField field : fields) {
					i++;
				}
				String value = String.valueOf(pid);
				String name = ivMainPanel.questionnaireService.searchQuestion(
						curqid).getQid()
						+ "_" + i;
				String label = kqquestion.getLabel() + "_" + i;
				KQQuestionField kqquestionField = createKQQuestionField(name,
						value, label);
				fields.add(kqquestionField);
				kqquestion.setFields(fields);
			}
			ivMainPanel.showPersonAnswers(curqid);
		} else {
			Toast.makeText(ivMainPanel.kmain.frame,
					mxResources.get("selectAPerson"), Style.SUCCESS).display();
		}
	}

	/**
	 * Get the selected person
	 * 
	 * @return
	 */
	public Integer getKSelectPid() {
		Integer pid = -1;
		pid = getKPeopleId();
		if (pid < 0) {
			pid = getKPeopleIdO();
		}
		return pid;
	}

	/**
	 * Check DCR questions
	 */
	private void checkQuestionDCRSetOK() {
		List<KPPerson> pserons = ivMainPanel.kassService.getKPersons();
		for (KPPerson person : pserons) {
			KQQuestion question = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (question.getValue().equals(Constants.DEFAULT_MARK)) {
				List<KQQuestionField> fields = question.getFields();
				if (fields.size() > 0) {
					question.setValue(Constants.DEFAULT_OK);
				}
			}
		}

	}

	/**
	 * View the answers of this question
	 */
	public void ViewPesonsAnswers() {
		ViewPesonsAnswersbyGraph(ivMainPanel.graphPanel,
				ivMainPanel.jqustionpanel.pael2.getKPersons());
		ViewPesonsAnswersbyGraph(ivMainPanel.ographPanel,
				ivMainPanel.jqustionpanel.pael2.getKPersonsO());

	}

	/**
	 * View the answer of the person when the person icon selected
	 */
	private void ViewPesonsAnswersbyGraph(GraphPanel graphPanel,
			List<KPPerson> persons) {
		for (KPPerson person : persons) {
			KQQuestion question = ivMainPanel.kassService.getPersonQuestion(
					person.getPid(), curqid);
			if (question.getValue().equals(Constants.DEFAULT_OK)) {
				List<KQQuestionField> fields = question.getFields();
				if (fields.size() > 0) {
					KQQuestionField kfield = ivMainPanel.kassService
							.getPersonQuestionField(person.getPid(),
									question.getQid(), fields.get(0).getQid());
					String pvalue = kfield.getValue();

					if (pvalue.length() > 1) {
						String tem;
						for (int j = 1; j < fields.size(); j++) {
							tem = fields.get(j).getValue();
							if (tem.length() == 1) {
								pvalue = tem;
								break;
							}
						}
					}
					if (!pvalue.equals("")) {
						int num = getQuestionOptionNum();
						String selcolor = ivMainPanel.jqustionpanel.pael2.selectcolor;
						if (pvalue.equals(Constants.DEFAULT_NA)) {
						} else if (pvalue.equals(Constants.DEFAULT_DNA)) {
						} else {
							selcolor = ColorUtil.geWebColor(num,
									Integer.valueOf(pvalue));
							mxCell mcell = graphPanel.findPersonById(person
									.getPid());
							graphPanel.setColor(mcell, selcolor, selcolor);
							graphPanel.graphComponent.refresh();
						}
					}

				}
			}
		}
	}

}
