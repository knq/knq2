package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.PageConfig;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of setting the layout of the diagram and the page
 * in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class PageSetupDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 668753894391224205L;
	private KassService kassService;
	private JPanel panel;
	private JLabel pageScaleLabel;
	private JLabel cellWidthLabel;
	private JLabel cellHeightLabel;
	private JLabel orientationLabel;

	private JTextField pageScaleField;
	private JTextField cellWidthField;
	private JTextField cellHeightField;
	private JRadioButton landscapeRB; //
	private JRadioButton portraitRB;
	private ButtonGroup lpBG;//

	private JLabel birthdayLabel;
	private JRadioButton showBirthdayRB; //
	private JRadioButton hideBirthdayRB;
	private ButtonGroup shBG;//

	public PageSetupDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {
		super.initDialog();
		kassService = new KassService();
	}

	@Override
	public JPanel createPanel() {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		panel = new JPanel(bagLayout);

		landscapeRB = new JRadioButton(mxResources.get("page.landscape"));
		portraitRB = new JRadioButton(mxResources.get("page.portrait"));
		lpBG = new ButtonGroup();
		lpBG.add(portraitRB);
		lpBG.add(landscapeRB);

		showBirthdayRB = new JRadioButton(mxResources.get("page.show.birthday"));
		hideBirthdayRB = new JRadioButton(mxResources.get("page.hide.birthday"));
		shBG = new ButtonGroup();
		shBG.add(showBirthdayRB);
		shBG.add(hideBirthdayRB);

		pageScaleLabel = new JLabel(mxResources.get("page.scale") + ":");
		orientationLabel = new JLabel(mxResources.get("page.orientation") + ":");
		cellWidthLabel = new JLabel(mxResources.get("page.cell.width") + ":");
		cellHeightLabel = new JLabel(mxResources.get("page.cell.height") + ":");
		birthdayLabel = new JLabel(mxResources.get("page.birthday") + ":");

		pageScaleField = new JTextField(30);
		cellWidthField = new JTextField(30);
		cellHeightField = new JTextField(30);

		int index = 0;
		UIHelper.addToBagpanel(panel, Color.gray, orientationLabel, c, 0,
				index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, landscapeRB, c, 1, index, 1,
				1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, portraitRB, c, 2, index, 1,
				1, GridBagConstraints.WEST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, pageScaleLabel, c, 0, index,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, pageScaleField, c, 1, index,
				2, 1, GridBagConstraints.WEST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, cellWidthLabel, c, 0, index,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, cellWidthField, c, 1, index,
				2, 1, GridBagConstraints.WEST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, cellHeightLabel, c, 0, index,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, cellHeightField, c, 1, index,
				2, 1, GridBagConstraints.WEST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, birthdayLabel, c, 0, index,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, showBirthdayRB, c, 1, index,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, hideBirthdayRB, c, 2, index,
				1, 1, GridBagConstraints.WEST);

		return panel;
	}

	@Override
	public boolean beforeValid() {
		if (!landscapeRB.isSelected() && !portraitRB.isSelected()) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.orientation.notnull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (StringUtils.isBlank(pageScaleField.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.scale.notnull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (!NumberUtils.isNumber(pageScaleField.getText())) {
			JOptionPane
					.showMessageDialog(container,
							"<HTML>" + mxResources.get("page.scale.number")
									+ "</HTML>", "Warning",
							JOptionPane.INFORMATION_MESSAGE,
							UIHelper.getImage("warning.png"));
			return false;
		}
		if (StringUtils.isBlank(cellWidthField.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.cell.width.notnull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (!NumberUtils.isNumber(cellWidthField.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.cell.width.number")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (StringUtils.isBlank(cellHeightField.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.cell.height.notnull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (!NumberUtils.isNumber(cellHeightField.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("page.cell.height.number")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}

		return true;
	}

	@Override
	public MessageModel submit() {
		PageConfig pageConfig = kassService.get().getPageConfig();

		if (landscapeRB.isSelected()) {
			pageConfig.setPageFormat(1);
		}
		if (portraitRB.isSelected()) {
			pageConfig.setPageFormat(0);
		}
		if (showBirthdayRB.isSelected()) {
			pageConfig.setShowBirthday(1);
		}
		if (hideBirthdayRB.isSelected()) {
			pageConfig.setShowBirthday(0);
		}
		pageConfig.setCellHeight(Integer.parseInt(cellHeightField.getText()));
		pageConfig.setCellWith(Integer.parseInt(cellWidthField.getText()));
		pageConfig.setPageScale(Integer.parseInt(pageScaleField.getText()));

		kassService.save();
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {
		// Reset the page
		PageConfig pageConfig = kassService.get().getPageConfig();
		if (this.km != null && this.km.ivMainPanel != null) {
			this.km.ivMainPanel.setOrientation(pageConfig.getPageFormat());
			this.km.ivMainPanel.repaint();
		}

	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		PageConfig pageConfig = kassService.get().getPageConfig();
		if (pageConfig.getPageFormat() == 0) {
			portraitRB.setSelected(true);
		} else {
			landscapeRB.setSelected(true);
		}
		if (pageConfig.getShowBirthday() == 0) {
			hideBirthdayRB.setSelected(true);
		} else {
			showBirthdayRB.setSelected(true);
		}

		cellHeightField.setText("" + pageConfig.getCellHeight());
		cellWidthField.setText("" + pageConfig.getCellWith());
		pageScaleField.setText("" + pageConfig.getPageScale());
	}
}
