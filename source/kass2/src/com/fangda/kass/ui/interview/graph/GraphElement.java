package com.fangda.kass.ui.interview.graph;

/**
 * The Class for the element of the graph.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphElement implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6749968654234218818L;
	private int id;
	/**
	 * the color of the graph
	 */
	private String color;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
