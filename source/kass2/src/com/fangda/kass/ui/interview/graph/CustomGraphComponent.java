package com.fangda.kass.ui.interview.graph;

import java.awt.Color;
import java.awt.Point;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxGraphicsCanvas2D;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.shape.mxStencil;
import com.mxgraph.shape.mxStencilRegistry;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;

/**
 * The Class for the graph component.
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomGraphComponent extends mxGraphComponent {

	private static final long serialVersionUID = 6197811457338598239L;

	public CustomGraphComponent(mxGraph graph) {
		super(graph);
		setPageVisible(true);
		setGridVisible(true);
		this.getConnectionHandler().setEnabled(false);
		getGraph().setKeepEdgesInBackground(true);
		getGraph().setAllowDanglingEdges(false);
		getGraph().setResetEdgesOnConnect(false);
		getGraph().setResetEdgesOnMove(false);
		getGraph().setResetEdgesOnResize(false);

		setToolTips(true);
		// getConnectionHandler().setCreateTarget(true);
		// Loads the defalt stylesheet from an external file
		mxCodec codec = new mxCodec();
		Document doc = mxUtils.loadDocument(CustomGraphComponent.class
				.getResource("/resources/default-style.xml").toString());
		codec.decode(doc.getDocumentElement(), graph.getStylesheet());
		loadNewShape();// load the new coordinates.
		// Sets the background to white
		getViewport().setOpaque(true);
		getViewport().setBackground(Color.WHITE);

	}

	public void loadNewShape() {
		try {
			// String filename =
			// CustomGraphComponent.class.getResource("/resources/shapes.xml").getPath().replaceFirst(".*?/",
			// "").replace("%20", " ");
			Document doc = mxUtils.loadDocument(CustomGraphComponent.class
					.getResource("/resources/shapes.xml").toString());
			Element shapes = (Element) doc.getDocumentElement();
			NodeList list = shapes.getElementsByTagName("shape");

			for (int i = 0; i < list.getLength(); i++) {
				Element shape = (Element) list.item(i);
				mxStencilRegistry.addStencil(shape.getAttribute("name"),
						new mxStencil(shape) {
							protected mxGraphicsCanvas2D createCanvas(
									final mxGraphics2DCanvas gc) {
								// Redirects image loading to graphics canvas
								return new mxGraphicsCanvas2D(gc.getGraphics());
							}
						});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object[] importCells(Object[] cells, double dx, double dy,
			Object target, Point location) {
		if (target == null && cells.length == 1 && location != null) {
			target = getCellAt(location.x, location.y);

			if (target instanceof mxICell && cells[0] instanceof mxICell) {
				mxICell targetCell = (mxICell) target;
				mxICell dropCell = (mxICell) cells[0];

				if (targetCell.isVertex() == dropCell.isVertex()
						|| targetCell.isEdge() == dropCell.isEdge()) {
					mxIGraphModel model = graph.getModel();
					model.setStyle(target, model.getStyle(cells[0]));
					graph.setSelectionCell(target);

					return null;
				}
			}
		}
		return super.importCells(cells, dx, dy, target, location);
	}
}
