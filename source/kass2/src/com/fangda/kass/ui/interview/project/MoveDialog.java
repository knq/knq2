package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of moving the diagram in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class MoveDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5828715730235392723L;
	private KassService kassService;
	private JPanel panel;

	private JLabel orientationLabel;
	private JRadioButton nkkRB; //
	private JRadioButton otherRB;
	private ButtonGroup lpBG;//

	private JButton upBtn;
	private JButton downBtn;
	private JButton leftBtn;
	private JButton rightBtn;

	public MoveDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {
		super.initDialog();
		kassService = new KassService();
	}

	@Override
	public JPanel createPanel() {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		panel = new JPanel(bagLayout);

		orientationLabel = new JLabel(mxResources.get("window") + ":");
		nkkRB = new JRadioButton(mxResources.get("print.nkk"));
		otherRB = new JRadioButton(mxResources.get("print.other"));
		lpBG = new ButtonGroup();
		lpBG.add(nkkRB);
		lpBG.add(otherRB);

		upBtn = new JButton(mxResources.get("move.up"));
		downBtn = new JButton(mxResources.get("move.down"));
		leftBtn = new JButton(mxResources.get("move.left"));
		rightBtn = new JButton(mxResources.get("move.right"));

		int index = 0;
		UIHelper.addToBagpanel(panel, Color.gray, orientationLabel, c, 0,
				index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, nkkRB, c, 1, index, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, otherRB, c, 2, index, 1, 1,
				GridBagConstraints.WEST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, upBtn, c, 1, index, 1, 1,
				GridBagConstraints.EAST);

		index++;
		UIHelper.addToBagpanel(panel, Color.gray, leftBtn, c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, rightBtn, c, 2, index, 1, 1,
				GridBagConstraints.EAST);
		index++;
		UIHelper.addToBagpanel(panel, Color.gray, downBtn, c, 1, index, 1, 1,
				GridBagConstraints.EAST);

		return panel;
	}

	public void afterShow() {
		nkkRB.setSelected(true);
		setSureBtnVisible(false);
		upBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nkkRB.isSelected()) {
					IvMainPanel.IVPANEL.graphPanel.moveAll(1);
				} else {
					IvMainPanel.IVPANEL.ographPanel.moveAll(1);
				}
			}
		});
		downBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nkkRB.isSelected()) {
					IvMainPanel.IVPANEL.graphPanel.moveAll(3);
				} else {
					IvMainPanel.IVPANEL.ographPanel.moveAll(3);
				}
			}
		});
		leftBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nkkRB.isSelected()) {
					IvMainPanel.IVPANEL.graphPanel.moveAll(4);
				} else {
					IvMainPanel.IVPANEL.ographPanel.moveAll(4);
				}
			}
		});
		rightBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (nkkRB.isSelected()) {
					IvMainPanel.IVPANEL.graphPanel.moveAll(2);
				} else {
					IvMainPanel.IVPANEL.ographPanel.moveAll(2);
				}
			}
		});
	}
}
