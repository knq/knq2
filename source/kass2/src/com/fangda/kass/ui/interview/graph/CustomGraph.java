package com.fangda.kass.ui.interview.graph;

import java.awt.Point;
import java.text.NumberFormat;

import org.apache.commons.lang.StringUtils;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;

/**
 * The Class for the graph.
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomGraph extends mxGraph {

	private static final long serialVersionUID = 5046871895662746959L;

	public static final NumberFormat numberFormat = NumberFormat.getInstance();

	protected Object edgeTemplate;
	public GraphPanel graphPanel;

	public CustomGraph(GraphPanel graphPanel) {
		setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
		this.graphPanel = graphPanel;
	}

	public void setEdgeTemplate(Object template) {
		edgeTemplate = template;
	}

	public String getToolTipForCell(Object cell) {
		if (cell instanceof mxCell
				&& ((mxCell) cell).getValue() instanceof GraphPerson) {
			GraphPerson gp = (GraphPerson) ((mxCell) cell).getValue();
			if (StringUtils.isNotBlank(gp.getTip())) {
				return gp.getTip();
			} else {
				return gp.getName();
			}
		}
		return null;
	}

	public Object createEdge(Object parent, String id, Object value,
			Object source, Object target, String style) {
		// if (edgeTemplate != null) {
		// mxCell edge = (mxCell) cloneCells(new Object[] { edgeTemplate })[0];
		// edge.setId(id);
		//
		// return edge;
		// }
		return super.createEdge(parent, id, value, source, target, style);
	}

	public Point transXy(Point xy) {

		double scale = this.getView().getScale();
		int x = (int) (xy.getX() / scale);
		int y = (int) (xy.getY() / scale);
		return new Point(x, y);
	}
}
