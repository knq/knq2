package com.fangda.kass.ui.interview.graph;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.fangda.kass.model.interview.KPPCoords;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.support.InitUtil;
import com.fangda.kass.ui.interview.support.QuestionMap;
import com.fangda.kass.util.ColorUtil;
import com.fangda.kass.util.Routing;
import com.fangda.kass.util.Utils;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.canvas.mxSvgCanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxCellRenderer.CanvasFactory;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUndoManager;
import com.mxgraph.util.mxUndoableEdit;
import com.mxgraph.util.mxUndoableEdit.mxUndoableChange;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;

/**
 * The Class for the stroke of the graph 1: Change the color of the graph when
 * click it 2: Right Click of the graph (the union and person are different ) 3:
 * Line Type 4: New a union and person 5: Setting the color change
 * 
 * @author Fangfang Zhao, Jianguo Zhou, Jing Kong
 * 
 */
public class GraphPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2533953215003826212L;

	public int cellSize = 40;

	public IvMainPanel ivMainPanel;
	public mxGraphComponent graphComponent;
	public int type = 1;// 1=graph, 2=ograph
	public mxUndoManager undoManager;
	public mxKeyboardHandler keyboardHandler;
	private GraphListener graphListener;
	public boolean unioning = false;
	public mxCell unioningCell;
	public int unioningType = 0;// 0=partner, 1=offspring
	public KassService kService;
	public QuestionnaireService qService;

	public String selectColor = "";
	public String selectBorderColor = "#5d65df";
	public String selectQid;
	private List<mxCell> lockCells = new ArrayList<mxCell>();// 锁定的cell
	private String lockColor = "#CCCCCC";

	public int mode = 1;// 1=Normal mode, 2=Select mode
	private int disable = DISABLE_NOCLICK;
	public static final int DISABLE_NEW = 1;
	public static final int DISABLE_CLICK = 3;
	public static final int DISABLE_NOCLICK = 5;

	public GraphPanel(IvMainPanel ivMainPanel, int type) {
		this.ivMainPanel = ivMainPanel;
		this.type = type;
		graphComponent = new CustomGraphComponent(new CustomGraph(this));
		this.setLayout(new BorderLayout());
		// this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		if (type == 1) {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("NKK window"),
					BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		} else {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Other window"),
					BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		}

		this.add(graphComponent, BorderLayout.CENTER);
		this.setVisible(true);
		kService = new KassService();
		qService = new QuestionnaireService();

		undoManager = createUndoManager();
		graphComponent.getGraph().getModel()
				.addListener(mxEvent.UNDO, undoRecordHandler);
		graphComponent.getGraph().getView()
				.addListener(mxEvent.UNDO, undoRecordHandler);
		undoManager.addListener(mxEvent.UNDO, undoRedoHandler);
		undoManager.addListener(mxEvent.REDO, undoRedoHandler);
		keyboardHandler = new CustomGraphKeyboardHandler(graphComponent,
				this.ivMainPanel);
		graphComponent.getGraphControl().addMouseListener(mouseAdapter);
		graphComponent.getGraph().addListener(mxEvent.MOVE_CELLS,
				moveEventListener);
		graphComponent.addMouseWheelListener(wheelTracker);
		graphComponent.addKeyListener(key);
		graphComponent.getGraph().setCellsEditable(false);
		graphComponent.getGraph().setCellsResizable(false);
		graphComponent.getGraph().setDropEnabled(false);
		graphComponent.setToolTips(true);
		graphComponent.setPageScale(10);
		graphComponent.zoomTo(0.5, true);

		// 绘图的边框
		mxSwingConstants.VERTEX_SELECTION_STROKE = new BasicStroke(3,
				BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10.0f,
				new float[] { 3, 3 }, 0.0f);
		;
	}

	KeyListener key = new KeyListener() {
		public void keyTyped(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
			// System.out.println("arg0.getModifiers()=" + e.getModifiers());
			// System.out.println("arg0.getKeyCode()=" + e.getKeyCode());

			// 缩放键盘监听
			if (e.getModifiers() == 2 && e.getKeyCode() == 61) {
				// System.out.println("CTRL+'+'......");
				graphComponent.zoomIn();
			} else if (e.getModifiers() == 2 && e.getKeyCode() == 45) {
				// System.out.println("CTRL+'-'......");
				graphComponent.zoomOut();
			}
		}

		public void keyPressed(KeyEvent e) {
		}

	};
	public mxIEventListener undoRecordHandler = new mxIEventListener() {
		public void invoke(Object source, mxEventObject evt) {
			undoManager.undoableEditHappened((mxUndoableEdit) evt
					.getProperty("edit"));
			// System.out.println("undoRecordHandler");
		}
	};

	public mxIEventListener undoRedoHandler = new mxIEventListener() {
		public void invoke(Object source, mxEventObject evt) {
			List<mxUndoableChange> changes = ((mxUndoableEdit) evt
					.getProperty("edit")).getChanges();
			graphComponent.getGraph().setSelectionCells(
					graphComponent.getGraph().getSelectionCellsForChanges(
							changes));
			undoChangeSave(changes);
		}
	};

	public mxUndoManager createUndoManager() {
		return new mxUndoManager();
	}

	public mxUndoManager getUndoManager() {
		return undoManager;
	}

	/**
	 * 鼠标点击事件
	 */
	MouseAdapter mouseAdapter = new MouseAdapter() {
		int num = 0;

		public void mousePressed(MouseEvent e) {
			if (!Utils.isMac())
				return;
			ivMainPanel.opGraphPanel = type;
			if (disable == DISABLE_NOCLICK) {
				// view mode click event
				mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(),
						e.getY());
				if (cell != null && !cell.isEdge()) {
					graphListener.clickOnViewModel(GraphPanel.this, cell);
				}
				return;
			}
			mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(), e.getY());
			if (cell != null && cell.isEdge()) {
				graphComponent.getGraph().removeSelectionCell(cell);
				graphListener.afterClickEmpty(GraphPanel.this);
				return;
			}
			if (cell == null) {
				graphListener.afterClickEmpty(GraphPanel.this);
			}
			if (isLocked(cell)) {// lock cell
				return;
			}

			if (e.isPopupTrigger()) {
				if (disable == DISABLE_CLICK) {
					return;
				}
				showGraphPopupMenu(e);
			}
			if (cell == null) {
				return;
			}

			if (e.getClickCount() == 1) {
				if (cell != null) {
					if (unioning && unioningCell != null) {
						singleClickAddEdge(cell);
						graphListener.afterSingleClickCell(GraphPanel.this,
								cell);
					} else {
						if (mode == 2) {// cell.getStyle().contains(selectColor)
							if (!"".equals(selectColor)
									&& !cellColorIfWhite(cell)) {
								Object[] el = { "Yes", "No" };
								int result = JOptionPane
										.showOptionDialog(
												null,
												"Are you sure you want"
														+ " to remove this question for which an answer has been recorded?",
												"Select Option",
												JOptionPane.DEFAULT_OPTION,
												JOptionPane.WARNING_MESSAGE,
												null, el, el[1]);

								if (result == JOptionPane.YES_OPTION) {
									setDefaultColor(cell);
									graphListener.afterUnSelectCell(
											GraphPanel.this, cell);
									graphComponent.getGraph().refresh();
								} else if (result == JOptionPane.NO_OPTION) {

								}
							} else {
								changeColor(cell);
								graphListener.afterSelectCell(GraphPanel.this,
										cell);
							}
						} else {
							graphListener.afterSingleClickCell(GraphPanel.this,
									cell);
						}
					}
				}
			} else if (e.getClickCount() == 2) {// 双击事件
				num = num + 1;
				if (num == 1) {
					if (cell != null) {
						graphListener.afterDbClickCell(GraphPanel.this, cell);
					}
				}
				if (num == 2) {
					num = 0;
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (Utils.isMac())
				return;
			ivMainPanel.opGraphPanel = type;
			if (disable == DISABLE_NOCLICK) {
				// view mode click event
				mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(),
						e.getY());
				if (cell != null && !cell.isEdge()) {
					graphListener.clickOnViewModel(GraphPanel.this, cell);
				}
				return;
			}
			mxCell cell = (mxCell) graphComponent.getCellAt(e.getX(), e.getY());
			if (cell != null && cell.isEdge()) {
				graphComponent.getGraph().removeSelectionCell(cell);
				graphListener.afterClickEmpty(GraphPanel.this);
				return;
			}
			if (cell == null) {
				graphListener.afterClickEmpty(GraphPanel.this);
			}
			if (isLocked(cell)) {// lock cell
				return;
			}

			if (e.isPopupTrigger()) {
				if (disable == DISABLE_CLICK) {
					return;
				}
				showGraphPopupMenu(e);
			}
			if (cell == null) {
				return;
			}

			if (e.getClickCount() == 1) {
				if (cell != null) {
					if (unioning && unioningCell != null) {
						singleClickAddEdge(cell);
						graphListener.afterSingleClickCell(GraphPanel.this,
								cell);
					} else {
						if (mode == 2) {// cell.getStyle().contains(selectColor)
							if (!"".equals(selectColor)
									&& !cellColorIfWhite(cell)) {
								Object[] el = { "Yes", "No" };
								int result = JOptionPane
										.showOptionDialog(
												null,
												"Are you sure you want"
														+ " to remove this question for which an answer has been recorded?",
												"Select Option",
												JOptionPane.DEFAULT_OPTION,
												JOptionPane.WARNING_MESSAGE,
												null, el, el[1]);

								if (result == JOptionPane.YES_OPTION) {
									setDefaultColor(cell);
									graphListener.afterUnSelectCell(
											GraphPanel.this, cell);
									graphComponent.getGraph().refresh();
								} else if (result == JOptionPane.NO_OPTION) {

								}
							} else {
								changeColor(cell);
								graphListener.afterSelectCell(GraphPanel.this,
										cell);
							}
						} else {
							graphListener.afterSingleClickCell(GraphPanel.this,
									cell);
						}
					}
				}
			} else if (e.getClickCount() == 2) {
				num = num + 1;
				if (num == 1) {
					if (cell != null) {
						graphListener.afterDbClickCell(GraphPanel.this, cell);
					}
				}
				if (num == 2) {
					num = 0;
				}
			}
		}
	};

	MouseWheelListener wheelTracker = new MouseWheelListener() {
		public void mouseWheelMoved(MouseWheelEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mouseWheelMoved1(e);
			}
		}
	};

	protected void mouseLocationChanged(MouseEvent e) {
		ivMainPanel.status(e.getX() + ", " + e.getY());
	}

	protected void mouseWheelMoved1(MouseWheelEvent e) {
		if (e.getWheelRotation() < 0) {
			graphComponent.zoomIn();
		} else {
			graphComponent.zoomOut();
		}
		if (ivMainPanel != null) {
			ivMainPanel.status(mxResources.get("scale")
					+ ": "
					+ (int) (100 * graphComponent.getGraph().getView()
							.getScale()) + "%");
		}
	}

	public void zoomToFit() {
		graphComponent.zoomTo(0.05, true);
		new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				zoomToCenter();
			}
		}.start();
	}

	public void zoomIn() {
		graphComponent.zoomIn();
	}

	public mxPoint getCenter() {
		mxPoint translate = graphComponent.getGraph().getView().getTranslate();
		double scale = graphComponent.getGraph().getView().getScale();
		int x0 = (int) Math.round(translate.getX() * scale) - 1;
		int y0 = (int) Math.round(translate.getY() * scale) - 1;

		int x = (int) ((graphComponent.getGraphControl().getWidth() / 2 - x0) / scale);
		int y = (int) ((graphComponent.getGraphControl().getHeight() / 2 - y0) / scale);
		return new mxPoint(x, y);
	}

	public void zoomOut() {
		graphComponent.zoomOut();
	}

	public void zoomToCenter() {
		graphComponent.scrollToCenter(false);
		graphComponent.scrollToCenter(true);
	}

	public void zoomToCell(final mxCell mxCell) {
		graphComponent.zoomTo(0.5, true);
		if (mxCell != null) {
			new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					graphComponent.scrollCellRectToVisible(mxCell, true);
				}
			}.start();
		} else {
			new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					zoomToCenter();
				}
			}.start();
		}
	}

	public void zoomToEge() {
		mxCell mxCell = ivMainPanel.graphPanel.findPersonById(0);
		ivMainPanel.graphPanel.zoomToCell(mxCell);
	}

	public void zoomToFirst() {
		mxCell root = (mxCell) ivMainPanel.ographPanel.graphComponent
				.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		mxCell mxCell = null;
		if (c.getChildCount() > 0) {
			mxCell = (mxCell) c.getChildAt(0);
		}
		ivMainPanel.ographPanel.zoomToCell(mxCell);
	}

	public void showGraphPopupMenu(MouseEvent e) {
		Object cell = graphComponent.getCellAt(e.getX(), e.getY());
		if (cell == null) {
			Point pt = SwingUtilities.convertPoint(e.getComponent(),
					e.getPoint(), graphComponent);
			CustomGraph graph = (CustomGraph) graphComponent.getGraph();
			mxPoint translate = graph.getView().getTranslate();
			double scale = graph.getView().getScale();
			int x0 = (int) Math.round(translate.getX() * scale) - 1;
			int y0 = (int) Math.round(translate.getY() * scale) - 1;
			int x = (int) ((e.getX() - x0) / scale);
			int y = (int) ((e.getY() - y0) / scale);
			GraphPopupMenu menu = new GraphPopupMenu(ivMainPanel, null, type,
					0, x, y);
			menu.show(graphComponent, pt.x, pt.y);
			e.consume();
		} else {
			mxCell mc = (mxCell) cell;
			GraphElement graphElement = (GraphElement) mc.getValue();
			Point pt = SwingUtilities.convertPoint(e.getComponent(),
					e.getPoint(), graphComponent);
			CustomGraph graph = (CustomGraph) graphComponent.getGraph();
			Point p = graph.transXy(pt);
			if (graphElement instanceof GraphUnion) {
				GraphPopupMenu menu = new GraphPopupMenu(ivMainPanel, mc, type,
						2, p.x, p.y);
				menu.show(graphComponent, pt.x, pt.y);
				e.consume();
			} else if (graphElement instanceof GraphPerson) {
				// if >= stopRUle, do not show menu
				if (type == 1) {
					int rcount = this.getRoutingCount(graphElement.getId());
					StopRule sr = qService.getStopRule();
					int step = sr.getStep();
					if (rcount >= step) {// 相等
						return;
					}
				}
				GraphPopupMenu menu = new GraphPopupMenu(ivMainPanel, mc, type,
						1, p.x, p.y);
				menu.show(graphComponent, pt.x, pt.y);
				e.consume();
			}
		}
	}

	// ===========Draw a Person=================
	/**
	 * Add person
	 * 
	 * @param x
	 * @param y
	 * @param sex
	 *            0 = male, 1 = female
	 */
	public mxCell addNewPerson(int id, double x, double y, int sex) {
		return addNewPerson(id, x, y, sex, true);
	}

	public mxCell addNewPerson(int id, double x, double y, int sex,
			boolean needAddListener) {
		GraphPerson person = new GraphPerson();
		person.setId(id);
		person.setSex(sex);
		if (type == 1) {// graph
			person.setIsOther(0);
		} else {// ograph
			person.setIsOther(1);
		}

		mxCell cell = null;
		if (person.getId() == 0) {
			if (sex == 0) {
				cell = new mxCell(person, new mxGeometry(x, y, cellSize + 10,
						cellSize + 10), "maleE");
			} else {
				cell = new mxCell(person, new mxGeometry(x, y, cellSize + 10,
						cellSize + 10), "femaleE");
			}
		} else {
			if (sex == 0) {
				cell = new mxCell(person, new mxGeometry(x, y, cellSize,
						cellSize), "maleO");
			} else {
				cell = new mxCell(person, new mxGeometry(x, y, cellSize,
						cellSize), "femaleO");
			}
		}
		cell.setVertex(true);
		graphComponent.getGraph().addCell(cell);
		if (graphListener != null && needAddListener) {
			graphListener.afterAddedCell(GraphPanel.this, cell);
		}
		return cell;
	}

	/**
	 * Draw person based a KPPerson
	 * 
	 * @param person
	 */
	public mxCell drawPerson(KPPerson person) {
		mxCell cell = null;
		double x = 0, y = 0;
		if (person.getCoords() != null) {
			x = person.getCoords().getX();
			y = person.getCoords().getY();
		}
		String personName = kService.getPersonName(person.getPid());
		String birthYear = kService.getPersonBirthYear(person.getPid());
		if (person.getSex() == 0) {
			if (person.getPid() == 0) {
				if (isLiving(person) == 2) {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 0, 2);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize + 10,
							cellSize + 10), "maleE");
				} else {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 0, 1);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize + 10,
							cellSize + 10), "maleDeathE");
				}
			} else {
				if (isLiving(person) == 2) {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 0, 2);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize,
							cellSize), "maleO");
				} else {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 0, 1);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize,
							cellSize), "maleDeathO");
				}
			}
		} else {
			if (person.getPid() == 0) {
				if (isLiving(person) == 2) {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 1, 2);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize + 10,
							cellSize + 10), "femaleE");
				} else {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 1, 1);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize + 10,
							cellSize + 10), "femaleDeathE");
				}
			} else {
				if (isLiving(person) == 2) {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 1, 2);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize,
							cellSize), "femaleO");
				} else {
					GraphPerson gp = new GraphPerson(person.getPid(),
							personName, 1, 1);
					gp.setIsOther(person.getIsOther());
					gp.setKpperson(person.clone());
					gp.setBirthDay(birthYear);
					cell = new mxCell(gp, new mxGeometry(x, y, cellSize,
							cellSize), "femaleDeathO");
				}
			}
		}
		cell.setVertex(true);

		graphComponent.getGraph().addCell(cell);
		// Check the step range based the stop rule
		if (type == 1) {
			int count = getRoutingCount(person.getPid());
			StopRule sr = qService.getStopRule();
			int step = sr.getStep();
			if (count >= step) {
				this.setColor(cell, ColorUtil.getStopRuleColor());
				this.graphComponent.refresh();
			}
		}
		return cell;
	}

	public void changeColorByStopRule() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					// Check the step range based the stop rule
					int count = getRoutingCount(gp.getId());
					StopRule sr = qService.getStopRule();
					int step = sr.getStep();
					if (count >= step) {
						this.setColor(child, ColorUtil.getStopRuleColor());
						this.graphComponent.refresh();
					}
				}
			}
		}
	}

	public String getPersonStyle(GraphPerson person) {
		if (person.getSex() == 0) {
			if (person.getId() == 0) {
				if (person.getIsDeath() == 2) {
					return "maleE";
				} else {
					return "maleDeathE";
				}
			} else {
				if (person.getIsDeath() == 2) {
					return "maleO";
				} else {
					return "maleDeathO";
				}
			}
		} else {
			if (person.getId() == 0) {
				if (person.getIsDeath() == 2) {
					return "femaleE";
				} else {
					return "femaleDeathE";
				}
			} else {
				if (person.getIsDeath() == 2) {
					return "femaleO";
				} else {
					return "femaleDeathO";
				}
			}
		}
	}

	/**
	 * get isdeath of person
	 * 
	 * @param person
	 * @return
	 */
	public int isLiving(KPPerson person) {
		KQQuestionField field = kService.getPersonField(person.getPid(),
				"cbIsDeceased");
		if ("false".equals(field.getValue())) {
			return 2;
		} else if ("true".equals(field.getValue())) {
			return 1;
		} else {
			return 2;
		}

	}

	public mxCell findPersonById(int id) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson one = (GraphPerson) child.getValue();
					if (id == one.getId()) {
						return child;
					}
				}
			}
		}
		return null;
	}

	// ============Draw a Union============
	/**
	 * Add a new union
	 * 
	 * @param x
	 * @param y
	 * @param sex
	 */
	public mxCell addNewUnion(int id, double x, double y) {
		return this.addNewUnion(id, x, y, true);
	}

	public mxCell addNewUnion(int id, double x, double y,
			boolean needAddListener) {
		GraphUnion union = new GraphUnion();
		union.setId(id);
		if (type == 1) {// graph
			union.setIsOther(0);
		} else {// ograph
			union.setIsOther(1);
		}
		mxCell cell = new mxCell(union,
				new mxGeometry(x, y, cellSize, cellSize), "unionE");
		cell.setVertex(true);
		graphComponent.getGraph().addCell(cell);
		if (needAddListener) {
			if (graphListener != null) {
				graphListener.afterAddedCell(GraphPanel.this, cell);
			}
		}
		return cell;
	}

	public mxCell drawOnlyUnion(KUUnion union) {
		double x = 0, y = 0;
		if (union.getCoords() != null) {
			x = union.getCoords().getX();
			y = union.getCoords().getY();
		}
		GraphUnion u = new GraphUnion();
		u.setId(union.getId());
		if (type == 1) {// graph
			u.setIsOther(0);
		} else {// ograph
			u.setIsOther(1);
		}
		mxCell cell = new mxCell(u, new mxGeometry(x, y, cellSize, cellSize),
				"unionE");
		cell.setVertex(true);
		graphComponent.getGraph().addCell(cell);
		return cell;
	}

	public void drawUnion(KUUnion union) {
		double x = 0, y = 0;
		if (union.getCoords() != null) {
			x = union.getCoords().getX();
			y = union.getCoords().getY();
		}
		GraphUnion u = new GraphUnion();
		u.setId(union.getId());
		u.setKuunion(union.clone());
		u.setIsOther(union.getIsOther());
		mxCell cell = new mxCell(u, new mxGeometry(x, y, cellSize, cellSize),
				"unionE");
		cell.setVertex(true);
		graphComponent.getGraph().addCell(cell);
		Object parent = graphComponent.getGraph().getDefaultParent();
		if (union.getMembers() != null && union.getMembers().size() > 0) {
			for (int i = 0; i < union.getMembers().size(); i++) {
				KUUMember member = union.getMembers().get(i);
				Object c = this.findPersonById(member.getId());
				if ("partner".equals(member.getType())) {
					graphComponent.getGraph().insertEdge(parent, null, "",
							cell, c, "partnerShape");
				} else {
					graphComponent
							.getGraph()
							.insertEdge(parent, null, "", cell, c,
									"offspringShape;exitX=0.5;exitY=1;entryX=0.5;entryY=0;");
				}
			}
		}
	}

	public mxCell findUnionById(int id) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphUnion) {
					GraphUnion one = (GraphUnion) child.getValue();
					if (id == one.getId()) {
						return child;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 0 = partner, 1 = offspring
	 * 
	 * @param type
	 * @param v1
	 * @param v2
	 */
	public boolean checkEdge(int type, Object v1, Object v2) {
		mxCell cell1 = (mxCell) v1;
		mxCell cell2 = (mxCell) v2;
		if (cell1.getValue() instanceof GraphUnion
				&& cell2.getValue() instanceof GraphUnion) {
			return false;
		}
		if (cell1.getValue() instanceof GraphPerson
				&& cell2.getValue() instanceof GraphPerson) {
			return false;
		}
		GraphUnion union = null;
		GraphPerson person = null;
		if (cell1.getValue() instanceof GraphUnion) {
			person = (GraphPerson) cell2.getValue();
			union = (GraphUnion) cell1.getValue();
		}
		if (cell2.getValue() instanceof GraphUnion) {
			person = (GraphPerson) cell1.getValue();
			union = (GraphUnion) cell2.getValue();
		}
		KUUMember m = kService.getUnionMember(union.getId(), person.getId());
		if (m != null) {
			System.out.println(" already have union between two elment");
			return false;
		}
		if (type == 0) {//
			int count = kService.getCountUnionById(union.getId());
			if (count >= 2) {
				System.out.println(" must only have 2 partner");
				return false;
			}
		}

		if (type == 1) {//
			Map<String, Boolean> map = kService.getPersonParent(person.getId());
			if (map.get("father") != null && map.get("mother") != null) {
				System.out.println(" already have father and mother");
				return false;
			}
		}
		return true;
	}

	/**
	 * 0 = Partner <br>
	 * 1 = offspring
	 * 
	 * @param type
	 */
	public void drawEdge(int type, Object v1, Object v2) {
		mxCell cell1 = (mxCell) v1;
		mxCell cell2 = (mxCell) v2;
		if (cell1.getValue() instanceof GraphUnion
				&& cell2.getValue() instanceof GraphUnion) {
			return;
		}
		if (cell1.getValue() instanceof GraphPerson
				&& cell2.getValue() instanceof GraphPerson) {
			return;
		}
		Object parent = graphComponent.getGraph().getDefaultParent();
		if (type == 0) {
			mxCell cell = (mxCell) graphComponent.getGraph().insertEdge(parent,
					null, "", v1, v2, "partnerShape");
		} else {
			mxCell cell = (mxCell) graphComponent.getGraph().insertEdge(parent,
					null, "", v1, v2,
					"offspringShape;exitX=0.5;exitY=0;entryX=0.5;entryY=1;");
			// graphComponent.getGraph().getView().updatePoints(cell, points,
			// source, target)
		}

		int uid = -1;
		int pid = -1;
		GraphUnion union = null;
		if (cell1.getValue() instanceof GraphUnion) {
			GraphPerson graphPerson = (GraphPerson) cell2.getValue();
			union = ((GraphUnion) cell1.getValue());
			uid = ((GraphUnion) cell1.getValue()).getId();
			pid = graphPerson.getId();
		}
		if (cell2.getValue() instanceof GraphUnion) {
			GraphPerson graphPerson = (GraphPerson) cell1.getValue();
			union = ((GraphUnion) cell2.getValue());
			uid = ((GraphUnion) cell2.getValue()).getId();
			pid = graphPerson.getId();
		}
		QuestionMap map = new QuestionMap();
		map.saveNewEdge(union, pid, type);
		KPPerson person = kService.getPerson(pid);
		InitUtil.modifyPersonDefault(person, qService, kService);
	}

	public int getCount() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		return c.getChildCount();
	}

	public void singleClickAddEdge(mxCell cell) {
		if (unioning && unioningCell != null) {
			if (this.checkEdge(unioningType, cell, unioningCell)) {
				if (this.handleRouting(unioningType, cell, unioningCell)) {
					drawEdge(unioningType, cell, unioningCell);
				} else {
					System.out.println("超过步骤，无法划线....");
				}
			} else {
				System.out.println("无法生成关系....");
			}
			unioning = false;
			unioningCell = null;
		}
	}

	public void setGraphListener(GraphListener graphListener) {
		this.graphListener = graphListener;
	}

	/**
	 * Set the color of the graph
	 * 
	 * @param mxCell
	 */
	public void setColor(mxCell mxCell, String color) {
		if (mxCell == null) {
			System.out.println(" mxCell is null");
			return;
		}
		if (this.isLocked(mxCell)) {
			return;
		}
		// String a = mxCell.getStyle();
		if (StringUtils.isBlank(color)) {
			System.out.println(mxCell.getId() + "  : color is null");
		} else {
			System.out.println("setColor : " + color + "  old style : "
					+ mxCell.getStyle());
		}
		setDefaultColor(mxCell);
		mxCell.setStyle(mxCell.getStyle() + ";strokeColor=" + selectBorderColor
				+ ";fillColor=" + color);
	}

	/**
	 * Set the color of the graph with the border color
	 * 
	 * @param mxCell
	 */
	public void setColor(mxCell mxCell, String color, String borderColor) {
		if (mxCell == null) {
			System.out.println(" mxCell is null");
			return;
		}
		if (this.isLocked(mxCell)) {
			return;
		}
		// String a = mxCell.getStyle();
		if (StringUtils.isBlank(color)) {
			System.out.println(mxCell.getId() + "  : color is null");
		} else {
			System.out.println("setColor : " + color + "  old style : "
					+ mxCell.getStyle());
		}
		setDefaultColor(mxCell);
		mxCell.setStyle(mxCell.getStyle() + ";strokeColor=" + borderColor
				+ ";fillColor=" + color);
	}

	/**
	 * Set the color of the graph without the border color
	 * 
	 * @param mxCell
	 * @param color
	 */
	public void setOnlyColor(mxCell mxCell, String color) {
		// String a = mxCell.getStyle();
		if (this.isLocked(mxCell)) {
			return;
		}
		setDefaultColor(mxCell);
		mxCell.setStyle(mxCell.getStyle() + ";fillColor=" + color);
	}

	/**
	 * Set the default color
	 * 
	 * @param mxCell
	 * @param color
	 */
	public void setDefaultColor(mxCell mxCell) {
		if (this.isLocked(mxCell)) {
			return;
		}
		String a = mxCell.getStyle();
		if (a.contains("fillColor")) {
			int index = a.indexOf("fillColor");
			String r = a.substring(index - 1, index + 17);
			String rp = a.replace(r, "");
			mxCell.setStyle(rp);
		}
		a = mxCell.getStyle();
		if (a.contains("strokeColor")) {
			int index = a.indexOf("strokeColor");
			String r = a.substring(index - 1, index + 19);
			mxCell.setStyle(a.replace(r, ""));
		}
	}

	public mxIEventListener moveEventListener = new mxIEventListener() {
		@Override
		public void invoke(Object sender, mxEventObject evt) {
			Object[] objs = (Object[]) evt.getProperty("cells");
			if (objs != null) {
				for (Object obj : objs) {
					if (obj instanceof mxCell
							&& ((mxCell) obj).getValue() instanceof GraphPerson) {
						GraphPerson person = (GraphPerson) (((mxCell) obj)
								.getValue());
						if ((person.getIsOther() == 0 && type == 2)
								|| (person.getIsOther() == 1 && type == 1)) {
							continue;
						}
						KPPerson p = kService.getPerson(person.getId());
						mxCell mc = (mxCell) obj;
						if (p != null) {
							int x = (int) mc.getGeometry().getX();
							int y = (int) mc.getGeometry().getY();
							if (!checkPersonPosition(person, x, y)) {
								mc.getGeometry().setX(p.getCoords().getX());
								mc.getGeometry().setY(p.getCoords().getY());
								graphComponent.getGraph().refresh();
							} else {
								p.getCoords().setX(x);
								p.getCoords().setY(y);
								person.setKpperson(p.clone());
							}
						}
					}
					if (obj instanceof mxCell
							&& ((mxCell) obj).getValue() instanceof GraphUnion) {
						GraphUnion union = (GraphUnion) (((mxCell) obj)
								.getValue());
						if ((union.getIsOther() == 0 && type == 2)
								|| (union.getIsOther() == 1 && type == 1)) {
							ivMainPanel.graphPanel.graphComponent.getGraph()
									.refresh();
							ivMainPanel.ographPanel.graphComponent.getGraph()
									.refresh();
							undoManager.undo();
							continue;
						}
						KUUnion u = kService.getUnion(union.getId());
						mxCell mc = (mxCell) obj;
						if (u != null) {
							int x = (int) mc.getGeometry().getX();
							int y = (int) mc.getGeometry().getY();
							if (!checkUnionPosition(union, x, y)) {
								mc.getGeometry().setX(u.getCoords().getX());
								mc.getGeometry().setY(u.getCoords().getY());
								graphComponent.getGraph().refresh();
							} else {
								u.getCoords().setX(x);
								u.getCoords().setY(y);
								union.setKuunion(u.clone());
							}
						}
					}
				}
				kService.save();
			}
		}
	};

	/**
	 * Get the all selected person
	 * 
	 * @return
	 */
	public mxCell getSelectCell() {
		return (mxCell) graphComponent.getGraph().getSelectionCell();
	}

	public void drawParentEdge(mxCell v1, mxCell v2) {
		Object parent = graphComponent.getGraph().getDefaultParent();
		graphComponent.getGraph().insertEdge(parent, null, "", v1, v2,
				"partnerShape");
	}

	public void drawOffspringEdge(mxCell v1, mxCell v2) {
		Object parent = graphComponent.getGraph().getDefaultParent();
		graphComponent.getGraph().insertEdge(parent, null, "", v1, v2,
				"offspringShape;exitX=0.5;exitY=1;entryX=0.5;entryY=0;");
	}

	/**
	 * Get the link line between the two graph
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public Object getEdge(mxCell v1, mxCell v2) {
		Object[] objs = graphComponent.getGraph()
				.getEdgesBetween(v1, v2, false);
		if (objs == null || objs.length == 0) {
			return null;
		}
		return objs[0];
	}

	/**
	 * Get the all person of a union
	 * 
	 * @return
	 */
	public int getMaxPersonId() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		int max = -1;
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphElement one = (GraphElement) child.getValue();
					max = Math.max(one.getId(), max);
				}
			}
		}
		return max;
	}

	public int getMaxUnionId() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		int max = -1;
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphUnion) {
					GraphElement one = (GraphElement) child.getValue();
					max = Math.max(one.getId(), max);
				}
			}
		}
		return max;
	}

	/**
	 * Start the Select Person Mode
	 * 
	 * @param color
	 */
	public void setSelectPersonMode(String color) {
		this.mode = 2;
		this.selectColor = color;
	}

	/**
	 * Clear color and fill the default color
	 */
	public void setAllDefaultColor() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					setDefaultColor(child);
				} else if (child.getValue() instanceof GraphUnion) {
					setDefaultColor(child);
				}
			}
		}
		graphComponent.getGraph().refresh();
	}

	/**
	 * End the Select Person Mode
	 */
	public void completeSelectPersonMode() {
		this.mode = 1;
		this.selectColor = "";
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					setDefaultColor(child);
				}
			}
		}
	}

	/**
	 * Change the color when the graph is selected
	 * 
	 * @param cell
	 */
	public void changeColor(mxCell cell) {
		if (cell != null && StringUtils.isNotBlank(selectColor)) {
			if (cell.getValue() instanceof GraphPerson) {
				if (cell.getStyle().contains(selectColor)) {
					this.setDefaultColor(cell);
				} else {
					this.setColor(cell, selectColor);
				}
				this.graphComponent.getGraph().refresh();
			}
		}
	}

	/**
	 * Change the color of the graph
	 * 
	 * @param cell
	 * @param color
	 */
	public void changeColor(mxCell cell, String color) {
		if (cell != null && StringUtils.isNotBlank(color)) {
			if (cell.getValue() instanceof GraphPerson) {
				this.setOnlyColor(cell, color);
				this.graphComponent.getGraph().refresh();
				// this.graphListener.afterSelectCell(this);
			}
		}
	}

	/**
	 * Get the pid of the selected person
	 * 
	 * @return
	 */
	public List<Integer> getSelectPerson() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		List<Integer> list = new ArrayList<Integer>();
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					String style = child.getStyle();
					if (style.contains(selectColor)) {
						GraphPerson gp = (GraphPerson) child.getValue();
						list.add(gp.getId());
					}
				}
			}
		}
		return list;
	}

	/**
	 * Initialize the selected person
	 */
	public void initSelectPerson(final List<Integer> pids) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		setAllDefaultColor();
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					if (pids.contains(gp.getId())) {
						this.setDefaultColor(child);
						this.changeColor(child);
					}
				}
			}
		}
		this.graphComponent.getGraph().refresh();
	}

	public void initSelectPerson(final List<Integer> pids, String color,
			String value) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					if (pids.contains(gp.getId())) {
						this.setDefaultColor(child);
						// this.changeColor(child, color);
						this.setColor(child, color, color);
						gp.setTip(value);
					}
				}
			}
		}
		this.graphComponent.getGraph().refresh();
	}

	public void initSelectUnion(final List<Integer> uids, String color,
			String value) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphUnion) {
					GraphUnion gu = (GraphUnion) child.getValue();
					if (uids.contains(gu.getId())) {
						this.setDefaultColor(child);
						// this.changeColor(child, color);
						this.setColor(child, color, color);
					}
				}
			}
		}
		this.graphComponent.getGraph().refresh();
	}

	/**
	 * Set all TIP as null
	 */
	public void setAllTipNull() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					gp.setTip("");
				}
			}
		}
		this.graphComponent.getGraph().refresh();
	}

	/**
	 * Check the select mode of a person
	 * 
	 * @param cell
	 */
	public Boolean isSelectModel(mxCell cell) {
		Boolean flag = false;
		if (cell != null && StringUtils.isNotBlank(selectColor)) {
			if (cell.getValue() instanceof GraphPerson) {
				if (cell.getStyle().contains(selectColor)) {
					flag = true;
				}
			}
		}
		return flag;
	}

	public mxCell getCellByXY(int x, int y) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson
						|| child.getValue() instanceof GraphUnion) {
					mxCell[] cells = new mxCell[] { child };
					mxRectangle rect = graphComponent.getGraph()
							.getBoundingBoxFromGeometry(cells);
					if (child.getValue() instanceof GraphPerson) {
						GraphPerson gp = (GraphPerson) child.getValue();
					}

					if (rect.contains(x, y)) {
						return child;
					}
				}
			}
		}
		return null;
	}

	public mxCell getCellByXYLeftRight(int x, int y) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson
						|| child.getValue() instanceof GraphUnion) {
					mxCell[] cells = new mxCell[] { child };
					mxRectangle rect = graphComponent.getGraph()
							.getBoundingBoxFromGeometry(cells);

					if (rect.contains(x, y)) {
						return child;
					} else if (rect.contains(x + cellSize, y + cellSize)) {
						return child;
					} else if (rect
							.contains(x + cellSize / 2, y + cellSize / 2)) {
						return child;
					}
				}
			}
		}
		return null;
	}

	public String getColorByPerson(int pid) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		String color = "";
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					if (gp.getId() == pid) {
						String a = child.getStyle();
						if (a.contains("fillColor")) {
							int index = a.indexOf("fillColor");
							color = a.substring(index + 10, index + 17);
							break;
						}
					}
				}
			}
		}
		return color;
	}

	/**
	 * Get the all person with the same color
	 * 
	 * @param color
	 * @return
	 */
	public List<Integer> getPersonByColor(String color) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		List<Integer> list = new ArrayList<Integer>();
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					String a = child.getStyle();
					if (a.contains(color)) {
						list.add(gp.getId());
					}
				}
			}
		}
		return list;
	}

	public void setSelectCellNull() {
		graphComponent.getGraph().setSelectionCell(null);
	}

	public void clearAllCell() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson one = (GraphPerson) child.getValue();
					if (kService.getPerson(one.getId()) == null) {
						mxCell[] cells = new mxCell[] { child };
						this.graphComponent.getGraph().removeCells(cells);
					}
				}
				if (child.getValue() instanceof GraphUnion) {
					GraphUnion one = (GraphUnion) child.getValue();
					if (kService.getUnion(one.getId()) == null) {
						mxCell[] cells = new mxCell[] { child };
						this.graphComponent.getGraph().removeCells(cells);
					}
				}
			}
		}
	}

	public int getDisable() {
		return disable;
	}

	/**
	 * Set the Draw Mode
	 * 
	 * @param disable
	 */
	public void setDisable(int disable) {
		this.disable = disable;
		if (this.disable == DISABLE_NEW) {
			graphComponent.getGraph().setCellsMovable(true);
			graphComponent.getGraph().setCellsDeletable(true);
			// graphComponent.getGraph().setCellsSelectable(false);
		} else {
			graphComponent.getGraph().setCellsMovable(false);
			graphComponent.getGraph().setCellsDeletable(false);
			// graphComponent.getGraph().setCellsSelectable(false);
		}
	}

	/**
	 * Get the kinship steps of a person
	 * 
	 * @param pid
	 * @return
	 */
	public int getRoutingCount(int pid) {
		Routing routing = new Routing();
		routing.initRoute();
		int count = Integer.MAX_VALUE;
		try {
			count = routing.comput_routing(1, pid + 1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return count;
	}

	/**
	 * Check the range of union, delete the union when it is over bounds
	 */
	public boolean handleRouting(int type, Object v1, Object v2) {
		StopRule sr = qService.getStopRule();
		int step = sr.getStep();
		GraphUnion union = null;
		GraphPerson person = null;
		mxCell cell1 = (mxCell) v1;
		mxCell cell2 = (mxCell) v2;
		if (cell1.getValue() instanceof GraphUnion) {
			person = (GraphPerson) cell2.getValue();
			union = (GraphUnion) cell1.getValue();
		}
		if (cell2.getValue() instanceof GraphUnion) {
			person = (GraphPerson) cell1.getValue();
			union = (GraphUnion) cell2.getValue();
		}
		KUUnion kunion = kService.getUnion(union.getId());
		KUUMember m = null;
		if (union != null) {
			List<KUUMember> members = kunion.getMembers();
			for (KUUMember member : members) {
				if (member.getId() == person.getId()) {
					m = member;
					break;
				}
			}
			if (m == null) {
				m = new KUUMember();
				m.setId(person.getId());
				if (type == 0) {
					m.setType("partner");
				} else {
					m.setType("offspring");
				}
				kunion.getMembers().add(m);
				kService.save();
			}
		}
		if (this.type == 2) {// otherGragh
			return true;
		}
		int rcount = getRoutingCount(person.getId());
		boolean flag = true;
		if (rcount == step || rcount == Integer.MAX_VALUE) {
			mxCell mxCell = findPersonById(person.getId());
			this.setColor(mxCell, ColorUtil.getStopRuleColor());
			this.graphComponent.refresh();
			// if("offspring".equals(m.getType())) {
			// flag = false;
			// }
		} else if (rcount > step) {
			flag = false;
		} else {
			mxCell mxCell = findPersonById(person.getId());
			this.setDefaultColor(mxCell);
			this.graphComponent.refresh();
		}
		// Delete the union's members
		kunion.getMembers().remove(m);
		kService.save();
		return flag;
	}

	// ========================================撤销操作==========================
	private void undoChangeSave(List<mxUndoableChange> changes) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			saveUndoPerson(c);
			saveUndoUnion(c);
		}
	}

	private void saveUndoPerson(mxCell c) {
		List<KPPerson> dbPersons = kService.get().getPeople().getPersons();
		// save the undo person
		// public int type = 1;//1=graph, 2=ograph
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				if (type == 1) {// nkk
					if (gp.getIsOther() == 1) {
						continue;
					}
				}
				if (type == 2) {// other
					if (gp.getIsOther() == 0) {
						continue;
					}
				}
				KPPerson kpperson = kService.getPerson(gp.getId());
				if (kpperson == null) {
					KPPCoords co = new KPPCoords();
					co.setX(child.getGeometry().getX());
					co.setY(child.getGeometry().getY());
					gp.getKpperson().setCoords(co);
					dbPersons.add(gp.getKpperson());
				} else {
					KPPCoords co = new KPPCoords();
					co.setX(child.getGeometry().getX());
					co.setY(child.getGeometry().getY());
					gp.getKpperson().setCoords(co);
					int index = this.getIndexOfPerson(dbPersons, gp.getId());
					dbPersons.set(index, gp.getKpperson());
				}
			}
		}
		// Delete the person
		List<KPPerson> deletes = new ArrayList<KPPerson>();
		for (int i = 0; i < dbPersons.size(); i++) {
			KPPerson kpperson = dbPersons.get(i);
			if (type == 1) {// nkk
				if (kpperson.getIsOther() == 1) {
					continue;
				}
			}
			if (type == 2) {// other
				if (kpperson.getIsOther() == 0) {
					continue;
				}
			}
			boolean flag = false;
			for (int j = 0; j < c.getChildCount(); j++) {
				mxCell child = (mxCell) c.getChildAt(j);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					if (type == 1) {// nkk
						if (gp.getIsOther() == 1) {
							continue;
						}
					}
					if (type == 2) {// other
						if (gp.getIsOther() == 0) {
							continue;
						}
					}
					if (gp.getId() == kpperson.getPid()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				deletes.add(kpperson);
			}
		}
		dbPersons.removeAll(deletes);
		kService.save();
	}

	private int getIndexOfPerson(List<KPPerson> dbPersons, int pid) {
		for (int i = 0; i < dbPersons.size(); i++) {
			KPPerson kpperson = dbPersons.get(i);
			if (kpperson.getPid() == pid) {
				return i;
			}
		}
		return -1;
	}

	private void saveUndoUnion(mxCell c) {
		List<KUUnion> dbUnions = kService.get().getUnions().getUnions();
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphUnion) {
				GraphUnion gu = (GraphUnion) child.getValue();
				if (type == 1) {// nkk
					if (gu.getIsOther() == 1) {
						continue;
					}
				}
				if (type == 2) {// other
					if (gu.getIsOther() == 0) {
						continue;
					}
				}
				KUUnion kuunion = kService.getUnion(gu.getId());
				if (kuunion == null) {
					KPPCoords co = new KPPCoords();
					co.setX(child.getGeometry().getX());
					co.setY(child.getGeometry().getY());
					gu.getKuunion().setCoords(co);
					dbUnions.add(gu.getKuunion());
				} else {
					int index = this.getIndexOfUnion(dbUnions, gu.getId());
					KPPCoords co = new KPPCoords();
					co.setX(child.getGeometry().getX());
					co.setY(child.getGeometry().getY());
					gu.getKuunion().setCoords(co);
					dbUnions.set(index, gu.getKuunion());

				}
			}
		}
		List<KUUnion> deletes = new ArrayList<KUUnion>();
		for (int i = 0; i < dbUnions.size(); i++) {
			KUUnion kuunion = dbUnions.get(i);
			if (type == 1) {// nkk
				if (kuunion.getIsOther() == 1) {
					continue;
				}
			}
			if (type == 2) {// other
				if (kuunion.getIsOther() == 0) {
					continue;
				}
			}
			boolean flag = false;
			for (int j = 0; j < c.getChildCount(); j++) {
				mxCell child = (mxCell) c.getChildAt(j);
				if (child.getValue() instanceof GraphUnion) {
					GraphUnion gu = (GraphUnion) child.getValue();
					if (type == 1) {// nkk
						if (gu.getIsOther() == 1) {
							continue;
						}
					}
					if (type == 2) {// other
						if (gu.getIsOther() == 0) {
							continue;
						}
					}
					if (gu.getId() == kuunion.getId()) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				deletes.add(kuunion);
			}
		}
		dbUnions.removeAll(deletes);
		kService.save();
		saveUnionEdge(c);
	}

	/**
	 * Lock some person to be selected when this question don't applied to these
	 * person
	 * 
	 * @param pids
	 */
	public void lockPerson(List<Integer> pids) {
		if (pids == null)
			return;
		for (Integer pid : pids) {
			mxCell cell = this.findPersonById(pid);
			this.setColor(cell, lockColor);
			lockCells.add(cell);
		}
		this.graphComponent.getGraph().refresh();
	}

	/**
	 * unlock person
	 */
	public void clearLockPerson() {
		for (mxCell mxCell : lockCells) {
			this.setDefaultColor(mxCell);
		}
		lockCells.clear();
	}

	/**
	 * Check the lock
	 * 
	 * @param cell
	 * @return
	 */
	public boolean isLocked(mxCell cell) {
		for (mxCell mxCell : lockCells) {
			if (cell == mxCell) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Save the union and its members
	 */
	private void saveUnionEdge(mxCell c) {
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphUnion) {
				GraphUnion gu = (GraphUnion) child.getValue();
				KUUnion union = kService.getUnion(gu.getId());
				union.getMembers().clear();
				for (int j = 0; j < child.getEdgeCount(); j++) {
					mxCell cell = (mxCell) child.getEdgeAt(j);
					String style = cell.getStyle();
					mxCell source = (mxCell) cell.getSource();
					mxCell target = (mxCell) cell.getTarget();
					GraphPerson a = null;
					if (source.getValue() instanceof GraphPerson) {
						a = (GraphPerson) source.getValue();
					} else if (target.getValue() instanceof GraphPerson) {
						a = (GraphPerson) target.getValue();
					}
					KUUMember member = new KUUMember();
					member.setId(a.getId());
					if (style.contains("partner")) {
						member.setType("partner");
					} else if (style.contains("offspring")) {
						member.setType("offspring");
					}
					union.getMembers().add(member);
				}
			}
		}
		kService.save();
	}

	private int getIndexOfUnion(List<KUUnion> dbUnions, int uid) {
		for (int i = 0; i < dbUnions.size(); i++) {
			KUUnion kuunion = dbUnions.get(i);
			if (kuunion.getId() == uid) {
				return i;
			}
		}
		return -1;
	}

	public void clearSelected() {
		this.graphComponent.getGraph().clearSelection();
	}

	/**
	 * Search the person in Nkk and other important diagram by the name and sex
	 * of person
	 * 
	 * @param name
	 * @return
	 */
	public mxCell searchByName(int pid, String name, int sex) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				if (StringUtils.equals(name, gp.getName())
						&& gp.getSex() == sex) {
					if (gp.getId() == pid) {
						continue;
					}
					return child;
				}
			}
		}
		return null;
	}

	/**
	 * Search the person in Nkk and other important diagram by the name of
	 * person
	 * 
	 * @param name
	 * @return
	 */
	public List<mxCell> searchByName(String name) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		List<mxCell> list = new ArrayList<mxCell>();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				if (StringUtils.contains(gp.getName(), name)) {
					list.add(child);
				}
			}
		}
		return list;
	}

	/**
	 * The flag for the mark status of a person in a question answer
	 * 
	 * @param qid
	 * @param flag
	 *            true = display, false为 = don't display
	 */
	public void showRemark(String qid, boolean flag) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				if (flag) {
					Question question = qService.searchQuestion(qid);
					gp.setRemark(kService.getPersonQuestionValue(gp.getId(),
							question));
				} else {
					gp.setRemark("");
				}
			}
		}
		this.graphComponent.refresh();

	}

	/**
	 * the tip of the graph when the mouse is over it
	 * 
	 * @param qid
	 * @param flag
	 */
	public void showTip(String qid, boolean flag) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				if (flag) {
					Question question = qService.searchQuestion(qid);
					gp.setTip(kService.getPersonQuestionValue(gp.getId(),
							question));
				} else {
					gp.setTip("");
				}
			}
		}
		this.graphComponent.refresh();

	}

	public void setToolTips(boolean flag) {
		graphComponent.setToolTips(flag);
	}

	public void setAllRemarkNone() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson gp = (GraphPerson) child.getValue();
				gp.setRemark("");
			}
		}
		this.graphComponent.refresh();
	}

	/**
	 * Highlight the graph
	 * 
	 * @param color
	 */
	public void highLine(String color) {
		clearHighLine();
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				String style = child.getStyle();
				if (style.contains(color)) {
					this.setColor(child, color, color);
				} else {
					this.setColor(child, this.getColorByCell(child),
							this.getColorByCell(child));
				}
			}
		}
		this.graphComponent.refresh();
	}

	public void clearHighLine() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				String a = child.getStyle();
				if (a.contains("strokeColor")) {
					int index = a.indexOf("strokeColor");
					String r = a.substring(index - 1, index + 19);
					child.setStyle(a.replace(r, ""));
				}
			}
		}
		this.graphComponent.refresh();
	}

	/**
	 * Check the white color of graph
	 * 
	 * @param cell
	 * @return
	 */
	private Boolean cellColorIfWhite(mxCell cell) {
		String color = getColorByCell(cell);
		if ("".equals(color) || color.equals("#FFFFFF")) {
			return true;
		}
		return false;
	}

	/**
	 * Get the color of the graph
	 * 
	 * @param child
	 * @return
	 */
	public String getColorByCell(mxCell child) {
		String color = "";
		String a = child.getStyle();
		if (a.contains("fillColor")) {
			int index = a.indexOf("fillColor");
			color = a.substring(index + 10, index + 17);
		}
		return color;
	}

	/**
	 * Set the color of the graph when it completed in the draw mode
	 */
	public void completePerson() {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		for (int i = 0; i < c.getChildCount(); i++) {
			mxCell child = (mxCell) c.getChildAt(i);
			if (child.getValue() instanceof GraphPerson) {
				GraphPerson person = (GraphPerson) child.getValue();
				KPPerson kperson = kService.getPerson(person.getId());
				if (kperson != null && kperson.getCompleted() == 1) {
					this.setColor(child, ColorUtil.getCompletePersonColor());
				}
			}
		}
		this.graphComponent.refresh();
	}

	public CustomGraph getCustomGraph() {
		return (CustomGraph) graphComponent.getGraph();
	}

	/**
	 * Check the position of the person
	 * 
	 * @param gp
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean checkPersonPosition(GraphPerson gp, int x, int y) {
		List<KUUnion> unions = kService.getUnionByPid(gp.getId());
		for (KUUnion union : unions) {
			List<KUUMember> ms = union.getMembers();
			for (KUUMember m : ms) {
				if (m.getId() == gp.getId()) {
					if ("partner".equals(m.getType())) {// 必须高于union
						if (y >= union.getCoords().getY()) {
							return false;
						}
					} else if ("offspring".equals(m.getType())) {
						if (y <= union.getCoords().getY()) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public boolean checkUnionPosition(GraphUnion gu, int x, int y) {
		KUUnion union = kService.getUnion(gu.getId());
		List<KUUMember> ms = union.getMembers();
		for (KUUMember m : ms) {
			if ("partner".equals(m.getType())) {// 必须高于union
				KPPerson kpperson = kService.getPerson(m.getId());
				if (kpperson.getCoords().getY() >= y) {
					return false;
				}
			} else if ("offspring".equals(m.getType())) {
				KPPerson kpperson = kService.getPerson(m.getId());
				if (kpperson.getCoords().getY() <= y) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Auto rearrange the NKK diagram
	 */
	public void reRange() {
		GraphAutoRangle.getInstance().setGraghPanel(this);
		GraphAutoRangle.getInstance().autoRange();
		return;
	}

	/**
	 * Display the NKK diagram by the range setting
	 * 
	 * @param range
	 *            -1= all 表示全部
	 */
	public void showByRange(int range) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		Routing routing = new Routing();
		routing.initRoute();
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				child.setVisible(true);
			}
		}
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson) {
					GraphPerson gp = (GraphPerson) child.getValue();
					if (gp.getId() == 0) {
						continue;
					}
					int count = Integer.MAX_VALUE;
					try {
						count = routing.comput_routing(1, gp.getId() + 1);
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
					if (count > range) {

						Object[] objs = graphComponent.getGraph()
								.getConnections(child);
						for (Object obj : objs) {
							mxCell u = (mxCell) obj;
							if (u.isEdge()) {
								mxCell source = (mxCell) u.getSource();
								mxCell target = (mxCell) u.getTarget();
								source.setVisible(false);
								target.setVisible(false);
							}
						}
						child.setVisible(false);
					}
				}

			}
		}
		this.graphComponent.refresh();
	}

	public void exportPNG(String absFilePath) {
		Color bg = Color.WHITE;
		BufferedImage image = mxCellRenderer.createBufferedImage(
				this.getCustomGraph(), null, 1, bg,
				graphComponent.isAntiAlias(), null, graphComponent.getCanvas());
		if (image != null) {
			try {
				ImageIO.write(image, "PNG", new File(absFilePath));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(graphComponent,
					mxResources.get("noImageData"));
		}
	}

	public BufferedImage exportPNG() {
		Color bg = Color.WHITE;
		BufferedImage image = mxCellRenderer.createBufferedImage(
				this.getCustomGraph(), null, 1, bg,
				graphComponent.isAntiAlias(), null, graphComponent.getCanvas());
		return image;
	}

	public void exportSVG(String absFilePath) {
		mxSvgCanvas canvas = (mxSvgCanvas) mxCellRenderer.drawCells(
				this.getCustomGraph(), null, 1, null, new CanvasFactory() {
					public mxICanvas createCanvas(int width, int height) {
						mxSvgCanvas canvas = new mxSvgCanvas(mxDomUtils
								.createSvgDocument(width, height));
						canvas.setEmbedded(true);

						return canvas;
					}

				});
		if (canvas != null) {
			try {
				Document document = canvas.getDocument();
				String xml = mxXmlUtils.getXml(document);
				mxUtils.writeFile(xml, absFilePath);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void changeSex(mxCell cell, int pid, int sex) {
		if (cell != null) {
			GraphPerson person = (GraphPerson) cell.getValue();
			person.setSex(sex);
			String style = getPersonStyle(person);
			cell.setStyle(style);
			this.graphComponent.getGraph().refresh();
		}
	}

	/**
	 * Set the zoom by the pid of person
	 * 
	 * @param pid
	 */
	public void zoomToCell(int pid) {
		mxCell cell = this.findPersonById(pid);
		this.zoomToCell(cell);
		this.graphComponent.getGraph().setSelectionCell(cell);
	}

	/**
	 * Moving all graphic cell: 1 = move up, 2 = move right, 3 = move down, 4 =
	 * move left
	 */
	public void moveAll(int or) {
		mxCell root = (mxCell) graphComponent.getGraph().getModel().getRoot();
		mxCell c = (mxCell) root.getChildAt(0);
		if (c.getChildCount() > 0) {
			for (int i = 0; i < c.getChildCount(); i++) {
				mxCell child = (mxCell) c.getChildAt(i);
				if (child.getValue() instanceof GraphPerson
						|| child.getValue() instanceof GraphUnion) {
					double x = child.getGeometry().getX();
					double y = child.getGeometry().getY();
					if (or == 1) {
						child.getGeometry().setY(y - 10);
					} else if (or == 2) {
						child.getGeometry().setX(x + 10);
					} else if (or == 3) {
						child.getGeometry().setY(y + 10);
					} else if (or == 4) {
						child.getGeometry().setX(x - 10);
					}
				}
			}
			graphComponent.getGraph().refresh();
		}
	}
}
