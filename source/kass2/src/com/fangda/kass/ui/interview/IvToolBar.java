package com.fangda.kass.ui.interview;

import javax.swing.BorderFactory;
import javax.swing.JToolBar;

import com.fangda.kass.ui.common.DefaultActions.SaveAction;
import com.fangda.kass.ui.interview.IvAction.DeleteAction;
import com.fangda.kass.ui.interview.IvAction.MoveAction;
import com.fangda.kass.ui.interview.IvAction.PageSetUpAction;
import com.fangda.kass.ui.interview.IvAction.RedoAction;
import com.fangda.kass.ui.interview.IvAction.RengeAction;
import com.fangda.kass.ui.interview.IvAction.UndoAction;
import com.fangda.kass.ui.interview.IvAction.ZoomAction;

/**
 * This class for the tool bar in the KNQ2
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class IvToolBar extends JToolBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1895899275647020539L;

	public IvToolBar(IvMainPanel mainPanel, int orientation) {
		super(orientation);
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(3, 3, 3, 3), getBorder()));
		setFloatable(false);

		// add(mainPanel.kmain.bind("New", new QuestionnaireEditAction(),
		// "/images/new.gif"));
		// add(mainPanel.kmain.bind("Open", new QuestionnaireLoadAction(),
		// "/images/open.gif"));
		add(mainPanel.kmain.bind("Save", new SaveAction(), "/images/save.gif"));

		addSeparator();

		// add(mainPanel.kmain.bind("Print", new PrintAction(),
		// "/images/print.gif"));

		// addSeparator();

		// add(mainPanel.kmain.bind("Cut", TransferHandler.getCutAction(),
		// "/images/cut.gif"));
		// add(mainPanel.kmain.bind("Copy", TransferHandler.getCopyAction(),
		// "/images/copy.gif"));
		// add(mainPanel.kmain.bind("Paste", TransferHandler.getPasteAction(),
		// "/images/paste.gif"));

		// addSeparator();

		add(mainPanel.kmain.bind("Delete", new DeleteAction(),
				"/images/delete.gif"));
		addSeparator();

		add(mainPanel.kmain.bind("Undo", new UndoAction(), "/images/undo.gif"));
		add(mainPanel.kmain.bind("Redo", new RedoAction(), "/images/redo.gif"));
		addSeparator();

		// add(mainPanel.kmain.bind("Preview", new PreviewAction(),
		// "/images/view.gif"));
		// add(mainPanel.kmain.bind("Release", new ReleaseAction(),
		// "/images/pagesetup.gif"));

		// addSeparator();

		/*
		 * add(mainPanel.kmain.bind("QImport", new QImportAction(),
		 * "/images/import.png")); add(mainPanel.kmain.bind("QExport", new
		 * QExportAction(), "/images/expotDoc.png"));
		 * 
		 * addSeparator();
		 */
		add(mainPanel.kmain.bind("zoomIn", new ZoomAction(2),
				"/images/zoomin.gif"));
		add(mainPanel.kmain.bind("zoomOut", new ZoomAction(1),
				"/images/zoomout.gif"));
		add(mainPanel.kmain.bind("zoom", new ZoomAction(3), "/images/zoom.gif"));
		add(mainPanel.kmain.bind("zoom1", new ZoomAction(4),
				"/images/zoomactual.gif"));
		addSeparator();
		add(mainPanel.kmain.bind("Gragh Setup", new PageSetUpAction(),
				"/images/pagesetup.gif"));
		add(mainPanel.kmain.bind("Gragh Renge", new RengeAction(),
				"/images/group.gif"));
		add(mainPanel.kmain.bind("Gragh Move", new MoveAction(),
				"/images/expand.gif"));
		addSeparator();
	}
}
