package com.fangda.kass.ui.interview.graph.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.fangda.kass.service.interview.KassService;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

/**
 * The Class for the the base action.
 * 
 * @author Fangfang Zhao
 * 
 */
public abstract class CustomBaseAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2988308608746109230L;
	private KassService kassService;

	@Override
	public void actionPerformed(ActionEvent e) {
		action(e);
	}

	public abstract void action(ActionEvent e);

	public mxGraph getGraph(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof mxGraphComponent) {
			return ((mxGraphComponent) source).getGraph();
		}
		return null;
	}

	public KassService getService() {
		if (kassService == null) {
			kassService = new KassService();
		}
		return kassService;
	}

}
