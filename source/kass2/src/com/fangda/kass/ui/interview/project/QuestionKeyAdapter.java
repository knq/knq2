package com.fangda.kass.ui.interview.project;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * This class for the key adapter for the data type of the question answer in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class QuestionKeyAdapter extends KeyAdapter {

	public final static int TYPE_INT = 1;
	public final static int TYPE_DECIMAL = 2;
	public final static int TYPE_PERCENT = 3;
	public final static int TYPE_STRING = 4;
	
	private int type;
	
	private QuestionKeyReleaseEvent keyReleaseEvent;
	
	public QuestionKeyAdapter(int t, QuestionKeyReleaseEvent event) {
		this.type = t;
		this.keyReleaseEvent = event;
	}

	@Override
	public void keyTyped(KeyEvent evt) {
		switch(type) {
		case TYPE_INT :
			if (!(evt.getKeyChar() >= KeyEvent.VK_0 && evt.getKeyChar() <= KeyEvent.VK_9)) {
				evt.consume();
			}
			break;
		case TYPE_DECIMAL :
			if (!((evt.getKeyChar() >= KeyEvent.VK_0 && evt.getKeyChar() <= KeyEvent.VK_9) || evt
					.getKeyChar() == KeyEvent.VK_PERIOD)) {
				evt.consume();
			}
			break;
		case TYPE_PERCENT:
			if (!(((evt.getKeyChar() >= KeyEvent.VK_0 && evt.getKeyChar() <= KeyEvent.VK_9)|| evt.getKeyChar()=='%') || evt
					.getKeyChar() == KeyEvent.VK_PERIOD) ) {
				evt.consume();
			}
			break;
		case TYPE_STRING:
			
			break;
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		super.keyPressed(e);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(keyReleaseEvent != null) {
			keyReleaseEvent.released(e);
		}
	}
}
