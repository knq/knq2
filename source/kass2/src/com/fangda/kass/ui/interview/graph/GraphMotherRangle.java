package com.fangda.kass.ui.interview.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.interview.PageConfig;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.interview.graph.GraphAutoRangle.PersonG;
import com.fangda.kass.ui.interview.graph.GraphAutoRangle.UnionG;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DPerson;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DUnion;

/**
 * The Class for the auto arrange of the unions and their members on the right
 * of the NKK diagram (the network kinship diagram).
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class GraphMotherRangle {

	/**
	 * The set of unions on the right of the NKK diagram (the network kinship
	 * diagram).
	 */
	private TreeMap<Integer, List<UnionG>> ums = null;

	/**
	 * The set of arranged unions on the right of the NKK diagram (the network
	 * kinship diagram).
	 */
	private TreeMap<Integer, List<UnionG>> aleadyUms = null;

	/**
	 * The set of all person on the right of the NKK diagram (the network
	 * kinship diagram).
	 */
	private TreeMap<Integer, List<PersonG>> pms = null;

	/**
	 * The set of arranged all person on the left of the NKK diagram (the
	 * network kinship diagram).
	 */
	private TreeMap<Integer, List<PersonG>> aleadyPms = null;

	private GraphAutoRangle range;
	private KassService kassService;

	private Double x0 = 600d;
	private Double p = 100d;
	private Double h = 100d;

	public GraphMotherRangle(GraphAutoRangle range, KassService kassService) {
		this.range = range;
		this.kassService = kassService;
		this.x0 = this.range.getX0();
		PageConfig pageConfig = kassService.get().getPageConfig();
		this.p = (double) pageConfig.getCellWith();
		this.h = (double) pageConfig.getCellHeight();
	}

	public void initRangle() {
		this.init();
		this.fillData();
	}

	public void range1(int g, int s, int m) {
		List<UnionG> gs = ums.get(g);
		if (gs != null && gs.size() > 0) {
			UnionG g0 = gs.get(0);
			KUUnion egoParentUnion = kassService.getPersonUnion(0, "offspring");
			if (egoParentUnion.getId() == g0.getUnion().getId()) {
				// Arrange the Ego's parent union
				arrangeEgoParentU(g0, g, s, m);
			}
			// Arrange the child unions of ego
			List<KUUnion> unions = kassService.getUnionByPid(0, "partner");
			for (KUUnion union : unions) {
				if (union.getId() == g0.getUnion().getId()) {
					arrangeEgoChildU(union.getId(), s, m, g);
				}
			}
			// Arrange the child unions of offsprings in the last arranged
			// generation
			this.arrangeUmOffspringU(s, m, g);

			// Arrange the unarranged unions of offsprings after the above steps
			this.arrangeUmOther(s, m, g);
			// System.out.println("-----m-: "+m+"-------");
			// System.out.println("uid: "+g0.getUnion().getId()+" uX: "+g0.getX()+"X0: "+x0);
			// printCoor();
		}
	}

	private void arrangeEgoParentU(UnionG g0, int g, int s, int m) {
		g0.setX(x0);
		g0.setY(2 * h * (s - m));
		g0.setXl(x0 - 2 * p);
		g0.setXr(x0 + 2 * p);
		addToAlreadyUms(g, g0, 0);//
		// Get the mother of ego
		List<Integer> motherIds = kassService.getEgoParent(true);
		if (motherIds.size() > 0) {
			Integer motherId = motherIds.get(0);
			Integer uid = this.getParentUnion(motherId);
			if (uid == -1) {
				PersonG personG = getFromPms(g + 1, motherId);
				if (personG != null) {
					personG.setX(x0 + p);
					personG.setY(2 * h * (s - m) - h);
					this.addToAlreadyPms(g + 1, personG, 0);
					this.moveUnionMethod0(m);// Step4.1.1.3.3
					this.delFromPms(g + 1, personG);
				}
			}
		}
		this.delFromUms(g, g0);
		this.rangeUnionOffspring(g0.getUnion().getId(), g, s, m);
	}

	/**
	 * Arrange offspring of a union
	 * 
	 * @param uid
	 * @param g
	 * @param s
	 * @param m
	 */
	private void rangeUnionOffspring(int uid, int g, int s, int m) {
		List<KPPerson> persons = this.getChildren(uid);
		UnionG ug = this.getFromAlreadyUms(g, uid);
		int n = this.getIndexFromAlreadyUms(g, uid);
		Collections.sort(persons, new Comparator<KPPerson>() {

			@Override
			public int compare(KPPerson o1, KPPerson o2) {
				String year1 = kassService.getPersonBirthYear(o1.getPid());
				String year2 = kassService.getPersonBirthYear(o2.getPid());
				if (StringUtils.isBlank(year1) || StringUtils.isBlank(year2)) {
					return o1.getPid() - o2.getPid();
				}

				return year1.compareTo(year2);
			}
		});
		if (persons.size() == 1) {
			int pid = persons.get(0).getPid();
			PersonG pg = this.getFromPms(g, pid);
			if (pg == null) {
				return;
			}
			List<PersonG> gs = this.getAleadyPms().get(g);
			int k = 0;
			if (gs != null) {
				for (int i = 0; i < gs.size(); i++) {
					PersonG p1 = gs.get(i);
					if (ug.getX() > p1.getX()) {
						k = i + 1;
					} else {
						break;
					}
				}
			}
			this.addToAlreadyPms(g, pg, k);
			this.delFromPms(g, pg);
			pg.setX(ug.getX());
			pg.setY(2 * h * (s - m) + h);
		} else {
			int k = 0;
			int kk = 0;
			for (int i = 0; i < persons.size(); i++) {
				int pid = persons.get(i).getPid();
				PersonG pg = this.getFromPms(g, pid);
				if (pg == null) {
					continue;
				}
				if (i == 0) {
					if (n == 0) {
						this.addToAlreadyPms(g, pg, 0);
						this.delFromPms(g, pg);
						pg.setX(ug.getX() - p);
						pg.setY(2 * h * (s - m) + h);
						k = 0;
					} else if (n > 0) {
						List<PersonG> gs = this.getAleadyPms().get(g);
						if (gs.size() == 0) {
							this.addToAlreadyPms(g, pg, 0);
							this.delFromPms(g, pg);
							pg.setX(ug.getX() - p);
							pg.setY(2 * h * (s - m) + h);
							k = 0;
						}
						for (int j = 0; j < gs.size(); j++) {
							PersonG p = gs.get(j);
							if (p.getX() < ug.getX()) {
								k = j + 1;
							} else {
								break;
							}
						}
						this.addToAlreadyPms(g, pg, k);
						this.delFromPms(g, pg);
						pg.setX(ug.getX() - p);
						pg.setY(2 * h * (s - m) + h);
					}
				} else {
					kk = i + k;
					this.addToAlreadyPms(g, pg, kk);
					this.delFromPms(g, pg);
					PersonG beforePG = this.getFromAlreadyPmsByIndex(g, kk - 1);
					pg.setX(beforePG.getX() + p);
					pg.setY(2 * h * (s - m) + h);
				}
			}
			if (persons.size() > 3) {
				PersonG pg = this.getFromAlreadyPmsByIndex(g, kk);
				ug.setXr(pg.getX() + p);
				this.moveUnionMethod5(ug, pg, s, m);
			}
		}
	}

	/**
	 * Arrange the unarranged unions of offsprings after the above steps
	 * 
	 * @param s
	 * @param m
	 * @param g
	 */
	private void arrangeUmOther(int s, int m, int g) {
		List<UnionG> unions = this.ums.get(g);
		while (unions.size() > 0) {
			UnionG u0 = unions.get(0);
			int n = -1;
			List<UnionG> alreadyUnions = this.aleadyUms.get(g);
			if (alreadyUnions == null || alreadyUnions.size() == 0) {
				n = 0;
			} else {
				// Sort the unions when these unions are the same kinship
				// distance from ego and calculate the position number: n
				List<UnionG> unionGs = new ArrayList<UnionG>();
				for (int i = 0; i < alreadyUnions.size(); i++) {
					UnionG ug0 = alreadyUnions.get(i);
					if (ug0.getDis() == u0.getDis()) {
						unionGs.add(ug0);
					}
				}
				if (unionGs.size() == 0) {
					for (int i = 0; i < alreadyUnions.size(); i++) {
						UnionG ug0 = alreadyUnions.get(i);
						if (u0.getDis() < ug0.getDis()) {
							n = i;
							break;
						} else {
							if (i == alreadyUnions.size() - 1) {
								n = i + 1;
								break;
							}
						}
					}
				} else if (unionGs.size() > 0) {
					for (int i = 0; i < unionGs.size(); i++) {
						UnionG unionG = unionGs.get(i);
						Object[] objs = getEffectiveUnion(unionG, u0);

						int index = this.getIndexFromAlreadyUms(g, unionG
								.getUnion().getId());
						if (objs == null) {
							String firstUid = u0.getPath()[0];
							KUUMember ku = kassService.getUnionMember(
									Integer.parseInt(firstUid), 0);
							if ("partner".equals(ku.getType())) {
								n = index + 1;
							} else {
								n = index;
								break;
							}
							break;
						} else {
							KUUnion kuunion = (KUUnion) objs[0];
							Integer kuIndex = (Integer) objs[1];
							Integer kuIndex2 = (Integer) objs[2];
							String pid = u0.getPath()[kuIndex + 1];
							KPPerson p1 = kassService.getPerson(Integer
									.parseInt(pid));
							KUUMember ku = kassService.getUnionMember(
									kuunion.getId(), Integer.parseInt(pid));
							if ("partner".equals(ku.getType())) {
								String pid2 = unionG.getPath()[kuIndex2 + 1];
								KUUMember ku2 = kassService
										.getUnionMember(kuunion.getId(),
												Integer.parseInt(pid2));
								if ("partner".equals(ku2.getType())) {
									if (p1.getSex() == 0) {
										n = index;
									} else {
										n = index + 1;
										break;
									}
									break;
								} else if ("offspring".equals(ku2.getType())) {
									n = index;
									break;
								}
							} else if ("offspring".equals(ku.getType())) {
								String pid2 = unionG.getPath()[kuIndex2 + 1];
								KUUMember ku2 = kassService
										.getUnionMember(kuunion.getId(),
												Integer.parseInt(pid2));
								if ("partner".equals(ku2.getType())) {
									n = index + 1;
								} else if ("offspring".equals(ku2.getType())) {
									String yob2 = kassService
											.getPersonBirthYear(Integer
													.parseInt(pid2));
									String yob1 = kassService
											.getPersonBirthYear(Integer
													.parseInt(pid));
									if (StringUtils.isBlank(yob1)
											|| StringUtils.isBlank(yob2)) {
										if (Integer.parseInt(pid) > Integer
												.parseInt(pid2)) {
											n = index + 1;
										} else {
											n = index;
											break;
										}
									} else {
										if (yob1.compareTo(yob2) > 0) {
											n = index + 1;
										} else {
											n = index;
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			this.addToAlreadyUms(g, u0, n);
			this.delFromUms(g, u0);
			if (n == 0) {
				u0.setXl(x0 + p);
				u0.setXr(u0.getXl() + 3 * p);
				u0.setX(x0 + 2 * p);
				u0.setY(2 * h * (s - m));
				List<KPPerson> parnters = this.getParnters(u0.getUnion()
						.getId());
				for (int i = 0; i < parnters.size(); i++) {
					KPPerson parnter = parnters.get(i);
					PersonG pg = this
							.getFromAlreadyPms(g + 1, parnter.getPid());
					if (pg == null) {
						pg = this.getFromPms(g + 1, parnter.getPid());
						if (pg == null) {
							continue;
						}
						if (parnter.getSex() == 0) {
							this.addToAlreadyPms(g + 1, pg, 0);
							pg.setX(x0 + p);
							pg.setY(2 * h * (s - m) - h);
						} else {
							List<PersonG> list = this.getAleadyPms().get(g + 1);
							if (list == null || list.size() == 0) {
								this.addToAlreadyPms(g + 1, pg, 0);
							} else {
								this.addToAlreadyPms(g + 1, pg, 1);
							}
							if (i == 0) {
								pg.setX(x0 + 3 * p);
								pg.setY(2 * h * (s - m) - h);
							} else {
								pg.setX(x0 + 3 * p);
								pg.setY(2 * h * (s - m) - h);
							}
						}
						this.delFromPms(g + 1, pg);
					}
				}
				this.moveUnionMethod1(u0, s, m);
			} else if (n > 0) {
				UnionG beforeUG = this.getFromAlreadyUmsByIndex(g, n - 1);
				int r = -1;
				List<KPPerson> list = this.getParnters(beforeUG.getUnion()
						.getId());
				for (int i = 0; i < list.size(); i++) {
					Integer pid = list.get(i).getPid();
					int indexTmp = this.getIndexFromAlreadyPms(g + 1, pid);
					if (indexTmp < 0) {
						list.remove(i);
					}
				}
				if (list.size() == 1) {
					Integer pid = list.get(0).getPid();
					r = this.getIndexFromAlreadyPms(g + 1, pid);
				} else if (list.size() == 2) {
					Integer pid1 = list.get(0).getPid();
					Integer pid2 = list.get(1).getPid();
					int index1 = this.getIndexFromAlreadyPms(g + 1, pid1);
					int index2 = this.getIndexFromAlreadyPms(g + 1, pid2);
					r = Math.max(index1, index2);
				}
				PersonG personG = this.getFromAlreadyPmsByIndex(g + 1, r);
				if (!this.hasParentUnion(personG.getPerson().getPid())) {
					if (personG.getX() < beforeUG.getXr()) {
						this.setUnionCoord1(u0, s, m);
						this.addPartnerMethod1(u0.getUnion().getId(), r, g, s,
								m);
					} else {
						this.setUnionCoord2(u0, personG, s, m);
						this.addPartnerMethod1(u0.getUnion().getId(), r, g, s,
								m);
					}
				}
				if (list.size() == 1
						&& this.hasParentUnion(list.get(0).getPid())) {
					Integer pid = list.get(0).getPid();
					PersonG pg = this.getFromAlreadyPms(g + 1, pid);
					if (pg != null) {
						Integer uid = this.getParentUnion(pid);// Parent-Union
						int rr = this.getRightOffspringIndex(uid, g + 1);
						int i = rr + 1;
						while (isChildParnter(g + 1, i, uid)) {
							int childUnionId = getChildParnter(g + 1, i, uid);
							UnionG ug = this.getFromAlreadyUms(g, childUnionId);
							PersonG p1 = this
									.getFromAlreadyPmsByIndex(g + 1, i);
							if (ug.getX() < beforeUG.getX()
									&& !this.hasParentUnion(p1.getPerson()
											.getPid())) {
								rr = i;
							}
							i = i + 1;
						}
						// If (Pm'[m,r].Xr+P<= Um'[m,n-1].Xr){
						r = rr;
						PersonG p1 = this.getFromAlreadyPmsByIndex(g + 1, rr);
						if (p1.getX() + p < beforeUG.getXr()) {
							this.setUnionCoord1(u0, s, m);
							this.addPartnerMethod1(u0.getUnion().getId(), r, g,
									s, m);
						} else {
							this.setUnionCoord2(u0, p1, s, m);
							this.addPartnerMethod1(u0.getUnion().getId(), r, g,
									s, m);
						}
					}
				}
				//
				if (list.size() == 2
						&& this.hasParentUnion(list.get(0).getPid())
						&& this.hasParentUnion(list.get(1).getPid())) {
					Integer pid1 = list.get(0).getPid();
					Integer pid2 = list.get(1).getPid();
					PersonG pg = this.getFromAlreadyPms(g + 1, pid1);
					if (pg != null) {
						Integer uid1 = this.getParentUnion(pid1);// Parent-Union
						int rr = this.getRightOffspringIndex(uid1, g + 1);
						int i = rr + 1;
						while (isChildParnter(g + 1, i, uid1)) {
							int childUnionId = getChildParnter(g + 1, i, uid1);
							UnionG ug = this.getFromAlreadyUms(g, childUnionId);
							PersonG p1 = this
									.getFromAlreadyPmsByIndex(g + 1, i);
							if (ug.getX() < beforeUG.getX()
									&& !this.hasParentUnion(p1.getPerson()
											.getPid())) {
								rr = i;
							}
							i = i + 1;
						}
						// PersonG pg2 = this.getFromAlreadyPms(g, pid2);
						Integer uid2 = this.getParentUnion(pid2);// Parent-Union
						UnionG ug2 = this.getFromAlreadyUms(g + 1, uid2);
						int rr2 = this.getRightOffspringIndex(uid2, g + 1);
						// PersonG rightPg = this.getFromAlreadyPmsByIndex(g,
						// rr2);
						// (Um'[m+1,b].X<=Um'[m,n-1].Xr+P){
						if (ug2.getX() <= beforeUG.getXr() + p) {
							// if(getOnlyOneChildUnion(uid2, pid2) == 1) {
							r = rr2;
							if (ug2.getXr() <= beforeUG.getXr()) {
								this.setUnionCoord1(u0, s, m);
								this.addPartnerMethod1(u0.getUnion().getId(),
										rr2, g, s, m);
							} else {
								this.setUnionCoord3(u0, ug2, s, m);
								this.addPartnerMethod1(u0.getUnion().getId(),
										rr2, g, s, m);
							}
							// } else if(getOnlyOneChildUnion(uid2, pid2) == 2)
							// {
							// r = rr;
							// }
						} else {
							r = rr;
						}
						if (r == rr) {
							PersonG p1 = this
									.getFromAlreadyPmsByIndex(g + 1, r);
							if (p1.getX() + p <= beforeUG.getXr()) {
								this.setUnionCoord1(u0, s, m);
								this.addPartnerMethod1(u0.getUnion().getId(),
										r, g, s, m);
							} else {
								this.setUnionCoord2(u0, p1, s, m);
							}
						}
					}
				}
				this.moveUnionMethod1(u0, s, m);
			}
			this.rangeUnionOffspring(u0.getUnion().getId(), g, s, m);
		}
	}

	private boolean isChildParnter(int g, int index, int uid) {
		if (index >= this.aleadyPms.size()) {
			return false;
		}
		PersonG pg = this.getFromAlreadyPmsByIndex(g, index);
		List<KPPerson> children = this.getChildren(uid);
		boolean flag = false;
		for (KPPerson child : children) {
			KUUnion u = kassService.getUnionByPid(pg.getPerson().getPid(),
					child.getPid());
			if (u != null) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	private Integer getChildParnter(int g, int index, int uid) {
		PersonG pg = this.getFromAlreadyPmsByIndex(g, index);
		List<KPPerson> children = this.getChildren(uid);
		for (KPPerson child : children) {
			KUUnion u = kassService.getUnionByPid(pg.getPerson().getPid(),
					child.getPid());
			if (u != null) {// 夫妻关系
				return u.getId();
			}
		}
		return -1;
	}

	private Object[] getEffectiveUnion(UnionG unionG, UnionG u0) {
		String id = "";
		int index = -1;
		int index2 = -1;
		boolean flag = false;
		for (int i = u0.getPath().length - 1; i >= 0; i = i - 2) {
			String uid = u0.getPath()[i];
			if (uid != null) {
				for (int j = unionG.getPath().length - 1; j >= 0; j = j - 2) {
					String uid2 = unionG.getPath()[j];
					if (uid2 == null) {
						continue;
					}
					if (StringUtils.equals(uid, uid2)) {
						id = uid;
						index = i;
						index2 = j;
						flag = true;
						break;
					}
				}
			}
			if (flag) {
				break;
			}
		}
		if (StringUtils.isNotBlank(id)) {
			return new Object[] { kassService.getUnion(Integer.parseInt(id)),
					index, index2 };
		}
		return null;
	}

	/**
	 * Arrange the child unions of Ego
	 * 
	 * @param s
	 * @param m
	 * @param g
	 */
	private void arrangeEgoChildU(int uid, int s, int m, int g) {
		KUUnion parentUnion = kassService.getPersonUnion(0, "offspring");
		int k = this.getRightOffspringIndex(parentUnion.getId(), g+1);
		k = k + 1;
		//System.out.println("uid: "+uid+" K: "+k);
		List<KUUnion> unions = kassService.getUnionByPid(0, "partner");
		List<PersonG> pgRemoves = new ArrayList<PersonG>();
		for (int i = 0; i < unions.size(); i++) {// ego's  child unions
			KUUnion kunion = unions.get(i);
			UnionG unionG = this.getFromUms(g, kunion.getId());
			this.addToAlreadyUms(g, unionG, i);

			if (i == 0) {
				unionG.setX(x0 + p);
				unionG.setXr(x0 + 4 * p);
				unionG.setXl(x0 + p);
				unionG.setY(2 * h * (s - m));

				// Get partners
				List<KPPerson> persons = this.getParnters(kunion.getId());
				for (KPPerson person : persons) {
					PersonG parnter = this.getFromPms(g + 1, person.getPid());
					if (parnter != null) {
						pgRemoves.add(parnter);
						this.addToAlreadyPms(g + 1, parnter, k);
						this.addPartnerMethod2(unionG, parnter, s, m);
					}
				}
				this.rangeUnionOffspring(unionG.getUnion().getId(), g, s, m);
				k = k + 1;
			} else {
				this.setUnionCoord1(unionG, s, m);
				List<KPPerson> persons = this.getParnters(kunion.getId());
				for (KPPerson person : persons) {
					PersonG parnter = this.getFromPms(g + 1, person.getPid());
					if (parnter != null) {
						pgRemoves.add(parnter);
						if (!this.hasParentUnion(parnter.getPerson().getPid())) {
							this.addToAlreadyPms(g + 1, parnter, k);
							this.addPartnerMethod3(unionG, parnter, s, m);
							this.moveUnionMethod2(unionG, parnter, s, m);
						}
					}
				}
				this.rangeUnionOffspring(unionG.getUnion().getId(), g, s, m);
				k = k + 1;
			}
			for (PersonG pg : pgRemoves) {
				this.delFromPms(pg.getG(), pg);
			}
			this.delFromUms(g, unionG);
		}
	}

	/**
	 * Arrange the child unions of offsprings in the last arranged generation
	 * (aleadyUms[g+1])
	 * 
	 * @param s
	 * @param m
	 * @param g
	 */
	private void arrangeUmOffspringU(int s, int m, int g) {
		List<UnionG> lastUnions = this.aleadyUms.get(g + 1);
		if (lastUnions != null && lastUnions.size() > 0) {
			for (int i = 0; i < lastUnions.size(); i++) {
				UnionG unionG = lastUnions.get(i);
				if (isEgoParentUnion(unionG)) {
					continue;
				}
				// Find the offsprings
				List<UnionG> up = new ArrayList<UnionG>();
				UnionG minDisUnionG = null;
				int minDis = 10000;
				List<KPPerson> offsprings = this.getChildren(unionG.getUnion()
						.getId());
				int d = offsprings.size();
				List<Integer> offspringIds = new ArrayList<Integer>();
				for (int c = 0; c < d; c++) {
					Integer pid = offsprings.get(c).getPid();
					int index = this.getIndexFromAlreadyPms(g + 1, pid);
					if (offspringIds.size() == 0) {
						offspringIds.add(pid);
					} else {
						boolean flag = false;
						for (int f = 0; f < offspringIds.size(); f++) {
							int index1 = this.getIndexFromAlreadyPms(g + 1,
									offspringIds.get(f));
							if (index < index1) {
								offspringIds.add(f, pid);
								flag = true;
								break;
							}
						}
						if (!flag) {
							offspringIds.add(pid);
						}
					}
				}
				for (int c = 0; c < d; c++) {
					Integer pid = offspringIds.get(c);
					List<KUUnion> list = kassService.getUnionByPid(pid,
							"partner");
					for (KUUnion ku : list) {
						UnionG ug = this.getFromUms(g, ku.getId());
						if (ug != null) {
							if (ug.getDis() < minDis) {
								minDis = ug.getDis();
								minDisUnionG = ug;
							}
							up.add(ug);
							this.delFromUms(g, ug);
						}
					}
				}
				if (minDisUnionG != null) {
					up.remove(minDisUnionG);
					up.add(0, minDisUnionG);
				}
				//
				int k = getRightOffspringIndex(unionG.getUnion().getId(), g + 1);
				k = k + 1;
				for (int b = 0; b < up.size(); b++) {
					UnionG ug = up.get(b);
					this.addToAlreadyUms(g, ug, -1);
					List<KUUMember> ms = ug.getUnion().getMembers();
					int personId = 0;
					for (KUUMember member : ms) {
						if (offspringIds.contains(member.getId())) {
							personId = member.getId();
							break;
						}
					}
					PersonG pg = this.getFromAlreadyPms(g + 1, personId);// s5
					int n = this.getIndexFromAlreadyUms(g, ug.getUnion()
							.getId());
					if (n == 0) {
						if (pg.getX() + p <= x0 + 2 * p) {
							ug.setX(x0 + 2 * p);
							ug.setY(2 * h * (s - m));
							ug.setXl(ug.getX() - p);
							ug.setXr(ug.getX() + 2 * p);
						} else {
							this.setUnionCoord4(ug, pg, s, m);
						}
						for (KUUMember member : ms) {
							if (member.getId() != personId) {
								PersonG otherPg = this.getFromPms(g + 1,
										member.getId());
								if (otherPg != null) {
									this.addToAlreadyPms(g + 1, otherPg, k);
									this.delFromPms(g + 1, otherPg);
									this.addPartnerMethod2(ug, otherPg, s, m);
									if (aleadyPms.get(g + 1).size() > k) {
										PersonG p = this
												.getFromAlreadyPmsByIndex(
														g + 1, k + 1);
										if (p != null
												&& p.getX() <= otherPg.getX()) {
											this.moveUnionMethod2(ug, otherPg,
													s, m);
										}
									}
									k = k + 1;
								}
							}
						}
					} else if (n > 0) {
						UnionG ug2 = this.getFromAlreadyUmsByIndex(g, n - 1);
						if (pg.getX() + p > ug2.getXr()) {
							setUnionCoord4(ug, pg, s, m);
						} else {
							setUnionCoord1(ug, s, m);
						}
						for (KUUMember member : ms) {
							if (member.getId() != personId) {
								PersonG otherPg = this.getFromPms(g + 1,
										member.getId());
								if (otherPg != null) {
									this.addToAlreadyPms(g + 1, otherPg, k);
									this.delFromPms(g + 1, otherPg);
									this.addPartnerMethod3(ug, otherPg, s, m);

									PersonG p = this.getFromAlreadyPmsByIndex(
											g + 1, k + 1);
									if (p != null && p.getX() <= otherPg.getX()) {
										this.moveUnionMethod2(ug, otherPg, s, m);
									}
									k = k + 1;
								}
							}
						}
					}
					this.delFromUms(g, ug);
					this.rangeUnionOffspring(ug.getUnion().getId(), g, s, m);
				}
			}
		}
	}

	private void setUnionCoord1(UnionG u, int s, int m) {
		int index = this.getIndexFromAlreadyUms(u.getG(), u.getUnion().getId());
		UnionG beforeUnion = this.getFromAlreadyUmsByIndex(u.getG(), index - 1);
		u.setX(beforeUnion.getXr() + p);
		u.setXl(u.getX() - p);
		u.setXr(u.getX() + 2 * p);
		u.setY(2 * h * (s - m));
	}

	private void setUnionCoord2(UnionG u, PersonG personG, int s, int m) {
		u.setX(personG.getX() + 2 * p);
		u.setXl(u.getX() - p);
		u.setXr(u.getX() + 2 * p);
		u.setY(2 * h * (s - m));
	}

	private void setUnionCoord3(UnionG u1, UnionG u2, int s, int m) {
		u1.setX(u2.getXr() + p);
		u1.setXl(u1.getX() - p);
		u1.setXr(u1.getX() + 2 * p);
		u1.setY(2 * h * (s - m));
	}

	private void setUnionCoord4(UnionG u, PersonG personG, int s, int m) {
		u.setX(personG.getX() + p);
		u.setXl(u.getX() - p);
		u.setXr(u.getX() + 2 * p);
		u.setY(2 * h * (s - m));
	}

	private void addPartnerMethod1(int uid, int r, int g, int s, int m) {
		List<KPPerson> list = this.getParnters(uid);
		UnionG ug = this.getFromAlreadyUms(g, uid);
		for (KPPerson person : list) {
			PersonG pg = this.getFromAlreadyPms(g + 1, person.getPid());
			if (pg == null) {
				pg = this.getFromPms(g + 1, person.getPid());
				if (person.getSex() == 0) {
					this.addToAlreadyPms(g + 1, pg, r + 1);
					pg.setX(ug.getX() - p);
					pg.setY(2 * h * (s - m) - h);
					r = r + 1;
				} else {
					this.addToAlreadyPms(g + 1, pg, r + 1);
					pg.setX(ug.getX() + p);
					pg.setY(2 * h * (s - m) - h);
				}
				this.delFromPms(g + 1, pg);
			}
		}
	}

	private void addPartnerMethod2(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPms(personG.getG(), personG.getPerson()
				.getPid());
		PersonG pg = getFromAlreadyPmsByIndex(personG.getG(), k - 1);
		if (u.getX() + p <= pg.getX()) {
			personG.setX(pg.getX() + p);
		} else {
			personG.setX(u.getX() + p);
		}
		personG.setY(2 * h * (s - m) - h);
	}

	private void addPartnerMethod3(UnionG u, PersonG personG, int s, int m) {
		int n = this.getIndexFromAlreadyUms(u.getG(), u.getUnion().getId());
		UnionG beforeU = this.getFromAlreadyUmsByIndex(u.getG(), n - 1);

		int k = this.getIndexFromAlreadyPms(personG.getG(), personG.getPerson()
				.getPid());
		PersonG beforeP = this.getFromAlreadyPmsByIndex(personG.getG(), k - 1);
		if (beforeU.getXr() >= beforeP.getX() + p) {
			personG.setX(u.getX() + p);
		} else {
			if (beforeP.getX() + p > u.getX()) {
				personG.setX(beforeP.getX() + p);
			} else {
				personG.setX(u.getX() + p);
			}
		}
		personG.setY(2 * h * (s - m) - h);

	}

	private void moveUnionMethod0(int m) {
		int a = this.range.getMaxG();
		int b = this.range.getMinG();
		int s = a - b + 1;
		for (Entry<Integer, List<UnionG>> entry : this.aleadyUms.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);// //根据代数就m行号
			if (i > m) {
				List<UnionG> list = entry.getValue();
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					thisUG.setX(thisUG.getX() + 1 * p);
					for (PersonG pg : thisUG.getChildrenPersons()) {
						PersonG personG = this.getFromAlreadyPms(g + 1, pg
								.getPerson().getPid());
						if (personG != null) {
							personG.setX(personG.getX() + 1 * p);
						}
					}
					for (PersonG pg : thisUG.getParentPersons()) {
						PersonG personG = this.getFromAlreadyPms(g + 1, pg
								.getPerson().getPid());
						if (personG != null) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0
									|| pg.getX() != null) {
								personG.setX(personG.getX() + 1 * p);
							}
						}
					}
				}
			}
		}
	}

	private void moveUnionMethod1(UnionG u, int s, int m) {
		int a = this.range.getMaxG();
		for (Entry<Integer, List<UnionG>> entry : this.aleadyUms.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);//
			if (i >= m) {
				List<UnionG> list = entry.getValue();
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					if (thisUG.getXl() >= u.getXl()
							&& thisUG.getUnion().getId() != u.getUnion()
									.getId()) {
						thisUG.setX(thisUG.getX() + 3 * p);
						thisUG.setXl(thisUG.getXl() + 3 * p);
						thisUG.setXr(thisUG.getXr() + 3 * p);
						for (PersonG pg : thisUG.getChildrenPersons()) {
							pg.setX(pg.getX() + 3 * p);
						}
						for (PersonG pg : thisUG.getParentPersons()) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0) {
								pg.setX(pg.getX() + 3 * p);
							}
						}
					}
				}
			}
		}
		int b = Math.abs(this.range.getMinG()) + 1;
		int cg = m - b;
		int n = getIndexFromAlreadyUms(cg, u.getUnion().getId());
		List<UnionG> cUnions = this.aleadyUms.get(cg);
		if (n < cUnions.size() - 1) {
			UnionG afterUG = this.getFromAlreadyUmsByIndex(cg, n + 1);
			if (afterUG.getXl() < u.getXl()) {
				double v = u.getX() - afterUG.getX() + 3 * p;
				afterUG.setX(afterUG.getX() + v);
				afterUG.setXl(afterUG.getXl() + v);
				afterUG.setXr(afterUG.getXr() + v);
				for (PersonG pg : afterUG.getChildrenPersons()) {
					pg.setX(pg.getX() + v);
				}
				for (PersonG pg : afterUG.getParentPersons()) {
					List<KUUnion> unions = kassService.getUnionByPid(pg
							.getPerson().getPid(), "offspring");
					if (unions == null || unions.size() == 0) {
						pg.setX(pg.getX() + v);
					}
				}
			}
		}
	}

	private void moveUnionMethod2(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPms(u.getG() + 1, personG.getPerson()
				.getPid());
		PersonG nextPG = getFromAlreadyPmsByIndex(personG.getG(), k + 1);
		if (nextPG == null) {
			return;
		}

		List<KUUnion> nextPunion = kassService.getUnionByPid(nextPG.getPerson()
				.getPid(), "offspring");
		int nextPuid = -1;
		if (nextPunion != null) {
			nextPuid = nextPunion.get(0).getId();
		}
		double v = personG.getX() - nextPG.getX() + p;
		double d = nextPG.getX();
		int a = this.range.getMaxG();

		for (Entry<Integer, List<UnionG>> entry : this.aleadyUms.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);//
			if (i >= m) {
				List<UnionG> list = entry.getValue();
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					KUUnion egoParentUnion = kassService.getPersonUnion(0,
							"offspring");
					if ((thisUG.getXl() >= d
							&& thisUG.getUnion().getId() != u.getUnion()
									.getId() && thisUG.getUnion().getId() != egoParentUnion
							.getId())
							|| (thisUG.getUnion().getId() == nextPuid)) {
						thisUG.setX(thisUG.getX() + v);
						thisUG.setXl(thisUG.getXl() + v);
						thisUG.setXr(thisUG.getXr() + v);
						for (PersonG pg : thisUG.getChildrenPersons()) {
							pg.setX(pg.getX() + v);
						}
						for (PersonG pg : thisUG.getParentPersons()) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0) {
								pg.setX(pg.getX() + v);
							}
						}
					}
				}
			}
		}
	}

	private void moveUnionMethod5(UnionG u, PersonG personG, int s, int m) {
		int k = this.getIndexFromAlreadyPms(u.getG(), personG.getPerson()
				.getPid());
		PersonG nextPG = getFromAlreadyPmsByIndex(personG.getG(), k + 1);
		if (nextPG == null) {
			return;
		}

		double v = personG.getX() - nextPG.getX() + p;
		double d = nextPG.getX();
		int a = this.range.getMaxG();

		for (Entry<Integer, List<UnionG>> entry : this.aleadyUms.entrySet()) {
			int g = entry.getKey();
			int i = s - (a - g);//
			if (i >= m) {
				List<UnionG> list = entry.getValue();
				KUUnion egoParentUnion = kassService.getPersonUnion(0,
						"offspring");
				for (int j = 0; j < list.size(); j++) {
					UnionG thisUG = list.get(j);
					if ((thisUG.getXl() >= d
							&& thisUG.getUnion().getId() != u.getUnion()
									.getId() && thisUG.getUnion().getId() != egoParentUnion
							.getId())) {
						thisUG.setX(thisUG.getX() + v);
						thisUG.setXl(thisUG.getXl() + v);
						thisUG.setXr(thisUG.getXr() + v);
						for (PersonG pg : thisUG.getChildrenPersons()) {
							pg.setX(pg.getX() + v);
						}
						for (PersonG pg : thisUG.getParentPersons()) {
							List<KUUnion> unions = kassService.getUnionByPid(pg
									.getPerson().getPid(), "offspring");
							if (unions == null || unions.size() == 0) {
								pg.setX(pg.getX() + v);
							}
						}
					}
				}
			}
		}
	}

	private void init() {
		ums = new TreeMap<Integer, List<UnionG>>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		aleadyUms = new TreeMap<Integer, List<UnionG>>(
				new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
		pms = new TreeMap<Integer, List<PersonG>>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		});
		aleadyPms = new TreeMap<Integer, List<PersonG>>(
				new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
	}

	private void fillData() {
		List<DUnion> dunions = range.getDunions();
		for (DUnion union : dunions) {
			boolean isMother = range.isMotherUnion(union);
			if (isMother) {
				List<UnionG> list = this.ums.get(union.getG());
				if (list == null) {
					list = new ArrayList<UnionG>();
					this.ums.put(union.getG(), list);
				}
				UnionG ug = new UnionG();
				ug.setDis(union.getDis());
				ug.setG(union.getG());
				ug.setUnion(kassService.getUnion(union.getUid()));
				ug.setPath(union.getPath());
				list.add(ug);
			}
		}
		// Sort
		for (Entry<Integer, List<UnionG>> entry : ums.entrySet()) {
			List<UnionG> list = entry.getValue();
			Collections.sort(list, new Comparator<UnionG>() {

				@Override
				public int compare(UnionG o1, UnionG o2) {
					if (o1.getDis() > o2.getDis()) {
						return 1;
					} else if (o1.getDis() < o2.getDis()) {
						return -1;
					} else if (o1.getDis() == o2.getDis()) {
						return o1.getUnion().getId() - o2.getUnion().getId();
					}
					return 0;
				}

			});
		}
		// Add Person
		for (Entry<Integer, List<UnionG>> entry : ums.entrySet()) {
			List<UnionG> list = entry.getValue();
			for (UnionG ug : list) {
				KUUnion union = ug.getUnion();
				List<KUUMember> members = union.getMembers();
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						DPerson dperson = range.getDPerson(member.getId());
						PersonG pg = this.getFromPms(dperson.getG(),
								member.getId());
						if (pg == null) {
							pg = new PersonG();
							pg.setDis(dperson.getDis());
							pg.setG(dperson.getG());
							pg.setPerson(kassService.getPerson(dperson.getPid()));
							ug.getParentPersons().add(pg);
							addToPms(pg);
						}
					} else {
						DPerson dperson = range.getDPerson(member.getId());

						PersonG pg = new PersonG();
						pg.setDis(dperson.getDis());
						pg.setG(dperson.getG());
						pg.setPerson(kassService.getPerson(dperson.getPid()));
						ug.getChildrenPersons().add(pg);
						pg.getUpUnions().add(ug);
						addToPms(pg);
					}
				}
			}
		}
	}

	private void addToPms(PersonG pg) {
		List<PersonG> persons = pms.get(pg.getG());
		if (persons == null) {
			persons = new ArrayList<PersonG>();
			pms.put(pg.getG(), persons);
		}
		persons.add(pg);
	}

	private PersonG getFromPms(int m, int pid) {
		List<PersonG> list = pms.get(m);
		if (list == null) {
			return null;
		}
		for (PersonG g : list) {
			if (g.getPerson().getPid() == pid) {
				return g;
			}
		}
		return null;
	}

	private PersonG getFromAlreadyPms(int g, int pid) {
		List<PersonG> list = this.aleadyPms.get(g);
		if (list == null) {
			return null;
		}
		for (PersonG pg : list) {
			if (pg.getPerson().getPid() == pid) {
				return pg;
			}
		}
		return null;
	}

	public int getIndexFromAlreadyPms(int g, int pid) {
		List<PersonG> list = this.aleadyPms.get(g);
		if (list == null) {
			return -1;
		}
		for (int i = 0; i < list.size(); i++) {
			PersonG p = list.get(i);
			if (p.getPerson().getPid() == pid) {
				return i;
			}
		}
		return -1;
	}

	private int getIndexFromAlreadyUms(int g, int uid) {
		List<UnionG> list = this.aleadyUms.get(g);
		if (list == null) {
			return -1;
		}
		for (int i = 0; i < list.size(); i++) {
			UnionG u = list.get(i);
			if (u.getUnion().getId() == uid) {
				return i;
			}
		}
		return -1;
	}

	private PersonG getFromAlreadyPmsByIndex(int g, int index) {
		List<PersonG> list = this.aleadyPms.get(g);
		if (list == null) {
			return null;
		}
		if (index >= list.size()) {
			return null;
		}
		return list.get(index);
	}

	private UnionG getFromAlreadyUmsByIndex(int g, int index) {
		List<UnionG> list = this.aleadyUms.get(g);
		if (list == null) {
			return null;
		}
		return list.get(index);
	}

	public UnionG getFromAlreadyUms(int g, int uid) {
		List<UnionG> list = this.aleadyUms.get(g);
		if (list == null) {
			return null;
		}
		for (int i = 0; i < list.size(); i++) {
			UnionG u = list.get(i);
			if (u.getUnion().getId() == uid) {
				return u;
			}
		}
		return null;
	}

	private UnionG getFromUms(int g, int uid) {
		List<UnionG> list = ums.get(g);
		for (UnionG ug : list) {
			if (ug.getUnion().getId() == uid) {
				return ug;
			}
		}
		return null;
	}

	private void addToAlreadyPms(int g, PersonG pg, int index) {
		List<PersonG> alreadyList = this.aleadyPms.get(g);
		if (alreadyList == null) {
			alreadyList = new ArrayList<PersonG>();
			aleadyPms.put(g, alreadyList);
		}
		if (index >= alreadyList.size()) {
			index = -1;
		}
		if (index == -1) {
			alreadyList.add(pg);
		} else {
			alreadyList.add(index, pg);
		}
	}

	private void delFromPms(int g, PersonG pg) {
		List<PersonG> list = pms.get(g);
		list.remove(pg);
	}

	private void addToAlreadyUms(int g, UnionG ug, int index) {
		List<UnionG> alreadyList = this.aleadyUms.get(g);
		if (alreadyList == null) {
			alreadyList = new ArrayList<UnionG>();
			aleadyUms.put(g, alreadyList);
		}
		if (index == -1) {
			alreadyList.add(ug);
		} else {
			alreadyList.add(index, ug);
		}
	}

	private void delFromUms(int g, UnionG ug) {
		List<UnionG> list = ums.get(g);
		list.remove(ug);
	}

	/**
	 * Check the parent Union
	 * 
	 * @param pid
	 * @return
	 */
	private boolean hasParentUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "offspring");
		if (unions == null || unions.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Check the child Union
	 * 
	 * @param pid
	 * @return
	 */
	private boolean hasChildUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "partner");
		if (unions == null || unions.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * Get the parent union id
	 * 
	 * @param pid
	 * @return
	 */
	private Integer getParentUnion(int pid) {
		List<KUUnion> unions = kassService.getUnionByPid(pid, "offspring");
		if (unions != null && unions.size() > 0) {
			return unions.get(0).getId();
		}
		return -1;
	}

	/**
	 * Check Ego's father
	 * 
	 * @param unionG
	 * @return
	 */
	private boolean isEgoParentUnion(UnionG unionG) {
		List<KUUnion> unions = kassService.getUnionByPid(0, "offspring");
		if (unions != null && unions.size() > 0) {
			for (KUUnion union : unions) {
				if (union.getId() == unionG.getUnion().getId()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get the right-most of offspring
	 * 
	 * @param uid
	 * @param g
	 */
	private int getRightOffspringIndex(int uid, int g) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		int k = -1;
		for (KUUMember member : members) {
			if ("offspring".equals(member.getType())) {
				int index = this.getIndexFromAlreadyPms(g, member.getId());
				if (k < index) {
					k = index;
				}
			}
		}
		if (k == -1) {
			k = 0;
		}
		return k;
	}

	/**
	 * Get the list of the partner unions
	 * 
	 * @param uid
	 * @return
	 */
	private List<KPPerson> getParnters(int uid) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		List<KPPerson> persons = new ArrayList<KPPerson>();
		for (KUUMember member : members) {
			if ("partner".equals(member.getType())) {
				persons.add(kassService.getPerson(member.getId()));
			}
		}
		Collections.sort(persons, new Comparator<KPPerson>() {

			@Override
			public int compare(KPPerson o1, KPPerson o2) {
				if (o1.getSex() == 0) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		return persons;
	}

	private List<KPPerson> getChildren(int uid) {
		KUUnion union = kassService.getUnion(uid);
		List<KUUMember> members = union.getMembers();
		List<KPPerson> persons = new ArrayList<KPPerson>();
		for (KUUMember member : members) {
			if ("offspring".equals(member.getType())) {
				persons.add(kassService.getPerson(member.getId()));
			}
		}
		return persons;
	}

	/**
	 * Get the Child-Union if just only one child union
	 * 
	 * @param uid
	 * @param pid
	 * @return
	 */
	private int getOnlyOneChildUnion(int uid, int pid) {
		List<KPPerson> list = this.getChildren(uid);
		int pidSize = 0;
		int othSize = 0;
		for (KPPerson p : list) {
			if (p.getPid() == pid && hasChildUnion(pid)) {
				pidSize++;
			}
			if (p.getPid() != pid && hasChildUnion(pid)) {
				othSize++;
			}
		}
		if (pidSize == 0 && othSize == 0) {
			return 0;
		} else if (pidSize == 1 && othSize == 0) {
			return 1;
		} else {
			return 2;
		}
	}

	public TreeMap<Integer, List<UnionG>> getUms() {
		return ums;
	}

	public TreeMap<Integer, List<UnionG>> getAleadyUms() {
		return aleadyUms;
	}

	public TreeMap<Integer, List<PersonG>> getAleadyPms() {
		return aleadyPms;
	}

	public Double getX0() {
		return x0;
	}

	public void setX0(Double x0) {
		this.x0 = x0;
	}

	public void printCoor() {
		for (Map.Entry<Integer, List<PersonG>> entry : this.aleadyPms
				.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			for (PersonG pg : gs) {
				System.out.println(key + "" + pg.getPerson().getPid() + " "
						+ pg.getX() + "," + pg.getY());
			}
		}
		System.out.println("==========printCoor");
	}

	public void print() {
		System.out.println("==========Mother U===========");
		for (Map.Entry<Integer, List<UnionG>> entry : ums.entrySet()) {
			String key = entry.getKey() + " : ";
			List<UnionG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Mother U'===========");
		for (Map.Entry<Integer, List<UnionG>> entry : this.aleadyUms.entrySet()) {
			String key = entry.getKey() + " : ";
			List<UnionG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Mother P===========");
		for (Map.Entry<Integer, List<PersonG>> entry : pms.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
		System.out.println("==========Mother P'===========");
		for (Map.Entry<Integer, List<PersonG>> entry : this.aleadyPms
				.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			System.out.println(key + " " + gs);
		}
	}

	public void printCoor(int g) {
		for (Map.Entry<Integer, List<PersonG>> entry : this.aleadyPms
				.entrySet()) {
			String key = entry.getKey() + " : ";
			List<PersonG> gs = entry.getValue();
			for (PersonG pg : gs) {
				System.out.println(key + "" + pg.getPerson().getPid() + " "
						+ pg.getX() + "," + pg.getY());
			}
		}
		System.out.println("==========printCoor===== " + g);
	}
}
