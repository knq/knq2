package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.KnqMain;
import com.fangda.kass.StartMain;
import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.Project;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.service.workspace.WorkSpaceService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.DefaultMenuBar;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of printing the diagram in the IMS
 * 
 * @author Fangfang Zhao, Shuang Ma, Jing Kong
 * 
 */
public class ProjectEditDialog extends BaseDialog implements MouseListener {

	private static final long serialVersionUID = 1L;
	protected ProjectService projectService;
	protected QuestionnaireService questionnaireService;
	JPanel panel;
	protected JTextField projectNameField;
	protected JTextField projectDirectoryField;

	protected JComboBox fieldLanguageBox;
	protected JComboBox fieldCountryBox;
	protected JTextField fieldProvinceField;
	protected JTextField fieldDistrictField;
	protected JTextField fieldCountyField;
	protected JTextField fieldTownField;
	protected JTextField fieldVillageField;
	protected JTextField fieldPlaceField;
	protected JTextField questionnaireField;
	protected JButton directoryButton;
	protected JButton pButton;

	public ProjectEditDialog(Map<String, Object> params) {
		super(params);
	}

	@Override
	public void initDialog() {

		super.initDialog();
		projectService = new ProjectService();
		questionnaireService = new QuestionnaireService();
	}

	@Override
	public JPanel createPanel() {
		projectNameField = new JTextField(50);
		projectDirectoryField = new JTextField(80);
		fieldLanguageBox = new JComboBox(QuestionConstants.listLanguage());
		fieldCountryBox = new JComboBox(QuestionConstants.listCountry(""));
		fieldProvinceField = new JTextField(10);
		fieldDistrictField = new JTextField(10);
		fieldCountyField = new JTextField(10);
		fieldTownField = new JTextField(10);
		fieldVillageField = new JTextField(10);
		fieldPlaceField = new JTextField(15);
		questionnaireField = new JTextField(85);

		projectDirectoryField.setEditable(false);
		questionnaireField.setEditable(false);
		projectDirectoryField.addMouseListener(this);
		questionnaireField.addMouseListener(this);

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel propPanel = new JPanel();
		propPanel.setBackground(Color.white);
		propPanel.setLayout(new BoxLayout(propPanel, BoxLayout.X_AXIS));
		propPanel.add(new JLabel("<html><strong>" + mxResources.get("project")
				+ "</strong></html>"));
		propPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(propPanel);

		GridBagLayout bagLayout = new GridBagLayout();
		JPanel projectPanel = new JPanel(bagLayout);
		projectPanel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();
		int index = 0;
		UIHelper.addToBagpanel(
				projectPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("projectTitle") + ":</HTML>"), c, 0,
				index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(projectPanel, Color.white, projectNameField, c,
				1, index, 1, 1, GridBagConstraints.WEST);
		index++;
		UIHelper.addToBagpanel(
				projectPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("projectDirectory") + ":</HTML>"), c,
				0, index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(projectPanel, Color.white,
				projectDirectoryField, c, 1, index, 1, 1,
				GridBagConstraints.WEST);
		directoryButton = new JButton("...");
		directoryButton.setPreferredSize(new Dimension(30, 20));
		directoryButton.addActionListener(this);
		UIHelper.addToBagpanel(projectPanel, Color.white, directoryButton, c,
				2, index, 1, 1, GridBagConstraints.WEST);
		panel.add(projectPanel);

		JPanel fieldPanel = new JPanel();
		fieldPanel.setBackground(Color.white);
		fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.X_AXIS));
		fieldPanel.add(new JLabel("<html><strong>"
				+ mxResources.get("fieldAddress") + "</strong></html>"));
		fieldPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(fieldPanel);

		JPanel fieldContentPanel = new JPanel(bagLayout);
		fieldContentPanel.setBackground(Color.white);
		int fieldIndex = 0;
		UIHelper.addToBagpanel(
				fieldContentPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("language") + ":</HTML>"), c, 0,
				fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldLanguageBox, c, 1, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldLanguageBox.addActionListener(this);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				"<HTML><font color='red'>*</font>" + mxResources.get("country")
						+ ":</HTML>"), c, 2, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, fieldCountryBox,
				c, 3, fieldIndex, 1, 1, GridBagConstraints.WEST);
		fieldIndex++;
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("province") + ":"), c, 0, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldProvinceField, c, 1, fieldIndex, 1, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("district") + ":"), c, 2, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldDistrictField, c, 3, fieldIndex, 1, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("county") + ":"), c, 4, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldCountyField, c, 5, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldIndex++;
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("town") + ":"), c, 0, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, fieldTownField,
				c, 1, fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("village") + ":"), c, 2, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldVillageField, c, 3, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldIndex++;
		UIHelper.addToBagpanel(
				fieldContentPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("placeName") + ":</HTML>"), c, 0,
				fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, fieldPlaceField,
				c, 1, fieldIndex, 2, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				mxResources.get("projectAbbr")), c, 3, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldIndex++;
		panel.add(fieldContentPanel);

		JPanel questionnairePanel = new JPanel();
		questionnairePanel.setBackground(Color.white);
		questionnairePanel.setLayout(new BoxLayout(questionnairePanel,
				BoxLayout.X_AXIS));
		questionnairePanel.add(new JLabel("<HTML><strong>"
				+ mxResources.get("questionnaire") + "</strong></html>"));
		questionnairePanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(questionnairePanel);

		JPanel qPanel = new JPanel(bagLayout);
		qPanel.setBackground(Color.white);
		int questionnaireIndex = 0;
		UIHelper.addToBagpanel(
				qPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("questionnaire") + ":</html>"), c, 0,
				questionnaireIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(qPanel, Color.white, questionnaireField, c, 1,
				questionnaireIndex, 1, 1, GridBagConstraints.WEST);
		pButton = new JButton("...");
		pButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		pButton.addActionListener(this);
		UIHelper.addToBagpanel(qPanel, Color.white, pButton, c, 2,
				questionnaireIndex, 1, 1, GridBagConstraints.WEST);
		panel.add(qPanel);

		return panel;
	}

	@Override
	public boolean beforeValid() {

		String projectDirectory = projectDirectoryField.getText();
		String questionnaireDirectory = questionnaireField.getText();
		String placeName = fieldPlaceField.getText();
		String projectName = projectNameField.getText();

		if ("".equals(projectDirectory) || "".equals(questionnaireDirectory)
				|| "".equals(placeName) || "".equals(projectName)) {
			JOptionPane.showMessageDialog(
					container,
					"<HTML><font color='red'>(*)</font>"
							+ mxResources.get("missInformation") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		Constants.QUESTIONNAIRE_XML = questionnaireDirectory;
		questionnaireService.get();
		if (!Constants.QUESTIONNAIRE.getStatus().equals("Formal")) {
			JOptionPane.showMessageDialog(panel,
					"<HTML>" + mxResources.get("FormalCheck") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	@Override
	public void afterSubmit() {

		super.afterSubmit();
		String op = (String) params.get("op");
		if ("add".equals(op)) {
			StartMain.getInstance().setVisible(false);
			KnqMain kmain = null;
			if (this.km == null) {
				kmain = new KnqMain();
				kmain.setLocation(100, 100);
				kmain.createFrame(new DefaultMenuBar(kmain), new JPanel())
						.setVisible(true);
			} else {
			}
			StartMain.getInstance().dispose();
		} else {

		}
	}

	@Override
	public void afterShow() {

		super.afterShow();
		String op = (String) params.get("op");
		if ("edit".equals(op)) {
			Project project = new Project();
			try {
				project = projectService.get();
			} catch (Exception e1) {
				System.err.println(" Open Project XML Error:  " + e1);
				JOptionPane.showMessageDialog(null,
						mxResources.get("openProjectXMLError"), "Error",
						JOptionPane.ERROR_MESSAGE);
				ProjectEditDialog.this.dispose();
			}
			projectNameField.setText(project.getTitle());
			projectDirectoryField.setText(project.getDirectory());

			fieldLanguageBox.setSelectedIndex(QuestionConstants
					.getLanguageIndex(project.getLan()));
			ComboItem filedItem = (ComboItem) fieldLanguageBox
					.getSelectedItem();
			// DefaultComboBoxModel model = (DefaultComboBoxModel)
			// fieldCountryBox.getModel();
			Object[] countryObjs = QuestionConstants.listCountry(filedItem
					.getValue());
			int i = 0;
			for (i = 0; i < countryObjs.length; i++) {
				ComboItem ci = (ComboItem) countryObjs[i];
				if (StringUtils.equalsIgnoreCase(ci.getName(),
						project.getCountry())) {
					break;
				}
			}
			fieldCountryBox.setSelectedIndex(i);
			fieldProvinceField.setText(project.getProvince());
			fieldDistrictField.setText(project.getDistrict());
			fieldCountyField.setText(project.getCounty());
			fieldTownField.setText(project.getTown());
			fieldVillageField.setText(project.getVillage());
			fieldPlaceField.setText(project.getPlaceName());
			questionnaireField.setText(project.getQuestionnaireDirectory());
			fieldPlaceField.setEditable(false);
		}
	}

	@Override
	public MessageModel submit() {

		String title = projectNameField.getText();
		String directory = projectDirectoryField.getText();
		ComboItem lanItem = (ComboItem) fieldLanguageBox.getSelectedItem();
		ComboItem countryItem = (ComboItem) fieldCountryBox.getSelectedItem();
		String province = fieldProvinceField.getText();
		String district = fieldDistrictField.getText();
		String county = fieldCountyField.getText();
		String town = fieldTownField.getText();
		String village = fieldVillageField.getText();
		String placeName = fieldPlaceField.getText();
		String questionnaireDir = questionnaireField.getText();
		Project project = null;
		if ("add".equals(params.get("op"))) {
			project = new Project();
		} else {
			project = projectService.get();
		}
		String questionaireDir = project.getQuestionnaireDirectory();
		project.setDirectory(directory);
		project.setDistrict(district);
		project.setCounty(county);
		project.setPlaceName(placeName);
		project.setProvince(province);
		project.setQuestionnaireDirectory(questionnaireDir);
		project.setVillage(village);
		project.setTown(town);
		project.setCountry(countryItem.getName());
		project.setLan(lanItem.getName());
		project.setTitle(title);
		if ("add".equals(params.get("op"))) {
			String projectPath = project.getDirectory() + File.separator
					+ placeName + ".project";
			projectService.saveFile(project, projectPath);
			Constants.PROJECT_XML = projectPath;
			projectService.get();
			Constants.QUESTIONNAIRE_XML = project.getQuestionnaireDirectory();
			questionnaireService.get();
			Preferences pref = Preferences.userRoot().node("project");
			int result = 0;
			try {
				for (int j = 0; j < pref.keys().length + 1; j++) {
					// A project has been opened
					if (projectPath.equals(pref.get(String.valueOf(j), ""))) {
						String tempPath = pref.get(String.valueOf(j), "");
						for (int k = j; k > 1; k--) {
							pref.put(String.valueOf(k),
									pref.get(String.valueOf(k - 1), ""));
						}
						pref.put(String.valueOf(1), tempPath);
						result = 1;
						break;
					}
				}
			} catch (BackingStoreException e4) {

				e4.printStackTrace();
			}
			int i;
			if (result == 0) {
				try {
					// The number of files in the list of the recent project
					if (pref.keys().length < 10) {
						for (i = pref.keys().length + 1; i > 1; i--) {
							pref.put(String.valueOf(i),
									pref.get(String.valueOf(i - 1), ""));
						}
						pref.put(String.valueOf(1), projectPath);
					} else {
						for (i = pref.keys().length; i > 1; i--) {
							pref.put(String.valueOf(i),
									pref.get(String.valueOf(i - 1), ""));
						}
						pref.put(String.valueOf(1), projectPath);
					}
				} catch (BackingStoreException e2) {

					e2.printStackTrace();
				}
			} else {
			}
			try {
				String path = Constants.getUserLogPath() + File.separator
						+ "projectLog.xml";
				FileOutputStream fos = new FileOutputStream(path);
				pref.exportNode(fos);
			} catch (Exception e3) {
				System.err.println(" Cannot export nodes:  " + e3);
			}
		} else {
			// String a = project.getQuestionnaireDirectory();
			// String b = questionaireDir;
			if (!project.getQuestionnaireDirectory().equals(questionaireDir)) {
				// JOptionPane.showMessageDialog(this,
				// mxResources.get("QuestionDirChange"));
				Constants.QUESTIONNAIRE_XML = project
						.getQuestionnaireDirectory();
				questionnaireService.get();
			}
			projectService.save();
		}
		return new MessageModel(true, "Submit success!");
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		super.actionPerformed(e);
		Object o = e.getSource();
		if (o == fieldLanguageBox) {
			ComboItem item = (ComboItem) fieldLanguageBox.getSelectedItem();
			DefaultComboBoxModel model = (DefaultComboBoxModel) fieldCountryBox
					.getModel();
			model.removeAllElements();
			Object[] obj = QuestionConstants.listCountry(item.getValue());
			for (int i = 0; i < obj.length; i++) {
				model.addElement(obj[i]);
			}
		} else if (o == directoryButton) {
			JFileChooser chooser = new JFileChooser(".");
			String defaultPath = WorkSpaceService.SetProjectDefaultDir();
			chooser.setCurrentDirectory(new File(defaultPath));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int result = chooser.showOpenDialog(getContentPane());
			if ("add".equals(params.get("op"))) {
				if (result == JFileChooser.APPROVE_OPTION) {
					projectDirectoryField.setText(chooser.getSelectedFile()
							.getAbsolutePath());
				}
			} else {
				JOptionPane.showMessageDialog(this,
						mxResources.get("projectDirChange"));
			}
		} else if (o == pButton) {
			JFileChooser chooser = new JFileChooser(".");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			String defaultPath = WorkSpaceService.SetQueDefaultDir();
			if (!questionnaireField.getText().equals("")) {
				defaultPath = questionnaireField.getText();
			}
			chooser.setCurrentDirectory(new File(defaultPath));
			int result = chooser.showOpenDialog(getContentPane());
			if (result == JFileChooser.APPROVE_OPTION) {
				questionnaireField.setText(chooser.getSelectedFile()
						.getAbsolutePath());
				// questionnaireField.setText("Questionnaire" + File.separator +
				// chooser.getSelectedFile().getName());
			}
			if (!"add".equals(params.get("op"))) {
				if (!defaultPath.equals(questionnaireField.getText())) {
					JOptionPane.showMessageDialog(this,
							mxResources.get("QuestionDirChange"));
				}
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		Object o = e.getSource();
		if (o == projectDirectoryField) {
			JFileChooser chooser = new JFileChooser(".");
			String defaultPath = WorkSpaceService.SetProjectDefaultDir();
			chooser.setCurrentDirectory(new File(defaultPath));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int result = chooser.showOpenDialog(getContentPane());
			if ("add".equals(params.get("op"))) {
				if (result == JFileChooser.APPROVE_OPTION) {
					projectDirectoryField.setText(chooser.getSelectedFile()
							.getAbsolutePath());
				}
			} else {
				JOptionPane.showMessageDialog(this,
						mxResources.get("projectDirChange"));
			}
		} else if (o == questionnaireField) {
			JFileChooser chooser = new JFileChooser(".");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			String defaultPath = WorkSpaceService.SetQueDefaultDir();
			if (!questionnaireField.getText().equals("")) {
				defaultPath = questionnaireField.getText();
			}
			chooser.setCurrentDirectory(new File(defaultPath));
			int result = chooser.showOpenDialog(getContentPane());
			if (result == JFileChooser.APPROVE_OPTION) {
				questionnaireField.setText(chooser.getSelectedFile()
						.getAbsolutePath());
			}
			if (!"add".equals(params.get("op"))) {
				if (!defaultPath.equals(questionnaireField.getText())) {
					JOptionPane.showMessageDialog(this,
							mxResources.get("QuestionDirChange"));
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

}
