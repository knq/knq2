package com.fangda.kass.ui.interview.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KPPCoords;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.interview.PageConfig;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DPerson;
import com.fangda.kass.ui.interview.graph.GraphCalerDistance.DUnion;
import com.fangda.kass.util.Constants;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxPoint;

/**
 * The Class for the auto arrange of the NKK diagram (the network kinship
 * diagram).
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class GraphAutoRangle {

	private static GraphAutoRangle AUTO_RANGLE = new GraphAutoRangle();
	private List<DUnion> dunions;
	private List<DPerson> dpersons;

	private GraphPanel graphPanel;

	private KassService kassService;
	GraphMotherRangle gmr = null;
	GraphFatherRangle gfr = null;

	private Double x0 = 300d;
	private Double y0 = 300d;

	private Integer maxG = 0;
	private Integer minG = 0;

	private GraphAutoRangle() {
		kassService = new KassService();
	}

	public static GraphAutoRangle getInstance() {
		return AUTO_RANGLE;
	}

	public void setGraghPanel(GraphPanel graphPanel) {
		this.graphPanel = graphPanel;
	}

	public void autoRange() {
		this.init();

	}

	private void init() {
		// Initialize
		GraphCalerDistance gcd = new GraphCalerDistance(kassService);
		gcd.init();
		gcd.calUGPGPathMain();
		dpersons = gcd.getPersons();
		dunions = gcd.getUnions();

		gmr = new GraphMotherRangle(this, kassService);
		gfr = new GraphFatherRangle(this, kassService, gmr);
		initRangle();
		PageConfig pageConfig = kassService.get().getPageConfig();
		int maxG = getMaxG();
		double h = (double) pageConfig.getCellHeight();
		mxPoint center = this.graphPanel.getCenter();
		x0 = center.getX();
		y0 = center.getY() - maxG * 2 * h;
		gmr.setX0(x0);
		gfr.setX0(x0);
		range();
	}

	private void initRangle() {
		gmr.initRangle();
		int ma = gmr.getUms().firstKey();
		int mb = gmr.getUms().lastKey();

		gfr.initRangle();
		int fa = gfr.getUfs().firstKey();
		int fb = gfr.getUfs().lastKey();
		maxG = Math.max(ma, fa);
		minG = Math.min(mb, fb);
	}

	private void range() {
		int a = getMaxG();
		int b = getMinG();
		int s = a - b + 1;// the total number of generations
		int m = s;
		for (int i = a; i >= b; i--) {
			int g = i;// the value of generation
			gmr.range1(g, s, m);
			gfr.range1(g, s, m);
			m--;
		}
		rangeM();
		rangeF();
		kassService.save();
		// gmr.print();
		// gfr.print();
	}

	/**
	 * Sort the ums
	 * 
	 * @param ums
	 */
	private void rangeM() {
		TreeMap<Integer, List<UnionG>> ums = gmr.getAleadyUms();
		TreeMap<Integer, List<PersonG>> pms = gmr.getAleadyPms();
		refresh(ums, pms);
	}

	private void rangeF() {
		TreeMap<Integer, List<UnionG>> ums = gfr.getAleadyUfs();
		TreeMap<Integer, List<PersonG>> pms = gfr.getAleadyPfs();
		refresh(ums, pms);
	}

	private void refresh(TreeMap<Integer, List<UnionG>> ums,
			TreeMap<Integer, List<PersonG>> pms) {
		for (Entry<Integer, List<UnionG>> entry : ums.entrySet()) {
			List<UnionG> gs = entry.getValue();
			for (UnionG g : gs) {
				if (g.getX() != null && g.getY() != null) {
					mxCell cell = graphPanel
							.findUnionById(g.getUnion().getId());
					cell.getGeometry().setX(g.getX());
					cell.getGeometry().setY(y0 + g.getY());
					KPPCoords co = new KPPCoords();
					co.setX(cell.getGeometry().getX());
					co.setY(cell.getGeometry().getY());
					g.getUnion().setCoords(co);
				}
			}
		}
		for (Entry<Integer, List<PersonG>> entry : pms.entrySet()) {
			List<PersonG> gs = entry.getValue();
			for (PersonG g : gs) {
				if (g.getX() != null) {
					mxCell cell = graphPanel.findPersonById(g.getPerson()
							.getPid());
					cell.getGeometry().setX(g.getX());
					cell.getGeometry().setY(y0 + g.getY());

					KPPCoords co = new KPPCoords();
					co.setX(cell.getGeometry().getX());
					co.setY(cell.getGeometry().getY());
					g.getPerson().setCoords(co);
				}
			}
		}
		graphPanel.graphComponent.getGraph().refresh();
	}

	/**
	 * Classify the unions ( ums ) : The set of unions on the left side of the
	 * NKK diagram, mostly maternal relatives.
	 * 
	 * @param union
	 * @return
	 */
	public boolean isMotherUnion(DUnion union) {
		int type = getUionType(union.getUid(), union.getPath());
		// Union Types on the left side of the NKK diagram
		if (type == 0 || type == 1 || type == 3 || type == 4) {
			return true;
		}
		return false;
	}

	/**
	 * Classify the unions ( ufs ) : on the right side of the NKK diagram,
	 * mostly paternal relatives
	 * 
	 * @param union
	 * @return
	 */
	public boolean isFatherUnion(DUnion union) {
		int type = getUionType(union.getUid(), union.getPath());
		// Union Types on the right side of the NKK diagram
		if (type == 1 || type == 2 || type == 5) {
			return true;
		}
		return false;
	}

	/**
	 * Union Type: 0 = the unions which ego is its partner. <br>
	 * 1 = the union which ego is its offspring. <br>
	 * 2 = the unions linked ego by ego's father. <br>
	 * 3 = the unions linked ego by ego's mother. <br>
	 * 4 = the unions linked ego by ego's children or ego's partners. <br>
	 * 5 = the unions linked ego by ego's siblings. <br>
	 * 
	 * @param uid
	 * @return
	 */
	public Integer getUionType(int uid, String[] path) {
		KUUMember kuu = kassService.getUnionMember(uid, 0);
		// 0 = the unions which ego is its partner.
		if (kuu != null && "partner".equals(kuu.getType())) {
			return 0;
		}
		// 1 = the union which ego is its offspring.
		if (kuu != null && "offspring".equals(kuu.getType())) {
			return 1;
		}
		Integer nearPersonId = Integer.parseInt(path[1]);
		// 2 = the unions linked ego by ego's father.
		List<Integer> mothers = this.getEgoParent(true);
		if (mothers.contains(nearPersonId)) {
			return 3;
		}
		// 3 = the unions linked ego by ego's mother.
		List<Integer> fathers = this.getEgoParent(false);
		if (fathers.contains(nearPersonId)) {
			return 2;
		}
		// 4 = the unions linked ego by ego's children or ego's partners.
		List<Integer> children = this.getEgoChildren();
		List<Integer> partners = this.getEgoPartners();
		if (children.contains(nearPersonId) || partners.contains(nearPersonId)) {
			return 4;
		}
		// 5 = the unions linked ego by ego's siblings.
		List<Integer> bs = this.getEgoBrothers();
		if (bs.contains(nearPersonId)) {
			return 5;
		}
		return -1;

	}

	/**
	 * Get ego's parent
	 * 
	 * @param isMother
	 * @return
	 */
	public List<Integer> getEgoParent(boolean isMother) {
		KASS kass = Constants.KASS;
		List<KUUnion> unions = kass.getUnions().getUnions();
		List<Integer> parents = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId().intValue() == 0
						&& "offspring".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						KPPerson person = kassService.getPerson(member.getId());
						if (person.getSex() == 0 && !isMother) {
							parents.add(member.getId());
						}
						if (person.getSex() == 1 && isMother) {
							parents.add(member.getId());
						}
					}
				}
			}
		}
		return parents;
	}

	/**
	 * Get Ego's children
	 * 
	 * @return
	 */
	public List<Integer> getEgoChildren() {
		List<KUUnion> unions = kassService.getUnionByPid(0);
		List<Integer> children = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == 0 && "partner".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if ("offspring".equals(member.getType())) {
						children.add(member.getId());
					}
				}
			}
		}
		return children;
	}

	public List<Integer> getEgoPartners() {
		List<KUUnion> unions = kassService.getUnionByPid(0);
		List<Integer> partners = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == 0 && "partner".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if ("partner".equals(member.getType())) {
						partners.add(member.getId());
					}
				}
			}
		}
		return partners;
	}

	public List<Integer> getEgoBrothers() {
		List<KUUnion> unions = kassService.getUnionByPid(0);
		List<Integer> bs = new ArrayList<Integer>();
		for (KUUnion union : unions) {
			List<KUUMember> members = union.getMembers();
			boolean flag = false;
			for (KUUMember member : members) {
				if (member.getId() == 0 && "offspring".equals(member.getType())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				for (KUUMember member : members) {
					if (member.getId() != 0
							&& "offspring".equals(member.getType())) {
						bs.add(member.getId());
					}
				}
			}
		}
		return bs;
	}

	DPerson getDPerson(int pid) {
		for (DPerson dp : this.dpersons) {
			if (dp.getPid() == pid) {
				return dp;
			}
		}
		return null;
	}

	public List<DUnion> getDunions() {
		return dunions;
	}

	public void setDunions(List<DUnion> dunions) {
		this.dunions = dunions;
	}

	public List<DPerson> getDpersons() {
		return dpersons;
	}

	public void setDpersons(List<DPerson> dpersons) {
		this.dpersons = dpersons;
	}

	public Double getX0() {
		return x0;
	}

	public Integer getMaxG() {
		return maxG;
	}

	public Integer getMinG() {
		return minG;
	}

	public static class UnionG {

		private KUUnion union;

		private Integer g;

		private Integer dis;

		private String[] path;

		private Double x;

		private Double y;

		private Double xl;

		private Double xr;

		private boolean isRange = false;
		// Parent
		private List<PersonG> parentPersons = new ArrayList<PersonG>();
		// children
		private List<PersonG> childrenPersons = new ArrayList<PersonG>();

		public KUUnion getUnion() {
			return union;
		}

		public void setUnion(KUUnion union) {
			this.union = union;
		}

		public Integer getG() {
			return g;
		}

		public void setG(Integer g) {
			this.g = g;
		}

		public Integer getDis() {
			return dis;
		}

		public void setDis(Integer dis) {
			this.dis = dis;
		}

		public Double getX() {
			return x;
		}

		public void setX(Double x) {
			this.x = x;
		}

		public Double getY() {
			return y;
		}

		public void setY(Double y) {
			this.y = y;
		}

		public Double getXl() {
			return xl;
		}

		public void setXl(Double xl) {
			this.xl = xl;
		}

		public Double getXr() {
			return xr;
		}

		public void setXr(Double xr) {
			this.xr = xr;
		}

		public boolean isRange() {
			return isRange;
		}

		public void setRange(boolean isRange) {
			this.isRange = isRange;
		}

		public List<PersonG> getParentPersons() {
			return parentPersons;
		}

		public void setParentPersons(List<PersonG> parentPersons) {
			this.parentPersons = parentPersons;
		}

		public List<PersonG> getChildrenPersons() {
			return childrenPersons;
		}

		public void setChildrenPersons(List<PersonG> childrenPersons) {
			this.childrenPersons = childrenPersons;
		}

		public String[] getPath() {
			return path;
		}

		public void setPath(String[] path) {
			this.path = path;
		}

		@Override
		public String toString() {
			return union.getId() + "";
		}
	}

	public static class PersonG {

		private KPPerson person;

		private Integer g;

		private Integer dis;

		private Double x;

		private Double y;

		// the Unions of the kinship path of a person
		private List<UnionG> upUnions = new ArrayList<UnionG>();

		public KPPerson getPerson() {
			return person;
		}

		public void setPerson(KPPerson person) {
			this.person = person;
		}

		public Integer getG() {
			return g;
		}

		public void setG(Integer g) {
			this.g = g;
		}

		public Integer getDis() {
			return dis;
		}

		public void setDis(Integer dis) {
			this.dis = dis;
		}

		public Double getX() {
			return x;
		}

		public void setX(Double x) {
			this.x = x;
		}

		public Double getY() {
			return y;
		}

		public void setY(Double y) {
			this.y = y;
		}

		public List<UnionG> getUpUnions() {
			return upUnions;
		}

		public void setUpUnions(List<UnionG> upUnions) {
			this.upUnions = upUnions;
		}

		@Override
		public String toString() {
			return person.getPid() + "";
		}
	}

}
