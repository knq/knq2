package com.fangda.kass.ui.interview.graph;

import com.fangda.kass.ui.interview.IvMainPanel;
import com.mxgraph.model.mxCell;

/**
 * The Class for the listener of the graph events
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphEventListener extends GraphListener {

	private IvMainPanel ivMainPanel;

	public GraphEventListener(IvMainPanel ivMainPanel) {
		this.ivMainPanel = ivMainPanel;
	}

	@Override
	public void afterAddedCell(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.addNewPerson(grapPanel, cell);
	}

	@Override
	public void afterDbClickCell(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.dbClickQuestion(grapPanel, cell);
	}

	@Override
	public void afterSingleClickCell(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.singleClickQuestion(grapPanel, cell);

	}

	@Override
	public void afterClickEmpty(GraphPanel grapPanel) {
		if (grapPanel.mode == 2) {

		} else {
			ivMainPanel.afterClickEmpty(grapPanel);
		}
	}

	public void afterSelectCell(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.afterSelectCell(grapPanel, cell);
	}

	public void afterUnSelectCell(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.afterUnSelectCell(grapPanel, cell);
	}

	@Override
	public void clickOnViewModel(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.clickOnViewModel(grapPanel, cell);
	}

	@Override
	public void pressedOnViewModel(GraphPanel grapPanel, mxCell cell) {
		ivMainPanel.pressedOnViewModel(grapPanel, cell);
	}

}
