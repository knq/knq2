package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.common.MultiLabel;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.graph.GraphElement;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.lowagie.text.Font;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;

/**
 * This class for the list of question in the IMS
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * 
 */
public class QuestionPanel extends JPanel {

	/**
	 *  The panel of the questions for the section 4-7
	 */
	private static final long serialVersionUID = -2934431201610058118L;
	protected IvMainPanel ivMainPanel;
	protected Question question;
	protected QuestionnaireService questionnaireService;
	public String curqid;
	public QuestionPanelPage paelq;
	public QuestionPanelPageBtn pael2;
	public GraphPanel graphPanel;
	public GraphPanel ographPanel;
	public MultiLabel multiLabel;

	public QuestionPanel(IvMainPanel ivMainPanel, Question question,
			GraphPanel graphPanel, GraphPanel ographPanel) {
		questionnaireService = new QuestionnaireService();
		this.ivMainPanel = ivMainPanel;
		this.question = question;
		this.graphPanel = graphPanel;
		this.ographPanel = ographPanel;
		this.initPanel();
	}

	public void initPanel() {
		this.setBackground(Color.white);
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		this.setVisible(true);
		this.setLayout(new VFlowLayout(VFlowLayout.TOP, 0, 0, true, false));
		createCont();
		
	}

	public void createCont() {
		
		curqid = question.getQid();
		int width = ivMainPanel.leftQuestionPanel.getWidth() - 30;
		multiLabel = new MultiLabel(question.getLead(), Font.BOLD, 13);
		multiLabel.resize(width);
		this.add(multiLabel.getLabel());

		paelq = new QuestionPanelPage(ivMainPanel, question);
		pael2 = new QuestionPanelPageBtn(ivMainPanel, this, question, paelq);
		this.add(pael2);
		this.add(paelq);
		
		this.addComponentListener(new ComponentListener() {

			@Override
			public void componentResized(ComponentEvent e) {
				JPanel panel = (JPanel) e.getSource();
				multiLabel.resize(panel.getWidth() - 30);
				
				paelq.resize(panel.getWidth() - 30);
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				
			}

			@Override
			public void componentShown(ComponentEvent e) {
				
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				
			}
			
		});
	}

	public void updateNextCont() {
		updateCont(ivMainPanel.questionnaireService.getNextQuestion(curqid));
	}

	public boolean ifSave() {
		boolean flag = false;
		KQQuestion kqquestion = paelq.getKQuestionbyqid(curqid);
		if (kqquestion != null && kqquestion.getValue().equals("OK")) {
			flag = true;
		}
		return flag;
	}

	public void updateCont(Question qst) {
		curqid = qst.getQid();
		updateLeadDetail(qst);
		paelq.upDatePageQuestion(qst);
	}

	/**
	 * Setting the label as the detail or the lead
	 * 
	 * @param qst
	 */
	public void updateLeadDetail(Question qst) {
		int width = ivMainPanel.leftQuestionPanel.getWidth() - 50;
		if (pael2.followupAnswerState.equals("1")) {
			multiLabel.update(qst.getDetail(), width);
		} else {
			multiLabel.update(qst.getLead(), width);
		}
	}

	public void updatepreCont() {
		updateCont(ivMainPanel.questionnaireService.getPrevQuestion(curqid));
	}

	public void refresh() {
		this.validate();
		this.repaint();
	}

	public void setCell(mxCell cell, GraphPanel gPanel) {
		if (cell != null) {
			if (cell.getValue() instanceof GraphElement) {
			}
			if (ivMainPanel.graphPanel.isSelectModel(cell)) {
				Object[] e1 = { "Yes", "No" };
				int result = JOptionPane.showOptionDialog(null,
						mxResources.get("removeQuestionTip"), "Select Option",
						JOptionPane.DEFAULT_OPTION,
						JOptionPane.WARNING_MESSAGE, null, e1, e1[0]);
				if (result == JOptionPane.YES_OPTION) {
					ivMainPanel.graphPanel.setDefaultColor(cell);
				} else if (result == JOptionPane.NO_OPTION) {
					System.out.println("No");
				}
			} else {
				pael2.singleClickQuestion(cell, gPanel);
			}
		} else {

		}

	}

	public void clearAllValue() {
		paelq.ClearOporationbyperson();
	}

	public void dbClickQuestion(mxCell cell) {
		paelq.dbClickQuestion(cell);
	}

	public void saveSelectPerson(GraphPanel grapPanel) {

	}

	public void afterSelectCell(GraphPanel grapPanel) {
		pael2.afterSelectCell(grapPanel);
	}

	public void afterUnSelectCell(GraphPanel grapPanel) {
		pael2.afterUnSelectCell(grapPanel);
	}
}
