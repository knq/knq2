package com.fangda.kass.ui.interview.project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.ConfigUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of setting the user directory in the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public class UserPathDialog extends BaseDialog implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 270393693071889423L;

	private JPanel panel;

	protected JButton directoryButton;

	protected JTextField directoryField;

	@Override
	public void initDialog() {
		super.initDialog();

	}

	@Override
	public JPanel createPanel() {

		directoryField = new JTextField(30);
		directoryField.setEditable(false);

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		GridBagLayout bagLayout = new GridBagLayout();
		JPanel projectPanel = new JPanel(bagLayout);
		projectPanel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();
		int index = 0;
		UIHelper.addToBagpanel(
				projectPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("select.user.directory") + ":</HTML>"),
				c, 0, index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(projectPanel, Color.white, directoryField, c, 1,
				index, 1, 1, GridBagConstraints.WEST);
		directoryButton = new JButton("...");
		directoryButton.setPreferredSize(new Dimension(30, 20));
		directoryButton.addActionListener(this);
		directoryButton.addMouseListener(this);
		UIHelper.addToBagpanel(projectPanel, Color.white, directoryButton, c,
				2, index, 1, 1, GridBagConstraints.WEST);
		panel.add(projectPanel);

		return panel;
	}

	@Override
	protected void afterInit() {
		super.afterInit();
		sureButton.setText("OK");
		closeButton.setText("Cancel");
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		JFileChooser chooser = new JFileChooser(".");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int result = chooser.showOpenDialog(getContentPane());
		if (result == JFileChooser.APPROVE_OPTION) {

			directoryField.setText(chooser.getSelectedFile().getAbsolutePath());
		}
	}

	@Override
	public boolean beforeValid() {
		String directory = directoryField.getText();
		if (StringUtils.isBlank(directory)) {
			JOptionPane
					.showMessageDialog(
							container,
							"<HTML><font color='red'>(*)</font>User Path must not be empty</HTML>",
							"Warning", JOptionPane.INFORMATION_MESSAGE,
							UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	@Override
	public MessageModel submit() {
		Constants.setUserPath(directoryField.getText());
		ConfigUtil.set("path", directoryField.getText());
		try {
			FileUtil.copyDirectory("language", Constants.getUserPath()
					+ File.separator + "language");
			FileUtil.copyDirectory("Questionnaire", Constants.getUserPath()
					+ File.separator + "Questionnaire");
			File dataFile = new File(Constants.getUserPath() + File.separator
					+ "data");
			if (!dataFile.exists()) {
				dataFile.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new MessageModel(true, "Submit success!");
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}
}
