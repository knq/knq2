package com.fangda.kass.ui.interview.graph;

import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.interview.KUUnion;

/**
 * This class for the some information of the union icons.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphUnion extends GraphElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1253351339631108348L;
	private List<GraphUnionOne> list = new ArrayList<GraphUnionOne>();
	/**
	 * The Indicator for the location of the unions . 1 = the unions belong to
	 * other important, 0 = the unions belong to NKK
	 */
	private int isOther;

	private KUUnion kuunion;

	public List<GraphUnionOne> getList() {
		return list;
	}

	public void setList(List<GraphUnionOne> list) {
		this.list = list;
	}

	public String toString() {
		return "   U:" + this.getId();
	}

	public int getIsOther() {
		return isOther;
	}

	public void setIsOther(int isOther) {
		this.isOther = isOther;
	}

	public KUUnion getKuunion() {
		return kuunion;
	}

	public void setKuunion(KUUnion kuunion) {
		this.kuunion = kuunion;
	}
}
