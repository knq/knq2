package com.fangda.kass.ui.interview;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KFieldSite;
import com.fangda.kass.model.interview.KInterviewInfo;
import com.fangda.kass.model.interview.Project;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.service.workspace.WorkSpaceService;
import com.fangda.kass.ui.common.DefaultFileFilter;
import com.fangda.kass.ui.interview.graph.CustomGraph;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.ui.interview.project.InterviewEditDialog;
import com.fangda.kass.ui.interview.project.InterviewLogDialog;
import com.fangda.kass.ui.interview.project.MoveDialog;
import com.fangda.kass.ui.interview.project.PageSetupDialog;
import com.fangda.kass.ui.interview.project.PrintViewDialog;
import com.fangda.kass.ui.interview.project.ProjectEditDialog;
import com.fangda.kass.util.Constants;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUndoManager;

/**
 * This class for the file actions in the KNQ2
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class IvAction {
	protected KassService kassService;

	/**
	 * New a Project
	 * 
	 * @author Shuang Ma
	 * 
	 */
	public static class NewProjectAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6433743717343406902L;

		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				kassService.isProjectNullValid(km, 0);
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(km, 0);
				}
				if (Constants.PROJECT_XML == null
						|| Constants.PROJECT_XML == "") {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "add");
					new ProjectEditDialog(map).setMainPanel(km);
				}
			}
		}
	}

	/**
	 * New an Interview
	 * 
	 * @author Fangfang Zhao
	 */
	public static class NewInterviewPropEditAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 46882421380260667L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML == null
						|| Constants.PROJECT_XML == "") {
					kassService.isProjectNullValid(km, 1);
				} else {
					if (Constants.QUESTIONNAIRE_XML != null
							&& Constants.QUESTIONNAIRE_XML
									.contains(Constants.TMP_EXT)) {
						QuestionnaireService questionnaireService = new QuestionnaireService();
						questionnaireService.isQuestionnaireNullValid(km, 0);
					}

					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "add");
					new InterviewEditDialog(map).setMainPanel(km);

					km.menuBar.getMenu(0).getItem(6).removeAll();
					JMenu submenu = (JMenu) km.menuBar.getMenu(0).getItem(6);
					List<String> recentInterviewPathList = WorkSpaceService
							.getRecentInterviewPathList();
					// Collections.reverse(recentProjectPathList);
					for (int j = 0; j < recentInterviewPathList.size(); j++) {
						submenu.add(km.bind(recentInterviewPathList.get(j),
								new OpenRecentInterviewAction(),
								"/images/open.gif"));
					}
					km.frame.setJMenuBar(km.menuBar);// menu
					km.frame.getJMenuBar();
				}
			}
		}
	}

	/**
	 * Open a Project
	 * 
	 * @author Shuang Ma
	 */
	public static class OpenProjectAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5384037431486163319L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.isInterviewCancel == 1) {
				} else {
					if (Constants.QUESTIONNAIRE_XML != null
							&& Constants.QUESTIONNAIRE_XML
									.contains(Constants.TMP_EXT)) {
						QuestionnaireService questionnaireService = new QuestionnaireService();
						questionnaireService.isQuestionnaireNullValid(km, 0);
					}
					JFileChooser chooser = new JFileChooser(".");
					String defaultPath = WorkSpaceService
							.SetProjectDefaultDir();
					chooser.setCurrentDirectory(new File(defaultPath));
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "project");
					int i = chooser.showOpenDialog(km.getRootPane());
					if (i == JFileChooser.APPROVE_OPTION) {
						if (km.ivMainPanel != null) {

							km.ivMainPanel.removeAll();
							km.ivMainPanel.repaint();
							km.setMainPanel(km.ivMainPanel);
							km.qnMainPanel = null;
						} else {
						}
						String path = chooser.getSelectedFile()
								.getAbsolutePath();
						String parentPath = chooser.getSelectedFile()
								.getParent();
						Constants.KASS = null;
						Constants.KASS_XML = null;
						Constants.PROJECT_XML = path;
						Constants.PROJECT = null;
						ProjectService projectService = new ProjectService();
						try {
							projectService.get();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openProjectXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}
						Project project = projectService.get();
						Constants.QUESTIONNAIRE = null;
						Constants.QUESTIONNAIRE_XML = project
								.getQuestionnaireDirectory();
						QuestionnaireService questionnaireService = new QuestionnaireService();
						questionnaireService.get();

						project.setDirectory(parentPath);
						String projectPath = project.getDirectory()
								+ File.separator + project.getPlaceName()
								+ ".project";
						projectService.saveFile(project, projectPath);
						// km.setMainPanel(km.ivMainPanel);

						Preferences pref = Preferences.userRoot().node(
								"project");
						File file = chooser.getSelectedFile(); // 得到选择的文件
						int result = 0;
						try {
							for (int j = 0; j < pref.keys().length + 1; j++) {
								// 打开已打开过的项目文件时
								if (file.getPath().equals(
										pref.get(String.valueOf(j), ""))) {
									String tempPath = pref.get(
											String.valueOf(j), "");
									for (int k = j; k > 1; k--) {
										pref.put(String.valueOf(k), pref.get(
												String.valueOf(k - 1), ""));
									}
									pref.put(String.valueOf(1), tempPath);
									result = 1;
									break;
								}
							}
						} catch (BackingStoreException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openProjectXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}
						if (result == 0) {
							try {
								// The number of the file list of the recent
								// projects
								if (pref.keys().length < 10) {
									for (i = pref.keys().length + 1; i > 1; i--) {
										pref.put(String.valueOf(i), pref.get(
												String.valueOf(i - 1), ""));
									}
									pref.put(String.valueOf(1), file.getPath());
								} else {
									for (i = pref.keys().length; i > 1; i--) {
										pref.put(String.valueOf(i), pref.get(
												String.valueOf(i - 1), ""));
									}
									pref.put(String.valueOf(1), file.getPath());
								}
							} catch (BackingStoreException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
								JOptionPane.showMessageDialog(null,
										mxResources.get("openProjectXMLError"),
										"Error", JOptionPane.ERROR_MESSAGE);

							}
						} else {
						}
						try {
							String p = Constants.getUserLogPath()
									+ File.separator + "projectLog.xml";
							FileOutputStream fos = new FileOutputStream(p);
							pref.exportNode(fos);
						} catch (Exception e1) {
							System.err.println(" Cannot export nodes:  " + e1);
							JOptionPane.showMessageDialog(null,
									mxResources.get("saveProjectLogError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}

						km.menuBar.getMenu(0).getItem(3).removeAll();
						JMenu submenu = (JMenu) km.menuBar.getMenu(0)
								.getItem(3);
						List<String> recentProjectPathList = WorkSpaceService
								.getRecentProjectPathList();
						// Collections.reverse(recentProjectPathList);
						for (int j = 0; j < recentProjectPathList.size(); j++) {
							submenu.add(km.bind(recentProjectPathList.get(j),
									new OpenRecentProjectAction(),
									"/images/open.gif"));
						}
						km.frame.setJMenuBar(km.menuBar);// menu
						km.frame.getJMenuBar();
						km.setTitle(null);
					}
				}
			}
		}
	}

	/**
	 * Open a Recent Project
	 * 
	 * @author Shuang Ma
	 */
	public static class OpenRecentProjectAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3899016336425124568L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = null;
			km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(km, 0);
				}
				String projectPath = e.getActionCommand();
				if (km.ivMainPanel != null) {
					km.ivMainPanel.removeAll();
					km.ivMainPanel.repaint();
					km.setMainPanel(km.ivMainPanel);
				} else {
				}

				Constants.KASS = null;
				Constants.KASS_XML = null;
				Constants.PROJECT_XML = projectPath;
				Constants.PROJECT = null;
				ProjectService projectService = new ProjectService();
				try {
					Project project = new Project();
					project = projectService.get();
					Constants.QUESTIONNAIRE = null;
					Constants.QUESTIONNAIRE_XML = project
							.getQuestionnaireDirectory();
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.get();
				}

				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("openProjectXMLError"), "Error",
							JOptionPane.ERROR_MESSAGE);

				}

				Preferences pref = Preferences.userRoot().node("project");
				int result = 0;
				try {
					for (int j = 0; j < pref.keys().length + 1; j++) {
						if (projectPath.equals(pref.get(String.valueOf(j), ""))) {
							String tempPath = pref.get(String.valueOf(j), "");
							for (int k = j; k > 1; k--) {
								pref.put(String.valueOf(k),
										pref.get(String.valueOf(k - 1), ""));
							}
							pref.put(String.valueOf(1), tempPath);
							result = 1;
							break;
						}
					}
				} catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("openProjectXMLError"), "Error",
							JOptionPane.ERROR_MESSAGE);

				}
				int i;
				if (result == 0) {
					try {
						if (pref.keys().length < 10) {
							for (i = pref.keys().length + 1; i > 1; i--) {
								pref.put(String.valueOf(i),
										pref.get(String.valueOf(i - 1), ""));
							}
							pref.put(String.valueOf(1), projectPath);
						} else {
							for (i = pref.keys().length; i > 1; i--) {
								pref.put(String.valueOf(i),
										pref.get(String.valueOf(i - 1), ""));
							}
							pref.put(String.valueOf(1), projectPath);
						}
					} catch (BackingStoreException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								mxResources.get("openProjectXMLError"),
								"Error", JOptionPane.ERROR_MESSAGE);

					}
				} else {
				}
				try {
					String p = Constants.getUserLogPath() + File.separator
							+ "projectLog.xml";
					FileOutputStream fos = new FileOutputStream(p);
					pref.exportNode(fos);
				} catch (Exception e1) {
					System.err.println(" Cannot export nodes:  " + e1);
					JOptionPane.showMessageDialog(null,
							mxResources.get("saveProjectLogError"), "Error",
							JOptionPane.ERROR_MESSAGE);

				}
				km.menuBar.getMenu(0).getItem(3).removeAll();
				JMenu submenu = (JMenu) km.menuBar.getMenu(0).getItem(3);
				List<String> recentProjectPathList = WorkSpaceService
						.getRecentProjectPathList();
				// Collections.reverse(recentProjectPathList);
				for (int j = 0; j < recentProjectPathList.size(); j++) {
					submenu.add(km.bind(recentProjectPathList.get(j),
							new OpenRecentProjectAction(), "/images/open.gif"));
				}
				km.frame.setJMenuBar(km.menuBar);// menu
				km.frame.getJMenuBar();
				km.setTitle(null);
				km.qnMainPanel = null;
			}
		}

	}

	/**
	 * Open a Recent Interview
	 * 
	 * @author Shuang Ma
	 */
	public static class OpenRecentInterviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 397408427050070539L;

		@Override
		public void actionPerformed(ActionEvent e) {

			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(km, 0);
				} else {

					String interviewPath = e.getActionCommand();
					Constants.KASS = null;
					Constants.KASS_XML = interviewPath;
					if (Constants.PROJECT_XML == null) {
						JOptionPane.showMessageDialog(null,
								mxResources.get("openInterviewError"), "Error",
								JOptionPane.ERROR_MESSAGE);
					}

					String tmpPath = kassService.openRealXmlFile(interviewPath);
					Constants.KASS_XML = tmpPath;
					try {
						kassService.get();
					}

					catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								mxResources.get("openInterviewXMLError"),
								"Error", JOptionPane.ERROR_MESSAGE);

					}

					KASS kass = new KASS();
					kass = kassService.get();
					KFieldSite fieldSite = new KFieldSite();
					fieldSite = kass.getFieldSite();
					Constants.PROJECT = null;
					Constants.PROJECT_XML = fieldSite.getProjectDirector()
							+ File.separator + fieldSite.getPlaceName()
							+ ".project";
					ProjectService projectService = new ProjectService();
					Project project = new Project();
					project = projectService.get();
					Constants.QUESTIONNAIRE = null;
					Constants.QUESTIONNAIRE_XML = project
							.getQuestionnaireDirectory();
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.get();

					try {
						km.ivMainPanel = new IvMainPanel(km);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								mxResources.get("openInterviewXMLError"),
								"Error", JOptionPane.ERROR_MESSAGE);

					}
					km.setMainPanel(km.ivMainPanel);

					Preferences pref = Preferences.userRoot().node("interview");
					int result = 0;
					try {
						for (int j = 0; j < pref.keys().length + 1; j++) {
							if (interviewPath.equals(pref.get(
									String.valueOf(j), ""))) {
								String tempPath = pref.get(String.valueOf(j),
										"");
								for (int k = j; k > 1; k--) {
									pref.put(String.valueOf(k),
											pref.get(String.valueOf(k - 1), ""));
								}
								pref.put(String.valueOf(1), tempPath);
								result = 1;
								break;
							}
						}
					} catch (BackingStoreException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								mxResources.get("openInterviewXMLError"),
								"Error", JOptionPane.ERROR_MESSAGE);

					}
					int i;
					if (result == 0) {
						try {
							if (pref.keys().length < 10) {
								for (i = pref.keys().length + 1; i > 1; i--) {
									pref.put(String.valueOf(i),
											pref.get(String.valueOf(i - 1), ""));
								}
								pref.put(String.valueOf(1), interviewPath);
							} else {
								for (i = pref.keys().length; i > 1; i--) {
									pref.put(String.valueOf(i),
											pref.get(String.valueOf(i - 1), ""));
								}
								pref.put(String.valueOf(1), interviewPath);
							}
						} catch (BackingStoreException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}
					} else {
					}
					try {
						String path = Constants.getUserLogPath()
								+ File.separator + "interviewLog.xml";
						FileOutputStream fos = new FileOutputStream(path);
						pref.exportSubtree(fos);
					} catch (Exception e1) {
						System.err.println(" Cannot export nodes:  " + e1);
						JOptionPane.showMessageDialog(null,
								mxResources.get("saveInterviewLogError"),
								"Error", JOptionPane.ERROR_MESSAGE);

					}

					km.menuBar.getMenu(0).getItem(6).removeAll();
					JMenu submenu = (JMenu) km.menuBar.getMenu(0).getItem(6);
					List<String> recentInterviewPathList = WorkSpaceService
							.getRecentInterviewPathList();
					// Collections.reverse(recentProjectPathList);
					for (int j = 0; j < recentInterviewPathList.size(); j++) {
						submenu.add(km.bind(recentInterviewPathList.get(j),
								new OpenRecentInterviewAction(),
								"/images/open.gif"));
					}
					km.frame.setJMenuBar(km.menuBar);// menu
					km.frame.getJMenuBar();
					km.qnMainPanel = null;

				}
			}
		}
	}

	/**
	 * Close a Project
	 * 
	 * @author Shuang Ma
	 */
	public static class CloseProjectAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4617774134568184085L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			int status = kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (status == 1) {
					kassService.exitValid(km, 0, 0, 1, false);
				} else {
					kassService.exitValid(km, 0, 0, 1, true);
				}

				// kassService.isNullValid(km, 0, 0, 1, 2, 0, 0, 1, 0, 0);
			}
		}
	}

	/**
	 * Close an Interview
	 * 
	 * @author MS20140928
	 */
	public static class CloseInterviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2925008965342156760L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			int status = kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (status == 1) {
					kassService.exitValid(km, 0, 1, 0, false);
				} else {
					kassService.exitValid(km, 0, 1, 0, true);
				}
				// kassService.isNullValid(km, 0, 1, 0, 0, 0, 1, 1, 0, 0);
			}

		}
	}

	/**
	 * Filter for the XML files
	 * 
	 * @author Fangfang Zhao
	 */

	public static class FileChooser {
		public void FileChooser(JFileChooser chooser, String s) {
			final List list = new ArrayList();
			list.add(s);

			chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

				@Override
				public boolean accept(File f) {
					if (f.isDirectory())
						return true;
					String name = f.getName();
					int p = name.lastIndexOf('.');
					if (p == -1)
						return false;
					String suffix = name.substring(p + 1).toLowerCase();
					return list.contains(suffix);
				}

				@Override
				public String getDescription() {
					return "xml files";
				}

			});

		}
	}

	/**
	 * Open an Interview
	 * 
	 * @author Fangfang Zhao
	 */
	public static class OpenInterviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2159101540567312170L;

		@Override
		public void actionPerformed(ActionEvent e) {

			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(km, 0);
				} else {

					JFileChooser chooser = new JFileChooser(".");
					String defaultInterviewPath;
					if (Constants.PROJECT == null) {
						defaultInterviewPath = WorkSpaceService
								.SetInterviewDefaultDir();
					} else {
						defaultInterviewPath = WorkSpaceService
								.SetProjectDefaultDir();
					}

					chooser.setCurrentDirectory(new File(defaultInterviewPath));
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "xml");

					int i = chooser.showOpenDialog(km.getRootPane());
					if (i == JFileChooser.APPROVE_OPTION) {
						chooser.setCurrentDirectory(new File(
								defaultInterviewPath));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						String path = chooser.getSelectedFile()
								.getAbsolutePath();
						Constants.KASS = null;
						String tmpPath = "";
						try {
							tmpPath = kassService.openRealXmlFile(path);
						} catch (Exception e11) {
							e11.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);
							return;
						}
						Constants.KASS_XML = tmpPath;
						try {
							kassService.get();

						}

						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}
						KASS kass = new KASS();
						kass = kassService.get();
						KFieldSite fieldSite = new KFieldSite();
						fieldSite = kass.getFieldSite();
						Constants.PROJECT = null;
						Constants.PROJECT_XML = fieldSite.getProjectDirector()
								+ File.separator + fieldSite.getPlaceName()
								+ ".project";
						ProjectService projectService = new ProjectService();
						Project project = new Project();
						project = projectService.get();
						Constants.QUESTIONNAIRE = null;
						Constants.QUESTIONNAIRE_XML = project
								.getQuestionnaireDirectory();
						QuestionnaireService questionnaireService = new QuestionnaireService();
						questionnaireService.get();

						try {
							km.ivMainPanel = new IvMainPanel(km);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}

						km.setMainPanel(km.ivMainPanel);

						km.qnMainPanel = null;

						Preferences pref = Preferences.userRoot().node(
								"interview");
						File file = chooser.getSelectedFile();
						int result = 0;
						try {
							for (int j = 0; j < pref.keys().length + 1; j++) {
								// Open a recently opened file
								if (file.getPath().equals(
										pref.get(String.valueOf(j), ""))) {
									String tempPath = pref.get(
											String.valueOf(j), "");
									for (int k = j; k > 1; k--) {
										pref.put(String.valueOf(k), pref.get(
												String.valueOf(k - 1), ""));
									}
									pref.put(String.valueOf(1), tempPath);
									result = 1;
									break;
								}
							}
						} catch (BackingStoreException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}
						// Open a recently unopened file
						if (result == 0) {
							try {
								// 最近打开访谈列表显示数目限定
								if (pref.keys().length < 10) {
									for (i = pref.keys().length + 1; i > 1; i--) {
										pref.put(String.valueOf(i), pref.get(
												String.valueOf(i - 1), ""));
									}
									pref.put(String.valueOf(1), file.getPath());
								} else {
									for (i = pref.keys().length; i > 1; i--) {
										pref.put(String.valueOf(i), pref.get(
												String.valueOf(i - 1), ""));
									}
									pref.put(String.valueOf(1), file.getPath());
								}
							} catch (BackingStoreException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
								JOptionPane.showMessageDialog(null, mxResources
										.get("openInterviewXMLError"), "Error",
										JOptionPane.ERROR_MESSAGE);

							}
						} else {
						}
						try {
							String path1 = Constants.getUserLogPath()
									+ File.separator + "interviewLog.xml";
							FileOutputStream fos = new FileOutputStream(path1);
							pref.exportSubtree(fos);
						} catch (Exception e1) {
							System.err.println(" Cannot export nodes:  " + e1);
							JOptionPane.showMessageDialog(null,
									mxResources.get("saveInterviewLogError"),
									"Error", JOptionPane.ERROR_MESSAGE);

						}

						km.menuBar.getMenu(0).getItem(6).removeAll();
						JMenu submenu = (JMenu) km.menuBar.getMenu(0)
								.getItem(6);
						List<String> recentInterviewPathList = WorkSpaceService
								.getRecentInterviewPathList();
						// Collections.reverse(recentProjectPathList);
						for (int j = 0; j < recentInterviewPathList.size(); j++) {
							submenu.add(km.bind(recentInterviewPathList.get(j),
									new OpenRecentInterviewAction(),
									"/images/open.gif"));
						}
						km.frame.setJMenuBar(km.menuBar);// menu
						km.frame.getJMenuBar();

					}
				}
			}
		}
	}

	public static class EditProjectAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 192997234885715861L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("op", "edit");

			if (Constants.PROJECT != null) {
				new ProjectEditDialog(map);
			} else if (Constants.KASS != null) {
				new ProjectEditDialog(map);
				KASS kass = new KASS();
				KInterviewInfo interviewInfo;
				kass = kassService.get();
				interviewInfo = kass.getInterviewInfo();
				if (interviewInfo.getSerialNumber() != null
						&& interviewInfo.getInterviewer() != null
						&& interviewInfo.getInterviewee() != null) {
					km.setTitle("|" + interviewInfo.getSerialNumber()
							+ ".xml - interviewer: "
							+ interviewInfo.getInterviewer()
							+ " / interviewee: "
							+ interviewInfo.getInterviewee());
				}
			} else {
				km.setTitle("");
				kassService.isProjectNullValid(km, 2);
			}

		}
	}

	/**
	 * View and Edit an Interview Information.
	 * 
	 * @author Fangfang Zhao
	 */
	public static class EditInterviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3195508949512050196L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("op", "edit");
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS != null) {
				new InterviewEditDialog(map);

				KASS kass = new KASS();

				kass = kassService.get();

				KInterviewInfo interviewInfo = new KInterviewInfo();
				interviewInfo = kass.getInterviewInfo();
				String interviewer = interviewInfo.getInterviewer();
				String interviewee = interviewInfo.getInterviewee();
				km.setTitle("|" + interviewInfo.getSerialNumber()
						+ ".xml - interviewer: " + interviewer
						+ " / interviewee: " + interviewee);
			} else {
				kassService.isInterviewNullValid(km, 1);
				// JOptionPane.showMessageDialog(null,
				// mxResources.get("openInterviewPropError"), "Error",
				// JOptionPane.ERROR_MESSAGE);

			}
		}
	}

	public static class returnInterviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4166260851205428850L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			km.qnMainPanel = null;
			km.setMainPanel(km.ivMainPanel);
		}
	}

	public static class ExitKassAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2642353907817629624L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.QUESTIONNAIRE_XML != null
					&& Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
				QuestionnaireService questionnaireService = new QuestionnaireService();
				questionnaireService.isQuestionnaireNullValid(km, 0);
			}
			if (Constants.isInterviewCancel == 1
					|| Constants.isQuestionnaireCancel == 1) {
			} else {
				kassService.exitValid(km, 1, 0, 0, true);
			}
		}
	}

	public static class UndoAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8734100308474375728L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			if (km.ivMainPanel.opGraphPanel == 1) {
				GraphPanel gp = km.ivMainPanel.graphPanel;
				gp.undoManager.undo();
			} else {
				GraphPanel gp = km.ivMainPanel.ographPanel;
				gp.undoManager.undo();
			}
		}
	}

	public static class RedoAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 273132413016117733L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			if (km.ivMainPanel.opGraphPanel == 1) {
				GraphPanel gp = km.ivMainPanel.graphPanel;
				gp.undoManager.redo();
			} else {
				GraphPanel gp = km.ivMainPanel.ographPanel;
				gp.undoManager.redo();
			}
		}
	}

	public static class DeleteAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1457568357810531844L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			GraphPanel gp;

			if (km.ivMainPanel.opGraphPanel == 1) {
				gp = km.ivMainPanel.graphPanel;
			} else {
				gp = km.ivMainPanel.ographPanel;
			}

			CustomGraph graph = gp.getCustomGraph();
			KassService service = new KassService();
			Object[] el = { "Yes", "No" };
			int result = JOptionPane.showOptionDialog(null,
					"Are you sure you want to remove this cell for graph?",
					"Select Option", JOptionPane.DEFAULT_OPTION,
					JOptionPane.WARNING_MESSAGE, null, el, el[1]);
			if (result == JOptionPane.YES_OPTION) {
				if (graph != null && graph.getSelectionCells() != null) {
					Object[] objs = graph.getSelectionCells();
					for (Object o : objs) {
						if (o instanceof mxCell) {
							mxCell cell = (mxCell) o;
							if (cell.getValue() instanceof GraphPerson) {
								GraphPerson graphPerson = (GraphPerson) cell
										.getValue();
								if (graphPerson.getId() == 0) {
									return;
								}
								service.deletePersonById(graphPerson.getId());
							} else if (cell.getValue() instanceof GraphUnion) {
								GraphUnion graphUnion = (GraphUnion) cell
										.getValue();
								service.deleteUnionById(graphUnion.getId());
							}
							graph.removeCells(new mxCell[] { cell });
						}
					}
				}
			} else if (result == JOptionPane.NO_OPTION) {

			}
		}
	}

	public static class InterviewLogAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6169218781360808167L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Map<String, Object> map = new HashMap<String, Object>();
			KnqMain km = (KnqMain) e.getSource();
			KassService service = new KassService();
			try {
				KInterviewInfo interview = service.get().getInterviewInfo();
				if (interview.getLogs().getKiLog().size() == 0) {
					JOptionPane.showConfirmDialog(km,
							mxResources.get("noInterviewLog"), "Care!",
							JOptionPane.YES_OPTION,
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					new InterviewLogDialog(map);
				}
			} catch (Exception e3) {
				System.err.println(" Cannot export nodes:  " + e3);
				JOptionPane.showMessageDialog(null,
						mxResources.get("openInterviewXMLError"), "Error",
						JOptionPane.ERROR_MESSAGE);
			}

		}
	}

	@SuppressWarnings("serial")
	public static class HistoryAction extends AbstractAction {

		protected boolean undo;

		public HistoryAction(boolean undo) {
			this.undo = undo;
		}

		public void actionPerformed(ActionEvent e) {
			GraphPanel panel = getPanel(e);
			mxUndoManager undoManager = panel.getUndoManager();
			if (undo) {
				undoManager.undo();
			} else {
				undoManager.redo();
			}
		}
	}

	/**
	 * Draw the diagram
	 * 
	 * @author Fangfang Zhao
	 */
	public static class DrawAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6753103112859863662L;
		private String drawType;
		private int type;
		private double x;
		private double y;

		public DrawAction() {
		}

		public DrawAction(String drawType, int type, double x, double y) {
			this.drawType = drawType;
			this.type = type;
			this.x = x;
			this.y = y;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			GraphPanel graphPanel = null;
			if (type == 1) {
				graphPanel = km.ivMainPanel.graphPanel;
			} else {
				graphPanel = km.ivMainPanel.ographPanel;
			}

			if ("male".equals(drawType)) {
				int cellId = km.ivMainPanel.getNextPersonId();
				graphPanel.addNewPerson(cellId, x, y, 0);
			} else if ("female".equals(drawType)) {
				int cellId = km.ivMainPanel.getNextPersonId();
				graphPanel.addNewPerson(cellId, x, y, 1);
			} else if ("union".equals(drawType)) {
				int cellId = km.ivMainPanel.getNextUnionId();
				graphPanel.addNewUnion(cellId, x, y);
			}
		}
	}

	public static class CompletePersonAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1266015444895854383L;
		private mxCell cell;
		private int position;// 1 graph, 2ograph

		public CompletePersonAction(mxCell cell, int position) {
			this.cell = cell;
			this.position = position;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			if (position == 1) {
				km.ivMainPanel.startAutocomplete(km.ivMainPanel.graphPanel,
						cell);
			} else {
				km.ivMainPanel.startAutocomplete(km.ivMainPanel.ographPanel,
						cell);
			}
		}
	}

	public static class DrawUnionAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4741340939307856030L;
		private mxCell cell;
		private int type;// 0 patener 1 offspring
		private int position;// 1 graph, 2ograph

		public DrawUnionAction(mxCell cell, int type, int position) {
			this.cell = cell;
			this.type = type;
			this.position = position;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			GraphPanel graphPanel = null;
			if (position == 1) {
				graphPanel = km.ivMainPanel.graphPanel;
			} else {
				graphPanel = km.ivMainPanel.ographPanel;
			}
			graphPanel.unioning = true;
			graphPanel.unioningCell = cell;
			graphPanel.unioningType = type;
		}
	}

	public static final GraphPanel getPanel(ActionEvent e) {
		if (e.getSource() instanceof Component) {
			Component component = (Component) e.getSource();

			while (component != null && !(component instanceof GraphPanel)) {
				component = component.getParent();
			}

			return (GraphPanel) component;
		}
		return null;
	}

	/**
	 * ZOOM the diagram
	 * 
	 * @author Fangfang Zhao
	 * 
	 */
	public static class ZoomAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6243893219867400125L;
		private int type;

		public ZoomAction(int t) {
			this.type = t;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			IvMainPanel ivMainPanel = km.ivMainPanel;
			if (this.type == 1) {// Zoom out
				if (ivMainPanel.opGraphPanel == 1) {
					ivMainPanel.graphPanel.zoomOut();
				} else {
					ivMainPanel.ographPanel.zoomOut();
				}
			} else if (this.type == 2) {// Zoom in
				if (ivMainPanel.opGraphPanel == 1) {
					ivMainPanel.graphPanel.zoomIn();
				} else {
					ivMainPanel.ographPanel.zoomIn();
				}
			} else if (this.type == 3) {// fit to window
				if (ivMainPanel.opGraphPanel == 1) {
					ivMainPanel.graphPanel.zoomToFit();
				} else {
					ivMainPanel.ographPanel.zoomToFit();
				}
			} else if (this.type == 4) {//
				if (ivMainPanel.opGraphPanel == 1) {
					mxCell mxCell = ivMainPanel.graphPanel.findPersonById(0);
					ivMainPanel.graphPanel.zoomToCell(mxCell);
				} else {
					mxCell root = (mxCell) ivMainPanel.ographPanel.graphComponent
							.getGraph().getModel().getRoot();
					mxCell c = (mxCell) root.getChildAt(0);
					mxCell mxCell = null;
					if (c.getChildCount() > 0) {
						mxCell = (mxCell) c.getChildAt(0);
					}
					ivMainPanel.ographPanel.zoomToCell(mxCell);
				}
			}
		}
	}

	/**
	 * Page Setting
	 * 
	 * @author Fangfang Zhao
	 * 
	 */
	public static class PageSetUpAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2295920879270823035L;

		public PageSetUpAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			// IvMainPanel ivMainPanel = km.ivMainPanel;
			// ivMainPanel.graphPanel.reRange();
			// ivMainPanel.graphPanel.showByRange(2);
			// ivMainPanel.graphPanel.clearAll();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 1);
			if (Constants.KASS != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("op", "add");
				new PageSetupDialog(map).setMainPanel(km);
			}

		}
	}

	/**
	 * Move the diagram
	 * 
	 * @author Fangfang Zhao
	 */
	public static class MoveAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1358609049148486595L;

		public MoveAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 1);
			if (Constants.KASS != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("op", "add");
				new MoveDialog(map).setMainPanel(km);
			}

		}
	}

	/**
	 * Print Preview of the diagram
	 * 
	 * @author Fangfang Zhao
	 * 
	 */
	public static class PrintViewAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -956436375421697203L;

		public PrintViewAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 1);
			if (Constants.KASS != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("op", "add");
				new PrintViewDialog(map).setMainPanel(km);
			}
		}
	}

	public static class ExportInterviewAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2569057057879123418L;

		public ExportInterviewAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 1);
			if (Constants.KASS != null) {

				if (km.ivMainPanel != null) {
					String defaultPath = WorkSpaceService
							.SetProjectDefaultDir();
					JFileChooser fc = new JFileChooser(defaultPath);
					fc.setAcceptAllFileFilterUsed(false);
					fc.addChoosableFileFilter(new DefaultFileFilter(".png",
							"PNG  " + mxResources.get("file") + " (.png)"));
					fc.addChoosableFileFilter(new DefaultFileFilter(".svg",
							"SVG  " + mxResources.get("file") + " (.svg)"));
					int rc = fc.showDialog(null, "Export");
					if (rc == JFileChooser.APPROVE_OPTION) {
						String name = fc.getSelectedFile().getName();
						String ext = ((DefaultFileFilter) fc.getFileFilter())
								.getExtension();
						if (".png".equals(ext)) {
							String preFileName = fc.getSelectedFile()
									.getParent() + File.separator + name;
							String gname = preFileName + "-NKK" + ext;
							String ogname = preFileName + "-OTHER" + ext;
							km.ivMainPanel.graphPanel.exportPNG(gname);
							km.ivMainPanel.ographPanel.exportPNG(ogname);
						} else if (".svg".equals(ext)) {
							String preFileName = fc.getSelectedFile()
									.getParent() + File.separator + name;
							String gname = preFileName + "-NKK" + ext;
							String ogname = preFileName + "-OTHER" + ext;
							km.ivMainPanel.graphPanel.exportSVG(gname);
							km.ivMainPanel.ographPanel.exportSVG(ogname);
						}
					}
				} else {

				}
			}
		}
	}

	public static class RengeAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6391405762768974378L;

		public RengeAction() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			IvMainPanel ivMainPanel = km.ivMainPanel;
			ivMainPanel.graphPanel.reRange();

		}
	}

	public static class showByRangeAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2772220212054116726L;
		private int STEP;

		public showByRangeAction(int stopStep) {
			STEP = stopStep;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			IvMainPanel ivMainPanel = km.ivMainPanel;
			ivMainPanel.graphPanel.showByRange(STEP);
		}
	}

}
