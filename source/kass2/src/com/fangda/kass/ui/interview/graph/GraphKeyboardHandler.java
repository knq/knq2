package com.fangda.kass.ui.interview.graph;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import com.fangda.kass.ui.interview.IvAction.HistoryAction;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.handler.mxKeyboardHandler;

/**
 * The Class for the keyboard shortcut of the graph operation, such as undo,
 * redo.
 * 
 * @author Fangfang Zhao
 * 
 */
public class GraphKeyboardHandler extends mxKeyboardHandler {

	public GraphKeyboardHandler(mxGraphComponent graphComponent) {
		super(graphComponent);
	}

	protected InputMap getInputMap(int condition) {
		InputMap map = super.getInputMap(condition);
		if (condition == JComponent.WHEN_FOCUSED && map != null) {
			map.put(KeyStroke.getKeyStroke("control Z"), "undo");
			map.put(KeyStroke.getKeyStroke("control Y"), "redo");
		}
		return map;
	}

	protected ActionMap createActionMap() {
		ActionMap map = super.createActionMap();
		map.put("undo", new HistoryAction(true));
		map.put("redo", new HistoryAction(false));
		return map;
	}

}
