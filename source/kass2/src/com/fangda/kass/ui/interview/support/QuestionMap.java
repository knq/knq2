package com.fangda.kass.ui.interview.support;

import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KPPCoords;
import com.fangda.kass.model.interview.KPPerson;
import com.fangda.kass.model.interview.KPeople;
import com.fangda.kass.model.interview.KQQuestion;
import com.fangda.kass.model.interview.KQQuestionField;
import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.interview.KUUMember;
import com.fangda.kass.model.interview.KUUnion;
import com.fangda.kass.model.interview.KUnions;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.model.support.Relevance;
import com.fangda.kass.model.support.RelevanceCondition;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.common.JRadioGroup;
import com.fangda.kass.ui.interview.graph.GraphPanel;
import com.fangda.kass.ui.interview.graph.GraphPerson;
import com.fangda.kass.ui.interview.graph.GraphUnion;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.RegularUtil;
import com.mxgraph.model.mxCell;

/**
 * This class for the question Map of an interview in the IMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class QuestionMap {

	public Map<Question, List<Object>> questionMap = new HashMap<Question, List<Object>>();
	KassService kassService = null;
	QuestionnaireService qService = null;

	public QuestionMap() {
		kassService = new KassService();
		qService = new QuestionnaireService();
	}

	/**
	 * Add question
	 * 
	 * @param question
	 * @param components
	 */
	public void addQuestion(Question question, Object component) {
		List<Object> components = questionMap.get(question);
		if (components == null) {
			components = new ArrayList<Object>();
			components.add(component);
			questionMap.put(question, components);
		} else {
			components.add(component);
		}
	}

	/**
	 * Initialize the value of the question answer
	 */
	public void initPersonValue(int pid, KassService kassService) {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int i = 0; i < question.getOptions().size(); i++) {
					Option option = question.getOptions().get(i);
					KQQuestionField field = kassService.getPersonField(pid,
							option.getLabel());
					if (field != null) {
						for (int j = 0; j < components.size(); j++) {
							JCheckBox cb = (JCheckBox) components.get(j);
							if (StringUtils.equals(cb.getName(),
									option.getOid())) {
								if (StringUtils.equalsIgnoreCase("true",
										field.getValue())) {
									cb.setSelected(true);
								} else {
									cb.setSelected(false);
								}
							}
						}
					}
				}
			} else if ("D".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field != null) {
					Option option = question.getOptions().get(0);
					for (int j = 0; j < components.size(); j++) {
						JCheckBox cb = (JCheckBox) components.get(j);
						if (StringUtils.equals(cb.getName(), option.getOid())
								&& StringUtils.equalsIgnoreCase("true",
										field.getValue())) {
							cb.setSelected(true);
						} else {
							cb.setSelected(false);
						}
					}
				}
			} else if ("A".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field != null) {
					JTextField cb = (JTextField) components.get(0);
					if (!StringUtils.equals(field.getValue(),
							Constants.DEFAULT_NA)) {
						cb.setText(field.getValue());
					}
				}
			} else if ("T".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field != null) {
					JTextArea cb = (JTextArea) components.get(0);
					if (!StringUtils.equals(field.getValue(),
							Constants.DEFAULT_NA)) {
						cb.setText(field.getValue());
					}
				}
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field == null && "sex".equals(question.getLabel())) {
					KPPerson person = kassService.getPerson(pid);
					field = new KQQuestionField();
					field.setLabel(question.getLabel());
					field.setQid(question.getQid());
					field.setValue(person.getSex() + "");
				}
				JRadioGroup group = (JRadioGroup) components.get(0);
				if (field != null) {
					group.clearSelection();
					for (int i = 0; i < question.getOptions().size(); i++) {
						Option option = question.getOptions().get(i);
						Enumeration<AbstractButton> ems = group.getElements();
						while (ems.hasMoreElements()) {
							AbstractButton button = ems.nextElement();
							if (StringUtils.equals(button.getName(),
									option.getOid())
									&& StringUtils.equals(
											button.getActionCommand(),
											field.getValue())) {
								button.setSelected(true);
							} else {
								button.setSelected(false);
							}
						}
					}
				}
			}
		}
	}

	// =======================Questions Display=====================
	/**
	 * Display or not Display
	 * 
	 * @param fquestion
	 * @return
	 */
	public boolean questionIsShow(int pid, Question fquestion) {
		if (pid <= 0) {
			return true;
		}
		KQQuestionField field = kassService.getPersonField(pid,
				fquestion.getLabel());
		if (field != null) {
			if (Constants.DEFAULT_DNA.equals(field.getValue())) {
				return false;
			} else {
				if (checkQuestionCondition(pid, fquestion)) {
					return true;
				} else {
					return false;
				}
			}
		}
		return true;
	}

	/***
	 * The flag to display or not display
	 * 
	 * @param fquestion
	 * @return true = display, false = not display
	 */
	public boolean checkQuestionCondition(int pid, Question fquestion) {
		Question leadQuestion = qService.searchParanetQuestion(fquestion
				.getQid());
		if (leadQuestion != null) {
			List<FollowupQuestion> follows = leadQuestion
					.getFollowupQuestions();
			FollowupQuestion q = follows.get(0);
			if (StringUtils.equals(q.getQid(), fquestion.getQid())) {
				return true;
			}
		}
		List<FollowupQuestion> follows = leadQuestion.getFollowupQuestions();
		for (int i = 1; i < follows.size(); i++) {
			FollowupQuestion q = follows.get(i);
			if (StringUtils.isNotBlank(q.getIfGo())) {
				if (q.getIfGo().contains(fquestion.getQid())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Get the value of the ifGo element (The skip condition)
	 * 
	 * @param obj
	 * @return
	 */
	public Map<String, List<String>> getValue(String ifGo) {
		return InitUtil.getValue(ifGo);
	}

	public String getQuestionValue008(int pid, String qid) {
		Question fquestion = qService.searchQuestion(qid);
		KQQuestionField field = kassService.getPersonField(pid,
				fquestion.getLabel());
		if (field != null) {
			return field.getValue();
		}
		return null;
	}

	public void initPersonDNA(int pid) {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			if (StringUtils.isNotBlank(question.getIfGo())) {
				Map<String, List<String>> map = getValue(question.getIfGo());
				String v = this.getQuestionValue008(pid, question.getQid());
				if (v == null || "".equals(v)) {
					v = Constants.DEFAULT_NA;
				}
				List<String> qids = map.get(v);
				if (qids != null) {
					for (String qid : qids) {//
						this.setValueDNA(qid, pid);
					}
				}
				kassService.save();
				Iterator<List<String>> values = map.values().iterator();
				while (values.hasNext()) {
					List<String> ids = values.next();
					for (String qid : ids) {
						if (qids != null && !qids.contains(qid)) {
							this.setValueNA(qid, pid);
						}
					}
				}
				kassService.save();
			}
		}
	}

	/**
	 * Get the question
	 * 
	 * @param pid
	 * @param qid
	 * @return
	 */
	public String getNoCompleteQuestion(int id, String qid) {
		Question question = qService.searchQuestion(qid);
		String name = QuestionConstants.getRelevantCondition(question
				.getRelevance());
		if (StringUtils.isBlank(name))
			return "";
		String noCompleteQid = "";
		if (StringUtils.isNotBlank(name)) {
			Relevance r = new Relevance();
			r.string2obj(name);
			noCompleteQid = checkByRelevance(r);
		}
		return noCompleteQid;
	}

	public String checkByRelevance(Relevance r) {
		List<RelevanceCondition> conditions = r.getConditions();
		for (RelevanceCondition condition : conditions) {
			String qid = checkByRelevanceCondition(condition);
			if (StringUtils.isNotBlank(qid)) {
				return qid;
			}
		}
		return "";
	}

	public String checkByRelevanceCondition(RelevanceCondition condition) {
		if (StringUtils
				.equals(condition.getType(), Relevance.TYPE_PERSON_FIELD)) {
			List<KPPerson> persons = kassService.get().getPeople().getPersons();
			for (KPPerson person : persons) {
				for (KQQuestionField field : person.getFields()) {
					if (StringUtils.equals(condition.getTypeAttribute(),
							field.getQid())
							&& StringUtils.equals(Constants.DEFAULT_NA,
									field.getValue())) {
						return condition.getTypeAttribute();
					}
				}
			}
		} else if (StringUtils.equals(condition.getType(),
				Relevance.TYPE_UNION_FIELD)) {
			List<KUUnion> unions = kassService.get().getUnions().getUnions();
			for (KUUnion union : unions) {
				for (KQQuestionField field : union.getFields()) {
					if (StringUtils.equals(condition.getTypeAttribute(),
							field.getQid())
							&& StringUtils.equals(Constants.DEFAULT_NA,
									field.getValue())) {
						return condition.getTypeAttribute();
					}
				}
			}
		}
		return "";
	}

	public void initPersonSex(mxCell cell, KassService kassService) {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		String value = "";
		if (cell.getStyle().contains("female")) {
			value = "1";
		} else {
			value = "0";
		}
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("C".equals(question.getType())
					&& "sex".equals(question.getLabel())) {
				JRadioGroup group = (JRadioGroup) components.get(0);

				for (int i = 0; i < question.getOptions().size(); i++) {
					Option option = question.getOptions().get(i);
					Enumeration<AbstractButton> ems = group.getElements();
					while (ems.hasMoreElements()) {
						AbstractButton button = ems.nextElement();
						if (StringUtils.equals(button.getName(),
								option.getOid())
								&& StringUtils.equals(
										button.getActionCommand(), value)) {
							button.setSelected(true);
						} else {
							button.setSelected(false);
						}
					}
				}
			}
		}
	}

	public void initUnionValue(int pid, KassService kassService) {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int i = 0; i < question.getOptions().size(); i++) {
					Option option = question.getOptions().get(i);
					KQQuestionField field = kassService.getUnionField(pid,
							option.getLabel());

					if (field != null) {
						for (int j = 0; j < components.size(); j++) {
							JCheckBox cb = (JCheckBox) components.get(j);
							if (StringUtils.equals(cb.getName(),
									option.getOid())) {
								if (StringUtils.equalsIgnoreCase("true",
										field.getValue())) {
									cb.setSelected(true);
								} else {
									cb.setSelected(false);
								}
							}
						}
					}
				}
			} else if ("D".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(pid,
						question.getLabel());
				if (field != null) {
					Option option = question.getOptions().get(0);
					for (int j = 0; j < components.size(); j++) {
						JCheckBox cb = (JCheckBox) components.get(j);
						if (StringUtils.equals(cb.getName(), option.getOid())) {
							if (StringUtils.equalsIgnoreCase("true",
									field.getValue())) {
								cb.setSelected(true);
							} else {
								cb.setSelected(false);
							}
						}
					}
				}
			} else if ("A".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(pid,
						question.getLabel());
				if (field != null) {
					JTextField cb = (JTextField) components.get(0);
					if (!StringUtils.equals(field.getValue(),
							Constants.DEFAULT_NA)) {
						cb.setText(field.getValue());
					}
				}
			} else if ("T".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(pid,
						question.getLabel());
				if (field != null) {
					JTextArea cb = (JTextArea) components.get(0);
					if (!StringUtils.equals(field.getValue(),
							Constants.DEFAULT_NA)) {
						cb.setText(field.getValue());
					}
				}
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(pid,
						question.getLabel());
				JRadioGroup group = (JRadioGroup) components.get(0);
				if (field != null) {
					group.clearSelection();
					for (int i = 0; i < question.getOptions().size(); i++) {
						Option option = question.getOptions().get(i);
						Enumeration<AbstractButton> ems = group.getElements();
						while (ems.hasMoreElements()) {
							AbstractButton button = ems.nextElement();
							if (StringUtils.equals(button.getName(),
									option.getOid())
									&& StringUtils.equals(
											button.getActionCommand(),
											field.getValue())) {
								button.setSelected(true);
							} else {
								button.setSelected(false);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Return -1
	 * 
	 * @param cell
	 * @param qService
	 * @param kassService
	 * @param panels
	 * @param dedupLabels
	 */
	public int savePersonValue(GraphPanel graphPanel, mxCell cell,
			QuestionnaireService qService, KassService kassService,
			List<JPanel> panels, List<String> dedupLabels) {

		GraphPerson one = (GraphPerson) cell.getValue();
		KPPerson person = kassService.getPerson(one.getId());
		boolean isNew = false;
		if (person == null) {
			person = new KPPerson();
			person.setPid(one.getId());
			person.setSex(one.getSex());
			person.setIsOther(one.getIsOther());
			saveNewPerson(person, qService, kassService);
			kassService.save();
			isNew = true;
		}
		if (person.getPid() == 0) {
			Iterator<Entry<Question, List<Object>>> iter1 = questionMap
					.entrySet().iterator();
			boolean isDeath = false;
			while (iter1.hasNext()) {
				Entry<Question, List<Object>> entry = iter1.next();
				Question question = entry.getKey();
				if ("cbIsDeceased".equals(question.getLabel())) {
					Option option = question.getOptions().get(0);
					List<Object> components = entry.getValue();
					for (int j = 0; j < components.size(); j++) {
						JCheckBox cb = (JCheckBox) components.get(j);
						if (StringUtils.equals(cb.getName(), option.getOid())) {
							if (cb.isSelected()) {
								isDeath = true;
								return -1;
							}
						}
					}
				}
			}
		}

		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		Map<String, String> m = new HashMap<String, String>();
		String sex = "";
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int i = 0; i < question.getOptions().size(); i++) {
					Option option = question.getOptions().get(i);
					KQQuestionField field = kassService.getPersonField(
							person.getPid(), option.getLabel());
					if (field != null) {
						for (int j = 0; j < components.size(); j++) {
							JCheckBox cb = (JCheckBox) components.get(j);
							if (StringUtils.equals(cb.getName(),
									option.getOid())) {
								if (cb.isSelected()) {
									field.setValue("true");
								} else {
									field.setValue("false");
								}
							}
						}
					}
				}
			} else if ("D".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(
						person.getPid(), question.getLabel());
				if (field != null) {
					Option option = question.getOptions().get(0);
					for (int j = 0; j < components.size(); j++) {
						JCheckBox cb = (JCheckBox) components.get(j);
						if (StringUtils.equals(cb.getName(), option.getOid())) {
							if (cb.isSelected()) {
								field.setValue("true");
							} else {
								field.setValue("false");
							}
						}
					}
				}
			} else if ("A".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(
						person.getPid(), question.getLabel());
				JTextField cb = (JTextField) components.get(0);
				if (cb != null && field != null) {
					String v = cb.getText();
					if (StringUtils.isBlank(v)) {
						v = Constants.DEFAULT_NA;
					}
					field.setValue(v);
					if (dedupLabels != null
							&& dedupLabels.contains(question.getLabel())) {
						if (m.get(question.getLabel()) != null) {
							m.put(question.getLabel(),
									m.get(question.getLabel()) + "," + v);
						} else {
							m.put(question.getLabel(), v);
						}
					}
				}
			} else if ("T".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(
						person.getPid(), question.getLabel());
				JTextArea cb = (JTextArea) components.get(0);
				if (cb != null && field != null) {
					String v = cb.getText();
					if (StringUtils.isBlank(v)) {
						v = Constants.DEFAULT_NA;
					}
					field.setValue(v);
					if (dedupLabels != null
							&& dedupLabels.contains(question.getLabel())) {
						if (m.get(question.getLabel()) != null) {
							m.put(question.getLabel(),
									m.get(question.getLabel()) + "," + v);
						} else {
							m.put(question.getLabel(), v);
						}
					}
				}
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				KQQuestionField field = kassService.getPersonField(
						person.getPid(), question.getLabel());
				String value = "";
				JRadioGroup group = (JRadioGroup) components.get(0);
				Enumeration<AbstractButton> ems = group.getElements();
				while (ems.hasMoreElements()) {
					AbstractButton button = ems.nextElement();
					if (button.isSelected()) {
						value = button.getActionCommand();
						break;
					}
				}
				if ("sex".equals(question.getLabel())) {
					sex = value;
				}
				if (field != null) {
					if (StringUtils.isBlank(value)) {
						value = Constants.DEFAULT_NA;
					}
					field.setValue(value);
					if (dedupLabels != null
							&& dedupLabels.contains(question.getLabel())) {
						if (m.get(question.getLabel()) != null) {
							m.put(question.getLabel(),
									m.get(question.getLabel()) + "," + value);
						} else {
							m.put(question.getLabel(), value);
						}
					}
				}
			}
		}
		if (isNew) {
			if (person.getCoords() == null) {
				person.setCoords(new KPPCoords());
			}
			person.getCoords().setX(cell.getGeometry().getX());
			person.getCoords().setY(cell.getGeometry().getY());
		}
		kassService.save();
		if (StringUtils.isNotBlank(sex)) {
			person.setSex(Integer.parseInt(sex));
		}
		one.setKpperson(person.clone());
		kassService.save();
		// NA/DNA
		// initPersonDNA(one.getId());
		if (panels != null) {
			for (JPanel panel : panels) {
				if (!panel.isVisible()) {
					String name = panel.getName();
					String id = name.replace("question_", "");
					this.setValueDNA(id, person.getPid());
				}
			}
			kassService.save();
		}
		InitUtil.handlePersonDeath(person);
		Iterator<Entry<String, String>> it = m.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			String[] args = entry.getValue().split(",");
			String v1 = args[0];
			String v2 = args[1];
			KQQuestionField f = kassService.getPersonField(person.getPid(),
					entry.getKey());
			if (StringUtils.equals(v1, v2)
					&& StringUtils.equals(v1, Constants.DEFAULT_DNA)) {
			} else {
				if (StringUtils.equals(v1, Constants.DEFAULT_NA)
						|| !StringUtils.equals(v2, Constants.DEFAULT_NA)) {
					f.setValue(v2);
				} else if (StringUtils.equals(v2, Constants.DEFAULT_NA)
						|| !StringUtils.equals(v1, Constants.DEFAULT_NA)) {
					f.setValue(v1);
				} else if (StringUtils.equals(v2, Constants.DEFAULT_NA)
						|| StringUtils.equals(v1, Constants.DEFAULT_NA)) {
					f.setValue(Constants.DEFAULT_NA);
				} else if (!StringUtils.equals(v2, Constants.DEFAULT_NA)
						|| !StringUtils.equals(v1, Constants.DEFAULT_NA)) {
					f.setValue(v1);
				}
			}
		}
		checkAllPerson();// Check the completed status
		kassService.save();
		if (isNew) {
			KPPerson kp = one.getKpperson();
			graphPanel.changeSex(cell, one.getId(), kp.getSex());
			one.setSex(kp.getSex());
		}
		one.setBirthDay(kassService.getPersonBirthYear(one.getId()));
		return 1;
	}

	public String getPersonStyle(GraphPerson person) {
		if (person.getSex() == 0) {
			if (person.getId() == 0) {
				if (person.getIsDeath() == 2) {
					return "maleE";
				} else {
					return "maleDeathE";
				}
			} else {
				if (person.getIsDeath() == 2) {
					return "maleO";
				} else {
					return "maleDeathO";
				}
			}
		} else {
			if (person.getId() == 0) {
				if (person.getIsDeath() == 2) {
					return "femaleE";
				} else {
					return "femaleDeathE";
				}
			} else {
				if (person.getIsDeath() == 2) {
					return "femaleO";
				} else {
					return "femaleDeathO";
				}
			}
		}
	}

	public void saveUnionValue(mxCell cell, QuestionnaireService qService,
			KassService kassService, List<JPanel> panels) {
		GraphUnion one = (GraphUnion) cell.getValue();
		KUUnion union = kassService.getUnion(one.getId());

		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();

		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int i = 0; i < question.getOptions().size(); i++) {
					Option option = question.getOptions().get(i);
					KQQuestionField field = kassService.getUnionField(
							union.getId(), option.getLabel());
					if (field == null) {
						field = new KQQuestionField();
						field.setLabel(option.getLabel());
						field.setQid(option.getOid());
						kassService.getUnion(union.getId()).getFields()
								.add(field);
					}
					for (int j = 0; j < components.size(); j++) {
						JCheckBox cb = (JCheckBox) components.get(j);
						if (StringUtils.equals(cb.getName(), option.getOid())) {
							if (cb.isSelected()) {
								field.setValue("true");
							} else {
								field.setValue("false");
							}
						}
					}
				}
			} else if ("D".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(
						union.getId(), question.getLabel());
				if (field == null) {
					field = new KQQuestionField();
					field.setLabel(question.getLabel());
					field.setQid(question.getQid());
					kassService.getUnion(union.getId()).getFields().add(field);
				}
				Option option = question.getOptions().get(0);
				for (int j = 0; j < components.size(); j++) {
					JCheckBox cb = (JCheckBox) components.get(j);
					if (StringUtils.equals(cb.getName(), option.getOid())) {
						if (cb.isSelected()) {
							field.setValue("true");
						} else {
							field.setValue("false");
						}
					}
				}
			} else if ("A".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(
						union.getId(), question.getLabel());
				if (field == null) {
					field = new KQQuestionField();
					field.setLabel(question.getLabel());
					field.setQid(question.getQid());
					kassService.getUnion(union.getId()).getFields().add(field);
				}
				JTextField cb = (JTextField) components.get(0);
				if (cb != null && field != null) {
					field.setValue(getNAByNull(cb.getText()));
				}
			} else if ("T".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(
						union.getId(), question.getLabel());
				if (field == null) {
					field = new KQQuestionField();
					field.setQid(question.getQid());
					field.setLabel(question.getLabel());
					kassService.getUnion(union.getId()).getFields().add(field);
				}
				JTextArea cb = (JTextArea) components.get(0);
				if (cb != null && field != null) {
					field.setValue(getNAByNull(cb.getText()));
				}
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				KQQuestionField field = kassService.getUnionField(
						union.getId(), question.getLabel());
				if (field == null) {
					field = new KQQuestionField();
					field.setLabel(question.getLabel());
					field.setQid(question.getQid());
					kassService.getUnion(union.getId()).getFields().add(field);
				}
				String value = "";
				JRadioGroup group = (JRadioGroup) components.get(0);
				Enumeration<AbstractButton> ems = group.getElements();
				while (ems.hasMoreElements()) {
					AbstractButton button = ems.nextElement();
					if (button.isSelected()) {
						value = button.getActionCommand();
						break;
					}
				}
				if (field != null) {
					field.setValue(value);
				}
			}
		}
		union.getCoords().setX(cell.getGeometry().getX());
		union.getCoords().setY(cell.getGeometry().getY());
		one.setKuunion(union.clone());
		kassService.save();
		checkAllUnion();
		kassService.save();
		if (panels != null) {
			for (JPanel panel : panels) {
				if (!panel.isVisible()) {
					String name = panel.getName();
					String id = name.replace("question_", "");
					this.setUnionValueDNA(id, union.getId());
				}
			}
			kassService.save();
		}
		InitUtil.modifyUnionDefault(qService, kassService);
		kassService.save();
	}

	/**
	 * Clear the component
	 */
	public void clearComponent() {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int j = 0; j < components.size(); j++) {
					JCheckBox cb = (JCheckBox) components.get(j);
					cb.setSelected(false);
				}
			} else if ("D".equals(question.getType())) {
				for (int j = 0; j < components.size(); j++) {
					JCheckBox cb = (JCheckBox) components.get(j);
					cb.setSelected(false);
				}
			} else if ("A".equals(question.getType())) {
				JTextField cb = (JTextField) components.get(0);
				cb.setText("");
			} else if ("T".equals(question.getType())) {
				JTextArea cb = (JTextArea) components.get(0);
				cb.setText("");
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				JRadioGroup group = (JRadioGroup) components.get(0);
				group.clearSelection();
			}
		}
	}

	public List<Object> getFieldTextComponents() {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		List<Object> result = new ArrayList<Object>();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("A".equals(question.getType())) {
				JTextField cb = (JTextField) components.get(0);
				result.add(cb);
			} else if ("T".equals(question.getType())) {
				JTextArea cb = (JTextArea) components.get(0);
				result.add(cb);
			}
		}
		return result;
	}

	public List<Object> getNameFieldTextComponents() {
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		List<Object> result = new ArrayList<Object>();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			if (StringUtils.equals("Surname", question.getLabel())
					|| StringUtils.equals("Othernames", question.getLabel())) {
				List<Object> components = entry.getValue();
				if ("A".equals(question.getType())) {
					JTextField cb = (JTextField) components.get(0);
					result.add(cb);
				} else if ("T".equals(question.getType())) {
					JTextArea cb = (JTextArea) components.get(0);
					result.add(cb);
				}
			}
		}
		return result;
	}

	public void saveNewPerson(KPPerson person, QuestionnaireService qService,
			KassService kassService) {

		List<Subsection> list = qService.getQuestionnaire().getSection()
				.getSubsections();
		for (Subsection subsection : list) {
			List<Question> questions = subsection.getQuestions();
			if (questions != null && questions.size() > 0) {
				for (Question question : questions) {
					InitUtil.saveDefaultQuestion(question, person);
				}
			}
			List<Subsection> subsections = subsection.getSubSections();
			if (subsections != null && subsections.size() > 0) {
				for (Subsection ss : subsections) {
					List<Question> qs = ss.getQuestions();
					if (qs != null && qs.size() > 0) {
						for (Question question : qs) {
							InitUtil.saveDefaultQuestion(question, person);
						}
					}
				}
			}
		}

		KPeople people = kassService.get().getPeople();
		if (people == null) {
			people = new KPeople();
			kassService.get().setPeople(people);
		}
		kassService.get().getPeople().getPersons().add(person);
		kassService.save();
		if (person.getPid() == 0) {
			// Ego: "mapResDistance"=1. When new an Ego, set "mapResDistance"=1
			KQQuestionField field = kassService.getPersonField(person.getPid(),
					"mapResDistance");
			if (field != null) {
				field.setValue("1");
			}
			kassService.save();
		}
		checkNewPersonComplete(person, qService, kassService);
	}

	public void checkNewPersonComplete(KPPerson person,
			QuestionnaireService qService, KassService kassService) {
		List<KQuestion> questions = kassService.get().getQuestions()
				.getQuestions();
		for (KQuestion question : questions) {
			if ("1".equals(question.getCompleted())) {

				String vals = kassService.getPersonQuestionValue(
						person.getPid(),
						qService.searchQuestion(question.getId()));
				if (!vals.contains(Constants.DEFAULT_DNA)) {
					question.setCompleted("2");
				}
			}
		}
		kassService.save();
	}

	/**
	 * Initialize the UNION
	 */
	public void initNewUnionDefault(KUUnion union) {
		List<Question> questions = qService.searchUnionQuestion();
		if (questions != null && questions.size() > 0) {
			for (Question question : questions) {
				this.saveDefaultUnionQuestion(question, union);
			}
		}
	}

	/**
	 * Save the default question of the union
	 * 
	 * @param question
	 * @param union
	 */
	private void saveDefaultUnionQuestion(Question question, KUUnion union) {
		List<String> labels = new ArrayList<String>();
		if (RegularUtil.isPersonOrUnion(question)) {
			List<FollowupQuestion> fquestions = question.getFollowupQuestions();
			if (fquestions != null && fquestions.size() > 0) {

				for (FollowupQuestion fquestion : fquestions) {
					if ("B".equals(fquestion.getType())) {
						List<Option> options = fquestion.getOptions();
						for (Option option : options) {
							if (!labels.contains(option.getLabel())) {
								KQQuestionField field = new KQQuestionField();
								field.setLabel(option.getLabel());
								field.setQid(option.getOid());
								field.setValue(Constants.DEFAULT_NA);
								union.getFields().add(field);
								labels.add(option.getLabel());
							}
						}
					} else {
						if (!labels.contains(fquestion.getLabel())) {
							KQQuestionField field = new KQQuestionField();
							field.setLabel(fquestion.getLabel());
							field.setQid(fquestion.getQid());
							field.setValue(Constants.DEFAULT_NA);
							union.getFields().add(field);
							labels.add(fquestion.getLabel());
						}
					}
				}
			}
		}

	}

	/**
	 * Set the DNA value for the question of a person
	 * 
	 * @param question
	 * @param pid
	 */
	public void setValueDNA(String qid, int pid) {
		Question question = qService.searchQuestion(qid);
		if (question instanceof FollowupQuestion) {
			if ("B".equals(question.getType())) {
				List<Option> options = question.getOptions();
				for (Option option : options) {
					KQQuestionField field = kassService.getPersonField(pid,
							option.getLabel());
					if (field != null) {
						field.setValue(Constants.DEFAULT_DNA);
					}
				}
			} else {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field != null) {
					field.setValue(Constants.DEFAULT_DNA);
				}
			}
		} else {
			if (RegularUtil.isMap(question)) {
				if (StringUtils.isNotBlank(question.getLabel())) {
					KQQuestionField field = kassService.getPersonField(pid,
							question.getLabel());
					if (field != null) {
						field.setValue(Constants.DEFAULT_DNA);
					}
				}
			} else if (RegularUtil.isPersonOrUnion(question)) {
				List<FollowupQuestion> fquestions = question
						.getFollowupQuestions();
				if (fquestions != null && fquestions.size() > 0) {
					for (FollowupQuestion fquestion : fquestions) {
						if ("B".equals(fquestion.getType())) {
							List<Option> options = fquestion.getOptions();
							for (Option option : options) {
								KQQuestionField field = kassService
										.getPersonField(pid, option.getLabel());
								if (field != null) {
									field.setValue(Constants.DEFAULT_DNA);
								}
							}
						} else {
							KQQuestionField field = kassService.getPersonField(
									pid, fquestion.getLabel());
							if (field != null) {
								field.setValue(Constants.DEFAULT_DNA);
							}
						}
					}
				}
			} else {
				KQQuestion kqq = kassService.getPersonQuestion(pid,
						question.getQid());
				if (kqq != null) {
					kqq.setValue(Constants.DEFAULT_DNA);
					List<KQQuestionField> fquestions = kqq.getFields();
					if (fquestions != null && fquestions.size() > 0) {
						for (KQQuestionField field : fquestions) {
							field.setValue(Constants.DEFAULT_DNA);
						}
					}
				}
			}
		}
	}

	/**
	 * Set the DNA value for the question of a union
	 * 
	 * @param question
	 * @param pid
	 */
	public void setUnionValueDNA(String qid, int id) {
		Question question = qService.searchQuestion(qid);
		if (question instanceof FollowupQuestion) {
			if ("B".equals(question.getType())) {
				List<Option> options = question.getOptions();
				for (Option option : options) {
					KQQuestionField field = kassService.getUnionField(id,
							option.getLabel());
					if (field != null) {
						field.setValue(Constants.DEFAULT_DNA);
					}
				}
			} else {
				KQQuestionField field = kassService.getUnionField(id,
						question.getLabel());
				if (field != null) {
					field.setValue(Constants.DEFAULT_DNA);
				}
			}
		} else {
			if (RegularUtil.isMap(question)) {
				if (StringUtils.isNotBlank(question.getLabel())) {
					KQQuestionField field = kassService.getUnionField(id,
							question.getLabel());
					if (field != null) {
						field.setValue(Constants.DEFAULT_DNA);
					}
				}
			} else if (RegularUtil.isPersonOrUnion(question)) {
				List<FollowupQuestion> fquestions = question
						.getFollowupQuestions();
				if (fquestions != null && fquestions.size() > 0) {
					for (FollowupQuestion fquestion : fquestions) {
						if ("B".equals(fquestion.getType())) {
							List<Option> options = fquestion.getOptions();
							for (Option option : options) {
								KQQuestionField field = kassService
										.getUnionField(id, option.getLabel());
								if (field != null) {
									field.setValue(Constants.DEFAULT_DNA);
								}
							}
						} else {
							KQQuestionField field = kassService.getUnionField(
									id, fquestion.getLabel());
							if (field != null) {
								field.setValue(Constants.DEFAULT_DNA);
							}
						}
					}
				}
			} else {

			}
		}
	}

	/**
	 * Set the NA value for the question of a person
	 * 
	 * @param question
	 * @param pid
	 */
	public void setValueNA(String qid, int pid) {
		Question question = qService.searchQuestion(qid);
		if (question instanceof FollowupQuestion) {
			if ("B".equals(question.getType())) {
				List<Option> options = question.getOptions();
				for (Option option : options) {
					KQQuestionField field = kassService.getPersonField(pid,
							option.getLabel());
					if (field != null) {
						if (Constants.DEFAULT_DNA.equals(field.getValue()))
							field.setValue(Constants.DEFAULT_NA);
					}
				}
			} else {
				KQQuestionField field = kassService.getPersonField(pid,
						question.getLabel());
				if (field != null) {
					if (Constants.DEFAULT_DNA.equals(field.getValue()))
						field.setValue(Constants.DEFAULT_NA);
				}
			}
		} else {
			if (RegularUtil.isMap(question)) {
				if (StringUtils.isNotBlank(question.getLabel())) {
					KQQuestionField field = kassService.getPersonField(pid,
							question.getLabel());
					if (field != null) {
						if (Constants.DEFAULT_DNA.equals(field.getValue()))
							field.setValue(Constants.DEFAULT_NA);
					}
				}
			} else if (RegularUtil.isPersonOrUnion(question)) {
				List<FollowupQuestion> fquestions = question
						.getFollowupQuestions();
				if (fquestions != null && fquestions.size() > 0) {
					for (FollowupQuestion fquestion : fquestions) {
						if ("B".equals(fquestion.getType())) {
							List<Option> options = fquestion.getOptions();
							for (Option option : options) {
								KQQuestionField field = kassService
										.getPersonField(pid, option.getLabel());
								if (field != null) {
									if (Constants.DEFAULT_DNA.equals(field
											.getValue()))
										field.setValue(Constants.DEFAULT_NA);
								}
							}
						} else {
							KQQuestionField field = kassService.getPersonField(
									pid, fquestion.getLabel());
							if (field != null) {
								if (Constants.DEFAULT_DNA.equals(field
										.getValue()))
									field.setValue(Constants.DEFAULT_NA);
							}
						}
					}
				}
			} else {
				KQQuestion kqq = kassService.getPersonQuestion(pid,
						question.getQid());
				if (kqq != null) {
					if (Constants.DEFAULT_DNA.equals(kqq.getValue()))
						kqq.setValue(Constants.DEFAULT_NA);
					List<KQQuestionField> fquestions = kqq.getFields();
					if (fquestions != null && fquestions.size() > 0) {
						for (KQQuestionField field : fquestions) {
							if (Constants.DEFAULT_DNA.equals(field.getValue()))
								field.setValue(Constants.DEFAULT_NA);
						}
					}
				}
			}
		}
	}


	/**
	 * Save UNION
	 * 
	 * @param cell
	 *            graphUnion
	 * @param kassService
	 */
	public void saveNewUnion(mxCell cell, KassService kassService) {
		GraphUnion graphUnion = (GraphUnion) cell.getValue();
		int id = graphUnion.getId();
		KUnions kunions = kassService.get().getUnions();
		KUUnion kuunion = new KUUnion();
		KPPCoords coord = new KPPCoords();
		coord.setX(cell.getGeometry().getX());
		coord.setY(cell.getGeometry().getY());
		kuunion.setCoords(coord);
		kuunion.setId(id);
		kuunion.setIsOther(graphUnion.getIsOther());
		kunions.getUnions().add(kuunion);
		this.initNewUnionDefault(kuunion);
		graphUnion.setKuunion(kuunion.clone());
		kassService.save();
	}

	public void saveNewEdge(GraphUnion gu, int pid, int type) {
		KassService kassService = new KassService();
		KUUnion union = kassService.getUnion(gu.getId());

		if (union != null) {
			List<KUUMember> members = union.getMembers();
			KUUMember m = null;
			for (KUUMember member : members) {
				if (member.getId() == pid) {
					m = member;
					break;
				}
			}
			if (m == null) {
				m = new KUUMember();
				m.setId(pid);
				if (type == 0) {
					m.setType("partner");
				} else {
					m.setType("offspring");
				}
				union.getMembers().add(m);
				gu.setKuunion(union.clone());
				kassService.save();
			}
		}
	}

	/**
	 * Check the completed status of a question
	 * 
	 * @param qid
	 *            the question Id
	 * @return
	 */
	public int checkComplete(String qid) {
		Question question = qService.searchQuestion(qid);
		String sid = question.getSubsection();
		if (sid.startsWith("SS_001") || sid.startsWith("SS_002")) {
			return 1;
		}
		List<KPPerson> persons = Constants.KASS.getPeople().getPersons();
		if (persons != null) {
			int unsize = 0;
			int allsize = 0;
			int insize = 0;
			for (KPPerson person : persons) {
				int z = checkPersonComplete(question, person.getPid());
				if (z == 0) {
					unsize++;
				} else if (z == 1) {
					allsize++;
				} else if (z == 2) {
					insize++;
				}
			}
			if (allsize == persons.size()) {
				return 1;
			} else if (unsize == persons.size()) {
				return 0;
			} else {
				return 2;
			}
		}
		return 0;
	}

	/**
	 * Check the completed status of a question of a person
	 * 
	 * @param question
	 * @param pid
	 * @return
	 */
	public int checkPersonComplete(Question question, int pid) {
		if (RegularUtil.isMap(question)) {
			KQQuestionField field = kassService.getPersonField(pid,
					question.getLabel());
			if (field != null && Constants.DEFAULT_NA.equals(field.getValue())) {
				return 0;
			} else {
				return 1;
			}
		} else if (RegularUtil.isPersonOrUnion(question)) {
			List<FollowupQuestion> fquestions = question.getFollowupQuestions();
			int unsize = 0;
			if (fquestions != null) {
				for (FollowupQuestion fquestion : fquestions) {
					if ("B".equals(fquestion.getType())) {
						List<Option> options = fquestion.getOptions();
						for (Option option : options) {
							KQQuestionField field = kassService.getPersonField(
									pid, option.getLabel());
							if (field != null
									&& Constants.DEFAULT_NA.equals(field
											.getValue())) {
								unsize++;
							}
						}
					} else {
						KQQuestionField field = kassService.getPersonField(pid,
								fquestion.getLabel());
						if (field != null
								&& Constants.DEFAULT_NA
										.equals(field.getValue())) {
							unsize++;
						}
					}
				}
				if (unsize == fquestions.size()) {
					return 0;
				} else if (unsize == 0) {
					return 1;
				} else {
					return 2;
				}
			} else {
				return 1;
			}
		}
		return 2;
	}

	public boolean checkUnionComplete(String qid) {
		Question question = qService.searchQuestion(qid);
		List<KUUnion> unions = Constants.KASS.getUnions().getUnions();
		if (unions != null) {
			for (KUUnion union : unions) {
				int i = checkUnionComplete(question, union.getId());
				if (i == 0 || i == 2) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Check the completed status of a question of a union
	 * 
	 * @param question
	 * @param uid
	 * @return
	 */
	public int checkUnionComplete(Question question, int uid) {
		List<FollowupQuestion> fquestions = question.getFollowupQuestions();
		int unsize = 0;
		if (fquestions != null) {
			for (FollowupQuestion fquestion : fquestions) {
				if ("B".equals(fquestion.getType())) {
					List<Option> options = fquestion.getOptions();
					for (Option option : options) {
						KQQuestionField field = kassService.getUnionField(uid,
								option.getLabel());
						if (field != null
								&& Constants.DEFAULT_NA
										.equals(field.getValue())) {
							unsize++;
						}
					}
				} else {
					KQQuestionField field = kassService.getUnionField(uid,
							fquestion.getLabel());
					if (field != null
							&& Constants.DEFAULT_NA.equals(field.getValue())) {
						unsize++;
					}
				}
			}
			if (unsize == fquestions.size()) {
				return 0;
			} else if (unsize == 0) {
				return 1;
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}

	/**
	 * Check the completed status of the question in the section 8
	 */
	public void checkAllPerson() {
		Subsection subsection = qService
				.searchSubsection(Constants.SUBSECTION_GENERAL);
		List<Question> questions = subsection.getQuestions();
		for (Question question : questions) {
			int s = checkComplete(question.getQid());
			KQuestion kquestion = kassService.getQuestion(question.getQid());
			if (s == 1) {
				kquestion.setCompleted("1");
				kquestion.setIsMarked(true);
			} else if (s == 2) {
				kquestion.setCompleted("2");
				kquestion.setIsMarked(true);
			} else if (s == 0) {
				kquestion.setCompleted("0");
				kquestion.setIsMarked(false);
			}
		}
		kassService.save();
	}

	public void checkAllUnion() {
		List<Question> questions = qService.searchUnionQuestion();
		for (Question question : questions) {
			boolean flag = checkUnionComplete(question.getQid());
			KQuestion kquestion = kassService.getQuestion(question.getQid());
			if (flag) {
				kquestion.setCompleted("1");
				kquestion.setIsMarked(true);
			} else {
				kquestion.setCompleted("2");
				kquestion.setIsMarked(true);
			}
		}
		kassService.save();
	}

	public void removeAll() {
		questionMap.clear();
	}

	/**
	 * Display or not display question in the tab by the DNA value
	 * 
	 * @param pid
	 * @param qids
	 */
	public void initDNAPerson(int pid, List<String> qids) {
		List<String> removes = new ArrayList<String>();
		for (int i = 0; i < qids.size(); i++) {
			boolean flag = isDNAPerson(pid, qids.get(i));
			if (flag) {
				removes.add(qids.get(i));
			}
		}
		qids.removeAll(removes);
	}

	private boolean isDNAPerson(int pid, String qid) {
		Question question = qService.searchQuestion(qid);
		List<FollowupQuestion> fquestions = question.getFollowupQuestions();
		if (fquestions != null && fquestions.size() > 0) {
			for (FollowupQuestion fquestion : fquestions) {
				if ("B".equals(fquestion.getType())) {
					List<Option> options = fquestion.getOptions();
					for (Option option : options) {
						KQQuestionField field = kassService.getPersonField(pid,
								option.getLabel());
						if (field != null) {
							if (!Constants.DEFAULT_DNA.equals(field.getValue())) {
								return false;
							}
						}
					}
				} else {
					KQQuestionField field = kassService.getPersonField(pid,
							fquestion.getLabel());
					if (field != null) {
						if (!Constants.DEFAULT_DNA.equals(field.getValue())) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Display or not display by the skip condition in the ifGo element
	 * 
	 * @param id
	 *            pid
	 * @param question
	 * @param value
	 * @param panels
	 * @param type
	 *            1 person; 2 union
	 * @return
	 */
	public List<String> isCheckShow(int id, Question question, String value,
			List<JPanel> panels, int type) {
		if (id == -1) {
			return null;
		}
		List<String> dnaList = new ArrayList<String>();
		if (StringUtils.isNotBlank(question.getIfGo())) {
			Map<String, List<String>> map = getValue(question.getIfGo());
			Iterator<Entry<String, List<String>>> iter = map.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (StringUtils.equals(key, value)) {
					for (String l : vals) {
						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field;
						if (type == 2) {
							field = kassService.getUnionField(id,
									fquestion.getLabel());
						} else {
							field = kassService.getPersonField(id,
									fquestion.getLabel());
						}

						if (field != null) {
							field.setValue(Constants.DEFAULT_DNA);
							setQuestionShow(panels, fquestion.getQid(), false);
							dnaList.add(l);
						}
					}
				}
			}
			iter = map.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (!StringUtils.equals(key, value)) {
					for (String l : vals) {
						if (dnaList.contains(l)) {
							continue;
						}
						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field;
						if (type == 2) {
							field = kassService.getUnionField(id,
									fquestion.getLabel());
						} else {
							field = kassService.getPersonField(id,
									fquestion.getLabel());
						}
						if (field != null) {
							if (Constants.DEFAULT_DNA.equals(field.getValue())) {
								field.setValue(Constants.DEFAULT_NA);
							}
							setQuestionShow(panels, fquestion.getQid(), true);
							List<String> dlist = this.isCheckShow(id,
									fquestion, field.getValue(), panels, type);
							dnaList.addAll(dlist);
						}
					}
				}
			}
		}
		hideLabelSame(panels);
		return dnaList;
	}

	public void setQuestionShow(List<JPanel> panels, String qid, boolean flag) {
		for (JPanel jpanel : panels) {
			String name = jpanel.getName();
			String id = name.replace("question_", "");
			if (StringUtils.equals(qid, id)) {
				if (flag) {
					showPanel(jpanel);
				} else {
					hidePanel(jpanel);
				}
			}
		}
	}

	public void showPanel(JPanel jpanel) {
		jpanel.setVisible(true);
		int count = jpanel.getComponentCount();
		for (int i = 0; i < count; i++) {
			Component component = jpanel.getComponent(i);
			if (component instanceof JTextField) {
				JTextField field = (JTextField) component;
				field.setText("");
			} else if (component instanceof JTextArea) {
				JTextArea noteField = (JTextArea) component;
				noteField.setText("");
			} else if (component instanceof JCheckBox) {
				JCheckBox field = (JCheckBox) component;
				field.setSelected(false);
			} else if (component instanceof JRadioButton) {
				JRadioButton field = (JRadioButton) component;
				field.setSelected(false);
			}
		}
		Container c = jpanel.getParent();
		if (c instanceof JPanel) {
			JPanel parent = (JPanel) c;
			if (StringUtils.isNotBlank(parent.getName())
					&& parent.getName().startsWith("group_")) {
				parent.setVisible(true);
			}
		}
	}

	public void hidePanel(JPanel jpanel) {
		jpanel.setVisible(false);
		Container c = jpanel.getParent();
		if (c instanceof JPanel) {
			JPanel parent = (JPanel) c;
			if (StringUtils.isNotBlank(parent.getName())
					&& parent.getName().startsWith("group_")) {
				int count = parent.getComponentCount();
				int invisableCount = 0;
				for (int i = 0; i < count; i++) {
					Component component = parent.getComponent(i);
					if (!component.isVisible()) {
						invisableCount++;
					}
				}
				if (invisableCount == count) {
					parent.setVisible(false);
				}
			}
		}
	}

	public void hideLabelSame(List<JPanel> panels) {
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			String name = jpanel.getName();
			String id = name.replace("question_", "");
			Question question = qService.searchQuestion(id);
			// Distinct the labelSame questions
			if (jpanel.isVisible()) {
				for (int j = i + 1; j < panels.size(); j++) {
					JPanel jpanel1 = panels.get(j);
					String name1 = jpanel1.getName();
					String id1 = name1.replace("question_", "");
					Question question1 = qService.searchQuestion(id1);
					if (StringUtils.equals(question.getLabel(),
							question1.getLabel())) {
						jpanel1.setVisible(false);
					}
				}
			}
		}
	}

	/**
	 * Initialize the display of question of the person
	 * 
	 * @param pid
	 * @param question
	 * @param value
	 * @param panels
	 */
	public void initPersonShow(int pid, Question question, String value,
			List<JPanel> panels) {
		if (pid == -1) {
			return;
		}
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			if (StringUtils.equals("question_" + question.getQid(),
					jpanel.getName())) {
				if (!panels.get(i).isVisible()) {
					return;
				}
			}
		}
		// for(int i = 0; i < panels.size(); i++) {
		// JPanel jpanel = panels.get(i);
		// System.out.println(question.getQid() + "  :  " + value
		// +"  :   111  : " + jpanel.getName() + "  : " + jpanel.isVisible());
		// }
		List<String> dnaList = new ArrayList<String>();
		if (StringUtils.isNotBlank(question.getIfGo())) {
			Map<String, List<String>> map = getValue(question.getIfGo());
			Iterator<Entry<String, List<String>>> iter = map.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (StringUtils.equals(key, value)) {
					for (String l : vals) {
						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field = kassService.getPersonField(pid,
								fquestion.getLabel());
						if (field != null) {
							setQuestionShow(panels, fquestion.getQid(), false);
							dnaList.add(l);
						}
					}
				}
			}
			iter = map.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (!StringUtils.equals(key, value)) {
					for (String l : vals) {
						if (dnaList.contains(l)) {
							continue;
						}
						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field = kassService.getPersonField(pid,
								fquestion.getLabel());
						if (field != null) {
							for (JPanel jpanel : panels) {
								String name = jpanel.getName();
								String id = name.replace("question_", "");
								if (StringUtils.equals(l, id)) {
									jpanel.setVisible(true);
									Container c = jpanel.getParent();
									if (c instanceof JPanel) {
										JPanel parent = (JPanel) c;
										if (StringUtils.isNotBlank(parent
												.getName())
												&& parent.getName().startsWith(
														"group_")) {
											parent.setVisible(true);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		// for(int i = 0; i < panels.size(); i++) {
		// JPanel jpanel = panels.get(i);
		// System.out.println(question.getQid() + "  :  " + value
		// +"  :   222  : " + jpanel.getName() + "  : " + jpanel.isVisible());
		// }
	}

	/**
	 * Initialize the display of the question of a union
	 * 
	 * @param uid
	 * @param question
	 * @param value
	 * @param panels
	 */
	public void initUnionShow(int uid, Question question, String value,
			List<JPanel> panels) {
		if (uid == -1) {
			return;
		}
		for (int i = 0; i < panels.size(); i++) {
			JPanel jpanel = panels.get(i);
			if (StringUtils.equals("question_" + question.getQid(),
					jpanel.getName())) {
				if (!panels.get(i).isVisible()) {
					return;
				}
			}
		}

		List<String> dnaList = new ArrayList<String>();
		if (StringUtils.isNotBlank(question.getIfGo())) {
			Map<String, List<String>> map = getValue(question.getIfGo());
			Iterator<Entry<String, List<String>>> iter = map.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (StringUtils.equals(key, value)) {
					for (String l : vals) {
						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field = kassService.getUnionField(uid,
								fquestion.getLabel());
						if (field != null) {
							setQuestionShow(panels, fquestion.getQid(), false);
							dnaList.add(l);
						}
					}
				}
			}
			iter = map.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<String, List<String>> entry = iter.next();
				String key = entry.getKey();
				List<String> vals = entry.getValue();
				if (!StringUtils.equals(key, value)) {
					for (String l : vals) {
						if (dnaList.contains(l)) {
							continue;
						}

						Question fquestion = qService.searchQuestion(l);
						KQQuestionField field = kassService.getUnionField(uid,
								fquestion.getLabel());
						if (field != null) {
							for (JPanel jpanel : panels) {
								String name = jpanel.getName();
								String id = name.replace("question_", "");
								if (StringUtils.equals(l, id)) {
									jpanel.setVisible(true);
									Container c = jpanel.getParent();
									if (c instanceof JPanel) {
										JPanel parent = (JPanel) c;
										if (StringUtils.isNotBlank(parent
												.getName())
												&& parent.getName().startsWith(
														"group_")) {
											parent.setVisible(true);
										}
									}
								}
							}
						}
					}
				}
			}

		}
	}

	/**
	 * Get the value by the field name
	 * 
	 * @param fieldName
	 * @return
	 */
	public String getLeftValue(String fieldName) {
		// 修改值
		Iterator<Entry<Question, List<Object>>> iter = questionMap.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Entry<Question, List<Object>> entry = iter.next();
			Question question = entry.getKey();
			List<Object> components = entry.getValue();
			if ("B".equals(question.getType())) {
				for (int j = 0; j < components.size(); j++) {
					JCheckBox cb = (JCheckBox) components.get(j);
					if (StringUtils.equals(cb.getName(), fieldName)) {
						if (cb.isSelected()) {
							return "true";
						} else {
							return "false";
						}
					}
				}
			} else if ("D".equals(question.getType())) {
				for (int j = 0; j < components.size(); j++) {
					JCheckBox cb = (JCheckBox) components.get(j);
					if (StringUtils.equals(cb.getName(), fieldName)) {
						if (cb.isSelected()) {
							return "true";
						} else {
							return "false";
						}
					}
				}
			} else if ("A".equals(question.getType())) {
				JTextField cb = (JTextField) components.get(0);
				if (StringUtils.equals(question.getLabel(), fieldName)) {
					return cb.getText();
				}
			} else if ("T".equals(question.getType())) {
				JTextArea cb = (JTextArea) components.get(0);
				if (StringUtils.equals(question.getLabel(), fieldName)) {
					return cb.getText();
				}
			} else if ("C".equals(question.getType())
					|| "Q".equals(question.getType())) {
				JRadioGroup group = (JRadioGroup) components.get(0);
				Enumeration<AbstractButton> ems = group.getElements();
				String value = "";
				while (ems.hasMoreElements()) {
					AbstractButton button = ems.nextElement();
					if (button.isSelected()) {
						value = button.getActionCommand();
						break;
					}
				}
				if (StringUtils.equals(question.getLabel(), fieldName)) {
					return value;
				}
			}
		}
		return "";
	}

	private String getNAByNull(String value) {
		if (StringUtils.isBlank(value)) {
			return Constants.DEFAULT_NA;
		}
		return value;
	}
}
