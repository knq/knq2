package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the question skip condition edit in the QMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class TextConditionDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5716481376438077142L;
	QuestionnaireService service;
	QuestionService questionService;
	JPanel panel1;
	JButton addBtn;
	double oldHeight = 0;

	public TextConditionDialog(Map<String, Object> params) {
		super(params);
	}

	public void initDialog() {
		this.setTitle(mxResources.get("LQueSkipCondition"));
		service = new QuestionnaireService();
		questionService = new QuestionService();
	}

	public JPanel createPanel() {

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		panel1 = new JPanel();
		panel1.setBackground(Color.white);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		String type = (String) params.get("type");
		String ifGo = "";
		Subsection parent = null;
		if ("section".equals(type)) {
			String sid = (String) params.get("sid");
			Subsection subsection = service.searchSubsection(sid);
			// ifGo = subsection.getIfGo();
			parent = service.searchParentSubsection(sid);
			if (parent == null) {
				parent = subsection;
			}
		} else {
			String qid = (String) params.get("qid");
			Question question = service.searchQuestion(qid);
			ifGo = question.getIfGo();
			Subsection subsection = service.searchSubsection(question
					.getSubsection());
			if (subsection == null) {
				Question q = service.searchParanetQuestion(qid);
				parent = service.searchParentSubsection(q.getSubsection());
			} else {
				parent = service.searchParentSubsection(subsection.getSid());
			}
			if (parent == null) {
				parent = subsection;
			}
		}
		List<Question> questions = questionService.listAllQuestion(parent);
		final ComboItem[] questionItems = new ComboItem[questions.size() + 1];
		questionItems[0] = new ComboItem("", "");
		for (int i = 0; i < questions.size(); i++) {
			Question q = questions.get(i);
			ComboItem item = new ComboItem(q.getQid(), q.getQid());
			questionItems[i + 1] = item;
		}
		if (StringUtils.isNotBlank(ifGo)) {
			String[] args = ifGo
					.split(com.fangda.kass.util.Constants.SPLIT_OBJECT);
			int size = 0;
			for (String arg : args) {
				String[] ps = arg
						.split(com.fangda.kass.util.Constants.SPLIT_PROPERTY);
				if (ps.length == 3) {
					JPanel jp = genRow(questionItems, ps[0], ps[1], ps[2]);
					panel1.add(jp);
					size++;
				}
			}
			if (size <= 0) {
				JPanel jp = genRow(questionItems, "", "", "");
				panel1.add(jp);
			}
		} else {
			JPanel jp = genRow(questionItems, "", "", "");
			panel1.add(jp);
		}
		panel1.addComponentListener(componentlistener);

		addBtn = new JButton("Add One Row");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel jpanel = genRow(questionItems, "", "", "");
				panel1.add(jpanel);
				panel1.updateUI();
			}
		});
		panel.add(addBtn, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		panel.add(panel1, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		return panel;
	}

	private JPanel genRow(ComboItem[] qitems, String checkbox,
			String textField, String jbox) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		JCheckBox cb = new JCheckBox("");
		if ("true".equals(checkbox)) {
			cb.setSelected(true);
		} else {
			cb.setSelected(false);
		}
		UIHelper.addToBagpanel(panel, Color.gray, cb, c, 0, 0, 1, 1,
				GridBagConstraints.EAST);
		JTextField field = new JTextField(textField, 40);
		if (StringUtil.isBlank(textField)) {
			field.setText(mxResources.get("questionnaire.skip.question"));
		}

		UIHelper.addToBagpanel(panel, Color.gray, field, c, 1, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionnaire.skip.goto") + ": "),
				c, 2, 0, 1, 1, GridBagConstraints.WEST);
		JComboBox box = new JComboBox(qitems);
		int index = getSelectIndex(box, jbox);
		box.setSelectedIndex(index);
		UIHelper.addToBagpanel(panel, Color.gray, box, c, 3, 0, 1, 1,
				GridBagConstraints.EAST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, 0, 1, 1,
				GridBagConstraints.EAST);
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				panel1.updateUI();
			}
		});
		return panel;
	}

	private void deleteRow(JButton deleteBtn) {
		JPanel jpanel = (JPanel) deleteBtn.getParent();
		deleteBtn.getParent().getParent().remove(jpanel);
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub

		return true;
	}

	/**
	 * After open a questionnaire
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel1) {
			Dimension currSize = (Dimension) panel1.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				double change = currSize.getHeight() - oldHeight;
				Dimension dialogSize = (Dimension) this.getSize().clone();
				dialogSize.height = (int) (dialogSize.height + change);
				this.setSize(dialogSize);
				oldHeight = currSize.getHeight();
			}
		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		Component[] components = panel1.getComponents();

		if (components != null && components.length > 0) {
			List<String> list = new ArrayList<String>();
			for (Component jpanel : components) {
				if (jpanel instanceof JPanel) {
					JPanel jp = (JPanel) jpanel;
					Component[] cs = jp.getComponents();
					if (cs != null && cs.length > 0) {
						StringBuffer sb = new StringBuffer();
						for (Component c : cs) {
							if (c instanceof JCheckBox) {
								sb.append(((JCheckBox) c).isSelected())
										.append(com.fangda.kass.util.Constants.JOIN_PROPERTY);
							}
							if (c instanceof JTextField) {
								sb.append(((JTextField) c).getText())
										.append(com.fangda.kass.util.Constants.JOIN_PROPERTY);
							}
							if (c instanceof JComboBox) {
								ComboItem item = (ComboItem) ((JComboBox) c)
										.getSelectedItem();
								sb.append(item.getValue());
							}
						}
						list.add(sb.toString());
					}
				}
			}
			String type = (String) params.get("type");
			if ("section".equals(type)) {
				String sid = (String) params.get("sid");
				Subsection subsection = service.searchSubsection(sid);
				// subsection.setIfGo(StringUtil.join(list.toArray(),
				// com.fangda.kass.util.Constants.JOIN_OBJECT));
			} else {
				String qid = (String) params.get("qid");
				Question question = service.searchQuestion(qid);
				question.setIfGo(StringUtil.join(list.toArray(),
						com.fangda.kass.util.Constants.JOIN_OBJECT));
			}
		}
		service.save(service.get());
		return new MessageModel(true, "Success", 0);
	}

	private int getSelectIndex(JComboBox jc, String value) {
		int size = jc.getModel().getSize();
		for (int i = 0; i < size; i++) {
			ComboItem item = (ComboItem) jc.getModel().getElementAt(i);
			if (item.getValue().equals(value)) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}
}
