package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the question edit of the questionnaire.
 * 
 * @author Fangfang Zhao, Jianguo Zhou, Shuang Ma, Jing Kong
 * 
 */
public class QuestionEditDialog extends BaseDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3713347014436486760L;

	QuestionnaireService service;

	JLabel sectionLabel;
	JLabel orderNoLabel;
	JLabel qidLabel;
	JComboBox relevant;
	JComboBox questionTypeBox;
	JComboBox dataTypeBox;
	JComboBox leadQuestionTypeBox;
	JTextField queTitle;
	JTextField queLable;
	JTextField lengthField;

	JTextField groupNo;
	JTextField groupName;

	JComboBox layoutBox;

	JScrollPane leadJSPane;
	JScrollPane noteJSPane;
	JTextArea queLead;
	JScrollPane detailJSPane;
	JTextArea queDetail;
	JTextArea queNote;

	JButton relevanceTypeBtn;
	JButton detailCopyLeadBtn;
	JComboBox direction;
	JComboBox isDisable;
	JButton configBt;
	// JButton dataTypeBtn;
	JTable table;
	JPanel tableOuterPanel;
	JPanel tablePanel;
	boolean isFollowup = false;
	JPanel panel;
	JPanel lengthPanel;
	String sectionId;
	String hsid = "";

	public QuestionEditDialog(Map<String, Object> params) {
		super(params);
	}

	public void initDialog() {
		this.setTitle("");
		service = new QuestionnaireService();
		if ("add".equals(params.get("op"))) {
			if (params.get("qid") != null && !"".equals(params.get("qid"))) {
				isFollowup = true;
			}
		} else {
			Question question = service.searchQuestion((String) params
					.get("qid"));
			if (question instanceof FollowupQuestion) {
				isFollowup = true;
			}
		}
	}

	public JPanel createPanel() {
		sectionId = getSectionId();
		sectionLabel = new JLabel("");
		orderNoLabel = new JLabel("");
		Object[] relevantArray = QuestionConstants.listRelevant();
		relevant = new JComboBox(relevantArray);
		qidLabel = new JLabel("");

		Object[] directionArray = QuestionConstants.listDirection();
		direction = new JComboBox(directionArray);

		Object[] isDisableArray = QuestionConstants.listDisable();
		isDisable = new JComboBox(isDisableArray);
		isDisable.removeItemAt(2);
		String[] columnNames = { "id", "SortNo", "Detail", "Value", "Note",
				"Layout", "DefaultValue", "Disable", "Color", "Label",
				"En_Detail", "En_Note" }; // 列名
		String[][] tableVales = {}; // 数据
		DefaultTableModel tableModel = new DefaultTableModel(tableVales,
				columnNames);
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // 单选
		DefaultTableColumnModel dcm = (DefaultTableColumnModel) table
				.getColumnModel();
		dcm.getColumn(1).setMinWidth(0);
		dcm.getColumn(1).setMaxWidth(0);
		dcm.getColumn(4).setMinWidth(0);
		dcm.getColumn(4).setMaxWidth(0);
		dcm.getColumn(5).setMinWidth(0);
		dcm.getColumn(5).setMaxWidth(0);
		dcm.getColumn(6).setMinWidth(0);
		dcm.getColumn(6).setMaxWidth(0);
		dcm.getColumn(7).setMinWidth(0);
		dcm.getColumn(7).setMaxWidth(0);
		dcm.getColumn(8).setMinWidth(0);
		dcm.getColumn(8).setMaxWidth(0);
		dcm.getColumn(10).setMinWidth(0);
		dcm.getColumn(10).setMaxWidth(0);
		dcm.getColumn(11).setMinWidth(0);
		dcm.getColumn(11).setMaxWidth(0);

		Object[] questionArray = QuestionConstants.listQuestionType();
		questionTypeBox = new JComboBox(questionArray);
		Subsection subsection = service.searchSubsection(sectionId);
		if (StringUtils.isBlank(subsection.getSectionMode())) {
			subsection = service.searchParentSubsection(sectionId);
		}
		Object[] leadQuestionArray = QuestionConstants
				.getLeadQuestionFiled(subsection.getSectionMode());
		leadQuestionTypeBox = new JComboBox(leadQuestionArray);
		Object[] dataTypeArray = QuestionConstants.listDataType();
		dataTypeBox = new JComboBox(dataTypeArray);
		queTitle = new JTextField("");
		queLable = new JTextField("");
		queLead = new JTextArea("", 2, 36);
		queLead.setLineWrap(true);
		queLead.setWrapStyleWord(true);
		leadJSPane = new JScrollPane(queLead);
		leadJSPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		leadJSPane.setPreferredSize(new Dimension(150, 50));
		detailCopyLeadBtn = new JButton(mxResources.get("detailCopyLeadBtn"));
		queDetail = new JTextArea("", 2, 36);
		queDetail.setLineWrap(true);
		queDetail.setWrapStyleWord(true);
		detailJSPane = new JScrollPane(queDetail);
		detailJSPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		detailJSPane.setPreferredSize(new Dimension(150, 50));

		queNote = new JTextArea("", 2, 36);
		queNote.setLineWrap(true);
		queNote.setWrapStyleWord(true);
		noteJSPane = new JScrollPane(queNote);
		noteJSPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		noteJSPane.setPreferredSize(new Dimension(150, 50));
		lengthField = new JTextField(10);
		lengthField.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				// 只能输入数字
				char keyCh = e.getKeyChar();
				if ((keyCh < '0') || (keyCh > '9')) {
					if (keyCh != '') // 回车字符
						e.setKeyChar('\0');
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		groupNo = new JTextField(10);
		groupNo.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				// 只能输入数字
				char keyCh = e.getKeyChar();
				if ((keyCh < '0') || (keyCh > '9')) {
					if (keyCh != '') // 回车字符
						e.setKeyChar('\0');
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		groupName = new JTextField(10);
		Object[] layoutArray = QuestionConstants.listLayout();
		layoutBox = new JComboBox(layoutArray);

		configBt = BtnHelper.createConfigButton("");
		relevanceTypeBtn = new JButton(mxResources.get("defineRelevanceType"));
		// dataTypeBtn = new JButton(mxResources.get("dataType"));
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		panel = new JPanel(bagLayout);
		lengthPanel = new JPanel(bagLayout);
		int index = 0;

		String qid = (String) params.get("qid");
		final Question question = service.searchQuestion(qid);
		if (!isFollowup) {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("subSection") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, sectionLabel, c, 1,
					index, 3, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("orderNumber") + ":"), c, 3,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, orderNoLabel, c, 4,
					index, 1, 1, GridBagConstraints.EAST);
			index++;

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("qid") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, qidLabel, c, 1, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("direction") + ":"), c, 3,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, direction, c, 4, index,
					1, 1, GridBagConstraints.EAST);

			index++;
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("title") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, queTitle, c, 1, index, 5,
					1, GridBagConstraints.EAST);
			index++;
		} else {

			Question fquestion = service.searchParanetQuestion(qid);
			if (fquestion == null) {
				fquestion = service.searchQuestion(qid);
			}
			UIHelper.addToBagpanel(
					panel,
					Color.gray,
					new JLabel(mxResources.get("question") + " "
							+ fquestion.getQid() + ":"), c, 0, index, 1, 1,
					GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(fquestion.getTitle()), c, 1, index, 3, 1,
					GridBagConstraints.EAST);
			index++;

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("fid") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, qidLabel, c, 1, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("orderNumber") + ":"), c, 3,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, orderNoLabel, c, 4,
					index, 1, 1, GridBagConstraints.EAST);
			index++;
		}
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("label") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, queLable, c, 1, index, 5, 1,
				GridBagConstraints.EAST);

		if (!isFollowup) {
			if (question != null && question.getLabel() != null
					&& question.getLabel() != "") {

			} else {
				queLable.addMouseListener(new MouseListener() {

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						if (queLable.getText().isEmpty()) {
							ComboItem item = (ComboItem) direction
									.getSelectedItem();

							queLable.setText(queTitle.getText() + "_"
									+ item.getValue());
							// }
						}
					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseClicked(MouseEvent e) {
						// TODO Auto-generated method stub

					}
				});
			}
		} else {
			final Question fquestion = service.searchParanetQuestion(qid);
			queLable.addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					if (queLable.getText().isEmpty()) {
						if (fquestion != null) {
						} else {
							final String fid = qidLabel.getText().substring(
									qidLabel.getText().length() - 1,
									qidLabel.getText().length());
							queLable.setText(question.getLabel() + "_" + fid);
						}
					}
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub

				}
			});
		}

		index++;
		if (!isFollowup) {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("lead") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, leadJSPane, c, 1, index,
					5, 1, GridBagConstraints.EAST);
			index++;
		}

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("detail") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		if (!isFollowup) {
			UIHelper.addToBagpanel(panel, Color.gray, detailCopyLeadBtn, c, 0,
					index + 1, 1, 1, GridBagConstraints.EAST);

			detailCopyLeadBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					queDetail.setText(queLead.getText());
				}
			});
		}
		UIHelper.addToBagpanel(panel, Color.gray, detailJSPane, c, 1, index, 5,
				2, GridBagConstraints.EAST);
		index++;
		index++;

		/*
		 * UIHelper.addToBagpanel(panel, Color.gray, new
		 * JLabel(mxResources.get("note") + ":"), c, 0, index, 1, 1,
		 * GridBagConstraints.EAST); UIHelper.addToBagpanel(panel, Color.gray,
		 * noteJSPane, c, 1, index, 5, 1, GridBagConstraints.EAST); index++;
		 */

		if (!isFollowup) {
			/*
			 * UIHelper.addToBagpanel(panel, Color.gray, new
			 * JLabel(mxResources.get("isAuto") + ":"), c, 0, index, 1, 1,
			 * GridBagConstraints.EAST); UIHelper.addToBagpanel(panel,
			 * Color.gray, isAutoTrue, c, 1, index, 1, 1,
			 * GridBagConstraints.EAST); UIHelper.addToBagpanel(panel,
			 * Color.gray, isAutoFalse, c, 2, index, 1, 1,
			 * GridBagConstraints.EAST);
			 * 
			 * UIHelper.addToBagpanel(panel, Color.gray, new
			 * JLabel(mxResources.get("askOthers") + ":"), c, 3, index, 1, 1,
			 * GridBagConstraints.EAST); UIHelper.addToBagpanel(panel,
			 * Color.gray, askOthersTrue, c, 4, index, 1, 1,
			 * GridBagConstraints.EAST); UIHelper.addToBagpanel(panel,
			 * Color.gray, askOthersFalse, c, 5, index, 1, 1,
			 * GridBagConstraints.EAST); index++;
			 */

		}
		if (!isFollowup) {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("type") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, leadQuestionTypeBox, c,
					1, index, 2, 1, GridBagConstraints.EAST);
			leadQuestionTypeBox.addActionListener(this);
			index++;
		} else {

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("type") + ":"), c, 0, index, 1,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, questionTypeBox, c, 1,
					index, 2, 1, GridBagConstraints.EAST);
			questionTypeBox.addActionListener(this);

			UIHelper.addToBagpanel(lengthPanel, Color.gray, new JLabel(
					mxResources.get("length") + ":"), c, 0, index, 1, 1,
					GridBagConstraints.WEST);
			UIHelper.addToBagpanel(lengthPanel, Color.gray, lengthField, c, 1,
					index, 2, 1, GridBagConstraints.WEST);
			UIHelper.addToBagpanel(panel, Color.gray, lengthPanel, c, 3, index,
					3, 1, GridBagConstraints.WEST);

			index++;
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("dataTypeLb") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, dataTypeBox, c, 1, index,
					2, 1, GridBagConstraints.EAST);
			// UIHelper.addToBagpanel(panel, Color.gray, dataTypeBtn, c, 3,
			// index,
			// 2, 1, GridBagConstraints.EAST);

			index++;

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("groupNo") + ":"), c, 0, index,
					1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, groupNo, c, 1, index, 2,
					1, GridBagConstraints.EAST);
			index++;

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("groupName") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, groupName, c, 1, index,
					2, 1, GridBagConstraints.EAST);
			index++;

			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("layout") + ":"), c, 0, index,
					1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, layoutBox, c, 1, index,
					2, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("layoutNote")), c, 3, index, 2,
					1, GridBagConstraints.EAST);

			index++;

		}
		if (!isFollowup) {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("relevance") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);

			UIHelper.addToBagpanel(panel, Color.gray, relevant, c, 1, index, 2,
					1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, relevanceTypeBtn, c, 3,
					index, 3, 1, GridBagConstraints.EAST);
			index++;
			relevanceTypeBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					(new RelevanceTypeDialog()).setMainPanel(km);
				}
			});
		}

		if (question != null && question.getDisable() == 0) {// DISABLE去除
			// panel.remove(panel.getComponentCount() - 3);
			// panel.remove(panel.getComponentCount() - 2);
			isDisable.setEnabled(false);
			panel.updateUI();
		} else {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("isDisable") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, isDisable, c, 1, index,
					2, 1, GridBagConstraints.EAST);

			index++;
		}
		tableOuterPanel = this.genOptionTable();
		UIHelper.addToBagpanel(panel, Color.gray, tableOuterPanel, c, 0, index,
				6, 1, GridBagConstraints.EAST);
		index++;
		return panel;
	}

	public JPanel genOptionTable() {
		GridBagLayout bagLayout1 = new GridBagLayout();
		JPanel jpanel1 = new JPanel(bagLayout1);

		GridBagLayout bagLayout = new GridBagLayout();
		JPanel jpanel = new JPanel(bagLayout);
		jpanel.setBorder(BorderFactory.createTitledBorder("Option"));
		JButton addButton = BtnHelper.createAddButton("");
		JButton configButton = BtnHelper.createConfigButton("");
		JButton upButton = BtnHelper.createUpButton("");
		JButton downButton = BtnHelper.createDownButton("");
		JButton deleteButton = BtnHelper.createDeleteButton("");
		JToolBar jtoolbarSection = new JToolBar();
		jtoolbarSection.add(addButton);
		jtoolbarSection.add(configButton);
		jtoolbarSection.add(upButton);
		jtoolbarSection.add(downButton);
		jtoolbarSection.add(deleteButton);
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("op", "add");
				map.put("table", table);
				if (StringUtils.isNotBlank(queLable.getText())) {
					map.put("qlabel", queLable.getText());
				}
				if (StringUtils.isNotBlank(qidLabel.getText())) {
					map.put("qid", qidLabel.getText());
				}
				(new QuestionOptionEditDialog(map)).setMainPanel(km);
			}
		});
		configButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRowCount() != 0) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "edit");
					map.put("table", table);
					if (StringUtils.isNotBlank(queLable.getText())) {
						map.put("qlabel", queLable.getText());
					}
					if (StringUtils.isNotBlank(qidLabel.getText())) {
						map.put("qid", qidLabel.getText());
					}
					(new QuestionOptionEditDialog(map)).setMainPanel(km);
				} else {
					JOptionPane.showMessageDialog(null,
							mxResources.get("selectOption"), "Error",
							JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveUp();
			}
		});
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveDown();
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteOption();
			}
		});

		jpanel.add(jtoolbarSection, new GridBagConstraints(0, 0, 1, 1, 1.0,
				1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(100, 100));

		jpanel.add(scrollPane, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));

		jpanel1.add(jpanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));
		tablePanel = jpanel;
		return jpanel1;
	}

	public void moveUp() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		int row = table.getSelectedRow();
		if (row <= 0) {
			return;
		}
		tableModel.moveRow(row, row, row - 1);
		table.setRowSelectionInterval(row - 1, row - 1);
	}

	public void moveDown() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		int row = table.getSelectedRow();
		if (row >= tableModel.getRowCount() - 1) {
			return;
		}
		tableModel.moveRow(row, row, row + 1);
		table.setRowSelectionInterval(row + 1, row + 1);
	}

	public void deleteOption() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		int row = table.getSelectedRow();
		tableModel.removeRow(row);
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		if (!isFollowup) {
			if ("".equals(queTitle.getText())) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("queTitleNull") + "</HTML>",
						"Warning", JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				return false;
			}
			if ("".equals(queLead.getText())) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("queLeadNull") + "</HTML>",
						"Warning", JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				return false;
			}
		}
		if ("".equals(queDetail.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("queDetailNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (dataTypeBox.getSelectedItem() == null) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("dataTypeNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (isFollowup) {
			ComboItem item = (ComboItem) questionTypeBox.getSelectedItem();
			if ("A".equals(item.getValue()) && "".equals(lengthField.getText())) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("lengthNull") + "</HTML>",
						"Warning", JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				return false;
			}
		}
		String op = (String) params.get("op");
		if ("add".equals(op) && service.getSameByLabel(queLable.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("labelCheckSame") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * After open the Dialog
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		String op = (String) params.get("op");
		String qid = (String) params.get("qid");
		String sid = (String) params.get("sid");
		if ("add".equals(op)) {
			if (StringUtil.isNotBlank(qid)) {
				this.setTitle("Add Followup Question");
				Question question = service.searchQuestion(qid);
				sectionLabel.setText(question.getSubsection());
				Map<String, String> map = service.genQuestionId(qid, sid, "");
				orderNoLabel.setText(map.get("sortNo"));
				qidLabel.setText(map.get("qestionId"));
				tableOuterPanel.setPreferredSize(new Dimension(tableOuterPanel
						.getWidth(), tableOuterPanel.getHeight()));
				tablePanel.setVisible(false);
			} else {
				if (StringUtil.isNotBlank(sid)) {
					this.setTitle("Add Question");
					Subsection subsection = service.searchSubsection(sid);
					Map<String, String> map = service
							.genQuestionId("", sid, "");
					orderNoLabel.setText(map.get("sortNo"));
					qidLabel.setText(map.get("qestionId"));
					sectionLabel.setText(subsection.getSid());
				}
			}
		} else if ("edit".equals(op)) {
			Question question = service.searchQuestion(qid);
			if (question instanceof FollowupQuestion) {
				this.setTitle("Edit Followup Question");
			} else {
				this.setTitle("Edit Question");
			}
			qidLabel.setText(question.getQid());
			orderNoLabel.setText(String.valueOf(question.getSortNo()));
			queLable.setText(question.getLabel());
			if (!isFollowup) {
				relevant.setSelectedIndex(QuestionConstants
						.getRelevantIndex(question.getRelevance()));
				if (question.getDisable() != 0) {
					isDisable.setSelectedIndex(QuestionConstants
							.getDisableIndex("" + question.getDisable()));
				}

				direction.setSelectedIndex(QuestionConstants
						.getDirectionIndex(question.getDirection()));
				queLead.setText(question.getLead());
				queTitle.setText(question.getTitle());

				Subsection subsection = service.searchSubsection(sectionId);
				if (StringUtils.isBlank(subsection.getSectionMode())) {
					subsection = service.searchParentSubsection(sectionId);
				}
				int typeIndex = QuestionConstants.getLeadQuestionTypeIndex(
						question.getType(), subsection.getSectionMode());
				leadQuestionTypeBox.setSelectedIndex(typeIndex);
			} else {
				if (question.getDisable() != 0) {
					isDisable.setSelectedIndex(QuestionConstants
							.getDisableIndex("" + question.getDisable()));
				}
				FollowupQuestion fquestion = (FollowupQuestion) question;
				if (fquestion.getLength() != null) {
					lengthField.setText("" + fquestion.getLength());
				}
				questionTypeBox.setSelectedIndex(QuestionConstants
						.getQuestionTypeIndex(question.getType()));

				if (fquestion.getDataType() != null) {
					dataTypeBox.setSelectedIndex(QuestionConstants
							.getDataTypeIndex(fquestion.getDataType()));
				} else {
					dataTypeBox.setSelectedIndex(-1);
				}
				groupNo.setText(fquestion.getGroupNo());
				groupName.setText(fquestion.getGroupName());

				if (fquestion.getLayout() != null) {
					layoutBox.setSelectedIndex(QuestionConstants
							.getLayout(fquestion.getLayout()));
				} else {
					layoutBox.setSelectedIndex(0);
				}

			}
			sectionLabel.setText(question.getSubsection());
			queDetail.setText(question.getDetail());
			// queNote.setText(question.getNote());
			List<Option> options = question.getOptions();
			if (options != null && options.size() > 0) {
				for (Option option : options) {
					DefaultTableModel tableModel = (DefaultTableModel) table
							.getModel();
					// "id", "SortNo", "Detail", "Value", "Note", "Layout",
					// "DefaultValue", "Disable","Color"
					tableModel.addRow(new Object[] { option.getOid(),
							option.getSortNo(), option.getDetail(),
							option.getValue(), option.getNote(),
							option.getLayout(), option.getDefaultValue(),
							"" + option.getDisable(), "", option.getLabel(),
							option.getEn_detail(), option.getEn_note() });
				}
			}
			if ("A".equals(question.getType())
					|| "T".equals(question.getType())
					|| "N".equals(question.getType())) {
				tableOuterPanel.setPreferredSize(new Dimension(tableOuterPanel
						.getWidth(), tableOuterPanel.getHeight()));
				tablePanel.setVisible(false);
			}

		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		String op = (String) params.get("op");
		try {
			if ("add".equals(op)) {
				String sid = (String) params.get("sid");
				String qid = (String) params.get("qid");
				if (StringUtil.isNotBlank(sid)) {
					Subsection subsection = service.searchSubsection(sid);
					ComboItem item = (ComboItem) leadQuestionTypeBox
							.getSelectedItem();
					Map<String, String> map = service.genQuestionId("", sid,
							item.getValue());
					Question question = new Question();
					question.setQid(map.get("qestionId"));
					question.setSortNo(Integer.parseInt(map.get("sortNo")));
					question.setSubsection(subsection.getSid());
					question.setTitle(StringUtil.trimToNull(queTitle.getText()));
					question.setLabel(StringUtil.trimToNull(queLable.getText()));
					question.setLead(StringUtil.trimToNull(queLead.getText()));
					question.setDetail(StringUtil.trimToNull(queDetail
							.getText()));
					ComboItem directionItem = (ComboItem) direction
							.getSelectedItem();
					question.setDirection(directionItem.getValue());
					ComboItem typeItem = (ComboItem) leadQuestionTypeBox
							.getSelectedItem();
					question.setType(typeItem.getValue());
					ComboItem relevantItem = (ComboItem) relevant
							.getSelectedItem();
					question.setRelevance(relevantItem.getValue());
					ComboItem isdisableItem = (ComboItem) isDisable
							.getSelectedItem();
					question.setDisable(Integer.parseInt(isdisableItem
							.getValue()));
					saveQuestionOption(question);
					subsection.getQuestions().add(question);
					params.put("hqid", question.getQid());
				} else {
					if (StringUtil.isNotBlank(qid)) {
						Question question = service.searchQuestion(qid);
						Map<String, String> map = service.genQuestionId(qid,
								"", "");
						FollowupQuestion fquestion = new FollowupQuestion();
						fquestion.setQid(map.get("qestionId"));
						fquestion
								.setSortNo(Integer.parseInt(map.get("sortNo")));
						fquestion.setDetail(queDetail.getText());
						ComboItem typeItem = (ComboItem) questionTypeBox
								.getSelectedItem();
						fquestion.setType(typeItem.getValue());
						if (StringUtils.isNotBlank(lengthField.getText())) {
							fquestion.setLength(Integer.parseInt(lengthField
									.getText()));
						}
						ComboItem dataTypeItem = (ComboItem) dataTypeBox
								.getSelectedItem();
						fquestion.setDataType(dataTypeItem.getValue());
						ComboItem isdisableItem = (ComboItem) isDisable
								.getSelectedItem();
						fquestion.setDisable(Integer.parseInt(isdisableItem
								.getValue()));
						fquestion.setGroupNo(StringUtil.trimToNull(groupNo
								.getText()));
						fquestion.setGroupName(StringUtil.trimToNull(groupName
								.getText()));
						ComboItem layoutItem = (ComboItem) layoutBox
								.getSelectedItem();
						fquestion.setLayout(StringUtil.trimToNull(layoutItem
								.getValue()));

						fquestion.setLabel(StringUtil.trimToNull(queLable
								.getText()));
						saveQuestionOption(fquestion);
						question.getFollowupQuestions().add(fquestion);

						params.put("hqid", question.getQid());
					}
				}
			} else {
				String qid = (String) params.get("qid");
				Question question = service.searchQuestion(qid);
				question.setLabel(queLable.getText());
				if (isFollowup) {
					FollowupQuestion fquestion = (FollowupQuestion) question;
					question.setDetail(queDetail.getText());
					ComboItem isdisableItem = (ComboItem) isDisable
							.getSelectedItem();
					question.setDisable(Integer.parseInt(isdisableItem
							.getValue()));
					if (StringUtils.isNotBlank(lengthField.getText())) {
						fquestion.setLength(Integer.parseInt(lengthField
								.getText()));
					}
					fquestion.setGroupNo(StringUtil.trimToNull(groupNo
							.getText()));
					fquestion.setGroupName(StringUtil.trimToNull(groupName
							.getText()));
					ComboItem layoutItem = (ComboItem) layoutBox
							.getSelectedItem();
					fquestion.setLayout(StringUtil.trimToNull(layoutItem
							.getValue()));

					ComboItem dataTypeItem = (ComboItem) dataTypeBox
							.getSelectedItem();
					fquestion.setDataType(dataTypeItem.getValue());
					ComboItem typeItem = (ComboItem) questionTypeBox
							.getSelectedItem();
					question.setType(typeItem.getValue());
				} else {
					question.setTitle(StringUtil.trimToNull(queTitle.getText()));
					question.setLead(StringUtil.trimToNull(queLead.getText()));
					question.setDetail(StringUtil.trimToNull(queDetail
							.getText()));
					ComboItem directionItem = (ComboItem) direction
							.getSelectedItem();
					question.setDirection(directionItem.getValue());
					ComboItem relevantItem = (ComboItem) relevant
							.getSelectedItem();
					question.setRelevance(relevantItem.getValue());
					ComboItem isdisableItem = (ComboItem) isDisable
							.getSelectedItem();
					question.setDisable(Integer.parseInt(isdisableItem
							.getValue()));
					ComboItem typeItem = (ComboItem) leadQuestionTypeBox
							.getSelectedItem();
					question.setType(typeItem.getValue());
				}
				params.put("hqid", question.getQid());
				saveQuestionOption(question);
			}
			service.save(service.get());
			return new MessageModel(true, "Submit success!");
		} catch (Exception e) {
			e.printStackTrace();
			return new MessageModel(false, "Submit failture:" + e.getMessage());
		}
	}

	private void saveQuestionOption(Question question) {
		if (!"A".equals(question.getType()) && !"T".equals(question.getType())) {
			DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
			List<Option> options = new ArrayList<Option>();
			for (int i = 0; i < tableModel.getRowCount(); i++) {
				// "id", "SortNo", "Detail", "Value", "Note", "Layout",
				// "DefaultValue", "Disable","Color", "Label"
				String oid = (String) tableModel.getValueAt(i, 0);
				String value = (String) tableModel.getValueAt(i, 3);
				if (StringUtil.isBlank(oid)) {
					oid = genQuestionOption(tableModel, question.getQid());
					tableModel.setValueAt(oid, i, 0);
				}
				String detail = (String) tableModel.getValueAt(i, 2);
				String note = (String) tableModel.getValueAt(i, 4);
				String layout = (String) tableModel.getValueAt(i, 5);
				String defaultValue = (String) tableModel.getValueAt(i, 6);
				String disable = (String) tableModel.getValueAt(i, 7);
				String label = (String) tableModel.getValueAt(i, 9);
				String enDetail = (String) tableModel.getValueAt(i, 10);
				String enNote = (String) tableModel.getValueAt(i, 11);
				Option option = new Option();
				option.setOid(oid);
				option.setDisable(Integer.parseInt(disable));
				option.setDefaultValue(defaultValue);
				option.setLayout(StringUtil.trimToNull(layout));
				option.setDetail(StringUtil.trimToNull(detail));
				option.setNote(StringUtil.trimToNull(note));
				option.setSortNo(i);
				option.setValue(StringUtil.trimToNull(value));
				option.setLabel(StringUtil.trimToNull(label));
				option.setEn_detail(StringUtil.trimToNull(enDetail));
				option.setEn_note(StringUtil.trimToNull(enNote));
				options.add(option);
			}
			question.setOptions(options);
		}
	}

	private String genQuestionOption(DefaultTableModel tableModel,
			String questionId) {
		int max = -1;
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			String oid = (String) tableModel.getValueAt(i, 0);
			if (StringUtils.isNotBlank(oid)) {
				int num = Integer.parseInt(StringUtil.substringAfterLast(oid,
						"_"));
				max = Math.max(num, max);
			}
		}
		return questionId + "_" + (max + 1);

	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {
		// Reload
		try {
			int height = this.km.qnMainPanel.rightPanel.getVerticalScrollBar()
					.getValue();
			this.km.qnMainPanel.reloadLeftTree();
			this.km.qnMainPanel.reloadRight();

			String hqid = (String) params.get("hqid");
			Question q = service.searchQuestion(hqid);
			if (q instanceof FollowupQuestion) {
				Question pq = service.searchParanetQuestion(hqid);
				hqid = pq.getQid();
			}
			if (StringUtils.isNotBlank(hqid)) {
				this.km.qnMainPanel.collAndSelectTreeNode(hqid);
			}

			this.km.qnMainPanel.rightPanel.getVerticalScrollBar().setValue(
					height);

			if (params.containsKey("sure_next") && clickType == 2) {
				String sid = (String) params.get("sid");
				String qid = (String) params.get("qid");

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("sure_next", "true");
				params.put("op", "add");
				if (StringUtils.isNotBlank(qid)) {
					params.put("qid", qid);
				} else {
					params.put("sid", sid);
				}
				(new QuestionEditDialog(params)).setMainPanel(km);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object o = e.getSource();
		if (o == questionTypeBox) {
			ComboItem item = (ComboItem) questionTypeBox.getSelectedItem();
			if ("A".equals(item.getValue())) {
				lengthPanel.setVisible(true);
				tableOuterPanel.setPreferredSize(new Dimension(tableOuterPanel
						.getWidth(), tableOuterPanel.getHeight()));
				tablePanel.setVisible(false);
			} else if ("T".equals(item.getValue())) {
				tableOuterPanel.setPreferredSize(new Dimension(tableOuterPanel
						.getWidth(), tableOuterPanel.getHeight()));
				tablePanel.setVisible(false);
				lengthPanel.setVisible(false);
				dataTypeBox.setSelectedIndex(QuestionConstants
						.getDataTypeIndex("string"));
			} else if ("B".equals(item.getValue())) {
				dataTypeBox.setSelectedIndex(QuestionConstants
						.getDataTypeIndex("boolean"));
				tablePanel.setVisible(true);
				lengthPanel.setVisible(false);
			} else if ("D".equals(item.getValue())) {
				dataTypeBox.setSelectedIndex(QuestionConstants
						.getDataTypeIndex("boolean"));
				tablePanel.setVisible(false);
				lengthPanel.setVisible(false);
			} else if ("R".equals(item.getValue())
					|| "N".equals(item.getValue())) {
				dataTypeBox.setSelectedIndex(QuestionConstants
						.getDataTypeIndex("int"));
				tablePanel.setVisible(false);
				lengthPanel.setVisible(false);
			} else {
				tablePanel.setVisible(true);
				lengthPanel.setVisible(false);
			}
		}
		if (o == leadQuestionTypeBox) {
			ComboItem item = (ComboItem) leadQuestionTypeBox.getSelectedItem();
			String op = (String) params.get("op");
			String sid = (String) params.get("sid");
			if ("add".equals(op)) {
				Map<String, String> map = service.genQuestionId("", sid,
						item.getValue());
				qidLabel.setText(map.get("qestionId"));
			}
			if ("N".equals(item.getValue())) {
				tableOuterPanel.setPreferredSize(new Dimension(tableOuterPanel
						.getWidth(), tableOuterPanel.getHeight()));
				tablePanel.setVisible(false);
			} else {
				tablePanel.setVisible(true);
			}
		}
	}

	private String getSectionId() {
		String sid = (String) params.get("sid");
		if (StringUtils.isNotBlank(sid)) {
			return sid;
		} else {
			Question question = service.searchQuestion((String) params
					.get("qid"));
			if (question instanceof FollowupQuestion) {
				Question q = service.searchParanetQuestion(question.getQid());
				return q.getSubsection();
			} else {
				return question.getSubsection();
			}
		}
	}
}
