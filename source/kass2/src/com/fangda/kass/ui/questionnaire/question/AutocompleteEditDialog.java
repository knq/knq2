package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.CompleteProcedure;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the auto complete edit of the questionnaire.
 * 
 * @author Fangfang Zhao, Jianguo Zhou, Shuang Ma, Jing Kong
 * 
 */
public class AutocompleteEditDialog extends BaseDialog {

	private static final long serialVersionUID = 1L;
	JPanel panel;
	JPanel panel1;
	JPanel panel2;
	JPanel panel3;
	JPanel panel4;
	private double oldHeight = 0;
	QuestionnaireService service;

	public void initDialog() {
		this.setTitle(mxResources.get("autoComplete.title"));
		service = new QuestionnaireService();
	}

	/**
	 * 
	 */
	public JPanel createPanel() {
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel1 = createPanel("Step 1 : Add Parent");
		panel2 = createActivePanel("Step 2 : Add Sibling");
		panel2.addComponentListener(componentlistener);
		panel3 = createPanel("Step 3 : Add Partner");
		panel4 = createPanel("Step 4 : Add Child");
		panel.add(panel1);
		panel.add(panel2);
		panel.add(panel3);
		panel.add(panel4);
		return panel;
	}

	private JPanel createPanel(String name) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBorder(BorderFactory.createTitledBorder(name));

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("supTypeLabel") + "1: "), c, 0, 0,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JTextField(30), c, 1, 0,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JComboBox(
				QuestionConstants.listSex()), c, 2, 0, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel(" "), c, 3, 0, 1,
				1, GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("supTypeLabel") + "2: "), c, 0, 1,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JTextField(30), c, 1, 1,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JComboBox(
				QuestionConstants.listSex()), c, 2, 1, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel(" "), c, 3, 1, 1,
				1, GridBagConstraints.WEST);
		return panel;
	}

	private JPanel createActivePanel(String name) {
		JPanel activePanel = new JPanel();
		activePanel.setLayout(new BoxLayout(activePanel, BoxLayout.Y_AXIS));
		activePanel.setBorder(BorderFactory.createTitledBorder(name));
		//
		activePanel.add(BtnHelper.createAddButton(mxResources.get("addOneRow"),
				"addRow", this));
		activePanel.add(createActiveChildPanel(1));
		activePanel.add(createActiveChildPanel(2));
		return activePanel;
	}

	private JPanel createActiveChildPanel(int num) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("supTypeLabel") + num + ": "), c, 0,
				1, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JTextField(23), c, 1, 1,
				1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JComboBox(
				QuestionConstants.listSex()), c, 2, 1, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray,
				BtnHelper.createDeleteButton("", "deleteRow", this), c, 3, 1,
				1, 1, GridBagConstraints.WEST);
		return panel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			if ("deleteRow".equals(button.getActionCommand())) {
				JPanel jpanel = (JPanel) button.getParent();
				JPanel parent = (JPanel) jpanel.getParent();
				parent.remove(jpanel);
				parent.updateUI();
			} else if ("addRow".equals(button.getActionCommand())) {
				JPanel jpanel = (JPanel) button.getParent();
				jpanel.add(createActiveChildPanel(jpanel.getComponentCount()));
				jpanel.updateUI();
			}
		}
	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel2) {
			Dimension currSize = (Dimension) panel2.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				double change = currSize.getHeight() - oldHeight;
				Dimension dialogSize = (Dimension) this.getSize().clone();
				dialogSize.height = (int) (dialogSize.height + change);
				this.setSize(dialogSize);
				oldHeight = currSize.getHeight();
			}
		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		CompleteProcedure cp = service.get().getDraw().getCompleteProcedure();
		for (int i = 0; i < panel.getComponentCount(); i++) {
			JPanel jpanel = (JPanel) panel.getComponent(i);
			CompleteStep step = cp.getCompleteStep().get(i);
			if (i == 1) {
				step.getSubSteps().clear();
				for (int j = 1; j < jpanel.getComponentCount(); j++) {
					JPanel jp = (JPanel) jpanel.getComponent(j);
					String v1 = ((JTextField) jp.getComponent(1)).getText();
					ComboItem item1 = (ComboItem) ((JComboBox) jp
							.getComponent(2)).getSelectedItem();
					CompleteSubStep s1 = new CompleteSubStep();
					s1.setKinshipTerm(v1);
					s1.setSex(Integer.parseInt(item1.getValue()));
					s1.setStepNo(j);
					s1.setPage(0);
					step.getSubSteps().add(s1);
				}
			} else {
				String v1 = ((JTextField) jpanel.getComponent(1)).getText();
				ComboItem item1 = (ComboItem) ((JComboBox) panel1
						.getComponent(2)).getSelectedItem();
				String v2 = ((JTextField) jpanel.getComponent(5)).getText();
				ComboItem item2 = (ComboItem) ((JComboBox) panel1
						.getComponent(6)).getSelectedItem();

				step.getSubSteps().clear();
				CompleteSubStep s1 = new CompleteSubStep();
				s1.setKinshipTerm(v1);
				s1.setSex(Integer.parseInt(item1.getValue()));
				s1.setStepNo(1);
				if (i == 0) {
					s1.setPage(1);
				} else {
					s1.setPage(0);
				}

				CompleteSubStep s2 = new CompleteSubStep();
				s2.setKinshipTerm(v2);
				s2.setSex(Integer.parseInt(item2.getValue()));
				s2.setStepNo(2);
				if (i == 0) {
					s2.setPage(1);
				} else {
					s2.setPage(0);
				}
				step.getSubSteps().add(s1);
				step.getSubSteps().add(s2);
			}
		}
		service.save(service.get());
		return new MessageModel(true, "Submit success!");
	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		CompleteProcedure cp = service.get().getDraw().getCompleteProcedure();
		CompleteStep step1 = cp.getCompleteStep().get(0);
		List<CompleteSubStep> subSteps1 = step1.getSubSteps();
		if (subSteps1 != null && subSteps1.size() > 0) {
			for (int i = 0; i < subSteps1.size(); i++) {
				CompleteSubStep step = subSteps1.get(i);
				if (i == 0) {
					((JTextField) panel1.getComponent(1)).setText(step
							.getKinshipTerm());
					((JComboBox) panel1.getComponent(2))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
				if (i == 1) {
					((JTextField) panel1.getComponent(5)).setText(step
							.getKinshipTerm());
					((JComboBox) panel1.getComponent(6))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
			}
		}
		CompleteStep step2 = cp.getCompleteStep().get(1);
		List<CompleteSubStep> subSteps2 = step2.getSubSteps();
		if (subSteps2 != null && subSteps2.size() > 0) {
			for (int i = 2; i < subSteps2.size(); i++) {
				panel2.add(createActiveChildPanel(i + 1));
				panel2.updateUI();
			}
			for (int i = 0; i < subSteps2.size(); i++) {
				CompleteSubStep step = subSteps2.get(i);
				JPanel jpanel = (JPanel) panel2.getComponent(i + 1);
				((JTextField) jpanel.getComponent(1)).setText(step
						.getKinshipTerm());
				((JComboBox) jpanel.getComponent(2))
						.setSelectedIndex(QuestionConstants.getSexIndex(step
								.getSex() + ""));
			}
		}
		CompleteStep step3 = cp.getCompleteStep().get(2);
		List<CompleteSubStep> subSteps3 = step3.getSubSteps();
		if (subSteps3 != null && subSteps3.size() > 0) {
			for (int i = 0; i < subSteps3.size(); i++) {
				CompleteSubStep step = subSteps3.get(i);
				if (i == 0) {
					((JTextField) panel3.getComponent(1)).setText(step
							.getKinshipTerm());
					((JComboBox) panel3.getComponent(2))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
				if (i == 1) {
					((JTextField) panel3.getComponent(5)).setText(step
							.getKinshipTerm());
					((JComboBox) panel3.getComponent(6))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
			}
		}
		CompleteStep step4 = cp.getCompleteStep().get(3);
		List<CompleteSubStep> subSteps4 = step4.getSubSteps();
		if (subSteps4 != null && subSteps4.size() > 0) {
			for (int i = 0; i < subSteps4.size(); i++) {
				CompleteSubStep step = subSteps4.get(i);
				if (i == 0) {
					((JTextField) panel4.getComponent(1)).setText(step
							.getKinshipTerm());
					((JComboBox) panel4.getComponent(2))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
				if (i == 1) {
					((JTextField) panel4.getComponent(5)).setText(step
							.getKinshipTerm());
					((JComboBox) panel4.getComponent(6))
							.setSelectedIndex(QuestionConstants
									.getSexIndex(step.getSex() + ""));
				}
			}
		}
	}

	/**
	 * After Submit
	 */
	@Override
	public void afterSubmit() {

	}
}
