package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.config.RelevanceType;
import com.fangda.kass.model.config.RelevanceTypeField;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.model.support.Relevance;
import com.fangda.kass.model.support.RelevanceCondition;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.VFlowLayout;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the Relevance Type condition edit and create in
 * the QMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class RelevanceTypeConditionDialog extends BaseDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -695351417741905114L;
	JPanel panel;
	JButton addBtn;
	JLabel qidLabel;
	JLabel noteLabel;
	double oldHeight = 0;

	List<String> types;
	List<String> conditions;
	List<String> personAtts;
	List<String> functionAtts;
	List<String> unionAtts;
	JPanel panel1;
	JTextField nameField;
	JTextField valueField;

	RelevanceTypeDialog typeDialog = null;
	ComboItem[] personFileds = null;
	ComboItem[] unionFields = null;
	QuestionnaireService service;

	public RelevanceTypeConditionDialog(Map<String, Object> params) {
		super(params);
		typeDialog = (RelevanceTypeDialog) params.get("typedialog");
	}

	public void initDialog() {
		this.setTitle(mxResources.get("SetRelevanceTypeBtn"));
		service = new QuestionnaireService();
		types = new ArrayList<String>();
		conditions = new ArrayList<String>();
		personAtts = new ArrayList<String>();
		functionAtts = new ArrayList<String>();
		unionAtts = new ArrayList<String>();
		types.add(Relevance.TYPE_PERSON);
		types.add(Relevance.TYPE_PERSON_FIELD);
		types.add(Relevance.TYPE_UNION);
		types.add(Relevance.TYPE_UNION_FIELD);
		types.add(Relevance.TYPE_FUNCTION);

		conditions.add(Relevance.OP_EQUALS);
		conditions.add(Relevance.OP_GREAT);
		conditions.add(Relevance.OP_LESS);
		conditions.add(Relevance.OP_NO_EQUALS);

		personAtts.add("pid");
		personAtts.add("sex");
		personAtts.add("isOther");

		functionAtts.add(Relevance.FUNCTION_MERRIED);
		functionAtts.add(Relevance.FUNCTION_HOUSEHOUD);
		functionAtts.add(Relevance.FUNCTION_STEPKINSHIP);

		unionAtts.add("uid");
		unionAtts.add("isOther");

		List<Question> pqs = service.searchPersonQuestion();
		List<Map<String, String>> pquesions = getPersonQuestions(pqs);
		personFileds = new ComboItem[pquesions.size()];
		for (int i = 0; i < pquesions.size(); i++) {
			Map<String, String> map = pquesions.get(i);
			ComboItem item = new ComboItem(map.get("qid") + ":"
					+ map.get("label"), map.get("qid"));
			personFileds[i] = item;
		}

		List<Question> uqs = service.searchUnionQuestion();
		List<Map<String, String>> uquesions = getUnionQuestions(uqs);
		unionFields = new ComboItem[uquesions.size()];
		for (int i = 0; i < uquesions.size(); i++) {
			Map<String, String> map = uquesions.get(i);
			ComboItem item = new ComboItem(map.get("qid") + ":"
					+ map.get("label"), map.get("qid"));
			unionFields[i] = item;
		}
	}

	public JPanel createPanel() {
		qidLabel = new JLabel("qid:");
		noteLabel = new JLabel(mxResources.get("SetRelevanceTypeBtn"));

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		panel1 = new JPanel();
		panel1.setBackground(Color.white);
		// panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setLayout(new VFlowLayout());
		panel1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		final ComboItem[] typeItems = new ComboItem[types.size() + 1];
		typeItems[0] = new ComboItem("", "");
		for (int i = 0; i < types.size(); i++) {
			String t = types.get(i);
			ComboItem item = new ComboItem(t, t);
			typeItems[i + 1] = item;
		}
		final ComboItem[] conditionItems = new ComboItem[conditions.size() + 1];
		conditionItems[0] = new ComboItem("", "");
		for (int i = 0; i < conditions.size(); i++) {
			String t = conditions.get(i);
			ComboItem item = new ComboItem(t, t);
			conditionItems[i + 1] = item;
		}

		// Name
		nameField = new JTextField(60);
		panel.add(new JLabel("Name"), new GridBagConstraints(0, 0, 1, 1, 1.0,
				1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		panel.add(nameField, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));
		// Value
		valueField = new JTextField(40);
		panel.add(new JLabel("Value"), new GridBagConstraints(0, 1, 1, 1, 1.0,
				1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		panel.add(valueField, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		String name = (String) params.get("name");
		String value = (String) params.get("value");
		RelevanceTypeField field = null;
		if (StringUtils.isNotBlank(value)) {
			nameField.setText(name);
			valueField.setText(value);
			field = search(value);
		}

		addBtn = new JButton("Add Row");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel jpanel = genRow(typeItems, conditionItems, "", "", "",
						"");
				panel1.add(jpanel);
				panel1.updateUI();
				return;
			}
		});
		JScrollPane pane = new JScrollPane(panel1);
		pane.setPreferredSize(new Dimension(300, 150));
		panel.add(addBtn, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));

		panel.add(pane, new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));
		panel.add(new JLabel(" "), new GridBagConstraints(2, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));
		if (field != null) {
			Relevance r = new Relevance();
			r.string2obj(field.getCondition());
			if (r != null) {
				List<RelevanceCondition> conditions = r.getConditions();
				for (RelevanceCondition condition : conditions) {

					JPanel jpanel = genRow(typeItems, conditionItems,
							condition.getType(), condition.getOp(),
							condition.getTypeAttribute(), condition.getValue());
					panel1.add(jpanel);
				}
				panel1.updateUI();
			}
		}
		return panel;
	}

	private JPanel genRow(ComboItem[] typeItems, ComboItem[] conditioItems,
			String type, String condition, String typeAtt, String value) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);

		JComboBox box = new JComboBox(typeItems);
		int index = getSelectIndex(box, type);
		box.setSelectedIndex(index);

		JComboBox box1 = new JComboBox(conditioItems);
		int index1 = getSelectIndex(box1, condition);
		box1.setSelectedIndex(index1);

		final JComboBox box01 = new JComboBox();
		if (Relevance.TYPE_PERSON.equals(type)) {
			box01.removeAllItems();
			for (int i = 0; i < personAtts.size(); i++) {
				String t = personAtts.get(i);
				ComboItem it = new ComboItem(t, t);
				box01.addItem(it);
			}
		} else if (Relevance.TYPE_UNION.equals(type)) {
			box01.removeAllItems();
			for (int i = 0; i < unionAtts.size(); i++) {
				String t = unionAtts.get(i);
				ComboItem it = new ComboItem(t, t);
				box01.addItem(it);
			}
		} else if (Relevance.TYPE_FUNCTION.equals(type)) {
			box01.removeAllItems();
			for (int i = 0; i < functionAtts.size(); i++) {
				String t = functionAtts.get(i);
				ComboItem it = new ComboItem(t, t);
				box01.addItem(it);
			}
		} else if (Relevance.TYPE_PERSON_FIELD.equals(type)) {
			box01.removeAllItems();
			box01.setModel(new DefaultComboBoxModel(personFileds));
		} else if (Relevance.TYPE_UNION_FIELD.equals(type)) {
			box01.removeAllItems();
			box01.setModel(new DefaultComboBoxModel(unionFields));
		}
		if (StringUtils.isNotBlank(type)) {
			int index01 = getSelectIndex(box01, typeAtt);
			box01.setSelectedIndex(index01);
		}

		JTextField valField = new JTextField(3);
		valField.setText(value);

		UIHelper.addToBagpanel(panel, Color.gray, box, c, 0, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, box01, c, 1, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, box1, c, 2, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, valField, c, 3, 0, 1, 1,
				GridBagConstraints.EAST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, 0, 1, 1,
				GridBagConstraints.EAST);
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				panel1.updateUI();
			}
		});
		box.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox b = (JComboBox) e.getSource();
				ComboItem item = (ComboItem) b.getSelectedItem();
				if (Relevance.TYPE_PERSON.equals(item.getName())) {
					box01.removeAllItems();
					for (int i = 0; i < personAtts.size(); i++) {
						String t = personAtts.get(i);
						ComboItem it = new ComboItem(t, t);
						box01.addItem(it);
					}
				} else if (Relevance.TYPE_UNION.equals(item.getName())) {
					box01.removeAllItems();
					for (int i = 0; i < unionAtts.size(); i++) {
						String t = unionAtts.get(i);
						ComboItem it = new ComboItem(t, t);
						box01.addItem(it);
					}
				} else if (Relevance.TYPE_FUNCTION.equals(item.getName())) {
					box01.removeAllItems();
					for (int i = 0; i < functionAtts.size(); i++) {
						String t = functionAtts.get(i);
						ComboItem it = new ComboItem(t, t);
						box01.addItem(it);
					}
				} else if (Relevance.TYPE_PERSON_FIELD.equals(item.getName())) {
					box01.removeAllItems();
					box01.setModel(new DefaultComboBoxModel(personFileds));
				} else if (Relevance.TYPE_UNION_FIELD.equals(item.getName())) {
					box01.removeAllItems();
					box01.setModel(new DefaultComboBoxModel(unionFields));
				}
			}
		});
		return panel;
	}

	private int getSelectIndex(JComboBox jc, String value) {
		int size = jc.getModel().getSize();
		for (int i = 0; i < size; i++) {
			ComboItem item = (ComboItem) jc.getModel().getElementAt(i);
			if (item.getValue().equals(value)) {
				return i;
			}
		}
		return 0;
	}

	private void deleteRow(JButton deleteBtn) {
		JPanel jpanel = (JPanel) deleteBtn.getParent();
		deleteBtn.getParent().getParent().remove(jpanel);
	}

	@Override
	public void afterShow() {
		super.afterShow();
	}

	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub

		String conditionValue = valueField.getText();
		String conditionName = nameField.getText();
		String conditionAttr = "True";
		int size = panel1.getComponentCount();
		for (int i = 0; i < size; i++) {
			JPanel jpanel = (JPanel) panel1.getComponent(i);
			int s1 = jpanel.getComponentCount();

			for (int j = 0; j < s1; j++) {
				Component c = jpanel.getComponent(j);
				if (j == 0) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					String Attr1 = item.getValue();
					if (Attr1.isEmpty()) {
						conditionAttr = Attr1;
					}
				} else if (j == 1) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					String Attr1 = item.getValue();
					if (Attr1.isEmpty()) {
						conditionAttr = Attr1;
					}
				} else if (j == 2) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					String Attr1 = item.getValue();
					if (Attr1.isEmpty()) {
						conditionAttr = Attr1;
					}
				} else if (j == 3) {
					JTextField box = (JTextField) c;
					String Attr1 = box.getText();
					if (Attr1.isEmpty()) {
						conditionAttr = Attr1;
					}
				}
			}
		}
		if ("".equals(conditionValue) || "".equals(conditionName)
				|| "".equals(conditionAttr)) {
			JOptionPane.showMessageDialog(
					container,
					"<HTML><font color='red'>(*)</font>"
							+ mxResources.get("missInformation") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		typeDialog = (RelevanceTypeDialog) params.get("typedialog");
		RelevanceTypeField field = new RelevanceTypeField();
		field.setValue(valueField.getText());
		field.setName(nameField.getText());
		int size = panel1.getComponentCount();
		Relevance r = new Relevance();
		r.getConditions().clear();
		for (int i = 0; i < size; i++) {
			JPanel jpanel = (JPanel) panel1.getComponent(i);
			int s1 = jpanel.getComponentCount();
			RelevanceCondition condtion = new RelevanceCondition();
			for (int j = 0; j < s1; j++) {
				Component c = jpanel.getComponent(j);
				if (j == 0) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					condtion.setType(item.getValue());
				} else if (j == 1) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					condtion.setTypeAttribute(item.getValue());
				} else if (j == 2) {
					JComboBox box = (JComboBox) c;
					ComboItem item = (ComboItem) box.getSelectedItem();
					condtion.setOp(item.getValue());
				} else if (j == 3) {
					JTextField box = (JTextField) c;
					condtion.setValue(box.getText());
				}
			}
			r.getConditions().add(condtion);
		}
		field.setCondition(r.obj2String());
		typeDialog.lastField = field;
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {
		if (typeDialog != null) {
			if (params.get("value") == null) {
				typeDialog.addNew();
			} else {
				typeDialog.updateFiled();
			}
		}

	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel1) {
			Dimension currSize = (Dimension) panel1.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				double change = currSize.getHeight() - oldHeight;
				Dimension dialogSize = (Dimension) this.getSize().clone();
				dialogSize.height = (int) (dialogSize.height + change);
				this.setSize(dialogSize);
				oldHeight = currSize.getHeight();
			}
		}
	}

	private RelevanceTypeField search(String value) {
		RelevanceType relevantType = QuestionConstants.getRelevantType();
		List<RelevanceTypeField> fields = relevantType.getFields();

		for (RelevanceTypeField p : fields) {
			if (StringUtils.equals(value, p.getValue())) {
				return p;
			}
		}
		return null;
	}

	private List<Map<String, String>> getPersonQuestions(List<Question> pqs) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (Question question : pqs) {
			if (question.getFollowupQuestions() != null) {
				for (Question q : question.getFollowupQuestions()) {
					if ("B".equals(q.getType())) {
						for (Option o : question.getOptions()) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("label", o.getLabel());
							map.put("qid", o.getOid());
							result.add(map);
						}
					} else {
						Map<String, String> map = new HashMap<String, String>();
						map.put("label", q.getLabel());
						map.put("qid", q.getQid());
						result.add(map);
					}
				}
			}
		}
		List<Subsection> subsections = service.get().getSection()
				.getSubsections();
		for (Subsection subsection : subsections) {
			if (StringUtils
					.equalsIgnoreCase("map", subsection.getSectionMode())) {
				pqs = subsection.getQuestions();
				for (Question question : pqs) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("label", question.getLabel());
					map.put("qid", question.getQid());
					result.add(map);
				}
			}
		}
		return result;
	}

	private List<Map<String, String>> getUnionQuestions(List<Question> pqs) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (Question question : pqs) {
			if (question.getFollowupQuestions() != null) {
				for (Question q : question.getFollowupQuestions()) {
					if ("B".equals(q.getType())) {
						for (Option o : question.getOptions()) {
							Map<String, String> map = new HashMap<String, String>();
							map.put("label", o.getLabel());
							map.put("qid", o.getOid());
							result.add(map);
						}
					} else {
						Map<String, String> map = new HashMap<String, String>();
						map.put("label", q.getLabel());
						map.put("qid", q.getQid());
						result.add(map);
					}
				}
			}
		}
		return result;
	}

	private void addSize(int change) {
		Dimension dialogSize = (Dimension) this.getSize().clone();
		dialogSize.height = (int) (dialogSize.height + change);
		RelevanceTypeConditionDialog.this.setSize(dialogSize);
	}
}
