package com.fangda.kass.ui.questionnaire.export;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Revise;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.WordTools;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireViewPanel;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.style.RtfFont;
import com.mxgraph.util.mxResources;

/**
 * This Class for exporting the Microsoft Word Document of the local language
 * questionnaire.
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * 
 */
public class ExportLocalWord {

	public static Boolean ifShowDetail;
	static LinkedHashMap<String, String> groupMap = new LinkedHashMap<String, String>();

	/**
	 * Write to the Microsoft Word Document file
	 * 
	 * @param qid
	 * @param wpath
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws DocumentException
	 */
	public static void writedoc(Questionnaire q, String wpath, Boolean ifSDetail)
			throws MalformedURLException, IOException, DocumentException {
		ifShowDetail = ifSDetail;
		if (q != null) {

			/*
			 * Landscape
			 */
			Document document = new Document(PageSize.A4);

			RtfWriter2.getInstance(document, new FileOutputStream(wpath));
			document.open();

			/*
			 * Set the margins of page. 25.4 mm = 72f，31.8mm = 90f
			 */
			document.setMargins(32f, 32f, 30f, 30f);

			/*
			 * Title
			 */
			WordTools.addZhengBiaoti(document, q.getTitle());

			/*
			 * subTitle
			 */
			addFuBiaoti(document, q);
			/*
			 * The revision history
			 */
			addRviseVersion(document, q);
			/*
			 * Preface
			 */
			WordTools.addFistChapter(document, "Preface");
			WordTools.addKonghang0d5(document);
			WordTools.addContent(document, q.getNote());

			/*
			 * Content
			 */
			addDocContent(document, q);

			if (document != null)
				document.close();

		}

		return;
	}

	public static String returnNull(String str) {
		String restr = "";
		restr = (str == null ? "" : str);
		return restr;
	}

	/**
	 * subTitle
	 */
	public static void addFuBiaoti(Document document, Questionnaire q)
			throws DocumentException {
		String subStrTitle = ("Language: " + (returnNull(q.getLanguage()))
				+ "   Version: " + q.getVersion());
		WordTools.addFuBiaoti(document, subStrTitle);

		subStrTitle = ("Country : " + q.getCountry() + "  CultureArea : " + (q
				.getCultureArea() == null ? "" : q.getCultureArea()));
		WordTools.addFuBiaoti(document, subStrTitle);
		subStrTitle = "Status : "
				+ mxResources.get(mxResources.get(q.getStatus()));
		WordTools.addFuBiaoti(document, subStrTitle);
	}

	/**
	 * The table for the revision history
	 */
	public static void addRviseVersion(Document document, Questionnaire q)
			throws DocumentException {
		List<Revise> kilog = q.getRevises();

		Table table = new Table(5, kilog.size() + 1);
		int[] withs = { 20, 20, 20, 20, 20 };
		table.setWidths(withs);
		table.setWidth(100);
		table.setAlignment(Element.ALIGN_CENTER);
		table.setAutoFillEmptyCells(true);

		RtfFont contextFont = new RtfFont("仿宋_GB2312", 9, Font.NORMAL,
				Color.BLACK);
		String contextString = "";
		Paragraph context = new Paragraph(contextString);
		context.setAlignment(Element.ALIGN_CENTER);
		context.setFont(contextFont);
		context.setSpacingBefore(10);
		// context.setFirstLineIndent(20);
		document.add(context);
		Cell[] cellHeaders = new Cell[5];
		cellHeaders[0] = new Cell(
				new Phrase(mxResources.get("No"), contextFont));
		cellHeaders[1] = new Cell(new Phrase(
				mxResources.get("questionniare.properties.version"),
				contextFont));
		cellHeaders[2] = new Cell(new Phrase(mxResources.get("reviseDate"),
				contextFont));
		cellHeaders[3] = new Cell(new Phrase(mxResources.get("reviser"),
				contextFont));
		cellHeaders[4] = new Cell(new Phrase(mxResources.get("note"),
				contextFont));

		for (int i = 0; i < 5; i++) {
			cellHeaders[i].setHorizontalAlignment(Element.ALIGN_CENTER);
			cellHeaders[i].setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellHeaders[i]);
		}

		String[] columnNames = { mxResources.get("No"),
				mxResources.get("questionniare.properties.version"),
				mxResources.get("reviseDate"), mxResources.get("reviser"),
				mxResources.get("note") };
		String[][] data = new String[kilog.size()][5];
		DefaultTableModel model = new DefaultTableModel(data, columnNames);

		for (int i = 0; i < kilog.size(); i++) {

			Cell cell0 = new Cell(
					new Phrase(String.valueOf(i + 1), contextFont));
			cell0.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell0.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell0);

			table.addCell(WordTableAddCell(kilog.get(i).getVersion(),
					contextFont));
			table.addCell(WordTableAddCell(kilog.get(i).getReviseDate(),
					contextFont));
			table.addCell(WordTableAddCell(kilog.get(i).getReviser(),
					contextFont));
			table.addCell(WordTableAddCell(kilog.get(i).getNote(), contextFont));
		}

		String subStrTitle = mxResources.get("revisehistory") + ":";
		WordTools.addFuBiaoti(document, subStrTitle);
		document.add(table);

	}

	public static Cell WordTableAddCell(String value, RtfFont contextFont)
			throws BadElementException {
		Cell cell = null;
		cell = new Cell(new Phrase(value, contextFont));
		return cell;
	}

	/**
	 * Content
	 * 
	 * @return
	 */
	public static void addDocContent(Document document, Questionnaire q)
			throws DocumentException, MalformedURLException, IOException {
		int c = 2;
		List<Subsection> subsections = q.getSection().getSubsections();
		int index = 1;
		int sectionIndex = 1;
		// Add a blank line
		WordTools.addKonghang(document);

		for (Subsection ss : subsections) {
			String sectionTitle = "Section " + sectionIndex + " "
					+ ss.getTitle();
			{
				sectionTitle += QuestionnaireViewPanel.getSectionDetail(ss,
						ifShowDetail);

				WordTools.addKonghang(document);
				WordTools.addFistChapter(document, sectionTitle);
				WordTools.addKonghang(document);

				// NKK Diagram Stop Rule：step=4
				if (sectionIndex == 1) {
					StopRule stoprule = q.getDraw().getStopRule();
					String content = mxResources.get("stoppingRuleSetupTitle")
							+ " " + stoprule.getStep();
					WordTools.addKonghang(document);
					WordTools.addFistChapter(document, content);
					WordTools.addKonghang(document);
					index++;
				}
				WordTools.addContent(document, ss.getDetail());
			}
			index++;

			if (sectionIndex == 1) {
				index = addSection1Content(index, document, c,
						subsections.get(0), q);
			}

			// subsection's question
			int questionIndex = 1;
			if (ss.getQuestions() != null) {
				for (Question question : ss.getQuestions()) {
					c = 2;
					index = addQuestion(document, c, question, index,
							questionIndex, sectionIndex);

					index++;

					questionIndex++;
				}
			}
			if (ss.getSubSections() != null) {
				int subsectionIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					String showTitle = sectionIndex + "." + subsectionIndex
							+ " ";
					showTitle = showTitle
							+ (subsection.getTitle() == null ? "" : subsection
									.getTitle());
					showTitle += QuestionnaireViewPanel.getSectionDetail(ss,
							ifShowDetail);

					index++;
					String snode = subsection.getDetail() == null ? ""
							: subsection.getDetail();
					index++;
					{
						WordTools.addKonghang(document);
						WordTools.addFistSection(document, showTitle);
						WordTools.addKonghang(document);
						WordTools.addContent2(document, snode);
					}

					// int questionIndex = 1;
					for (Question question : subsection.getQuestions()) {
						c = 3;
						index = addQuestion(document, c, question, index,
								questionIndex, sectionIndex);
						questionIndex++;
					}
					subsectionIndex++;
				}
			}

			sectionIndex++;
		}

	}

	/**
	 * c = the level of title， document = Microsoft Word Document
	 * 
	 * @return
	 */
	public static int addQuestion(Document document, int c, Question question,
			int index, int questionIndex, int sectionIndex)
			throws DocumentException, MalformedURLException, IOException {
		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		boolean otherCondition = false;
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())
					&& fquestion.getGroupName().length() > 0) {
				String parentQid = StringUtil.substringBeforeLast(
						question.getQid(), "_");
				String key = parentQid + fquestion.getGroupName();
				String jpanel = (String) groupMap.get(key);
				if (jpanel == null) {
					groupMap.put(key, fquestion.getGroupName());
					WordTools.addContent2(document, fquestion.getGroupName()
							+ ":");
					index++;
				}
				if (StringUtil.isNotBlank(question.getDetail())) {

				}

				addPanelOption(document, question);
			} else {
				if (fquestion.getLayout() != null) {
					String parentQid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					String key = parentQid + fquestion.getLayout();
					String jpanel = (String) groupMap.get(key);
					if (jpanel == null) {
						groupMap.put(key, fquestion.getLayout());
						// addContent2(document, question.getLayout()+":");
						WordTools.addKonghang0d5(document);
						index++;
					}
					// addContent2(document, question.getDetail());
					// addContentTextSingle(document, question.getDetail());
					addPanelOption(document, question);
				} else {
					otherCondition = true;
				}
			}
		} else {
			otherCondition = true;
		}

		if (otherCondition) {
			if (question.getTitle() != null) {
				String titleA;
				if (isFollowup) {
					titleA = questionIndex + "." + question.getTitle();
				} else {
					question.getRelevance();
					String st;
					st = String.format("%02d", questionIndex);
					String title = st + ". " + question.getTitle();
					if ("G".equals(question.getDirection())) {
						title = title + "(Help given)";
					} else if ("R".equals(question.getDirection())) {
						title = title + "(Help received)";
					}
					titleA = title;
				}
				{
					titleA += QuestionnaireViewPanel.getLeadQuestionNoDetail(
							question, ifShowDetail);

					WordTools.addKonghang0d5(document);
					if (2 == c) {
						WordTools.addFistSection(document, titleA);
					} else if (3 == c) {
						WordTools.addFistSectionChild(document, titleA);
					}
					WordTools.addKonghang0d5(document);
				}
				index++;
			}

			// if (question.getLead() != null) {
			// //JLabel leadLabel = UIHelper.genLabel("&nbsp;" +
			// question.getLead());
			// WordTools.addContent2(document, question.getLead());
			// index++;
			// }

			if (question.getDetail() != null
					&& !question.getDetail().equals(question.getLead())) {
				String title = question.getDetail();
				if (isFollowup) {
					title += QuestionnaireViewPanel.getFollowupQuestionDetail(
							question, ifShowDetail);
				}

				WordTools.addContent2(document, title);

				index++;

			} else {
				WordTools.addContent2(document, question.getDetail());
			}

			index = addOption(document, c, question, index, sectionIndex);
			if (question.getFollowupQuestions() != null) {
				int followupIndex = 1;
				for (FollowupQuestion fqestion : question
						.getFollowupQuestions()) {
					index = addQuestion(document, c, fqestion, index,
							followupIndex, sectionIndex);
					followupIndex++;
				}
			}
		}

		return index;
	}

	/**
	 * Add the answer option
	 * 
	 * @param document
	 * @param c
	 * @param question
	 * @param index
	 * @param sectionIndex
	 * @return
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static int addOption(Document document, int c, Question question,
			int index, int sectionIndex) throws DocumentException,
			MalformedURLException, IOException {
		if ("A".equals(question.getType())) {// text
			WordTools.addContentTextSingle(document, question.getDetail());
			index++;
		} else if ("T".equals(question.getType())) {
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += QuestionnaireViewPanel
							.getQuestionOptionDetail(option, ifShowDetail);
					if ("C".equals(question.getType()) || sectionIndex == 3) {
						optionTitle = "      〇 " + optionTitle;
					} else {
						optionTitle = "      口" + optionTitle;
					}
					if (option.getNote() != null
							&& !"".equals(option.getNote())) {
						optionTitle += " (Note:" + option.getNote() + ")";
					}
					WordTools.addContent3(document, optionTitle);
					index++;
				}
			}
		}
		return index;
	}

	/**
	 * The panel for the answer option
	 * 
	 * @param document
	 * @param question
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static void addPanelOption(Document document, Question question)
			throws DocumentException, MalformedURLException, IOException {

		if ("A".equals(question.getType())) {// text
			WordTools.addContent2PanelOption(document, question.getDetail());
			// addContentTextSinglePanel(document, question.getDetail());
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += QuestionnaireViewPanel
							.getQuestionOptionDetail(option, ifShowDetail);
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      〇 " + optionTitle;
					} else {
						optionTitle = "      口 " + optionTitle;
					}
					WordTools.addContent3(document, optionTitle);
				}
			}
		}
	}

	/**
	 * Section 1
	 * 
	 * @throws DocumentException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static int addSection1Content(int index, Document document, int c,
			Subsection ss, Questionnaire q) throws DocumentException,
			MalformedURLException, IOException {
		StopRule stoprule = q.getDraw().getStopRule();
		String content = mxResources.get("stoppingRuleSetup") + " "
				+ stoprule.getStep() + " " + mxResources.get("stoppingStep");
		content += ". (" + mxResources.get("questionniare.properties.note")
				+ ": " + stoprule.getNote() + ")";

		WordTools.addContent(document, content);

		WordTools.addImage(document, UIHelper.getPath("stoppingRuleImg.png"));

		WordTools.addContent(document, mxResources.get("autoCompleteStep"));

		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		for (CompleteStep step : steps) {
			content = mxResources.get("stepLabel") + " " + step.getStepNo()
					+ ": " + step.getTitle();
			WordTools.addContent(document, content);

			List<CompleteSubStep> substeps = step.getSubSteps();
			for (CompleteSubStep subStep : substeps) {
				if (subStep.getKinshipTerm() != null) {
					content = "      ○ " + subStep.getKinshipTerm();
					if (ifShowDetail) {
						content += " ("
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ":"
								+ subStep.getSex() + ")";
					} else {
						content += " ("
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ")";
					}
				} else {
					content = "      ○ "
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex()));
					if (ifShowDetail) {
						content = "      ○ "
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ":"
								+ subStep.getSex();
					} else {
						content = "      ○ "
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex()));
					}
				}

				WordTools.addContent(document, content);
			}
		}

		return index;
	}
}
