package com.fangda.kass.ui.questionnaire.export;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.WordTools;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireViewPanel;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.rtf.RtfWriter2;
import com.mxgraph.util.mxResources;

/**
 * This Class for exporting the Microsoft Word Document of the bilingual
 * questionnaire.
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * 
 */
public class ExportBialigualWord {

	public static Boolean ifShowDetail;
	static LinkedHashMap<String, String> groupMap = new LinkedHashMap<String, String>();

	/**
	 * Write to the Microsoft Word Document file
	 * 
	 * @param qid
	 * @param wpath
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws DocumentException
	 */
	public void writedoc(Questionnaire q, String wpath, Boolean ifSDetail)
			throws MalformedURLException, IOException, DocumentException {
		ifShowDetail = ifSDetail;
		if (q != null) {

			/*
			 * Landscape
			 */
			Document document = new Document(PageSize.A4);

			RtfWriter2.getInstance(document, new FileOutputStream(wpath));
			document.open();

			/*
			 * Set the margins of page. 25.4 mm = 72f，31.8mm = 90f
			 */
			document.setMargins(32f, 32f, 30f, 30f);

			/*
			 * Title
			 */
			WordTools.addZhengBiaoti(document, q.getTitle());

			/*
			 * SubTitle
			 */
			addFuBiaoti(document, q);

			/*
			 * Preface
			 */
			WordTools.addFistChapter(document, "Preface");
			WordTools.addKonghang0d5(document);
			WordTools.addContent(document, q.getNote());
			WordTools.addContent(document, q.getEn_note());
			/*
			 * Content
			 */
			addDocContent(document, q);

			if (document != null)
				document.close();

		}

		return;
	}

	/**
	 * Subtitle
	 */
	public static void addFuBiaoti(Document document, Questionnaire q)
			throws DocumentException {
		com.fangda.kass.model.config.IsoLangCode isoLangCode = QuestionConstants
				.getLangByXml(Constants.QUESTIONNAIRE_XML);
		String subStrTitle = mxResources.get("language") + " : "
				+ isoLangCode.getLocalLanguage() + " (" + " "
				+ q.getLanguageName() + ")" + "                   "
				+ mxResources.get("version") + " : " + q.getVersion();
		WordTools.addFuBiaoti(document, subStrTitle);

		subStrTitle = mxResources.get("country") + " : "
				+ isoLangCode.getLocalCountry() + " (" + ""
				+ q.getCountryName() + ")" + "  "
				+ mxResources.get("questionniare.properties.culture") + " : "
				+ q.getCultureArea() + " (" + "" + q.getEn_cultureArea() + ")";
		WordTools.addFuBiaoti(document, subStrTitle);

		subStrTitle = mxResources.get("status") + " : "
				+ mxResources.get(q.getStatus());
		WordTools.addFuBiaoti(document, subStrTitle);

	}

	/**
	 * Content
	 * 
	 * @return
	 */
	public void addDocContent(Document document, Questionnaire q)
			throws DocumentException, MalformedURLException, IOException {
		int c = 2;
		List<Subsection> subsections = q.getSection().getSubsections();
		int index = 1;
		int sectionIndex = 1;
		for (Subsection ss : subsections) {
			String sectionTitle = "Section " + sectionIndex + " "
					+ ss.getTitle();
			{
				sectionTitle += " (" + ss.getEn_title() + ")";
				WordTools.addKonghang(document);
				WordTools.addFistChapter(document, sectionTitle);
				WordTools.addKonghang(document);
				String detail = ss.getDetail();
				detail += " (" + ss.getEn_detail() + ")";
				WordTools.addContent(document, detail);
			}
			index++;

			if (sectionIndex == 1) {
				index = addSection1Content(index, document, c, ss, q);
			}
			// subsection's question
			int questionIndex = 1;
			if (ss.getQuestions() != null) {

				for (Question question : ss.getQuestions()) {
					c = 2;
					index = addQuestion(document, c, question, index,
							questionIndex, sectionIndex);

					questionIndex++;
				}
			}
			if (ss.getSubSections() != null) {
				int subsectionIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					String showTitle = sectionIndex + "." + subsectionIndex
							+ " ";
					showTitle = showTitle
							+ (subsection.getTitle() == null ? "" : subsection
									.getTitle());
					showTitle += " (" + subsection.getEn_title() + ")";

					index++;
					String snode = subsection.getDetail() == null ? ""
							: subsection.getDetail();
					snode += " (" + subsection.getEn_detail() + ")";
					{
						// Blank line
						WordTools.addKonghang(document);
						WordTools.addFistSection(document, showTitle);
						WordTools.addKonghang(document);
						WordTools.addContent2(document, snode);
					}

					// int questionIndex = 1;
					for (Question question : subsection.getQuestions()) {
						c = 3;
						index = addQuestion(document, c, question, index,
								questionIndex, sectionIndex);
						questionIndex++;
					}
					subsectionIndex++;
				}
			}

			sectionIndex++;
		}

	}

	/**
	 * c = the level of title， document = Microsoft Word Document
	 * 
	 * @return
	 */
	public static int addQuestion(Document document, int c, Question question,
			int index, int questionIndex, int sectionIndex)
			throws DocumentException, MalformedURLException, IOException {
		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		boolean otherCondition = false;
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())) {
				String parentQid = StringUtil.substringBeforeLast(
						question.getQid(), "_");
				String key = parentQid + fquestion.getGroupName();
				String jpanel = (String) groupMap.get(key);
				if (jpanel == null) {
					groupMap.put(key, fquestion.getGroupName());
					WordTools.addContent2(document, fquestion.getGroupName()
							+ ":");
					index++;
				}

				addPanelOption(document, question);
			} else {
				if (fquestion.getLayout() != null) {
					String parentQid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					String key = parentQid + fquestion.getLayout();
					String jpanel = (String) groupMap.get(key);
					if (jpanel == null) {
						groupMap.put(key, fquestion.getLayout());
						// addContent2(document, question.getLayout()+":");
						WordTools.addKonghang0d5(document);
						index++;
					}
					// addContent2(document, question.getDetail());
					// addContentTextSingle(document, question.getDetail());
					addPanelOption(document, question);
				} else {
					otherCondition = true;
				}
			}
		} else {
			otherCondition = true;
		}

		if (otherCondition) {
			if (question.getTitle() != null) {
				String titleA;
				if (isFollowup) {
					titleA = questionIndex + "." + question.getTitle();
				} else {
					question.getRelevance();
					String st;
					st = String.format("%02d", questionIndex);
					String title = st + ". " + question.getTitle();
					if ("G".equals(question.getDirection())) {
						title = title + "(Help given)";
					} else if ("R".equals(question.getDirection())) {
						title = title + "(Help received)";
					}
					titleA = title;
				}
				{
					titleA += " (" + question.getEn_title() + ")";

					WordTools.addKonghang0d5(document);
					if (2 == c) {
						WordTools.addFistSection(document, titleA);
					} else if (3 == c) {
						WordTools.addFistSectionChild(document, titleA);
					}
					WordTools.addKonghang0d5(document);
				}
				index++;
			}
			if (question.getRelevance() != null) {// sectionIndex
				Object[] relevantArray = QuestionConstants.listRelevant();
				int idx = QuestionConstants.getRelevantIndex(question
						.getRelevance());
				WordTools.addContent2Relevant(document,
						relevantArray[idx].toString());
				index++;
			}
			if (question.getLead() != null) {
				// JLabel leadLabel = UIHelper.genLabel("&nbsp;" +
				// question.getLead());
				String lead = question.getLead();
				lead += " (" + question.getEn_lead() + ")";

				WordTools.addContent2(document, lead);
				index++;
			}

			if (question.getLead() != null
					&& !question.getDetail().equals(question.getLead())
					&& isFollowup) {
				if (question.getDetail() != null) {
					String title = question.getDetail();
					title += " (" + question.getEn_detail() + ")";

					String st1 = "", st2 = "";
					st1 = question.getLead().trim();
					st2 = question.getDetail().trim();
					if (st1.equals(st2)) {
					} else {
						WordTools.addContent2(document, title);
					}
					index++;
				}
			} else {
				WordTools.addContent2(document, question.getDetail() + " ("
						+ question.getEn_detail() + ")");
			}

			index = addOption(document, c, question, index, sectionIndex);
			if (question.getFollowupQuestions() != null) {
				int followupIndex = 1;
				for (FollowupQuestion fqestion : question
						.getFollowupQuestions()) {
					index = addQuestion(document, c, fqestion, index,
							followupIndex, sectionIndex);
					followupIndex++;
				}
			}
		}

		return index;
	}

	/**
	 * Add the answer option
	 * 
	 * @param document
	 * @param c
	 * @param question
	 * @param index
	 * @param sectionIndex
	 * @return
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static int addOption(Document document, int c, Question question,
			int index, int sectionIndex) throws DocumentException,
			MalformedURLException, IOException {
		if ("A".equals(question.getType())) {// text
			WordTools.addContentTextSingle(document, question.getDetail());
			index++;
		} else if ("T".equals(question.getType())) {
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += QuestionnaireViewPanel
							.getQuestionOptionDetail(option, ifShowDetail);
					if ("C".equals(question.getType()) || sectionIndex == 3) {
						optionTitle = "      〇 " + optionTitle;
					} else {
						optionTitle = "      口" + optionTitle;
					}

					if (option.getNote() != null) {
						optionTitle += " Note:" + option.getNote() + " ";
					}
					if (option.getEn_note() != null) {
						optionTitle += " (" + option.getEn_note() + ")";
					}
					WordTools.addContent3(document, optionTitle);
					index++;
				}
			}
		}
		return index;
	}

	/**
	 * Add the option panel
	 * 
	 * @param document
	 * @param question
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static void addPanelOption(Document document, Question question)
			throws DocumentException, MalformedURLException, IOException {

		if ("A".equals(question.getType())) {// text
			String detail = question.getDetail();
			detail += " (" + question.getEn_detail() + ")";
			WordTools.addContent2PanelOption(document, detail);
			// addContentTextSinglePanel(document, question.getDetail());
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += QuestionnaireViewPanel
							.getQuestionOptionDetail(option, ifShowDetail);
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      〇 " + optionTitle;
					} else {
						optionTitle = "      口 " + optionTitle;
					}

					if (option.getNote() != null) {
						optionTitle += " Note:" + option.getNote() + " ";
					}
					if (option.getEn_note() != null) {
						optionTitle += " (" + option.getEn_note() + ")";
					}
					WordTools.addContent3(document, optionTitle);
				}
			}
		}
	}

	/**
	 * Section 1
	 * 
	 * @throws DocumentException
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public int addSection1Content(int index, Document document, int c,
			Subsection ss, Questionnaire q) throws DocumentException,
			MalformedURLException, IOException {
		StopRule stoprule = q.getDraw().getStopRule();
		String content = mxResources.get("stoppingRuleSetup") + " "
				+ stoprule.getStep() + " " + mxResources.get("stepLabel");

		content += ". (" + mxResources.get("questionniare.properties.note")
				+ ": " + stoprule.getNote() + ")";

		content += " (" + stoprule.getEn_note() + ")";

		WordTools.addContent(document, content);

		WordTools.addImage(document, UIHelper.getPath("stoppingRuleImg.png"));

		WordTools.addContent(document, mxResources.get("autoCompleteStep"));

		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		for (CompleteStep step : steps) {
			content = mxResources.get("stepLabel") + " " + step.getStepNo()
					+ " " + step.getTitle();
			WordTools.addContent(document, content);

			List<CompleteSubStep> substeps = step.getSubSteps();
			for (CompleteSubStep subStep : substeps) {
				if (subStep.getKinshipTerm() != null) {
					content = "      ○ " + subStep.getKinshipTerm();
					content += " ("
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex())) + ")";
					WordTools.addContent(document, content);
				} else {
					content = "      ○ "
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex()));
					WordTools.addContent(document, content);
				}
			}
		}

		return index;
	}
}
