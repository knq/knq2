package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the question edit in the QMS
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QuestionOptionEditDialog extends BaseDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2173285901250920835L;
	QuestionnaireService service;
	JLabel orderNoLabel;
	JLabel oidLabel;
	JScrollPane detailJSPane;
	JTextArea opDetail;
	JTextField opLabel;
	JTextField opValue;
	JCheckBox defaultOp;
	// JCheckBox disableOp;
	JTable table;
	JComboBox isDisable;
	JComboBox layoutCb;

	public QuestionOptionEditDialog(Map<String, Object> params) {
		super(params);
	}

	public void initDialog() {
		this.setTitle(mxResources.get("opAttributes"));
		service = new QuestionnaireService();
		table = (JTable) params.get("table");
	}

	public JPanel createPanel() {
		orderNoLabel = new JLabel("");
		oidLabel = new JLabel("");
		opLabel = new JTextField("");
		opDetail = new JTextArea("", 2, 36);
		opDetail.setLineWrap(true);
		opDetail.setWrapStyleWord(true);
		detailJSPane = new JScrollPane(opDetail);
		detailJSPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		detailJSPane.setPreferredSize(new Dimension(150, 50));
		opValue = new JTextField("");
		defaultOp = new JCheckBox("");
		// disableOp = new JCheckBox("");
		Object[] isDisableArray = QuestionConstants.listDisable();
		isDisable = new JComboBox(isDisableArray);
		isDisable.removeItemAt(2);

		Object[] layoutArray = QuestionConstants.listLayout();
		layoutCb = new JComboBox(layoutArray);

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		int index = 0;

		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("oid:"), c, 0,
				index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, oidLabel, c, 1, index, 2, 1,
				GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("orderNumber") + ":"), c, 0, index,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, orderNoLabel, c, 1, index, 2,
				1, GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("label") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, opLabel, c, 1, index, 2, 1,
				GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("detail") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, detailJSPane, c, 1, index, 2,
				1, GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("value") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, opValue, c, 1, index, 2, 1,
				GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("layout") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, layoutCb, c, 1, index, 2, 1,
				GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray, defaultOp, c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("checkValue")), c, 1, index, 2, 1,
				GridBagConstraints.EAST);
		index++;

		/*
		 * UIHelper.addToBagpanel(panel, Color.gray, disableOp, c, 0, index, 1,
		 * 1, GridBagConstraints.EAST); UIHelper.addToBagpanel(panel,
		 * Color.gray, new JLabel(mxResources.get("checkDisable")), c, 1, index,
		 * 2, 1, GridBagConstraints.EAST); index++;
		 */
		String qid = (String) params.get("qid");
		final Question question = service.searchQuestion(qid);
		if (question != null && question.getDisable() == 0) {// DISABLE去除
			// panel.remove(panel.getComponentCount() - 3);
			// panel.remove(panel.getComponentCount() - 2);
			isDisable.setEnabled(false);
			panel.updateUI();
		} else {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("isDisable") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, isDisable, c, 1, index,
					2, 1, GridBagConstraints.EAST);

			index++;
		}

		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		if ("".equals(opDetail.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("opDetailNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if ("".equals(opValue.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("opValueNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		// check if same for label
		String op = (String) params.get("op");
		if ("add".equals(op) && service.getSameByLabel(opLabel.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("labelCheckSame") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * After open the Dialog
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		String op = (String) params.get("op");
		String qlabel = (String) params.get("qlabel");
		String qid = (String) params.get("qid");
		if ("add".equals(op)) {
			this.setTitle("Add Question Option");
			DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
			String oid = genQuestionOption(tableModel, qid);
			orderNoLabel.setText((tableModel.getRowCount() + 1) + "");
			oidLabel.setText(oid);
			opLabel.setText(qlabel + "_"
					+ StringUtil.substringAfterLast(oid, "_"));
			opValue.setText(StringUtil.substringAfterLast(oid, "_"));
		} else {
			this.setTitle("Edit Question Option");
			int row = table.getSelectedRow();
			if (row > -1) {
				DefaultTableModel tableModel = (DefaultTableModel) table
						.getModel();
				oidLabel.setText("" + tableModel.getValueAt(row, 0));
				orderNoLabel.setText("" + tableModel.getValueAt(row, 1));
				opDetail.setText((String) tableModel.getValueAt(row, 2));
				opValue.setText((String) tableModel.getValueAt(row, 3));
				opLabel.setText((String) tableModel.getValueAt(row, 9));
				if ("true".equals(tableModel.getValueAt(row, 6))) {
					defaultOp.setSelected(true);
				} else {
					defaultOp.setSelected(false);
				}
				/*
				 * if ("1".equals(tableModel.getValueAt(row, 7))) {
				 * disableOp.setSelected(true); } else {
				 * disableOp.setSelected(false); }
				 */

				if (Integer.parseInt((String) tableModel.getValueAt(row, 7)) != 0) {
					isDisable
							.setSelectedIndex(QuestionConstants
									.getDisableIndex(""
											+ tableModel.getValueAt(row, 7)));
				}

				layoutCb.setSelectedIndex(QuestionConstants
						.getLayout((String) tableModel.getValueAt(row, 5)));
			}
		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		String op = (String) params.get("op");
		String detail = opDetail.getText();
		String value = opValue.getText();
		String optionLabel = opLabel.getText();
		boolean def = defaultOp.isSelected();
		String oid = oidLabel.getText();
		// int disable = disableOp.isSelected() == true ? 1 : 0;

		ComboItem isdisableItem = (ComboItem) isDisable.getSelectedItem();
		ComboItem layoutItem = (ComboItem) layoutCb.getSelectedItem();
		if ("add".equals(op)) {
			DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
			tableModel.addRow(new Object[] { oid, "", detail, value, "",
					layoutItem.getValue(), "" + def,
					"" + Integer.parseInt(isdisableItem.getValue()), "",
					optionLabel });
		} else {
			int row = table.getSelectedRow();
			if (row > -1) {
				DefaultTableModel tableModel = (DefaultTableModel) table
						.getModel();
				tableModel.setValueAt(detail, row, 2);
				tableModel.setValueAt(value, row, 3);
				tableModel.setValueAt(layoutItem.getValue(), row, 5);
				tableModel.setValueAt("" + def, row, 6);
				tableModel
						.setValueAt(
								"" + Integer.parseInt(isdisableItem.getValue()),
								row, 7);
				tableModel.setValueAt(optionLabel, row, 9);
			}
		}
		return new MessageModel(true, "", 1);
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	private String genQuestionOption(DefaultTableModel tableModel, String qlabel) {
		int max = -1;
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			String oid = (String) tableModel.getValueAt(i, 0);
			if (StringUtils.isNotBlank(oid)) {
				int num = Integer.parseInt(StringUtil.substringAfterLast(oid,
						"_"));
				max = Math.max(num, max);
			}
		}
		return qlabel + "_" + (max + 1);

	}
}
