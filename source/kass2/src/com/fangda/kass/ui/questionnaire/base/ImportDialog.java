package com.fangda.kass.ui.questionnaire.base;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.KnqMain;
import com.fangda.kass.StartMain;
import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.config.IsoLangCode;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.PropertiesTools;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.DefaultMenuBar;
import com.fangda.kass.ui.questionnaire.QnMainPanel;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of importing the multilingual questionnaires from
 * the old version of KNQ.
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class ImportDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6889210562920122717L;

	public static Logger logger = LoggerFactory.getLogger(ImportDialog.class);

	JTextField questionnaireMDirectorField;
	JTextField questionnaireQDirectorField;
	JTextField questionnaireXDirectorField;
	String path;
	QuestionnaireService service;
	String fileName;
	Integer fisError;

	public void initDialog() {
		this.setTitle(mxResources.get("questionImport"));
		// this.setSize(n);
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		questionnaireMDirectorField = new JTextField(50);
		questionnaireQDirectorField = new JTextField(50);
		questionnaireXDirectorField = new JTextField(50);
		questionnaireMDirectorField.setText(mxResources.get("messageFile"));
		questionnaireQDirectorField.setText(mxResources.get("questionFile"));

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(400, 30));
		titlePanel1.add(new JLabel("<HTML><STRONG>" + mxResources.get("source")
				+ "</STRONG></HTML>"));

		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new BorderLayout(8, 8));

		contentPanel1.add(
				new JLabel(mxResources.get("questionnaireMessageFile")),
				BorderLayout.NORTH);

		JPanel TextPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		TextPanel1.add(questionnaireMDirectorField);
		TextPanel1.add(questionnaireQDirectorField);
		contentPanel1.add(TextPanel1, BorderLayout.WEST);

		JPanel buttonPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QMDirectorButton = new JButton("...");
		JButton QQDirectorButton = new JButton("...");
		QMDirectorButton.setPreferredSize(new Dimension(30, 20));
		QQDirectorButton.setPreferredSize(new Dimension(30, 20));
		buttonPanel1.add(QMDirectorButton);
		buttonPanel1.add(QQDirectorButton);
		contentPanel1.add(buttonPanel1, BorderLayout.EAST);

		JPanel titlePanel2 = new JPanel(new GridLayout(1, 1));
		titlePanel2.setPreferredSize(new Dimension(400, 30));
		titlePanel2.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("destination") + "</STRONG></HTML>"));

		titlePanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel2 = new JPanel(new BorderLayout(5, 5));

		contentPanel2.add(new JLabel(mxResources.get("questionnaireXMLFile")),
				BorderLayout.NORTH);

		JPanel TextPanel2 = new JPanel();
		TextPanel2.add(questionnaireXDirectorField);
		contentPanel2.add(TextPanel2, BorderLayout.WEST);

		JPanel buttonPanel2 = new JPanel();
		JButton QXDirectorButton = new JButton("...");
		QXDirectorButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		buttonPanel2.add(QXDirectorButton);
		contentPanel2.add(buttonPanel2, BorderLayout.EAST);

		QMDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("properties");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "Properties Files(*.properties)";
					}

				});

				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					questionnaireMDirectorField.setText(absPath);
					String path = StringUtil.substringBeforeLast(absPath,
							File.separator);

					String fileName = StringUtil.substringAfterLast(absPath,
							File.separator);
					questionnaireQDirectorField.setText(path + File.separator
							+ fileName.replace("messages", "questions"));
					questionnaireXDirectorField.setText(path);

				}
			}
		});

		QQDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("properties");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "Properties Files(*.properties)";
					}

				});
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					questionnaireQDirectorField.setText(chooser
							.getSelectedFile().getAbsolutePath());
				}
			}
		});

		QXDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					questionnaireXDirectorField.setText(chooser
							.getSelectedFile().getAbsolutePath());
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		panelBorder.add(titlePanel2, c);
		panelBorder.add(contentPanel2, c);
		return panelBorder;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		String qMDirector = questionnaireMDirectorField.getText();
		String qQDirector = questionnaireQDirectorField.getText();
		String qXDirector = questionnaireXDirectorField.getText();
		if ("".equals(qMDirector) || "".equals(qQDirector)) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("questionnaireTXTFileNull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if ("".equals(qXDirector)) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("questionnaireXMLFileNull")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}

		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		String messagePath = questionnaireMDirectorField.getText();
		String questionPath = questionnaireQDirectorField.getText();
		path = questionnaireXDirectorField.getText();
		String questionName = StringUtil.substringAfterLast(questionPath, "\\");
		fileName = questionName;

		File file = new File(path);
		file.mkdirs();
		String a = StringUtil.substringBeforeLast(questionName, ".");
		path = path + File.separator + a + "_v2.properties";
		InputStream is = null;
		try {
			is = new FileInputStream(
					"Questionnaire/template/questions_v2.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		FileUtil.copySingleFile(is, path);
		Integer ifError = 0;
		ifError = PropertiesTools.readProperties(messagePath, questionPath,
				path);
		if (ifError == 1) {
			fisError = 1;
			return new MessageModel(true, "Error! File is not correct!");
		} else {
			fisError = 0;
		}
		Questionnaire q = service.get();
		String[] args = a.split("_");
		if (args.length >= 3) {
			String language = args[1];
			String country = args[2];

			q.setLanguage(language);
			int index = QuestionConstants.getLanIndex(language + "-" + country);
			if (index >= 0) {
				Object obj = QuestionConstants.listLans()[index];
				IsoLangCode item = (IsoLangCode) obj;
				q.setLanguageName(item.getLanguageName());
				q.setCountry(item.getCountryCode());
				q.setCountryName(item.getCountryName());
				q.getLanguageCountry().getIsoLangCode().setCode(item.getCode());
				q.getLanguageCountry().getIsoLangCode()
						.setCountryCode(item.getCountryCode());
				q.getLanguageCountry().getIsoLangCode()
						.setCountryName(item.getCountryName());
				q.getLanguageCountry().getIsoLangCode()
						.setLanguageCode(item.getLanguageCode());
				q.getLanguageCountry().getIsoLangCode()
						.setLanguageName(item.getLanguageName());
				q.getLanguageCountry().getIsoLangCode()
						.setLocalCountry(item.getLocalCountry());
				q.getLanguageCountry().getIsoLangCode()
						.setLocalLanguage(item.getLocalLanguage());
				q.getLanguageCountry().getIsoLangCode()
						.setLanguageName(item.getLocalName());
				q.getLanguageCountry().getIsoLangCode()
						.setLocalName(item.getLocalName());
				q.getLanguageCountry().getIsoLangCode().setName(item.getName());
			}
		}
		copyFile(q);
		service.save(q);
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After Submit
	 */
	@Override
	public void afterSubmit() {
		if (fisError == 1)
			return;
		if (km == null) {
			StartMain.getInstance().setVisible(false);
			KnqMain kmain = new KnqMain();
			kmain.qnMainPanel = new QnMainPanel(kmain);
			kmain.setLocation(100, 100);
			kmain.createFrame(new DefaultMenuBar(kmain), kmain.qnMainPanel)
					.setVisible(true);
			kmain.setAfterShowEvent("import");
			kmain.qnMainPanel.reload(path, "local");
			kmain.setTitle(fileName);
			StartMain.getInstance().dispose();
		} else {
			if (km.qnMainPanel == null) {
				km.qnMainPanel = new QnMainPanel(km);
				km.setMainPanel(km.qnMainPanel);
			}
			km.qnMainPanel.reload(path, "local");
		}
	}

	public void copyFile(Questionnaire q) {
		String code = q.getLanguageCountry().getIsoLangCode().getCode()
				.replace("-", "_");
		File file = new File("language/message_" + code + "_" + q.getVersion()
				+ ".properties");
		if (!file.exists()) {
			try {
				file.createNewFile();
				InputStream is = new FileInputStream(Constants.MESSAGE_PATH);
				FileUtil.copySingleFile(is, file.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File file1 = new File("language/config_" + code + "_" + q.getVersion()
				+ ".properties");
		if (!file1.exists()) {
			try {
				file1.createNewFile();
				InputStream is = new FileInputStream(Constants.CONFIG_PATH);
				FileUtil.copySingleFile(is, file1.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}