package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.PqdQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the union required question edit of the
 * questionnaire.
 * 
 * @author Fangfang Zhao
 * 
 */
public class UnionRequiredQuestionDialog extends BaseDialog {
	private static final long serialVersionUID = 1L;
	JPanel panel1;
	JButton addBtn;
	JLabel qidLabel;
	JLabel noteLabel;
	double oldHeight = 0;
	QuestionnaireService service;

	public void initDialog() {
		this.setTitle(mxResources.get("SetUnionRequiredQuestionBtn"));
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		qidLabel = new JLabel("qid:");
		noteLabel = new JLabel(mxResources.get("SetUnionRequiredQuestionBtn"));

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		List<Question> questions = service.searchUnionQuestion();

		panel1 = new JPanel();
		panel1.setBackground(Color.white);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		final ComboItem[] questionItems = new ComboItem[questions.size() + 1];
		questionItems[0] = new ComboItem("", "");
		for (int i = 0; i < questions.size(); i++) {
			Question q = questions.get(i);
			ComboItem item = new ComboItem(q.getQid() + ":" + q.getTitle(),
					q.getQid());
			questionItems[i + 1] = item;
		}
		List<PqdQuestion> pqdquestions = service.get().getDraw().getUnion()
				.getuQuestion();
		for (PqdQuestion p : pqdquestions) {
			JPanel jp = genRow(questionItems, p.getQid());
			panel1.add(jp);
		}

		panel1.addComponentListener(componentlistener);
		addBtn = new JButton("Add One Row");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel jpanel = genRow(questionItems, "");
				panel1.add(jpanel);
				panel1.updateUI();
			}
		});
		panel.add(addBtn, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		panel.add(panel1, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));

		return panel;
	}

	private JPanel genRow(ComboItem[] qitems, String jbox) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);

		JComboBox box = new JComboBox(qitems);
		int index = getSelectIndex(box, jbox);
		box.setSelectedIndex(index);
		UIHelper.addToBagpanel(panel, Color.gray, box, c, 0, 0, 1, 1,
				GridBagConstraints.EAST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, 0, 1, 1,
				GridBagConstraints.EAST);
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				panel1.updateUI();
			}
		});
		return panel;
	}

	private int getSelectIndex(JComboBox jc, String value) {
		int size = jc.getModel().getSize();
		for (int i = 0; i < size; i++) {
			ComboItem item = (ComboItem) jc.getModel().getElementAt(i);
			if (item.getValue().equals(value)) {
				return i;
			}
		}
		return 0;
	}

	private void deleteRow(JButton deleteBtn) {
		JPanel jpanel = (JPanel) deleteBtn.getParent();
		deleteBtn.getParent().getParent().remove(jpanel);
	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		Component[] components = panel1.getComponents();
		List<PqdQuestion> questions = service.get().getDraw().getUnion()
				.getuQuestion();
		questions.clear();
		if (components != null && components.length > 0) {
			for (Component jpanel : components) {
				if (jpanel instanceof JPanel) {
					JPanel jp = (JPanel) jpanel;
					Component[] cs = jp.getComponents();
					if (cs != null && cs.length > 0) {
						String qid = "";
						for (Component c : cs) {
							if (c instanceof JComboBox) {
								ComboItem item = (ComboItem) ((JComboBox) c)
										.getSelectedItem();
								qid = item.getValue();
							}
						}
						if (StringUtils.isNotBlank(qid)) {
							PqdQuestion pquestion = new PqdQuestion();
							pquestion.setQid(qid);
							questions.add(pquestion);
						}
					}
				}
			}
		}
		service.save(service.get());
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel1) {
			Dimension currSize = (Dimension) panel1.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				double change = currSize.getHeight() - oldHeight;
				Dimension dialogSize = (Dimension) this.getSize().clone();
				dialogSize.height = (int) (dialogSize.height + change);
				this.setSize(dialogSize);
				oldHeight = currSize.getHeight();
			}
		}
	}
}
