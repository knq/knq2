package com.fangda.kass.ui.questionnaire;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.TransferHandler;
import javax.swing.border.EmptyBorder;

import com.fangda.kass.KnqMain;

/**
 * This Class for the Editor Explore
 * 
 * @author Fangfang Zhao
 * 
 */
public class EditorExplore extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3984983899883039029L;

	@SuppressWarnings("serial")
	public EditorExplore() {
		setBackground(new Color(255, 255, 255));
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new GridLayout(200, 1, 5, 5));
		

		// Clears the current selection when the background is clicked
		addMouseListener(new MouseListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent
			 * )
			 */
			public void mousePressed(MouseEvent e) {

			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			public void mouseClicked(MouseEvent e) {
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent
			 * )
			 */
			public void mouseEntered(MouseEvent e) {
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent
			 * )
			 */
			public void mouseExited(MouseEvent e) {
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent
			 * )
			 */
			public void mouseReleased(MouseEvent e) {
			}

		});

		// Shows a nice icon for drag and drop but doesn't import anything
		setTransferHandler(new TransferHandler() {
			public boolean canImport(JComponent comp, DataFlavor[] flavors) {
				return true;
			}
		});
	}

	public void addTemplate(final String name, final boolean isSelected) {
		ImageIcon icon = new ImageIcon(
				KnqMain.class.getResource("/images/open.gif"));
		final JLabel label = new JLabel();
		label.setIcon(icon);
		if (isSelected) {
			label.setText("<HTML><STRONG>" + name + "</STRONG></HTML>");
		} else {
			label.setText(name);
		}
		label.setToolTipText(name);
		label.setIconTextGap(1);
		label.setBackground(this.getBackground().brighter());
		//label.setFont(new Font(label.getFont().getFamily(), 0, 14));
		label.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		label.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		this.add(label);
	}
}
