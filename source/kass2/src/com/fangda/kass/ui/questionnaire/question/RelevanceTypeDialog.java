package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.config.RelevanceType;
import com.fangda.kass.model.config.RelevanceTypeField;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the Relevance Type view, edit and delete in the
 * QMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class RelevanceTypeDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5243290076072877886L;
	JPanel panel;
	JButton addBtn;
	JLabel qidLabel;
	JLabel noteLabel;
	double oldHeight = 0;
	int index = 0;
	GridBagConstraints c = null;
	RelevanceTypeField lastField = null;
	boolean addHeight = false;
	QuestionnaireService service;

	public void initDialog() {
		this.setTitle(mxResources.get("SetRelevanceTypeBtn"));
		c = new GridBagConstraints();
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		qidLabel = new JLabel("qid:");
		noteLabel = new JLabel(mxResources.get("SetRelevanceTypeBtn"));

		GridBagLayout bagLayout = new GridBagLayout();

		panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		RelevanceType relevantType = QuestionConstants.getRelevantType();
		List<RelevanceTypeField> fields = relevantType.getFields();

		panel.addComponentListener(componentlistener);
		addBtn = new JButton("Add Condition");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Map map = new HashMap();
				map.put("typedialog", RelevanceTypeDialog.this);
				new RelevanceTypeConditionDialog(map);
			}
		});
		panel.add(addBtn, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		index++;
		for (RelevanceTypeField p : fields) {
			genRow(p.getName(), p.getValue(), p.getCondition(), c, index);
			index++;
		}
		return panel;
	}

	private void genRow(final String name, final String value,
			String condition, GridBagConstraints c, int index) {
		JLabel nameLabel = new JLabel(name);
		JLabel valueLabel = new JLabel(value);
		JLabel conditionLabel = new JLabel(condition);

		UIHelper.addToBagpanel(panel, Color.gray, nameLabel, c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, valueLabel, c, 1, index, 1,
				1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, conditionLabel, c, 2, index,
				1, 1, GridBagConstraints.EAST);
		JButton editBtn = BtnHelper.createConfigButton("");
		UIHelper.addToBagpanel(panel, Color.gray, editBtn, c, 3, index, 1, 1,
				GridBagConstraints.EAST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, index, 1, 1,
				GridBagConstraints.EAST);
		editBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Map map = new HashMap();
				map.put("typedialog", RelevanceTypeDialog.this);
				map.put("name", name);
				map.put("value", value);
				new RelevanceTypeConditionDialog(map);
			}
		});
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				addSize(-38);
			}
		});
	}

	private void deleteRow(JButton deleteBtn) {
		JPanel jpanel = (JPanel) deleteBtn.getParent();
		int index = getIndexOfTab(jpanel, deleteBtn);
		jpanel.remove(index);
		jpanel.remove(index - 1);
		jpanel.remove(index - 2);
		jpanel.remove(index - 3);
		jpanel.remove(index - 4);
	}

	@Override
	public void afterShow() {
		super.afterShow();
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		int count = panel.getComponentCount();
		RelevanceType relevantType = QuestionConstants.getRelevantType();
		relevantType.getFields().clear();
		if (count > 1) {
			int z = (count - 1) / 5;
			for (int i = 0; i < z; i++) {
				Component c1 = panel.getComponent(i * 5 + 1);
				JLabel box1 = (JLabel) c1;
				String a = box1.getText();
				Component c2 = panel.getComponent(i * 5 + 2);
				JLabel box2 = (JLabel) c2;
				String b = box2.getText();
				Component c3 = panel.getComponent(i * 5 + 3);
				JLabel box3 = (JLabel) c3;
				String c = box3.getText();

				RelevanceTypeField filed = new RelevanceTypeField();
				filed.setCondition(c);
				filed.setName(a);
				filed.setValue(b);
				relevantType.getFields().add(filed);
			}
			// QuestionConstants.saveConfig();
			service.save(service.get());
		}
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel) {
			Dimension currSize = (Dimension) panel.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				if (!addHeight) {
					double change = currSize.getHeight() - oldHeight;
					Dimension dialogSize = (Dimension) this.getSize().clone();
					dialogSize.height = (int) (dialogSize.height + change);
					this.setSize(dialogSize);
					oldHeight = currSize.getHeight();
				} else {
					addHeight = false;
				}
			}
		}
	}

	public void addNew() {
		genRow(lastField.getName(), lastField.getValue(),
				lastField.getCondition(), c, index);
		addSize(38);
		index++;

	}

	public void updateFiled() {
		int count = panel.getComponentCount();
		if (count > 1) {
			int z = (count - 1) / 5;
			for (int i = 0; i < z; i++) {
				Component c1 = panel.getComponent(i * 5 + 1);
				JLabel box1 = (JLabel) c1;
				Component c2 = panel.getComponent(i * 5 + 2);
				JLabel box2 = (JLabel) c2;
				String b = box2.getText();
				Component c3 = panel.getComponent(i * 5 + 3);
				JLabel box3 = (JLabel) c3;
				if (StringUtils.equals(b, lastField.getValue())) {
					box1.setText(lastField.getName());
					box3.setText(lastField.getCondition());
					box2.setText(b);
					break;
				}
			}
		}
	}

	public void addSize(int change) {
		Dimension currSize = (Dimension) panel.getSize().clone();
		Dimension dialogSize = (Dimension) this.getSize().clone();
		dialogSize.height = (int) (dialogSize.height + change);
		this.setSize(dialogSize);
		oldHeight = currSize.getHeight();
		addHeight = true;
		return;
	}

	public int getIndexOfTab(JPanel jpanel, JButton delBtn) {
		int count = panel.getComponentCount();
		for (int i = 0; i < count; i++) {
			Component c = panel.getComponent(i);
			if (delBtn == c) {
				return i;
			}
		}
		return -1;
	}
}
