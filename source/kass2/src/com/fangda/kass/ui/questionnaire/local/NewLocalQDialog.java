package com.fangda.kass.ui.questionnaire.local;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.IsoLangCode;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.QnMainPanel;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.FileUtil;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of new a local language questionnaire.
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * 
 */
public class NewLocalQDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8765851307354206026L;

	public static Logger logger = LoggerFactory
			.getLogger(NewLocalQDialog.class);

	JTextField questionnaireMDirectorField;
	JTextField questionnaireQDirectorField;
	JTextField questionnaireXDirectorField;
	String path;
	QuestionnaireService service;
	String fileName;
	Integer fisError;

	// 2015-06-22
	JComboBox fieldLanguageBox;
	JComboBox fieldCountryBox;

	public static String FILE_SEPTER_FLAG = ".";
	public static String FILE_JOIN_FLAG = "_";
	public static String ORIGINAL_MESSAGE_FILE_NAME = "message";
	public static String ORIGINAL_CONFIG_FILE_NAME = "config";
	public static String FILE_EXTENT_NAME = "properties";

	public String curSelLocalPath; // the directory path of the local language
									// questionnaire

	// 2015-06-22

	public void initDialog() {
		this.setTitle(mxResources.get("newLocalQ"));
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		questionnaireMDirectorField = new JTextField(80);

		File file = new File("Questionnaire/template/questions_v2.properties");
		questionnaireMDirectorField.setText(file.getAbsolutePath());

		questionnaireXDirectorField = new JTextField(80);
		questionnaireXDirectorField.setText("");

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(600, 30));
		titlePanel1.add(new JLabel("<HTML><STRONG>" + mxResources.get("source")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new BorderLayout(8, 8));
		contentPanel1.add(
				new JLabel(mxResources.get("localUISourceDirectoryMsg")),
				BorderLayout.NORTH);

		JPanel TextPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		TextPanel1.add(questionnaireMDirectorField);
		contentPanel1.add(TextPanel1, BorderLayout.WEST);

		JPanel buttonPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QMDirectorButton = new JButton("...");
		QMDirectorButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		buttonPanel1.add(QMDirectorButton);
		contentPanel1.add(buttonPanel1, BorderLayout.EAST);

		JPanel titlePanel2 = new JPanel(new GridLayout(1, 1));
		titlePanel2.setPreferredSize(new Dimension(600, 30));
		titlePanel2.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("destination") + "</STRONG></HTML>"));
		titlePanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel emptyRowPanel = new JPanel(new GridLayout(1, 1));
		emptyRowPanel.setPreferredSize(new Dimension(600, 5));

		JPanel contentPanellanguageAndCountry = new JPanel(new BorderLayout(8,
				8));

		JPanel contentPanellanguage = new JPanel(new BorderLayout(5, 5));
		contentPanellanguage.add(
				new JLabel(mxResources.get("localUILanguageMsg")),
				BorderLayout.WEST);
		fieldLanguageBox = new JComboBox(QuestionConstants.listLanguage());
		contentPanellanguage.add(fieldLanguageBox, BorderLayout.EAST);

		contentPanellanguageAndCountry.add(contentPanellanguage,
				BorderLayout.WEST);

		JPanel contentPanelCountry = new JPanel(new BorderLayout(5, 5));
		contentPanelCountry.add(
				new JLabel(mxResources.get("localUICountryMsg")),
				BorderLayout.WEST);
		fieldCountryBox = new JComboBox(QuestionConstants.listCountry(""));
		contentPanelCountry.add(fieldCountryBox, BorderLayout.EAST);

		contentPanellanguageAndCountry.add(contentPanelCountry,
				BorderLayout.EAST);

		JPanel contentPanel2 = new JPanel(new BorderLayout(5, 5));
		contentPanel2.add(
				new JLabel("<html><br>"
						+ mxResources.get("localUIDestinationDirectoryMsg")
						+ "</html>"), BorderLayout.NORTH);

		JPanel TextPanel2 = new JPanel();
		TextPanel2.add(questionnaireXDirectorField);
		contentPanel2.add(TextPanel2, BorderLayout.WEST);

		JPanel buttonPanel2 = new JPanel();
		JButton QXDirectorButton = new JButton("...");
		QXDirectorButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		buttonPanel2.add(QXDirectorButton);

		contentPanel2.add(buttonPanel2, BorderLayout.EAST);

		/**
		 * the comboBox for language
		 */

		fieldLanguageBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == fieldLanguageBox) {
					ComboItem item = (ComboItem) fieldLanguageBox
							.getSelectedItem();
					DefaultComboBoxModel model = (DefaultComboBoxModel) fieldCountryBox
							.getModel();
					model.removeAllElements();
					Object[] obj = QuestionConstants.listCountry(item
							.getValue());
					for (int i = 0; i < obj.length; i++) {
						model.addElement(obj[i]);
					}
					setLocalFileFullPath();
				}
			}
		});

		fieldCountryBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setLocalFileFullPath();
			}
		});

		/**
		 * Source file of the English questionnaire
		 */
		QMDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("properties");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "Properties Files(*.properties)";
					}

				});

				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					questionnaireMDirectorField.setText(absPath);
					setLocalFileFullPath();
				}
			}
		});

		/**
		 * Select the local language questionnaire
		 */
		QXDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					String selectPath = chooser.getSelectedFile()
							.getAbsolutePath();
					curSelLocalPath = selectPath;
					setLocalFileFullPath();
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;

		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		panelBorder.add(titlePanel2, c);
		panelBorder.add(emptyRowPanel, c);
		panelBorder.add(contentPanellanguageAndCountry, c); //
		panelBorder.add(contentPanel2, c);

		return panelBorder;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub

		String qMDirector = questionnaireMDirectorField.getText();
		String qXDirector = questionnaireXDirectorField.getText();
		if ("".equals(qMDirector)) {
			String originalVersonFullMsg = "Please Choose Original Version Questionnaire File !";
			JOptionPane.showMessageDialog(container, "<HTML>"
					+ originalVersonFullMsg + "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if ("".equals(qXDirector)) {
			String localVersonFullMsg = "Please Choose Localization Version Questionnaire Directory! ";
			JOptionPane.showMessageDialog(container, "<HTML>"
					+ localVersonFullMsg + "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}

		String localFileFullPath = questionnaireXDirectorField.getText();
		File localfile = new File(localFileFullPath);
		if (localfile.exists()) {
			JLabel lb1 = new JLabel();
			lb1.setText(mxResources.get("localUISameFileExistMsg"));
			UIHelper.createMultiLabel(lb1, 300);

			int replaced = JOptionPane
					.showConfirmDialog(this, lb1,
							mxResources.get("localUISameFileTitleMsg"),
							JOptionPane.YES_NO_OPTION, 0,
							UIHelper.getImage("logo.png"));
			switch (replaced) {
			case JOptionPane.CLOSED_OPTION:
				return false;
			case JOptionPane.NO_OPTION:
				return false;
			case JOptionPane.YES_OPTION:
				if (localfile.delete()) {
					return true;
				} else {
					JLabel lb2 = new JLabel();
					lb1.setText(mxResources.get("localUISameFileDeleteMsg"));
					UIHelper.createMultiLabel(lb1, 300);
					JOptionPane.showConfirmDialog(null, lb2,
							mxResources.get("localUISameFileTitleMsg"),
							JOptionPane.PLAIN_MESSAGE, 0,
							UIHelper.getImage("logo.png"));
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		try {

			// 1 保存questions_v2.properties 文件
			String orginalFileFullPath = questionnaireMDirectorField.getText();
			String localFileFullPath = questionnaireXDirectorField.getText();

			File localfile = new File(localFileFullPath);
			FileUtil.copySingleFile(orginalFileFullPath, localFileFullPath);

			ComboItem languageItem = (ComboItem) fieldLanguageBox
					.getSelectedItem();
			ComboItem countryItem = (ComboItem) fieldCountryBox
					.getSelectedItem();

			Questionnaire localQuestion = XmlHelper.getInstance()
					.readFileProperty(localFileFullPath);

			localQuestion.setStatus("Draft");

			localQuestion.setLanguage(languageItem.getValue());
			localQuestion.setLanguageName(languageItem.getName());

			localQuestion.setCountry(countryItem.getValue());
			localQuestion.setCountryName(countryItem.getName());

			IsoLangCode localLangCode = localQuestion.getLanguageCountry()
					.getIsoLangCode();
			com.fangda.kass.model.config.IsoLangCode fromFile = QuestionConstants
					.getLangByCode(languageItem.getValue() + "-"
							+ countryItem.getValue());

			localLangCode.setCode(fromFile.getCode());
			localLangCode.setName(fromFile.getName());
			localLangCode.setLocalCountry(fromFile.getLocalCountry());
			localLangCode.setLocalLanguage(fromFile.getLocalLanguage());
			localLangCode.setLocalName(fromFile.getLocalName());

			localLangCode.setLanguageCode(languageItem.getValue());
			localLangCode.setLanguageName(languageItem.getName());
			localLangCode.setCountryName(countryItem.getValue());
			localLangCode.setCountryCode(countryItem.getName());

			// 2015-08-02
			// 1.<Questionnaire>： en_title，en_cultureArea，en_note
			// if <> en，then Null
			if (localQuestion.getTitle() != null) {
				localQuestion.setEn_title(localQuestion.getTitle());
				if (!"en".equals(languageItem.getValue())) {
					localQuestion.setTitle("");
				}
			}

			if (localQuestion.getNote() != null) {
				localQuestion.setEn_note(localQuestion.getNote());
				if (!"en".equals(languageItem.getValue())) {
					localQuestion.setNote("");
				}
			}

			if (localQuestion.getCultureArea() != null) {
				localQuestion.setEn_cultureArea(localQuestion.getCultureArea());
				if (!"en".equals(languageItem.getValue())) {
					localQuestion.setCultureArea("");
				}
			}

			// 2. < completeProcedure><step>：en_title。
			// 3.< completeProcedure><step><subStep>：en_kinshipTerm
			List<CompleteStep> localCompleteStepList = localQuestion.getDraw()
					.getCompleteProcedure().getCompleteStep();

			if (localCompleteStepList != null
					&& localCompleteStepList.size() > 0) {
				for (CompleteStep item : localCompleteStepList) {

					if (item.getTitle() != null) {
						item.setEn_title(item.getTitle());
						if (!"en".equals(languageItem.getValue())) {
							item.setTitle("");
						}
					}

					List<CompleteSubStep> localCompleteSubStepList = item
							.getSubSteps();
					if (localCompleteSubStepList != null
							&& localCompleteSubStepList.size() > 0) {
						for (CompleteSubStep subStepItem : localCompleteSubStepList) {

							if (subStepItem.getKinshipTerm() != null) {
								subStepItem.setEn_kinshipTerm(subStepItem
										.getKinshipTerm());
								if (!"en".equals(languageItem.getValue())) {
									subStepItem.setKinshipTerm("");
								}
							}

						}
					}
				}
			}

			// 4 <stopRule>：en_note
			if (localQuestion.getDraw().getStopRule().getNote() != null) {
				localQuestion
						.getDraw()
						.getStopRule()
						.setEn_note(
								localQuestion.getDraw().getStopRule().getNote());
				if (!"en".equals(languageItem.getValue())) {
					localQuestion.getDraw().getStopRule().setNote("");
				}
			}

			/*
			 * 5. <subsection>：en_title ,en_detail 6. <question>：en_title
			 * ,en_detail, en_lead 7. < followupQuestion>：en_detail,
			 * en_groupName 8. <option>：en_detail, en_note
			 */

			List<Subsection> subSectionList = localQuestion.getSection()
					.getSubsections();

			if (subSectionList != null && subSectionList.size() > 0) {
				for (Subsection subSectionItem : subSectionList) {
					// 5.Section <subsection>：en_title ,en_detail
					if (subSectionItem.getTitle() != null) {
						subSectionItem.setEn_title(subSectionItem.getTitle());
						if (!"en".equals(languageItem.getValue())) {
							subSectionItem.setTitle("");
						}
					}

					if (subSectionItem.getDetail() != null) {
						subSectionItem.setEn_detail(subSectionItem.getDetail());
						if (!"en".equals(languageItem.getValue())) {
							subSectionItem.setDetail("");
						}
					}

					// 6.<subsection> <question>：en_title ,en_detail,
					// en_lead
					List<Question> questionList = subSectionItem.getQuestions();

					if (questionList != null && questionList.size() > 0) {
						for (Question questionItem : questionList) {

							if (questionItem.getTitle() != null) {
								questionItem.setEn_title(questionItem
										.getTitle());
								if (!"en".equals(languageItem.getValue())) {
									questionItem.setTitle("");
								}
							}

							if (questionItem.getDetail() != null) {
								questionItem.setEn_detail(questionItem
										.getDetail());
								if (!"en".equals(languageItem.getValue())) {
									questionItem.setDetail("");
								}
							}

							if (questionItem.getLead() != null) {
								questionItem.setEn_lead(questionItem.getLead());
								if (!"en".equals(languageItem.getValue())) {
									questionItem.setLead("");
								}
							}

							// 8. <question><option>：en_detail,
							// en_note
							List<Option> optionList = questionItem.getOptions();
							if (optionList != null && optionList.size() > 0) {
								for (Option optionItem : optionList) {

									if (optionItem.getDetail() != null) {
										optionItem.setEn_detail(optionItem
												.getDetail());
										if (!"en".equals(languageItem
												.getValue())) {
											optionItem.setDetail("");
										}
									}

									if (optionItem.getNote() != null) {
										optionItem.setEn_note(optionItem
												.getNote());
										if (!"en".equals(languageItem
												.getValue())) {
											optionItem.setNote("");
										}
									}
								}
							}

							// 7.< followupQuestion>：en_detail, en_groupName
							List<FollowupQuestion> followupQuestionList = questionItem
									.getFollowupQuestions();
							if (followupQuestionList != null
									&& followupQuestionList.size() > 0) {
								for (FollowupQuestion followupQuestionItem : followupQuestionList) {

									if (followupQuestionItem.getDetail() != null) {
										followupQuestionItem
												.setEn_detail(followupQuestionItem
														.getDetail());
										if (!"en".equals(languageItem
												.getValue())) {
											followupQuestionItem.setDetail("");
										}
									}

									if (followupQuestionItem.getGroupName() != null) {
										followupQuestionItem
												.setEn_groupName(followupQuestionItem
														.getGroupName());
										if (!"en".equals(languageItem
												.getValue())) {
											followupQuestionItem
													.setGroupName("");
										}
									}

									// 8.< followupQuestion>
									// <option>：en_detail, en_note
									List<Option> followupOptionList = followupQuestionItem
											.getOptions();
									if (followupOptionList != null
											&& followupOptionList.size() > 0) {
										for (Option followupOptionItem : followupOptionList) {

											if (followupOptionItem.getDetail() != null) {
												followupOptionItem
														.setEn_detail(followupOptionItem
																.getDetail());
												if (!"en".equals(languageItem
														.getValue())) {
													followupOptionItem
															.setDetail("");
												}
											}

											if (followupOptionItem.getNote() != null) {
												followupOptionItem
														.setEn_note(followupOptionItem
																.getNote());
												if (!"en".equals(languageItem
														.getValue())) {
													followupOptionItem
															.setNote("");
												}
											}
										}
									}
								}// FollowupQuestion
							}
						} // Question
					}

					List<Subsection> subSectionList2 = subSectionItem
							.getSubSections();

					if (subSectionList2 != null && subSectionList2.size() > 0) {

						for (Subsection subSectionItem2 : subSectionList2) {

							// Section
							// 5.Section <subsection>：en_title
							// ,en_detail
							if (subSectionItem2.getTitle() != null) {
								subSectionItem2.setEn_title(subSectionItem2
										.getTitle());
								if (!"en".equals(languageItem.getValue())) {
									subSectionItem2.setTitle("");
								}
							}

							if (subSectionItem2.getDetail() != null) {
								subSectionItem2.setEn_detail(subSectionItem2
										.getDetail());
								if (!"en".equals(languageItem.getValue())) {
									subSectionItem2.setDetail("");
								}
							}

							// 6.<subsection> <question>：en_title
							// ,en_detail, en_lead
							List<Question> questionList2 = subSectionItem2
									.getQuestions();

							if (questionList2 != null
									&& questionList2.size() > 0) {
								for (Question questionItem2 : questionList2) {

									if (questionItem2.getTitle() != null) {
										questionItem2.setEn_title(questionItem2
												.getTitle());
										if (!"en".equals(languageItem
												.getValue())) {
											questionItem2.setTitle("");
										}
									}

									if (questionItem2.getDetail() != null) {
										questionItem2
												.setEn_detail(questionItem2
														.getDetail());
										if (!"en".equals(languageItem
												.getValue())) {
											questionItem2.setDetail("");
										}
									}

									if (questionItem2.getLead() != null) {
										questionItem2.setEn_lead(questionItem2
												.getLead());
										if (!"en".equals(languageItem
												.getValue())) {
											questionItem2.setLead("");
										}
									}

									// 8. <question>
									// <option>：en_detail, en_note
									List<Option> optionList2 = questionItem2
											.getOptions();
									if (optionList2 != null
											&& optionList2.size() > 0) {
										for (Option optionItem2 : optionList2) {

											if (optionItem2.getDetail() != null) {
												optionItem2
														.setEn_detail(optionItem2
																.getDetail());
												if (!"en".equals(languageItem
														.getValue())) {
													optionItem2.setDetail("");
												}
											}

											if (optionItem2.getNote() != null) {
												optionItem2
														.setEn_note(optionItem2
																.getNote());
												if (!"en".equals(languageItem
														.getValue())) {
													optionItem2.setNote("");
												}
											}
										}
									}

									// 7.< followupQuestion>：en_detail,
									// en_groupName
									List<FollowupQuestion> followupQuestionList2 = questionItem2
											.getFollowupQuestions();
									if (followupQuestionList2 != null
											&& followupQuestionList2.size() > 0) {
										for (FollowupQuestion followupQuestionItem2 : followupQuestionList2) {

											if (followupQuestionItem2
													.getDetail() != null) {
												followupQuestionItem2
														.setEn_detail(followupQuestionItem2
																.getDetail());
												if (!"en".equals(languageItem
														.getValue())) {
													followupQuestionItem2
															.setDetail("");
												}
											}

											if (followupQuestionItem2
													.getGroupName() != null) {
												followupQuestionItem2
														.setEn_groupName(followupQuestionItem2
																.getGroupName());
												if (!"en".equals(languageItem
														.getValue())) {
													followupQuestionItem2
															.setGroupName("");
												}
											}

											// 8.< followupQuestion>
											// <option>：en_detail,
											// en_note
											List<Option> followupOptionList2 = followupQuestionItem2
													.getOptions();
											if (followupOptionList2 != null
													&& followupOptionList2
															.size() > 0) {
												for (Option followupOptionItem2 : followupOptionList2) {

													if (followupOptionItem2
															.getDetail() != null) {
														followupOptionItem2
																.setEn_detail(followupOptionItem2
																		.getDetail());
														if (!"en"
																.equals(languageItem
																		.getValue())) {
															followupOptionItem2
																	.setDetail("");
														}
													}

													if (followupOptionItem2
															.getNote() != null) {
														followupOptionItem2
																.setEn_note(followupOptionItem2
																		.getNote());
														if (!"en"
																.equals(languageItem
																		.getValue())) {
															followupOptionItem2
																	.setNote("");
														}
													}
												}
											}
										}// FollowupQuestion
									}
								} // Question
							}
						}
					}
				}
			}
			// 2015-08-02 end

			Constants.QUESTIONNAIRE_XML = localFileFullPath;
			XmlHelper.getInstance().saveFile(localQuestion, localFileFullPath);

			// message.properties and config.properties
			this.copyFile(localQuestion);
			fisError = 0;
			return new MessageModel(true, "submit success!");
		} catch (Exception e) {
			e.printStackTrace();
			fisError = 1;
			return new MessageModel(false, "sumbit fail!");
		}
	}

	public void copyFile(Questionnaire q) {
		String code = q.getLanguageCountry().getIsoLangCode().getCode()
				.replace("-", "_");
		File file = new File("language/message_" + code + "_" + q.getVersion()
				+ ".properties");
		if (!file.exists()) {
			try {
				file.createNewFile();
				InputStream is = new FileInputStream(Constants.MESSAGE_PATH);
				FileUtil.copySingleFile(is, file.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		File file1 = new File("language/config_" + code + "_" + q.getVersion()
				+ ".properties");
		if (!file1.exists()) {
			try {
				file1.createNewFile();
				InputStream is = new FileInputStream(Constants.CONFIG_PATH);
				FileUtil.copySingleFile(is, file1.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {
		try {
			String localFileFullPath = questionnaireXDirectorField.getText();
			if (km.qnMainPanel == null) {
				km.qnMainPanel = new QnMainPanel(km);
				km.setMainPanel(km.qnMainPanel);
			}

			ComboItem languageItem = (ComboItem) fieldLanguageBox
					.getSelectedItem();
			if ("en".equals(languageItem.getValue())) {
				// English: View mode
				String questionName = StringUtil.substringAfterLast(
						localFileFullPath, "\\");
				km.questionairPath = questionName;
				km.qnMainPanel.ifShowDetail = false;
				km.setLocation(100, 100);
				km.qnMainPanel.reload(localFileFullPath, "view");
			} else {
				// Local: Localize mode
				km.qnMainPanel.reload(localFileFullPath, "local");
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Open LocalPanel Fail!",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void setLocalFileFullPath() {
		if (curSelLocalPath != null) {
			ComboItem countryItem = (ComboItem) fieldCountryBox
					.getSelectedItem();
			ComboItem languageItem = (ComboItem) fieldLanguageBox
					.getSelectedItem();

			if (languageItem != null && languageItem.getValue() != null
					&& countryItem != null && countryItem.getValue() != null) {
				String originalPath = questionnaireMDirectorField.getText();
				String originalfileFullName = StringUtil.substringAfterLast(
						originalPath, File.separator);
				String[] originalfileName = StringUtil.substringBeforeLast(
						originalfileFullName, FILE_SEPTER_FLAG).split(
						FILE_JOIN_FLAG);
				String originalfileExtentName = StringUtil.substringAfterLast(
						originalfileFullName, FILE_SEPTER_FLAG);

				com.fangda.kass.model.config.IsoLangCode fromFile = QuestionConstants
						.getLangByCode(languageItem.getValue() + "-"
								+ countryItem.getValue());

				if (fromFile != null) {
					String locaFileFullPath = curSelLocalPath
							+ File.separator
							+ originalfileName[0]
							+ // The original filename:
								// questions_v2.properties(The substring before
								// V2)
							FILE_JOIN_FLAG
							+ fromFile.getCode().replace('-', '_')
							+ FILE_JOIN_FLAG + originalfileName[1] + // The
																		// original
																		// filename:
																		// questions_v2.properties(The
																		// substring
																		// after
																		// V2)
							FILE_SEPTER_FLAG + originalfileExtentName;
					questionnaireXDirectorField.setText(locaFileFullPath);
				} else {
					questionnaireXDirectorField.setText("");
					String localVersonFullMsg = "IsoLangCode is null!Please choose other Language and Country!";
					JOptionPane.showMessageDialog(container, "<HTML>"
							+ localVersonFullMsg + "</HTML>", "Warning",
							JOptionPane.INFORMATION_MESSAGE,
							UIHelper.getImage("warning.png"));

				}
			}
		}
	}

}
