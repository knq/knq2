package com.fangda.kass.ui.questionnaire;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.fangda.kass.KnqMain;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.service.workspace.WorkSpaceService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.davd.ExportModelConstant;
import com.fangda.kass.ui.davd.ExportModelDataDialog;
import com.fangda.kass.ui.davd.ViewForTable10Dialog;
import com.fangda.kass.ui.davd.ViewNKKIndependentPathsDialog;
import com.fangda.kass.ui.davd.ViewNKKKDistanceNumPathDialog;
import com.fangda.kass.ui.davd.ViewNKKKShortPathsTermDialog;
import com.fangda.kass.ui.davd.ViewNKKKinshipTermDialog;
import com.fangda.kass.ui.davd.ViewNKKPersonDialog;
import com.fangda.kass.ui.davd.ViewNKKPersonbyUnionMatrixSexDialog;
import com.fangda.kass.ui.davd.ViewNKKShortPathsDialog;
import com.fangda.kass.ui.davd.ViewNKKSimplePathsDialog;
import com.fangda.kass.ui.davd.ViewNKKUnionDialog;
import com.fangda.kass.ui.davd.ViewPersonbyUnionMatrixTypeDialog;
import com.fangda.kass.ui.interview.IvAction.FileChooser;
import com.fangda.kass.ui.questionnaire.base.ExportDialog;
import com.fangda.kass.ui.questionnaire.base.ImportDialog;
import com.fangda.kass.ui.questionnaire.base.QuestionnaireEditDialog;
import com.fangda.kass.ui.questionnaire.base.ReleaseDialog;
import com.fangda.kass.ui.questionnaire.local.NewLocalQDialog;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireLocalPanel;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * This class for the actions in the KNQ2
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QnAction {
	/**
	 * Questionnaire Edit
	 * 
	 * @author Fangfang Zhao
	 */
	public static class QuestionnaireEditAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -9186811490078920975L;

		@Override
		public void actionPerformed(ActionEvent e) {
			new QuestionnaireEditDialog();
		}
	}

	/**
	 * Load a questionnaire
	 * 
	 * @author Fangfang Zhao
	 */
	public static class QuestionnaireLoadAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1736921476174995625L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isProjectNullValid(km, 0);

			if (Constants.PROJECT_XML == null || Constants.PROJECT_XML == "") {
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(km, 0);
				} else {
					JFileChooser chooser = new JFileChooser(".");
					chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "properties");
					int result = chooser.showOpenDialog(km); // opendialog
					if (result == JFileChooser.APPROVE_OPTION) {
						if (km.qnMainPanel != null) {
							km.qnMainPanel.reload(chooser.getSelectedFile()
									.getAbsoluteFile().getAbsolutePath());
							km.setTitle(chooser.getSelectedFile()
									.getAbsoluteFile().getName());
						} else {
							km.qnMainPanel = new QnMainPanel(km);
							km.qnMainPanel.ifShowDetail = false;
							km.setLocation(100, 100);
							km.setMainPanel(km.qnMainPanel);
							km.qnMainPanel.reload(chooser.getSelectedFile()
									.getAbsoluteFile().getAbsolutePath());
							km.setTitle(chooser.getSelectedFile()
									.getAbsoluteFile().getName());

						}
					}
				}

			} else {

			}
		}
	}

	/**
	 * Questionnaire localize
	 * 
	 * @author Fangfang Zhao
	 */
	public static class QuestionnaireLocalizeAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8033203682115533739L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML != null
						&& Constants.PROJECT_XML != "") {
					kassService.isProjectNullValid(km, 0);
				} else {
					if (Constants.QUESTIONNAIRE_XML == null
							|| Constants.QUESTIONNAIRE_XML == ""
							|| Constants.QUESTIONNAIRE.getLanguage().equals(
									"en")) {
						questionnaireService.isQuestionnaireNullValid(km, 2);
					}

					else {
						try {
							if (km.qnMainPanel == null) {
								km.qnMainPanel = new QnMainPanel(km);
								km.qnMainPanel.ifShowDetail = false;
								km.setLocation(100, 100);
								km.setMainPanel(km.qnMainPanel);
							}
							km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML,
									"local");

						} catch (Exception e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, mxResources
									.get("questionnaireLocalizeError"),
									"Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		}
	}

	// ================2015-06-30 begin
	/**
	 * open a local questionnaire
	 */

	public static class OpenLocalQ extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4469191685265173145L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML != null
						&& Constants.PROJECT_XML != "") {
					kassService.isProjectNullValid(km, 0);
				} else {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					if (Constants.QUESTIONNAIRE_XML != null
							&& Constants.QUESTIONNAIRE_XML
									.contains(Constants.TMP_EXT)) {
						questionnaireService.isQuestionnaireNullValid(km, 0);
					}
					JFileChooser chooser = new JFileChooser(".");
					String defaultInterviewPath;
					defaultInterviewPath = WorkSpaceService.SetQueDefaultDir();
					chooser.setCurrentDirectory(new File(defaultInterviewPath));
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "properties");
					int i = chooser.showOpenDialog(km.getRootPane());
					if (i == JFileChooser.APPROVE_OPTION) {
						String path = chooser.getSelectedFile()
								.getAbsolutePath();
						// Constants.QUESTIONNAIRE_XML = path;
						com.fangda.kass.model.config.IsoLangCode isoLangCode = QuestionConstants
								.getLangByXml(path);
						if (!isoLangCode.getLanguageCode().equals("en")) {
							if (km.qnMainPanel != null) {
								System.out.println(km.qnMainPanel.rightPanel
										.getComponent(0));
								if (km.qnMainPanel.rightPanel.getViewport()
										.getComponentCount() > 0
										&& km.qnMainPanel.rightPanel
												.getViewport().getComponent(0) instanceof QuestionnaireLocalPanel) {
									XmlHelper.getInstance().alertExitQn(km,
											"revise");
								}
								try {
									km.qnMainPanel.reload(path, "local");
								} catch (Exception e1) {
									e1.printStackTrace();
									JOptionPane
											.showMessageDialog(
													null,
													mxResources
															.get("questionnaireLocalizeError"),
													"Error",
													JOptionPane.ERROR_MESSAGE);
								}
							} else {
								km.qnMainPanel = new QnMainPanel(km);
								km.qnMainPanel.ifShowDetail = false;
								km.setLocation(100, 100);
								km.setMainPanel(km.qnMainPanel);
								try {
									km.qnMainPanel.reload(path, "local");
								} catch (Exception e1) {
									e1.printStackTrace();
									JOptionPane
											.showMessageDialog(
													null,
													mxResources
															.get("questionnaireLocalizeError"),
													"Error",
													JOptionPane.ERROR_MESSAGE);
								}
							}
							km.setTitle(chooser.getSelectedFile()
									.getAbsoluteFile().getName());
						} else {
							JOptionPane.showMessageDialog(null, mxResources
									.get("questionnaireLocalizeError"),
									"Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		}
	}

	/**
	 * New a Local questionnaire
	 */

	public static class NewLocalQ extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -9078069137464979852L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML != null
						&& Constants.PROJECT_XML != "") {
					kassService.isProjectNullValid(km, 0);
				} else {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					if (Constants.QUESTIONNAIRE_XML != null
							&& Constants.QUESTIONNAIRE_XML
									.contains(Constants.TMP_EXT)) {
						questionnaireService.isQuestionnaireNullValid(km, 0);
					}
					try {
						new NewLocalQDialog().setMainPanel(km);
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
								mxResources.get("openQuestionnaireXMLError"),
								"Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
	}

	// ================2015-06-30 begin =========

	/**
	 * Close a Questionnaire localize
	 * 
	 * @author Fangfang Zhao
	 */
	public static class QuestionnaireCloseAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2368798816177198857L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			kassService.isProjectNullValid(km, 0);
			if (Constants.QUESTIONNAIRE_XML != null
					&& Constants.QUESTIONNAIRE_XML.contains(Constants.TMP_EXT)) {
				questionnaireService.isQuestionnaireNullValid(km, 0);
			}
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				questionnaireService.isQuestionnaireNullValid(km, 1);
			}

			if (Constants.PROJECT_XML == null || Constants.PROJECT_XML == "") {

				if (km.qnMainPanel != null) {
					km.qnMainPanel.removeAll();
					km.qnMainPanel.repaint();
					km.qnMainPanel = null;
				}

				if (Constants.QUESTIONNAIRE_STATUS == 11) {
					XmlHelper xmlhelper = new XmlHelper();
					xmlhelper.alertExitQn(km);
				}

				Constants.QUESTIONNAIRE = null;
				Constants.QUESTIONNAIRE_XML = null;
				Constants.QUESTIONNAIRE_STATUS = 0;
				km.setTitle(null);
			} else {

			}
		}
	}

	/**
	 * View and Edit an Questionnaire Information.
	 * 
	 * @author Fangfang Zhao
	 */
	public static class EditQuestionnaireAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1522941470571252252L;

		@Override
		public void actionPerformed(ActionEvent e) {

			KnqMain km = (KnqMain) e.getSource();
			if (Constants.QUESTIONNAIRE != null) {
				try {
					new QuestionnaireEditDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("openQuestionnaireXMLError"),
							"Error", JOptionPane.ERROR_MESSAGE);

				}
			} else {
				JOptionPane.showMessageDialog(null,
						mxResources.get("openQuestionnaireXMLError"), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	/**
	 * Questionnaire Revise
	 * 
	 * @author Shuang Ma
	 */
	public static class QEditAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6665228021425326984L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				questionnaireService.isQuestionnaireNullValid(km, 3);
			} else {
				KassService kassService = new KassService();
				kassService.isInterviewNullValid(km, 0);
				if (Constants.isInterviewCancel == 1) {
				} else {
					if (Constants.PROJECT_XML != null
							&& Constants.PROJECT_XML != "") {
						kassService.isProjectNullValid(km, 0);
					} else {
						if (km.qnMainPanel == null) {
							km.qnMainPanel = new QnMainPanel(km);
							km.qnMainPanel.ifShowDetail = false;
							km.setLocation(100, 100);
							km.setMainPanel(km.qnMainPanel);
						}
						km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML,
								"revise");
					}
				}
			}
		}
	}

	/**
	 * Questionnaire Template Revise
	 * 
	 * @author Shuang Ma
	 */
	public static class QTemplateAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 349627874336753357L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML != null
						&& Constants.PROJECT_XML != "") {
					kassService.isProjectNullValid(km, 0);
				} else {
					if (km.qnMainPanel == null) {
						km.qnMainPanel = new QnMainPanel(km);
						km.qnMainPanel.ifShowDetail = false;
						km.setLocation(100, 100);
						km.setMainPanel(km.qnMainPanel);
					}
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_TEMPLATE_XML,
							"temprevise");
				}
			}
		}
	}

	/**
	 * Questionnaire Import
	 * 
	 * @author Shuang Ma
	 */
	public static class QImportAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7475160343935553703L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			kassService.isInterviewNullValid(km, 0);
			if (Constants.isInterviewCancel == 1) {
			} else {
				if (Constants.PROJECT_XML != null
						&& Constants.PROJECT_XML != "") {
					kassService.isProjectNullValid(km, 0);
				} else {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					if (Constants.QUESTIONNAIRE_XML != null
							&& Constants.QUESTIONNAIRE_XML
									.contains(Constants.TMP_EXT)) {
						questionnaireService.isQuestionnaireNullValid(km, 0);
					}
					new ImportDialog().setMainPanel(km);
					;
				}
			}
		}
	}

	/**
	 * Questionnaire Export
	 * 
	 * @author Shuang Ma
	 */
	public static class QExportAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5029231151734237237L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				questionnaireService.isQuestionnaireNullValid(km, 3);
			} else {
				new ExportDialog();
			}
		}
	}

	public static class PreviewAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3820081493283354524L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				questionnaireService.isQuestionnaireNullValid(km, 1);
			} else {
				if (km.qnMainPanel != null) {
					km.qnMainPanel.ifShowDetail = false;
					// String tmpfile =
					// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
					// Constants.PROPERTIES_EXT);
					// Constants.QUESTIONNAIRE_XML = tmpfile;
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML);
				} else {
					km.qnMainPanel = new QnMainPanel(km);
					km.qnMainPanel.ifShowDetail = false;
					km.setLocation(100, 100);
					km.setMainPanel(km.qnMainPanel);
					// String tmpfile =
					// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
					// Constants.PROPERTIES_EXT);
					// Constants.QUESTIONNAIRE_XML = tmpfile;
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML);
				}
			}
		}
	}

	public static class PreviewDetailAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2730283351281492545L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == "") {
				questionnaireService.isQuestionnaireNullValid(km, 1);
			} else {
				if (km.qnMainPanel != null) {
					km.qnMainPanel.ifShowDetail = true;
					// String tmpfile =
					// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
					// Constants.PROPERTIES_EXT);
					// Constants.QUESTIONNAIRE_XML = tmpfile;
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML);
				} else {
					km.qnMainPanel = new QnMainPanel(km);
					km.qnMainPanel.ifShowDetail = false;
					km.setLocation(100, 100);
					km.setMainPanel(km.qnMainPanel);
					// String tmpfile =
					// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
					// Constants.PROPERTIES_EXT);
					// Constants.QUESTIONNAIRE_XML = tmpfile;
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML);
				}
			}
		}
	}

	public static class PreviewBilingualAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6560509046312668895L;

		@Override
		public void actionPerformed(ActionEvent e) {

			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == ""
					|| Constants.QUESTIONNAIRE.getLanguage().equals("en")) {
				questionnaireService.isQuestionnaireNullValid(km, 2);
			} else {
				if (km.qnMainPanel != null) {
					if (Constants.QUESTIONNAIRE.getLanguage().equals("en")) {
						JOptionPane.showMessageDialog(null,
								mxResources.get("nonBilingualQestionnaire"));
					} else {
						km.qnMainPanel.ifShowDetail = false;
						// String tmpfile =
						// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
						// Constants.PROPERTIES_EXT);
						// Constants.QUESTIONNAIRE_XML = tmpfile;
						km.qnMainPanel
								.reloadbilingual(Constants.QUESTIONNAIRE_XML);
					}
				} else {
					km.qnMainPanel = new QnMainPanel(km);
					km.qnMainPanel.ifShowDetail = false;
					km.setLocation(100, 100);
					km.setMainPanel(km.qnMainPanel);
					// String tmpfile =
					// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
					// Constants.PROPERTIES_EXT);
					// Constants.QUESTIONNAIRE_XML = tmpfile;
					km.qnMainPanel.reload(Constants.QUESTIONNAIRE_XML);
				}
			}
		}
	}

	/**
	 * Release Version
	 * 
	 * @author Shuang Ma
	 */

	public static class ReleaseAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5472876963831889193L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			QuestionnaireService questionnaireService = new QuestionnaireService();
			/*
			 * if(Constants.QUESTIONNAIRE_XML!=null&&Constants.QUESTIONNAIRE_XML.
			 * contains(Constants.TMP_EXT)) {
			 * questionnaireService.isQuestionnaireNullValid(km, 0); }
			 */
			if (Constants.QUESTIONNAIRE_XML == null
					|| Constants.QUESTIONNAIRE_XML == ""
					|| Constants.QUESTIONNAIRE.getStatus().equals("Formal")) {
				questionnaireService.isQuestionnaireNullValid(km, 4);
			} else {
				new ReleaseDialog().setMainPanel(km);
			}
		}
	}

	/**
	 * Questionnaire Find
	 * 
	 * @author Shuang Ma
	 */
	public static class FindAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5660509140927792448L;

		@Override
		public void actionPerformed(ActionEvent e) {
		}
	}

	/**
	 * 
	 */
	public static class AboutKNQAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -7800991513467104524L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JLabel lb1 = new JLabel();
			lb1.setText(mxResources.get("aboutKNQ2Note"));
			UIHelper.createMultiLabel(lb1, 300);
			JOptionPane.showConfirmDialog(null, lb1,
					mxResources.get("aboutKNQ2"), JOptionPane.PLAIN_MESSAGE, 0,
					UIHelper.getImage("logo.png"));
		}
	}

	public static class ZoomInAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 751475518900898624L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JLabel lb1 = new JLabel();
			lb1.setText(mxResources.get("zoomIn"));
			UIHelper.createMultiLabel(lb1, 300);
			KnqMain km = (KnqMain) e.getSource();
			km.ivMainPanel.graphPanel.graphComponent.zoomIn();
		}
	}

	public static class ZoomOutAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6081197184544561247L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JLabel lb1 = new JLabel();
			lb1.setText(mxResources.get("zoomOut"));
			UIHelper.createMultiLabel(lb1, 300);
			KnqMain km = (KnqMain) e.getSource();
			km.ivMainPanel.graphPanel.graphComponent.zoomOut();
		}
	}

	// table 1
	public static class ViewNKKPersonbyUnionMatrixTypeAction extends
			AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -457925431995256918L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewPersonbyUnionMatrixTypeDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKPersonbyUnionMatrixType"),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 2
	public static class ViewNKKPersonbyUnionMatrixSexAction extends
			AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6810547211049842028L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKPersonbyUnionMatrixSexDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKPersonbyUnionMatrixSex"),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 3
	public static class ViewNKKSimplePathsAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8168498640052506498L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKSimplePathsDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKSimplePaths"), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 4
	public static class ViewNKKIndependentPaths extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1386157795748202114L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKIndependentPathsDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKIndependentPaths"),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 5
	public static class ViewNKKShortPaths extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 907599892886942387L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKShortPathsDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKShortPaths"), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 6
	public static class ViewNKKKinshipTerm extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -613773028744517329L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKKinshipTermDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKKinshipTerm"), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 7
	public static class ViewNKKKShortPathsTerm extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6842866440645555862L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKKShortPathsTermDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKKShortPathsTerm"), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 8
	public static class ViewNKKKDistanceNumPath extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6734121694284955122L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKKDistanceNumPathDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKKDistanceNumPath"),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	// table 9
	public static class ViewPersonByVariable extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1096182876622713659L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			try {
				new ViewNKKPersonDialog().setMainPanel(km);
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null,
						mxResources.get("ViewNKKKPerson"), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// table 10
	public static class ViewUnionByVariable extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -356276341751835402L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewNKKUnionDialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewNKKKUnion"), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 * begin export
	 * 
	 * //export table 1 public static class ExportNKKPersonbyUnionMatrixType
	 * extends AbstractAction{
	 * 
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKPersonbyUnionMatrixTypeDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKPersonbyUnionMatrixType"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 2 public static class
	 *           ExportNKKPersonbyUnionMatrixSex extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKPersonbyUnionMatrixSexDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKPersonbyUnionMatrixSex"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 * 
	 *           //export table 3 public static class ExportNKKSimplePaths
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKSimplePathsDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKSimplePaths"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 4 public static class ExportNKKIndependentPaths
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKIndependentPathsDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKIndependentPaths"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 5 public static class ExportNKKShortPaths
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKShortPathsDialog().setMainPanel(km); } catch(Exception
	 *           e1) { e1.printStackTrace(); JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKShortPaths"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 6 public static class ExportNKKKinshipTerm
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKKinshipTermDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKKinshipTerm"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 7 public static class ExportNKKShortPathsTerm
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKKShortPathsTermDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKKShortPathsTerm"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           //export table 8 public static class ExportNKKDistanceNumPath
	 *           extends AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKKDistanceNumPathDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKKDistanceNumPath"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 *           exoprot end
	 */

	// export NKK
	public static class ExportNKKModelData extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8502131915930786744L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			try {
				String title = mxResources.get("ExportNKKallMatrices");
				new ExportModelDataDialog(title,
						ExportModelConstant.getExoprtDataType1())
						.setMainPanel(km);
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null,
						mxResources.get("ExportNKKallMatrices"), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// export Martices
	public static class ExportMarticesModelData extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1819572997979124092L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			try {
				String title = mxResources.get("ExportAllCaseMatrix");
				new ExportModelDataDialog(title,
						ExportModelConstant.getExoprtDataType2())
						.setMainPanel(km);
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null,
						mxResources.get("ExportAllCaseMatrix"), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// View table 10
	public static class ViewForTable10 extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1990788181936592073L;

		@Override
		public void actionPerformed(ActionEvent e) {
			KnqMain km = (KnqMain) e.getSource();
			KassService kassService = new KassService();
			if (Constants.KASS_XML == null || Constants.KASS_XML == "") {
				kassService.isInterviewNullValid(km, 1);
			} else {
				try {
					new ViewForTable10Dialog().setMainPanel(km);
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							mxResources.get("ViewAllPersonbyUnionMatrixType"),
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 * //export table 10 public static class ExportAllPersonbyUnionMatrixType
	 * extends AbstractAction{
	 * 
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKTable10Dialog().setMainPanel(km); } catch(Exception
	 *           e1) { e1.printStackTrace(); JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportAllPersonbyUnionMatrixType"), "Error",
	 *           JOptionPane.ERROR_MESSAGE); } } }
	 * 
	 * 
	 *           //export ExportNKKallMatricesforCurrentInterview public static
	 *           class ExportNKKallMatricesforCurrentInterview extends
	 *           AbstractAction{
	 * @Override public void actionPerformed(ActionEvent e) { KnqMain km =
	 *           (KnqMain) e.getSource(); try{ new
	 *           ExportNKKAllCurInterviewDialog().setMainPanel(km); }
	 *           catch(Exception e1) { e1.printStackTrace();
	 *           JOptionPane.showMessageDialog(null,
	 *           mxResources.get("ExportNKKallMatricesforCurrentInterview"),
	 *           "Error", JOptionPane.ERROR_MESSAGE); } } }
	 */
}
