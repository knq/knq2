package com.fangda.kass.ui.questionnaire.base;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.WordTools;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.export.ExportBialigualWord;
import com.fangda.kass.ui.questionnaire.export.ExportLocalWord;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.DocumentException;
import com.mxgraph.util.mxResources;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class for the dialog of exporting the Microsoft Word Document of the
 * questionnaire.
 * 
 * @author Fangfang Zhao, Shuang Ma, Jianguo Zhou, Jing Kong
 * 
 */
public class ExportDialog extends BaseDialog {

	private static final long serialVersionUID = 2119125622059371123L;
	JTextField questionnaireXDirectorField; // Source Directory
	JTextField questionnaireDDirectorField; // Destination Directory
	JCheckBox checkBox = new JCheckBox();
	JRadioButton english, localLanguge, localLangugeEnglish;
	JCheckBox savewithProperty;
	JComboBox jcomboBox;

	private String locallanguageFileName;
	private String locallanguageWithPropertyFileName;

	public void initDialog() {
		this.setTitle(mxResources.get("questionExport"));
		// this.setSize(n);
	}

	public JPanel createPanel() {
		questionnaireDDirectorField = new JTextField(50);
		questionnaireXDirectorField = new JTextField(50);
		// if(!"".equals(Constants.QUESTIONNAIRE_XML)){
		// String tmpfile =
		// Constants.QUESTIONNAIRE_XML.replace(Constants.TMP_EXT,
		// Constants.PROPERTIES_EXT);
		// Constants.QUESTIONNAIRE_XML = tmpfile;
		// questionnaireXDirectorField.setText(Constants.QUESTIONNAIRE_XML);
		// String tm= Constants.QUESTIONNAIRE_XML;
		// tm=tm.replace("properties", "doc");
		// locallanguageFileName = tm;
		// locallanguageWithPropertyFileName =
		// Constants.QUESTIONNAIRE_XML.replace(".properties",
		// "_properties.doc");
		// questionnaireDDirectorField.setText(tm);
		// }

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(400, 30));
		titlePanel1.add(new JLabel("<HTML><STRONG>" + mxResources.get("source")
				+ "</STRONG></HTML>"));

		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new BorderLayout(5, 5));

		contentPanel1.add(
				new JLabel(mxResources.get("questionnaireXMLFileExport")),
				BorderLayout.NORTH);

		JPanel TextPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		TextPanel1.add(questionnaireXDirectorField);
		contentPanel1.add(TextPanel1, BorderLayout.WEST);

		JPanel buttonPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QXDirectorButton = new JButton("...");
		QXDirectorButton.setPreferredSize(new Dimension(30, 20));
		buttonPanel1.add(QXDirectorButton);
		contentPanel1.add(buttonPanel1, BorderLayout.EAST);

		JPanel titlePanel2 = new JPanel(new GridLayout(1, 1));
		titlePanel2.setPreferredSize(new Dimension(400, 30));
		titlePanel2.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("destination") + "</STRONG></HTML>"));

		titlePanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel3 = new JPanel(new GridLayout(1, 2));
		contentPanel3.add(new JLabel(mxResources.get("version")),
				BorderLayout.WEST);

		JPanel checkBox = new JPanel();
		ButtonGroup jbtn = new ButtonGroup();
		english = new JRadioButton(mxResources.get("english"), true);
		localLanguge = new JRadioButton(mxResources.get("localLanguge"));
		localLangugeEnglish = new JRadioButton(
				mxResources.get("localLangugeEnglish"));
		savewithProperty = new JCheckBox(mxResources.get("savewithProperty"));

		com.fangda.kass.model.config.IsoLangCode isoLangCode = null;
		if (StringUtils.isNotBlank(Constants.QUESTIONNAIRE_XML)) {
			isoLangCode = QuestionConstants
					.getLangByXml(Constants.QUESTIONNAIRE_XML);
		}
		jcomboBox = new JComboBox();
		jcomboBox.addItem(mxResources.get("localLanguge"));
		jcomboBox.addItem(mxResources.get("localLangugeWithProperty"));
		if (isoLangCode != null && !isoLangCode.getLanguageCode().equals("en")) {
			jcomboBox.addItem(mxResources.get("localLangugeEnglish"));
		}

		jcomboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int index = ((JComboBox) e.getSource()).getSelectedIndex();// 获取到选中项的索引
				String Item = ((JComboBox) e.getSource()).getSelectedItem()
						.toString();// 获取到项
				if (index == 0) {
					questionnaireDDirectorField.setText(locallanguageFileName);
				} else if (index == 1) {
					questionnaireDDirectorField
							.setText(locallanguageWithPropertyFileName);
				}
				questionnaireDDirectorField.updateUI();

			}

		});

		checkBox.add(jcomboBox);

		// jbtn.add(english); jbtn.add(localLanguge);
		// jbtn.add(localLangugeEnglish);

		// checkBox.add(english);
		// checkBox.add(localLanguge);
		// checkBox.add(localLangugeEnglish);
		// checkBox.add(savewithProperty,1);
		contentPanel3.add(checkBox, BorderLayout.EAST, 1);
		JPanel checkBox1 = new JPanel();
		checkBox1.add(savewithProperty);
		// contentPanel3.add(checkBox1,BorderLayout.EAST,2);

		JPanel contentPanel2 = new JPanel(new BorderLayout(5, 5));

		contentPanel2.add(
				new JLabel(mxResources.get("questionnaireDocFileExport")),
				BorderLayout.NORTH);

		JPanel TextPanel2 = new JPanel();
		TextPanel2.add(questionnaireDDirectorField);
		contentPanel2.add(TextPanel2, BorderLayout.WEST);

		JPanel buttonPanel2 = new JPanel();
		JButton QDDirectorButton = new JButton("...");
		QDDirectorButton.setPreferredSize(new Dimension(30, 20));
		buttonPanel2.add(QDDirectorButton);
		contentPanel2.add(buttonPanel2, BorderLayout.EAST);

		// Destination Directory Select
		QDDirectorButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");

				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				// String tm= Constants.QUESTIONNAIRE_XML;
				// tm=tm.replace("properties", "doc");
				// chooser.setSelectedFile(new File(tm));
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					questionnaireDDirectorField.setText(chooser
							.getSelectedFile().getAbsolutePath());
				}
			}
		});

		QXDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("properties");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "Properties Files(*.properties)";
					}

				});
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				// chooser.setSelectedFile(new
				// File(Constants.QUESTIONNAIRE_XML));
				int i = chooser.showOpenDialog(getContentPane()); // opendialog
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					// reSetfilePath(chooser);
					questionnaireXDirectorField.setText(absPath);
					String path = StringUtil.substringBeforeLast(absPath,
							File.separator);
					String fileName = StringUtil.substringAfterLast(absPath,
							File.separator);
					locallanguageFileName = path + File.separator
							+ fileName.replace(".properties", ".doc");
					locallanguageWithPropertyFileName = path
							+ File.separator
							+ fileName
									.replace(".properties", "_properties.doc");
					// questionnaireDDirectorField.setText(locallanguageFileName);
					if (jcomboBox.getSelectedIndex() == 0) {
						questionnaireDDirectorField
								.setText(locallanguageFileName);
					} else {
						questionnaireDDirectorField
								.setText(locallanguageWithPropertyFileName);
					}
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		panelBorder.add(titlePanel2, c);
		panelBorder.add(contentPanel3, c);
		panelBorder.add(contentPanel2, c);
		return panelBorder;
	}

	/**
	 * Reset the directory path
	 */
	private void reSetfilePath(JFileChooser sFile) {
		if (locallanguageWithPropertyFileName != null) {

			String path = sFile.getSelectedFile().getAbsolutePath();
			String fileName = sFile.getSelectedFile().getName();

			String tem = locallanguageWithPropertyFileName;
			System.out.println("###" + tem);
			int pos = tem.lastIndexOf("\\");
			String tem2 = locallanguageWithPropertyFileName.substring(0,
					pos + 1) + fileName;
			locallanguageFileName = tem2.replace(".properties", ".doc");
			tem2 = tem2.replace(".", "_");
			tem2 += ".doc";
			locallanguageWithPropertyFileName = tem2;

			if (jcomboBox.getSelectedIndex() == 0) {
				questionnaireDDirectorField.setText(locallanguageFileName);
			} else {
				questionnaireDDirectorField
						.setText(locallanguageWithPropertyFileName);
			}

		}
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		String qDDirector = questionnaireDDirectorField.getText();
		String qXDirector = questionnaireXDirectorField.getText();

		if ("".equals(qXDirector)) {
			JOptionPane.showMessageDialog(container,
					mxResources.get("questionnaireXMLFileNullExport"),
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if (!english.isSelected() && !localLanguge.isSelected()
				&& !localLangugeEnglish.isSelected()) {
			JOptionPane.showMessageDialog(container,
					mxResources.get("questionnaireVersionNullExport"),
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if ("".equals(qDDirector)) {
			JOptionPane.showMessageDialog(container,
					mxResources.get("questionnaireDOCFileNullExport"),
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		Questionnaire q = WordTools.readproperty(questionnaireXDirectorField
				.getText());

		try {
			String wordPath = questionnaireDDirectorField.getText();
			if (!wordPath.contains(".doc")) {
				String tm = Constants.QUESTIONNAIRE_XML;
				tm = tm.replace("properties", "doc");
				int pos = tm.lastIndexOf("\\");
				String name = tm.substring(pos, tm.length());
				wordPath += name;
			}
			String itemname = jcomboBox.getSelectedItem().toString();
			if (itemname.equals("local Language With Properties")) {
				ExportLocalWord.writedoc(q, wordPath, true);
			} else if (itemname.equals(mxResources.get("english"))) {

			} else if (itemname.equals(mxResources.get("localLanguge"))) {
				ExportLocalWord.writedoc(q, wordPath, false);
			} else if (itemname.equals(mxResources.get("localLangugeEnglish"))) {
				ExportBialigualWord bialigualWord = new ExportBialigualWord();
				bialigualWord.writedoc(q, wordPath, true);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

}
