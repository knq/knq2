package com.fangda.kass.ui.questionnaire;

import javax.swing.BorderFactory;
import javax.swing.JToolBar;
import javax.swing.TransferHandler;

import com.fangda.kass.ui.common.DefaultActions.PrintAction;
import com.fangda.kass.ui.common.DefaultActions.SaveAction;
import com.fangda.kass.ui.questionnaire.QnAction.FindAction;
import com.fangda.kass.ui.questionnaire.QnAction.PreviewAction;
import com.fangda.kass.ui.questionnaire.QnAction.QExportAction;
import com.fangda.kass.ui.questionnaire.QnAction.QImportAction;
import com.fangda.kass.ui.questionnaire.QnAction.QuestionnaireLoadAction;
import com.fangda.kass.ui.questionnaire.QnAction.ReleaseAction;

/**
 * This class for the ToolBar in the KNQ2
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QnToolBar extends JToolBar {

	private static final long serialVersionUID = 8078285706196724114L;

	public QnToolBar(QnMainPanel mainPanel, int orientation) {
		super(orientation);
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createEmptyBorder(3, 3, 3, 3), getBorder()));
		setFloatable(false);

		// add(mainPanel.kmain.bind("New", new QuestionnaireEditAction(),
		// "/images/new.gif"));
		add(mainPanel.kmain.bind("Open", new QuestionnaireLoadAction(),
				"/images/open.gif"));
		add(mainPanel.kmain.bind("Save", new SaveAction(), "/images/save.gif"));

		addSeparator();

		add(mainPanel.kmain.bind("Print", new PrintAction(),
				"/images/print.gif"));

		addSeparator();

		add(mainPanel.kmain.bind("Cut", TransferHandler.getCutAction(),
				"/images/cut.gif"));
		add(mainPanel.kmain.bind("Copy", TransferHandler.getCopyAction(),
				"/images/copy.gif"));
		add(mainPanel.kmain.bind("Paste", TransferHandler.getPasteAction(),
				"/images/paste.gif"));

		addSeparator();

		// add(mainPanel.kmain.bind("Delete", mxGraphActions.getDeleteAction(),
		// "/images/delete.gif"));
		// addSeparator();
		// add(mainPanel.kmain.bind("Undo", new HistoryAction(true),
		// "/images/undo.gif"));
		// add(mainPanel.kmain.bind("Redo", new HistoryAction(false),
		// "/images/redo.gif"));
		// addSeparator();

		add(mainPanel.kmain.bind("Preview", new PreviewAction(),
				"/images/view.gif"));
		add(mainPanel.kmain.bind("Release", new ReleaseAction(),
				"/images/pagesetup.gif"));

		addSeparator();

		add(mainPanel.kmain.bind("QImport", new QImportAction(),
				"/images/import.png"));
		add(mainPanel.kmain.bind("QExport", new QExportAction(),
				"/images/expotDoc.png"));

		addSeparator();
		add(mainPanel.kmain.bind("Find", new FindAction(), "/images/zoom.gif"));
		addSeparator();
	}

}
