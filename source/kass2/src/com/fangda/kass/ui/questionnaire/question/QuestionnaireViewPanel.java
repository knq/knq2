package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import com.fangda.kass.KnqMain;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Revise;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the questionnaire view
 * 
 * @author Jianguo Zhou, Shuang Ma, Jing Kong
 * 
 */
public class QuestionnaireViewPanel extends BasePanel {

	private static final long serialVersionUID = 1L;
	private LinkedHashMap<String, Object> layoutMap;
	private LinkedHashMap<String, Object> groupMap;
	private QuestionnaireService service;
	private JTable InterviewLogTable;
	private JPanel revisePanel;
	public static int textArearows = 5;
	public static int textAreacolus = 36;

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		this.setName("questionViewPanel");
	}

	public QuestionnaireViewPanel(String name, KnqMain km) {
		super(name, km);
	}

	@Override
	public void initPanel() {
		service = new QuestionnaireService();
		layoutMap = new LinkedHashMap<String, Object>();
		groupMap = new LinkedHashMap<String, Object>();
	}

	@Override
	public JPanel createPanel() {
		// TODO Auto-generated method stub
		Questionnaire q = service.get();

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);

		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JLabel titleLabel1 = UIHelper.genLabel(q.getTitle(), true, 13);

		JLabel titleLabel2 = new JLabel("<HTML>Language : "
				+ Constants.QUESTIONNAIRE.getLanguage()
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version : " + q.getVersion()
				+ "</HTML>");
		JLabel titleLabel3 = new JLabel("<HTML>Country : "
				+ Constants.QUESTIONNAIRE.getCountry()
				+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CultureArea : "
				+ q.getCultureArea() + "</HTML>");

		JLabel titleLabel4 = new JLabel("<HTML>Status : "
				+ mxResources.get(q.getStatus()) + "</HTML>");
		int index = 0;
		UIHelper.addToBagpanel(panel, Color.white, titleLabel1, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel2, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel3, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel4, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);

		List<Revise> revises = q.getRevises();
		if (revises.size() > 0) {
			JLabel titleLabel5 = new JLabel("<HTML>"
					+ mxResources.get("revisehistory") + " :</HTML>");
			UIHelper.addToBagpanel(panel, Color.white, titleLabel5, c, 0,
					index++, 1, 1, GridBagConstraints.CENTER);
			createVersionPanel(q);
			UIHelper.addToBagpanel(panel, Color.white, this.revisePanel, c, 0,
					index++, 1, 1, GridBagConstraints.CENTER);
		}
		UIHelper.addToBagpanel(panel, Color.white,
				UIHelper.genLabel("Preface", true, 13), c, 0, index++, 1, 1,
				GridBagConstraints.WEST);
		JTextArea noteJta = UIHelper.genTextArea(q.getNote(), 700);
		UIHelper.addToBagpanel(panel, Color.white, noteJta, c, 0, index++, 1,
				1, GridBagConstraints.CENTER);

		List<Subsection> subsections = q.getSection().getSubsections();

		int sectionIndex = 1;

		for (Subsection ss : subsections) {
			String sectionTitle = "Section" + sectionIndex + " "
					+ ss.getTitle();

			sectionTitle += getSectionDetail(ss, km.qnMainPanel.ifShowDetail);

			JLabel jt = new JLabel(sectionTitle);
			jt.setName("section_" + ss.getSid());
			UIHelper.createMultiLabel(jt, Font.BOLD, 13, 700);
			UIHelper.addToBagpanel(panel, Color.white, jt, c, 0, 1 + index, 1,
					1, GridBagConstraints.WEST);
			index++;

			// Highlight “NKK Diagram Stop Rule"
			if (sectionIndex == 1) {
				StopRule stoprule = q.getDraw().getStopRule();
				String content = mxResources.get("stoppingRuleSetupTitle")
						+ " " + stoprule.getStep();

				JLabel jtStep = new JLabel(content);
				jtStep.setName("section_Step" + ss.getSid());
				UIHelper.createMultiLabel(jtStep, Font.BOLD, 13, 700);
				UIHelper.addToBagpanel(panel, Color.white, jtStep, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);

				index++;
			}

			JLabel subsectionJta = new JLabel(ss.getDetail());
			UIHelper.createMultiLabel(subsectionJta, 700);
			UIHelper.addToBagpanel(panel, Color.white, subsectionJta, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
			if (sectionIndex == 1) {
				index = this.addSection1Content(index, panel, c,
						subsections.get(0), q);
			}
			// subsection's question
			if (ss.getQuestions() != null) {
				int questionIndex = 1;
				for (Question question : ss.getQuestions()) {
					index = this.addQuestion(panel, c, question, index,
							questionIndex);
					questionIndex++;
				}
			}
			if (ss.getSubSections() != null) {
				int subsectionIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					String showTitle = "  " + sectionIndex + "."
							+ subsectionIndex + " ";
					showTitle = showTitle
							+ (subsection.getTitle() == null ? "" : subsection
									.getTitle());
					showTitle += getSectionDetail(ss,
							km.qnMainPanel.ifShowDetail);

					JLabel jt1 = new JLabel(showTitle);
					jt1.setName("section_" + subsection.getSid());
					UIHelper.createMultiLabel(jt1, Font.BOLD, 11, 700);
					UIHelper.addToBagpanel(panel, Color.white, jt1, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
					if (subsection.getDetail() != null) {
						JLabel snodeJta = new JLabel(subsection.getDetail());
						UIHelper.createMultiLabel(snodeJta, 700);
						UIHelper.addToBagpanel(panel, Color.white, snodeJta, c,
								0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;
					}

					int questionIndex = 1;
					for (Question question : subsection.getQuestions()) {
						index = this.addQuestion(panel, c, question, index,
								questionIndex);
						questionIndex++;
					}
					subsectionIndex++;
				}
			}
			sectionIndex++;
		}
		return panel;
	}

	/**
	 * Add the panel of the answer option
	 * 
	 * @param panel
	 * @param c
	 * @param question
	 * @param index
	 * @return
	 */
	public int addOption(JPanel panel, GridBagConstraints c, Question question,
			int index) {

		if ("A".equals(question.getType())) {// text
			if (question instanceof FollowupQuestion) {
				JTextField field = new JTextField(
						((FollowupQuestion) question).getLength());
				UIHelper.addToBagpanelFill(panel, Color.white, field, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST,
						GridBagConstraints.NONE);
			} else {
				JPanel jp = new JPanel();
				jp.setBackground(Color.white);
				UIHelper.addToBagpanel(panel, Color.white, jp, c, 0, 1 + index,
						1, 1, GridBagConstraints.WEST);
			}
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(textArearows, textAreacolus);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			UIHelper.addToBagpanelFill(panel, Color.white, notePane, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST,
					GridBagConstraints.BOTH);
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += getQuestionOptionDetail(option,
							km.qnMainPanel.ifShowDetail);
					if (("C".equals(question.getType()) && question instanceof FollowupQuestion)
							|| "Q".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;
					} else {
						optionTitle = "      □ " + optionTitle;
					}
					if (option.getNote() != null
							&& !"".equals(option.getNote())) {
						optionTitle += " (Note:" + option.getNote() + ")";
					}

					JLabel optionLabel = new JLabel(optionTitle);
					UIHelper.createMultiLabel(optionLabel, 650);
					UIHelper.addToBagpanel(panel, Color.white, optionLabel, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
			}
		}
		return index;
	}

	/**
	 * Add the panel of the answer option
	 * 
	 * @param panel
	 * @param question
	 */
	public void addPanelOption(JPanel panel, Question question) {
		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField(
					((FollowupQuestion) question).getLength());
			GridBagLayout bagLayout = new GridBagLayout();
			JPanel tieldPanel = new JPanel(bagLayout);

			tieldPanel.setBackground(Color.white);
			tieldPanel.add(field, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(0, 0, 0, 0), 0, 0));
			panel.add(tieldPanel);

		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(textArearows, textAreacolus);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(noteField);
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					optionTitle += getQuestionOptionDetail(option,
							km.qnMainPanel.ifShowDetail);
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;
					} else {
						optionTitle = "      □ " + optionTitle;
					}
					JLabel optionLabel = new JLabel(optionTitle);
					panel.add(optionLabel);
				}
			}
		}
	}

	/**
	 * Add the panel of the question
	 * 
	 * @param panel
	 * @param c
	 * @param question
	 * @param index
	 * @param questionIndex
	 * @return
	 */
	public int addQuestion(JPanel panel, GridBagConstraints c,
			Question question, int index, int questionIndex) {
		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		boolean otherCondition = false;
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())
					&& fquestion.getGroupName().length() > 0) {
				String parentQid = StringUtil.substringBeforeLast(
						question.getQid(), "_");
				String key = parentQid + fquestion.getGroupName();// groupMap
				JPanel jpanel = (JPanel) groupMap.get(key);
				if (jpanel == null) {
					jpanel = new JPanel();
					jpanel.setBackground(Color.white);
					jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));

					jpanel.setBorder(BorderFactory.createTitledBorder(fquestion
							.getGroupName()));
					groupMap.put(key, jpanel);
					UIHelper.addToBagpanel(panel, Color.white, jpanel, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
				if (StringUtil.isNotBlank(question.getDetail())) {
					String title = getFollowupQuestionDetail(question,
							km.qnMainPanel.ifShowDetail);
					JLabel optionLabel = UIHelper.genLabelwidth(title, false,
							9, 460);
					;
					jpanel.add(optionLabel);
				} else {
					String title = getFollowupQuestionNoDetail(question,
							km.qnMainPanel.ifShowDetail);
					JLabel optionLabel = UIHelper.genLabelwidth(title, false,
							9, 460);
					jpanel.add(optionLabel);
				}
				this.addPanelOption(jpanel, question);
			} else {
				if (isFollowup && fquestion.getLayout() != null
						&& fquestion.getLayout().length() > 0) {
					String parentQid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					String key = parentQid + fquestion.getLayout();
					JPanel jpanel = (JPanel) layoutMap.get(key);
					if (jpanel == null) {
						jpanel = new JPanel();
						jpanel.setBackground(Color.white);
						jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));
						layoutMap.put(key, jpanel);
						UIHelper.addToBagpanel(panel, Color.white, jpanel, c,
								0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;

						String title = getFollowupQuestionDetail(question,
								km.qnMainPanel.ifShowDetail);

						JLabel titleLabel = UIHelper.genLabel(title, false, 9);
						jpanel.add(titleLabel);
						this.addPanelOption(jpanel, question);
					} else {
						if (question.getDetail() != null
								&& !"".equals(question.getDetail())) {
							String title = getFollowupQuestionDetail(question,
									km.qnMainPanel.ifShowDetail);
							JLabel titleLabel = UIHelper.genLabel(title);
							jpanel.add(titleLabel);
						} else {
							String title = getFollowupQuestionNoDetail(
									question, km.qnMainPanel.ifShowDetail);
							JLabel titleLabel = UIHelper.genLabel(title, false,
									9);
							jpanel.add(titleLabel);
						}
						this.addPanelOption(jpanel, question);
					}
				} else {
					otherCondition = true;
				}
			}
		} else {
			otherCondition = true;
		}

		if (otherCondition) {
			if (question.getTitle() != null) {
				String title = "";
				if (isFollowup) {
					title = question.getSortNo() + "." + question.getTitle();
				} else {
					title = question.getSortNo() + "." + question.getTitle();
					if ("G".equals(question.getDirection())) {
						title = title + "(Help given)";
					} else if ("R".equals(question.getDirection())) {
						title = title + "(Help received)";
					}
				}
				title += getLeadQuestionNoDetail(question,
						km.qnMainPanel.ifShowDetail);
				JLabel jta = new JLabel(title);
				jta.setName("section_" + question.getQid());
				UIHelper.createMultiLabel(jta, Font.BOLD, 11, 700);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
			// if (question.getLead() != null) {
			// JLabel jta = new JLabel(" " + question.getLead());
			// UIHelper.createMultiLabel(jta, 680);
			//
			// UIHelper.addToBagpanel(panel, Color.white, jta, c, 0, 1 + index,
			// 1, 1, GridBagConstraints.WEST);
			// index++;
			// }
			if (question.getDetail() != null
					&& !question.getDetail().equals(question.getLead())) {
				String title = question.getDetail();
				if (isFollowup) {
					title += getFollowupQuestionDetail(question,
							km.qnMainPanel.ifShowDetail);
				}

				JLabel jta = new JLabel(" " + title);
				UIHelper.createMultiLabel(jta, 680);

				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}

			index = this.addOption(panel, c, question, index);
			if (question.getFollowupQuestions() != null) {
				int followupIndex = 1;
				for (FollowupQuestion fqestion : question
						.getFollowupQuestions()) {
					index = this.addQuestion(panel, c, fqestion, index,
							followupIndex);
					followupIndex++;
				}
			}
		}
		return index;
	}

	/**
	 * Generate the table of the revision history
	 * 
	 * @param q
	 * @return
	 */
	public JPanel createVersionPanel(Questionnaire q) {
		List<Revise> kilog = q.getRevises();
		String[] columnNames = { mxResources.get("No"),
				mxResources.get("questionniare.properties.version"),
				mxResources.get("reviseDate"), mxResources.get("reviser"),
				mxResources.get("note") };
		String[][] data = new String[kilog.size()][5];
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		int height = 20;
		for (int i = 0; i < kilog.size(); i++) {
			model.setValueAt(i + 1, i, 0);
			model.setValueAt(kilog.get(i).getVersion(), i, 1);
			model.setValueAt(kilog.get(i).getReviseDate(), i, 2);
			model.setValueAt(kilog.get(i).getReviser(), i, 3);
			model.setValueAt(kilog.get(i).getNote(), i, 4);
			height += 20;
		}

		InterviewLogTable = new JTable(model);
		InterviewLogTable.setAutoResizeMode(5);
		TableColumn column = null;
		for (int i = 0; i < 4; i++) {
			column = InterviewLogTable.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(10);
			} else {
				column.setPreferredWidth(50);
			}
		}

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		InterviewLogTable.getColumn(mxResources.get("No")).setCellRenderer(
				render);
		DefaultTableCellRenderer render2 = new DefaultTableCellRenderer();
		render2.setHorizontalAlignment(SwingConstants.CENTER);
		InterviewLogTable.getColumn(
				mxResources.get("questionniare.properties.version"))
				.setCellRenderer(render2);
		JTableHeader tableHeader = InterviewLogTable.getTableHeader();
		tableHeader.setReorderingAllowed(false);
		DefaultTableCellRenderer hr = (DefaultTableCellRenderer) tableHeader
				.getDefaultRenderer();
		hr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

		JScrollPane scrollpane = new JScrollPane(InterviewLogTable);
		InterviewLogTable.setFillsViewportHeight(true);
		scrollpane.setPreferredSize(new Dimension(100, height));

		revisePanel = new JPanel();

		revisePanel.setLayout(new BoxLayout(revisePanel, BoxLayout.Y_AXIS));
		revisePanel.add(scrollpane);
		return revisePanel;
	}

	/**
	 * Get the description of the Follow up question with detail
	 * 
	 * @param question
	 * @return
	 */
	public static String getFollowupQuestionDetail(Question question,
			Boolean ifShowDetail) {
		String title = "";
		// title = question.getDetail();
		title += getFollowupQuestionNoDetail(question, ifShowDetail);

		return title;
	}

	/**
	 * Get the description of the Follow up question without the detail
	 * 
	 * @param question
	 * @return
	 */
	public static String getFollowupQuestionNoDetail(Question question,
			Boolean ifShowDetail) {
		String title = "";

		if (ifShowDetail) {
			title += " (QID:" + question.getQid();
			if (question.getLabel() != null && question.getLabel().length() > 0) {
				title += ", Label:" + question.getLabel();
			}
			title += ", Type:"
					+ question.getType()
					+ "("
					+ QuestionConstants.getFollouQuestionTypeName(question
							.getType()) + ")";
			;
			if (((FollowupQuestion) question).getDataType() != null) {
				title += ", DataType:"
						+ ((FollowupQuestion) question).getDataType();
			}

			if (question.getDisable() == 2) {
				title += ", "
						+ QuestionConstants.getDisableName(question
								.getDisable().toString());
			}
			if (question.getIfGo() != null) {
				title += ", IfGo:" + question.getIfGo();
			}
			title += ")";
		}
		return title;
	}

	/**
	 * Get the description of the lead question
	 * 
	 * @param question
	 * @return
	 */
	public static String getLeadQuestionNoDetail(Question question,
			Boolean ifShowDetail) {
		String title = "";

		if (ifShowDetail) {
			title += " (QID:" + question.getQid();
			if (question.getLabel() != null && !question.getLabel().equals("")) {
				title += ", Label:" + question.getLabel();
			}
			if (question.getRelevance() != null) {
				title += ", "
						+ question.getRelevance()
						+ "("
						+ QuestionConstants.getRelevantName(question
								.getRelevance()) + ")";
			}
			title += ", SID:"
					+ question.getSubsection()
					+ ", Type:"
					+ question.getType()
					+ "("
					+ QuestionConstants.getLeadQuestionTypeString(question
							.getType()) + ")";
			if (question.getDisable() == 2) {
				title += ", "
						+ QuestionConstants.getDisableName(question
								.getDisable().toString());
			}

			if (question.getIfGo() != null) {
				title += ", IfGo:" + question.getIfGo();
			}

			title += ")";
		}
		return title;
	}

	/**
	 * Get the description of the answer option of the question option
	 * 
	 * @param question
	 * @return
	 */
	public static String getQuestionOptionDetail(Option option,
			Boolean ifShowDetail) {
		String optionTitle = "";

		if (ifShowDetail) {
			optionTitle += " (Value:" + option.getValue();
			if (option.getDefaultValue() != null
					&& "true".equals(option.getDefaultValue())) {
				optionTitle += ", defaultValue";
			}
			optionTitle += ", Oid:" + option.getOid();
			if (option.getLabel() != null && !option.getLabel().equals("")) {
				optionTitle += ", Label:" + option.getLabel();
			}
			if (option.getDisable() == 2) {
				optionTitle += ", "
						+ QuestionConstants.getDisableName(String
								.valueOf(option.getDisable()));
			}
			optionTitle += ")";
		}
		return optionTitle;
	}

	/**
	 * Get the section description
	 * 
	 * @param question
	 * @return
	 */
	public static String getSectionDetail(Subsection ss, Boolean ifShowDetail) {
		String sectionTitle = "";
		if (ifShowDetail) {
			sectionTitle += " (SID:" + ss.getSid() + ", SectionMode:"
					+ ss.getSectionMode();
			if (ss.getDisable() == 2) {
				sectionTitle += ", "
						+ QuestionConstants.getDisableName(String.valueOf(ss
								.getDisable()));
			}
			sectionTitle += ")";
		}
		return sectionTitle;
	}

	/**
	 * Section 1
	 */
	public int addSection1Content(int index, JPanel panel,
			GridBagConstraints c, Subsection ss, Questionnaire q) {
		StopRule stoprule = q.getDraw().getStopRule();
		String content = mxResources.get("stoppingRuleSetup") + " "
				+ stoprule.getStep() + " " + mxResources.get("stoppingStep");
		content += ". (" + mxResources.get("questionniare.properties.note")
				+ ": " + stoprule.getNote() + ")";

		JLabel section1Jta = new JLabel(content);
		UIHelper.createMultiLabel(section1Jta, 700);
		UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
				1 + index, 1, 1, GridBagConstraints.WEST);
		index++;

		ImageIcon s1NKK = UIHelper.getImage("stoppingRuleImg.png");
		UIHelper.addImgToBagpanel(panel, Color.white, s1NKK, c, 0, 1 + index,
				1, 1, GridBagConstraints.CENTER);
		index++;
		JLabel s1NKKNote = new JLabel(
				"Figure1: A simplified example of diagram.", JLabel.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, s1NKKNote, c, 0, 1 + index,
				1, 1, GridBagConstraints.CENTER);
		index++;

		section1Jta = new JLabel(mxResources.get("autoCompleteStep"));
		UIHelper.createMultiLabel(section1Jta, 700);
		UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
				1 + index, 1, 1, GridBagConstraints.WEST);
		index++;
		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		for (CompleteStep step : steps) {
			content = mxResources.get("stepLabel") + " " + step.getStepNo()
					+ ": " + step.getTitle();
			section1Jta = new JLabel(content);
			UIHelper.createMultiLabel(section1Jta, 700);
			UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
			List<CompleteSubStep> substeps = step.getSubSteps();
			for (CompleteSubStep subStep : substeps) {
				if (subStep.getKinshipTerm() != null) {
					content = "      ○ " + subStep.getKinshipTerm();
					if (km.qnMainPanel.ifShowDetail) {
						content += " ("
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ":"
								+ subStep.getSex() + ")";
					} else {
						content += " ("
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ")";
					}
				} else {
					content = "      ○ "
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex()));
					if (km.qnMainPanel.ifShowDetail) {
						content = "      ○ "
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex())) + ":"
								+ subStep.getSex();
					} else {
						content = "      ○ "
								+ QuestionConstants.getSexString(String
										.valueOf(subStep.getSex()));
					}
				}

				section1Jta = new JLabel(content);
				UIHelper.createMultiLabel(section1Jta, 650);
				UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
		}

		return index;
	}
}
