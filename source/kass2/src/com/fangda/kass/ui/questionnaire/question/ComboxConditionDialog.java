package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the question skip condition edit of the
 * questionnaire.
 * 
 * @author Fangfang Zhao
 * 
 */
public class ComboxConditionDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6392685431090728457L;
	QuestionnaireService service;
	JPanel panel1;
	JButton addBtn;
	JCheckBox labelSameCB;
	ComboItem[] optionItems;
	ComboItem[] questionItems;
	double oldHeight = 0;

	public void initDialog() {
		this.setTitle(mxResources.get("opSkipCondition"));
		service = new QuestionnaireService();
	}

	public ComboxConditionDialog(Map<String, Object> params) {
		super(params);
	}

	public JPanel createPanel() {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		panel1 = new JPanel();
		panel1.setBackground(Color.white);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		String qid = (String) params.get("qid");
		Question question = service.searchQuestion(qid);
		List<Option> options = question.getOptions();
		if (options != null) {

			if ("D".equals(question.getType())) {
				optionItems = new ComboItem[options.size() + 2];
				optionItems[0] = new ComboItem("", "");
				Option option = options.get(0);
				ComboItem item1 = new ComboItem(option.getDetail() + " : true",
						"true");
				optionItems[1] = item1;
				ComboItem item2 = new ComboItem(
						option.getDetail() + " : false", "false");
				optionItems[2] = item2;
			} else {
				optionItems = new ComboItem[options.size() + 1];
				optionItems[0] = new ComboItem("", "");
				for (int i = 0; i < options.size(); i++) {
					Option option = options.get(i);
					ComboItem item = new ComboItem(option.getDetail(),
							option.getValue());
					optionItems[i + 1] = item;
				}
			}
		}
		List<Question> allQuestions = service.searchAllQuestion();
		questionItems = new ComboItem[allQuestions.size() + 1];
		questionItems[0] = new ComboItem("", "");
		for (int i = 0; i < allQuestions.size(); i++) {
			Question q = allQuestions.get(i);
			ComboItem item = new ComboItem(q.getQid() + ":" + q.getLabel(),
					q.getQid());
			questionItems[i + 1] = item;
		}
		labelSameCB = new JCheckBox();
		labelSameCB.setText("labelSame");
		if (StringUtils.isBlank(question.getIfGo())) {
			JPanel jpanel = genRow(optionItems, "", "");
			panel1.add(jpanel);
		} else {
			String[] args1 = StringUtils.split(question.getIfGo(),
					Constants.SPLIT_OBJECT);
			int i = 0;
			if (StringUtils.equals(args1[0], "labelSame")) {
				labelSameCB.setSelected(true);
				i = 1;
			}
			for (int j = i; j < args1.length; j++) {
				String[] args2 = StringUtils.split(args1[j],
						Constants.SPLIT_PROPERTY);
				String value = args2[0];
				String qids = args2[1];
				JPanel jpanel = genRow(optionItems, value, qids);
				panel1.add(jpanel);
			}
		}
		panel1.addComponentListener(componentlistener);
		addBtn = new JButton("Add One Row");
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel jpanel = genRow(optionItems, "", "");
				panel1.add(jpanel);
				panel1.updateUI();
			}
		});
		panel.add(labelSameCB, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		panel.add(addBtn, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		panel.add(panel1, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub

		return true;
	}

	/**
	 * After open the Dialog
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		String qid = (String) params.get("qid");
		Question question = service.searchQuestion(qid);
		String ifGo = question.getIfGo();
		if (StringUtil.isNotBlank(ifGo) && question.getOptions() != null) {

		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		// labelSame
		String qid = (String) params.get("qid");
		Question question = service.searchQuestion(qid);
		Component[] c1s = panel1.getComponents();
		List<String> l = new ArrayList<String>();

		for (Component c1 : c1s) {
			JPanel p1 = (JPanel) c1;
			int size = p1.getComponentCount();
			StringBuffer sb = new StringBuffer();
			if (p1.getComponent(0) instanceof JComboBox) {
				JComboBox box = (JComboBox) p1.getComponent(0);
				ComboItem item = (ComboItem) box.getSelectedItem();
				sb.append(item.getValue());
			} else {
				JTextField box = (JTextField) p1.getComponent(0);
				sb.append(box.getText());
			}
			if (StringUtils.isBlank(sb.toString())) {
				continue;
			}
			JPanel p2 = (JPanel) p1.getComponent(size - 1);
			List<String> qids = getSelectQid(p2);
			sb.append(Constants.JOIN_PROPERTY).append(
					StringUtils.join(qids.toArray(), ","));
			l.add(sb.toString());
		}
		String result = StringUtils.join(l.toArray(), Constants.SPLIT_OBJECT);
		if (labelSameCB.isSelected()) {
			result = "labelSame" + Constants.SPLIT_OBJECT + result;
		}
		question.setIfGo(result);
		service.save(service.get());
		return new MessageModel(true, "Success");
	}

	private List<String> getSelectQid(JPanel p2) {
		Component[] c1s = p2.getComponents();
		List<String> qids = new ArrayList<String>();
		for (Component c1 : c1s) {
			JPanel p1 = (JPanel) c1;
			JLabel jlabel = (JLabel) p1.getComponent(0);
			qids.add(jlabel.getName());
		}
		return qids;
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	private JPanel genRow(ComboItem[] qitems, String jbox, String qids) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		JComponent box = null;
		if (qitems != null && qitems.length > 1) {
			box = new JComboBox(qitems);
			int index = getSelectIndex((JComboBox) box, jbox);
			((JComboBox) box).setSelectedIndex(index);
		} else {
			box = new JTextField();
			box.setPreferredSize(new Dimension(100, 30));
			((JTextField) box).setText(jbox);
		}

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("opSkipCondition") + ":"), c, 0, 0,
				3, 1, GridBagConstraints.EAST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("answerEqual") + ":"), c, 0, 1, 1,
				1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, box, c, 1, 1, 2, 1,
				GridBagConstraints.EAST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, 1, 1, 1,
				GridBagConstraints.EAST);

		// The second line
		final JComboBox qbox = new JComboBox(questionItems);
		// qbox.setPreferredSize(new Dimension(500, 30));
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("  "), c, 0, 2, 1,
				1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, qbox, c, 1, 2, 6, 1,
				GridBagConstraints.EAST);
		JButton addBtn = BtnHelper.createAddButton("Add DNA Question");
		UIHelper.addToBagpanel(panel, Color.gray, addBtn, c, 7, 2, 3, 1,
				GridBagConstraints.EAST);

		// The third line
		final JPanel ypanel = new JPanel();
		ypanel.setBackground(Color.white);
		ypanel.setLayout(new BoxLayout(ypanel, BoxLayout.Y_AXIS));
		ypanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("skipQuestion") + ":"), c, 0, 3, 1,
				1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("  "), c, 1, 3, 1,
				1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, ypanel, c, 2, 3, 1, 1,
				GridBagConstraints.EAST);
		if (StringUtils.isNotBlank(qids)) {
			String[] args = StringUtils.split(qids, ",");
			for (String arg : args) {
				Question q = service.searchQuestion(arg);
				ComboItem item = new ComboItem(q.getQid() + ":" + q.getLabel(),
						q.getQid());
				ypanel.add(genPanelRow(item.getValue(), item.getName()));
			}
		}
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				panel1.updateUI();
			}
		});
		addBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ComboItem item = (ComboItem) qbox.getSelectedItem();
				if (StringUtils.isNotBlank(item.getValue())) {
					ypanel.add(genPanelRow(item.getValue(), item.getName()));
					ypanel.updateUI();
				}
			}
		});

		return panel;
	}

	private JPanel genPanelRow(String qid, String qtext) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		JLabel label = new JLabel(qtext);
		label.setName(qid);
		UIHelper.addToBagpanel(panel, Color.gray, label, c, 0, 0, 3, 1,
				GridBagConstraints.WEST);
		JButton deleteBtn = BtnHelper.createDeleteButton("");
		UIHelper.addToBagpanel(panel, Color.gray, deleteBtn, c, 4, 0, 1, 1,
				GridBagConstraints.WEST);

		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteRow((JButton) e.getSource());
				panel1.updateUI();
			}
		});
		return panel;
	}

	private int getSelectIndex(JComboBox jc, String value) {
		int size = jc.getModel().getSize();
		for (int i = 0; i < size; i++) {
			ComboItem item = (ComboItem) jc.getModel().getElementAt(i);
			if (item.getValue().equals(value)) {
				return i;
			}
		}
		return 0;
	}

	private void deleteRow(JButton deleteBtn) {
		JPanel jpanel = (JPanel) deleteBtn.getParent();
		deleteBtn.getParent().getParent().remove(jpanel);
	}

	public void windowResize(ComponentEvent e) {
		if (e.getSource() == panel1) {
			Dimension currSize = (Dimension) panel1.getSize().clone();
			if (oldHeight == 0) {
				oldHeight = currSize.getHeight();
			} else {
				double change = currSize.getHeight() - oldHeight;
				Dimension dialogSize = (Dimension) this.getSize().clone();
				dialogSize.height = (int) (dialogSize.height + change);
				this.setSize(dialogSize);
				oldHeight = currSize.getHeight();
			}
		}
	}
}
