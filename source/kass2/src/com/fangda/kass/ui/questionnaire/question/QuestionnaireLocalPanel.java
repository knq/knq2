package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.JTextComponent;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.config.Config;
import com.fangda.kass.model.config.Field;
import com.fangda.kass.model.config.IsoLangCode;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.lowagie.text.Font;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the local language questionnaire edit, also the
 * bilingual language edit (English and Local language)
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QuestionnaireLocalPanel extends BasePanel {

	private static final long serialVersionUID = 1L;
	private QuestionnaireService service;
	Questionnaire q = null;
	int width = 0;
	int defaultCols = 120;
	private List<Component> comps;
	private Properties messageProp;
	private Config config;
	private String type;

	public QuestionnaireLocalPanel(String name, KnqMain km) {
		super(name, km);
	}

	@Override
	public void initPanel() {
		service = new QuestionnaireService();
		messageProp = this.loadProperties();
		config = this.loadConfig();
		q = service.get();
		width = km.qnMainPanel.rightPanel.getWidth();
	}

	@Override
	public JPanel createPanel() {
		List<Component> components = new ArrayList<Component>();
		JPanel panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		createHeader(panel, false, components);
		// questionnaire
		createQuestionnaire(panel, components);
		// draw
		createDrawPanel(panel, components);
		// section
		createSectionPanel(panel, components);
		this.setComps(components);
		return panel;
	}

	/**
	 * Create the Head of the page
	 * 
	 * @param panel
	 * @param flag
	 *            false = display all, true = only display the UnLocalized
	 *            content
	 */
	public void createHeader(JPanel panel, boolean flag,
			final List<Component> components) {
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		// Heading
		JLabel jta = new JLabel(mxResources.get("qLocalizeTitle"));
		UIHelper.createMultiLabel(jta, Font.BOLD, 16, 400);
		JPanel titlePanel = new JPanel(bagLayout);
		UIHelper.addToBagpanel(titlePanel, Color.white, jta, c, 0, 0, 1, 1,
				GridBagConstraints.CENTER);
		panel.add(titlePanel);

		// Buttons
		String[] checkLocalItem = null;
		if (flag) {
			checkLocalItem = new String[] { "UnLocalized", "All" };
		} else {
			checkLocalItem = new String[] { "All", "UnLocalized" };
		}
		JComboBox checkLocalBox = new JComboBox(checkLocalItem);
		final JButton localPreviewBtn = new JButton(
				mxResources.get("editLocalEngButton"));
		JPanel checkPanel = new JPanel(bagLayout);
		checkPanel.setBackground(Color.white);
		UIHelper.addToBagpanel(checkPanel, Color.white, checkLocalBox, c, 0, 0,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(checkPanel, Color.white, localPreviewBtn, c, 2,
				0, 1, 1, GridBagConstraints.CENTER);
		panel.add(checkPanel);
		final JPanel a = panel;
		localPreviewBtn.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if (localPreviewBtn.getText().equals(
						mxResources.get("editLocalEngButton"))) {
					localPreviewBtn.setText(mxResources
							.get("editLocalOnlyButton"));
					setEnglishEdit(a, true, components);
				} else {
					localPreviewBtn.setText(mxResources
							.get("editLocalEngButton"));
					setEnglishEdit(a, false, components);
				}

			}
		});

		checkLocalBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if ("All".equals(e.getItem())) {
						hideUnlocal(a, false, components);
					} else {
						hideUnlocal(a, true, components);
					}
				}
			}
		});

		// Tip for localize
		JPanel tipPanel = new JPanel(bagLayout);
		JLabel tipLocal = new JLabel(mxResources.get("ckeckLocalInfo"));
		UIHelper.createMultiLabel(tipLocal, Font.BOLD, 13, 660);
		tipPanel.setBackground(Color.white);
		UIHelper.addToBagpanel(tipPanel, Color.white, tipLocal, c, 0, 0, 1, 1,
				GridBagConstraints.WEST);
		panel.add(tipPanel);
	}

	public void createQuestionnaire(JPanel panel, List<Component> components) {
		// Heading
		createTransAreaPanel(panel, q.getEn_title(), q.getTitle(), 2,
				defaultCols, "q-title", components);
		// Language
		createTransPanel(panel, "Language",
				messageProp.getProperty("language"), 300, "q-language",
				components);
		String lanCode = q.getLanguage() + "-" + q.getCountry();
		IsoLangCode langCode = QuestionConstants.getLangByCode(lanCode);
		String lanName = "";
		if (langCode != null) {
			lanName = langCode.getLocalLanguage();
		}
		createTransPanel(panel, q.getLanguageName(), lanName, 300, "q-lan",
				components);

		// Country
		createTransPanel(panel, "Country", messageProp.getProperty("country"),
				300, "q-country", components);
		String countryName = "";
		if (langCode != null) {
			countryName = langCode.getLocalCountry();
		}
		createTransPanel(panel, q.getCountryName(), countryName, 300,
				"q-country", components);

		// Version
		createTransPanel(panel, "Version", messageProp.getProperty("version"),
				300, "q-version", components);

		// Status
		String enStatus = q.getStatus();
		String localStatus = "";
		localStatus = getStatusName(q.getStatus());

		createTransPanel(panel, enStatus, localStatus, 300, "q-status",
				components);

		// Preface
		createTransPanel(panel, "Preface", messageProp.getProperty("preface"),
				300, "q-preface", components);

		// Section
		createTransPanel(panel, "Section", messageProp.getProperty("section"),
				300, "q-section", components);

		// Note
		createTransAreaPanel(panel, q.getEn_note(), q.getNote(), 10,
				defaultCols, "q-note", components);

		panel.add(Box.createVerticalStrut(10));
	}

	public void createDrawPanel(JPanel panel, List<Component> components) {
		createTransAreaPanel(panel, q.getDraw().getStopRule().getEn_note(), q
				.getDraw().getStopRule().getNote(), 5, defaultCols, "d-note",
				components);
		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		for (CompleteStep step : steps) {
			createTransPanel(panel, step.getEn_title(), step.getTitle(), 300,
					"d-title-" + step.getStepNo(), components);
			for (CompleteSubStep subStep : step.getSubSteps()) {
				if (StringUtils.isNotBlank(subStep.getEn_kinshipTerm())) {
					createTransPanel(
							panel,
							subStep.getEn_kinshipTerm(),
							subStep.getKinshipTerm(),
							300,
							"d-kinshipTerm-" + step.getStepNo() + "-"
									+ subStep.getStepNo(), components);
				}
			}
		}
	}

	/**
	 * Section and Question
	 * 
	 * @param panel
	 */
	public void createSectionPanel(JPanel panel, List<Component> components) {
		List<Subsection> subsections = q.getSection().getSubsections();
		for (Subsection subsection : subsections) {
			if (StringUtils.isNotBlank(subsection.getEn_title())) {
				createTransPanel(panel, subsection.getEn_title(),
						subsection.getTitle(), 300, "subsection-title-"
								+ subsection.getSid(), components);
				panel.add(Box.createVerticalStrut(10));
			}
			if (StringUtils.isNotBlank(subsection.getEn_detail())) {
				createTransAreaPanel(panel, subsection.getEn_detail(),
						subsection.getDetail(), 3, defaultCols,
						"subsection-detail-" + subsection.getSid(), components);
				panel.add(Box.createVerticalStrut(10));
			}
			createQuestionPanel(panel, subsection.getQuestions(), components);
			List<Subsection> ss = subsection.getSubSections();
			for (Subsection s : ss) {
				if (StringUtils.isNotBlank(s.getEn_title())) {
					createTransPanel(panel, s.getEn_title(), s.getTitle(), 300,
							"subsection-title-" + s.getSid(), components);
					panel.add(Box.createVerticalStrut(10));
				}
				if (StringUtils.isNotBlank(s.getEn_detail())) {
					createTransAreaPanel(panel, s.getEn_detail(),
							s.getDetail(), 3, defaultCols, "subsection-detail-"
									+ s.getSid(), components);
					panel.add(Box.createVerticalStrut(10));
				}
				createQuestionPanel(panel, s.getQuestions(), components);
			}
		}
	}

	public void createQuestionPanel(JPanel panel, List<Question> questions,
			List<Component> components) {
		if (questions == null || questions.size() <= 0)
			return;
		for (Question question : questions) {
			if (StringUtils.isNotBlank(question.getEn_title())) {
				createTransPanel(panel, question.getEn_title(),
						question.getTitle(), 300,
						"question-title-" + question.getQid(), components);
				panel.add(Box.createVerticalStrut(10));
			}
			if (StringUtils.isNotBlank(question.getEn_lead())) {
				createTransAreaPanel(panel, question.getEn_lead(),
						question.getLead(), 2, defaultCols, "question-lead-"
								+ question.getQid(), components);
				panel.add(Box.createVerticalStrut(10));
			}
			if (StringUtils.isNotBlank(question.getEn_detail())) {
				createTransAreaPanel(panel, question.getEn_detail(),
						question.getDetail(), 3, defaultCols,
						"question-detail-" + question.getQid(), components);
				panel.add(Box.createVerticalStrut(10));
			}
			createOptionPanel(panel, question.getOptions(), components);
			if (question.getFollowupQuestions() != null
					&& question.getFollowupQuestions().size() > 0) {
				for (FollowupQuestion fquestion : question
						.getFollowupQuestions()) {
					if (StringUtils.isNotBlank(fquestion.getEn_title())) {
						createTransPanel(panel, fquestion.getEn_title(),
								fquestion.getTitle(), 300, "question-title-"
										+ fquestion.getQid(), components);
					}
					if (StringUtils.isNotBlank(fquestion.getEn_groupName())) {
						createTransPanel(panel, fquestion.getEn_groupName(),
								fquestion.getGroupName(), 300,
								"question-groupName-" + fquestion.getQid(),
								components);
					}
					if (StringUtils.isNotBlank(fquestion.getEn_detail())) {
						createTransAreaPanel(panel, fquestion.getEn_detail(),
								fquestion.getDetail(), 3, defaultCols,
								"question-detail-" + fquestion.getQid(),
								components);
						panel.add(Box.createVerticalStrut(10));
					}
					createOptionPanel(panel, fquestion.getOptions(), components);
				}
			}
		}
	}

	public void createOptionPanel(JPanel panel, List<Option> options,
			List<Component> components) {
		if (options == null || options.size() <= 0)
			return;
		for (Option option : options) {
			createTransAreaPanel(panel,
					StringUtils.trimToEmpty(option.getEn_note()),
					option.getNote(), 2, defaultCols,
					"option-note-" + option.getOid(), components);
			panel.add(Box.createVerticalStrut(10));
			createTransAreaPanel(panel,
					StringUtils.trimToEmpty(option.getEn_detail()),
					option.getDetail(), 2, defaultCols, "option-detail-"
							+ option.getOid(), components);
			panel.add(Box.createVerticalStrut(10));
		}
	}

	/**
	 * Text Field of the Translation in local language
	 * 
	 * @param label
	 * @param value
	 * @param length
	 * @return
	 */
	private void createTransPanel(JPanel panel, String label, String value,
			int length, String qid, List<Component> components) {
		JPanel qtitlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		qtitlePanel.setBackground(Color.white);

		JTextField qtitleEnText = new JTextField();
		qtitleEnText.setBackground(Color.LIGHT_GRAY);
		qtitleEnText.setPreferredSize(new Dimension(length, 30));
		qtitleEnText.setText(label);
		qtitleEnText.setEditable(false);
		qtitleEnText.setName(qid + "-en");
		qtitlePanel.add(qtitleEnText);
		qtitleEnText.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				handleFocus(e);
			}
		});

		JLabel mlabel = new JLabel(":");
		qtitlePanel.add(mlabel);

		JTextField qtitleText = new JTextField();
		qtitleText.setPreferredSize(new Dimension(length, 30));
		qtitleText.setText(value);
		qtitleText.setName(qid + "-local");
		qtitlePanel.add(qtitleText);
		qtitleText.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
			}

			@Override
			public void focusLost(FocusEvent e) {
				handleFocus(e);
			}
		});
		panel.add(qtitlePanel);
		components.add(qtitleText);
	}

	/**
	 * Text Area of the Translation in local language
	 * 
	 * @param panel
	 * @param label
	 * @param value
	 * @param length
	 */
	private void createTransAreaPanel(JPanel panel, String label, String value,
			int rows, int cols, String qid, List<Component> components) {
		if (StringUtils.isBlank(label) && StringUtils.isBlank(value)) {
			return;
		}
		if (label.length() <= 50) {
			createTransPanel(panel, label, value, 300, qid, components);
		} else {
			JPanel panel1 = new JPanel();
			panel1.setBackground(Color.white);
			panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
			panel1.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

			JTextArea labelField = new JTextArea(rows, cols);
			labelField.setLineWrap(true);
			labelField.setBackground(Color.LIGHT_GRAY);
			labelField.setEditable(false);
			labelField.setWrapStyleWord(true);
			JScrollPane labelPane = new JScrollPane(labelField);
			labelPane
					.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			labelField.setText(label);
			labelField.setName(qid + "-en");
			labelField.addFocusListener(new FocusListener() {
				@Override
				public void focusGained(FocusEvent e) {
				}

				@Override
				public void focusLost(FocusEvent e) {
					handleFocus(e);
				}
			});
			panel1.add(labelPane);

			JTextArea noteField = new JTextArea(rows, cols);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			noteField.setText(value);
			noteField.setName(qid + "-local");
			noteField.addFocusListener(new FocusListener() {
				@Override
				public void focusGained(FocusEvent e) {
				}

				@Override
				public void focusLost(FocusEvent e) {
					handleFocus(e);
				}
			});
			panel1.add(notePane);
			panel.add(panel1);
			components.add(noteField);
		}
	}

	private void handleFocus(FocusEvent e) {
		Component component = e.getComponent();
		String value = "";
		String name = component.getName();
		if (component instanceof JTextField) {
			value = ((JTextField) component).getText();
		} else if (component instanceof JTextArea) {
			value = ((JTextArea) component).getText();
		}
		if (name.startsWith("option-")) {
			name = name.replace("option-", "");
			String[] args = name.split("-");
			String field = args[0];
			String oid = args[1];
			String type = args[2];
			this.updateOption(field, type, oid, value);
		} else if (name.startsWith("question-")) {
			name = name.replace("question-", "");
			String[] args = name.split("-");
			String field = args[0];
			String qid = args[1];
			String type = args[2];
			this.updateQuestion(field, type, qid, value);
		} else if (name.startsWith("subsection-")) {
			name = name.replace("subsection-", "");
			String[] args = name.split("-");
			String field = args[0];
			String sid = args[1];
			String type = args[2];
			updateSubsection(field, sid, type, value);
		} else if (name.startsWith("d-")) {
			name = name.replace("d-", "");
			String[] args = name.split("-");
			String field = args[0];
			String step = args[1];
			String subStep = "";
			String type = "";
			if (args.length == 4) {
				subStep = args[2];
				type = args[3];
			} else if (args.length == 3) {
				type = args[2];
			}
			this.updateStep(field, type, step, subStep, value);
		} else if (name.startsWith("q-")) {
			name = name.replace("q-", "");
			String[] args = name.split("-");
			String field = args[0];
			String type = args[1];
			updateQuestionnaire(field, type, value);
		}
		service.save(service.get());
	}

	private void updateQuestionnaire(String field, String type, String value) {
		Questionnaire q = service.get();
		if ("title".equals(field)) {
			if ("en".equals(type)) {
				q.setEn_title(value);
			} else {
				q.setTitle(value);
			}
		} else if ("note".equals(field)) {
			if ("en".equals(type)) {
				q.setEn_note(value);
			} else {
				q.setNote(value);
			}
		} else if ("cultureArea".equals(field)) {
			if ("en".equals(type)) {
				q.setEn_cultureArea(value);
			} else {
				q.setCultureArea(value);
			}
		} else if ("language".equals(field)) {

		}
	}

	private void updateStep(String field, String type, String step,
			String subStep, String value) {
		Questionnaire q = service.get();
		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		if ("note".equals(field)) {
			if ("en".equals(type)) {
				q.getDraw().getStopRule().setEn_note(value);
			} else {
				q.getDraw().getStopRule().setNote(value);
			}
		} else if ("title".equals(field)) {
			for (CompleteStep sp : steps) {
				if (StringUtils.equals(sp.getStepNo().toString(), step)) {
					if ("en".equals(type)) {
						sp.setEn_title(value);
					} else {
						sp.setTitle(value);
					}
				}
			}
		} else if ("kinshipTerm".equals(field)) {
			for (CompleteStep sp : steps) {
				if (StringUtils.equals(sp.getStepNo().toString(), step)) {
					for (CompleteSubStep substep : sp.getSubSteps()) {
						if (StringUtils.equals(substep.getStepNo().toString(),
								subStep)) {
							if ("en".equals(type)) {
								substep.setEn_kinshipTerm(value);
							} else {
								substep.setKinshipTerm(value);
							}
						}
					}
				}
			}
		}
	}

	private void updateOption(String field, String type, String oid,
			String value) {
		String qid = StringUtils.substringBeforeLast(oid, "_");
		Question question = service.searchQuestion(qid);
		if (question == null) {
			qid = StringUtils.substringBeforeLast(qid, "_");
			question = service.searchQuestion(qid);
		}
		if (question == null) {
			return;
		}
		List<Option> options = question.getOptions();
		for (Option option : options) {
			if (StringUtils.equals(oid, option.getOid())) {
				if ("note".equals(field)) {
					if ("en".equals(type)) {
						option.setEn_note(value);
					} else {
						option.setNote(value);
					}
				} else if ("detail".equals(field)) {
					if ("en".equals(type)) {
						option.setEn_detail(value);
					} else {
						option.setDetail(value);
					}
				}
			}
		}
	}

	private void updateQuestion(String field, String type, String qid,
			String value) {
		Question question = service.searchQuestion(qid);
		if ("title".equals(field)) {
			if ("en".equals(type)) {
				question.setEn_title(value);
			} else {
				question.setTitle(value);
			}
		} else if ("detail".equals(field)) {
			if ("en".equals(type)) {
				question.setEn_detail(value);
			} else {
				question.setDetail(value);
			}
		} else {
			if (question instanceof FollowupQuestion) {
				FollowupQuestion fquestion = (FollowupQuestion) question;
				if ("groupName".equals(field)) {
					if ("en".equals(type)) {
						fquestion.setEn_groupName(value);
					} else {
						fquestion.setGroupName(value);
					}
				}
			} else {
				if ("lead".equals(field)) {
					if ("en".equals(type)) {
						question.setEn_lead(value);
					} else {
						question.setLead(value);
					}
				}
			}
		}
	}

	private void updateSubsection(String field, String sid, String type,
			String value) {
		Subsection subsection = service.searchSubsection(sid);
		if ("title".equals(field)) {
			if ("en".equals(type)) {
				subsection.setEn_title(value);
			} else {
				subsection.setTitle(value);
			}
		} else if ("detail".equals(field)) {
			if ("en".equals(type)) {
				subsection.setEn_detail(value);
			} else {
				subsection.setDetail(value);
			}
		}
	}

	/**
	 * Display or hide the Localized content
	 * 
	 * @param panel
	 * @param flag
	 * @param components
	 */
	private void hideUnlocal(JPanel panel, boolean flag,
			List<Component> components) {

		panel.removeAll();
		this.createHeader(panel, flag, components);
		for (Component component : components) {
			String value = "";
			Component parent = null;
			if (component instanceof JTextField) {
				value = ((JTextField) component).getText();
				parent = component.getParent();
			} else if (component instanceof JTextArea) {
				value = ((JTextArea) component).getText();
				parent = component.getParent().getParent().getParent();
			}
			if (flag) {
				if (StringUtils.isNotBlank(value)) {
				} else {
					panel.add(parent);
				}
			} else {
				panel.add(parent);
			}
		}
		panel.updateUI();
	}

	/**
	 * Set the English content to editable or not editable
	 * 
	 * @param panel
	 * @param flag
	 *            true = editable, false = not editable
	 * @param components
	 */
	private void setEnglishEdit(JPanel panel, boolean flag,
			List<Component> components) {

		for (Component component : components) {
			JTextComponent c = null;
			if (component instanceof JTextField) {
				JPanel parent = (JPanel) component.getParent();
				c = (JTextComponent) parent.getComponent(0);
			} else if (component instanceof JTextArea) {
				JPanel parent = (JPanel) component.getParent().getParent()
						.getParent();
				JScrollPane z = (JScrollPane) parent.getComponent(0);
				JViewport a = (JViewport) z.getComponent(0);
				c = (JTextComponent) a.getComponent(0);
			}
			if (flag) {
				c.setEditable(true);
				c.setBackground(Color.WHITE);
			} else {
				c.setEditable(false);
				c.setBackground(Color.LIGHT_GRAY);
			}
		}
		panel.updateUI();
	}

	public int getPanelY(JPanel jpanel, String pre) {
		Component c = null;
		for (Component comp : this.getComps()) {
			if (comp.getName().startsWith(pre)) {
				c = comp;
				break;
			}
		}
		if (c != null) {
			Component parent = null;
			if (c instanceof JTextField) {
				parent = c.getParent();
			} else if (c instanceof JTextArea) {
				parent = c.getParent().getParent().getParent();
			}
			return parent.getY();
		}
		return 0;
	}

	public List<Component> getComps() {
		return comps;
	}

	public void setComps(List<Component> comps) {
		this.comps = comps;
	}

	private Properties loadProperties() {
		InputStream is = null;
		InputStreamReader reader = null;
		Properties prop = new Properties();
		try {
			String code = service.get().getLanguageCountry().getIsoLangCode()
					.getCode();
			code = code.replace("-", "_");
			String fileName = "language" + File.separator + "message_" + code
					+ ".properties";
			is = new FileInputStream(fileName);
			reader = new InputStreamReader(is, "UTF-8");
			prop.load(reader);
		} catch (Exception e) {
			try {
				is = new FileInputStream(Constants.MESSAGE_PATH);
				reader = new InputStreamReader(is, "UTF-8");
				prop.load(reader);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				reader.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return prop;
	}

	private Config loadConfig() {
		Config config = null;
		InputStream is = null;
		InputStreamReader reader = null;
		try {
			String version = service.get().getVersion();
			String code = service.get().getLanguageCountry().getIsoLangCode()
					.getCode();
			code = code.replace("-", "_");
			String fileName = "language" + File.separator + "config_" + code
					+ ".properties";
			is = new FileInputStream(fileName);
			reader = new InputStreamReader(is, "UTF-8");

			JAXBContext cxt = JAXBContext.newInstance(Config.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			config = (Config) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				File file = new File(Constants.CONFIG_PATH);
				config = (Config) unmarshaller.unmarshal(file);
			} catch (JAXBException e1) {
				e1.printStackTrace();
				throw new RuntimeException(" read xml file error ");
			}
		} finally {
			try {
				if (reader != null)
					reader.close();
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return config;
	}

	public String getStatusName(String value) {
		Object[] objs = listStatus();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public Object[] listStatus() {
		Object[] os = new Object[config.getStatus().getFields().size()];
		for (int i = 0; i < config.getStatus().getFields().size(); i++) {
			Field field = config.getStatus().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	/**
	 * Relevance Type Configure Processing
	 */
	public int getStatusIndex(String value) {
		Object[] objs = listStatus();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
