package com.fangda.kass.ui.questionnaire.support;

/**
 * This Class for the Combo Item
 * 
 * @author Fangfang Zhao
 * 
 */
public class ComboItem {

	public ComboItem() {

	}

	public ComboItem(String name, String value) {
		this.name = name;
		this.value = value;
	}

	private String name;

	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return name;
	}

}
