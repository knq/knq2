package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the section edit in the QMS
 * 
 * @author Shuang Ma
 * 
 */
public class SectionEditDialog extends BaseDialog {
	QuestionnaireService service;
	JTextField sectionTitle;
	JLabel sortNoLabel;
	JLabel sidLabel;
	JComboBox sectionModelBox;
	JScrollPane detailJSPane;
	JTextArea sectionDetail;
	JTextArea sectionNote;
	JComboBox isDisable;
	JPanel panel;
	private static final long serialVersionUID = 1368382467172457229L;

	public SectionEditDialog(Map<String, Object> params) {
		super(params);
	}

	public void initDialog() {
		this.setTitle("");
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		sectionTitle = new JTextField(40);
		sortNoLabel = new JLabel("");
		sidLabel = new JLabel("");
		Object[] sectionModelArray = QuestionConstants.listSectionMode();
		sectionModelBox = new JComboBox(sectionModelArray);
		Object[] isDisableArray = QuestionConstants.listDisable();
		isDisable = new JComboBox(isDisableArray);
		isDisable.removeItemAt(2);
		sectionDetail = new JTextArea(5, 36);
		sectionDetail.setLineWrap(true);
		sectionDetail.setWrapStyleWord(true);
		detailJSPane = new JScrollPane(sectionDetail);
		detailJSPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		detailJSPane.setPreferredSize(new Dimension(300, 100));

		sectionNote = new JTextArea(5, 36);
		sectionNote.setLineWrap(true);
		sectionNote.setWrapStyleWord(true);

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		panel = new JPanel(bagLayout);
		int index = 0;
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("Sid:"), c, 0,
				index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sidLabel, c, 1, index, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("Section Mode:"),
				c, 2, index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sectionModelBox, c, 3, index,
				1, 1, GridBagConstraints.WEST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("Sort:"), c, 0,
				index, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sortNoLabel, c, 1, index, 1,
				1, GridBagConstraints.WEST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("title") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sectionTitle, c, 1, index, 3,
				1, GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("detail") + ":"), c, 0, index, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, detailJSPane, c, 1, index, 3,
				1, GridBagConstraints.EAST);
		index++;

		UIHelper.addToBagpanel(panel, Color.gray, new JLabel(""), c, 0, index,
				1, 1, GridBagConstraints.EAST);
		Subsection subsection = service.searchSubsection((String) params
				.get("sid"));
		if (subsection != null && subsection.getDisable() == 0) {// DISABLE去除
			// panel.remove(panel.getComponentCount() - 3);
			// panel.remove(panel.getComponentCount() - 2);
			isDisable.setEnabled(false);
			panel.updateUI();
		} else {
			UIHelper.addToBagpanel(panel, Color.gray,
					new JLabel(mxResources.get("isDisable") + ":"), c, 0,
					index, 1, 1, GridBagConstraints.EAST);
			UIHelper.addToBagpanel(panel, Color.gray, isDisable, c, 1, index,
					2, 1, GridBagConstraints.WEST);
			index++;
		}

		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		if (StringUtils.isBlank(sectionTitle.getText())) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("sectionTitleNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		if (StringUtils.isBlank(sectionDetail.getText())) {
			JOptionPane
					.showMessageDialog(container,
							"<HTML>" + mxResources.get("sectionDeatilNull")
									+ "</HTML>", "Warning",
							JOptionPane.INFORMATION_MESSAGE,
							UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * After open a questionnaire
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		String op = (String) params.get("op");
		if ("edit".equals(op)) {
			Subsection subsection = service.searchSubsection((String) params
					.get("sid"));
			if (subsection.getDisable() == 0) {
				panel.remove(panel.getComponentCount() - 1);
			}

			String pid = StringUtil.substringBeforeLast(subsection.getSid(),
					"_");
			Subsection parent = service.searchSubsection(pid);
			String no = "" + subsection.getSortNo();
			if (parent != null) {
				no = parent.getSortNo() + "." + subsection.getSortNo();
			}
			this.setTitle("Section " + no + " Edit");

			sidLabel.setText(subsection.getSid());
			sortNoLabel.setText("" + subsection.getSortNo());
			sectionModelBox.setSelectedIndex(QuestionConstants
					.getSectionModeIndex(subsection.getSectionMode()));

			String sid = subsection.getSid().substring(0, 6);
			int num = Integer.parseInt(sid.replace("SS_00", ""));
			if (num <= 8) {
				sectionModelBox.setEnabled(false);
			} else {
				sectionModelBox.setEnabled(true);
			}
			sectionDetail.setText(subsection.getDetail());
			sectionTitle.setText(subsection.getTitle());

			if (subsection.getDisable() != 0) {
				isDisable.setSelectedIndex(QuestionConstants.getDisableIndex(""
						+ subsection.getDisable()));
			}

		} else {
			if (params.get("sid") == null) {
				Map<String, String> map = service.genSubsectionId("");
				sidLabel.setText(map.get("sectionId"));
				sortNoLabel.setText(map.get("sortNo"));
				sectionModelBox.setSelectedIndex(QuestionConstants
						.getSectionModeIndex("question"));

			} else {
				String sid = (String) params.get("sid");
				Map<String, String> map = service.genSubsectionId(sid);
				Subsection parent = service.searchSubsection((String) params
						.get("sid"));
				String no = parent.getSortNo() + "." + map.get("sortNo");
				sidLabel.setText(map.get("sectionId"));
				sortNoLabel.setText(no);
				sectionModelBox.setSelectedIndex(QuestionConstants
						.getSectionModeIndex("question"));
				this.setTitle("Section " + no + " Add");
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		String op = (String) params.get("op");
		if ("edit".equals(op)) {
			Subsection subsection = service.searchSubsection((String) params
					.get("sid"));
			subsection.setTitle(sectionTitle.getText());
			subsection.setDetail(sectionDetail.getText());
			ComboItem ci = (ComboItem) sectionModelBox.getSelectedItem();
			subsection.setSectionMode(ci.getValue());
			if (subsection.getDisable() != 0) {
				ComboItem isdisableItem = (ComboItem) isDisable
						.getSelectedItem();
				subsection
						.setDisable(Integer.parseInt(isdisableItem.getValue()));
			}
			service.save(service.get());
		} else {
			String sid = (String) params.get("sid");
			if (StringUtil.isBlank(sid)) {
				Map<String, String> map = service.genSubsectionId("");
				Subsection subsection = new Subsection();
				subsection.setSortNo(Integer.parseInt(map.get("sortNo")));
				subsection.setSid(map.get("sectionId"));
				subsection.setTitle(sectionTitle.getText());
				subsection.setDetail(sectionDetail.getText());
				ComboItem ci = (ComboItem) sectionModelBox.getSelectedItem();
				subsection.setSectionMode(ci.getValue());
				if (subsection.getDisable() != 0) {
					ComboItem isdisableItem = (ComboItem) isDisable
							.getSelectedItem();
					subsection.setDisable(Integer.parseInt(isdisableItem
							.getValue()));
				}

				service.get().getSection().getSubsections().add(subsection);
				service.save(service.get());
			} else {
				Subsection parent = service.searchSubsection((String) params
						.get("sid"));
				Map<String, String> map = service.genSubsectionId(parent
						.getSid());

				Subsection subsection = new Subsection();
				subsection.setSid(map.get("sectionId"));
				subsection.setSortNo(Integer.parseInt(map.get("sortNo")));
				subsection.setSectionMode("question");
				subsection.setTitle(sectionTitle.getText());
				subsection.setDetail(sectionDetail.getText());
				ComboItem ci = (ComboItem) sectionModelBox.getSelectedItem();
				subsection.setSectionMode(ci.getValue());
				if (subsection.getDisable() != 0) {
					ComboItem isdisableItem = (ComboItem) isDisable
							.getSelectedItem();
					subsection.setDisable(Integer.parseInt(isdisableItem
							.getValue()));
				}
				parent.getSubSections().add(subsection);
				service.save(service.get());
			}
		}
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {
		try {
			this.km.qnMainPanel.reloadLeftTree();
			this.km.qnMainPanel.reloadRight();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
