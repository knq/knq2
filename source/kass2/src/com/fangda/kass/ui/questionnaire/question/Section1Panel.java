package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the section 1 of the questionnaire in the QMS
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class Section1Panel extends BasePanel implements ActionListener {

	private static final long serialVersionUID = 8085764691314969052L;
	private QuestionnaireService service;

	JTextField Section1Title;
	JTextField SubSection11Title;
	JTextField SubSection12Title;
	JTextArea Section1Detail;
	JTextArea Section11Detail;
	JTextArea Section11Note;
	JTextArea Section12Detail;
	JTextArea Section12Note;
	JButton stoppingRule;
	JButton autoComplete;
	JButton reviseUnion;
	JPanel selectedPanel;
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Section1Panel(String name, KnqMain km) {
		super(name, km);
	}

	@Override
	public void initPanel() {
		service = new QuestionnaireService();
	}

	/*
	 * JButton configBt; ImageIcon configIcon;
	 */
	@Override
	public JPanel createPanel() {

		JPanel panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		String panelName = this.getName();
		String sid = panelName.replace("section_", "");
		Subsection subsection = service.searchSubsection(sid);
		genSectionPanel(subsection, "" + subsection.getSortNo(), panel);
		if (subsection.getSubSections() != null) {
			for (Subsection ss : subsection.getSubSections()) {
				genSectionPanel(ss,
						subsection.getSortNo() + "." + ss.getSortNo(), panel);
			}
		}
		return panel;
	}

	private void genSectionPanel(final Subsection subsection, String no,
			JPanel panel) {

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel sectionPanel = new JPanel(bagLayout);
		sectionPanel.setBackground(Color.white);
		sectionPanel.setBorder(BorderFactory.createTitledBorder("Section " + no
				+ ""));
		sectionPanel.setName("section_" + subsection.getSid());

		JPanel propPanel = new JPanel();
		propPanel.setBackground(Color.white);
		propPanel.setLayout(new BoxLayout(propPanel, BoxLayout.X_AXIS));
		propPanel.add(new JLabel("<html><strong>Properties</strong></html>"));
		propPanel.add(Box.createHorizontalStrut(km.qnMainPanel.rightPanel
				.getWidth() - 400));

		propPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));

		UIHelper.addToBagpanel(sectionPanel, Color.white, propPanel, c, 0, 0,
				2, 1, GridBagConstraints.WEST);
		genSectionDetail(sectionPanel, subsection);// 3
		if ("SS_001".equals(subsection.getSid())) {
			genSection1Buttons(sectionPanel);// 4
		}

		JPanel questionPanel = new JPanel();
		questionPanel.setBackground(Color.white);
		questionPanel.setLayout(new BoxLayout(questionPanel, BoxLayout.X_AXIS));
		questionPanel
				.add(new JLabel("<html><strong>Questions</strong></html>"));
		questionPanel.add(Box.createHorizontalStrut(km.qnMainPanel.rightPanel
				.getWidth() - 300));
		questionPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		UIHelper.addToBagpanel(sectionPanel, Color.white, questionPanel, c, 0,
				6, 2, 1, GridBagConstraints.WEST);
		this.genQuestions(subsection, sectionPanel, 7);
		panel.add(sectionPanel);
	}

	public void genSection1Buttons(JPanel sectionPanel) {
		JButton button1 = new JButton(mxResources.get("SetUpStoppingRuleBtn"));
		GridBagConstraints c = new GridBagConstraints();
		sectionPanel.add(button1, new GridBagConstraints(0, 4, 1, 1, 1, 1,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));
		JButton button2 = new JButton(mxResources.get("SetUpAutoCompleteBtn"));
		sectionPanel.add(button2, new GridBagConstraints(1, 4, 1, 1, 1, 1,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));

		JButton button3 = new JButton(
				mxResources.get("SetPersonRequiredQuestionBtn"));
		sectionPanel.add(button3, new GridBagConstraints(0, 5, 1, 1, 1, 1,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));

		JButton button4 = new JButton(
				mxResources.get("SetUnionRequiredQuestionBtn"));
		sectionPanel.add(button4, new GridBagConstraints(1, 5, 1, 1, 1, 1,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));

		JButton button5 = new JButton(mxResources.get("SetRelevanceTypeBtn"));
		sectionPanel.add(button5, new GridBagConstraints(0, 6, 1, 1, 1, 1,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 0), 0, 0));

		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				(new StopruleEditDialog()).setMainPanel(km);
			}
		});
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				(new AutocompleteEditDialog()).setMainPanel(km);
			}
		});
		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				(new PersonRequiredQuestionDialog()).setMainPanel(km);
			}
		});
		button4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				(new UnionRequiredQuestionDialog()).setMainPanel(km);
			}
		});
		button5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				(new RelevanceTypeDialog()).setMainPanel(km);
			}
		});

	}

	public void genSectionDetail(JPanel sectionPanel, Subsection subsection) {
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel secPropertyPanel = new JPanel(bagLayout);
		secPropertyPanel.setBackground(Color.white);
		secPropertyPanel.setName("squ-section-" + subsection.getSid());

		GridBagConstraints c = new GridBagConstraints();
		UIHelper.addToBagpanel(secPropertyPanel, Color.white, new JLabel(
				"Title:"), c, 0, 0, 1, 1, GridBagConstraints.EAST);
		int titleWidth = km.qnMainPanel.rightPanel.getWidth() - 100;
		if (subsection.getTitle() != null) {
			JLabel jta = new JLabel(subsection.getTitle());
			UIHelper.createMultiLabel(jta, titleWidth);
			UIHelper.addToBagpanel(secPropertyPanel, Color.white, jta, c, 1, 0,
					1, 1, GridBagConstraints.WEST);
		}

		UIHelper.addToBagpanel(secPropertyPanel, Color.white, new JLabel(
				"Detail:"), c, 0, 3, 1, 1, GridBagConstraints.EAST);
		int detailWidth = km.qnMainPanel.rightPanel.getWidth() - 100;
		if (subsection.getDetail() != null) {
			JLabel jta = new JLabel(subsection.getDetail());
			UIHelper.createMultiLabel(jta, detailWidth);
			UIHelper.addToBagpanel(secPropertyPanel, Color.white, jta, c, 1, 3,
					1, 1, GridBagConstraints.WEST);
		}
		secPropertyPanel.addMouseListener(new MouseHandler(this,
				secPropertyPanel));
		UIHelper.addToBagpanel(sectionPanel, Color.white, secPropertyPanel, c,
				0, 1, 2, 1, GridBagConstraints.WEST);
	}

	public void genQuestions(Subsection subsection, JPanel panel, int index) {
		if (subsection.getQuestions() != null) {
			JPanel questionPanel = new JPanel();
			questionPanel.setLayout(new BoxLayout(questionPanel,
					BoxLayout.Y_AXIS));
			questionPanel.setBackground(Color.white);
			for (Question question : subsection.getQuestions()) {
				JPanel boxPanel = new JPanel();
				boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
				boxPanel.setName("section_" + question.getQid());
				boxPanel.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory
								.createMatteBorder(0, 0, 1, 0, Color.black),
						BorderFactory.createEmptyBorder(0, 0, 0, 0)));
				boxPanel.setBackground(Color.white);
				this.genQuestionDetail(question, boxPanel);
				if (question.getFollowupQuestions() != null) {
					for (Question fquestion : question.getFollowupQuestions()) {
						this.genQuestionDetail(fquestion, boxPanel);
					}
				}
				questionPanel.add(boxPanel);
			}
			panel.add(questionPanel, new GridBagConstraints(0, index, 4, 1,
					1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
			index++;
		}
	}

	public void genQuestionDetail(Question question, JPanel panel) {
		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		GridBagConstraints c = new GridBagConstraints();
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel qPanel = new JPanel(bagLayout);
		qPanel.addMouseListener(new MouseHandler(this, qPanel));
		qPanel.setName("squ-question-" + question.getQid());
		qPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
		if (!isFollowup) {
			JPanel jpanel = new JPanel();
			jpanel.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY),
					BorderFactory.createEmptyBorder(0, 0, 0, 0)));
			jpanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			jpanel.setBackground(Color.white);
			jpanel.add(new JLabel("<html><strong>" + question.getSortNo()
					+ "</strong></html>"));
			qPanel.add(jpanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
					GridBagConstraints.WEST, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
		} else {
			JPanel jpanel = new JPanel();
			jpanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0,
					Color.GRAY));
			jpanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			jpanel.setBackground(Color.white);
			jpanel.add(new JLabel(question.getSortNo() + ")"));
			// jpanel.setPreferredSize(new Dimension(50, 120));
			qPanel.add(jpanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
					GridBagConstraints.EAST, GridBagConstraints.BOTH,
					new Insets(0, 0, 0, 0), 0, 0));
		}
		JPanel questionPanel = new JPanel(bagLayout);
		questionPanel.setName("section_" + question.getQid());
		questionPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));

		int qindex = 0;

		if (StringUtil.isNotBlank(question.getTitle())) {
			UIHelper.addToBagpanel(questionPanel, Color.white, new JLabel(
					"Title:"), c, 0, qindex, 1, 1, GridBagConstraints.EAST);
			int titleWidth = km.qnMainPanel.rightPanel.getWidth() - 150;
			JLabel jta = new JLabel(question.getTitle());
			UIHelper.createMultiLabel(jta, titleWidth);
			UIHelper.addToBagpanel(questionPanel, Color.white, jta, c, 1,
					qindex, 3, 1, GridBagConstraints.WEST);
			qindex++;
		}
		String lead = "";
		if (StringUtil.isNotBlank(question.getLead())) {
			lead = question.getLead();
			UIHelper.addToBagpanel(questionPanel, Color.white, new JLabel(
					"Lead:"), c, 0, qindex, 1, 1, GridBagConstraints.EAST);
			int leadWidth = km.qnMainPanel.rightPanel.getWidth() - 150;
			JLabel jta = new JLabel(question.getLead());
			UIHelper.createMultiLabel(jta, leadWidth);
			UIHelper.addToBagpanel(questionPanel, Color.white, jta, c, 1,
					qindex, 3, 1, GridBagConstraints.WEST);
			qindex++;
		}
		if (StringUtil.isNotBlank(question.getDetail())
				&& !question.getDetail().equals(lead)) {
			UIHelper.addToBagpanel(questionPanel, Color.white, new JLabel(
					"Detail:"), c, 0, qindex, 1, 1, GridBagConstraints.EAST);
			int detailWidth = km.qnMainPanel.rightPanel.getWidth() - 150;
			JLabel jta = new JLabel(question.getDetail());
			UIHelper.createMultiLabel(jta, detailWidth);
			UIHelper.addToBagpanel(questionPanel, Color.white, jta, c, 1,
					qindex, 3, 1, GridBagConstraints.WEST);
			qindex++;
		}
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())) {
				UIHelper.addToBagpanel(questionPanel, Color.white, new JLabel(
						"GroupName:"), c, 0, qindex, 1, 1,
						GridBagConstraints.EAST);
				int detailWidth = km.qnMainPanel.rightPanel.getWidth() - 150;
				JLabel jta = new JLabel(fquestion.getGroupName());
				UIHelper.createMultiLabel(jta, detailWidth);
				UIHelper.addToBagpanel(questionPanel, Color.white, jta, c, 1,
						qindex, 3, 1, GridBagConstraints.WEST);
				qindex++;
			}
			UIHelper.addToBagpanel(questionPanel, Color.white, new JLabel(
					"Type:"), c, 0, qindex, 1, 1, GridBagConstraints.EAST);
			int detailWidth = km.qnMainPanel.rightPanel.getWidth() - 150;
			int index = QuestionConstants.getQuestionTypeIndex(question
					.getType());
			ComboItem item = (ComboItem) QuestionConstants.listQuestionType()[index];
			JLabel jta = new JLabel(item.getName());
			UIHelper.createMultiLabel(jta, detailWidth);
			UIHelper.addToBagpanel(questionPanel, Color.white, jta, c, 1,
					qindex, 3, 1, GridBagConstraints.WEST);
			qindex++;
		}
		questionPanel.setBackground(Color.white);

		genOptions(question, c, questionPanel, qindex);

		qPanel.add(questionPanel, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,
						0, 0, 0), 0, 0));

		panel.add(qPanel);
	}

	public void genOptions(Question question, GridBagConstraints c,
			JPanel panel, int index) {
		GridBagLayout bagLayout = new GridBagLayout();
		JPanel optionPanel = new JPanel(bagLayout);
		optionPanel.setBackground(Color.white);
		this.addOption(optionPanel, c, question, 0);// 选项增加
		UIHelper.addToBagpanel(panel, Color.white, optionPanel, c, 1,
				1 + index, 1, 1, GridBagConstraints.WEST);
		index++;
	}

	public int addOption(JPanel panel, GridBagConstraints c, Question question,
			int index) {

		if ("A".equals(question.getType())) {// text
			if (question instanceof FollowupQuestion) {
				FollowupQuestion fquestion = (FollowupQuestion) question;
				JTextField field = new JTextField(fquestion.getLength());
				UIHelper.addToBagpanelFill(panel, Color.white, field, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST,
						GridBagConstraints.NONE);
			}
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(5, 36);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			UIHelper.addToBagpanel(panel, Color.white, notePane, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					if ("C".equals(question.getType())) {// 单选
						optionTitle = "○ " + optionTitle;
					} else {
						optionTitle = "□ " + optionTitle;
					}

					JLabel optionLabel = new JLabel(optionTitle);
					UIHelper.addToBagpanel(panel, Color.white, optionLabel, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
			}
		}
		return index;
	}

	@Override
	public void afterShow() {
		km.qnMainPanel.rightOuterToolbar.removeAll();
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createAddButton("",
				"add", this));
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createConfigButton("",
				"edit", this));
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createUpButton("",
				"moveUp", this));
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createDownButton("",
				"moveDown", this));
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper
				.createSkipConditionButton("", "condition", this));
		km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createDeleteButton("",
				"delete", this));
		km.qnMainPanel.rightOuterToolbar.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			if ("add".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					String panelName = selectedPanel.getName();
					String[] args = panelName.split("-");
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "add");
					if ("section".equals(args[1])) {
						map.put("sid", args[2]);
						map.put("sure_next", "true");
						(new QuestionEditDialog(map)).setMainPanel(km);
					} else if ("question".equals(args[1])) {
						Question question = service.searchQuestion(args[2]);
						if (!(question instanceof FollowupQuestion)) {
							map.put("qid", args[2]);
							map.put("sure_next", "true");
							(new QuestionEditDialog(map)).setMainPanel(km);
						}
					}
				}
			} else if ("edit".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					String panelName = selectedPanel.getName();
					String[] args = panelName.split("-");
					Map<String, Object> map = new HashMap<String, Object>();
					if ("section".equals(args[1])) {
						map.put("sid", args[2]);
						map.put("op", "edit");
						(new SectionEditDialog(map)).setMainPanel(km);
					} else if ("question".equals(args[1])) {
						map.put("qid", args[2]);
						map.put("op", "edit");
						(new QuestionEditDialog(map)).setMainPanel(km);
					}
				}
			} else if ("delete".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					int result = JOptionPane.showConfirmDialog(null,
							"Are you sure delete this item", "",
							JOptionPane.YES_NO_OPTION);
					if (result == JOptionPane.YES_OPTION) {
						String panelName = selectedPanel.getName();
						String[] args = panelName.split("-");
						if ("section".equals(args[1])) {
							deleteSection(args[2]);
						} else if ("question".equals(args[1])) {
							deleteQuestion(args[2]);
						}
					}
				}
			} else if ("moveUp".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					String panelName = selectedPanel.getName();
					String[] args = panelName.split("-");
					if ("section".equals(args[1])) {
						moveUpSection(args[2]);
					} else if ("question".equals(args[1])) {
						this.moveUpQuestion(args[2]);
					}
				}
			} else if ("moveDown".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					String panelName = selectedPanel.getName();
					String[] args = panelName.split("-");
					if ("section".equals(args[1])) {
						moveDownSection(args[2]);
					} else if ("question".equals(args[1])) {
						this.moveDownQuestion(args[2]);
					}
				}
			} else if ("condition".equals(button.getActionCommand())) {
				if (selectedPanel != null) {
					String panelName = selectedPanel.getName();
					String[] args = panelName.split("-");
					this.goCondition(args);
				}
			}
		}
	}

	public void deleteQuestion(String qid) {
		Question question = service.searchQuestion(qid);
		if (question != null && question.getDisable() == 0) {
			UIHelper.showMessageDialog(this, new MessageModel(false,
					"Can't delete question "));
			return;
		}
		if (question.getFollowupQuestions() != null
				&& question.getFollowupQuestions().size() > 0) {
			UIHelper.showMessageDialog(this, new MessageModel(false,
					"Can't delete because having follow-up question"));
			return;
		}
		if (StringUtil.isNotBlank(question.getSubsection())) {
			Subsection subsection = service.searchSubsection(question
					.getSubsection());
			subsection.getQuestions().remove(question);
		} else {
			Question q = service.searchParanetQuestion(qid);
			q.getFollowupQuestions().remove(question);
		}
		service.save(service.get());

		try {
			this.km.qnMainPanel.reloadLeftTree();
			this.km.qnMainPanel.reloadRight();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteSection(String sid) {
		Subsection subsection = service.searchSubsection(sid);
		if (subsection.getDisable() == 0) {
			UIHelper.showMessageDialog(this, new MessageModel(false,
					"Can't delete subsection "));
			return;
		}
		if (subsection.getSubSections() != null
				&& subsection.getSubSections().size() > 0) {
			UIHelper.showMessageDialog(this, new MessageModel(false,
					"Can't delete because having children sections"));
			return;
		}
		if (subsection.getQuestions() != null
				&& subsection.getQuestions().size() > 0) {
			UIHelper.showMessageDialog(this, new MessageModel(false,
					"Can't delete because having children questions"));
			return;
		}
		Questionnaire q = Constants.QUESTIONNAIRE;
		List<Subsection> subsections = q.getSection().getSubsections();
		if (subsections != null) {
			for (Subsection s1 : subsections) {
				if (s1.getSid().equals(sid)) {
					subsections.remove(s1);
					break;
				}
				if (s1.getSubSections() != null) {
					for (Subsection s2 : s1.getSubSections()) {
						if (s2.getSid().equals(sid)) {
							s1.getSubSections().remove(s2);
							break;
						}
					}
				}
			}
		}
		try {
			this.km.qnMainPanel.reloadLeftTree();
			this.km.qnMainPanel.reloadRight();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void moveUpQuestion(String qid) {
		Question question = service.searchQuestion(qid);
		if (question instanceof FollowupQuestion) {
			JPanel parentPanel = (JPanel) selectedPanel.getParent();
			int index = -1;
			for (int i = 0; i < parentPanel.getComponentCount(); i++) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(i);
				if (jpanel.getName().endsWith(qid)) {
					index = i;
					break;
				}
			}
			if (index > 1) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(index);
				parentPanel.add(jpanel, index - 1);
				parentPanel.updateUI();
				Question parentQuestion = service.searchParanetQuestion(qid);
				FollowupQuestion p1 = parentQuestion.getFollowupQuestions()
						.get(index - 2);
				parentQuestion.getFollowupQuestions().set(index - 2,
						(FollowupQuestion) question);
				parentQuestion.getFollowupQuestions().set(index - 1, p1);
				service.save(service.get());
			}
		} else {
			JPanel parentPanel = (JPanel) selectedPanel.getParent().getParent();
			int index = -1;
			for (int i = 0; i < parentPanel.getComponentCount(); i++) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(i);
				if (jpanel.getName().endsWith(qid)) {
					index = i;
					break;
				}
			}
			if (index > 0) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(index);
				parentPanel.add(jpanel, index - 1);
				parentPanel.updateUI();
				Subsection subsection = service.searchSubsection(question
						.getSubsection());
				Question p1 = subsection.getQuestions().get(index - 1);
				subsection.getQuestions().set(index - 1, question);
				subsection.getQuestions().set(index, p1);
				service.save(service.get());
			}
		}
	}

	public void moveUpSection(String sid) {
		Subsection subsection = service.searchSubsection(sid);

		JPanel parentPanel = (JPanel) selectedPanel.getParent().getParent();
		int index = -1;
		for (int i = 0; i < parentPanel.getComponentCount(); i++) {
			JPanel jpanel = (JPanel) parentPanel.getComponent(i);
			if (jpanel.getName().endsWith(sid)) {
				index = i;
				break;
			}
		}
		if (index > 1) {
			JPanel jpanel = (JPanel) parentPanel.getComponent(index);
			parentPanel.add(jpanel, index - 1);
			parentPanel.updateUI();
			String psid = StringUtil.substringBeforeLast(sid, "_");
			Subsection psection = service.searchSubsection(psid);
			if (psection != null) {
				Subsection p1 = psection.getSubSections().get(index - 2);
				psection.getSubSections().set(index - 2, subsection);
				psection.getSubSections().set(index - 1, p1);
				service.save(service.get());
			}
		}
	}

	public void moveDownQuestion(String qid) {
		Question question = service.searchQuestion(qid);
		if (question instanceof FollowupQuestion) {
			JPanel parentPanel = (JPanel) selectedPanel.getParent();
			int index = -1;
			for (int i = 0; i < parentPanel.getComponentCount(); i++) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(i);
				if (jpanel.getName().endsWith(qid)) {
					index = i;
					break;
				}
			}
			if (index < parentPanel.getComponentCount() - 1) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(index);
				parentPanel.add(jpanel, index + 1);
				parentPanel.updateUI();
				Question parentQuestion = service.searchParanetQuestion(qid);
				FollowupQuestion p1 = parentQuestion.getFollowupQuestions()
						.get(index);
				parentQuestion.getFollowupQuestions().set(index,
						(FollowupQuestion) question);
				parentQuestion.getFollowupQuestions().set(index - 1, p1);
				service.save(service.get());
			}
		} else {
			JPanel parentPanel = (JPanel) selectedPanel.getParent().getParent();
			int index = -1;
			for (int i = 0; i < parentPanel.getComponentCount(); i++) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(i);
				if (jpanel.getName().endsWith(qid)) {
					index = i;
					break;
				}
			}
			if (index < parentPanel.getComponentCount() - 1) {
				JPanel jpanel = (JPanel) parentPanel.getComponent(index);
				parentPanel.add(jpanel, index + 1);
				parentPanel.updateUI();
				Subsection subsection = service.searchSubsection(question
						.getSubsection());
				Question p1 = subsection.getQuestions().get(index + 1);
				subsection.getQuestions().set(index + 1, question);
				subsection.getQuestions().set(index, p1);
				service.save(service.get());
			}
		}
	}

	public void moveDownSection(String sid) {
		Subsection subsection = service.searchSubsection(sid);
		JPanel parentPanel = (JPanel) selectedPanel.getParent().getParent();
		int index = -1;
		for (int i = 0; i < parentPanel.getComponentCount(); i++) {
			JPanel jpanel = (JPanel) parentPanel.getComponent(i);
			if (jpanel.getName().endsWith(sid)) {
				index = i;
				break;
			}
		}
		if (index > 0 && index < parentPanel.getComponentCount() - 1) {
			JPanel jpanel = (JPanel) parentPanel.getComponent(index);
			parentPanel.add(jpanel, index + 1);
			parentPanel.updateUI();
			String psid = StringUtil.substringBeforeLast(sid, "_");
			Subsection psection = service.searchSubsection(psid);
			if (psection != null) {
				Subsection p1 = psection.getSubSections().get(index);
				psection.getSubSections().set(index, subsection);
				psection.getSubSections().set(index - 1, p1);
				service.save(service.get());
			}
		}
	}

	public void goCondition(String[] args) {
		if ("section".equals(args[1])) {
			// Map<String, Object> params = new HashMap<String, Object>();
			// params.put("sid", args[2]);
			// params.put("type", "section");
			// new TextConditionDialog(params);
			return;
		} else if ("question".equals(args[1])) {
			Question question = service.searchQuestion(args[2]);
			if ("B".equals(question.getType())) {
				return;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("qid", args[2]);
			new ComboxConditionDialog(params);
			// if ("B".equals(question.getType()) ||
			// "C".equals(question.getType())) {
			// Map<String, Object> params = new HashMap<String, Object>();
			// params.put("qid", args[2]);
			// new ComboxConditionDialog(params);
			// } else {
			// Map<String, Object> params = new HashMap<String, Object>();
			// params.put("qid", args[2]);
			// params.put("type", "question");
			// new TextConditionDialog(params);
			// }
		}
	}
}

class MouseHandler implements MouseListener {
	private JPanel squPanel;
	private Section1Panel root;

	public MouseHandler() {

	}

	public MouseHandler(Section1Panel root, JPanel panel) {
		this.root = root;
		this.squPanel = panel;
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
		if (root.selectedPanel != null
				&& root.selectedPanel.getName().equals(squPanel.getName())) {
			setNone(root.selectedPanel);
			root.selectedPanel = null;
		} else {
			if (root.selectedPanel != null) {
				setNone(root.selectedPanel);
			}
			root.selectedPanel = squPanel;
			this.setHighLight(squPanel);
		}
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		this.setHighLight(squPanel);
	}

	public void mouseExited(MouseEvent e) {
		if (root.selectedPanel != null
				&& root.selectedPanel.getName().equals(squPanel.getName())) {

		} else {
			this.setNone(squPanel);
		}
	}

	private void setNone(JPanel panel) {
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
	}

	private void setHighLight(JPanel panel) {
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED),
				BorderFactory.createEmptyBorder(0, 0, 0, 0)));
	}
}