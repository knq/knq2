package com.fangda.kass.ui.questionnaire.base;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the questionnaires revise (Questionnaire edit)
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QuestionnaireEditDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3034665381970017260L;
	JLabel titleLabel;
	JLabel lanLabel;
	JLabel countryLabel;
	JLabel versionLabel;
	JLabel cultureLabel;
	JLabel noteLabel;
	JLabel lanField;
	JLabel countryField;

	JTextField titleField;
	JTextField cultureField;
	JTextArea noteField;
	JTextField versionField;
	JScrollPane notePane;
	QuestionnaireService service;

	public void initDialog() {
		this.setTitle(mxResources.get("questionnaire.edit.title"));
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		titleLabel = new JLabel();
		lanLabel = new JLabel();
		countryLabel = new JLabel();
		versionLabel = new JLabel();
		cultureLabel = new JLabel();
		noteLabel = new JLabel();
		lanField = new JLabel();
		countryField = new JLabel();

		titleField = new JTextField(40);

		versionField = new JTextField(40);
		cultureField = new JTextField(40);
		noteField = new JTextArea(5, 36);
		noteField.setLineWrap(true);
		noteField.setWrapStyleWord(true);
		notePane = new JScrollPane(noteField);
		notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		notePane.setPreferredSize(new Dimension(300, 100));

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel(bagLayout);
		panel.setBorder(BorderFactory.createTitledBorder(mxResources
				.get("questionniare.properties.config")));
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.title")
						+ ":"), c, 0, 0, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, titleField, c, 1, 0, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.language")
						+ ":"), c, 0, 1, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, lanField, c, 1, 1, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.country")
						+ ":"), c, 0, 2, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, countryField, c, 1, 2, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.version")
						+ ":"), c, 0, 3, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, versionField, c, 1, 3, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.culture")
						+ ":"), c, 0, 4, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, cultureField, c, 1, 4, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("questionniare.properties.note")
						+ ":"), c, 0, 5, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, notePane, c, 1, 5, 2, 1,
				GridBagConstraints.WEST);

		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		String title = titleField.getText();
		if ("".equals(title)) {// common.valid.required
			UIHelper.showMessageDialog(container, new MessageModel(false,
					mxResources.get("questionniare.properties.title") + " "
							+ mxResources.get("common.valid.required")));
			return false;
		}
		String version = versionField.getText();
		if ("".equals(version)) {
			UIHelper.showMessageDialog(container, new MessageModel(false,
					mxResources.get("questionniare.properties.version") + " "
							+ mxResources.get("common.valid.required")));
			return false;
		}
		return true;
	}

	public MessageModel submit() {
		Questionnaire q = service.get();
		q.setTitle(titleField.getText());
		q.setVersion(versionField.getText());
		q.setCultureArea(cultureField.getText());
		q.setNote(noteField.getText());
		service.save(q);
		return new MessageModel(true, "Submit success!");
	}

	public void afterShow() {
		Questionnaire q = service.get();
		titleField.setText(q.getTitle());
		lanField.setText(q.getLanguageName());
		countryField.setText(q.getCountryName());
		versionField.setText(q.getVersion());
		cultureField.setText(q.getCultureArea());
		noteField.setText(q.getNote());
	}

	/**
	 * After Submit
	 */
	@Override
	public void afterSubmit() {
		try {
			if (km != null) {
				this.km.qnMainPanel.reloadRight();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
