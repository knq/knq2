package com.fangda.kass.ui.questionnaire;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the project properties edit of the
 * questionnaire.
 * 
 * @author Shuang Ma, Fangfang Zhao
 * 
 */
public class ProjectPropDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7504161901042013812L;

	protected JTextField projectNameField = new JTextField();

	protected JTextField projectDirectorField = new JTextField();

	protected JTextField projectAddress = new JTextField();

	public ProjectPropDialog() {

		super((Frame) null, mxResources.get("projectProperties"), true);

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 2));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("projectProperties") + "</STRONG></HTML>"));
		titlePanel1.add(new JLabel(" "));

		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new GridLayout(5, 2));

		contentPanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		contentPanel1.add(new JLabel(mxResources.get("projectName") + ":"));
		contentPanel1.add(projectNameField);

		contentPanel1
				.add(new JLabel(mxResources.get("projectDirectory") + ":"));
		JButton projectDirectoryButton = new JButton(mxResources.get("select")
				+ "...");
		contentPanel1.add(projectDirectoryButton);
		projectDirectoryButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser(".");
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int i = chooser.showOpenDialog(getContentPane()); // opendialog

			}
		});

		contentPanel1
				.add(new JLabel(mxResources.get("projectDirectory") + ":"));
		contentPanel1.add(projectDirectorField);

		contentPanel1.add(new JLabel(mxResources.get("projectAddress") + ":"));
		JPanel p1 = new JPanel(new GridLayout(1, 3));
		String[] str1 = { "Nation", "China", "Gemany" };
		JComboBox j1 = new JComboBox(str1);
		p1.add(j1);
		String[] str2 = { "Province", "Beijing", "Shanghai" };
		JComboBox j2 = new JComboBox(str2);
		p1.add(j2);
		String[] str3 = { "City", "Beijing", "Shanghai" };
		JComboBox j3 = new JComboBox(str3);
		p1.add(j3);
		String[] str4 = { "County", "County1", "County2" };
		JComboBox j4 = new JComboBox(str4);
		p1.add(j4);
		contentPanel1.add(p1);

		contentPanel1.add(new JLabel("  "));
		contentPanel1.add(new JTextField());

		JPanel titlePanel2 = new JPanel(new GridLayout(1, 2));
		titlePanel2.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("preferences") + "</STRONG></HTML>"));
		titlePanel2.add(new JLabel(" "));
		titlePanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel2 = new JPanel(new GridLayout(2, 2));
		contentPanel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		contentPanel2.add(new JLabel(mxResources.get("stopingRule") + ":"));
		contentPanel2.add(new JTextField());

		contentPanel2.add(new JLabel(mxResources.get("fileRule") + ":"));
		contentPanel2.add(new JTextField());

		JPanel titlePanel3 = new JPanel(new GridLayout(1, 2));
		titlePanel3.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("specifyFiles") + "</STRONG></HTML>"));
		titlePanel3.add(new JLabel(" "));
		titlePanel3.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel3 = new JPanel(new GridLayout(1, 1));
		contentPanel3.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 0, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		contentPanel3.add(new JButton(mxResources.get("addExistingFile")));

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		panelBorder.add(titlePanel2, c);
		panelBorder.add(contentPanel2, c);
		panelBorder.add(titlePanel3, c);
		panelBorder.add(contentPanel3, c);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JButton applyButton = new JButton(mxResources.get("sure"));
		JButton closeButton = new JButton(mxResources.get("cancel"));
		buttonPanel.add(closeButton);
		buttonPanel.add(applyButton);
		getRootPane().setDefaultButton(applyButton);

		getContentPane().add(panelBorder, BorderLayout.CENTER);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		pack();
		setResizable(false);
		// setLocationRelativeTo(parent);
	}

}
