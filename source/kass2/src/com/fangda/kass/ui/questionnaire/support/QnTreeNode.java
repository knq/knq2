package com.fangda.kass.ui.questionnaire.support;

import java.util.List;
import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.interview.KQuestion;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.util.ColorUtil;

/**
 * This Class for the LEFT tree Model
 * 
 * @author Fangfang Zhao
 * 
 */
public class QnTreeNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 987568203016852109L;

	public QnTreeNode() {
		
	}
	
	public QnTreeNode(int type, String no, Subsection subsection, Question question,int maintype) {
		this.type = type;
		this.no = no;
		this.question = question;
		this.subsection = subsection;
		this.mainType = maintype;
	}
	/**
	 * 序号
	 */
	private String no;
	
	/**
	 * 0 question
	 * 1 interview
	 */
	private int mainType;
	
	public int getMainType() {
		return mainType;
	}

	public void setMainType(int mainType) {
		this.mainType = mainType;
	}
	/**
	 * 0 section
	 * 1 subsection
	 * 2 question
	 */
	private int type;
	
	private Question question;
	
	private Subsection subsection;

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Subsection getSubsection() {
		return subsection;
	}

	public void setSubsection(Subsection subsection) {
		this.subsection = subsection;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if(type == 2) {
			
			String showTitle = "";
			if(StringUtils.isBlank(question.getTitle())) {
				showTitle = no + " " + question.getEn_title();
			} else {
				showTitle = no + " " + question.getTitle();
			}
			
			if("G".equalsIgnoreCase(question.getDirection())) {
				showTitle = showTitle + "(Help given)";
			} else if("R".equalsIgnoreCase(question.getDirection())) {
				showTitle = showTitle + "(Help received)";
			}
			if(mainType==1){
				return getQuestionStateColor(showTitle);
			}else 
				return showTitle;
			
		} else {
			String showTitle = "";
			if(StringUtils.isBlank(subsection.getTitle())) {
				showTitle = no + " " + subsection.getEn_title();
			} else {
				showTitle = no + " " + subsection.getTitle();
			}
			if(mainType==1){
				return getSubsectionStateColor(showTitle);
			}else{
				return showTitle;
			}
			
		}
	}
	public String getQuestionStateColor(String showTitle){
		String str="";
		KassService kassService = new KassService();
		Integer completed = Integer.valueOf(kassService.getQuestion(question.getQid()).getCompleted());
		if(completed==0){
			str="<HTML><font color=\""+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}else if(completed==1){
			str="<HTML><font color=\""+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}else if(completed==2){
			str="<HTML><font color="+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}
		return str;
	}
	public String getSubsectionStateColor(String showTitle){
		String str="";
		Integer completed = Integer.valueOf(getSubsectionComplete());
		if(completed==0){ 			
			str="<HTML><font color=\""+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}else if(completed==1){
			str="<HTML><font color=\""+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}else if(completed==2){
			str="<HTML><font color="+ColorUtil.getProgressColor(completed)+"\">"+showTitle+"</font></HTML>";
		}
		return str;
	}
	public String getSubsectionComplete(){
		String str="0";
		KassService kassService = new KassService();
		List<Question> questions = subsection.getQuestions();
		for(Question qs:questions){
			KQuestion kq = kassService.getQuestion(qs.getQid());
			if(kq != null) {
				String completed = kq.getCompleted();
				if(!completed.equals("0")){
					str="1";
					break;
				}
			}
		}

		for(Question qs:questions){
			KQuestion kq = kassService.getQuestion(qs.getQid());
			if(kq != null) {
				String completed = kq.getCompleted();
				if(!completed.equals("1")){
					str="2";
					return str;
				}
			}
		}				
		
		List<Subsection> subsections = subsection.getSubSections();
		for(Subsection subsec:subsections){
			questions = subsec.getQuestions();
			for(Question qs:questions){
				String completed = kassService.getQuestion(qs.getQid()).getCompleted();
				if(!completed.equals("0")){
					str="1";
					break;
				}
			}
			if(str.equals("0")){
				return str;
			}
			for(Question qs:questions){
				String completed = kassService.getQuestion(qs.getQid()).getCompleted();
				if(!completed.equals("1")){
					str="2";
					return str;
				}
			}			
		}
		
		if(str.equals("1")){
			return str;
		}
		return str;
	}
}
