package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.BtnHelper;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.base.QuestionnaireEditDialog;

/**
 * This Class for the dialog of the questionnaire basic information view in the
 * QMS
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class QuestionPropViewPanel extends BasePanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7473455822535814530L;
	private QuestionnaireService service;
	private String type;

	public QuestionPropViewPanel(String name, KnqMain km) {
		super(name, km);
	}

	@Override
	public void initPanel() {
		service = new QuestionnaireService();
	}

	@Override
	public JPanel createPanel() {
		Questionnaire q = service.get();
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		int index = 0;
		JLabel titleLabel1 = UIHelper.genLabel(q.getTitle(), true, 13);

		UIHelper.addToBagpanel(panel, Color.white, titleLabel1, c, 0, index, 2,
				1, GridBagConstraints.CENTER);

		UIHelper.addToBagpanel(panel, Color.white, new JLabel("Language:"), c,
				0, ++index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.white,
				new JLabel(q.getLanguageName()), c, 1, index, 1, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.white, new JLabel("Version:"), c,
				0, ++index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.white, new JLabel(q.getVersion()),
				c, 1, index, 1, 1, GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.white, new JLabel("Country:"), c,
				0, ++index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.white,
				new JLabel(q.getCountryName()), c, 1, index, 1, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.white, new JLabel("CultureArea:"),
				c, 0, ++index, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.white,
				new JLabel(q.getCultureArea()), c, 1, index, 1, 1,
				GridBagConstraints.WEST);

		JTextArea jta = UIHelper.genTextArea(q.getNote(), 700);
		UIHelper.addToBagpanel(panel, Color.white, jta, c, 0, ++index, 2, 1,
				GridBagConstraints.CENTER);

		return panel;
	}

	@Override
	public void afterShow() {
		if (km.qnMainPanel != null) {
			km.qnMainPanel.rightOuterToolbar.removeAll();
			km.qnMainPanel.rightOuterToolbar.add(BtnHelper.createConfigButton(
					"", "edit", this));
			km.qnMainPanel.rightOuterToolbar.updateUI();
			km.qnMainPanel.rightOuterToolbar.setVisible(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if ("edit".equals(e.getActionCommand())) {
			new QuestionnaireEditDialog().setMainPanel(km);
			;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
