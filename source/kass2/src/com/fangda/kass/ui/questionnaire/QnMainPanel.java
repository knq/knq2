package com.fangda.kass.ui.questionnaire;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.question.QuestionPropViewPanel;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireLocalPanel;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireViewBilingual;
import com.fangda.kass.ui.questionnaire.question.QuestionnaireViewPanel;
import com.fangda.kass.ui.questionnaire.question.Section1Panel;
import com.fangda.kass.ui.questionnaire.question.SectionEditDialog;
import com.fangda.kass.ui.questionnaire.support.QnTreeNode;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the panel of the main IU of the KNQ2
 * 
 * @author Fangfang Zhao, Jianguo Zhou, Shuang Ma, Jing Kong
 * 
 */
public class QnMainPanel extends JPanel {

	private static final long serialVersionUID = 7955498697718712775L;

	JLabel statusBar;
	KnqMain kmain;
	JScrollPane leftPanel;
	public JPanel leftOuterPanel;
	public JToolBar leftOuterToolbar;
	public JToolBar topToolBar;
	public JPanel rightOuterPanel;
	public JToolBar rightOuterToolbar;
	public JScrollPane rightPanel;// 中间右侧菜单
	JPanel rightContentPanel;// 中间右侧内容
	public JTree treePanel;// 中间左 菜单
	private QuestionnaireService questionnaireService;
	JPopupMenu popmenu;
	public Boolean ifShowDetail;
	private int lastRightStatus = 0;
	private String nowPanelType = "";

	public QnMainPanel(KnqMain km) {

		Constants.PANELFLAG = "QN";
		this.kmain = km;
		// Border layout
		setLayout(new BorderLayout());

		// Creates the status bar
		statusBar = createStatusBar();
		createCenterPanel();
		add(statusBar, BorderLayout.SOUTH);
		createToolBar();
		questionnaireService = new QuestionnaireService();
	}

	/**
	 * Create status bar
	 * 
	 * @return
	 */
	protected JLabel createStatusBar() {
		JLabel statusBar = new JLabel(mxResources.get("ready"));
		statusBar.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 4));
		return statusBar;
	}

	/**
	 * Create Tool Bar
	 */
	protected void createToolBar() {

		add(topToolBar = new QnToolBar(this, JToolBar.HORIZONTAL),
				BorderLayout.NORTH);
		JButton b0 = (JButton) topToolBar.getComponentAtIndex(0);
		b0.setToolTipText(mxResources.get("open"));

		JButton b1 = (JButton) topToolBar.getComponentAtIndex(1);
		b1.setDisabledIcon(UIHelper.getImage("save_disable.png"));
		// b1.setEnabled(false);
		b1.setToolTipText(mxResources.get("save"));

		JButton b2 = (JButton) topToolBar.getComponentAtIndex(3);
		b2.setDisabledIcon(UIHelper.getImage("print_disable.png"));
		b2.setEnabled(false);
		b2.setToolTipText(mxResources.get("print"));

		JButton b3 = (JButton) topToolBar.getComponentAtIndex(5);
		b3.setDisabledIcon(UIHelper.getImage("cut_disable.png"));
		b3.setEnabled(false);
		b3.setToolTipText(mxResources.get("cut"));

		JButton b4 = (JButton) topToolBar.getComponentAtIndex(6);
		b4.setDisabledIcon(UIHelper.getImage("copy_disable.png"));
		b4.setEnabled(false);
		b4.setToolTipText(mxResources.get("copy"));

		JButton b5 = (JButton) topToolBar.getComponentAtIndex(7);
		b5.setDisabledIcon(UIHelper.getImage("paste_disable.png"));
		b5.setEnabled(false);
		b5.setToolTipText(mxResources.get("paste"));

		JButton b6 = (JButton) topToolBar.getComponentAtIndex(9);
		b6.setToolTipText(mxResources.get("view"));

		JButton b7 = (JButton) topToolBar.getComponentAtIndex(10);
		b7.setToolTipText(mxResources.get("reversionRelease"));

		JButton b8 = (JButton) topToolBar.getComponentAtIndex(12);
		b8.setToolTipText(mxResources.get("questionImport"));

		JButton b9 = (JButton) topToolBar.getComponentAtIndex(13);
		b9.setToolTipText(mxResources.get("questionExport"));

		JButton b10 = (JButton) topToolBar.getComponentAtIndex(15);
		b10.setDisabledIcon(UIHelper.getImage("zoom_disable.png"));
		b10.setEnabled(false);
		b10.setToolTipText(mxResources.get("find"));
	}

	protected void createCenterPanel() {

		getLeftPanel();
		getRightPanel();

		JSplitPane inner = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				leftOuterPanel, rightOuterPanel);
		inner.setOneTouchExpandable(true);
		inner.setDividerLocation(200);
		inner.setDividerSize(6);
		inner.setBorder(null);
		this.add(inner, BorderLayout.CENTER);
	}

	private void getLeftPanel() {
		leftOuterPanel = new JPanel(new BorderLayout());
		leftOuterPanel.setBackground(Color.white);
		leftOuterPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		leftOuterToolbar = new JToolBar();
		leftOuterToolbar.setPreferredSize(new Dimension(0, 0));
		leftOuterToolbar.setVisible(false);
		leftOuterPanel.add(leftOuterToolbar, BorderLayout.NORTH);

		leftPanel = new JScrollPane();
		leftPanel.setPreferredSize(new Dimension(200, 100));
		leftPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		leftPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		leftPanel.setBackground(Color.white);
		leftOuterPanel.add(leftPanel, BorderLayout.CENTER);
	}

	private void getRightPanel() {
		rightOuterPanel = new JPanel(new BorderLayout());
		rightOuterPanel.setBackground(Color.white);
		rightOuterPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0,
				Color.gray));
		rightOuterToolbar = new JToolBar();
		// rightOuterToolbar.setBackground(Color.white);
		rightOuterToolbar.setPreferredSize(new Dimension(200, 30));
		rightOuterToolbar.setVisible(false);
		rightOuterPanel.add(rightOuterToolbar, BorderLayout.NORTH);

		rightPanel = new JScrollPane();
		rightPanel.setPreferredSize(new Dimension(200, 100));
		rightPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		rightPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		rightPanel.setBackground(Color.white);
		rightPanel.getViewport().addContainerListener(new ContainerListener() {
			@Override
			public void componentAdded(ContainerEvent arg0) {
				if (arg0.getChild() != null) {
					String t = "";
					if (arg0.getChild() instanceof QuestionnaireLocalPanel) {
						QuestionnaireLocalPanel panel = (QuestionnaireLocalPanel) arg0
								.getChild();
						t = panel.getType();
					} else if (arg0.getChild() instanceof QuestionPropViewPanel) {
						QuestionPropViewPanel panel = (QuestionPropViewPanel) arg0
								.getChild();
						t = panel.getType();
					} else if (arg0.getChild() instanceof Section1Panel) {
						Section1Panel panel = (Section1Panel) arg0.getChild();
						t = panel.getType();
					}
					int status = 0;
					if ("temprevise".equals(t)) {
						status = 12;
					} else if ("revise".equals(t)) {
						status = 11;
					} else if ("local".equals(t)) {
						status = 13;
					}
					if (lastRightStatus == 11 && status != 11) {
						System.out.println("弹出 保存 revise");
						XmlHelper.getInstance().alertExitQn(kmain, "revise");
					}
					if (lastRightStatus == 12 && status != 12) {
						System.out.println("弹出 保存 temp revise");
						XmlHelper.getInstance()
								.alertExitQn(kmain, "temprevise");
					}
					if (lastRightStatus == 13 && status != 13) {
						System.out.println("弹出 保存 localize");
						XmlHelper.getInstance().alertExitQn(kmain, "local");
					}
				}
			}

			@Override
			public void componentRemoved(ContainerEvent arg0) {
				if (arg0.getChild() != null) {
					String t = "";
					if (arg0.getChild() instanceof QuestionnaireLocalPanel) {
						QuestionnaireLocalPanel panel = (QuestionnaireLocalPanel) arg0
								.getChild();
						t = panel.getType();
					} else if (arg0.getChild() instanceof QuestionPropViewPanel) {
						QuestionPropViewPanel panel = (QuestionPropViewPanel) arg0
								.getChild();
						t = panel.getType();
					} else if (arg0.getChild() instanceof Section1Panel) {
						Section1Panel panel = (Section1Panel) arg0.getChild();
						t = panel.getType();
					}
					if ("temprevise".equals(t)) {
						lastRightStatus = 12;
					} else if ("revise".equals(t)) {
						lastRightStatus = 11;
					} else if ("local".equals(t)) {
						lastRightStatus = 13;
					} else {
						lastRightStatus = 0;
					}
				}

			}

		});
		rightOuterPanel.add(rightPanel, BorderLayout.CENTER);
	}

	public void reload(String absPath) {
		reload(absPath, "");
	}

	public void reloadbilingual(String absPath) {
		reload(absPath, "bilingual");
	}

	public void reload(String absPath, String type) {
		nowPanelType = type;
		if ("temprevise".equals(type)) {
			String path = XmlHelper.getInstance().openRealXmlFile(absPath);
			Constants.QUESTIONNAIRE_TEMPLATE_XML = path;
			Constants.QUESTIONNAIRE = XmlHelper.getInstance().readFile();
			Constants.QUESTIONNAIRE.setStatus("draft");
			questionnaireService.save(Constants.QUESTIONNAIRE);
			Constants.QUESTIONNAIRE_STATUS = 12;
		} else if ("revise".equals(type)) {
			String path = XmlHelper.getInstance().openRealXmlFile(absPath);
			Constants.QUESTIONNAIRE_XML = path;
			Constants.QUESTIONNAIRE = XmlHelper.getInstance().readFile();
			// Constants.QUESTIONNAIRE.setStatus("draft");
			questionnaireService.save(Constants.QUESTIONNAIRE);
			Constants.QUESTIONNAIRE_STATUS = 11;
		} else if ("local".equals(type)) {
			String path = XmlHelper.getInstance().openRealXmlFile(absPath);
			Constants.QUESTIONNAIRE_XML = path;
			Constants.QUESTIONNAIRE = XmlHelper.getInstance().readFile();
			Constants.QUESTIONNAIRE.setStatus("draft");
			questionnaireService.save(Constants.QUESTIONNAIRE);
			Constants.QUESTIONNAIRE_STATUS = 13;
		} else {
			Constants.QUESTIONNAIRE_XML = absPath;
			Constants.QUESTIONNAIRE = XmlHelper.getInstance().readFile();
			Constants.QUESTIONNAIRE_STATUS = 0;
		}

		// The navigation tree of questions in the left window
		genLeftTree();
		rightPanel.getViewport().removeAll();
		if ("revise".equals(type)) {
			QuestionPropViewPanel qv = new QuestionPropViewPanel(
					"questionprop", kmain);
			qv.setType(type);
			rightPanel.getViewport().add(qv);
			rightOuterToolbar.setVisible(true);
		} else if ("temprevise".equals(type)) {
			QuestionPropViewPanel qv = new QuestionPropViewPanel(
					"questionprop", kmain);
			qv.setType(type);
			rightPanel.getViewport().add(qv);
			rightOuterToolbar.setVisible(false);
		} else if ("local".equals(type)) {
			QuestionnaireLocalPanel qv = new QuestionnaireLocalPanel(
					"questionLocal", kmain);
			qv.setType(type);
			rightPanel.getViewport().add(qv);
			rightOuterToolbar.removeAll();
			rightOuterToolbar.setVisible(false);
		} else if ("bilingual".equals(type)) {
			QuestionnaireViewBilingual qv = new QuestionnaireViewBilingual("",
					kmain);
			rightPanel.getViewport().add(qv);
			rightOuterToolbar.removeAll();
			rightOuterToolbar.setVisible(false);
		} else {
			QuestionnaireViewPanel qv = new QuestionnaireViewPanel("", kmain);
			rightPanel.getViewport().add(qv);
			rightOuterToolbar.removeAll();
			rightOuterToolbar.setVisible(false);
		}
	}

	public void reloadLeftTree() {
		genLeftTree();
	}

	public void reloadRight() {
		int count = rightPanel.getViewport().getComponentCount();
		if (count > 0
				&& rightPanel.getViewport().getComponent(0) instanceof JPanel) {
			JPanel jpanel = (JPanel) rightPanel.getViewport().getComponent(0);
			rightPanel.getViewport().removeAll();
			if (jpanel.getName() != null
					&& jpanel.getName().startsWith("section_")) {
				Section1Panel s = (Section1Panel) jpanel;
				String typ = s.getType();
				Section1Panel section1 = new Section1Panel(jpanel.getName(),
						kmain);
				section1.setType(typ);
				rightPanel.getViewport().add(section1);
			} else if (jpanel.getName() != null
					&& jpanel.getName().startsWith("questionprop")) {
				QuestionPropViewPanel vpanel = new QuestionPropViewPanel(
						jpanel.getName(), kmain);
				rightPanel.getViewport().add(vpanel);
			}
		}
	}

	private void genLeftTree() {
		// Right click menus
		popmenu = new JPopupMenu();
		JMenuItem addItem = new JMenuItem("Add", UIHelper.getImage("add.png"));
		final JMenuItem editItem = new JMenuItem("Edit",
				UIHelper.getImage("config.gif"));
		final JMenuItem moveUpItem = new JMenuItem("Up",
				UIHelper.getImage("up.gif"));
		final JMenuItem moveDownItem = new JMenuItem("Down",
				UIHelper.getImage("down.gif"));
		final JMenuItem delItem = new JMenuItem("Delete",
				UIHelper.getImage("deleteIcon.png"));

		popmenu.add(addItem);
		popmenu.add(editItem);
		popmenu.add(moveUpItem);
		popmenu.add(moveDownItem);
		popmenu.add(delItem);
		addItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePanel
						.getLastSelectedPathComponent();
				if (selectedNode == null
						|| selectedNode.getUserObject() instanceof String) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "add");
					(new SectionEditDialog(map)).setMainPanel(kmain);
				} else {
					QnTreeNode qntn = (QnTreeNode) selectedNode.getUserObject();
					if (qntn.getType() == 0) {//
						Subsection subsection = qntn.getSubsection();
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("sid", subsection.getSid());
						map.put("op", "add");
						(new SectionEditDialog(map)).setMainPanel(kmain);
					}
				}
			}
		});
		editItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePanel
						.getLastSelectedPathComponent();
				if (selectedNode != null) {
					QnTreeNode qntn = (QnTreeNode) selectedNode.getUserObject();
					if (qntn.getType() == 0 || qntn.getType() == 1) {//
						Subsection subsection = qntn.getSubsection();
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("sid", subsection.getSid());
						map.put("op", "edit");
						(new SectionEditDialog(map)).setMainPanel(kmain);
					}
				}
			}
		});
		moveUpItem.addActionListener(new ActionListener() {// UP
					@Override
					public void actionPerformed(ActionEvent e) {
						DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePanel
								.getLastSelectedPathComponent();
						QnTreeNode qntn = (QnTreeNode) selectedNode
								.getUserObject();
						if (selectedNode != null
								&& (qntn.getType() == 0 || qntn.getType() == 1)) {
							Subsection subsection = qntn.getSubsection();

							List<Subsection> subsections = null;
							if (qntn.getType() == 0) {
								subsections = questionnaireService.get()
										.getSection().getSubsections();
							} else {
								String parentId = StringUtil
										.substringBeforeLast(qntn
												.getSubsection().getSid(), "_");
								Subsection ss = questionnaireService
										.searchSubsection(parentId);
								subsections = ss.getSubSections();
							}
							int index = -1;
							for (int i = 0; i < subsections.size(); i++) {
								Subsection s = subsections.get(i);
								if (s.getSid().equals(subsection.getSid())) {
									index = i;
									break;
								}
							}
							if (index > 0) {
								Subsection s1 = subsections.get(index - 1);
								subsections.set(index - 1, subsection);
								subsections.set(index, s1);
								questionnaireService.save(questionnaireService
										.get());
								reloadLeftTree();
								collAndSelectTreeNode(subsection.getSid());
								reloadRight();
							}
						}
					}
				});
		moveDownItem.addActionListener(new ActionListener() {// DOWN
					@Override
					public void actionPerformed(ActionEvent e) {
						DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePanel
								.getLastSelectedPathComponent();
						QnTreeNode qntn = (QnTreeNode) selectedNode
								.getUserObject();
						if (selectedNode != null
								&& (qntn.getType() == 0 || qntn.getType() == 1)) {
							Subsection subsection = qntn.getSubsection();
							List<Subsection> subsections = null;
							if (qntn.getType() == 0) {
								subsections = questionnaireService.get()
										.getSection().getSubsections();
							} else {
								String parentId = StringUtil
										.substringBeforeLast(qntn
												.getSubsection().getSid(), "_");
								Subsection ss = questionnaireService
										.searchSubsection(parentId);
								subsections = ss.getSubSections();
							}
							int index = -1;
							for (int i = 0; i < subsections.size(); i++) {
								Subsection s = subsections.get(i);
								if (s.getSid().equals(subsection.getSid())) {
									index = i;
									break;
								}
							}
							if (index >= 0 && index < subsections.size() - 1) {
								Subsection s1 = subsections.get(index + 1);
								subsections.set(index + 1, subsection);
								subsections.set(index, s1);
								questionnaireService.save(questionnaireService
										.get());
								reloadLeftTree();
								collAndSelectTreeNode(subsection.getSid());
								reloadRight();
							}
						}
					}
				});
		delItem.addActionListener(new ActionListener() {// DELETE
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePanel
						.getLastSelectedPathComponent();
				QnTreeNode qntn = (QnTreeNode) selectedNode.getUserObject();
				if (selectedNode != null
						&& (qntn.getType() == 0 || qntn.getType() == 1)) {
					int result = JOptionPane.showConfirmDialog(null,
							"Are you sure delete this item", "",
							JOptionPane.YES_NO_OPTION);
					if (result == JOptionPane.YES_OPTION) {
						Subsection subsection = qntn.getSubsection();
						if (subsection.getSubSections() != null
								&& subsection.getSubSections().size() > 0) {
							UIHelper.showMessageDialog(
									kmain,
									new MessageModel(false,
											"Can't delete because having children sections"));
							return;
						}
						if (subsection.getQuestions() != null
								&& subsection.getQuestions().size() > 0) {
							UIHelper.showMessageDialog(
									kmain,
									new MessageModel(false,
											"Can't delete because having children questions"));
							return;
						}
						Questionnaire q = Constants.QUESTIONNAIRE;
						if (qntn.getType() == 0) {
							List<Subsection> subsections = q.getSection()
									.getSubsections();
							subsections.remove(subsection);
						} else {
							String parentId = StringUtil.substringBeforeLast(
									qntn.getSubsection().getSid(), "_");
							Subsection parent = questionnaireService
									.searchSubsection(parentId);
							parent.getSubSections().remove(subsection);
						}
						questionnaireService.save(questionnaireService.get());
						reloadLeftTree();
						if (qntn.getType() == 0) {
							rightPanel.getViewport().removeAll();
							rightPanel.repaint();
						} else {
							reloadRight();
						}
					}
				}
			}
		});

		// Left tree
		DefaultMutableTreeNode top = questionnaireService.getLeftTree(0);
		treePanel = new JTree(top);
		treePanel.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				// TODO Auto-generated method stub
				DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) e
						.getPath().getLastPathComponent();
				if (treeNode.getUserObject() instanceof QnTreeNode) {
					QnTreeNode qntn = (QnTreeNode) treeNode.getUserObject();
					clickTree(qntn);
				} else {
					if (rightPanel.getViewport().getComponentCount() > 0
							&& "questionViewPanel".equals(rightPanel
									.getViewport().getComponent(0).getName())) {
						rightPanel.getVerticalScrollBar().setValue(0);
					} else if (rightPanel.getViewport().getComponentCount() > 0
							&& "questionLocal".equals(rightPanel.getViewport()
									.getComponent(0).getName())) {
						rightPanel.getVerticalScrollBar().setValue(0);
					} else {
						rightPanel.getViewport().removeAll();
						QuestionPropViewPanel qv = new QuestionPropViewPanel(
								"questionprop", kmain);
						rightPanel.getViewport().add(qv);
					}
				}
			}
		});
		treePanel.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				TreePath path = treePanel.getPathForLocation(e.getX(), e.getY());
				if (path == null) {
					return;
				}
				treePanel.setSelectionPath(path);
				if (e.getButton() == 3) {
					DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) path
							.getLastPathComponent();

					if (treeNode.getUserObject() instanceof QnTreeNode) {
						QnTreeNode qntn = (QnTreeNode) treeNode.getUserObject();
						if (qntn.getType() == 0 || qntn.getType() == 1) {
							Subsection subsection = qntn.getSubsection();
							if ((qntn.getType() == 0 || qntn.getType() == 1)
									&& subsection.getRequired() != null
									&& subsection.getRequired() == 1) {
								editItem.setEnabled(true);
								moveUpItem.setEnabled(false);
								moveDownItem.setEnabled(false);
								delItem.setEnabled(false);
							} else {
								editItem.setEnabled(true);
								moveUpItem.setEnabled(true);
								moveDownItem.setEnabled(true);
								delItem.setEnabled(true);
							}
							popmenu.show(e.getComponent(), e.getX(), e.getY());
						}
					} else {
						editItem.setEnabled(false);
						moveUpItem.setEnabled(false);
						moveDownItem.setEnabled(false);
						delItem.setEnabled(false);
						popmenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});

		// Set the navigation tree of the questions in the left window
		DefaultTreeCellRenderer cellRenderer = (DefaultTreeCellRenderer) treePanel
				.getCellRenderer();
		UIHelper.setTreeRender(cellRenderer);
		treePanel.setShowsRootHandles(false);
		treePanel.setRootVisible(true);
		leftPanel.getViewport().removeAll();
		leftPanel.getViewport().add(treePanel);
		leftPanel.repaint();
		leftOuterToolbar.setVisible(true);

	}

	/**
	 * Click the tree
	 * 
	 * @param qntn
	 */
	private void clickTree(QnTreeNode qntn) {
		if (rightPanel.getViewport().getComponentCount() > 0
				&& "questionViewPanel".equals(rightPanel.getViewport()
						.getComponent(0).getName())) {
			String labelName = "section_";
			if (qntn.getType() == 0) {
				labelName = labelName + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 1) {
				labelName = labelName + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 2) {
				labelName = labelName + qntn.getQuestion().getQid();
			}
			JPanel component1 = (JPanel) rightPanel.getViewport().getComponent(
					0);
			JPanel component2 = (JPanel) component1.getComponent(0);
			Component component = component2.getComponent(0);
			int y = 0;
			if (component instanceof JPanel) {
				JPanel jpanel = (JPanel) component;
				int count = jpanel.getComponentCount();
				for (int i = 0; i < count; i++) {
					Component c = jpanel.getComponent(i);
					if (labelName.equals(c.getName())) {
						y = c.getY();
						break;
					}
				}
			}
			rightPanel.getVerticalScrollBar().setValue(y);
		} else if (rightPanel.getViewport().getComponentCount() > 0
				&& "questionLocal".equals(rightPanel.getViewport()
						.getComponent(0).getName())) {
			String labelName = "";
			if (qntn.getType() == 0) {
				labelName = "subsection-title-" + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 1) {
				labelName = "subsection-title-" + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 2) {
				labelName = "question-title-" + qntn.getQuestion().getQid();
			}
			JPanel component1 = (JPanel) rightPanel.getViewport().getComponent(
					0);
			QuestionnaireLocalPanel panel = (QuestionnaireLocalPanel) component1;
			int y = panel.getPanelY(panel, labelName);
			rightPanel.getVerticalScrollBar().setValue(y);
		} else {
			String panelName = "section_";
			String componentName = "section_";
			boolean needParent = false;
			if (qntn.getType() == 0) {
				panelName = panelName + qntn.getSubsection().getSid();
				componentName = componentName + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 1) {
				panelName = panelName
						+ StringUtil.substringBeforeLast(qntn.getSubsection()
								.getSid(), "_");
				componentName = componentName + qntn.getSubsection().getSid();
			} else if (qntn.getType() == 2) {
				componentName = componentName + qntn.getQuestion().getQid();
				String subsection = qntn.getQuestion().getSubsection();
				if (StringUtil.countStr(subsection, "_") == 2) {
					panelName = panelName
							+ StringUtil.substringBeforeLast(subsection, "_");
					needParent = true;
				}
				if (StringUtil.countStr(subsection, "_") == 1) {
					panelName = panelName + subsection;
				}
			}
			if (rightPanel.getViewport().getComponentCount() == 0) {
				this.addSectionPanel(qntn, panelName);
			} else {
				Component component = rightPanel.getViewport().getComponent(0);
				if (component instanceof BasePanel) {
					BasePanel panel = (BasePanel) component;
					if (panelName.equals(panel.getName())) {// 查询到此数据
						JPanel jpanel = (JPanel) rightPanel.getViewport()
								.getComponent(0);
						int y = this.getCompontPosition(componentName, jpanel);
						if (needParent) {
							String subsection = qntn.getQuestion()
									.getSubsection();
							String pname = "section_" + subsection;
							int py = this.getCompontPosition(pname, jpanel);
							rightPanel.getVerticalScrollBar().setValue(py + y);
						} else {
							rightPanel.getVerticalScrollBar().setValue(y);
						}
					} else {
						rightPanel.getViewport().removeAll();
						this.addSectionPanel(qntn, panelName);
					}
				} else {
					rightPanel.getViewport().removeAll();
					this.addSectionPanel(qntn, panelName);
				}
			}
		}
	}

	private void addSectionPanel(QnTreeNode qntn, String panelName) {
		Section1Panel section1 = new Section1Panel(panelName, kmain);
		section1.setType(nowPanelType);
		rightPanel.getViewport().add(section1);
		rightPanel.repaint();
	}

	private int getCompontPosition(String componentName, JPanel jpanel) {
		if (componentName.equals(jpanel.getName())) {
			return jpanel.getY();
		}
		int count = jpanel.getComponentCount();
		for (int i = 0; i < count; i++) {
			Component component = jpanel.getComponent(i);
			if (component instanceof JPanel) {
				int c = getCompontPosition(componentName, (JPanel) component);
				if (c > 0) {
					return c;
				}
			}
		}
		return 0;
	}

	public void collAndSelectTreeNode(String value) {
		DefaultTreeModel model = (DefaultTreeModel) treePanel.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		collAndSelectTreeNode(root, value);
	}

	private void collAndSelectTreeNode(DefaultMutableTreeNode node, String value) {
		if (node.isLeaf()) {
			return;
		}
		for (int i = 0; i < node.getChildCount(); i++) {
			DefaultMutableTreeNode tn = (DefaultMutableTreeNode) node
					.getChildAt(i);
			QnTreeNode item = (QnTreeNode) tn.getUserObject();
			if ((item.getSubsection() != null && value.equals(item
					.getSubsection().getSid()))
					|| (item.getQuestion() != null && value.equals(item
							.getQuestion().getQid()))) {
				// expand
				TreePath path = new TreePath(
						((DefaultMutableTreeNode) tn).getPath());
				treePanel.collapsePath(path);
				treePanel.setSelectionPath(path);
				return;
			} else {
				collAndSelectTreeNode(tn, value);
			}
		}
	}
}
