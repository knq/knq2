package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the Complete Procedure Step Person Type Edit in
 * the QMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class StepPersonTypeEditDialog extends BaseDialog {

	/**
	 * Complete Procedure Step Person Type Edit
	 * 
	 * Data Model: CompleteSubStep.java
	 * 
	 * @author KongJong
	 * 
	 */

	QuestionnaireService service;
	JTextArea sectionTitle;
	JTextArea sectionDetail;
	JTextArea note;
	JButton disableBt;
	private static final long serialVersionUID = 1L;

	public void initDialog() {
		this.setTitle("Section 1 Edit");
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		sectionTitle = new JTextArea(
				"Help with everyone including people inside your household", 2,
				50);
		sectionDetail = new JTextArea(
				"The next few questions apply to everyone including people inside your household.",
				2, 50);
		note = new JTextArea("", 2, 50);
		JTextArea[] jtextarea = { sectionTitle, sectionDetail, note };
		UIHelper.setTextAreaFormat(jtextarea);
		String[] str1 = { "1", "2", "3" };
		JComboBox j1 = new JComboBox(str1);
		disableBt = new JButton(mxResources.get("disable"));

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel(bagLayout);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("parentDirectory") + ":"), c, 0, 0,
				2, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("Section 5"), c,
				2, 0, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("orderNumber") + ":"), c, 3, 0, 1,
				1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, j1, c, 4, 0, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("sid:"), c, 0, 1,
				1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("SS_005_01"), c,
				1, 1, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("sectionMode") + ":"), c, 3, 1, 1,
				1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel("Question"), c, 4,
				1, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("title") + ":"), c, 0, 2, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sectionTitle, c, 1, 2, 4, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("detail") + ":"), c, 0, 3, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, sectionDetail, c, 1, 3, 4, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("note") + ":"), c, 0, 4, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, note, c, 1, 4, 4, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, disableBt, c, 0, 5, 2, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, new JLabel(
				"This subsection can be disable for some culture area"), c, 2,
				5, 4, 1, GridBagConstraints.WEST);

		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		if ("".equals(sectionTitle)) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("sectionTitleNull") + "</HTML>",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		} else if ("".equals(sectionDetail)) {
			JOptionPane
					.showMessageDialog(container,
							"<HTML>" + mxResources.get("sectionDeatilNull")
									+ "</HTML>", "Warning",
							JOptionPane.INFORMATION_MESSAGE,
							UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object o = e.getSource();
		if (o == disableBt) {

			Questionnaire questionnire = service.getQuestionnaire();
			// service.searchSubsection(id);
		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {

		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

}