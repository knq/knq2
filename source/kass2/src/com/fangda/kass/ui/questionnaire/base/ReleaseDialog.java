package com.fangda.kass.ui.questionnaire.base;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Revise;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.DateUtil;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the questionnaires revision release.
 * 
 * @author Fangfang Zhao, Shuang Ma
 * 
 */
public class ReleaseDialog extends BaseDialog {

	JLabel lanLabel;
	JLabel countryLabel;
	JTextField versionField;
	JTextField reviserField;
	JTextField directoryField;
	JTextArea noteArea;
	JDatePickerImpl datePicker;
	QuestionnaireService service;
	JButton fileButton;

	private static final long serialVersionUID = 1368382467172457229L;

	public void initDialog() {
		this.setTitle(mxResources.get("reversionRelease"));
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		versionField = new JTextField(30);
		reviserField = new JTextField(30);
		directoryField = new JTextField(30);
		noteArea = new JTextArea(3, 28);
		lanLabel = new JLabel();
		countryLabel = new JLabel();
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel);
		datePicker.setTextEditable(true);
		fileButton = new JButton("...");
		fileButton.addActionListener(this);

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel(bagLayout);
		panel.setBorder(BorderFactory.createTitledBorder(mxResources
				.get("releaseInfo")));
		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("language") + ":"), c, 0, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, lanLabel, c, 1, 0, 1, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("country") + ":"), c, 2, 0, 1, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, countryLabel, c, 3, 0, 2, 1,
				GridBagConstraints.EAST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("version") + ":"), c, 0, 1, 2, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, versionField, c, 2, 1, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("reviser") + ":"), c, 0, 2, 2, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, reviserField, c, 2, 2, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("reviseDate") + ":"), c, 0, 3, 2, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, datePicker, c, 2, 3, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("note") + ":"), c, 0, 4, 2, 1,
				GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, noteArea, c, 2, 4, 2, 1,
				GridBagConstraints.WEST);

		UIHelper.addToBagpanel(panel, Color.gray,
				new JLabel(mxResources.get("ReleaseToDirectory") + ":"), c, 0,
				5, 2, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel, Color.gray, directoryField, c, 2, 5, 1,
				1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel, Color.gray, fileButton, c, 3, 5, 1, 1,
				GridBagConstraints.EAST);

		return panel;
	}

	/**
	 * Validation
	 */
	@Override
	public boolean beforeValid() {
		// TODO Auto-generated method stub
		String version = versionField.getText();
		if ("".equals(version)) {
			JOptionPane.showMessageDialog(container, "verson can't be null",
					"Warning", JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}
		return true;
	}

	/**
	 * After open a questionnaire
	 */
	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		Questionnaire questionnire = service.getQuestionnaire();
		lanLabel.setText(questionnire.getLanguage());
		countryLabel.setText(questionnire.getCultureArea());
		versionField.setText(questionnire.getVersion());
		Date now = new Date();
		datePicker.setDate(DateUtil.getYear(now), DateUtil.getMonth(now),
				DateUtil.getDay(now));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object o = e.getSource();
		if (o == fileButton) {
			JFileChooser chooser = new JFileChooser(".");
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int result = chooser.showOpenDialog(this.container); // opendialog
			if (result == JFileChooser.APPROVE_OPTION) {
				directoryField.setText(chooser.getSelectedFile()
						.getAbsolutePath());
			}
		}
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		String version = versionField.getText();
		String reviser = reviserField.getText();
		Date reviseDate = (Date) datePicker.getModel().getValue();
		String note = noteArea.getText();
		String dir = directoryField.getText();

		Questionnaire questionnire = service.getQuestionnaire();
		String fileName = "questions_" + questionnire.getLanguage() + "_"
				+ questionnire.getCountry() + "_" + version + ".properties";
		String filePath = dir + File.separator + fileName;
		Revise r = new Revise();
		r.setVersion(version);
		r.setReviseDate(DateUtil.format(reviseDate));
		r.setReviser(reviser);
		r.setNote(note);
		questionnire.getRevises().add(r);
		questionnire.setVersion(version);
		questionnire.setStatus("Formal");
		service.saveQuestionnaireFile(questionnire, filePath);
		return new MessageModel(true, mxResources.get("submitSuccess"));
	}

	/**
	 * After Submit
	 */
	@Override
	public void afterSubmit() {

	}
}
