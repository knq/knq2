package com.fangda.kass.ui.questionnaire.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PropertyResourceBundle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.config.Config;
import com.fangda.kass.model.config.Field;
import com.fangda.kass.model.config.IsoLangCode;
import com.fangda.kass.model.config.LeadQuestionTypeField;
import com.fangda.kass.model.config.RelevanceType;
import com.fangda.kass.model.config.RelevanceTypeField;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * This Class for the constants of the questionnaire from the config.properties
 * 
 * @author Fangfang Zhao
 * 
 */
public class QuestionConstants {

	private static Config config;

	static {
		loadConfig();
	}

	public static void loadConfig() {
		if (config == null) {
			try {
				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				File file = new File(Constants.CONFIG_PATH);
				config = (Config) unmarshaller.unmarshal(file);
			} catch (JAXBException e) {
				e.printStackTrace();
				throw new RuntimeException(" read xml file error ");
			}
		}
	}

	public static void reload(String languageName, String languageCountry) {
		List<IsoLangCode> codes = config.getLanguageCountry().getIsoLangCodes();
		String c = "";
		for (int i = 0; i < codes.size(); i++) {
			IsoLangCode code = codes.get(i);
			if (StringUtils.equals(code.getLanguageName(), languageName)
					&& StringUtils.equals(code.getCountryName(),
							languageCountry)) {
				c = code.getCode();
				break;
			}
		}
		c = c.replace("-", "_");
		reloadMessage(c);
		reloadConfig(c);
	}

	public static void reloadMessage(String code) {
		String messagePath = "language" + File.separator + "message_" + code
				+ ".properties";
		File messageFile = new File(messagePath);
		if (!messageFile.exists()) {
			messagePath = "language" + File.separator + "message.properties";
		}
		mxResources.getBundles().clear();
		InputStream is = null;
		try {
			is = new FileInputStream(messagePath);
			mxResources.add(new PropertyResourceBundle(is));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Reload
	 * 
	 * @param code
	 */
	public static void reloadConfig(String code) {

		String configPath = "language" + File.separator + "config_" + code
				+ ".properties";
		File configFile = new File(configPath);
		if (!configFile.exists()) {
			configPath = "language" + File.separator + "config.properties";
		}
		Constants.CONFIG_PATH = configPath;

		InputStream is = null;
		InputStreamReader reader = null;
		try {
			is = new FileInputStream(configPath);
			reader = new InputStreamReader(is, "UTF-8");

			JAXBContext cxt = JAXBContext.newInstance(Config.class);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			config = (Config) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			try {
				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				File file = new File(Constants.CONFIG_PATH);
				config = (Config) unmarshaller.unmarshal(file);
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Force to reload
	 */
	public static void loadForceConfig() {
		InputStream is = null;
		InputStreamReader reader = null;
		try {
			if (Constants.QUESTIONNAIRE != null) {
				String code = Constants.QUESTIONNAIRE.getLanguageCountry()
						.getIsoLangCode().getCode();
				String version = Constants.QUESTIONNAIRE.getVersion();
				code = code.replace("-", "_");
				String fileName = "language" + File.separator + "config_"
						+ code + "_" + version + ".properties";
				is = new FileInputStream(fileName);
				reader = new InputStreamReader(is, "UTF-8");

				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				config = (Config) unmarshaller.unmarshal(reader);
			} else {
				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				File file = new File(Constants.CONFIG_PATH);
				config = (Config) unmarshaller.unmarshal(file);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			System.err.println(e.getMessage());
			try {
				JAXBContext cxt = JAXBContext.newInstance(Config.class);
				Unmarshaller unmarshaller = cxt.createUnmarshaller();
				File file = new File(Constants.CONFIG_PATH);
				config = (Config) unmarshaller.unmarshal(file);
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Save the config.properties XML file
	 */
	public static void saveConfig() {
		try {
			JAXBContext context = JAXBContext.newInstance(Config.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
			File file = new File(Constants.CONFIG_PATH);
			marshaller.marshal(config, file);
		} catch (Exception e) {
			throw new RuntimeException(" read xml file error ");
		}
	}

	public static Object[] listLans() {
		Object[] os = new Object[config.getLanguageCountry().getIsoLangCodes()
				.size()];
		for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
				.size(); i++) {
			IsoLangCode code = config.getLanguageCountry().getIsoLangCodes()
					.get(i);
			os[i] = code;
		}
		return os;
	}

	public static int getLanIndex(String value) {
		Object[] objs = listLans();
		int index = 0;
		for (Object obj : objs) {
			IsoLangCode item = (IsoLangCode) obj;
			if (item.getCode().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	/**
	 * Get the language
	 * 
	 * @return Object[]
	 */
	public static Object[] listLanguage() {

		List<ComboItem> list = new ArrayList<ComboItem>();
		for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
				.size(); i++) {
			IsoLangCode code = config.getLanguageCountry().getIsoLangCodes()
					.get(i);
			boolean contains = false;
			for (ComboItem ci : list) {
				if (ci.getValue().equals(code.getLanguageCode())) {
					contains = true;
					break;
				}
			}
			if (!contains) {
				list.add(new ComboItem(code.getLanguageName(), code
						.getLanguageCode()));
			}
		}
		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});
		// for(ComboItem u : list){System.out.println("$$$"+u.getName());}
		return list.toArray();
	}

	public static int getLanguageIndex(String value) {
		Object[] objs = listLanguage();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getName().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	/**
	 * Get the Country by language
	 * 
	 * @return Object[]
	 */
	public static Object[] listCountry(String language) {
		List<ComboItem> list = new ArrayList<ComboItem>();
		if (language != null && !"".equals(language)) {
			for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
					.size(); i++) {
				IsoLangCode code = config.getLanguageCountry()
						.getIsoLangCodes().get(i);
				if (language.equals(code.getLanguageCode())) {
					boolean contains = false;
					for (ComboItem ci : list) {
						if (ci.getValue().equals(code.getLanguageCode())) {
							contains = true;
							break;
						}
					}
					if (!contains) {
						if (code.getCountryName() != "") {
							list.add(new ComboItem(code.getCountryName(), code
									.getCountryCode()));
						}
					}
				}
			}
		} else {
			for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
					.size(); i++) {
				IsoLangCode code = config.getLanguageCountry()
						.getIsoLangCodes().get(i);
				boolean contains = false;
				if (code.getCountryName() != "") {
					for (ComboItem ci : list) {
						if (ci.getValue().equals(code.getLanguageCode())) {
							contains = true;
							break;
						}
					}
				}
				if (!contains) {
					if (code.getCountryName() != "") {
						list.add(new ComboItem(code.getCountryName(), code
								.getCountryCode()));
					}
				}
			}
		}
		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});
		return list.toArray();
	}

	public static int getCountryIndex(String value) {
		Object[] objs = listCountry("");
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getName().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static Object[] listDirection() {
		Object[] os = new Object[config.getDirectionType().getFields().size()];
		for (int i = 0; i < config.getDirectionType().getFields().size(); i++) {
			Field field = config.getDirectionType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getDirectionIndex(String value) {
		Object[] objs = listDirection();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getDirectionValue(String value) {
		Object[] objs = listDirection();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	/**
	 * Status Configure Processing
	 */
	public static Object[] listStatus() {
		Object[] os = new Object[config.getStatus().getFields().size()];
		for (int i = 0; i < config.getStatus().getFields().size(); i++) {
			Field field = config.getStatus().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	/**
	 * Relevance Type Configure Processing
	 */
	public static int getStatusIndex(String value) {
		Object[] objs = listStatus();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getStatusName(String value) {
		Object[] objs = listStatus();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	/**
	 * Relevance Type Processing
	 */
	public static Object[] listRelevant() {
		RelevanceType rt = getRelevantType();
		Object[] os = new Object[rt.getFields().size()];
		for (int i = 0; i < rt.getFields().size(); i++) {
			Field field = rt.getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}


	/**
	 * Get the RelevanceType
	 * 
	 * @return RelevanceType
	 */
	public static RelevanceType getRelevantType() {
		Questionnaire q = Constants.QUESTIONNAIRE;
		if (q != null) {
			if (q.getRelevanceType() == null) {
				q.setRelevanceType(config.getRelevanceType());
			}
			return q.getRelevanceType();
		}
		return config.getRelevanceType();
	}

	public static int getRelevantIndex(String value) {
		Object[] objs = listRelevant();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getRelevantName(String value) {
		Object[] objs = listRelevant();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public static String getRelevantCondition(String value) {
		RelevanceType rt = getRelevantType();
		List<RelevanceTypeField> fields = rt.getFields();
		for (RelevanceTypeField field : fields) {
			if (StringUtils.equalsIgnoreCase(field.getValue(), value)) {
				return field.getCondition();
			}
		}
		return "";
	}

	public static Object[] listSectionMode() {
		Object[] os = new Object[config.getSectionMode().getFields().size()];
		for (int i = 0; i < config.getSectionMode().getFields().size(); i++) {
			Field field = config.getSectionMode().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getSectionModeIndex(String value) {
		Object[] objs = listSectionMode();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static Object[] listDisable() {
		Object[] os = new Object[config.getDisableType().getFields().size()];
		for (int i = 0; i < config.getDisableType().getFields().size(); i++) {
			Field field = config.getDisableType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getDisableIndex(String value) {
		Object[] objs = listDisable();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getDisableName(String value) {
		Object[] objs = listDisable();
		int i = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public static Object[] listQuestionType() {
		Object[] os = new Object[config.getFollowupQuestionType().getFields()
				.size()];
		for (int i = 0; i < config.getFollowupQuestionType().getFields().size(); i++) {
			Field field = config.getFollowupQuestionType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static Object[] listFollowupQuestionType() {
		Object[] os = new Object[config.getFollowupQuestionType().getFields()
				.size()];
		for (int i = 0; i < config.getFollowupQuestionType().getFields().size(); i++) {
			Field field = config.getFollowupQuestionType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getQuestionTypeIndex(String value) {
		Object[] objs = listQuestionType();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getFollouQuestionTypeName(String value) {
		Object[] objs = listFollowupQuestionType();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
			index++;
		}
		return "";
	}

	public static Object[] listDataType() {
		Object[] os = new Object[config.getDataType().getFields().size()];
		for (int i = 0; i < config.getDataType().getFields().size(); i++) {
			Field field = config.getDataType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getDataTypeIndex(String value) {
		Object[] objs = listDataType();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static Object[] listLayout() {
		Object[] os = new Object[config.getLayout().getFields().size()];
		for (int i = 0; i < config.getLayout().getFields().size(); i++) {
			Field field = config.getLayout().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getLayout(String value) {
		Object[] objs = listLayout();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getQuestionTypeName(String value) {
		Object[] objs = listQuestionType();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public static Object[] listLeadQuestionType() {
		Object[] os = new Object[config.getLeadQuestionType().getFields()
				.size()];
		for (int i = 0; i < config.getLeadQuestionType().getFields().size(); i++) {
			Field field = config.getLeadQuestionType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static Object[] getLeadQuestionFiled(String sectionMode) {
		List<ComboItem> fields = new ArrayList<ComboItem>();
		for (int i = 0; i < config.getLeadQuestionType().getFields().size(); i++) {
			LeadQuestionTypeField field = config.getLeadQuestionType()
					.getFields().get(i);
			if (StringUtils.equals(sectionMode, field.getSectionMode())) {
				fields.add(new ComboItem(field.getName(), field.getValue()));
			}
		}
		return fields.toArray();
	}

	public static int getLeadQuestionTypeIndex(String value) {
		Object[] objs = listLeadQuestionType();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static int getLeadQuestionTypeIndex(String value, String sectionMode) {
		Object[] objs = getLeadQuestionFiled(sectionMode);
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getLeadQuestionTypeString(String value) {
		Object[] objs = listLeadQuestionType();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public static String getFollowupQuestionTypeString(String value) {
		Object[] objs = listLeadQuestionType();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return item.getName();
			}
		}
		return "";
	}

	public static Object[] listSex() {
		Object[] os = new Object[config.getSex().getFields().size()];
		for (int i = 0; i < config.getSex().getFields().size(); i++) {
			Field field = config.getSex().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getSexIndex(String value) {
		Object[] objs = listSex();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static String getSexString(String index) {
		Object[] objs = listSex();
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(index)) {
				return item.getName();
			}
		}
		return "";
	}

	public static Object[] listDirectionType() {
		Object[] os = new Object[config.getDirectionType().getFields().size()];
		for (int i = 0; i < config.getDirectionType().getFields().size(); i++) {
			Field field = config.getDirectionType().getFields().get(i);
			os[i] = new ComboItem(field.getName(), field.getValue());
		}
		return os;
	}

	public static int getDirectionTypeIndex(String value) {
		Object[] objs = listDirectionType();
		int index = 0;
		for (Object obj : objs) {
			ComboItem item = (ComboItem) obj;
			if (item.getValue().equals(value)) {
				return index;
			}
			index++;
		}
		return 0;
	}

	public static IsoLangCode getLangByXml(String xml) {
		String n = xml;
		int index1 = StringUtils.lastIndexOf(xml, File.separator);
		if (index1 >= 0) {
			n = StringUtils.substring(xml, index1 + 1, xml.length());
		}
		n = n.replace("questions_", "");
		n = n.replace(".properties", "");
		// en_EN_v2
		String[] args = StringUtils.split(n, "_");
		if (args.length >= 3) {
			n = args[0] + "-" + args[1];
		} else {
			n = args[0];
		}
		for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
				.size(); i++) {
			IsoLangCode code = config.getLanguageCountry().getIsoLangCodes()
					.get(i);
			if (StringUtils.equals(code.getCode(), n)) {
				return code;
			}
		}
		return null;
	}

	public static IsoLangCode getLangByCode(String lanCode) {
		for (int i = 0; i < config.getLanguageCountry().getIsoLangCodes()
				.size(); i++) {
			IsoLangCode code = config.getLanguageCountry().getIsoLangCodes()
					.get(i);
			if (StringUtils.equals(code.getCode(), lanCode)) {
				return code;
			}
		}
		return null;
	}

	public static String getAnswerCode(String name) {
		for (int i = 0; i < config.getAnswerCode().getFields().size(); i++) {
			Field field = config.getAnswerCode().getFields().get(i);
			if (StringUtils.equalsIgnoreCase(name, field.getName())) {
				return field.getValue();
			}
		}
		return name;
	}
}
