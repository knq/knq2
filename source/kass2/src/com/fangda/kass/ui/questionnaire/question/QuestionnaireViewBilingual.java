package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import com.fangda.kass.KnqMain;
import com.fangda.kass.model.config.IsoLangCode;
import com.fangda.kass.model.questionnaire.CompleteStep;
import com.fangda.kass.model.questionnaire.CompleteSubStep;
import com.fangda.kass.model.questionnaire.FollowupQuestion;
import com.fangda.kass.model.questionnaire.Option;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.Revise;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BasePanel;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.lowagie.text.Font;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the questionnaire view in bilingual language
 * (English and Local language)
 * 
 * @author Jianguo Zhou, Shuang Ma, Fangfang Zhao, Jing Kong
 * 
 */
public class QuestionnaireViewBilingual extends BasePanel {

	private static final long serialVersionUID = 1L;
	private LinkedHashMap<String, Object> layoutMap;
	private LinkedHashMap<String, Object> groupMap;
	private QuestionnaireService service;
	private JTable InterviewLogTable;
	private JPanel revisePanel;
	public static int textArearows = 5;
	public static int textAreacolus = 36;

	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();
		this.setName("questionViewPanel");
	}

	public QuestionnaireViewBilingual(String name, KnqMain km) {
		super(name, km);
	}

	@Override
	public void initPanel() {
		service = new QuestionnaireService();
		layoutMap = new LinkedHashMap<String, Object>();
		groupMap = new LinkedHashMap<String, Object>();
	}

	@Override
	public JPanel createPanel() {
		// TODO Auto-generated method stub
		IsoLangCode isoLangCode = QuestionConstants
				.getLangByXml(Constants.QUESTIONNAIRE_XML);
		Questionnaire q = service.get();
		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel(bagLayout);

		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JLabel titleLabel1 = UIHelper.genLabel(
				q.getTitle() + " (" + q.getEn_title() + ")", true, 13);

		JLabel titleLabel2 = new JLabel("<HTML>" + mxResources.get("language")
				+ " : " + isoLangCode.getLocalLanguage() + " (" + " "
				+ q.getLanguageName() + ")" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ mxResources.get("version") + " : " + q.getVersion()
				+ " </HTML>");

		JLabel titleLabel3 = new JLabel("<HTML>" + mxResources.get("country")
				+ " : " + isoLangCode.getLocalCountry() + " (" + ""
				+ q.getCountryName() + ")" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				+ mxResources.get("questionniare.properties.culture") + " : "
				+ q.getCultureArea() + " (" + "" + q.getEn_cultureArea() + ")"
				+ "</HTML>");
		JLabel titleLabel4 = new JLabel("<HTML>" + mxResources.get("status")
				+ " : " + mxResources.get(q.getStatus()) + " (" + q.getStatus()
				+ ")" + "</HTML>");
		int index = 0;

		UIHelper.addToBagpanel(panel, Color.white, titleLabel1, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel2, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel3, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, titleLabel4, c, 0, index++,
				1, 1, GridBagConstraints.CENTER);

		List<Revise> revises = q.getRevises();
		if (revises.size() > 0) {
			JLabel titleLabel5 = new JLabel("<HTML>"
					+ mxResources.get("revisehistory") + " :</HTML>");
			UIHelper.addToBagpanel(panel, Color.white, titleLabel5, c, 0,
					index++, 1, 1, GridBagConstraints.CENTER);
			createVersionPanel(q);
			UIHelper.addToBagpanel(panel, Color.white, this.revisePanel, c, 0,
					index++, 1, 1, GridBagConstraints.CENTER);
		}
		UIHelper.addToBagpanel(panel, Color.white,
				UIHelper.genLabel("Preface", true, 13), c, 0, index++, 1, 1,
				GridBagConstraints.WEST);
		JTextArea noteJta = UIHelper.genTextArea(q.getNote(), 700);
		UIHelper.addToBagpanel(panel, Color.white, noteJta, c, 0, index++, 1,
				1, GridBagConstraints.CENTER);
		{
			JTextArea noteJtaB = UIHelper.genTextArea(q.getEn_note(), 700);
			UIHelper.addToBagpanel(panel, Color.white, noteJtaB, c, 0, index++,
					1, 1, GridBagConstraints.CENTER);
		}

		List<Subsection> subsections = q.getSection().getSubsections();

		int sectionIndex = 1;
		for (Subsection ss : subsections) {
			String sectionTitle = "Section" + sectionIndex + " "
					+ ss.getTitle();
			sectionTitle += " (" + ss.getEn_title() + ")";

			JLabel jt = new JLabel(sectionTitle);
			jt.setName("section_" + ss.getSid());
			UIHelper.createMultiLabel(jt, Font.BOLD, 13, 700);
			UIHelper.addToBagpanel(panel, Color.white, jt, c, 0, 1 + index, 1,
					1, GridBagConstraints.WEST);
			index++;

			// JTextArea subsectionJta = UIHelper.genTextArea(ss.getDetail(),
			// 700);
			String detail = ss.getDetail();
			detail += " (" + ss.getEn_detail() + ")";
			JLabel subsectionJta = new JLabel(detail);
			UIHelper.createMultiLabel(subsectionJta, 700);
			UIHelper.addToBagpanel(panel, Color.white, subsectionJta, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
			if (sectionIndex == 1) {
				index = this.addSection1Content(index, panel, c, ss, q);
			}
			// subsection's question
			if (ss.getQuestions() != null) {
				int questionIndex = 1;
				for (Question question : ss.getQuestions()) {
					index = this.addQuestion(panel, c, question, index,
							questionIndex);
					questionIndex++;
				}
			}
			if (ss.getSubSections() != null) {
				int subsectionIndex = 1;
				for (Subsection subsection : ss.getSubSections()) {
					String showTitle = "  " + sectionIndex + "."
							+ subsectionIndex + " ";
					showTitle = showTitle
							+ (subsection.getTitle() == null ? "" : subsection
									.getTitle());
					showTitle += " (" + subsection.getEn_title() + ")";

					// JLabel subsectionTitleLabel =
					// UIHelper.genLabel(showTitle, true, 11);
					JLabel jt1 = new JLabel(showTitle);
					jt1.setName("section_" + subsection.getSid());
					UIHelper.createMultiLabel(jt1, Font.BOLD, 11, 700);
					UIHelper.addToBagpanel(panel, Color.white, jt1, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;

					if (subsection.getDetail() != null) {
						detail = subsection.getDetail();
						detail += " (" + subsection.getEn_detail() + ")";
						JLabel snodeJta = new JLabel(detail);
						UIHelper.createMultiLabel(snodeJta, 700);
						UIHelper.addToBagpanel(panel, Color.white, snodeJta, c,
								0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;
					}

					int questionIndex = 1;
					for (Question question : subsection.getQuestions()) {
						index = this.addQuestion(panel, c, question, index,
								questionIndex);
						questionIndex++;
					}
					subsectionIndex++;
				}
			}

			sectionIndex++;
		}
		return panel;
	}

	/**
	 * Add the panel of the answer option
	 * 
	 * @param panel
	 * @param c
	 * @param question
	 * @param index
	 * @return
	 */
	public int addOption(JPanel panel, GridBagConstraints c, Question question,
			int index) {

		if ("A".equals(question.getType())) {// text
			if (question instanceof FollowupQuestion) {
				JTextField field = new JTextField(
						((FollowupQuestion) question).getLength());
				UIHelper.addToBagpanelFill(panel, Color.white, field, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST,
						GridBagConstraints.NONE);
			} else {
				JPanel jp = new JPanel();
				jp.setBackground(Color.white);
				UIHelper.addToBagpanel(panel, Color.white, jp, c, 0, 1 + index,
						1, 1, GridBagConstraints.WEST);
			}
			index++;
		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(textArearows, textAreacolus);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			UIHelper.addToBagpanelFill(panel, Color.white, notePane, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST,
					GridBagConstraints.BOTH);
			index++;
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();
					String optionTitleB = option.getEn_detail() == null ? ""
							: option.getEn_detail();
					optionTitle += "(" + optionTitleB + ")";
					if (("C".equals(question.getType()) && question instanceof FollowupQuestion)
							|| "Q".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;
					} else {
						optionTitle = "      □ " + optionTitle;
					}

					if (option.getNote() != null) {
						optionTitle += " Note:" + option.getNote() + " ";
					}
					if (option.getEn_note() != null) {
						optionTitle += " (" + option.getEn_note() + ")";
					}

					JLabel optionLabel = new JLabel(optionTitle);
					UIHelper.createMultiLabel(optionLabel, 650);
					UIHelper.addToBagpanel(panel, Color.white, optionLabel, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
			}
		}
		return index;
	}

	/**
	 * Add the panel of the answer option
	 * 
	 * @param panel
	 * @param question
	 */
	public void addPanelOption(JPanel panel, Question question) {
		if ("A".equals(question.getType())) {// text
			JTextField field = new JTextField(
					((FollowupQuestion) question).getLength());
			GridBagLayout bagLayout = new GridBagLayout();
			JPanel tieldPanel = new JPanel(bagLayout);

			tieldPanel.setBackground(Color.white);
			tieldPanel.add(field, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(0, 0, 0, 0), 0, 0));
			panel.add(tieldPanel);

		} else if ("T".equals(question.getType())) {
			JTextArea noteField = new JTextArea(textArearows, textAreacolus);
			noteField.setLineWrap(true);
			noteField.setWrapStyleWord(true);
			JScrollPane notePane = new JScrollPane(noteField);
			notePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			panel.add(noteField);
		} else {
			if (question.getOptions() != null) {
				for (Option option : question.getOptions()) {
					String optionTitle = option.getDetail() == null ? ""
							: option.getDetail();

					if ("C".equals(question.getType())) {// 单选
						optionTitle = "      ○ " + optionTitle;
					} else {
						optionTitle = "      □ " + optionTitle;
					}
					JLabel optionLabel = new JLabel(optionTitle);
					panel.add(optionLabel);
				}
			}
		}
	}

	/**
	 * Add the panel of the question
	 * 
	 * @param panel
	 * @param c
	 * @param question
	 * @param index
	 * @param questionIndex
	 * @return
	 */
	public int addQuestion(JPanel panel, GridBagConstraints c,
			Question question, int index, int questionIndex) {
		boolean isFollowup = false;
		if (question instanceof FollowupQuestion) {
			isFollowup = true;
		}
		boolean otherCondition = false;
		if (isFollowup) {
			FollowupQuestion fquestion = (FollowupQuestion) question;
			if (StringUtil.isNotBlank(fquestion.getGroupName())
					&& fquestion.getGroupName().length() > 0) {
				String parentQid = StringUtil.substringBeforeLast(
						question.getQid(), "_");
				String key = parentQid + fquestion.getGroupName();// groupMap
				JPanel jpanel = (JPanel) groupMap.get(key);
				if (jpanel == null) {
					jpanel = new JPanel();
					jpanel.setBackground(Color.white);
					jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));

					jpanel.setBorder(BorderFactory.createTitledBorder(fquestion
							.getGroupName()
							+ " ("
							+ fquestion.getEn_groupName() + ")"));
					groupMap.put(key, jpanel);
					UIHelper.addToBagpanel(panel, Color.white, jpanel, c, 0,
							1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
				if (StringUtil.isNotBlank(question.getDetail())) {
					JLabel optionLabel = UIHelper.genLabelwidth(
							question.getDetail(), false, 9, 460);
					;
					jpanel.add(optionLabel);
					{
						JLabel optionLabelB = UIHelper.genLabelwidth(
								question.getEn_detail(), false, 9, 460);
						jpanel.add(optionLabelB);
					}

					// JLabel titleLabel = UIHelper.genLabel("&nbsp;" +
					// questionTitle);
					// jpanel.add(titleLabel);
				} else {
					JLabel optionLabel = UIHelper.genLabelwidth("", false, 9,
							460);
					jpanel.add(optionLabel);
				}
				this.addPanelOption(jpanel, question);
			} else {
				if (isFollowup && fquestion.getLayout() != null
						&& fquestion.getLayout().length() > 0) {
					String parentQid = StringUtil.substringBeforeLast(
							question.getQid(), "_");
					String key = parentQid + fquestion.getLayout();
					JPanel jpanel = (JPanel) layoutMap.get(key);
					if (jpanel == null) {
						jpanel = new JPanel();
						jpanel.setBackground(Color.white);
						jpanel.setLayout(new BoxLayout(jpanel, BoxLayout.Y_AXIS));
						layoutMap.put(key, jpanel);
						UIHelper.addToBagpanel(panel, Color.white, jpanel, c,
								0, 1 + index, 1, 1, GridBagConstraints.WEST);
						index++;

						JLabel titleLabel = UIHelper.genLabel(
								question.getDetail(), false, 9);
						jpanel.add(titleLabel);
						this.addPanelOption(jpanel, question);
					} else {
						if (question.getDetail() != null
								&& !"".equals(question.getDetail())) {
							JLabel titleLabel = UIHelper.genLabel(question
									.getDetail());
							jpanel.add(titleLabel);
						} else {
							JLabel titleLabel = UIHelper.genLabel("", false, 9);
							jpanel.add(titleLabel);
						}
						this.addPanelOption(jpanel, question);
					}
				} else {
					otherCondition = true;
				}
			}
		} else {
			otherCondition = true;
		}

		if (otherCondition) {
			if (question.getTitle() != null) {
				String title = "";
				if (isFollowup) {
					title = question.getSortNo() + "." + question.getTitle();
				} else {
					title = question.getSortNo() + "." + question.getTitle();
					if ("G".equals(question.getDirection())) {
						title = title + "(Help given)";
					} else if ("R".equals(question.getDirection())) {
						title = title + "(Help received)";
					}
				}
				title += " (" + question.getEn_title() + ")";
				JLabel jta = new JLabel(title);
				jta.setName("section_" + question.getQid());
				UIHelper.createMultiLabel(jta, Font.BOLD, 11, 700);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;

			}
			if (question.getLead() != null) {
				String lead = question.getLead();
				lead += " (" + question.getEn_lead() + ")";

				JLabel jta = new JLabel(" " + lead);
				UIHelper.createMultiLabel(jta, 680);
				// JTextArea jta = UIHelper.genTextArea(" " +
				// question.getLead(), 680);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}
			if (question.getDetail() != null
					&& !question.getDetail().equals(question.getLead())) {
				String title = question.getDetail();
				title += " (" + question.getEn_detail() + ")";
				JLabel jta = new JLabel(" " + title);
				UIHelper.createMultiLabel(jta, 680);
				// JTextArea jta = UIHelper.genTextArea(" " +
				// question.getDetail(), 680);
				UIHelper.addToBagpanel(panel, Color.white, jta, c, 0,
						1 + index, 1, 1, GridBagConstraints.WEST);
				index++;
			}

			index = this.addOption(panel, c, question, index);
			if (question.getFollowupQuestions() != null) {
				int followupIndex = 1;
				for (FollowupQuestion fqestion : question
						.getFollowupQuestions()) {
					index = this.addQuestion(panel, c, fqestion, index,
							followupIndex);
					followupIndex++;
				}
			}
		}
		return index;
	}

	/**
	 * Generate the table of the revision history
	 * 
	 * @param q
	 * @return
	 */
	public JPanel createVersionPanel(Questionnaire q) {
		List<Revise> kilog = q.getRevises();
		String[] columnNames = { mxResources.get("No"),
				mxResources.get("questionniare.properties.version"),
				mxResources.get("reviseDate"), mxResources.get("reviser"),
				mxResources.get("note") };
		String[][] data = new String[kilog.size()][5];
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		int height = 20;
		for (int i = 0; i < kilog.size(); i++) {
			model.setValueAt(i + 1, i, 0);
			model.setValueAt(kilog.get(i).getVersion(), i, 1);
			model.setValueAt(kilog.get(i).getReviseDate(), i, 2);
			model.setValueAt(kilog.get(i).getReviser(), i, 3);
			model.setValueAt(kilog.get(i).getNote(), i, 4);
			height += 20;
		}

		InterviewLogTable = new JTable(model);
		InterviewLogTable.setAutoResizeMode(5);
		TableColumn column = null;
		for (int i = 0; i < 4; i++) {
			column = InterviewLogTable.getColumnModel().getColumn(i);
			if (i == 0) {
				column.setPreferredWidth(10);
			} else {
				column.setPreferredWidth(50);
			}
		}

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		InterviewLogTable.getColumn(mxResources.get("No")).setCellRenderer(
				render);
		DefaultTableCellRenderer render2 = new DefaultTableCellRenderer();
		render2.setHorizontalAlignment(SwingConstants.CENTER);
		InterviewLogTable.getColumn(
				mxResources.get("questionniare.properties.version"))
				.setCellRenderer(render2);
		JTableHeader tableHeader = InterviewLogTable.getTableHeader();
		tableHeader.setReorderingAllowed(false);
		DefaultTableCellRenderer hr = (DefaultTableCellRenderer) tableHeader
				.getDefaultRenderer();
		hr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

		JScrollPane scrollpane = new JScrollPane(InterviewLogTable);
		InterviewLogTable.setFillsViewportHeight(true);
		scrollpane.setPreferredSize(new Dimension(100, height));

		revisePanel = new JPanel();

		revisePanel.setLayout(new BoxLayout(revisePanel, BoxLayout.Y_AXIS));
		revisePanel.add(scrollpane);
		return revisePanel;
	}

	/**
	 * Section 1
	 */
	public int addSection1Content(int index, JPanel panel,
			GridBagConstraints c, Subsection ss, Questionnaire q) {
		StopRule stoprule = q.getDraw().getStopRule();
		String content = mxResources.get("stoppingRuleSetup") + " "
				+ stoprule.getStep() + " " + mxResources.get("stepLabel");

		content += ". (" + mxResources.get("questionniare.properties.note")
				+ ": " + stoprule.getNote() + ")";

		content += " (" + mxResources.get("questionniare.properties.note")
				+ ": " + stoprule.getEn_note() + ")";

		JLabel section1Jta = new JLabel(content);
		UIHelper.createMultiLabel(section1Jta, 700);
		UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
				1 + index, 1, 1, GridBagConstraints.WEST);
		index++;

		ImageIcon s1NKK = UIHelper.getImage("stoppingRuleImg.png");
		UIHelper.addImgToBagpanel(panel, Color.white, s1NKK, c, 0, 1 + index,
				1, 1, GridBagConstraints.CENTER);
		index++;
		JLabel s1NKKNote = new JLabel(
				"Figure1: A simplified example of diagram.", JLabel.CENTER);
		UIHelper.addToBagpanel(panel, Color.white, s1NKKNote, c, 0, 1 + index,
				1, 1, GridBagConstraints.CENTER);
		index++;

		section1Jta = new JLabel(mxResources.get("autoCompleteStep"));
		UIHelper.createMultiLabel(section1Jta, 700);
		UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
				1 + index, 1, 1, GridBagConstraints.WEST);
		index++;

		List<CompleteStep> steps = q.getDraw().getCompleteProcedure()
				.getCompleteStep();
		for (CompleteStep step : steps) {
			content = mxResources.get("stepLabel") + " " + step.getStepNo()
					+ " " + step.getTitle();

			content += " (";
			content += step.getEn_title();
			content += ")";

			section1Jta = new JLabel(content);
			UIHelper.createMultiLabel(section1Jta, 700);
			UIHelper.addToBagpanel(panel, Color.white, section1Jta, c, 0,
					1 + index, 1, 1, GridBagConstraints.WEST);
			index++;
			List<CompleteSubStep> substeps = step.getSubSteps();
			for (CompleteSubStep subStep : substeps) {
				if (subStep.getKinshipTerm() != null) {
					content = "      ○ " + subStep.getKinshipTerm();
					content += " ("
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex())) + ")";
					section1Jta = new JLabel(content);

					UIHelper.createMultiLabel(section1Jta, 650);

					UIHelper.addToBagpanel(panel, Color.white, section1Jta, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				} else {
					content = "      ○ "
							+ QuestionConstants.getSexString(String
									.valueOf(subStep.getSex()));
					section1Jta = new JLabel(content);

					UIHelper.createMultiLabel(section1Jta, 650);

					UIHelper.addToBagpanel(panel, Color.white, section1Jta, c,
							0, 1 + index, 1, 1, GridBagConstraints.WEST);
					index++;
				}
			}
		}
		return index;
	}
}
