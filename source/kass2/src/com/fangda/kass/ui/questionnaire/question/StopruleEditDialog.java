package com.fangda.kass.ui.questionnaire.question;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.questionnaire.Draw;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.mxgraph.util.mxResources;

/**
 * This Class for the dialog of the stop rule edit in the QMS
 * 
 * @author Fangfang Zhao
 * 
 */
public class StopruleEditDialog extends BaseDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel setupStepLabel;
	JLabel stepLabel;
	JLabel noteLabel;

	JComboBox stepField;
	JTextArea noteField;
	ImageIcon stoppingRuleNKKImg;
	QuestionnaireService service;

	public void initDialog() {
		this.setTitle(mxResources.get("stoppingRuleSetup.title"));
		service = new QuestionnaireService();
	}

	public JPanel createPanel() {
		setupStepLabel = new JLabel();
		stepLabel = new JLabel();
		noteLabel = new JLabel();

		Integer[] ruleArray = new Integer[10];
		for (int i = 1; i < 11; i++) {
			ruleArray[i - 1] = Integer.valueOf(i);
		}
		stepField = new JComboBox(ruleArray);
		noteField = new JTextArea("", 5, 61);
		noteField.setLineWrap(true);
		noteField.setWrapStyleWord(true);
		noteField.setBackground(Color.white);
		stoppingRuleNKKImg = UIHelper.getImage("stoppingRuleImg.png");

		GridBagLayout bagLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		JPanel panel = new JPanel(bagLayout);

		JPanel panel1 = new JPanel(bagLayout);
		UIHelper.addToBagpanel(panel1, Color.gray,
				new JLabel(mxResources.get("stoppingRuleSetup.detail") + ":"),
				c, 0, 0, 1, 1, GridBagConstraints.EAST);
		UIHelper.addToBagpanel(panel1, Color.gray, stepField, c, 2, 0, 2, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(panel1, Color.gray,
				new JLabel(mxResources.get("stoppingStep") + "."), c, 4, 0, 2,
				1, GridBagConstraints.EAST);

		UIHelper.addToBagpanel(panel, Color.gray, panel1, c, 0, 0, 4, 1,
				GridBagConstraints.NORTH);

		JPanel panel2 = new JPanel(bagLayout);

		panel2.setBorder(BorderFactory.createTitledBorder(mxResources
				.get("note") + ":"));
		UIHelper.addToBagpanel(panel2, Color.gray, noteField, c, 0, 0, 1, 1,
				GridBagConstraints.NORTH);
		UIHelper.addImgToBagpanel(panel2, Color.gray, stoppingRuleNKKImg, c, 0,
				1, 1, 1, GridBagConstraints.SOUTH);

		UIHelper.addToBagpanel(panel, Color.gray, panel2, c, 0, 3, 5, 1,
				GridBagConstraints.SOUTH);

		return panel;
	}

	@Override
	public void afterShow() {
		// TODO Auto-generated method stub
		super.afterShow();
		Draw draw = service.get().getDraw();
		StopRule stopRule = draw.getStopRule();

		noteField.setText(stopRule.getNote());
		stepField.setSelectedItem(stopRule.getStep());
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		// TODO Auto-generated method stub
		Draw draw = service.get().getDraw();
		StopRule stopRule = draw.getStopRule();

		stopRule.setNote(noteField.getText());
		stopRule.setStep(Integer.parseInt(stepField.getSelectedItem()
				.toString()));
		service.save(service.get());
		return new MessageModel(true, "Submit success!");
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}
}

/**
 * public MessageModel submit() { StopRule sr = service.get();
 * sr.setNote(noteField.getText()); if(stepField.getSelectedItem() != null) {
 * q.setStep(stepField.getSelectedItem().toString()); }
 * 
 * service.save(q); return new MessageModel(true, "Submit success!"); }
 * 
 * public void afterShow() { Questionnaire q = service.get(); StopRule sr =
 * service.get(); stepField.setSelectedItem(q.getStep());
 * noteField.setText(q.getNote()); } }
 */
