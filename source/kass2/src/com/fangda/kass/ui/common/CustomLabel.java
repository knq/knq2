package com.fangda.kass.ui.common;

import javax.swing.JLabel;

/**
 * The Class for the Custom Label
 * 
 * @author Fangfang Zhao
 * 
 */
public class CustomLabel extends JLabel {

	public CustomLabel(String text) {
		super(text);
	}

	/**
	 * the serial Version UID
	 */
	private static final long serialVersionUID = 7330336262316323524L;
	
	private Object attr;

	public Object getAttr() {
		return attr;
	}

	public void setAttr(Object attr) {
		this.attr = attr;
	}
	

}
