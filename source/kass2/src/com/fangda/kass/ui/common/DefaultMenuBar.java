package com.fangda.kass.ui.common;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.questionnaire.Question;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.model.questionnaire.StopRule;
import com.fangda.kass.model.questionnaire.Subsection;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.service.workspace.WorkSpaceService;
import com.fangda.kass.ui.common.DefaultActions.OpenMapQuestionAction;
import com.fangda.kass.ui.common.DefaultActions.SaveAction;
import com.fangda.kass.ui.common.DefaultActions.SaveAsAction;
import com.fangda.kass.ui.interview.IvAction.CloseInterviewAction;
import com.fangda.kass.ui.interview.IvAction.CloseProjectAction;
import com.fangda.kass.ui.interview.IvAction.DeleteAction;
import com.fangda.kass.ui.interview.IvAction.EditInterviewAction;
import com.fangda.kass.ui.interview.IvAction.EditProjectAction;
import com.fangda.kass.ui.interview.IvAction.ExitKassAction;
import com.fangda.kass.ui.interview.IvAction.ExportInterviewAction;
import com.fangda.kass.ui.interview.IvAction.InterviewLogAction;
import com.fangda.kass.ui.interview.IvAction.NewInterviewPropEditAction;
import com.fangda.kass.ui.interview.IvAction.NewProjectAction;
import com.fangda.kass.ui.interview.IvAction.OpenInterviewAction;
import com.fangda.kass.ui.interview.IvAction.OpenProjectAction;
import com.fangda.kass.ui.interview.IvAction.OpenRecentInterviewAction;
import com.fangda.kass.ui.interview.IvAction.OpenRecentProjectAction;
import com.fangda.kass.ui.interview.IvAction.PageSetUpAction;
import com.fangda.kass.ui.interview.IvAction.PrintViewAction;
import com.fangda.kass.ui.interview.IvAction.RedoAction;
import com.fangda.kass.ui.interview.IvAction.RengeAction;
import com.fangda.kass.ui.interview.IvAction.UndoAction;
import com.fangda.kass.ui.interview.IvAction.ZoomAction;
import com.fangda.kass.ui.interview.IvAction.returnInterviewAction;
import com.fangda.kass.ui.interview.IvAction.showByRangeAction;
import com.fangda.kass.ui.questionnaire.QnAction.AboutKNQAction;
import com.fangda.kass.ui.questionnaire.QnAction.ExportMarticesModelData;
import com.fangda.kass.ui.questionnaire.QnAction.ExportNKKModelData;
import com.fangda.kass.ui.questionnaire.QnAction.NewLocalQ;
import com.fangda.kass.ui.questionnaire.QnAction.OpenLocalQ;
import com.fangda.kass.ui.questionnaire.QnAction.PreviewAction;
import com.fangda.kass.ui.questionnaire.QnAction.PreviewBilingualAction;
import com.fangda.kass.ui.questionnaire.QnAction.PreviewDetailAction;
import com.fangda.kass.ui.questionnaire.QnAction.QEditAction;
import com.fangda.kass.ui.questionnaire.QnAction.QExportAction;
import com.fangda.kass.ui.questionnaire.QnAction.QImportAction;
import com.fangda.kass.ui.questionnaire.QnAction.QTemplateAction;
import com.fangda.kass.ui.questionnaire.QnAction.QuestionnaireCloseAction;
import com.fangda.kass.ui.questionnaire.QnAction.QuestionnaireLoadAction;
import com.fangda.kass.ui.questionnaire.QnAction.QuestionnaireLocalizeAction;
import com.fangda.kass.ui.questionnaire.QnAction.ReleaseAction;
import com.fangda.kass.ui.questionnaire.QnAction.ViewForTable10;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKIndependentPaths;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKKDistanceNumPath;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKKShortPathsTerm;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKKinshipTerm;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKPersonbyUnionMatrixSexAction;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKPersonbyUnionMatrixTypeAction;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKShortPaths;
import com.fangda.kass.ui.questionnaire.QnAction.ViewNKKSimplePathsAction;
import com.mxgraph.analysis.StructuralException;
import com.mxgraph.analysis.mxAnalysisGraph;
import com.mxgraph.analysis.mxGraphProperties;
import com.mxgraph.analysis.mxGraphProperties.GraphType;
import com.mxgraph.analysis.mxGraphStructure;
import com.mxgraph.analysis.mxTraversal;
import com.mxgraph.costfunction.mxCostFunction;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxResources;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxGraphView;

/**
 * The Class for creating the menu bar.
 * 
 * @author Fangfang Zhao
 * 
 */
public class DefaultMenuBar extends JMenuBar {
	
	private QuestionnaireService qService;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4060203894740766714L;

	public enum AnalyzeType {
		IS_CONNECTED, IS_SIMPLE, IS_CYCLIC_DIRECTED, IS_CYCLIC_UNDIRECTED, COMPLEMENTARY, REGULARITY, COMPONENTS, MAKE_CONNECTED, MAKE_SIMPLE, IS_TREE, ONE_SPANNING_TREE, IS_DIRECTED, GET_CUT_VERTEXES, GET_CUT_EDGES, GET_SOURCES, GET_SINKS, PLANARITY, IS_BICONNECTED, GET_BICONNECTED, SPANNING_TREE, FLOYD_ROY_WARSHALL
	}

	public DefaultMenuBar(final KnqMain editor) {

		JMenu menu = null;
		JMenu submenu = null;
		JMenu fileMenu; 
		JMenu editMenu;
		JMenu interviewMenu;
		JMenu viewMenu;
		JMenu davdMenu;
		JMenu localizeMenu;
		JMenu questionnaireMenu;

		// Creates the file menu
		menu = add( fileMenu = new JMenu(mxResources.get("file")));
		fileMenu.setEnabled(true);

		menu.add(editor.bind(mxResources.get("newProject"), new NewProjectAction(), "/images/new.gif"));
		menu.add(editor.bind(mxResources.get("newInterview"), new NewInterviewPropEditAction(), "/images/new.gif"));
		menu.add(editor.bind(mxResources.get("openProject"), new OpenProjectAction(), "/images/open.gif"));
		submenu = (JMenu) menu.add(new JMenu(mxResources.get("openRecentProject")));
		
		List<String> recentProjectPathList = WorkSpaceService.getRecentProjectPathList();
				//Collections.reverse(recentProjectPathList);
			  for(int i=0;i<recentProjectPathList.size();i++){
				  submenu.add(editor.bind(recentProjectPathList.get(i), new OpenRecentProjectAction(),"/images/open.gif"));
		    }
		
		
		menu.add(editor.bind(mxResources.get("closeProject"), new CloseProjectAction()));
		
		menu.add(editor.bind(mxResources.get("openInterview"), new OpenInterviewAction(), "/images/open.gif"));
		
		submenu = (JMenu) menu.add(new JMenu(mxResources.get("openRecentInterview")));
		List<String> recentInterviewPathList = WorkSpaceService.getRecentInterviewPathList();
		for(int i=0;i<recentInterviewPathList.size();i++){
		submenu.add(editor.bind(recentInterviewPathList.get(i), new OpenRecentInterviewAction(),"/images/open.gif"));
    }
		menu.add(editor.bind(mxResources.get("closeInterview"), new CloseInterviewAction()));
		
		menu.add(editor.bind(mxResources.get("questionnaireLoad"), new QuestionnaireLoadAction()));
		menu.add(editor.bind(mxResources.get("questionnaireClose"), new QuestionnaireCloseAction()));
		menu.addSeparator();

		menu.add(editor.bind(mxResources.get("save"), new SaveAction(), "/images/save.gif"));
	 	menu.add(editor.bind(mxResources.get("saveAs"), new SaveAsAction(), "/images/saveas.gif"));
	//	menu.add(editor.bind(mxResources.get("saveAll"), new SaveAction(true), "/images/saveas.gif"));

		menu.addSeparator();

		menu.add(editor.bind(mxResources.get("projectProperties"), new EditProjectAction()));
		menu.add(editor.bind(mxResources.get("interviewProperties"), new EditInterviewAction()));
		//menu.add(editor.bind(mxResources.get("questionnaireProperties"), new EditQuestionnaireAction()));
		menu.addSeparator();

		menu.add(editor.bind(mxResources.get("pageLayout"), new PageSetUpAction()));
		menu.add(editor.bind(mxResources.get("printView"), new PrintViewAction()));
		menu.add(editor.bind(mxResources.get("export.interview"), new ExportInterviewAction()));
		menu.addSeparator();
		menu.add(editor.bind(mxResources.get("exit"), new ExitKassAction()));

		// Creates the edit menu
		menu = add(editMenu = new JMenu(mxResources.get("edit")));
		editMenu.setEnabled(true);
		
		menu.add(editor.bind(mxResources.get("undo"), new UndoAction(),
				"/images/undo.gif"));
		menu.add(editor.bind(mxResources.get("redo"), new RedoAction(),
				"/images/redo.gif"));

		menu.addSeparator();

		// menu.add(editor.bind(mxResources.get("cut"), new
		// ColorAction("Fillcolor",
		// mxConstants.STYLE_FILLCOLOR),"/images/cut.gif"));
		// menu.add(editor.bind(mxResources.get("copy"), new
		// ColorAction("Fillcolor",
		// mxConstants.STYLE_FILLCOLOR),"/images/copy.gif"));
		// menu.add(editor.bind(mxResources.get("paste"), new
		// ColorAction("Fillcolor",
		// mxConstants.STYLE_FILLCOLOR),"/images/paste.gif"));
		menu.add(editor.bind(mxResources.get("delete"), new DeleteAction(),
				"/images/delete.gif"));

		menu = add(interviewMenu =  new JMenu(mxResources.get("interview")));
		interviewMenu.setEnabled(true);

		menu.add(editor.bind(mxResources.get("interviewWindows"), new returnInterviewAction()));
		menu.add(editor.bind(mxResources.get("interviewProperties"), new EditInterviewAction()));
		menu.add(editor.bind(mxResources.get("interviewLog"), new InterviewLogAction()));

		// Creates the View menu
		menu = add(viewMenu = new JMenu(mxResources.get("view")));
		viewMenu.setEnabled(true);

		menu.add(editor.bind(mxResources.get("zoomIn"), new ZoomAction(2)));
		menu.add(editor.bind(mxResources.get("zoomOut"), new ZoomAction(1)));
		menu.add(editor.bind(mxResources.get("fitInWindow"), new ZoomAction(3)));
		menu.add(editor.bind(mxResources.get("zoomactual"), new ZoomAction(4)));
		menu.add(editor.bind(mxResources.get("autoArrange"), new RengeAction()));
		
		
		
		if(qService == null) {
			qService = new QuestionnaireService();
		}
		Questionnaire q  = qService.getQuestionnaire();
		if(q!=null){
		submenu = (JMenu) menu.add(new JMenu(mxResources.get("range")));
		StopRule stoprule = q.getDraw().getStopRule();
		int stopStep = stoprule.getStep();
			  for(int i=1;i<stopStep+1;i++){
				  submenu.add(editor.bind(""+i, new showByRangeAction(i)));
		    }
		}
		
		// Creates the DAVD menu
		menu = add(davdMenu = new JMenu(mxResources.get("dataConvert")));
		davdMenu.setEnabled(true);

		JMenu viewNKKMatrixMenu = (JMenu) menu.add(new JMenu(mxResources.get("viewNKKMatrix")));
		viewNKKMatrixMenu.setEnabled(true);
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKPersonbyUnionMatrixType"), new ViewNKKPersonbyUnionMatrixTypeAction()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKPersonbyUnionMatrixSex"), new ViewNKKPersonbyUnionMatrixSexAction()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKSimplePaths"), new ViewNKKSimplePathsAction()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKIndependentPaths"), new ViewNKKIndependentPaths()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKShortPaths"), new ViewNKKShortPaths()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKKinshipTerm"), new ViewNKKKinshipTerm()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKKShortPathsTerm"), new ViewNKKKShortPathsTerm()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewNKKKDistanceNumPath"), new ViewNKKKDistanceNumPath()));
		viewNKKMatrixMenu.add(editor.bind(mxResources.get("ViewAllPersonbyUnionMatrixType"), new ViewForTable10 ()));
		
//		JMenu viewqMatrixMenu = (JMenu) menu.add(new JMenu(mxResources.get("viewQuestionMatrix"))); 
//		viewqMatrixMenu.setEnabled(true);
//		viewqMatrixMenu.add(editor.bind(mxResources.get("ViewPersonByVariable"), new ViewPersonByVariable()));
//		viewqMatrixMenu.add(editor.bind(mxResources.get("ViewUnionByVariable"), new ViewUnionByVariable()));
//		menu.addSeparator();
		
		/* 20160117 modify by  zhangyu delete  ExportNKKMatrix Menu 
		JMenu exportNKKMatrixMenu = (JMenu) menu.add(new JMenu(mxResources.get("ExportNKKMatrix"))); 
		exportNKKMatrixMenu.setEnabled(true);
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKPersonbyUnionMatrixType"), new ExportNKKPersonbyUnionMatrixType()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKPersonbyUnionMatrixSex"), new ExportNKKPersonbyUnionMatrixSex()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKSimplePaths"), new ExportNKKSimplePaths()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKIndependentPaths"), new ExportNKKIndependentPaths()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKShortPaths"), new ExportNKKShortPaths()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKKinshipTerm"), new ExportNKKKinshipTerm()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKShortPathsTerm"), new ExportNKKShortPathsTerm()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKDistanceNumPath"), new ExportNKKDistanceNumPath()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportAllPersonbyUnionMatrixType"), new ExportAllPersonbyUnionMatrixType()));
		exportNKKMatrixMenu.add(editor.bind(mxResources.get("ExportNKKallMatricesforCurrentInterview"), new ExportNKKallMatricesforCurrentInterview()));
     20160117 modify by  zhangyu delete  ExportNKKMatrix Menu */ 
		
		menu.addSeparator();
		
		JMenu exportQMatrixMenu = (JMenu) menu.add(new JMenu(mxResources.get("OutputMatrices"))); 
		exportQMatrixMenu.setEnabled(true);
		exportQMatrixMenu.add(editor.bind(mxResources.get("ExportNKKallMatrices"), new ExportNKKModelData()));
		exportQMatrixMenu.add(editor.bind(mxResources.get("ExportAllCaseMatrix"), new ExportMarticesModelData()));
		
		
		// Creates the Questionnaire menu
		
		menu = add(questionnaireMenu = new JMenu(mxResources.get("questionnaire")));
		questionnaireMenu.setEnabled(true);

		menu.add(editor.bind(mxResources.get("questionnaireView"), new PreviewAction()));
		menu.add(editor.bind(mxResources.get("questionnaireViewProperties"), new PreviewDetailAction()));
		JMenuItem qViewBilingualMenu = menu.add(editor.bind(mxResources.get("questionnaireViewbilingual"), new PreviewBilingualAction()));	
		qViewBilingualMenu.setEnabled(true);

		menu.addSeparator();
		
		menu.add(editor.bind(mxResources.get("questionnaireLocalize"), new QuestionnaireLocalizeAction()));
		menu.add(editor.bind(mxResources.get("questionnaireRevise"), new QEditAction()));
		menu.add(editor.bind(mxResources.get("templateRevise"), new QTemplateAction()));
		menu.addSeparator();
		
		menu.add(editor.bind(mxResources.get("reversionRelease"), new ReleaseAction()));
		menu.add(editor.bind(mxResources.get("questionExport"), new QExportAction()));
//		menu.add(editor.bind(mxResources.get("questionImport"), new QImportAction()));


		// Creates the localization menu
		menu = add(localizeMenu = new JMenu(mxResources.get("localization")));
		localizeMenu.setEnabled(true);
		menu.add(editor.bind(mxResources.get("openLocalQ"), new OpenLocalQ()));
		
		menu.add(editor.bind(mxResources.get("newLocalQ"), new NewLocalQ()));
		menu.add(editor.bind(mxResources.get("importLocaLQ"), new QImportAction()));
//		menu.addSeparator();
		
//		JMenuItem editLocalM = menu.add(editor.bind(mxResources.get("editLocalM"), new HistoryAction(true)));
//  	editLocalM.setEnabled(false);
//		JMenuItem editLocalC = menu.add(editor.bind(mxResources.get("editLocalC"), new HistoryAction(true)));
//		editLocalC.setEnabled(false);
//		menu.addSeparator();
		
//		JMenuItem setLocalVersion = menu.add(editor.bind(mxResources.get("setLocalVersion"), new SystemFonfigMessageAction()));		
		
		// Creates the Help menu
		menu = add(new JMenu(mxResources.get("help")));
		menu.add(editor.bind(mxResources.get("aboutKNQ2"), new AboutKNQAction()));
	}

	/**
	 * Adds menu items to the given shape menu. This is factored out because the
	 * shape menu appears in the menubar and also in the popupmenu.
	 */
	public static void populateShapeMenu(JMenu menu, BasicGraphEditor editor) {
		// menu.add(editor.bind(mxResources.get("home"),
		// mxGraphActions.getHomeAction(), "/images/house.gif"));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("exitGroup"),
		// mxGraphActions.getExitGroupAction(), "/images/up.gif"));
		// menu.add(editor.bind(mxResources.get("enterGroup"),
		// mxGraphActions.getEnterGroupAction(), "/images/down.gif"));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("group"),
		// mxGraphActions.getGroupAction(), "/images/group.gif"));
		// menu.add(editor.bind(mxResources.get("ungroup"),
		// mxGraphActions.getUngroupAction(), "/images/ungroup.gif"));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("removeFromGroup"),
		// mxGraphActions.getRemoveFromParentAction()));
		//
		// menu.add(editor.bind(mxResources.get("updateGroupBounds"),
		// mxGraphActions.getUpdateGroupBoundsAction()));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("collapse"),
		// mxGraphActions.getCollapseAction(), "/images/collapse.gif"));
		// menu.add(editor.bind(mxResources.get("expand"),
		// mxGraphActions.getExpandAction(), "/images/expand.gif"));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("toBack"),
		// mxGraphActions.getToBackAction(), "/images/toback.gif"));
		// menu.add(editor.bind(mxResources.get("toFront"),
		// mxGraphActions.getToFrontAction(), "/images/tofront.gif"));
		//
		// menu.addSeparator();
		//
		// JMenu submenu = (JMenu) menu.add(new
		// JMenu(mxResources.get("align")));
		//
		// submenu.add(editor.bind(mxResources.get("left"), new
		// AlignCellsAction(
		// mxConstants.ALIGN_LEFT), "/images/alignleft.gif"));
		// submenu.add(editor.bind(mxResources.get("center"),
		// new AlignCellsAction(mxConstants.ALIGN_CENTER),
		// "/images/aligncenter.gif"));
		// submenu.add(editor.bind(mxResources.get("right"), new
		// AlignCellsAction(
		// mxConstants.ALIGN_RIGHT), "/images/alignright.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("top"), new AlignCellsAction(
		// mxConstants.ALIGN_TOP), "/images/aligntop.gif"));
		// submenu.add(editor.bind(mxResources.get("middle"),
		// new AlignCellsAction(mxConstants.ALIGN_MIDDLE),
		// "/images/alignmiddle.gif"));
		// submenu.add(editor.bind(mxResources.get("bottom"),
		// new AlignCellsAction(mxConstants.ALIGN_BOTTOM),
		// "/images/alignbottom.gif"));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("autosize"), new
		// AutosizeAction()));

	}

	/**
	 * Adds menu items to the given format menu. This is factored out because
	 * the format menu appears in the menubar and also in the popupmenu.
	 */
	public static void populateFormatMenu(JMenu menu, BasicGraphEditor editor) {
		// JMenu submenu = (JMenu) menu.add(new JMenu(mxResources
		// .get("background")));
		//
		// submenu.add(editor.bind(mxResources.get("fillcolor"), new
		// ColorAction(
		// "Fillcolor", mxConstants.STYLE_FILLCOLOR),
		// "/images/fillcolor.gif"));
		// submenu.add(editor.bind(mxResources.get("gradient"), new ColorAction(
		// "Gradient", mxConstants.STYLE_GRADIENTCOLOR)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("image"),
		// new PromptValueAction(mxConstants.STYLE_IMAGE, "Image")));
		// submenu.add(editor.bind(mxResources.get("shadow"), new ToggleAction(
		// mxConstants.STYLE_SHADOW)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("opacity"),
		// new PromptValueAction(mxConstants.STYLE_OPACITY,
		// "Opacity (0-100)")));
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("label")));
		//
		// submenu.add(editor.bind(mxResources.get("fontcolor"), new
		// ColorAction(
		// "Fontcolor", mxConstants.STYLE_FONTCOLOR),
		// "/images/fontcolor.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("labelFill"), new
		// ColorAction(
		// "Label Fill", mxConstants.STYLE_LABEL_BACKGROUNDCOLOR)));
		// submenu.add(editor.bind(mxResources.get("labelBorder"),
		// new ColorAction("Label Border",
		// mxConstants.STYLE_LABEL_BORDERCOLOR)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("rotateLabel"),
		// new ToggleAction(mxConstants.STYLE_HORIZONTAL, true)));
		//
		// submenu.add(editor.bind(mxResources.get("textOpacity"),
		// new PromptValueAction(mxConstants.STYLE_TEXT_OPACITY,
		// "Opacity (0-100)")));
		//
		// submenu.addSeparator();
		//
		// JMenu subsubmenu = (JMenu) submenu.add(new JMenu(mxResources
		// .get("position")));
		//
		// subsubmenu.add(editor.bind(mxResources.get("top"),
		// new SetLabelPositionAction(mxConstants.ALIGN_TOP,
		// mxConstants.ALIGN_BOTTOM)));
		// subsubmenu.add(editor.bind(mxResources.get("middle"),
		// new SetLabelPositionAction(mxConstants.ALIGN_MIDDLE,
		// mxConstants.ALIGN_MIDDLE)));
		// subsubmenu.add(editor.bind(mxResources.get("bottom"),
		// new SetLabelPositionAction(mxConstants.ALIGN_BOTTOM,
		// mxConstants.ALIGN_TOP)));
		//
		// subsubmenu.addSeparator();
		//
		// subsubmenu.add(editor.bind(mxResources.get("left"),
		// new SetLabelPositionAction(mxConstants.ALIGN_LEFT,
		// mxConstants.ALIGN_RIGHT)));
		// subsubmenu.add(editor.bind(mxResources.get("center"),
		// new SetLabelPositionAction(mxConstants.ALIGN_CENTER,
		// mxConstants.ALIGN_CENTER)));
		// subsubmenu.add(editor.bind(mxResources.get("right"),
		// new SetLabelPositionAction(mxConstants.ALIGN_RIGHT,
		// mxConstants.ALIGN_LEFT)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("wordWrap"),
		// new KeyValueAction(mxConstants.STYLE_WHITE_SPACE, "wrap")));
		// submenu.add(editor.bind(mxResources.get("noWordWrap"),
		// new KeyValueAction(mxConstants.STYLE_WHITE_SPACE, null)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("hide"), new ToggleAction(
		// mxConstants.STYLE_NOLABEL)));
		//
		// menu.addSeparator();
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("line")));
		//
		// submenu.add(editor.bind(mxResources.get("linecolor"), new
		// ColorAction(
		// "Linecolor", mxConstants.STYLE_STROKECOLOR),
		// "/images/linecolor.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("orthogonal"),
		// new ToggleAction(mxConstants.STYLE_ORTHOGONAL)));
		// submenu.add(editor.bind(mxResources.get("dashed"), new ToggleAction(
		// mxConstants.STYLE_DASHED)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("linewidth"),
		// new PromptValueAction(mxConstants.STYLE_STROKEWIDTH,
		// "Linewidth")));
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("connector")));
		//
		// submenu.add(editor.bind(mxResources.get("straight"),
		// new SetStyleAction("straight"), "/images/straight.gif"));
		//
		// submenu.add(editor.bind(mxResources.get("horizontal"),
		// new SetStyleAction(""), "/images/connect.gif"));
		// submenu.add(editor.bind(mxResources.get("vertical"),
		// new SetStyleAction("vertical"), "/images/vertical.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("entityRelation"),
		// new SetStyleAction("edgeStyle=mxEdgeStyle.EntityRelation"),
		// "/images/entity.gif"));
		// submenu.add(editor.bind(mxResources.get("arrow"), new SetStyleAction(
		// "arrow"), "/images/arrow.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("plain"), new ToggleAction(
		// mxConstants.STYLE_NOEDGESTYLE)));
		//
		// menu.addSeparator();
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("linestart")));
		//
		// submenu.add(editor.bind(mxResources.get("open"), new KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OPEN),
		// "/images/open_start.gif"));
		// submenu.add(editor.bind(mxResources.get("classic"), new
		// KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.ARROW_CLASSIC),
		// "/images/classic_start.gif"));
		// submenu.add(editor.bind(mxResources.get("block"), new KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.ARROW_BLOCK),
		// "/images/block_start.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("diamond"), new
		// KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.ARROW_DIAMOND),
		// "/images/diamond_start.gif"));
		// submenu.add(editor.bind(mxResources.get("oval"), new KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OVAL),
		// "/images/oval_start.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("none"), new KeyValueAction(
		// mxConstants.STYLE_STARTARROW, mxConstants.NONE)));
		// submenu.add(editor.bind(mxResources.get("size"), new
		// PromptValueAction(
		// mxConstants.STYLE_STARTSIZE, "Linestart Size")));
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("lineend")));
		//
		// submenu.add(editor.bind(mxResources.get("open"), new KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OPEN),
		// "/images/open_end.gif"));
		// submenu.add(editor.bind(mxResources.get("classic"), new
		// KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.ARROW_CLASSIC),
		// "/images/classic_end.gif"));
		// submenu.add(editor.bind(mxResources.get("block"), new KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK),
		// "/images/block_end.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("diamond"), new
		// KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.ARROW_DIAMOND),
		// "/images/diamond_end.gif"));
		// submenu.add(editor.bind(mxResources.get("oval"), new KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.ARROW_OVAL),
		// "/images/oval_end.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("none"), new KeyValueAction(
		// mxConstants.STYLE_ENDARROW, mxConstants.NONE)));
		// submenu.add(editor.bind(mxResources.get("size"), new
		// PromptValueAction(
		// mxConstants.STYLE_ENDSIZE, "Lineend Size")));
		//
		// menu.addSeparator();
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("alignment")));
		//
		// submenu.add(editor.bind(mxResources.get("left"), new KeyValueAction(
		// mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT),
		// "/images/left.gif"));
		// submenu.add(editor.bind(mxResources.get("center"), new
		// KeyValueAction(
		// mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER),
		// "/images/center.gif"));
		// submenu.add(editor.bind(mxResources.get("right"), new KeyValueAction(
		// mxConstants.STYLE_ALIGN, mxConstants.ALIGN_RIGHT),
		// "/images/right.gif"));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("top"), new KeyValueAction(
		// mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP),
		// "/images/top.gif"));
		// submenu.add(editor.bind(mxResources.get("middle"), new
		// KeyValueAction(
		// mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE),
		// "/images/middle.gif"));
		// submenu.add(editor.bind(mxResources.get("bottom"), new
		// KeyValueAction(
		// mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM),
		// "/images/bottom.gif"));
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("spacing")));
		//
		// submenu.add(editor.bind(mxResources.get("top"), new
		// PromptValueAction(
		// mxConstants.STYLE_SPACING_TOP, "Top Spacing")));
		// submenu.add(editor.bind(mxResources.get("right"),
		// new PromptValueAction(mxConstants.STYLE_SPACING_RIGHT,
		// "Right Spacing")));
		// submenu.add(editor.bind(mxResources.get("bottom"),
		// new PromptValueAction(mxConstants.STYLE_SPACING_BOTTOM,
		// "Bottom Spacing")));
		// submenu.add(editor.bind(mxResources.get("left"), new
		// PromptValueAction(
		// mxConstants.STYLE_SPACING_LEFT, "Left Spacing")));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("global"),
		// new PromptValueAction(mxConstants.STYLE_SPACING, "Spacing")));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(
		// mxResources.get("sourceSpacing"),
		// new PromptValueAction(
		// mxConstants.STYLE_SOURCE_PERIMETER_SPACING, mxResources
		// .get("sourceSpacing"))));
		// submenu.add(editor.bind(
		// mxResources.get("targetSpacing"),
		// new PromptValueAction(
		// mxConstants.STYLE_TARGET_PERIMETER_SPACING, mxResources
		// .get("targetSpacing"))));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("perimeter"),
		// new PromptValueAction(mxConstants.STYLE_PERIMETER_SPACING,
		// "Perimeter Spacing")));
		//
		// submenu = (JMenu) menu.add(new JMenu(mxResources.get("direction")));
		//
		// submenu.add(editor.bind(mxResources.get("north"), new KeyValueAction(
		// mxConstants.STYLE_DIRECTION, mxConstants.DIRECTION_NORTH)));
		// submenu.add(editor.bind(mxResources.get("east"), new KeyValueAction(
		// mxConstants.STYLE_DIRECTION, mxConstants.DIRECTION_EAST)));
		// submenu.add(editor.bind(mxResources.get("south"), new KeyValueAction(
		// mxConstants.STYLE_DIRECTION, mxConstants.DIRECTION_SOUTH)));
		// submenu.add(editor.bind(mxResources.get("west"), new KeyValueAction(
		// mxConstants.STYLE_DIRECTION, mxConstants.DIRECTION_WEST)));
		//
		// submenu.addSeparator();
		//
		// submenu.add(editor.bind(mxResources.get("rotation"),
		// new PromptValueAction(mxConstants.STYLE_ROTATION,
		// "Rotation (0-360)")));
		//
		// menu.addSeparator();
		//
		// menu.add(editor.bind(mxResources.get("rounded"), new ToggleAction(
		// mxConstants.STYLE_ROUNDED)));
		//
		// menu.add(editor.bind(mxResources.get("style"), new StyleAction()));
	}

	/**
	 *
	 */
	public static class InsertGraph extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4010463992665008365L;

		/**
		 * 
		 */
		protected GraphType graphType;

		protected mxAnalysisGraph aGraph;

		/**
		 * @param aGraph
		 * 
		 */
		public InsertGraph(GraphType tree, mxAnalysisGraph aGraph) {
			this.graphType = tree;
			this.aGraph = aGraph;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();

				// dialog = new FactoryConfigDialog();
				String dialogText = "";
				if (graphType == GraphType.NULL)
					dialogText = "Configure null graph";
				else if (graphType == GraphType.COMPLETE)
					dialogText = "Configure complete graph";
				else if (graphType == GraphType.NREGULAR)
					dialogText = "Configure n-regular graph";
				else if (graphType == GraphType.GRID)
					dialogText = "Configure grid graph";
				else if (graphType == GraphType.BIPARTITE)
					dialogText = "Configure bipartite graph";
				else if (graphType == GraphType.COMPLETE_BIPARTITE)
					dialogText = "Configure complete bipartite graph";
				else if (graphType == GraphType.BFS_DIR)
					dialogText = "Configure BFS algorithm";
				else if (graphType == GraphType.BFS_UNDIR)
					dialogText = "Configure BFS algorithm";
				else if (graphType == GraphType.DFS_DIR)
					dialogText = "Configure DFS algorithm";
				else if (graphType == GraphType.DFS_UNDIR)
					dialogText = "Configure DFS algorithm";
				else if (graphType == GraphType.DIJKSTRA)
					dialogText = "Configure Dijkstra's algorithm";
				else if (graphType == GraphType.BELLMAN_FORD)
					dialogText = "Configure Bellman-Ford algorithm";
				else if (graphType == GraphType.MAKE_TREE_DIRECTED)
					dialogText = "Configure make tree directed algorithm";
				else if (graphType == GraphType.KNIGHT_TOUR)
					dialogText = "Configure knight's tour";
				else if (graphType == GraphType.GET_ADJ_MATRIX)
					dialogText = "Configure adjacency matrix";
				else if (graphType == GraphType.FROM_ADJ_MATRIX)
					dialogText = "Input adjacency matrix";
				else if (graphType == GraphType.PETERSEN)
					dialogText = "Configure Petersen graph";
				else if (graphType == GraphType.WHEEL)
					dialogText = "Configure Wheel graph";
				else if (graphType == GraphType.STAR)
					dialogText = "Configure Star graph";
				else if (graphType == GraphType.PATH)
					dialogText = "Configure Path graph";
				else if (graphType == GraphType.FRIENDSHIP_WINDMILL)
					dialogText = "Configure Friendship Windmill graph";
				else if (graphType == GraphType.INDEGREE)
					dialogText = "Configure indegree analysis";
				else if (graphType == GraphType.OUTDEGREE)
					dialogText = "Configure outdegree analysis";
				GraphConfigDialog dialog = new GraphConfigDialog(graphType, dialogText);
				dialog.configureLayout(graph, graphType, aGraph);
				dialog.setModal(true);
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension frameSize = dialog.getSize();
				dialog.setLocation(screenSize.width / 2 - (frameSize.width / 2), screenSize.height / 2
						- (frameSize.height / 2));
				dialog.setVisible(true);
			}
		}
	}

	/**
	 *
	 */
	public static class AnalyzeGraph extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6926170745240507985L;

		mxAnalysisGraph aGraph;

		/**
		 * 
		 */
		protected AnalyzeType analyzeType;

		/**
		 * Examples for calling analysis methods from mxGraphStructure
		 */
		public AnalyzeGraph(AnalyzeType analyzeType, mxAnalysisGraph aGraph) {
			this.analyzeType = analyzeType;
			this.aGraph = aGraph;
		}

		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof mxGraphComponent) {
				mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
				mxGraph graph = graphComponent.getGraph();

				if (analyzeType == AnalyzeType.IS_CONNECTED) {
					boolean isConnected = mxGraphStructure.isConnected(aGraph);

					if (isConnected) {
						System.out.println("The graph is connected");
					} else {
						System.out.println("The graph is not connected");
					}
				} else if (analyzeType == AnalyzeType.IS_SIMPLE) {
					boolean isSimple = mxGraphStructure.isSimple(aGraph);

					if (isSimple) {
						System.out.println("The graph is simple");
					} else {
						System.out.println("The graph is not simple");
					}
				} else if (analyzeType == AnalyzeType.IS_CYCLIC_DIRECTED) {
					boolean isCyclicDirected = mxGraphStructure.isCyclicDirected(aGraph);

					if (isCyclicDirected) {
						System.out.println("The graph is cyclic directed");
					} else {
						System.out.println("The graph is acyclic directed");
					}
				} else if (analyzeType == AnalyzeType.IS_CYCLIC_UNDIRECTED) {
					boolean isCyclicUndirected = mxGraphStructure.isCyclicUndirected(aGraph);

					if (isCyclicUndirected) {
						System.out.println("The graph is cyclic undirected");
					} else {
						System.out.println("The graph is acyclic undirected");
					}
				} else if (analyzeType == AnalyzeType.COMPLEMENTARY) {
					graph.getModel().beginUpdate();

					mxGraphStructure.complementaryGraph(aGraph);

					mxGraphStructure.setDefaultGraphStyle(aGraph, true);
					graph.getModel().endUpdate();
				} else if (analyzeType == AnalyzeType.REGULARITY) {
					try {
						int regularity = mxGraphStructure.regularity(aGraph);
						System.out.println("Graph regularity is: " + regularity);
					} catch (StructuralException e1) {
						System.out.println("The graph is irregular");
					}
				} else if (analyzeType == AnalyzeType.COMPONENTS) {
					Object[][] components = mxGraphStructure.getGraphComponents(aGraph);
					mxIGraphModel model = aGraph.getGraph().getModel();

					for (int i = 0; i < components.length; i++) {
						System.out.print("Component " + i + " :");

						for (int j = 0; j < components[i].length; j++) {
							System.out.print(" " + model.getValue(components[i][j]));
						}

						System.out.println(".");
					}

					System.out.println("Number of components: " + components.length);

				} else if (analyzeType == AnalyzeType.MAKE_CONNECTED) {
					graph.getModel().beginUpdate();

					if (!mxGraphStructure.isConnected(aGraph)) {
						mxGraphStructure.makeConnected(aGraph);
						mxGraphStructure.setDefaultGraphStyle(aGraph, false);
					}

					graph.getModel().endUpdate();
				} else if (analyzeType == AnalyzeType.MAKE_SIMPLE) {
					mxGraphStructure.makeSimple(aGraph);
				} else if (analyzeType == AnalyzeType.IS_TREE) {
					boolean isTree = mxGraphStructure.isTree(aGraph);

					if (isTree) {
						System.out.println("The graph is a tree");
					} else {
						System.out.println("The graph is not a tree");
					}
				} else if (analyzeType == AnalyzeType.ONE_SPANNING_TREE) {
					try {
						graph.getModel().beginUpdate();
						aGraph.getGenerator().oneSpanningTree(aGraph, true, true);
						mxGraphStructure.setDefaultGraphStyle(aGraph, false);
						graph.getModel().endUpdate();
					} catch (StructuralException e1) {
						System.out.println("The graph must be simple and connected");
					}
				} else if (analyzeType == AnalyzeType.IS_DIRECTED) {
					boolean isDirected = mxGraphProperties.isDirected(aGraph.getProperties(),
							mxGraphProperties.DEFAULT_DIRECTED);

					if (isDirected) {
						System.out.println("The graph is directed.");
					} else {
						System.out.println("The graph is undirected.");
					}
				} else if (analyzeType == AnalyzeType.GET_CUT_VERTEXES) {
					Object[] cutVertices = mxGraphStructure.getCutVertices(aGraph);

					System.out.print("Cut vertices of the graph are: [");
					mxIGraphModel model = aGraph.getGraph().getModel();

					for (int i = 0; i < cutVertices.length; i++) {
						System.out.print(" " + model.getValue(cutVertices[i]));
					}

					System.out.println(" ]");
				} else if (analyzeType == AnalyzeType.GET_CUT_EDGES) {
					Object[] cutEdges = mxGraphStructure.getCutEdges(aGraph);

					System.out.print("Cut edges of the graph are: [");
					mxIGraphModel model = aGraph.getGraph().getModel();

					for (int i = 0; i < cutEdges.length; i++) {
						System.out.print(" "
								+ Integer.parseInt((String) model.getValue(aGraph.getTerminal(cutEdges[i], true)))
								+ "-"
								+ Integer.parseInt((String) model.getValue(aGraph.getTerminal(cutEdges[i], false))));
					}

					System.out.println(" ]");
				} else if (analyzeType == AnalyzeType.GET_SOURCES) {
					try {
						Object[] sourceVertices = mxGraphStructure.getSourceVertices(aGraph);
						System.out.print("Source vertices of the graph are: [");
						mxIGraphModel model = aGraph.getGraph().getModel();

						for (int i = 0; i < sourceVertices.length; i++) {
							System.out.print(" " + model.getValue(sourceVertices[i]));
						}

						System.out.println(" ]");
					} catch (StructuralException e1) {
						System.out.println(e1);
					}
				} else if (analyzeType == AnalyzeType.GET_SINKS) {
					try {
						Object[] sinkVertices = mxGraphStructure.getSinkVertices(aGraph);
						System.out.print("Sink vertices of the graph are: [");
						mxIGraphModel model = aGraph.getGraph().getModel();

						for (int i = 0; i < sinkVertices.length; i++) {
							System.out.print(" " + model.getValue(sinkVertices[i]));
						}

						System.out.println(" ]");
					} catch (StructuralException e1) {
						System.out.println(e1);
					}
				} else if (analyzeType == AnalyzeType.PLANARITY) {
					// TODO implement
				} else if (analyzeType == AnalyzeType.IS_BICONNECTED) {
					boolean isBiconnected = mxGraphStructure.isBiconnected(aGraph);

					if (isBiconnected) {
						System.out.println("The graph is biconnected.");
					} else {
						System.out.println("The graph is not biconnected.");
					}
				} else if (analyzeType == AnalyzeType.GET_BICONNECTED) {
					// TODO implement
				} else if (analyzeType == AnalyzeType.SPANNING_TREE) {
					// TODO implement
				} else if (analyzeType == AnalyzeType.FLOYD_ROY_WARSHALL) {

					ArrayList<Object[][]> FWIresult = new ArrayList<Object[][]>();
					try {
						// only this line is needed to get the result from
						// Floyd-Roy-Warshall, the rest is code for displaying
						// the result
						FWIresult = mxTraversal.floydRoyWarshall(aGraph);

						Object[][] dist = FWIresult.get(0);
						Object[][] paths = FWIresult.get(1);
						Object[] vertices = aGraph.getChildVertices(aGraph.getGraph().getDefaultParent());
						int vertexNum = vertices.length;
						System.out.println("Distances are:");

						for (int i = 0; i < vertexNum; i++) {
							System.out.print("[");

							for (int j = 0; j < vertexNum; j++) {
								System.out.print(" " + Math.round((Double) dist[i][j] * 100.0) / 100.0);
							}

							System.out.println("] ");
						}

						System.out.println("Path info:");

						mxCostFunction costFunction = aGraph.getGenerator().getCostFunction();
						mxGraphView view = aGraph.getGraph().getView();

						for (int i = 0; i < vertexNum; i++) {
							System.out.print("[");

							for (int j = 0; j < vertexNum; j++) {
								if (paths[i][j] != null) {
									System.out.print(" " + costFunction.getCost(view.getState(paths[i][j])));
								} else {
									System.out.print(" -");
								}
							}

							System.out.println(" ]");
						}

						try {
							Object[] path = mxTraversal.getWFIPath(aGraph, FWIresult, vertices[0],
									vertices[vertexNum - 1]);
							System.out.print("The path from " + costFunction.getCost(view.getState(vertices[0]))
									+ " to " + costFunction.getCost((view.getState(vertices[vertexNum - 1]))) + " is:");

							for (int i = 0; i < path.length; i++) {
								System.out.print(" " + costFunction.getCost(view.getState(path[i])));
							}

							System.out.println();
						} catch (StructuralException e1) {
							System.out.println(e1);
						}
					} catch (StructuralException e2) {
						System.out.println(e2);
					}
				}
			}
		}
	};
	/**
	 * Get the sub menu of the Section 3
	 */
	public void getMapSubmenus(KnqMain editor, JMenu menu) {
		if(qService == null) {
			qService = new QuestionnaireService();
		}
		Subsection subsection = qService.searchSubsection("SS_003");
		List<Question> questions = subsection.getQuestions();
		//
		for(int i = 0; i < questions.size(); i++) {
			Question question = questions.get(i);
			menu.add(editor.bind(question.getTitle(), new OpenMapQuestionAction(question)));
			//question.getQid();
			//question.getTitle();
		}
	}
};