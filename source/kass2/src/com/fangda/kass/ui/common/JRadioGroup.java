package com.fangda.kass.ui.common;

import javax.swing.ButtonGroup;

public class JRadioGroup extends ButtonGroup {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1427309284412486213L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
