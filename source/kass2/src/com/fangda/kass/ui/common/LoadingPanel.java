package com.fangda.kass.ui.common;

import java.awt.BorderLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

 /**
 * The class for Loading panel
 * 
 * @author Fangfang Zhao
 * 
 */
public class LoadingPanel extends JPanel {

    private static final long serialVersionUID = 1962748329465603630L;

    private String mesg = null;

    public LoadingPanel(String mesg) {
        this.mesg = mesg;
        initUI();
        interceptInput();
        setOpaque(false);
        setVisible(false);
    }

    private void initUI() {
        JLabel label = new JLabel(mesg);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setIcon(new ImageIcon(getClass().getResource("/images/loading.gif")));

        setLayout(new BorderLayout());
        add(label, BorderLayout.CENTER);
    }

    private void interceptInput() {
        addMouseListener(new MouseAdapter() {});
        addMouseMotionListener(new MouseMotionAdapter() {});
        addKeyListener(new KeyAdapter() {});

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                requestFocusInWindow();
            }
        });
        setFocusTraversalKeysEnabled(false);
    }
}