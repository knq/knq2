package com.fangda.kass.ui.common;

import java.awt.Font;

import javax.swing.JLabel;

import com.fangda.kass.ui.UIHelper;

/**
 * The Class for the MultiLabel.
 * 
 * @author Fangfang Zhao
 * 
 */
public class MultiLabel {

	private String originText = "";

	private CustomLabel label;

	private int fontBold = 0;

	private int fontSize = 0;

	public MultiLabel(String content) {
		this.originText = content;
		this.label = new CustomLabel(content);
		this.label.setAttr(this);
	}

	public MultiLabel(String content, int fontBold, int fontSize) {
		this.originText = content;
		this.fontBold = fontBold;
		this.fontSize = fontSize;
		this.label = new CustomLabel(content);
		this.label.setFont(new Font(Font.SANS_SERIF, fontBold, fontSize));
		this.label.setAttr(this);
	}

	public void resize(int width) {
		//this.label.repaint();
		this.label.validate();
		this.label.setText(originText);
		if (fontBold > 0) {
			this.label.setFont(new Font(Font.SANS_SERIF, fontBold, fontSize));
			UIHelper.createMultiLabel(this.label, fontBold, fontSize, width);
		} else {
			UIHelper.createMultiLabel(this.label, width);
		}
	}
	
	public void update(String text, int width) {
		this.label.validate();
		this.originText = text;
		this.label.setText(originText);
		if (fontBold > 0) {
			this.label.setFont(new Font(Font.SANS_SERIF, fontBold, fontSize));
			UIHelper.createMultiLabel(this.label, fontBold, fontSize, width);
		} else {
			UIHelper.createMultiLabel(this.label, width);
		}
	}

	public JLabel getLabel() {
		return this.label;
	}
}
