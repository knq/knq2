package com.fangda.kass.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.table.TableColumn;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This class for Setting the UI of the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public class UIHelper {

	public static int FONTSIZE = 10;
	public static String MAIN_BACK_COLOR = "";

	/**
	 * Set the style of tree
	 * 
	 * @param cellRenderer
	 */
	public static void setTreeRender(DefaultTreeCellRenderer cellRenderer) {
		// 设置节点及打开关闭图标
		cellRenderer.setLeafIcon(new ImageIcon(""));
		cellRenderer.setOpenIcon(UIHelper.getImage("openIcon.gif"));
		cellRenderer.setClosedIcon(UIHelper.getImage("closedIcon.gif"));
		// 设置字体.
		// cellRenderer.setFont(new Font("宋体",Font.PLAIN,12));
		// 设置选时或者不选时，背景的变化颜色
		cellRenderer.setBackgroundNonSelectionColor(Color.white);
		cellRenderer.setBackgroundSelectionColor(Color.yellow);
		// 设置边框颜色
		cellRenderer.setBorderSelectionColor(Color.red);
		// 设置选时或者不选时，文字的变化颜色
		cellRenderer.setTextNonSelectionColor(Color.black);
		cellRenderer.setTextSelectionColor(Color.blue);
	}

	/**
	 * BasPanel
	 * 
	 * @param panel
	 * @param c
	 *            GridBagConstraints
	 * @param gridx
	 *            x轴所在的格子序号
	 * @param gridy
	 *            y轴所在的格子序号
	 * @param gridwidth
	 *            此格子所占的列数
	 * @param gridheight
	 *            此格子所占的行数
	 * @param anthor
	 *            位置居中/居左/居右
	 */
	public static void addToBagpanel(JPanel panel, Color color,
			JComponent component, GridBagConstraints c, int gridx, int gridy,
			int gridwidth, int gridheight, int anthor) {
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = gridwidth;
		c.gridheight = gridheight;
		c.anchor = anthor;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5, 5, 5, 5);

		panel.add(component, c);
	}

	/**
	 * BasPanel
	 * 
	 * @param panel
	 * @param c
	 *            GridBagConstraints
	 * @param gridx
	 *            x轴所在的格子序号
	 * @param gridy
	 *            y轴所在的格子序号
	 * @param gridwidth
	 *            此格子所占的列数
	 * @param gridheight
	 *            此格子所占的行数
	 * @param anthor
	 *            位置居中/居左/居右
	 */
	public static void addToBagpanelFill(JPanel panel, Color color,
			JComponent component, GridBagConstraints c, int gridx, int gridy,
			int gridwidth, int gridheight, int anthor, int fillStyle) {
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = gridwidth;
		c.gridheight = gridheight;
		c.anchor = anthor;
		c.fill = fillStyle;
		c.insets = new Insets(5, 5, 5, 5);

		panel.add(component, c);
	}

	/**
	 * BasPanel添加image
	 * 
	 * @authorMS 20140723
	 */
	public static void addImgToBagpanel(JPanel panel, Color color,
			ImageIcon img, GridBagConstraints c, int gridx, int gridy,
			int gridwidth, int gridheight, int anthor) {
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = gridwidth;
		c.gridheight = gridheight;
		c.anchor = anthor;
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(5, 5, 5, 5);
		panel.add(new JLabel(img), c);
	}

	/**
	 * 设置页面边框
	 * 
	 * @authorMS 20140723
	 */

	public static void addBorder(JComponent component, int top, int left,
			int bottom, int right, Color color) {
		component.setBorder(BorderFactory.createMatteBorder(top, left, bottom,
				right, color));

	}

	/**
	 * 弹出对话框
	 * 
	 * @param c
	 * @param mm
	 * @return
	 */
	public static int showMessageDialog(JComponent c, MessageModel mm) {
		int result = 0;
		if (mm.isSuccess()) {
			result = JOptionPane.showConfirmDialog(c, mm.getMessage(),
					mxResources.get("common.success.title"),
					JOptionPane.PLAIN_MESSAGE);
		} else {
			result = JOptionPane.showConfirmDialog(c, mm.getMessage(),
					mxResources.get("common.failure.title"),
					JOptionPane.PLAIN_MESSAGE);
		}
		return result;
	}

	public static int calPanelHeight(String str, JComponent component, int width) {
		if (StringUtil.isBlank(str)) {
			return 0;
		}
		FontMetrics fontMetrics = component.getFontMetrics(component.getFont());
		byte[] bytes = str.getBytes();
		int fontSize = fontMetrics.bytesWidth(bytes, 0,
				str.getBytes().length - 1);
		int lineSize = (fontSize / width) + 1;
		int lineHeight = fontMetrics.getHeight() + 1;
		int height = lineSize * lineHeight;
		return height;
	}

	public static JLabel genLabel(String note) {
		return genLabel(note, false);
	}

	public static JLabel genLabel(String note, boolean isBold) {
		return genLabel(note, isBold, FONTSIZE);
	}

	public static JLabel genLabel(String note, boolean isBold, int fontSize) {
		return genLabel(note, isBold, fontSize, "#000000");
	}

	public static JLabel genLabel(String note, boolean isBold, int fontSize,
			String color) {
		StringBuffer sb = new StringBuffer();
		StringBuffer styleBuffer = new StringBuffer("style='");
		styleBuffer.append("font-size:" + fontSize + "px;");
		if (isBold) {
			styleBuffer.append("font-weight:bold;");
		} else {
			styleBuffer.append("font-weight:normal;");
		}
		styleBuffer.append("font-color:" + color + ";");
		styleBuffer.append("'");
		sb.append("<html>");
		sb.append("<body ").append(styleBuffer.toString()).append(">");
		sb.append(note);
		sb.append("</body>");
		sb.append("</html>");
		JLabel label = new JLabel(sb.toString());
		//
		// if (component instanceof JLabel) {
		// javax.swing.text.View v =
		// javax.swing.plaf.basic.BasicHTML.createHTMLView(component,
		// ((JLabel) component).getText());
		//
		// if (((JLabel) component).getText().length() > 100) {
		// v.setSize(682, Integer.MAX_VALUE);
		// int h = (int) v.getMinimumSpan(View.Y_AXIS); // 取得的高度
		// ((JLabel) component).setPreferredSize(new Dimension(682, h));
		// gridheight = h;
		// }
		// }

		return label;
	}

	public static JLabel genLabelwidth(String note, boolean isBold,
			int fontSize, int width) {

		StringBuffer sb = new StringBuffer();
		StringBuffer styleBuffer = new StringBuffer("style='");
		styleBuffer.append("width:" + width + "px;");
		styleBuffer.append("font-size:" + fontSize + "px;");
		if (isBold) {
			styleBuffer.append("font-weight:bold;");
		} else {
			styleBuffer.append("font-weight:normal;");
		}
		styleBuffer.append("font-color:#000000;");
		styleBuffer.append("'");
		sb.append("<html>");
		sb.append("<body ").append(styleBuffer.toString()).append(">");
		sb.append(note);
		sb.append("</body>");
		sb.append("</html>");
		JLabel label = new JLabel(sb.toString());
		return label;
	}

	/**
	 * toolbar格式设置
	 * 
	 * @author MS
	 */
	public static void toolBarStyle(JToolBar jtoolbar, int style) {
		jtoolbar.setFloatable(false);
		jtoolbar.setOrientation(style);
		jtoolbar.setBackground(Color.white);
		jtoolbar.setRollover(true);
	}

	/**
	 * TextArea格式设置
	 * 
	 * @author MS20140730
	 */
	public static void setTextAreaFormat(JTextArea[] jtextarea) {
		for (int i = 0, n = jtextarea.length; i < n; i++) {
			jtextarea[i].setEditable(false);
			jtextarea[i].setLineWrap(true);
			jtextarea[i].setWrapStyleWord(true);
		}
	}

	/**
	 * TextArea非数组格式，可编辑状态
	 * 
	 * @author MS20140805
	 */
	public static void setTextAreaFormat(JTextArea jtextarea) {
		jtextarea.setLineWrap(true);
		jtextarea.setWrapStyleWord(true);
	}

	/**
	 * 自动计算高度
	 * 
	 * @param title
	 * @param width
	 */
	public static JTextArea genTextArea(String text, int width) {
		JTextArea jta = new JTextArea(text);
		int height = UIHelper.calPanelHeight(text, jta, width);
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		jta.setEditable(false);
		jta.setPreferredSize(new Dimension(width, height));
		return jta;
	}

	/**
	 * 滚动条格式设置
	 * 
	 * @author MS20140730
	 */
	public static void setJScrollPaneFormat(JScrollPane[] jScrollPane) {
		for (int i = 0, n = jScrollPane.length; i < n; i++) {
			jScrollPane[i]
					.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			jScrollPane[i]
					.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		}
	}

	/**
	 * 按钮样式设置
	 * 
	 * @author MS20140730
	 */
	public static void setJButtonStyle(JButton[] jButton) {
		for (int i = 0, n = jButton.length; i < n; i++) {
			jButton[i].setBackground(Color.white);
			jButton[i].setSize(150, 50);
		}
	}
	
	public static int getLineHeight(String text, int width) {
		if(text == null || text.length() == 0) {
			return 1;
		}
		List<String> lines = new ArrayList<String>();
		PlainDocument doc = new PlainDocument();
        try {
            doc.insertString(0, text, null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        JLabel label = new JLabel();
        FontMetrics fontMetrics = label.getFontMetrics(label.getFont());
        Element root = doc.getDefaultRootElement();
        for (int i = 0, j = root.getElementCount(); i < j; i++) {
            wrap(lines, root.getElement(i), fontMetrics, width);
        }
        int lineHeight = fontMetrics.getHeight() + 2;
        int height = (lines.size() + 1) * lineHeight;
        return height;
	}

	/**
	 * 创建多行文本
	 * 
	 * @param jabel
	 */
	public static void createMultiLabel(JLabel jlabel, int width) {
		if (jlabel.getText() == null) {
			return;
		}
		
		List<String> lines = new ArrayList<String>();
        String text = jlabel.getText();
        PlainDocument doc = new PlainDocument();
        try {
            doc.insertString(0, text, null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        FontMetrics fontMetrics = jlabel.getFontMetrics(jlabel.getFont());
        Element root = doc.getDefaultRootElement();
        for (int i = 0, j = root.getElementCount(); i < j; i++) {
            wrap(lines, root.getElement(i), fontMetrics, width);
        }
        int lineHeight = fontMetrics.getHeight() + 2;
        int height = (lines.size() + 1) * lineHeight;
		jlabel.setSize(new Dimension(width, height));
		StringBuffer sb = new StringBuffer("<html>");
		for(String line: lines) {
			sb.append(line).append("<br/>");
		}
		sb.append("</html>");
		jlabel.setText(sb.toString());
	}
	
	protected static void wrap(List<String> lines, Element elem, FontMetrics fontMetrics, int width) {
        int p1 = elem.getEndOffset();
        Document doc = elem.getDocument();
        for (int p0 = elem.getStartOffset(); p0 < p1;) {
            int p = calculateBreakPosition(doc, p0, p1, fontMetrics, width);
            try {
                lines.add(doc.getText(p0, p - p0));
            } catch (BadLocationException e) {
                throw new Error("Can't get line text. p0=" + p0 + " p=" + p);
            }
            p0 = (p == p0) ? p1 : p;
        }
    }
	
	/**
     * Calculate the position on which to break (wrap) the line.
     * 
     * @param doc
     *            the document
     * @param p0
     *            start position
     * @param p1
     *            end position
     * @return the actual end position, will be <code>p1</code> if content does
     *         not need to wrap, otherwise it will be less than <code>p1</code>.
     */
    protected static int calculateBreakPosition(Document doc, int p0, int p1, FontMetrics fontMetrics, int width) {
        Segment segment = SegmentCache.getSegment();
        try {
            doc.getText(p0, p1 - p0, segment);
        } catch (BadLocationException e) {
            throw new Error("Can't get line text");
        }

        int p = p0
                + Utilities.getBreakLocation(segment, fontMetrics, 0, width, null,
                        p0);
        SegmentCache.releaseSegment(segment);
        return p;
    }

	public static void createMultiLabel(JLabel jlabel, int fontBold,
			int fontSize, int width) {

		if (jlabel.getText() == null) {
			return;
		}
		jlabel.setFont(new Font(Font.SANS_SERIF, fontBold, fontSize));
		createMultiLabel(jlabel, width);
	}

	public static ImageIcon getImage(String imageName) {
		return new ImageIcon(UIHelper.class.getResource("/images/" + imageName));
	}

	public static String getPath(String imageName) {
		return (UIHelper.class.getResource("/images/" + imageName)).toString();
	}

	public static void changeTable(JTable table, int width, int dialogWidth) {
		TableColumn column = null;
		int allWidth = 0;
		for(int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
			column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth(width);
			allWidth = allWidth + width;
		}
		if(allWidth > dialogWidth) {
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		} else {
			table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}
	}
	
	/**
     * Static singleton {@link Segment} cache.
     * 
     * @see javax.swing.text.SegmentCache
     * 
     * @author Samuel Sjoberg
     */
    protected static final class SegmentCache {

        /** Reused segments. */
        private ArrayList<Segment> segments = new ArrayList<Segment>(2);

        /** Singleton instance. */
        private static SegmentCache cache = new SegmentCache();

        /** Private constructor. */
        private SegmentCache() {
        }

        /**
         * Returns a <code>Segment</code>. When done, the <code>Segment</code>
         * should be recycled by invoking {@link #releaseSegment(Segment)}.
         * 
         * @return a <code>Segment</code>.
         */
        public static Segment getSegment() {
            int size = cache.segments.size();
            if (size > 0) {
                return cache.segments.remove(size - 1);
            }
            return new Segment();
        }

        /**
         * Releases a <code>Segment</code>. A segment should not be used after
         * it is released, and a segment should never be released more than
         * once.
         */
        public static void releaseSegment(Segment segment) {
            segment.array = null;
            segment.count = 0;
            cache.segments.add(segment);
        }
    }
}
