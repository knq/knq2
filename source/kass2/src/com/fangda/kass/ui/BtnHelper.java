package com.fangda.kass.ui;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.mxgraph.util.mxResources;

/**
 * This class for Setting the UI of the buttons of the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public class BtnHelper {

	public static JButton createAddButton(String text) {
		return createImageButton(text,mxResources.get("addBtnTip"), "add.png", "", null);
	}
	public static JButton createAddButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("addBtnTip"), "add.png", command, act);
	}
	
	public static JButton createConfigButton(String text) {
		return createImageButton(text,mxResources.get("configBtnTip"), "config.gif", "", null);
	}
	public static JButton createConfigButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("configBtnTip"), "config.gif", command, act);
	}
	
	public static JButton createUpButton(String text) {
		return createImageButton(text,mxResources.get("upBtnTip"), "up.gif", "", null);
	}
	public static JButton createUpButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("upBtnTip"), "up.gif", command, act);
	}
	
	public static JButton createDownButton(String text) {
		return createImageButton(text,mxResources.get("downBtnTip"), "down.gif", "", null);
	}
	public static JButton createDownButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("downBtnTip"), "down.gif", command, act);
	}
	
	public static JButton createSkipConditionButton(String text) {
		return createImageButton(text,mxResources.get("skipConditionBtnTip"), "skipCondition.png", "", null);
	}
	public static JButton createSkipConditionButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("skipConditionBtnTip"), "skipCondition.png", command, act);
	}
	
	public static JButton createCopyButton(String text) {
		return createImageButton(text,mxResources.get("paste"), "copyIcon.png", "", null);
	}
	
	public static JButton createDisableButton(String text) {
		return createImageButton(text,mxResources.get("paste"), "disable.png", "", null);
	}
	
	public static JButton createDeleteButton(String text) {
		return createImageButton(text,mxResources.get("deleteBtnTip"), "deleteIcon.png", "", null);
	}
	public static JButton createDeleteButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("deleteBtnTip"), "deleteIcon.png", command, act);
	}
	
	public static JButton createMaleButton(String text) {
		return createImageButton(text,mxResources.get("paste"), "male.png", "", null);
	}
	public static JButton createMaleButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("paste"), "male.png", command, act);
	}
	
	public static JButton createFemaleButton(String text) {
		return createImageButton(text,mxResources.get("paste"), "female.png", "", null);
	}
	public static JButton createFemaleButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("paste"), "female.png", command, act);
	}
	
	public static JButton createUnionButton(String text) {
		return createImageButton(text,mxResources.get("paste"), "hline.png", "", null);
	}
	public static JButton createUnionButton(String text, String command, ActionListener act) {
		return createImageButton(text,mxResources.get("paste"), "hline.png", command, act);
	}
	public static JButton createImageButton(String text,String btnTip, String icon, String command, ActionListener act) {
		JButton button = new JButton(text);
		button.setActionCommand(command);
		button.addActionListener(act);
		button.setIcon(new ImageIcon(BtnHelper.class.getResource("/images/" + icon)));
		button.setToolTipText(btnTip);
		return button;
	}
}
