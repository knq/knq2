package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTable;
import javax.swing.JTextField;
//import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KASS;
//import com.fangda.kass.model.questionnaire.Questionnaire;
//import com.fangda.kass.service.WordTools;
import com.fangda.kass.service.davd.KnnGenerImp8;
import com.fangda.kass.service.interview.KassService;
//import com.fangda.kass.service.interview.ProjectService;
//import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
//import com.fangda.kass.ui.questionnaire.export.ExportBialigualWord;
//import com.fangda.kass.ui.questionnaire.export.ExportLocalWord;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.ExportCSVUtils;
//import com.lowagie.text.DocumentException;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of exporting the Matrix Table 8 (Measures of
 * connectedness between Ego and each network member) of the interview XML file by
 * DAVD.
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ExportNKKKDistanceNumPathDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5467235327911693514L;

	public static Logger logger = LoggerFactory
			.getLogger(ExportNKKKDistanceNumPathDialog.class);

	KnnGenerImp8 knngenerImpl8;
	JTextField questionnaireMDirectorField;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources.get("ExportNKKKDistanceNumPath"));
		if (knngenerImpl8 == null) {
			knngenerImpl8 = new KnnGenerImp8();
		}
	}

	@Override
	/**
	 * Creat the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {

		questionnaireMDirectorField = new JTextField(80);

		// Default directory
		String path = Constants.getUserDataPath() + File.separator + "output";
		questionnaireMDirectorField.setText(path);

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(500, 40));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ExportMatrixFileNullMsg")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new BorderLayout(8, 8));
		contentPanel1.add(new JLabel(), BorderLayout.NORTH);

		JPanel TextPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		TextPanel1.add(questionnaireMDirectorField);
		contentPanel1.add(TextPanel1, BorderLayout.WEST);

		JPanel buttonPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QMDirectorButton = new JButton("...");
		QMDirectorButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		buttonPanel1.add(QMDirectorButton);
		contentPanel1.add(buttonPanel1, BorderLayout.EAST);

		/**
		 * Select the source files
		 */
		QMDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("csv");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "CSV Files(*.csv)";
					}

				});

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					questionnaireMDirectorField.setText(absPath);
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;

		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		return panelBorder;

	};

	/**
	 * Validation before submit
	 */
	@Override
	public boolean beforeValid() {

		String curexportFilePath = questionnaireMDirectorField.getText();

		KassService kassService = new KassService();
		KASS k = kassService.get();
		String serrialNumber = k.getInterviewInfo().getSerialNumber();
		String exportFilePath = curexportFilePath + File.separator
				+ serrialNumber + "_Matrix_8.csv";

		if ("".equals(exportFilePath)) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("ExportMatrixFileNullMsg")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}

		File localfile = new File(exportFilePath);
		if (localfile.exists()) {

			// Pop-up dialog
			JLabel lb1 = new JLabel();
			lb1.setText(mxResources.get("ExportMatrixSameFileExistMsg"));
			UIHelper.createMultiLabel(lb1, 300);

			int replaced = JOptionPane
					.showConfirmDialog(this, lb1,
							mxResources.get("ExportMatrixSameFileExistMsg"),
							JOptionPane.YES_NO_OPTION, 0,
							UIHelper.getImage("logo.png"));
			switch (replaced) {
			case JOptionPane.CLOSED_OPTION:
				return false;
			case JOptionPane.NO_OPTION:
				return false;
			case JOptionPane.YES_OPTION:
				if (localfile.delete()) {
					return true;
				} else {
					JLabel lb2 = new JLabel();
					lb1.setText(mxResources.get("localUISameFileDeleteMsg"));
					UIHelper.createMultiLabel(lb1, 300);
					JOptionPane.showConfirmDialog(null, lb2,
							mxResources.get("ExportMatrixSameFileExistMsg"),
							JOptionPane.PLAIN_MESSAGE, 0,
							UIHelper.getImage("logo.png"));
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		try {

			KassService kassService = new KassService();
			KASS k = kassService.get();
			String serrialNumber = k.getInterviewInfo().getSerialNumber();
			String exportFilePath = questionnaireMDirectorField.getText()
					+ File.separator + serrialNumber + "_Matrix_8.csv";

			File file = new File(exportFilePath);

			String[][] exportArrayData = this.genTableDataByResultData(
					knngenerImpl8.getResult(), knngenerImpl8.getColumns());
			String[] exportHeadArray = this.genTableCol(knngenerImpl8
					.getHeader());

			boolean exportResult = ExportCSVUtils.exportCSV(file,
					exportHeadArray, exportArrayData);
			if (exportResult) {
				return new MessageModel(true, "Export success!");
			} else {
				return new MessageModel(true, "Export fail!");
			}
		} catch (Exception e) {
			return new MessageModel(false, "Export fail!");
		}
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	@Override
	/**
	 * After show
	 */
	public void afterShow() {

	}

	private String[] genTableCol(List<String> headers) {
		int colArrayLength = headers.size();
		String[] colArray = new String[colArrayLength + 1];
		colArray[0] = "End person pid";
		colArray[1] = "Kinship distance";

		for (int firstRowColIndex = 2; firstRowColIndex <= colArrayLength; firstRowColIndex++) {
			colArray[firstRowColIndex] = headers.get(firstRowColIndex - 1);
		}

		return colArray;
	}

	private String[][] genTableDataByResultData(String[][] result,
			List<String> columns) {
		int rowLength = result.length;
		int colLength = result[0].length;

		String[][] tableData = new String[rowLength][colLength + 1];
		for (int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
			tableData[rowIndex][0] = columns.get(rowIndex);
			for (int colIndex = 1; colIndex <= colLength; colIndex++) {
				tableData[rowIndex][colIndex] = result[rowIndex][colIndex - 1];
			}
		}

		return tableData;
	}
}
