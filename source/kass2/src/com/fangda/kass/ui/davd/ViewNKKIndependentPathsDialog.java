package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.service.davd.KnnGenerImp4;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of viewing the Matrix Table 4 (Independent Paths
 * matrix) produced by DAVD from the opening interview XML file in IMS
 * (Interview Management System).
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ViewNKKIndependentPathsDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4902715192876885479L;

	public static Logger logger = LoggerFactory
			.getLogger(ViewNKKIndependentPathsDialog.class);

	KnnGenerImp4 knngenerImpl4;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources.get("ViewNKKIndependentPaths"));
		if (knngenerImpl4 == null) {
			knngenerImpl4 = new KnnGenerImp4();
		}
	}

	@Override
	/**
	 * Creat the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(8, 8));

		// The column heading of the table
		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(620, 50));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKIndependentPathsTitle")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));
		panel.add(titlePanel1, BorderLayout.NORTH);

		// The data row of the table
		String[][] result = knngenerImpl4.getResult();
		String[] colArray = this.genTableCol(knngenerImpl4.getHeader());

		JTable table = new JTable(result, colArray);
		table.setFillsViewportHeight(true);
		// The width of the table
		UIHelper.changeTable(table, 60, 620);

		JScrollPane scorllPanel = new JScrollPane(table);
		scorllPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scorllPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scorllPanel.setPreferredSize(new Dimension(620, 200));
		panel.add(scorllPanel, BorderLayout.CENTER);

		// The note of the table
		JPanel titlePanelNote = new JPanel(new GridLayout(1, 1));
		titlePanelNote.setPreferredSize(new Dimension(620, 90));
		titlePanelNote.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKIndependentPathsNote")
				+ "</STRONG></HTML>"));
		titlePanelNote.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));
		panel.add(titlePanelNote, BorderLayout.SOUTH);

		return panel;
	};

	@Override
	/**
	 * After show
	 */
	public void afterShow() {
		this.getContentPane().remove(this.closeButton.getParent());
		this.validate();
	}

	private String[] genTableCol(List<String> headers) {
		int colArrayLength = headers.size();
		String[] colArray = new String[colArrayLength];
		for (int firstRowColIndex = 0; firstRowColIndex < colArrayLength; firstRowColIndex++) {
			colArray[firstRowColIndex] = headers.get(firstRowColIndex);
		}
		return colArray;
	}
}
