package com.fangda.kass.ui.davd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fangda.kass.ui.questionnaire.support.ComboItem;

/**
 * The Class for the constant setting of exporting data matrices for analysis by
 * DAVD.
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ExportModelConstant {

	public static String EXPORT_MAP_KEY = "exportTableTypeValue";
	public static String EXPORT_MAP_VALUE1 = "1";
	public static String EXPORT_MAP_VALUE2 = "2";

	public static Object[] getExportDataType1Object() {
		List<ComboItem> list = new ArrayList<ComboItem>();
		list.add(new ComboItem("NKK Data Export", "1"));
		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});
		return list.toArray();
	}

	public static Object[] getExportDataType2Object() {
		List<ComboItem> list = new ArrayList<ComboItem>();
		list.add(new ComboItem("Martices Data Export", "2"));
		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});
		return list.toArray();
	}

	public static Map<String, Object> getExoprtDataType1() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(EXPORT_MAP_KEY, EXPORT_MAP_VALUE1);
		return params;
	}

	public static Map<String, Object> getExoprtDataType2() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(EXPORT_MAP_KEY, EXPORT_MAP_VALUE2);
		return params;
	}

	/**
	 * The type of exporting data matrices
	 * 
	 * @return
	 */
	public static Object[] getExportTableTypeList() {
		List<ComboItem> list = new ArrayList<ComboItem>();

		String[] exportTableType = new String[] { "NKK Data Export",
				"Martices Data Export" };
		for (int i = 0; i < 2; i++) {
			list.add(new ComboItem(exportTableType[i], String.valueOf(i)));
		}

		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});

		return list.toArray();
	}

	/**
	 * The file type of exporting data
	 * 
	 * @return
	 */
	public static Object[] getExportFileTypeList() {
		List<ComboItem> list = new ArrayList<ComboItem>();

		list.add(new ComboItem(".csv", "1"));
		list.add(new ComboItem(".txt", "2"));

		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});

		return list.toArray();
	}

	/**
	 * The variable name of the column heading for exporting the
	 * case-by-variable-matrices
	 * 
	 * @return
	 */
	public static Object[] getVariableNameList() {
		List<ComboItem> list = new ArrayList<ComboItem>();

		list.add(new ComboItem("qid", "1"));
		list.add(new ComboItem("label", "2"));
		list.add(new ComboItem("qid_label", "3"));

		Collections.sort(list, new Comparator<ComboItem>() {
			public int compare(ComboItem arg0, ComboItem arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});

		return list.toArray();
	}
}
