package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of exporting the network matrices of the current
 * interview by DAVD.
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ExportNKKAllCurInterviewDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8600872412031333161L;

	public static Logger logger = LoggerFactory
			.getLogger(ExportNKKAllCurInterviewDialog.class);

	JTextField questionnaireMDirectorField;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources
				.get("ExportNKKallMatricesforCurrentInterview"));
	}

	@Override
	/**
	 * Create the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {

		questionnaireMDirectorField = new JTextField(80);

		// 1 Default Directory
		String path = Constants.getUserDataPath() + File.separator + "output";
		questionnaireMDirectorField.setText(path);

		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(500, 40));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ExportMatrixFileNullMsg")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));

		JPanel contentPanel1 = new JPanel(new BorderLayout(8, 8));
		contentPanel1.add(new JLabel(), BorderLayout.NORTH);

		JPanel TextPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		TextPanel1.add(questionnaireMDirectorField);
		contentPanel1.add(TextPanel1, BorderLayout.WEST);

		JPanel buttonPanel1 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QMDirectorButton = new JButton("...");
		QMDirectorButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		buttonPanel1.add(QMDirectorButton);
		contentPanel1.add(buttonPanel1, BorderLayout.EAST);

		/**
		 * Select the source files
		 */
		QMDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("csv");
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "CSV Files(*.csv)";
					}

				});

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					questionnaireMDirectorField.setText(absPath);
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;

		panelBorder.add(titlePanel1, c);
		panelBorder.add(contentPanel1, c);
		return panelBorder;

	};

	/**
	 * Validation before submit
	 */
	@Override
	public boolean beforeValid() {
		String curexportFilePath = questionnaireMDirectorField.getText();
		if ("".equals(curexportFilePath)) {
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("ExportMatrixFileNullMsg")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return false;
		}

		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		try {
			
			String exportFilePath = questionnaireMDirectorField.getText();
			List<String> exportNKKDataResultList = new ArrayList<String>();
			KassService kassService = new KassService();
			String interviewFileName = Constants.KASS_XML;
			if (interviewFileName == null || interviewFileName.length() == 0) {
				return new MessageModel(false,
						mxResources
								.get("ExportNKKallMatricesforCurrentInterview")
								+ " fail! Interview File is null");
			}

			for (int tableindex = 1; tableindex < 10; tableindex++) {
				int curIndex = tableindex;
				// for export 10
				if (9 == curIndex) {
					curIndex = curIndex + 1;
				}

				String curExportPath = this.getCurExportFileName(
						interviewFileName, exportFilePath, ".csv", curIndex);
				boolean curExportResult = ExportModelUtils.ExportTable(
						interviewFileName, curExportPath, curIndex);

				if (!curExportResult) {
					exportNKKDataResultList.add(curExportPath
							+ "export  fail!\n\r");
				}
			}

			StringBuffer exportMsg = new StringBuffer();
			if (exportNKKDataResultList.size() > 0) {
				for (String msg : exportNKKDataResultList) {
					exportMsg.append(msg);
				}
			} else {
				exportMsg.append(mxResources
						.get("ExportNKKallMatricesforCurrentInterview")
						+ " Success!");
			}

			return new MessageModel(true, exportMsg.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return new MessageModel(false,
					mxResources.get("ExportNKKallMatricesforCurrentInterview")
							+ " fail!");
		}
	}

	/**
	 * 提交结束以后的操作
	 */
	@Override
	public void afterSubmit() {

	}

	@Override
	/**
	 * 显示完成以后的
	 */
	public void afterShow() {

	}

	private String getCurExportFileName(String curInterviewPath,
			String exportFilePath, String exportFileType, int tableIndex) {
		KassService kassService = new KassService();
		KASS k = kassService.get();
		String serrialNumber = k.getInterviewInfo().getSerialNumber();
		String curExportPath = exportFilePath + File.separator + serrialNumber
				+ "_Matrix_" + String.valueOf(tableIndex) + exportFileType;
		return curExportPath;
	}

}
