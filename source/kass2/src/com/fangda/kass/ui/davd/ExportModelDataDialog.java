package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.ExportProgressUtil;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of exporting the data matrices for analysis by DAVD.
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ExportModelDataDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3322865251814100653L;

	public static Logger logger = LoggerFactory
			.getLogger(ExportModelDataDialog.class);

	JTextField exportInterviewField; // The list of filenames of the interview
										// XML files which is used for the
										// source of export data matrices.
	JTextField exportQuestionaireField; // The filenames of the questionnaire
										// used by the interview
	JComboBox exportTableTypeCombox; // The types of data matrices: 1 for
										// exporting the network matrices, 2 for
										// exporting the
										// case-by-variable-matrices including
										// the person matrix and the union
										// matrix.
	JComboBox exportFileTypeCombox; // The file type of the output data files.
	JTextField questionnaireMDirectorField; // The directory path of the output
											// data files.
	JComboBox VariableNameTypeCombox; // The variable name of the column heading
										// for exporting the
										// case-by-variable-matrices
	JProgressBar progressBar;

	@Override
	public void initDialog() {
		super.initDialog();
	}

	public ExportModelDataDialog(String title, Map<String, Object> para) {
		super(title, para);
	}

	@Override
	/**
	 * Creating the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {
		// The chose the interview XML files which is used for the source of
		// export data matrices.

		progressBar = new JProgressBar();
		progressBar.setOrientation(JProgressBar.HORIZONTAL);
		progressBar.setMinimum(0);
		progressBar.setMaximum(100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setPreferredSize(new Dimension((int) (this.getSize()
				.getWidth() - 50), 20));
		progressBar.setBorderPainted(true);
		progressBar.setBackground(Color.pink);
		;

		JPanel interviewFileSelectPanel = new JPanel(new BorderLayout(5, 5));
		interviewFileSelectPanel.add(
				new JLabel(mxResources.get("ExportDataSelectInterviewMsg")),
				BorderLayout.NORTH);

		JPanel interviewTextPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		exportInterviewField = new JTextField(80);
		interviewTextPanel.add(exportInterviewField);

		interviewFileSelectPanel.add(interviewTextPanel, BorderLayout.WEST);

		JPanel interviewButtonPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton interviewButton = new JButton("...");
		interviewButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		interviewButtonPanel.add(interviewButton);

		interviewFileSelectPanel.add(interviewButtonPanel, BorderLayout.EAST);

		JPanel questionaireFileSelectPanel = new JPanel(new BorderLayout(0, 0));
		questionaireFileSelectPanel.add(new JLabel(
				"Please Select Questionaire for QRC (maybe Empty)"),
				BorderLayout.NORTH);
		JPanel questionaireTextPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		exportQuestionaireField = new JTextField(80);
		questionaireTextPanel.add(exportQuestionaireField);
		JPanel questionaireButtonPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton questionaireButton = new JButton("...");
		questionaireButton.setPreferredSize(new Dimension(30, 20));// 设置尺寸
		questionaireButtonPanel.add(questionaireButton);
		questionaireFileSelectPanel.add(questionaireTextPanel,
				BorderLayout.WEST);
		questionaireFileSelectPanel.add(questionaireButtonPanel,
				BorderLayout.EAST);
		questionaireButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("properties");

				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "Questioniare Files(*.properties)";
					}
				});

				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(false);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					if (file != null) {
						String filePath = file.getAbsolutePath();
						exportQuestionaireField.setText(filePath);
					}
				}
			}
		});

		// 2 -3
		JPanel tableAndFileTypePanel = new JPanel(new BorderLayout(5, 5));

		JPanel exportTableTypePanel = new JPanel(new BorderLayout(5, 5));
		exportTableTypePanel.add(
				new JLabel(mxResources.get("ExportDataTypeMsg")),
				BorderLayout.WEST);

		if (ExportModelConstant.EXPORT_MAP_VALUE1.equals(this.params.get(
				ExportModelConstant.EXPORT_MAP_KEY).toString())) {
			exportTableTypeCombox = new JComboBox(
					ExportModelConstant.getExportDataType1Object());
		} else if (ExportModelConstant.EXPORT_MAP_VALUE2.equals(this.params
				.get(ExportModelConstant.EXPORT_MAP_KEY).toString())) {
			exportTableTypeCombox = new JComboBox(
					ExportModelConstant.getExportDataType2Object());
		}
		exportTableTypeCombox.setEnabled(false);

		exportTableTypePanel.add(exportTableTypeCombox, BorderLayout.EAST);
		tableAndFileTypePanel.add(exportTableTypePanel, BorderLayout.WEST);

		JPanel exportFileTypePanel = new JPanel(new BorderLayout(5, 5));
		exportFileTypePanel.add(
				new JLabel(mxResources.get("ExportFileTypeMsg")),
				BorderLayout.WEST);
		exportFileTypeCombox = new JComboBox(
				ExportModelConstant.getExportFileTypeList());
		exportFileTypePanel.add(exportFileTypeCombox, BorderLayout.EAST);
		tableAndFileTypePanel.add(exportFileTypePanel, BorderLayout.EAST);

		interviewFileSelectPanel.add(tableAndFileTypePanel, BorderLayout.SOUTH);

		// 4
		JPanel exportFilePathPanel = new JPanel(new BorderLayout(5, 5));
		exportFilePathPanel.add(
				new JLabel(mxResources.get("ExportFilePathMsg")),
				BorderLayout.NORTH);

		JPanel exportFilePathSelectPanel = new JPanel(
				new GridLayout(2, 1, 0, 5));

		questionnaireMDirectorField = new JTextField(80);
		String path = Constants.getUserDataPath() + File.separator + "output";
		questionnaireMDirectorField.setText(path);
		exportFilePathSelectPanel.add(questionnaireMDirectorField);

		exportFilePathPanel.add(exportFilePathSelectPanel, BorderLayout.WEST);

		JPanel buttonPanel2 = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton QMDirectorButton = new JButton("...");
		QMDirectorButton.setPreferredSize(new Dimension(30, 20));
		buttonPanel2.add(QMDirectorButton);

		exportFilePathPanel.add(buttonPanel2, BorderLayout.EAST);

		if (ExportModelConstant.EXPORT_MAP_VALUE2.equals(this.params.get(
				ExportModelConstant.EXPORT_MAP_KEY).toString())) {

			JPanel varivablePanel = new JPanel(new BorderLayout(5, 5));
			varivablePanel.add(new JLabel(mxResources.get("ExportVarTypeMsg")),
					BorderLayout.WEST);
			VariableNameTypeCombox = new JComboBox(
					ExportModelConstant.getVariableNameList());
			varivablePanel.add(VariableNameTypeCombox, BorderLayout.EAST);
			exportFilePathPanel.add(varivablePanel, BorderLayout.SOUTH);
		}

		JPanel progressLabelPanel = new JPanel(new GridLayout(2, 5));
		progressLabelPanel.add(new JLabel(""));
		progressLabelPanel.add(new JLabel(mxResources.get("ProgressLabel")));

		interviewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("xml");

				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "XML Files(*.xml)";
					}
				});

				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(true);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					File[] files = chooser.getSelectedFiles();
					int fileCount = files.length;
					StringBuffer selectFiles = new StringBuffer();

					if (exportInterviewField.getText() != null
							&& exportInterviewField.getText().length() > 0) {
						selectFiles.append(exportInterviewField.getText() + ";");
					}

					for (int cur = 0; cur < fileCount; cur++) {
						selectFiles.append(files[cur].getAbsolutePath());
						if (cur != fileCount - 1) {
							selectFiles.append(";");
						}
					}
					exportInterviewField.setText(selectFiles.toString());
				}
			}
		});

		QMDirectorButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					String absPath = chooser.getSelectedFile()
							.getAbsolutePath();
					questionnaireMDirectorField.setText(absPath);
				}
			}
		});

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;

		// panelBorder.add(interviewTitlePanel, c);
		panelBorder.add(questionaireFileSelectPanel, c);
		panelBorder.add(interviewFileSelectPanel, c);
		panelBorder.add(exportFilePathPanel, c);

		panelBorder.add(progressLabelPanel, c);
		panelBorder.add(progressBar, c);

		return panelBorder;

	};

	/**
	 * Validation before submit
	 */
	@Override
	public boolean beforeValid() {

		/**
		 * // 1 文件是否选择 String exportFilePath =
		 * questionnaireMDirectorField.getText(); if ("".equals(exportFilePath))
		 * { JOptionPane.showMessageDialog(container,"<HTML>" +
		 * mxResources.get("ExportMatrixFileNullMsg")+ "</HTML>", "Warning",
		 * JOptionPane.INFORMATION_MESSAGE, UIHelper.getImage("warning.png"));
		 * return false; }
		 * 
		 * //2 验证是否存在同名的文件 File localfile = new File(exportFilePath);
		 * if(localfile.exists()){
		 * 
		 * // 1 弹出对话框 JLabel lb1 = new JLabel();
		 * lb1.setText(mxResources.get("ExportMatrixSameFileExistMsg"));
		 * UIHelper.createMultiLabel(lb1, 300);
		 * 
		 * int replaced =
		 * JOptionPane.showConfirmDialog(this,lb1,mxResources.get(
		 * "ExportMatrixSameFileExistMsg"),
		 * JOptionPane.YES_NO_OPTION,0,UIHelper.getImage("logo.png")); switch
		 * (replaced) { case JOptionPane.CLOSED_OPTION: return false; case
		 * JOptionPane.NO_OPTION: return false; case JOptionPane.YES_OPTION:
		 * if(localfile.delete()){ return true; }else{ JLabel lb2 = new
		 * JLabel(); lb1.setText(mxResources.get("localUISameFileDeleteMsg"));
		 * UIHelper.createMultiLabel(lb1, 300);
		 * JOptionPane.showConfirmDialog(null,lb2,
		 * mxResources.get("ExportMatrixSameFileExistMsg"),
		 * JOptionPane.PLAIN_MESSAGE,0, UIHelper.getImage("logo.png")); return
		 * false; } } }
		 */
		return true;
	}

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		try {
			// Check the interview XML file
			sureButton.setEnabled(false);
			String exportInterviewFilePath = exportInterviewField.getText();
			if ("".equals(exportInterviewFilePath)) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("ExportIntervieFileNullMsg")
								+ "</HTML>", "Warning",
						JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				sureButton.setEnabled(true);
				return null;
			}

			// Check the directory path
			String exportFilePath = questionnaireMDirectorField.getText();
			if ("".equals(exportFilePath)) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("ExportMatrixFileNullMsg")
								+ "</HTML>", "Warning",
						JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				sureButton.setEnabled(true);
				return null;
			}

			List<String> fileList = new ArrayList<String>();
			String[] selectFiles = exportInterviewField.getText().split(";");

			for (int i = 0; i < selectFiles.length; i++) {
				fileList.add(selectFiles[i]);
			}
			String questionaireFile = exportQuestionaireField.getText();

			ComboItem selectedTableTypeItem = (ComboItem) exportTableTypeCombox
					.getSelectedItem();
			String exportTableType = selectedTableTypeItem.getValue();

			ComboItem selectedFileTypeItem = (ComboItem) exportFileTypeCombox
					.getSelectedItem();
			String exportFileType = selectedFileTypeItem.getName();

			boolean[] exportPersonAndUnionResult = new boolean[2];

			List<String> exportNKKDataResultList = new ArrayList<String>();

			if (exportTableType.equals(ExportModelConstant.EXPORT_MAP_VALUE2)) {
				ComboItem selectedVariableNameItem = (ComboItem) VariableNameTypeCombox
						.getSelectedItem();
				String variableName = selectedVariableNameItem.getValue();
				ExportProgressUtil.getInstance().setProgressBar(progressBar);
				ExportProgressUtil.getInstance().setType(1);
				boolean exportPersonResult = ExportModelUtils.ExportPersonData(
						fileList, exportFilePath, exportFileType, variableName,
						questionaireFile);
				progressBar.setValue(50);
				exportPersonAndUnionResult[0] = exportPersonResult;
				ExportProgressUtil.getInstance().setType(2);
				boolean exportUnionResult = ExportModelUtils.ExportUnionData(
						fileList, exportFilePath, exportFileType, variableName);
				exportPersonAndUnionResult[1] = exportUnionResult;
				progressBar.setValue(100);
				ExportProgressUtil.getInstance().setProgressBar(null);
				ExportProgressUtil.getInstance().setType(0);
			} else {
				int fileListLength = fileList.size();
				for (int index = 0; index < fileListLength; index++) {
					String interviewFileName = fileList.get(index);
					for (int tableindex = 1; tableindex < 10; tableindex++) {
						int curIndex = tableindex;
						// for export 10
						if (9 == curIndex) {
							curIndex = curIndex + 1;
						}
						String curExportPath = this.getCurExportFileName(
								interviewFileName, exportFilePath,
								exportFileType, curIndex);
						boolean curExportResult = ExportModelUtils.ExportTable(
								interviewFileName, curExportPath, curIndex);
						progressBar.setValue(tableindex * 10);
						if (!curExportResult) {
							exportNKKDataResultList.add(curExportPath
									+ "export  fail!\n\r");
						}
					}
				}
			}

			StringBuffer exportMsg = new StringBuffer();

			if (exportTableType.equals(ExportModelConstant.EXPORT_MAP_VALUE2)) {
				if (!exportPersonAndUnionResult[0]) {
					exportMsg.append("Export Person Data Fail!");
				} else {
					if (!exportPersonAndUnionResult[1]) {
						exportMsg.append("Export Union Data Fail!");
					} else {
						exportMsg
								.append("Export Person And Union Data Success!");
					}
				}
			} else {
				if (exportNKKDataResultList.size() > 0) {
					for (String msg : exportNKKDataResultList) {
						exportMsg.append(msg);
					}
				} else {
					exportMsg.append("Export NKK Data Success!");
				}
			}

			return new MessageModel(true, exportMsg.toString());

		} catch (Exception e) {
			e.printStackTrace();
			return new MessageModel(false, "Export fail!");
		} finally {
			sureButton.setEnabled(true);
		}
	}

	/**
	 * After submit
	 */
	@Override
	public void afterSubmit() {

	}

	@Override
	/**
	 * After show
	 */
	public void afterShow() {

	}

	private String getCurExportFileName(String curInterviewPath,
			String exportFilePath, String exportFileType, int tableIndex) {
		KassService kassService = new KassService();
		KASS k = kassService.read(curInterviewPath);
		String serrialNumber = k.getInterviewInfo().getSerialNumber();
		String curExportPath = exportFilePath + File.separator + serrialNumber
				+ "_Matrix_" + String.valueOf(tableIndex) + exportFileType;
		return curExportPath;
	}

}
