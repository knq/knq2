package com.fangda.kass.ui.davd;

import java.util.List;

/**
 * The Class for the entity of the constant setting of exporting data matrices
 * for analysis by DAVD.
 * 
 * @author YU Zhang
 * 
 */
public class ExportModelEntity {

	/**
	 * the list of the file name (full path) of the interview XML files which is
	 * used for the source of export data matrices.
	 */
	private List<String> interviewFileFullPath;

	/**
	 * The types of data matrices: 1 for exporting the network matrices, 2 for
	 * exporting the case-by-variable-matrices including the person matrix and
	 * the union matrix.
	 */
	private String exporTableType;

	/**
	 * The file type of exporting data matrices: 1 for .cvs , 2 for .txt
	 */
	private String exportFileType;

	/**
	 * The directory path (full path) of the output data files
	 */
	private String exportFileFullPath;

	/**
	 * The variable name of the column heading for exporting the
	 * case-by-variable-matrices
	 */
	private String variableName; 
	

	public List<String> getInterviewFileFullPath() {
		return interviewFileFullPath;
	}

	public void setInterviewFileFullPath(List<String> interviewFileFullPath) {
		this.interviewFileFullPath = interviewFileFullPath;
	}

	public String getExporTableType() {
		return exporTableType;
	}

	public void setExporTableType(String exporTableType) {
		this.exporTableType = exporTableType;
	}

	public String getExportFileType() {
		return exportFileType;
	}

	public void setExportFileType(String exportFileType) {
		this.exportFileType = exportFileType;
	}

	public String getExportFileFullPath() {
		return exportFileFullPath;
	}

	public void setExportFileFullPath(String exportFileFullPath) {
		this.exportFileFullPath = exportFileFullPath;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
}
