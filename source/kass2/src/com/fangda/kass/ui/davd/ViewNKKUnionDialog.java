package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.service.davd.KnnGenerImp3;
import com.fangda.kass.service.davd.KnnWrapper;
import com.fangda.kass.service.davd.support.GenterOption;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of viewing the union matrix produced by DAVD from
 * the opening interview XML file in IMS (Interview Management System).
 * 
 * @author Yu Zhang
 * 
 */
public class ViewNKKUnionDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9113376539838863099L;

	public static Logger logger = LoggerFactory
			.getLogger(ViewNKKUnionDialog.class);

	JTextField exportInterviewField; // The list of filenames of the interview
										// XML files which is used for the
										// source of export data matrices.
	JComboBox VariableNameTypeCombox; // The variable name of the column heading
										// for exporting the
										// case-by-variable-matrices
	JScrollPane scorllPanel;
	JTable table;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources.get("ViewNKKKUnion"));
	}

	@Override
	/**
	 * Creat the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {

		// 1 select the interview XML file

		JPanel interviewFileSelectPanel = new JPanel(new BorderLayout(5, 5));
		;
		interviewFileSelectPanel.add(
				new JLabel(mxResources.get("ExportDataSelectInterviewMsg")),
				BorderLayout.NORTH);

		JPanel interviewTextPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		exportInterviewField = new JTextField(80);
		interviewTextPanel.add(exportInterviewField);

		interviewFileSelectPanel.add(interviewTextPanel, BorderLayout.WEST);

		JPanel interviewButtonPanel = new JPanel(new GridLayout(2, 1, 0, 5));
		JButton interviewButton = new JButton("...");
		interviewButton.setPreferredSize(new Dimension(30, 20));
		interviewButtonPanel.add(interviewButton);

		interviewFileSelectPanel.add(interviewButtonPanel, BorderLayout.EAST);

		JPanel varivablePanel = new JPanel(new BorderLayout(5, 5));
		varivablePanel.add(new JLabel(mxResources.get("ExportVarTypeMsg")),
				BorderLayout.WEST);
		VariableNameTypeCombox = new JComboBox(
				ExportModelConstant.getVariableNameList());

		JPanel varivablePane2 = new JPanel(new BorderLayout(5, 5));
		varivablePane2.add(VariableNameTypeCombox, BorderLayout.WEST);

		JButton interviewButton2 = new JButton(
				mxResources.get("ViewNKKKUnionBtnMsg"));
		interviewButton2.setPreferredSize(new Dimension(200, 20));
		varivablePane2.add(interviewButton2, BorderLayout.EAST);

		varivablePanel.add(varivablePane2, BorderLayout.EAST);

		interviewFileSelectPanel.add(varivablePanel, BorderLayout.SOUTH);

		interviewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(".");
				final List list = new ArrayList();
				list.add("xml");

				chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
					public boolean accept(File f) {
						if (f.isDirectory())
							return true;
						String name = f.getName();
						int p = name.lastIndexOf('.');
						if (p == -1)
							return false;
						String suffix = name.substring(p + 1).toLowerCase();
						return list.contains(suffix);
					}

					@Override
					public String getDescription() {
						return "XML Files(*.xml)";
					}
				});

				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(true);
				int i = chooser.showOpenDialog(getContentPane());
				if (i == JFileChooser.APPROVE_OPTION) {
					File[] files = chooser.getSelectedFiles();
					int fileCount = files.length;
					StringBuffer selectFiles = new StringBuffer();

					if (exportInterviewField.getText() != null
							&& exportInterviewField.getText().length() > 0) {
						selectFiles.append(exportInterviewField.getText() + ";");
					}

					for (int cur = 0; cur < fileCount; cur++) {
						selectFiles.append(files[cur].getAbsolutePath());
						if (cur != fileCount - 1) {
							selectFiles.append(";");
						}
					}
					exportInterviewField.setText(selectFiles.toString());
				}
			}
		});

		interviewButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submit();
			}
		});

		table = new JTable();
		table.setFillsViewportHeight(true);

		scorllPanel = new JScrollPane(table);
		scorllPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scorllPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scorllPanel.setPreferredSize(new Dimension(620, 300));

		JPanel panelBorder = new JPanel(new GridBagLayout());
		panelBorder.setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 0;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;

		panelBorder.add(interviewFileSelectPanel, c);
		panelBorder.add(scorllPanel, c);

		return panelBorder;
	};

	/**
	 * Submit
	 */
	@Override
	public MessageModel submit() {
		try {
			String exportInterviewFilePath = exportInterviewField.getText();
			if ("".equals(exportInterviewFilePath)) {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("ExportIntervieFileNullMsg")
								+ "</HTML>", "Warning",
						JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
				return null;
			}

			List<String> fileList = new ArrayList<String>();
			String[] selectFiles = exportInterviewField.getText().split(";");
			for (int i = 0; i < selectFiles.length; i++) {
				fileList.add(selectFiles[i]);
			}

			ComboItem selectedVariableNameItem = (ComboItem) VariableNameTypeCombox
					.getSelectedItem();
			String variableName = selectedVariableNameItem.getValue();

			GenterOption optionUnion = new GenterOption();
			optionUnion.setIvFileNames(fileList);
			optionUnion.setGenerType(22);
			optionUnion.setVarType(Integer.valueOf(variableName));

			KnnWrapper knnwrapper = new KnnWrapper(optionUnion);
			String[][] exportDataArray = knnwrapper.getResult();
			if (exportDataArray != null && exportDataArray.length > 0
					&& exportDataArray[0] != null
					&& exportDataArray[0].length > 0) {
				String[] exportHeadArray = ExportModelUtils
						.getExportPersonOrUnionArray(knnwrapper.getHeader());
				DefaultTableModel tableModel = new DefaultTableModel(
						exportDataArray, exportHeadArray);
				table.setModel(tableModel);
				table.repaint();
			} else {
				JOptionPane.showMessageDialog(container,
						"<HTML>" + mxResources.get("ViewNKKKUnionDataNullMsg")
								+ "</HTML>", "Warning",
						JOptionPane.INFORMATION_MESSAGE,
						UIHelper.getImage("warning.png"));
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(container,
					"<HTML>" + mxResources.get("ViewNKKKUnionDataErrMsg")
							+ "</HTML>", "Warning",
					JOptionPane.INFORMATION_MESSAGE,
					UIHelper.getImage("warning.png"));
			return null;
		}
	}

	@Override
	/**
	 *  After show
	 */
	public void afterShow() {
		this.getContentPane().remove(this.closeButton.getParent());
		this.validate();
	}

}
