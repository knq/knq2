package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.service.davd.KnnGenerImp1;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of viewing the Matrix Table 1 (Person-by-union
 * network matrix-distinguishing partners and offspring) produced by DAVD from
 * the opening interview XML file in IMS (Interview Management System).
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ViewPersonbyUnionMatrixTypeDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8596005524303703402L;

	public static Logger logger = LoggerFactory
			.getLogger(ViewPersonbyUnionMatrixTypeDialog.class);

	KnnGenerImp1 knngenerImpl1;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources.get("ViewNKKPersonbyUnionMatrixType"));
		if (knngenerImpl1 == null) {
			knngenerImpl1 = new KnnGenerImp1();
		}
	}

	@Override
	/**
	 * Creat the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {
		this.setResizable(true);
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(8, 8));

		// The column heading of the table
		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(620, 50));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKPersonbyUnionMatrixTypeTitle")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));
		panel.add(titlePanel1, BorderLayout.NORTH);

		// The data row of the table
		String[][] result = knngenerImpl1.getResult();
		String[][] tableDataResult = this.getTableDataByOrginalResult(result);

		List<String> headers = knngenerImpl1.getHeader();
		String[] colArray = this.getColumnsArrayByColumnsList(headers);

		JTable table = new JTable(tableDataResult, colArray);
		table.setFillsViewportHeight(true);

		// The width of the table
		UIHelper.changeTable(table, 60, 620);

		JScrollPane scorllPanel = new JScrollPane(table);
		scorllPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scorllPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scorllPanel.setPreferredSize(new Dimension(620, 200));
		panel.add(scorllPanel, BorderLayout.CENTER);

		// The note of the table
		JPanel titlePanelNote = new JPanel(new GridLayout(1, 1));
		titlePanelNote.setPreferredSize(new Dimension(620, 50));
		titlePanelNote.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKPersonbyUnionMatrixTypeNote")
				+ "</STRONG></HTML>"));
		titlePanelNote.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));
		panel.add(titlePanelNote, BorderLayout.SOUTH);

		return panel;
	};

	@Override
	/**
	 * After show
	 */
	public void afterShow() {
		this.getContentPane().remove(this.closeButton.getParent());
		this.validate();
	}

	public String[] getColumnsArrayByColumnsList(List<String> columns) {
		if (columns != null) {
			int columnCount = columns.size();
			String[] result = new String[columnCount + 1];
			result[0] = "";
			for (int i = 1; i <= columnCount; i++) {
				result[i] = "uid=" + columns.get(i - 1);
			}
			return result;
		} else {
			return null;
		}
	}

	public String[][] getTableDataByOrginalResult(String[][] result) {
		int rowLength = result.length;
		int colLength = result[0].length;
		String[][] tableData = new String[rowLength][colLength + 1];
		for (int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
			// Add PID
			tableData[rowIndex][0] = "PID="
					+ knngenerImpl1.getColumns().get(rowIndex);
			for (int colIndex = 1; colIndex <= colLength; colIndex++) {
				tableData[rowIndex][colIndex] = result[rowIndex][colIndex - 1];
			}
		}
		return tableData;
	}
}
