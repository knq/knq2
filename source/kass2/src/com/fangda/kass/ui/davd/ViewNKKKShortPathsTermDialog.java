package com.fangda.kass.ui.davd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.service.davd.KnnGenerImp3;
import com.fangda.kass.service.davd.KnnGenerImp6;
import com.fangda.kass.service.davd.KnnGenerImp7;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.util.Constants;
import com.mxgraph.util.mxResources;

/**
 * The Class for the Dialog of viewing the Matrix Table 7 ("First" Paths between
 * Ego and other network members) produced by DAVD from the opening interview
 * XML file in IMS (Interview Management System).
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ViewNKKKShortPathsTermDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6026093885633444701L;

	public static Logger logger = LoggerFactory
			.getLogger(ViewNKKKShortPathsTermDialog.class);

	KnnGenerImp7 knngenerImpl7;

	@Override
	public void initDialog() {
		super.initDialog();
		this.setTitle(mxResources.get("ViewNKKKShortPathsTerm"));
		if (knngenerImpl7 == null) {
			knngenerImpl7 = new KnnGenerImp7();
		}
	}

	@Override
	/**
	 * Creat the dialog
	 * 
	 * @return
	 */
	public JPanel createPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(8, 8));

		// The column heading of the table
		JPanel titlePanel1 = new JPanel(new GridLayout(1, 1));
		titlePanel1.setPreferredSize(new Dimension(800, 50));
		titlePanel1.add(new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKKShortPathsTermTitle")
				+ "</STRONG></HTML>"));
		titlePanel1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(8, 8, 8, 8)));
		panel.add(titlePanel1, BorderLayout.NORTH);

		// The data row of the table
		String[][] tableDataArray = this
				.getTableDataByOrginalResult(knngenerImpl7.getResult());
		String[] colArray = this.getColumnsArrayByColumnsList(knngenerImpl7
				.getHeader());

		JTable table = new JTable(tableDataArray, colArray);
		table.setFillsViewportHeight(true);

		// The width of the table
		UIHelper.changeTable(table, 130, 800);

		JScrollPane scorllPanel = new JScrollPane(table);
		scorllPanel
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scorllPanel
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scorllPanel.setPreferredSize(new Dimension(800, 200));
		panel.add(scorllPanel, BorderLayout.CENTER);

		// The note 1 of the table
		JPanel titlePanelNote = new JPanel(new BorderLayout(2, 2));
		titlePanelNote.setPreferredSize(new Dimension(800, 225));
		JLabel lb = new JLabel("<HTML><STRONG>"
				+ mxResources.get("ViewNKKKShortPathsTermNote")
				+ "</STRONG></HTML>");
		lb.setPreferredSize(new Dimension(800, 50));
		titlePanelNote.add(lb, BorderLayout.NORTH);

		// The note 2 of the table
		String[] noteColArray = new String[] { "numeric code (not shown)",
				"single-letter label", "relationship" };
		JTable tableNote = new JTable(this.getNoteArray(), noteColArray);
		tableNote.setFillsViewportHeight(true);

		JScrollPane scorllPane2 = new JScrollPane(tableNote);
		scorllPane2
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scorllPane2
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scorllPane2.setPreferredSize(new Dimension(800, 170));
		titlePanelNote.add(scorllPane2, BorderLayout.SOUTH);

		panel.add(titlePanelNote, BorderLayout.SOUTH);

		return panel;
	};

	@Override
	/**
	 * After show
	 */
	public void afterShow() {
		this.getContentPane().remove(this.closeButton.getParent());
		this.validate();
	}

	// The value code in the note 2 of the table
	public String[][] getNoteArray() {
		String[] noteColArrayFirst = new String[] { "1", "2", "3", "4", "5",
				"6", "7", "8" };
		String[] noteColArraySecond = new String[] { "M", "F", "Z", "B", "W",
				"H", "D", "S" };
		String[] noteColArrayThird = new String[] { "mother", "father",
				"sister", "brother", "wife", "husband", "daughter", "son" };
		String[][] noteColArray = new String[8][3];
		for (int i = 0; i < 8; i++) {
			noteColArray[i][0] = noteColArrayFirst[i];
			noteColArray[i][1] = noteColArraySecond[i];
			noteColArray[i][2] = noteColArrayThird[i];
		}
		return noteColArray;
	}

	private String[] getColumnsArrayByColumnsList(List<String> columns) {
		if (columns != null) {
			int columnCount = columns.size();
			String[] result = new String[columnCount + 1];
			result[0] = "End person pid";
			int last1Index = columns.lastIndexOf("1");
			for (int i = 1; i <= columnCount; i++) {
				if (i <= last1Index) {
					result[i] = "step" + columns.get(i - 1) + "(Letter labels)";
				} else {
					result[i] = "step" + columns.get(i - 1) + "(Number codes)";
				}
			}
			return result;
		} else {
			return null;
		}
	};

	public String[][] getTableDataByOrginalResult(String[][] result) {
		int rowLength = result.length;
		int colLength = result[0].length;
		String[][] tableData = new String[rowLength][colLength + 1];
		for (int rowIndex = 0; rowIndex < rowLength; rowIndex++) {
			tableData[rowIndex][0] = knngenerImpl7.getColumns().get(rowIndex);
			for (int colIndex = 1; colIndex <= colLength; colIndex++) {
				tableData[rowIndex][colIndex] = result[rowIndex][colIndex - 1];
			}
		}
		return tableData;
	}

}
