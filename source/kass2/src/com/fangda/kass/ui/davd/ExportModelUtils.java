package com.fangda.kass.ui.davd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.service.davd.KnnWrapper;
import com.fangda.kass.service.davd.support.GenterOption;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.util.ExportCSVUtils;

/**
 * The Class for generating the exporting data matrices for analysis by DAVD.
 * 
 * @author Yu Zhang, Fangfang Zhao
 * 
 */
public class ExportModelUtils {
	
	//==================================Person Export====================
	public static boolean ExportPersonData(List<String> fileList,
			String exportFilePath,String exportFileType, String variableName, String questionaireFile){
		boolean exportResult = false;
		try{
			//*20160121 add the project name to the filename of the output files. Begin
			KassService kassService = new KassService(); 
			KASS   kass= null;
			if(fileList!=null && fileList.size() > 0){
				 kass = kassService.read(fileList.get(0));
			}else{
				kass = kassService.get();
			}
			String placeName = kass.getFieldSite().getPlaceName();
			//*20160121 add the project name to the filename of the output files. End
			
			GenterOption optionPerson = new GenterOption();
			optionPerson.setIvFileNames(fileList);
			String outFileName =exportFilePath + File.separator+placeName+"_MatrixPerson"+exportFileType;
			optionPerson.setOutFileName(outFileName);
			optionPerson.setGenerType(21); 
			optionPerson.setVarType(Integer.valueOf(variableName));
			optionPerson.setQuestionFileName(questionaireFile);
			
			KnnWrapper knnwrapper = new KnnWrapper(optionPerson);
		
			String[][] exportDataArray = knnwrapper.getResult();
			String[] exportHeadArray = getExportPersonOrUnionArray(knnwrapper.getHeader());
			exportResult =  ExportCSVUtils.exportCSV(new File(outFileName),exportHeadArray,exportDataArray);
		
		}catch(Exception e){
			e.printStackTrace();
			exportResult=  false;
		}
		return  exportResult;
	}
	
	public static String[] getExportPersonOrUnionArray(List<String> headCoulmns){
		int  headCoulmnsLength =headCoulmns.size();
		String[] result = new String[headCoulmnsLength];
		for(int i= 0;i <headCoulmnsLength;i++ ){
			result[i]=headCoulmns.get(i);
		}
		return result;
	}
	//==================================Person Export====================
	
	
	//==================================Union Export====================
	public static boolean ExportUnionData(List<String> fileList,
			String exportFilePath,String exportFileType, String variableName){
		boolean exportResult = false;
		try{
			
			//*20160121 add the project name to the filename of the output files. Begin
			KassService kassService = new KassService(); 
			KASS   kass= null;
			if(fileList!=null && fileList.size() > 0){
				 kass = kassService.read(fileList.get(0));
			}else{
				kass = kassService.get();
			}
			String placeName = kass.getFieldSite().getPlaceName();
			//*20160121 add the project name to the filename of the output files. End
			
			GenterOption optionUnion = new GenterOption();
			optionUnion.setIvFileNames(fileList);
			String outFileNameUnion =exportFilePath  + File.separator+placeName+"_MatrixUnion"+exportFileType;
			optionUnion.setOutFileName(outFileNameUnion);
			optionUnion.setGenerType(22); 
			optionUnion.setVarType(Integer.valueOf(variableName));
			
			KnnWrapper knnwrapper2 = new KnnWrapper(optionUnion);
			String[][]  exportDataArray2=   knnwrapper2.getResult();
			if(exportDataArray2!=null &&  exportDataArray2.length >0 &&
					knnwrapper2.getHeader()!=null&& knnwrapper2.getHeader().size()>0){
				String[] exportHeadArray2 = getExportPersonOrUnionArray(knnwrapper2.getHeader());
				exportResult = ExportCSVUtils.exportCSV(new File(outFileNameUnion),exportHeadArray2,exportDataArray2);
			}else{
				exportResult = false; 
			}	
		}catch(Exception e){
			e.printStackTrace();
			exportResult =  false;
		}
		return exportResult;
	}
	//==================================Union Export====================
	
	
	public static boolean ExportTable(String curInterviewFilePath,String exportPath,int tableindex){
		if(1==tableindex){
			return ExportTable1(curInterviewFilePath,exportPath);
		}else if(2==tableindex){
			return ExportTable2(curInterviewFilePath,exportPath);
		}else if(3==tableindex){
			return ExportTable3(curInterviewFilePath,exportPath);
		}else if(4==tableindex){
			return ExportTable4(curInterviewFilePath,exportPath);
		}else if(5==tableindex){
			return ExportTable5(curInterviewFilePath,exportPath);
		}else if(6==tableindex){
			return ExportTable6(curInterviewFilePath,exportPath);
		}else if(7==tableindex){
			return ExportTable7(curInterviewFilePath,exportPath);
		}else if(8==tableindex){
			return ExportTable8(curInterviewFilePath,exportPath);
		}else if(10==tableindex){
			return ExportTable10(curInterviewFilePath,exportPath);
		}else{
			return true;
		}
	}
	
	//==================================table1 Export====================
	public static boolean ExportTable1(String curInterviewFilePath,String exportPath){	
		GenterOption option = new GenterOption();
		option.setGenerType(1); 
		
		List<String>  ivFileNames = new ArrayList<String>();
		ivFileNames.add(curInterviewFilePath);
		option.setIvFileNames(ivFileNames);
		
		KnnWrapper knngenerImpl1 = new KnnWrapper(option);
		
		File file = new File(exportPath);	
		String[][] exportArrayData =getTableDataByOrginalResult1(knngenerImpl1.getResult(),knngenerImpl1.getColumns());
		String[] exportHeadArray = getColumnsArrayByColumnsList1(knngenerImpl1.getHeader());
		boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
		return exportResult;
	}

	private static String[] getColumnsArrayByColumnsList1(List<String> columns){
			if(columns!=null){
				int columnCount= columns.size();
				String[] result =new String[columnCount+1];
				result[0]="";
				for(int i=1; i <=columnCount;i++){
					result[i]="uid="+columns.get(i-1);
				}
				return result;
			}else{
				return null;
			}
		}
	
	private static String[][] getTableDataByOrginalResult1(String[][] result,List<String> columnsList){	
			int rowLength = result.length;
			int colLength = result[0].length;
			String[][] tableData = new  String[rowLength][colLength+1];
			for(int rowIndex=0;rowIndex< rowLength;rowIndex++){
				tableData[rowIndex][0]="PID="+columnsList.get(rowIndex);
				for(int colIndex=1;colIndex<=colLength;colIndex++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
				}
			}
			return tableData;
		}
	
	//==================================table1 Export====================
	
	
	//==================================table10 Export====================
	public static boolean ExportTable10(String curInterviewFilePath,String exportPath){	
		GenterOption option = new GenterOption();
		option.setGenerType(10); 
		
		List<String>  ivFileNames = new ArrayList<String>();
		ivFileNames.add(curInterviewFilePath);
		option.setIvFileNames(ivFileNames);
		
		KnnWrapper knngenerImpl10 = new KnnWrapper(option);
		
		File file = new File(exportPath);	
		String[][] exportArrayData =getTableDataByOrginalResult10(knngenerImpl10.getResult(),knngenerImpl10.getColumns());
		String[] exportHeadArray = getColumnsArrayByColumnsList10(knngenerImpl10.getHeader());
		boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
		return exportResult;
	}

	private static String[] getColumnsArrayByColumnsList10(List<String> columns){
			if(columns!=null){
				int columnCount= columns.size();
				String[] result =new String[columnCount+1];
				result[0]="";
				for(int i=1; i <=columnCount;i++){
					result[i]="uid="+columns.get(i-1);
				}
				return result;
			}else{
				return null;
			}
		}
	
	private static String[][] getTableDataByOrginalResult10(String[][] result,List<String> columnsList){	
			int rowLength = result.length;
			int colLength = result[0].length;
			String[][] tableData = new  String[rowLength][colLength+1];
			for(int rowIndex=0;rowIndex< rowLength;rowIndex++){
				tableData[rowIndex][0]="PID="+columnsList.get(rowIndex);
				for(int colIndex=1;colIndex<=colLength;colIndex++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
				}
			}
			return tableData;
		}
	//==================================table10 Export====================


	//==================================table2 Export====================
	public static boolean ExportTable2(String curInterviewFilePath,String exportPath) {
		GenterOption option = new GenterOption();
		option.setGenerType(2); 
		
		List<String>  ivFileNames = new ArrayList<String>();
		ivFileNames.add(curInterviewFilePath);
		option.setIvFileNames(ivFileNames);
		
		KnnWrapper knngenerImpl2 = new KnnWrapper(option);
		
		File file = new File(exportPath);		
		String[][] exportArrayData = getTableDataByOrginalResult2(knngenerImpl2.getResult(),knngenerImpl2.getColumns());
		String[] exportHeadArray = getColumnsArrayByColumnsList2(knngenerImpl2.getHeader());
		boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
		return exportResult;
	}

	private static String[] getColumnsArrayByColumnsList2(List<String> columns){
			if(columns!=null){
				int columnCount= columns.size();
				String[] result =new String[columnCount+1];
				result[0]="";
				for(int i=1; i <=columnCount;i++){
					result[i]="uid="+columns.get(i-1);
				}
				return result;
			}else{
				return null;
			}
		}
	
	private static String[][] getTableDataByOrginalResult2(String[][] result,List<String> columnsList){
			int rowLength = result.length;
			int colLength = result[0].length;
			String[][] tableData = new  String[rowLength][colLength+1];
			for(int rowIndex=0;rowIndex< rowLength;rowIndex++){
				tableData[rowIndex][0]="PID="+columnsList.get(rowIndex);
				for(int colIndex=1;colIndex<=colLength;colIndex ++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
					}
				}
			return tableData;
		}
	//==================================table2 Export====================


	//==================================table3 Export====================
	public static boolean ExportTable3(String curInterviewFilePath,String exportPath){	
		GenterOption option = new GenterOption();
		option.setGenerType(3); 
		
		List<String>  ivFileNames = new ArrayList<String>();
		ivFileNames.add(curInterviewFilePath);
		option.setIvFileNames(ivFileNames);
		
		KnnWrapper knngenerImpl3 = new KnnWrapper(option);

		File file = new File(exportPath);
		
		String[][] exportArrayData = knngenerImpl3.getResult();
		String[] exportHeadArray = genTableCol3(knngenerImpl3.getHeader());
		boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
		return exportResult;
	}
	
	private static String[] genTableCol3(List<String> headers){
			int colArrayLength  = headers.size();
			String[] colArray = new String[colArrayLength];
			for(int firstRowColIndex = 0; firstRowColIndex<colArrayLength; firstRowColIndex++){
				colArray[firstRowColIndex] = headers.get(firstRowColIndex);
			}
			return colArray;
		}
	//==================================table3 Export====================


	//==================================table4 Export====================	
	public static boolean ExportTable4(String curInterviewFilePath,String exportPath){	
		GenterOption option = new GenterOption();
		option.setGenerType(4); 
		
		List<String>  ivFileNames = new ArrayList<String>();
		ivFileNames.add(curInterviewFilePath);
		option.setIvFileNames(ivFileNames);
		
		KnnWrapper knngenerImpl4 = new KnnWrapper(option);
		
		File file = new File(exportPath);
		
		String[][] exportArrayData = knngenerImpl4.getResult();
		String[] exportHeadArray = genTableCol4(knngenerImpl4.getHeader());
		boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
		return exportResult;	
	}
		
		
	private static String[] genTableCol4(List<String> headers){
			int colArrayLength  = headers.size();
			String[] colArray = new String[colArrayLength];
			for(int firstRowColIndex = 0; firstRowColIndex<colArrayLength; firstRowColIndex++){
				colArray[firstRowColIndex] = headers.get(firstRowColIndex);
			}
			return colArray;
		}
	//==================================table4 Export====================


	//==================================table5 Export====================
		public static boolean ExportTable5(String curInterviewFilePath,String exportPath){
			GenterOption option = new GenterOption();
			option.setGenerType(5); 
			
			List<String>  ivFileNames = new ArrayList<String>();
			ivFileNames.add(curInterviewFilePath);
			option.setIvFileNames(ivFileNames);
			
			KnnWrapper knngenerImpl5 = new KnnWrapper(option);
			
			File file = new File(exportPath);
			
			String[][] exportArrayData = knngenerImpl5.getResult();
			String[] exportHeadArray = genTableCol5(knngenerImpl5.getHeader());
			boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
			return exportResult;	
		}
		
		private static String[] genTableCol5(List<String> headers){
			int colArrayLength  = headers.size();
			String[] colArray = new String[colArrayLength];
			for(int firstRowColIndex = 0; firstRowColIndex<colArrayLength; firstRowColIndex++){
				colArray[firstRowColIndex] = headers.get(firstRowColIndex);
			}
			return colArray;
		}
	//==================================table5 Export====================



	//==================================table6 Export====================
	public static boolean ExportTable6(String curInterviewFilePath,String exportPath){
			GenterOption option = new GenterOption();
			option.setGenerType(6); 
			
			List<String>  ivFileNames = new ArrayList<String>();
			ivFileNames.add(curInterviewFilePath);
			option.setIvFileNames(ivFileNames);
			
			KnnWrapper knngenerImpl6 = new KnnWrapper(option);
			
			File file = new File(exportPath);
			
			String[][] exportArrayData = getTableDataByOrginalResult6(knngenerImpl6.getResult(),knngenerImpl6.getColumns());
			String[] exportHeadArray =  getColumnsArrayByColumnsList6(knngenerImpl6.getHeader());
			boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
			return exportResult;	
	}
		
	private static String[] getColumnsArrayByColumnsList6(List<String> columns) {
			if(columns!=null){
				int columnCount= columns.size();
				String[] result =new String[columnCount+1];
				result[0]="";
				for(int i=1; i <=columnCount;i++){
					result[i]="pid="+columns.get(i-1);
				}
				return result;
			}else{
				return null;
			}
		};

	private static String[][] getTableDataByOrginalResult6(String[][] result,List<String> columnsList){
			int rowLength = result.length;
			int colLength = result[0].length;
			String[][] tableData = new  String[rowLength][colLength+1];
			for(int rowIndex=0;rowIndex< rowLength;rowIndex++){
				tableData[rowIndex][0]="pid="+columnsList.get(rowIndex);
				for(int colIndex=1;colIndex<=colLength;colIndex ++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
					}
				}
			return tableData;
		}
	//==================================table6 Export====================


	//==================================table7 Export====================
		public static boolean ExportTable7(String curInterviewFilePath,String exportPath){
			GenterOption option = new GenterOption();
			option.setGenerType(7); 
			
			List<String>  ivFileNames = new ArrayList<String>();
			ivFileNames.add(curInterviewFilePath);
			option.setIvFileNames(ivFileNames);
			
			KnnWrapper knngenerImpl7 = new KnnWrapper(option);
			File file = new File(exportPath);
			
			String[][] exportArrayData = getTableDataByOrginalResult7(knngenerImpl7.getResult(),knngenerImpl7.getColumns());
			String[] exportHeadArray = getColumnsArrayByColumnsList7(knngenerImpl7.getHeader());
			boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
			return exportResult;	
			}
		
		
		private static String[] getColumnsArrayByColumnsList7(List<String> columns) {
			if(columns!=null){
				int columnCount= columns.size();
				String[] result =new String[columnCount+1];
				result[0]="End person pid";
				int last1Index = columns.lastIndexOf("1");
				for(int i=1; i <=columnCount;i++){
					if(i <=last1Index ){
						result[i]="step"+columns.get(i-1)+"(Letter labels)";
					}else{
						result[i]="step"+columns.get(i-1)+"(Number codes)";
					}
				}
				return result;
			}else{
				return null;
			}
		};
		
	public static String[][] getTableDataByOrginalResult7(String[][] result,List<String> columnsList){	
			int rowLength = result.length;
			int colLength = result[0].length;
			String[][] tableData = new  String[rowLength][colLength+1];
			for(int rowIndex=0;rowIndex< rowLength;rowIndex++){
				tableData[rowIndex][0]=columnsList.get(rowIndex);
				for(int colIndex=1;colIndex<=colLength;colIndex ++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
					}
				}
			return tableData;
		}
		

	//==================================table7 Export====================


	//==================================table8 Export====================
		public static boolean ExportTable8(String curInterviewFilePath,String exportPath){
			GenterOption option = new GenterOption();
			option.setGenerType(8); 
			
			List<String>  ivFileNames = new ArrayList<String>();
			ivFileNames.add(curInterviewFilePath);
			option.setIvFileNames(ivFileNames);
			
			KnnWrapper knngenerImpl8 = new KnnWrapper(option);
			
			File file = new File(exportPath);
			String[][] exportArrayData = genTableDataByResultData8(knngenerImpl8.getResult(),knngenerImpl8.getColumns());
			String[] exportHeadArray= genTableCol8(knngenerImpl8.getHeader());
		
			boolean exportResult = ExportCSVUtils.exportCSV(file, exportHeadArray,exportArrayData);
			return exportResult;	
			}
		
		private static String[] genTableCol8(List<String> headers){
			int colArrayLength  = headers.size();
			String[] colArray = new String[colArrayLength+1];
			colArray[0]="End person pid";
			colArray[1]="Kinship distance";
			
			for(int firstRowColIndex = 2; firstRowColIndex<=colArrayLength; firstRowColIndex++){
				colArray[firstRowColIndex] = headers.get(firstRowColIndex-1);
			}
			
			return colArray;
		}
		
		private static String[][]  genTableDataByResultData8(String[][] result,List<String> columns){
			int rowLength = result.length;
			int colLength = result[0].length;	
			
			String [][] tableData = new  String[rowLength][colLength+1];	
			for(int rowIndex = 0 ;  rowIndex <rowLength; rowIndex++){
				tableData[rowIndex][0] = columns.get(rowIndex);
				for(int colIndex = 1;colIndex <=colLength;colIndex++ ){
					tableData[rowIndex][colIndex] = result[rowIndex][colIndex-1];
				}
			}
			
			return  tableData;
		}
	//==================================table8 Export====================
}
