package com.fangda.kass.ui;

import java.awt.BorderLayout;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.fangda.kass.KnqMain;
import com.fangda.kass.ui.common.LoadingPanel;

/**
 * This class for the Base Panel in the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public abstract class BasePanel extends JPanel {

	private static final long serialVersionUID = -978296244691542739L;
	
	protected JPanel container;
	protected LoadingPanel loadingPanel;
	protected KnqMain km;

	public BasePanel() {

	}

	public BasePanel(String name, KnqMain km) {
		this.setName(name);
		this.km = km;
		init();
	}

	public void init() {
		initPanel();
		this.addContainerListener(containerListener);
		loadingPanel = new LoadingPanel("");
		loadingPanel.setVisible(false);
		container = new JPanel(new BorderLayout());
		container.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		container.add(createPanel(), BorderLayout.CENTER);
		this.add(container);
		this.setVisible(true);
	}

	public void setMainPanel(KnqMain km) {
		this.km = km;
	}

	private ContainerListener containerListener = new ContainerListener() {

		@Override
		public void componentAdded(ContainerEvent e) {
			// TODO Auto-generated method stub
			afterShow();
		}

		@Override
		public void componentRemoved(ContainerEvent e) {
			// TODO Auto-generated method stub
		}

	};

	public void initPanel() {
	}

	public void afterShow() {

		
	}

	/**
	 * 创建对话框的中的页面
	 * 
	 * @return
	 */
	public abstract JPanel createPanel();

}
