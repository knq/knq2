package com.fangda.kass.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.KnqMain;
import com.fangda.kass.model.MessageModel;
import com.fangda.kass.ui.common.LoadingPanel;
import com.mxgraph.util.mxResources;

/**
 * This class for the Base Dialog in the KNQ2
 * 
 * @author Fangfang Zhao
 * 
 */
public class BaseDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = -696364362506680072L;

	Logger log = LoggerFactory.getLogger(BaseDialog.class);

	protected JPanel container;
	protected JButton closeButton;
	protected JButton sureButton;
	protected JButton sureNextButton;
	protected LoadingPanel loadingPanel;
	protected KnqMain km;
	protected Map<String, Object> params;
	protected int clickType = 1;//1=sure  2= sure next

	public BaseDialog() {
		this("");
	}

	public BaseDialog(String title) {
		this(title, 400, 500, null);
	}
	
	public BaseDialog(Map<String, Object> params) {
		this("", 800, 500, params);
	}
	
	public BaseDialog(String title, Map<String, Object> params) {
		this(title, 800, 500, params);
	}

	public BaseDialog(String title, int width, int height, Map<String, Object> params) {
		this.setTitle(title);
		this.setSize(width, height);
		this.params = params;
		init();
	}
	
	public void setMainPanel(KnqMain km) {
		this.km = km;
	}

	public void init() {
		initDialog();
		loadingPanel = new LoadingPanel("");
		loadingPanel.setVisible(false);
		this.setModal(true);//设置为模式对话框
		this.addComponentListener(componentlistener);
		container = new JPanel(new BorderLayout());
		container.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		container.add(createPanel(), BorderLayout.CENTER);
		container.add(createButtonPanel(), BorderLayout.SOUTH);
		this.afterInit();
		setContentPane(container);
		// 是调整窗口大小，尽量满足每一个组件的最优大小的情况下让窗口最小化
		pack();
		// 定义打开的位置
		setLocationRelativeTo(this.getParent());
		setResizable(true);
		setVisible(true);
		container.add(loadingPanel, 0);
		
	}
	
	protected void afterInit() {
		
	}

	protected ComponentListener componentlistener = new ComponentListener() {
		@Override
		public void componentResized(ComponentEvent e) {
			// TODO Auto-generated method stub
			windowResize(e);
		}

		@Override
		public void componentMoved(ComponentEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void componentShown(ComponentEvent e) {
			// TODO Auto-generated method stub
			afterShow();
		}

		@Override
		public void componentHidden(ComponentEvent e) {
			// TODO Auto-generated method stub
		}
	};

	/**
	 * 创建下边的窗口
	 * 
	 * @return
	 */
	public JPanel createButtonPanel() {
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		closeButton = new JButton(mxResources.get("cancel"));
		sureButton = new JButton(mxResources.get("sure"));
		sureNextButton = new JButton(mxResources.get("sure") + "&" + mxResources.get("next"));
		buttonPanel.add(closeButton);
		buttonPanel.add(sureButton);
		if(params != null && params.containsKey("sure_next")) {
			buttonPanel.add(sureNextButton);
		}
		closeButton.addActionListener(this);
		sureButton.addActionListener(this);
		sureNextButton.addActionListener(this);
		return buttonPanel;
	}

	/**
	 * 初始化窗口
	 */
	public void initDialog() {

	}

	/**
	 * 创建对话框的中的页面
	 * 
	 * @return
	 */
	public JPanel createPanel() {
		return null;
	};

	/**
	 * 点击确定的时候进行的操作
	 */
	public MessageModel sure() {
		MessageModel message = null;
		try {
			message = submit();
		} catch (Exception e) {
			e.printStackTrace();
			message = new MessageModel(false, mxResources.get("error")+": " + e.getMessage(), 1);
		}
		return message;
	}

	public boolean beforeValid() {
		return true;
	}

	public MessageModel submit() {

		return null;
	}
	public void afterSubmit() {
		
	}
	public void afterSubmit(ActionEvent e) {
		
	}

	/**
	 * 显示完成以后的
	 */
	public void afterShow() {

	}
	
	public void windowResize(ComponentEvent e) {

	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		// TODO Auto-generated method stub
		final Object o = e.getSource();
		if (o == closeButton) {
			this.setVisible(false);
			this.dispose();// 全部关闭释放资源
		}
		if (o == sureButton || o == sureNextButton) {
			if (beforeValid()) {
				loadingPanel.setVisible(true);
				new SwingWorker<Long, Void>() {
					@Override
					protected Long doInBackground() {
						MessageModel m = sure();
						loadingPanel.setVisible(false);
						
						if(m.getState() == 0) {
							UIHelper.showMessageDialog(container, m);
							setVisible(false);
							dispose();
						} else if(m.getState() == 1) {
							setVisible(false);
							dispose();
						}
						if(o == sureButton) {
							clickType = 1;
						} else if(o == sureNextButton) {
							clickType = 2;
						}
						afterSubmit();
						return 1L;
					}

					@Override
					protected void done() {
						
					}
				}.execute();
			}
		}
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
	public void setSureBtnVisible(boolean flag) {
		this.sureButton.setVisible(flag);
	}

}
