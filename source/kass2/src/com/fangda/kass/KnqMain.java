package com.fangda.kass;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import com.fangda.kass.model.interview.Project;
import com.fangda.kass.model.questionnaire.Questionnaire;
import com.fangda.kass.service.XmlHelper;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.questionnaire.QnMainPanel;
import com.fangda.kass.ui.questionnaire.base.QuestionnaireEditDialog;
import com.fangda.kass.ui.questionnaire.base.ReleaseDialog;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.mxgraph.util.mxResources;

/**
 * This class for the main interface of KNQ2.
 * 
 * @author Fangfang Zhao, Shuang Ma, Jianguo Zhou, Jing Kong
 * @version V2.0-Beta3.2
 */
public class KnqMain extends JPanel {

	private static final long serialVersionUID = 1536528550408420066L;
	public static KnqMain kmain = null;
	public JFrame frame = null;
	public JMenuBar menuBar;
	public IvMainPanel ivMainPanel = null; // Interview Management System IU
	public QnMainPanel qnMainPanel = null; // Questionnaire Management System IU

	private String afterShowEvent;
	public String questionairPath;

	public KnqMain() {
		super();
		kmain = this;
	}

	public void setTitle(String path) {
		if (frame != null) {
			String version = Constants.VERSION;
			Project project = new Project();
			ProjectService projectService = new ProjectService();
			if (Constants.PROJECT_XML != null && Constants.PROJECT_XML != "") {
				project = projectService.get();
				if (path != null) {
					frame.setTitle(mxResources.get("KNQ2") + version + "("
							+ project.getLan() + "-" + project.getCountry()
							+ ")" + path);
				} else {
					frame.setTitle(mxResources.get("KNQ2") + version + "("
							+ project.getLan() + "-" + project.getCountry()
							+ ")");
				}
			} else {
				if (path != null) {
					QuestionnaireService service = new QuestionnaireService();
					Questionnaire q = service.get();
					frame.setTitle(mxResources.get("KNQ2") + version + "("
							+ q.getLanguageName() + "-" + q.getCountryName()
							+ ")|" + path);
				} else {
					frame.setTitle(mxResources.get("KNQ2") + version);
				}
			}
		}
	}

	public void setAfterShowEvent(String event) {
		this.afterShowEvent = event;
	}

	/**
	 * The frame container defined
	 */
	public JFrame createFrame(JMenuBar bar, JPanel panel) {
		frame = new JFrame();
		frame.getContentPane().add(panel);//
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setJMenuBar(bar);// menu
		this.menuBar = bar;
		frame.getJMenuBar();
		frame.setSize(1024, 720);// size defined
		frame.setLocationRelativeTo(null);
		String version = Constants.VERSION;
		if (Constants.PROJECT != null) {
			Project project = new Project();
			ProjectService projectService = new ProjectService();
			project = projectService.get();
			if (project.getLan() != null && project.getCountry() != null) {
				frame.setTitle(mxResources.get("KNQ2") + version + "("
						+ project.getLan() + "-" + project.getCountry() + ")");
			} else {
				frame.setTitle(mxResources.get("KNQ2") + version);
			}
		} else {

		}

		try {
			String src = "/images/logo.png";
			Image image = ImageIO.read(this.getClass().getResource(src));
			frame.setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		frame.addComponentListener(componentListener);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				KassService kassService = new KassService();
				int status = kassService.isInterviewNullValid(kmain, 0);
				if (Constants.QUESTIONNAIRE_XML != null
						&& Constants.QUESTIONNAIRE_XML
								.contains(Constants.TMP_EXT)) {
					QuestionnaireService questionnaireService = new QuestionnaireService();
					questionnaireService.isQuestionnaireNullValid(kmain, 0);
				}
				if (Constants.isInterviewCancel == 1
						|| Constants.isQuestionnaireCancel == 1) {
				} else {
					if (status == 1) {
						kassService.exitValid(kmain, 1, 0, 0, false);
					} else {
						kassService.exitValid(kmain, 1, 0, 0, true);
					}
				}
			}
		});
		return frame;
	}

	public void setMainPanel(JPanel panel) {
		int status = Constants.QUESTIONNAIRE_STATUS;
		if (status == 11) {
			XmlHelper.getInstance().alertExitQn(kmain, "revise");
		}
		if (status == 12) {
			XmlHelper.getInstance().alertExitQn(kmain, "temprevise");
		}
		if (status == 13) {
			XmlHelper.getInstance().alertExitQn(kmain, "local");
		}
		Constants.QUESTIONNAIRE_STATUS = 0;
		JPanel jpanel = (JPanel) frame.getContentPane();
		jpanel.removeAll();
		jpanel.add(panel);
		jpanel.updateUI();
	}

	/**
	 * 
	 * @param name
	 * @param action
	 * @return a new Action bound to the specified string name
	 */
	public Action bind(String name, final Action action) {
		return bind(name, action, null);
	}

	/**
	 * 
	 * @param name
	 * @param action
	 * @return a new Action bound to the specified string name and icon
	 */
	@SuppressWarnings("serial")
	public Action bind(String name, final Action action, String iconUrl) {
		AbstractAction newAction = new AbstractAction(name,
				(iconUrl != null) ? new ImageIcon(
						KnqMain.class.getResource(iconUrl)) : null) {
			public void actionPerformed(ActionEvent e) {
				action.actionPerformed(new ActionEvent(kmain, e.getID(), e
						.getActionCommand()));
			}
		};

		newAction.putValue(Action.SHORT_DESCRIPTION,
				action.getValue(Action.SHORT_DESCRIPTION));

		return newAction;
	}

	private ComponentListener componentListener = new ComponentListener() {

		@Override
		public void componentResized(ComponentEvent e) {
		}

		@Override
		public void componentMoved(ComponentEvent e) {
		}

		@Override
		public void componentShown(ComponentEvent e) {
			if (StringUtil.isNotBlank(afterShowEvent)) {
				if ("revise".equals(afterShowEvent)) {
					new QuestionnaireEditDialog().setMainPanel(kmain);
				} else if ("release".equals(afterShowEvent)) {
					new ReleaseDialog().setMainPanel(kmain);
				}
			}
		}

		@Override
		public void componentHidden(ComponentEvent e) {
		}
	};
}
