package com.fangda.kass;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.PropertyResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.apache.commons.lang.StringUtils;

import com.fangda.kass.model.MessageModel;
import com.fangda.kass.model.config.IsoLangCode;
import com.fangda.kass.ui.BaseDialog;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.questionnaire.support.ComboItem;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.SystemUtil;
import com.mxgraph.util.mxResources;

/**
 * This class for the dialog of setting the system language of the KNQ2.
 * 
 * @author Fangfang Zhao, Jianguo Zhou
 * @version V2.0-Beta3.2
 */
public class SystemDialog extends BaseDialog implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2892303107658323951L;
	JPanel panel;
	protected JComboBox fieldLanguageBox;
	protected JComboBox fieldCountryBox;

	public String configPath;
	public String messagePath;

	@Override
	public JPanel createPanel() {

		fieldLanguageBox = new JComboBox(QuestionConstants.listLanguage());
		fieldCountryBox = new JComboBox(QuestionConstants.listCountry(""));

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel propPanel = new JPanel();
		propPanel.setBackground(Color.white);
		propPanel.setLayout(new BoxLayout(propPanel, BoxLayout.X_AXIS));
		propPanel.add(new JLabel("<html><strong>"
				+ mxResources.get("system.lan.select") + "</strong></html>"));
		propPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		panel.add(propPanel);

		GridBagLayout bagLayout = new GridBagLayout();
		JPanel projectPanel = new JPanel(bagLayout);
		projectPanel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();

		JPanel fieldContentPanel = new JPanel(bagLayout);
		fieldContentPanel.setBackground(Color.white);
		int fieldIndex = 0;
		UIHelper.addToBagpanel(
				fieldContentPanel,
				Color.white,
				new JLabel("<HTML><font color='red'>*</font>"
						+ mxResources.get("language") + ":</HTML>"), c, 0,
				fieldIndex, 1, 1, GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white,
				fieldLanguageBox, c, 1, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		fieldLanguageBox.addActionListener(this);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, new JLabel(
				"<HTML><font color='red'>*</font>" + mxResources.get("country")
						+ ":</HTML>"), c, 2, fieldIndex, 1, 1,
				GridBagConstraints.WEST);
		UIHelper.addToBagpanel(fieldContentPanel, Color.white, fieldCountryBox,
				c, 3, fieldIndex, 1, 1, GridBagConstraints.WEST);
		fieldIndex++;
		panel.add(fieldContentPanel);

		return panel;
	}

	@Override
	public boolean beforeValid() {
		return true;
	}

	@Override
	public void afterSubmit() {
		super.afterSubmit();
		ComboItem lanItem = (ComboItem) fieldLanguageBox.getSelectedItem();
		ComboItem countryItem = (ComboItem) fieldCountryBox.getSelectedItem();
		String lan = lanItem.getValue() + "_" + countryItem.getValue();

		String errorLan = "";
		KnqMain kqnM = KnqMain.kmain;
		if (kqnM != null) {
			kqnM.setVisible(false);
		}
		try {
			SystemUtil.set("lan", lan);
			String path = Constants.getUserPath() + File.separator;
			String configPath = path + "language" + File.separator + "config_"
					+ lan + ".properties";
			String messagePath = path + "language" + File.separator
					+ "message_" + lan + ".properties";
			File configFile = new File(configPath);
			File messageFile = new File(messagePath);

			if (!messageFile.exists() || !configFile.exists()) {
				configPath = "language" + File.separator
						+ "config_en_EN.properties";
				messagePath = "language" + File.separator
						+ "message_en_EN.properties";
				errorLan = "[" + lan + "]";
			}
			Constants.MESSAGE_PATH = messagePath;
			Constants.CONFIG_PATH = configPath;
			try {
				mxResources.getBundles().clear();
				InputStream is = new FileInputStream(Constants.MESSAGE_PATH);
				mxResources.add(new PropertyResourceBundle(is));
				QuestionConstants.loadConfig();
			} catch (Exception e) {
				e.printStackTrace();
				// ignore
			}

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		StartMain sm = StartMain.getInstance();
		sm.setVisible(false);
		StartMain.clear();
		StartMain.getInstance().setVisible(true);
		if (StringUtils.isNotBlank(errorLan)) {
			JOptionPane.showMessageDialog(StartMain.getInstance(), "Langeuage "
					+ errorLan + " not exist, use default en_EN");
		}
		if (kqnM != null) {
			kqnM.frame.dispose();
		}
		if (sm != null) {
			sm.dispose();
		}
	}

	@Override
	public void afterShow() {
		String b = SystemUtil.get("lan", "en_EN");
		b = b.replace("_", "-");
		IsoLangCode lang = QuestionConstants.getLangByCode(b);
		if (lang != null) {
			fieldLanguageBox.setSelectedIndex(QuestionConstants
					.getLanguageIndex(lang.getLanguageName()));
			ComboItem filedItem = (ComboItem) fieldLanguageBox
					.getSelectedItem();
			DefaultComboBoxModel model = (DefaultComboBoxModel) fieldCountryBox
					.getModel();
			Object[] countryObjs = QuestionConstants.listCountry(filedItem
					.getValue());
			int index = -1;
			for (int i = 0; i < countryObjs.length; i++) {
				ComboItem ci = (ComboItem) countryObjs[i];
				if (StringUtils.equalsIgnoreCase(ci.getName(),
						lang.getCountryName())) {
					index = i;
					break;
				}
			}
			fieldCountryBox.setSelectedIndex(index);
		}
	}

	@Override
	public MessageModel submit() {

		return new MessageModel(true, "Submit success!");
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		super.actionPerformed(e);
		Object o = e.getSource();
		if (o == fieldLanguageBox) {
			ComboItem item = (ComboItem) fieldLanguageBox.getSelectedItem();
			DefaultComboBoxModel model = (DefaultComboBoxModel) fieldCountryBox
					.getModel();
			model.removeAllElements();
			Object[] obj = QuestionConstants.listCountry(item.getValue());
			for (int i = 0; i < obj.length; i++) {
				model.addElement(obj[i]);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		Object o = e.getSource();

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

}
