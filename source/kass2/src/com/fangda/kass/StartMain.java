package com.fangda.kass;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fangda.kass.model.interview.KASS;
import com.fangda.kass.model.interview.KFieldSite;
import com.fangda.kass.model.interview.Project;
import com.fangda.kass.service.interview.KassService;
import com.fangda.kass.service.interview.ProjectService;
import com.fangda.kass.service.questionnaire.QuestionnaireService;
import com.fangda.kass.service.workspace.WorkSpaceService;
import com.fangda.kass.ui.UIHelper;
import com.fangda.kass.ui.common.DefaultMenuBar;
import com.fangda.kass.ui.interview.IvAction.FileChooser;
import com.fangda.kass.ui.interview.IvAction.OpenRecentProjectAction;
import com.fangda.kass.ui.interview.IvMainPanel;
import com.fangda.kass.ui.interview.project.ProjectEditDialog;
import com.fangda.kass.ui.interview.project.UserPathDialog;
import com.fangda.kass.ui.questionnaire.QnMainPanel;
import com.fangda.kass.ui.questionnaire.base.ExportDialog;
import com.fangda.kass.ui.questionnaire.support.QuestionConstants;
import com.fangda.kass.util.ConfigUtil;
import com.fangda.kass.util.Constants;
import com.fangda.kass.util.StringUtil;
import com.fangda.kass.util.SystemUtil;
import com.mxgraph.util.mxResources;// The static method add the message for i18n

/**
 * This class is the main class to start the first Welcome page of the KNQ2.
 * 
 * @author Fangfang Zhao, Shuang Ma, Jianguo Zhou, Jing Kong
 * @version V2.0-Beta3.2
 */

public class StartMain extends JFrame {
	
	private static final long serialVersionUID = -6325728180154256346L;

	public static Logger logger = LoggerFactory.getLogger(StartMain.class);

	private static StartMain startMain = null;

	ProjectService projectService;

	QuestionnaireService questionnaireService;

	// static {
	// try {
	// mxResources.add(Constants.MESSAGE_PATH);
	// QuestionConstants.loadConfig();
	// } catch (Exception e) {
	// e.printStackTrace();
	// // ignore
	// }
	// }

	public static StartMain getInstance() {
		if (startMain == null) {
			startMain = new StartMain();
		}
		return startMain;
	}

	public static void clear() {
		startMain = null;
	}

	private StartMain() {
		projectService = new ProjectService();
		questionnaireService = new QuestionnaireService();
		setLayout(new BorderLayout());
		createCenterPanel();
		((JPanel) this.getContentPane()).setOpaque(false);
		this.setSize(800, 600);
		this.setLocation(100, 100);
		String version = Constants.VERSION;
		this.setTitle(mxResources.get("KNQ2") + version);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		try {
			String src = "/images/logo.png";
			Image image = ImageIO.read(this.getClass().getResource(src));
			this.setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void createCenterPanel() {
		GridBagLayout bagLayout = new GridBagLayout();
		final JPanel panel = new JPanel(bagLayout) {
			private static final long serialVersionUID = 5415686911468910362L;

			protected void paintComponent(Graphics g) {
				ImageIcon icon = UIHelper.getImage("welcome.jpg");
				Image img = icon.getImage();
				g.drawImage(img, 0, 0, icon.getIconWidth(),
						icon.getIconHeight(), icon.getImageObserver());
			}
		};
		JLabel lb = new JLabel(
				"<html><br><br><br><br><br><br><br><br><br><br><br><br><span style='color:#FF9933;font-size:40;font-weight:bold;'>"
						+ mxResources.get("welcomeKNQ2") + "</span></html>");
		JPanel jp1 = new JPanel();
		jp1.setLayout(new FlowLayout(FlowLayout.CENTER));
		jp1.add(lb);
		jp1.setOpaque(false);
		panel.add(jp1, new GridBagConstraints(0, 0, 2, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						0, 0, 0, 0), 0, 0));

		JButton button1 = new JButton(mxResources.get("interview"));
		button1.setPreferredSize(new Dimension(160, 80));
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] e1 = { mxResources.get("projectOpenButton"),
						mxResources.get("projectNewButton"),
						mxResources.get("interviewOpenButton") };
				int response = JOptionPane.showOptionDialog(startMain,
						mxResources.get("interviewStartTip"),
						mxResources.get("interviewStartTitle"),
						JOptionPane.DEFAULT_OPTION,
						JOptionPane.WARNING_MESSAGE, null, e1, e1[0]);
				if (response == 0) {
					JFileChooser chooser = new JFileChooser(".");

					String defaultPath = WorkSpaceService
							.SetProjectDefaultDir();
					chooser.setCurrentDirectory(new File(defaultPath));
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "project");
					int i = chooser.showOpenDialog(getContentPane());
					if (i == JFileChooser.APPROVE_OPTION) {
						String path = chooser.getSelectedFile()
								.getAbsolutePath();
						Constants.PROJECT_XML = path;
						Project project = new Project();
						try {
							project = projectService.get();
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openProjectXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);
						}

						Constants.QUESTIONNAIRE_XML = project
								.getQuestionnaireDirectory();
						questionnaireService.get();
						if (!Constants.QUESTIONNAIRE.getStatus().equals(
								"Formal")) {
							JOptionPane.showMessageDialog(panel, "<HTML>"
									+ mxResources.get("FormalCheck")
									+ "</HTML>", "Warning",
									JOptionPane.INFORMATION_MESSAGE,
									UIHelper.getImage("warning.png"));
						} else {
							StartMain.getInstance().setVisible(false);
							KnqMain kmain = new KnqMain();
							kmain.setLocation(100, 100);
							JPanel jpanel = new JPanel();
							kmain.createFrame(new DefaultMenuBar(kmain), jpanel)
									.setVisible(true);
							StartMain.getInstance().dispose();

							Preferences pref = Preferences.userRoot().node(
									"project");
							File file = chooser.getSelectedFile(); // 得到选择的文件
							int result = 0;
							try {
								for (int j = 0; j < pref.keys().length + 1; j++) {
									// Open a project file which is recently
									// opened.
									if (file.getPath().equals(
											pref.get(String.valueOf(j), ""))) {
										String tempPath = pref.get(
												String.valueOf(j), "");
										for (int k = j; k > 1; k--) {
											pref.put(String.valueOf(k), pref
													.get(String.valueOf(k - 1),
															""));
										}
										pref.put(String.valueOf(1), tempPath);
										result = 1;
										break;
									}
								}
							} catch (BackingStoreException e4) {
								// TODO Auto-generated catch block
								e4.printStackTrace();
							}
							// Open a project file which is not recently opened.
							if (result == 0) {
								try {
									// Limit the number of the recent project
									// files
									if (pref.keys().length < 10) {
										for (i = pref.keys().length + 1; i > 1; i--) {
											pref.put(String.valueOf(i), pref
													.get(String.valueOf(i - 1),
															""));
										}
										pref.put(String.valueOf(1),
												file.getPath());
									} else {
										for (i = pref.keys().length; i > 1; i--) {
											pref.put(String.valueOf(i), pref
													.get(String.valueOf(i - 1),
															""));
										}
										pref.put(String.valueOf(1),
												file.getPath());
									}
								} catch (BackingStoreException e2) {
									// TODO Auto-generated catch block
									e2.printStackTrace();
								}
							} else {
							}
							try {
								String p = Constants.getUserLogPath()
										+ File.separator + "projectLog.xml";
								FileOutputStream fos = new FileOutputStream(p);
								pref.exportNode(fos);
							} catch (Exception e3) {
								System.err.println(" Cannot export nodes:  "
										+ e3);
							}

							kmain.menuBar.getMenu(0).getItem(3).removeAll();
							JMenu submenu = (JMenu) kmain.menuBar.getMenu(0)
									.getItem(3);
							List<String> recentProjectPathList = WorkSpaceService
									.getRecentProjectPathList();
							// Collections.reverse(recentProjectPathList);
							for (int j = 0; j < recentProjectPathList.size(); j++) {
								submenu.add(kmain.bind(
										recentProjectPathList.get(j),
										new OpenRecentProjectAction(),
										"/images/open.gif"));
							}

						}

					}
				}
				if (response == 1) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("op", "add");
					new ProjectEditDialog(map);
				}
				if (response == 2) {
					JFileChooser chooser = new JFileChooser(".");
					chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					FileChooser choose = new FileChooser();
					choose.FileChooser(chooser, "xml");
					int i = chooser.showOpenDialog(getContentPane());
					if (i == JFileChooser.APPROVE_OPTION) {
						String defaultInterviewPath = WorkSpaceService
								.SetInterviewDefaultDir();
						chooser.setCurrentDirectory(new File(
								defaultInterviewPath));

						String path = chooser.getSelectedFile()
								.getAbsolutePath();

						KassService kassService = new KassService();
						String tmpPath = "";
						try {
							tmpPath = kassService.openRealXmlFile(path);
						} catch (Exception e11) {
							e11.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openInterviewXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);
							return;
						}

						Constants.KASS_XML = tmpPath;
						KASS kass = new KASS();
						kass = kassService.get();
						KFieldSite fieldSite = new KFieldSite();
						fieldSite = kass.getFieldSite();
						Constants.PROJECT_XML = fieldSite.getProjectDirector()
								+ File.separator + fieldSite.getPlaceName()
								+ ".project";
						Project project = new Project();
						try {
							project = projectService.get();
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
							JOptionPane.showMessageDialog(null,
									mxResources.get("openProjectXMLError"),
									"Error", JOptionPane.ERROR_MESSAGE);
						}
						Constants.QUESTIONNAIRE_XML = project
								.getQuestionnaireDirectory();
						questionnaireService.get();
						if (!Constants.QUESTIONNAIRE.getStatus().equals(
								"Formal")) {
							JOptionPane.showMessageDialog(panel, "<HTML>"
									+ mxResources.get("FormalCheck")
									+ "</HTML>", "Warning",
									JOptionPane.INFORMATION_MESSAGE,
									UIHelper.getImage("warning.png"));
						} else {
							Preferences pref = Preferences.userRoot().node(
									"interview");
							File file = chooser.getSelectedFile(); // 得到选择的文件
							int result = 0;
							try {
								for (int j = 0; j < pref.keys().length + 1; j++) {
									// Open an interview xml file which is
									// recently opened.
									if (file.getPath().equals(
											pref.get(String.valueOf(j), ""))) {
										String tempPath = pref.get(
												String.valueOf(j), "");
										for (int k = j; k > 1; k--) {
											pref.put(String.valueOf(k), pref
													.get(String.valueOf(k - 1),
															""));
										}
										pref.put(String.valueOf(1), tempPath);
										result = 1;
										break;
									}
									/* System.out.println(j); */
								}
							} catch (BackingStoreException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
								JOptionPane.showMessageDialog(null, mxResources
										.get("openInterviewXMLError"), "Error",
										JOptionPane.ERROR_MESSAGE);

							}
							// Open an interview xml file which is not recently
							// opened.
							if (result == 0) {
								try {
									// Limit the number of the recent interview
									// files
									if (pref.keys().length < 10) {
										for (i = pref.keys().length + 1; i > 1; i--) {
											pref.put(String.valueOf(i), pref
													.get(String.valueOf(i - 1),
															""));
										}
										pref.put(String.valueOf(1),
												file.getPath());
									} else {
										for (i = pref.keys().length; i > 1; i--) {
											pref.put(String.valueOf(i), pref
													.get(String.valueOf(i - 1),
															""));
										}
										pref.put(String.valueOf(1),
												file.getPath());
									}
								} catch (BackingStoreException e3) {
									// TODO Auto-generated catch block
									e3.printStackTrace();
									JOptionPane
											.showMessageDialog(
													null,
													mxResources
															.get("openInterviewXMLError"),
													"Error",
													JOptionPane.ERROR_MESSAGE);

								}
							} else {
							}
							try {
								String path1 = Constants.getUserLogPath()
										+ File.separator + "interviewLog.xml";
								FileOutputStream fos = new FileOutputStream(
										path1);
								pref.exportSubtree(fos);
							} catch (Exception e4) {
								System.err.println(" Cannot export nodes:  "
										+ e1);
								JOptionPane.showMessageDialog(null, mxResources
										.get("saveInterviewLogError"), "Error",
										JOptionPane.ERROR_MESSAGE);

							}
							StartMain.getInstance().setVisible(false);
							KnqMain km = new KnqMain();
							km.setLocation(100, 100);
							JPanel jpanel = new JPanel();
							km.createFrame(new DefaultMenuBar(km), jpanel)
									.setVisible(true);
							StartMain.getInstance().dispose();
							km.ivMainPanel = new IvMainPanel(km);
							km.setMainPanel(km.ivMainPanel);
						}
					}
				}
			}
		});
		JButton button2 = new JButton(mxResources.get("questionnaireView"));
		button2.setPreferredSize(new Dimension(160, 80));
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				openQuestionnaire("view");
			}
		});
		JButton button3 = new JButton(mxResources.get("questionnaireRevise"));
		button3.setPreferredSize(new Dimension(160, 80));
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openQuestionnaire("revise");

				Constants.QUESTIONNAIRE_STATUS = 11;

			}
		});

		JButton button4 = new JButton(mxResources.get("reversionRelease"));
		button4.setPreferredSize(new Dimension(160, 80));
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openQuestionnaire("release");
			}
		});

		JButton button5 = new JButton(mxResources.get("questionExport"));
		button5.setPreferredSize(new Dimension(160, 80));
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// new ImportDialog();
				new ExportDialog();
			}
		});

		JButton button6 = new JButton(mxResources.get("setLocalVersion"));
		// JButton button6 = new JButton(mxResources.get("questionExport"));
		button6.setPreferredSize(new Dimension(160, 80));
		button6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SystemDialog();
			}
		});

		panel.add(button1, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		panel.add(button2, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		panel.add(button3, new GridBagConstraints(2, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		panel.add(button4, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		panel.add(button5, new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		panel.add(button6, new GridBagConstraints(2, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.NORTH,
				new Insets(10, 10, 10, 10), 0, 0));
		this.add(panel, BorderLayout.CENTER);
	}

	private void openQuestionnaire(String type) {
		JFileChooser chooser = new JFileChooser(".");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int i = chooser.showOpenDialog(getContentPane());
		if (i == JFileChooser.APPROVE_OPTION) {
			String path = chooser.getSelectedFile().getAbsolutePath();
			String questionName = StringUtil.substringAfterLast(path, "\\");
			StartMain.getInstance().setVisible(false);
			KnqMain kmain = new KnqMain();
			kmain.questionairPath = questionName;
			kmain.qnMainPanel = new QnMainPanel(kmain);
			kmain.qnMainPanel.ifShowDetail = false;
			kmain.setLocation(100, 100);
			kmain.createFrame(new DefaultMenuBar(kmain), kmain.qnMainPanel)
					.setVisible(true);
			kmain.setAfterShowEvent(type);
			kmain.qnMainPanel.reload(path, type);
			kmain.setTitle(questionName);
			StartMain.getInstance().dispose();
		}
	}

	private void openInterview() {
		StartMain.getInstance().setVisible(false);
		KnqMain kmain = new KnqMain();
		kmain.ivMainPanel = new IvMainPanel(kmain);
		kmain.setLocation(100, 100);
		kmain.createFrame(new DefaultMenuBar(kmain), kmain.ivMainPanel)
				.setVisible(true);
		StartMain.getInstance().dispose();
	}

	public static void main(String[] args) {
		String errorLan = "";
		try {
			// The default path
			InputStream is1 = new FileInputStream(Constants.MESSAGE_PATH);
			mxResources.add(new PropertyResourceBundle(is1));

			String userConfigPath1 = "sys.properties";
			File configFile1 = new File(userConfigPath1);
			boolean showPath = false;
			if (!configFile1.exists()) {
				configFile1.createNewFile();
				showPath = true;
			} else {
				String path = ConfigUtil.get("path", "");
				if (StringUtils.isNotBlank(path)) {
					showPath = false;
					Constants.setUserPath(path);
				} else {
					showPath = true;
				}
			}
			if (showPath) {
				new UserPathDialog();
			}
			if (StringUtils.isBlank(Constants.getUserPath())) {
				System.exit(-1);
				return;
			}

			String lan = SystemUtil.get("lan", "");// en_EN
			String configPath = Constants.getUserPath() + File.separator
					+ "language" + File.separator + "config_" + lan
					+ ".properties";
			String messagePath = Constants.getUserPath() + File.separator
					+ "language" + File.separator + "message_" + lan
					+ ".properties";
			File configFile = new File(configPath);
			File messageFile = new File(messagePath);

			if (!messageFile.exists() || !configFile.exists()) {
				configPath = Constants.getUserPath() + File.separator
						+ "language" + File.separator
						+ "config_en_EN.properties";
				messagePath = Constants.getUserPath() + File.separator
						+ "language" + File.separator
						+ "message_en_EN.properties";
				errorLan = "[" + lan + "]";
			}
			Constants.MESSAGE_PATH = messagePath;
			Constants.CONFIG_PATH = configPath;
			try {
				InputStream is = new FileInputStream(Constants.MESSAGE_PATH);
				mxResources.add(new PropertyResourceBundle(is));
				QuestionConstants.loadConfig();
			} catch (Exception e) {
				e.printStackTrace();
				// ignore
			}

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		StartMain.getInstance().setVisible(true);
		if (StringUtils.isNotBlank(errorLan)) {
			JOptionPane.showMessageDialog(StartMain.getInstance(), "Langeuage "
					+ errorLan + " not exist, use default en_EN");
		}

	}
}
