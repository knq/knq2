package com.fangda.kass.model.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the field sub-element of the relevanceType
 * element in the config.properties file in XML form, which is used for setting
 * of the type of relevance.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RelevanceTypeField extends Field {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4231447275943878795L;
	
	/**
	 * The condition attribute: the condition of the relevance type.
	 */
	@XmlAttribute(name = "condition")
	private String condition;

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
