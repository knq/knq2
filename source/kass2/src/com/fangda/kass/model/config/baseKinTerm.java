package com.fangda.kass.model.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the baseKinTerm sub-element of the kinshipTerm
 * element in the config.properties file in XML form, which is used for the
 * kinship term of kinship between the inputting person and Ego for the step
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class baseKinTerm {
	
	/**
	 * The numericCode attribute: the numeric Code of the kinship term
	 */
	@XmlAttribute(name = "numericCode")
	private int numericCode;

	/**
	 * The letterLable attribute: the letter label of the kinship term
	 */
	@XmlAttribute(name = "letterLable")
	private String letterLable;

	/**
	 * The kinTerm attribute: the kinship term
	 */
	@XmlAttribute(name = "kinTerm")
	private String kinTerm;

	public int getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(int numericCode) {
		this.numericCode = numericCode;
	}

	public String getletterLable() {
		return letterLable;
	}

	public void setletterLable(String letterLable) {
		this.letterLable = letterLable;
	}

	public String getkinTerm() {
		return kinTerm;
	}

	public void setkinTerm(String kinTerm) {
		this.kinTerm = kinTerm;
	}
}
