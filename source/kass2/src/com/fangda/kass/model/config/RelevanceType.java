package com.fangda.kass.model.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the relevanceType sub-element of the config
 * element in the config.properties file in XML form, which is used for the
 * relevance type of the person to who the question apply in a questionnaire.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RelevanceType {

	/**
	 * The field element: Generic storage element for any attributes of the
	 * configure of the setting.
	 */
	@XmlElement(name = "field")
	private List<RelevanceTypeField> fields;

	public void setFields(List<RelevanceTypeField> fields) {
		this.fields = fields;
	}

	public List<RelevanceTypeField> getFields() {
		if (fields == null) {
			fields = new ArrayList<RelevanceTypeField>();
		}
		return fields;
	}
}
