package com.fangda.kass.model.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the languageCountry sub-element of the config
 * element in the config.properties file in XML form, which is the detail
 * description of the iso language code and country code of questionnaire.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LanguageCountry {

	/**
	 * The isoLangCode element: it is the detail description of the iso language
	 * code and country code of questionnaire.
	 */
	@XmlElement(name = "isoLangCode")
	private List<IsoLangCode> isoLangCodes;

	public List<IsoLangCode> getIsoLangCodes() {
		return isoLangCodes;
	}

	public void setIsoLangCodes(List<IsoLangCode> isoLangCodes) {
		this.isoLangCodes = isoLangCodes;
	}
}
