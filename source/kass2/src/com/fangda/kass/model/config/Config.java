package com.fangda.kass.model.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class stores / retrieves the root element (config) in the
 * config.properties file in XML form
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlRootElement(name = "config")
@XmlAccessorType(XmlAccessType.FIELD)
public class Config {

	/**
	 * The languageCountry element: it is a placeholder element for the
	 * isoLangCode element for the detail description of the iso language code
	 * and country code of questionnaire.
	 */
	@XmlElement(name = "languageCountry")
	private LanguageCountry languageCountry;

	/**
	 * The relevanceType element: the relevance type of the person to who the
	 * question apply in a questionnaire.
	 */
	@XmlElement(name = "relevanceType")
	private RelevanceType relevanceType;

	/**
	 * The disableType element: the disable setting of question. 0=Required,
	 * 1=Enabled, 2=Disabled.
	 */
	@XmlElement(name = "disableType")
	private DisableType disableType;

	/**
	 * The leadQuestionType element: the type of lead Question
	 */
	@XmlElement(name = "leadQuestionType")
	private LeadQuestionType leadQuestionType;

	/**
	 * The followupQuestionType element: the type of follow up Question
	 */
	@XmlElement(name = "followupQuestionType")
	private FollowupQuestionType followupQuestionType;

	/**
	 * The optionLayout element: the layout setting of the answer options of
	 * questions.
	 */
	@XmlElement(name = "optionLayout")
	private OptionLayout optionLayout;

	/**
	 * The leadQuestionTypeOld element: the type of lead Question in the old
	 * version of KNQ
	 */
	@XmlElement(name = "leadQuestionTypeOld")
	private LeadQuestionTypeOld leadQuestionTypeOld;

	/**
	 * The sex element: the Gender of the person, 0 = Male, 1 = Female
	 */
	@XmlElement(name = "sex")
	private Sex sex;

	/**
	 * The status element: the completed status of questionnaire（Formal or
	 * Draft）
	 */

	@XmlElement(name = "status")
	private Status status;

	/**
	 * The dataType element: the data type of the inputting data.
	 */
	@XmlElement(name = "dataType")
	private DataType dataType;

	/**
	 * The layout element: the layout setting of the a group of questions.
	 */
	@XmlElement(name = "layout")
	private Layout layout;

	/**
	 * The directionType element:the setting of question direction. N = No
	 * direction, R = Help received, G = Help given
	 */
	@XmlElement(name = "directionType")
	private DirectionType directionType;

	/**
	 * The sectionMode element: the question mode of the section. There are four
	 * modes: draw, map, question and moreDetail.
	 */
	@XmlElement(name = "sectionMode")
	private SectionMode sectionMode;

	/**
	 * The kinshipTerm element: the kinship term of kinship between the
	 * inputting person and Ego for the step.
	 */
	@XmlElement(name = "kinshipTerm")
	private KinshipTerm kinshipTerm;

	/**
	 * The answerCode element: the code of the answer of the question. For
	 * example: 1 = true, 0 = false, -98 = NA (No Answer), -99 = DNA (Don't
	 * apply answer)
	 */
	@XmlElement(name = "answerCode")
	private AnswerCode answerCode;

	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public SectionMode getSectionMode() {
		return sectionMode;
	}

	public void setSectionMode(SectionMode sectionMode) {
		this.sectionMode = sectionMode;
	}

	public LanguageCountry getLanguageCountry() {
		return languageCountry;
	}

	public void setLanguageCountry(LanguageCountry languageCountry) {
		this.languageCountry = languageCountry;
	}

	public RelevanceType getRelevanceType() {
		return relevanceType;
	}

	public void setRelevanceType(RelevanceType relevanceType) {
		this.relevanceType = relevanceType;
	}

	public DisableType getDisableType() {
		return disableType;
	}

	public void setDisableType(DisableType disableType) {
		this.disableType = disableType;
	}

	public LeadQuestionType getLeadQuestionType() {
		return leadQuestionType;
	}

	public void setLeadQuestionType(LeadQuestionType leadQuestionType) {
		this.leadQuestionType = leadQuestionType;
	}

	public FollowupQuestionType getFollowupQuestionType() {
		return followupQuestionType;
	}

	public void setFollowupQuestionType(
			FollowupQuestionType followupQuestionType) {
		this.followupQuestionType = followupQuestionType;
	}

	public OptionLayout getOptionLayout() {
		return optionLayout;
	}

	public void setOptionLayout(OptionLayout optionLayout) {
		this.optionLayout = optionLayout;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public DirectionType getDirectionType() {
		return directionType;
	}

	public void setDirectionType(DirectionType directionType) {
		this.directionType = directionType;
	}

	public LeadQuestionTypeOld getLeadQuestionTypeOld() {
		return leadQuestionTypeOld;
	}

	public void setLeadQuestionTypeOld(LeadQuestionTypeOld leadQuestionTypeOld) {
		this.leadQuestionTypeOld = leadQuestionTypeOld;
	}

	public KinshipTerm getKinshipTerm() {
		return kinshipTerm;
	}

	public void setKinshipTerm(KinshipTerm kinshipTerm) {
		this.kinshipTerm = kinshipTerm;
	}

	public AnswerCode getAnswerCode() {
		return answerCode;
	}

	public void setAnswerCode(AnswerCode answerCode) {
		this.answerCode = answerCode;
	}

}
