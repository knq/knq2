package com.fangda.kass.model.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the kinshipTerm sub-element of the config
 * element in the config.properties file in XML form, which is used for the
 * kinship term of kinship between the inputting person and Ego for the step
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KinshipTerm {

	/**
	 * The baseKinTerm element: the setting of kinship term of kinship between
	 * the inputting person and Ego for the step.
	 */
	@XmlElement(name = "baseKinTerm")
	private List<baseKinTerm> baseKinTerms;

	public List<baseKinTerm> getBaseKinTerms() {
		return baseKinTerms;
	}

	public void setBaseKinTerms(List<baseKinTerm> baseKinTerms) {
		this.baseKinTerms = baseKinTerms;
	}
}
