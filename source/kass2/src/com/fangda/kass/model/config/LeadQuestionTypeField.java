package com.fangda.kass.model.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the field sub-element of the leadQuestionType
 * element in the config.properties file in XML form, which is used for setting
 * of the type of lead Question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadQuestionTypeField extends Field {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6321784137239035459L;

	/**
	 * The sectionMode attribute: the mode of the section of the questionnaire.
	 */
	@XmlAttribute(name = "sectionMode")
	private String sectionMode;

	/**
	 * The note attribute: the note of the section mode of the questionnaire.
	 */
	@XmlAttribute(name = "note")
	private String note;

	public String getSectionMode() {
		return sectionMode;
	}

	public void setSectionMode(String sectionMode) {
		this.sectionMode = sectionMode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
