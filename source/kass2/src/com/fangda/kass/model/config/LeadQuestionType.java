package com.fangda.kass.model.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the leadQuestionType sub-element of the config
 * element in the config.properties file in XML form, which is used for the type
 * of lead Question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadQuestionType {

	/**
	 * The field element: Generic storage element for any attributes of the
	 * setting of the type of lead Question..
	 */
	@XmlElement(name = "field")
	private List<LeadQuestionTypeField> fields;

	public List<LeadQuestionTypeField> getFields() {
		if (fields == null) {
			fields = new ArrayList<LeadQuestionTypeField>();
		}
		return fields;
	}

	public void setFields(List<LeadQuestionTypeField> fields) {
		this.fields = fields;
	}
}
