package com.fangda.kass.model.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the leadQuestionType sub-element of the config
 * element in the config.properties file in XML form, which is used for the type
 * of lead Question in the old version of KNQ.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadQuestionTypeOld {

	/**
	 * The field element: Generic storage element for any attributes of the
	 * configure of the setting.
	 */
	@XmlElement(name = "field")
	private List<Field> fields;

	public List<Field> getFields() {
		if (fields == null) {
			fields = new ArrayList<Field>();
		}
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
}
