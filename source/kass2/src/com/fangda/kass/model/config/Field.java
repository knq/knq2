package com.fangda.kass.model.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the field element in the config.properties file
 * in XML form, which is used for generic storage element for any attributes of
 * the configure of the setting.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Field implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1015728861443628656L;

	/**
	 * The value attribute: the value of the setting
	 */
	@XmlAttribute(name = "value")
	private String value;

	/**
	 * The name attribute: the title of the setting
	 */
	@XmlAttribute(name = "name")
	private String name;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
