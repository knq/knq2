package com.fangda.kass.model.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the dataType sub-element of the config element
 * in the config.properties file in XML form, which is used for the data type of
 * the inputting data.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataType {

	/**
	 * The field element: Generic storage element for any attributes of the
	 * configure of the setting.
	 */
	@XmlElement(name = "field")
	private List<Field> fields;

	public List<Field> getFields() {
		if (fields == null) {
			fields = new ArrayList<Field>();
		}
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
}
