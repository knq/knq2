package com.fangda.kass.model.support;

/**
 * This class stores / retrieves the expressions of relevance conditions of the
 * person to who the question apply in a questionnaire.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class RelevanceCondition {

	/**
	 * The field (variables name) type of relevance condition 
	 * person
	 * person.field
	 * union
	 * union.field
	 * function
	 */
	private String type;
	/**
	 * The attribute of field (variables)
	 * 
	 * function
	 * married
	 * househoud
	 * stepKinship
	 */
	private String typeAttribute;
	/**
	 * The operators
	 * =
	 * !=
	 * >
	 * <
	 */
	private String op;
	/**
	 * The value of variables
	 */
	private String value;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeAttribute() {
		return typeAttribute;
	}
	public void setTypeAttribute(String typeAttribute) {
		this.typeAttribute = typeAttribute;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(type);
		if(!type.contains("{")) {
			sb.append("[").append(this.getTypeAttribute()).append("]");
		}
		sb.append(op);
		sb.append(value);
		return sb.toString();
	}
}
