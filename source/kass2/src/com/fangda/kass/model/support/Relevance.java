package com.fangda.kass.model.support;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * This class stores / retrieves the relevance variables of the person to who
 * the question apply in a questionnaire.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class Relevance {

	public static final String OP_EQUALS = "=";
	public static final String OP_NO_EQUALS = "!=";
	public static final String OP_GREAT = ">";
	public static final String OP_LESS = "<";

	public static final String TYPE_PERSON = "person";
	public static final String TYPE_PERSON_FIELD = "person.field";
	public static final String TYPE_UNION = "union";
	public static final String TYPE_UNION_FIELD = "union.field";
	public static final String TYPE_FUNCTION = "function";
	public static final String FUNCTION_MERRIED = "married";
	public static final String FUNCTION_HOUSEHOUD = "household";
	public static final String FUNCTION_STEPKINSHIP = "stepKinship";

	private List<RelevanceCondition> conditions = new ArrayList<RelevanceCondition>();

	public List<RelevanceCondition> getConditions() {
		return conditions;
	}

	public void setConditions(List<RelevanceCondition> conditions) {
		this.conditions = conditions;
	}

	/**
	 * Convert a object to a string.
	 * 
	 * @return String
	 */
	public String obj2String() {
		String result = "";
		if (conditions.size() > 0) {
			List<String> as = new ArrayList<String>();
			for (RelevanceCondition condition : conditions) {
				as.add(condition.toString());
			}
			result = StringUtils.join(as.toArray(), " AND ");
		}
		return result;
	}

	/**
	 * Convert a string to a object.
	 * 
	 * @param a
	 */
	public void string2obj(String a) {
		String[] args = a.split("AND");
		this.getConditions().clear();
		for (String arg : args) {
			RelevanceCondition rc = split(arg);
			this.getConditions().add(rc);
		}
	}

	private RelevanceCondition split(String gs) {
		RelevanceCondition rc = new RelevanceCondition();
		if (gs.contains(OP_EQUALS)) {
			if (gs.contains(OP_NO_EQUALS)) {
				String[] args = gs.split(OP_NO_EQUALS);
				rc.setOp(OP_NO_EQUALS);
				rc.setValue(StringUtils.trim(args[1]));
				transType(args[0], rc);
			} else {
				String[] args = gs.split(OP_EQUALS);
				rc.setOp(OP_EQUALS);
				rc.setValue(StringUtils.trim(args[1]));
				transType(args[0], rc);
			}
		} else if (gs.contains(OP_GREAT)) {
			String[] args = gs.split(OP_GREAT);
			rc.setOp(OP_GREAT);
			rc.setValue(StringUtils.trim(args[1]));
			transType(args[0], rc);
		} else if (gs.contains(OP_LESS)) {
			String[] args = gs.split(OP_LESS);
			rc.setOp(OP_LESS);
			rc.setValue(StringUtils.trim(args[1]));
			transType(args[0], rc);
		}
		return rc;
	}

	/**
	 * Conversion type
	 * 
	 * @param type
	 * @param rc
	 */
	private void transType(String type, RelevanceCondition rc) {
		type = type.trim();

		if (type.contains(TYPE_UNION_FIELD)) {
			rc.setType(TYPE_UNION_FIELD);
			rc.setTypeAttribute(match(type));
			return;
		}
		if (type.contains(TYPE_PERSON_FIELD)) {
			rc.setType(TYPE_PERSON_FIELD);
			rc.setTypeAttribute(match(type));
			return;
		}
		if (type.contains(TYPE_UNION)) {
			rc.setType(TYPE_UNION);
			rc.setTypeAttribute(match(type));
			return;
		}
		if (type.contains(TYPE_PERSON)) {
			rc.setType(TYPE_PERSON);
			rc.setTypeAttribute(match(type));
			return;
		}
		if (type.contains(TYPE_FUNCTION)) {
			rc.setType(TYPE_FUNCTION);
			rc.setTypeAttribute(match(type));
			return;
		}
	}

	private String match(String str) {
		String reg = "[0-9a-zA-Z_]+";
		Matcher m = Pattern.compile(reg).matcher(str);
		String result = "";
		while (m.find()) {
			result = m.group(0);
		}
		return result;
	}

	public static void main(String[] args) {
		Relevance r = new Relevance();
		r.string2obj("person.field[mapResDistance]=1 AND {household}>1");
		System.out.println(r.obj2String());
	}

}
