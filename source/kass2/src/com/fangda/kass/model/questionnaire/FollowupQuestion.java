package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the followupQuestion sub-element of the
 * question element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FollowupQuestion extends Question {

	/**
	 * The groupName attribute: the title of a group of questions.
	 */
	@XmlAttribute(name = "groupName")
	private String groupName;

	/**
	 * The groupNo attribute: the serial number of a group of questions.
	 */
	@XmlAttribute(name = "groupNo")
	private String groupNo;

	/**
	 * The layout attribute: the layout setting of a group of questions.
	 */
	@XmlAttribute(name = "layout")
	private String layout;

	/**
	 * The length attribute: the width of the inputting box of the questions.
	 */
	@XmlAttribute(name = "length")
	private Integer length;

	/**
	 * The dataType attribute: the data type of the inputting data.
	 */
	@XmlAttribute(name = "dataType")
	private String dataType;

	/**
	 * The groupName attribute: the English title of a group of questions.
	 */
	@XmlAttribute(name = "en_groupName")
	private String en_groupName;

	public String getEn_groupName() {
		return en_groupName;
	}

	public void setEn_groupName(String enGroupName) {
		en_groupName = enGroupName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

}
