package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the union sub-element of the draw element in
 * the questionnaire XML file, which is used for the required questions when
 * inputting a new union.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Union {

	/**
	 * The uQuestion element: the required questions about union when inputting
	 * a new union.
	 */
	@XmlElement(name = "uQuestion")
	private List<PqdQuestion> uQuestion = new ArrayList<PqdQuestion>();

	public List<PqdQuestion> getuQuestion() {
		return uQuestion;
	}

	public void setuQuestion(List<PqdQuestion> uQuestion) {
		this.uQuestion = uQuestion;
	}

}
