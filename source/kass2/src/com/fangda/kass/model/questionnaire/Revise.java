package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the revise sub-element of the Questionnaire
 * element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Revise {

	/**
	 * The reviser attribute: the name of reviser of the current version of
	 * questionnaire.
	 */
	@XmlAttribute(name = "reviser")
	private String reviser;

	/**
	 * The reviseDate attribute: the revise date of the current version of
	 * questionnaire.
	 */
	@XmlAttribute(name = "reviseDate")
	private String reviseDate;

	/**
	 * The version attribute: the version Number of the questionnaire.
	 */
	@XmlAttribute(name = "version")
	private String version;

	/**
	 * The note attribute: the note of the current version of questionnaire.
	 */
	@XmlAttribute(name = "note")
	private String note;

	public String getReviser() {
		return reviser;
	}

	public void setReviser(String reviser) {
		this.reviser = reviser;
	}

	public String getReviseDate() {
		return reviseDate;
	}

	public void setReviseDate(String reviseDate) {
		this.reviseDate = reviseDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
