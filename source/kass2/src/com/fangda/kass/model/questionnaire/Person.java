package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the person sub-element of the draw element in
 * the questionnaire XML file, which is used for the required questions when
 * inputting a new person.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Person {

	/**
	 * The pQuestion element: the required questions about person when inputting a new
	 * person.
	 */
	@XmlElement(name = "pQuestion")
	private List<PqdQuestion> pquestion = new ArrayList<PqdQuestion>();

	public List<PqdQuestion> getPquestion() {
		return pquestion;
	}

	public void setPquestion(List<PqdQuestion> pquestion) {
		this.pquestion = pquestion;
	}
}
