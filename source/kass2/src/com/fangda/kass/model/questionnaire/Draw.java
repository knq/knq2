package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the draw element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Draw {

	/**
	 * The stopRule element: it is used for controlling the scale size of NKK
	 * diagram .Example: For a 2-step stopping rule, the person at the second
	 * step, whose kinship distance from ego is equal 2, will turn yellow
	 * automatically , and there is no need to ask about anyone beyond the
	 * person.
	 */
	@XmlElement(name = "stopRule")
	private StopRule stopRule;

	/**
	 * The person element: it is a placeholder element for the required
	 * questions when inputting a new person.
	 */
	@XmlElement(name = "person")
	private Person person;

	/**
	 * The union element: it is a placeholder element for the required questions
	 * when inputting a new union.
	 */
	@XmlElement(name = "union")
	private Union union;

	/**
	 * The completeProcedure element: it is a placeholder element for the steps
	 * setting of automatically inputting all family members of a person.
	 */
	@XmlElement(name = "completeProcedure")
	private CompleteProcedure completeProcedure;

	public StopRule getStopRule() {
		return stopRule;
	}

	public void setStopRule(StopRule stopRule) {
		this.stopRule = stopRule;
	}

	public Person getPerson() {
		if (person == null) {
			person = new Person();
		}
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public CompleteProcedure getCompleteProcedure() {
		return completeProcedure;
	}

	public void setCompleteProcedure(CompleteProcedure completeProcedure) {
		this.completeProcedure = completeProcedure;
	}

	public Union getUnion() {
		if (union == null) {
			union = new Union();
		}
		return union;
	}

	public void setUnion(Union union) {
		this.union = union;
	}
}
