package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the option element in the questionnaire XML
 * file， which records the information of answer options of question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Option {

	/**
	 * The oid attribute: Option ID
	 */
	@XmlAttribute(name = "oid")
	private String oid;

	/**
	 * The sortNo attribute: the sorting number of the questions answer options
	 */
	@XmlAttribute(name = "sortNo")
	private Integer sortNo;

	/**
	 * The note attribute: the note of the questions answer options
	 */
	@XmlAttribute(name = "note")
	private String note;

	/**
	 * The detail attribute: the description of the questions answer options
	 */
	@XmlAttribute(name = "detail")
	private String detail;

	/**
	 * The value attribute: the value of the questions answer options
	 */
	@XmlAttribute(name = "value")
	private String value;

	/**
	 * The layout attribute: it is the layout setting of the questions answer options, also see
	 * com.fangda.kass.model.config.OptionLayout.
	 */
	@XmlAttribute(name = "layout")
	private String layout;

	/**
	 * The defaultValue attribute: the default Value of the questions answer options
	 */
	@XmlAttribute(name = "defaultValue")
	private String defaultValue;

	/**
	 * The label attribute: the label of the questions answer options
	 */
	@XmlAttribute(name = "label")
	private String label;

	/**
	 * The en_detail attribute: the English description of the questions answer options
	 */
	@XmlAttribute(name = "en_detail")
	private String en_detail;

	/**
	 * The note attribute: the English note of the questions answer options
	 */
	@XmlAttribute(name = "en_note")
	private String en_note;

	public String getEn_detail() {
		return en_detail;
	}

	public void setEn_detail(String enDetail) {
		en_detail = enDetail;
	}

	public String getEn_note() {
		return en_note;
	}

	public void setEn_note(String enNote) {
		en_note = enNote;
	}

	/**
	 * The disable attribute: the disable setting of the question answer options, also see
	 * com.fangda.kass.model.config.DisableType.
	 */
	@XmlAttribute(name = "disable")
	private Integer disable;

	/**
	 * The type attribute: the type of the question answer options
	 */
	@XmlAttribute
	private String type;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public Integer getDisable() {
		return disable;
	}

	public void setDisable(Integer disable) {
		this.disable = disable;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
