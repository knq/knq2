package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the stopRule sub-element of the draw element in
 * the questionnaire XML file, which is used for controlling the scale size of
 * NKK diagram.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class StopRule {

	/**
	 * The step attribute: the step is the maximum kinship distance from ego to
	 * a person can be inputted in the NKK. Example: For a 2-step stopping rule,
	 * the person at the second step, whose kinship distance from ego is equal
	 * 2, will turn yellow automatically , and there is no need to ask about
	 * anyone beyond the person.
	 */
	@XmlAttribute(name = "step")
	private Integer step;

	/**
	 * The note attribute: the note of the Stop Rule
	 */
	@XmlAttribute(name = "note")
	private String note;

	/**
	 * The picture attribute: the sample diagram of the Stop Rule
	 */
	@XmlAttribute(name = "picture")
	private String picture;

	/**
	 * The en_note attribute: the English note of the Stop Rule
	 */
	@XmlAttribute(name = "en_note")
	private String en_note;

	public String getEn_note() {
		return en_note;
	}

	public void setEn_note(String enNote) {
		en_note = enNote;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}
