package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 * This class stores / retrieves the step sub-element of the CompleteProcedure
 * element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CompleteStep {
	
	/**
	 * The stepNo attribute: the sequence number for the step.
	 */
	@XmlAttribute
	private Integer stepNo;
	
	/**
	 * The title attribute: the title of the step.
	 */
	@XmlAttribute
	private String title;
	
	/**
	 * The disable attribute: 1 refers to disable this step, 0 refers to enable this step.
	 */
	@XmlAttribute
	private Integer disable;
	
	/**
	 * The en_title attribute: the title of the step in English.
	 */
	@XmlAttribute(name = "en_title")
	private String en_title;
	
	/**
	 * The subStep element: the sub-element of the step element, which records the detail information of a step.
	 */
	@XmlElement(name = "subStep")
	List<CompleteSubStep> subSteps = new ArrayList<CompleteSubStep>();

	public String getEn_title() {
		return en_title;
	}

	public void setEn_title(String enTitle) {
		en_title = enTitle;
	}

	public Integer getStepNo() {
		return stepNo;
	}

	public void setStepNo(Integer stepNo) {
		this.stepNo = stepNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDisable() {
		return disable;
	}

	public void setDisable(Integer disable) {
		this.disable = disable;
	}

	public List<CompleteSubStep> getSubSteps() {
		return subSteps;
	}

	public void setSubSteps(List<CompleteSubStep> subSteps) {
		this.subSteps = subSteps;
	}

}
