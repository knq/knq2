package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the languageCountry sub-element of the
 * Questionnaire element in the questionnaire XML file, which is a placeholder
 * element for the isoLangCode element.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LanguageCountry {

	/**
	 * The isoLangCode element: it is the detail description of the iso language
	 * code and country code of questionnaire.
	 */
	@XmlElement(name = "isoLangCode")
	private IsoLangCode isoLangCode;

	public IsoLangCode getIsoLangCode() {
		if (isoLangCode == null) {
			isoLangCode = new IsoLangCode();
		}
		return isoLangCode;
	}

	public void setIsoLangCode(IsoLangCode isoLangCode) {
		this.isoLangCode = isoLangCode;
	}
}
