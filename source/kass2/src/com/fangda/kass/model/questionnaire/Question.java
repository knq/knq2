package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the question sub-element of the subsection
 * element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Question {

	/**
	 * The qid attribute: Question ID.
	 */
	@XmlAttribute(name = "qid")
	private String qid;

	/**
	 * The type attribute: the display controls of questions, 1 checkbox 2 radio
	 * 3 text
	 */
	@XmlAttribute(name = "type")
	private String type;

	/**
	 * The title attribute: Question Title FollowupQuestion hasn't this
	 * attribute.
	 */
	@XmlAttribute(name = "title")
	private String title;

	/**
	 * The lead attribute: the description of a lead question FollowupQuestion
	 * hasn't this attribute.
	 */
	@XmlAttribute(name = "lead")
	private String lead;

	/**
	 * The subsection attribute FollowupQuestion hasn't this attribute.
	 */
	@XmlAttribute(name = "subsection")
	private String subsection;

	/**
	 * The direction attribute: The setting of question direction, also see
	 * com.fangda.kass.model.config.DirectionType. FollowupQuestion hasn't this
	 * attribute.
	 */
	@XmlAttribute(name = "direction")
	private String direction;

	/**
	 * The relevance attribute: The relevance person setting of question, also
	 * see com.fangda.kass.model.config.RelevanceType. FollowupQuestion hasn't
	 * this attribute.
	 */
	@XmlAttribute(name = "relevance")
	private String relevance;

	/**
	 * The sortNo attribute: the sorting number of the questions
	 */
	@XmlAttribute(name = "sortNo")
	private Integer sortNo;

	/**
	 * The disable attribute: The disable setting of question, also see
	 * com.fangda.kass.model.config.DisableType.
	 */
	@XmlAttribute(name = "disable")
	private Integer disable;

	/**
	 * The completed attribute: The disable setting of question, also see
	 * com.fangda.kass.model.config.DisableType.
	 */
	@XmlAttribute(name = "completed")
	private Boolean completed;

	/**
	 * The detail attribute: the detail description of a question
	 */
	@XmlAttribute(name = "detail")
	private String detail;

	/**
	 * The ifGo attribute: it is used for skipping condition of a question
	 */
	@XmlAttribute(name = "ifGo")
	private String ifGo;

	/**
	 * The label attribute: Question label
	 */
	@XmlAttribute(name = "label")
	private String label;

	/**
	 * The en_title attribute: the English Question Title FollowupQuestion
	 * hasn't this attribute.
	 */
	@XmlAttribute(name = "en_title")
	private String en_title;

	/**
	 * The en_detail attribute: the English detail description of a question
	 */
	@XmlAttribute(name = "en_detail")
	private String en_detail;

	public String getEn_title() {
		return en_title;
	}

	public void setEn_title(String enTitle) {
		en_title = enTitle;
	}

	public String getEn_detail() {
		return en_detail;
	}

	public void setEn_detail(String enDetail) {
		en_detail = enDetail;
	}

	public String getEn_lead() {
		return en_lead;
	}

	public void setEn_lead(String enLead) {
		en_lead = enLead;
	}

	/**
	 * The en_lead attribute: the English description of a lead question.
	 * FollowupQuestion hasn't this attribute.
	 */
	@XmlAttribute(name = "en_lead")
	private String en_lead;

	/**
	 * The option element: it records the information of answer options of
	 * question.
	 */
	@XmlElement(name = "option")
	private List<Option> options = new ArrayList<Option>();

	/**
	 * The followupQuestion element: it records the information of follow up
	 * question.
	 */
	@XmlElement(name = "followupQuestion")
	private List<FollowupQuestion> followupQuestions = new ArrayList<FollowupQuestion>();

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public final String getLead() {
		return lead;
	}

	public final void setLead(String lead) {
		this.lead = lead;
	}

	public final String getSubsection() {
		return subsection;
	}

	public final void setSubsection(String subsection) {
		this.subsection = subsection;
	}

	public final String getDirection() {
		return direction;
	}

	public final void setDirection(String direction) {
		this.direction = direction;
	}

	public final String getRelevance() {
		return relevance;
	}

	public final void setRelevance(String relevance) {
		this.relevance = relevance;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public Integer getDisable() {
		return disable;
	}

	public void setDisable(Integer disable) {
		this.disable = disable;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getIfGo() {
		return ifGo;
	}

	public void setIfGo(String ifGo) {
		this.ifGo = ifGo;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public final List<FollowupQuestion> getFollowupQuestions() {
		return followupQuestions;
	}

	public final void setFollowupQuestions(
			List<FollowupQuestion> followupQuestions) {
		this.followupQuestions = followupQuestions;
	}

}
