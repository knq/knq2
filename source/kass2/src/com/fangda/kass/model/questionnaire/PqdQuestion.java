package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the pQuestion sub-element of the person element
 * in the questionnaire XML file, which records the required questions when
 * inputting a new person.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PqdQuestion {
	
	/**
	 * The qid attribute: Question ID of the required questions about person when inputting a new
	 * person.
	 */
	@XmlAttribute(name = "qid")
	private String qid;

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}
}
