package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fangda.kass.model.config.RelevanceType;

/**
 * This class stores / retrieves the root element (Questionnaire) of a
 * questionnaire file in XML form, For example: questions_en_EN_v2.properties,
 * questions_zh_CN_v3.properties.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
// @XmlTransient
@XmlRootElement(name = "Questionnaire")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "languageCountry", "relevanceType", "draw", "section",
		"revises" })
public class Questionnaire {

	/**
	 * The title attribute: the title of questionnaire.
	 */
	@XmlAttribute(name = "title")
	private String title;

	/**
	 * The language attribute: the ISO 639-1 language code of questionnaire
	 * language.
	 */
	@XmlAttribute(name = "language")
	private String language;

	/**
	 * The languageName attribute: the language name of questionnaire language.
	 */
	@XmlAttribute(name = "languageName")
	private String languageName;

	/**
	 * The version attribute: the version of questionnaire
	 */
	@XmlAttribute(name = "version")
	private String version;

	/**
	 * The country attribute: the ISO 2-letter code of country where the
	 * questionnaire applies to.
	 */
	@XmlAttribute(name = "country")
	private String country;

	/**
	 * The countryName attribute: the name of country where the questionnaire
	 * applies to.
	 */
	@XmlAttribute(name = "countryName")
	private String countryName;

	/**
	 * The note attribute: the note of the questionnaire
	 */
	@XmlAttribute(name = "note")
	private String note;

	/**
	 * The cultureArea attribute: the culture area where the questionnaire
	 * applies to.
	 */
	@XmlAttribute(name = "cultureArea")
	private String cultureArea;

	/**
	 * The languageCountry element: it is a placeholder element for the
	 * isoLangCode element for the detail description of the iso language code
	 * and country code of questionnaire.
	 */
	@XmlElement(name = "languageCountry")
	private LanguageCountry languageCountry;

	/**
	 * The relevanceType element: the relevance type of the person to who the
	 * question apply in a questionnaire.
	 */
	@XmlElement(name = "relevanceType")
	private RelevanceType relevanceType;

	/**
	 * The draw element: the setting of the drawing of NKK diagram in the draw
	 * mode section of the questions
	 */
	@XmlElement(name = "draw")
	private Draw draw;

	/**
	 * The section element: the setting of the section of the questions
	 */
	@XmlElement(name = "section")
	private Section section;

	/**
	 * The revise element:
	 */
	@XmlElement(name = "revise")
	private List<Revise> revises;

	/**
	 * The status attribute: the completed status of questionnaire（Formal or
	 * Draft）
	 */
	@XmlAttribute(name = "status")
	private String status;

	/**
	 * The en_title attribute: the title of the questionnaire in English.
	 */
	@XmlAttribute(name = "en_title")
	private String en_title;

	/**
	 * The en_cultureArea attribute: the culture Area of the questionnaire in
	 * English.
	 */
	@XmlAttribute(name = "en_cultureArea")
	private String en_cultureArea;

	/**
	 * The en_note attribute: the note of the questionnaire in English.
	 */
	@XmlAttribute(name = "en_note")
	private String en_note;

	public LanguageCountry getLanguageCountry() {
		if (languageCountry == null) {
			languageCountry = new LanguageCountry();
		}
		return languageCountry;
	}

	public void setLanguageCountry(LanguageCountry languageCountry) {
		this.languageCountry = languageCountry;
	}

	public String getEn_title() {
		return en_title;
	}

	public void setEn_title(String enTitle) {
		en_title = enTitle;
	}

	public String getEn_cultureArea() {
		return en_cultureArea;
	}

	public void setEn_cultureArea(String enCultureArea) {
		en_cultureArea = enCultureArea;
	}

	public String getEn_note() {
		return en_note;
	}

	public void setEn_note(String enNote) {
		en_note = enNote;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Draw getDraw() {
		return draw;
	}

	public void setDraw(Draw draw) {
		this.draw = draw;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public String getCultureArea() {
		return cultureArea;
	}

	public void setCultureArea(String cultureArea) {
		this.cultureArea = cultureArea;
	}

	public List<Revise> getRevises() {
		if (revises == null) {
			revises = new ArrayList<Revise>();
		}
		return revises;
	}

	public void setRevises(List<Revise> revises) {
		this.revises = revises;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RelevanceType getRelevanceType() {
		return relevanceType;
	}

	public void setRelevanceType(RelevanceType relevanceType) {
		this.relevanceType = relevanceType;
	}
}
