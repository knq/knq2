package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fangda.kass.util.Constants;

/**
 * This class stores / retrieves the section sub-element of the Questionnaire
 * element in the questionnaire XML file, which is a placeholder element for the
 * subsection element used for a section of questions.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Section {

	/**
	 * The subsection element: it is a description of section and subsection.
	 */
	@XmlElement(name = "subsection")
	private List<Subsection> subsections = new ArrayList<Subsection>();

	public List<Subsection> getSubsections() {
		if (subsections == null) {
			subsections = new ArrayList<Subsection>();
		}
		if (Constants.PANELFLAG == "IV") {
			for (int i = 0; i < subsections.size(); i++) {
				if (subsections.get(i).getDisable() == 2) {
					subsections.remove(i);
				}
			}
		}
		return subsections;

	}

	public void setSubsections(List<Subsection> subsections) {
		this.subsections = subsections;
	}
}
