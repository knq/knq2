package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the subStep sub-element of the step element in
 * the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CompleteSubStep {

	/**
	 * The page attribute: 1 refers to each subStep has a standalone page, 0
	 * refers to all subSteps are the same page.
	 */
	@XmlAttribute
	private Integer page;

	/**
	 * The stepNo attribute: the sequence number for the subStep.
	 */
	@XmlAttribute
	private Integer stepNo;

	/**
	 * The kinshipTerm attribute: the kinship term of kinship between the
	 * inputting person and Ego for the step.
	 */
	@XmlAttribute
	private String kinshipTerm;

	/**
	 * The sex attribute: Gender of the inputting person
	 */
	@XmlAttribute
	private Integer sex;

	/**
	 * The en_kinshipTerm attribute: the English kinship term of kinship between
	 * the inputting person and Ego for the step.
	 */
	@XmlAttribute(name = "en_kinshipTerm")
	private String en_kinshipTerm;

	public String getEn_kinshipTerm() {
		return en_kinshipTerm;
	}

	public void setEn_kinshipTerm(String enKinshipTerm) {
		en_kinshipTerm = enKinshipTerm;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getStepNo() {
		return stepNo;
	}

	public void setStepNo(Integer stepNo) {
		this.stepNo = stepNo;
	}

	public String getKinshipTerm() {
		return kinshipTerm;
	}

	public void setKinshipTerm(String kinshipTerm) {
		this.kinshipTerm = kinshipTerm;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

}
