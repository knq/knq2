package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fangda.kass.util.Constants;

/**
 * This class stores / retrieves the subsection sub-element of the section
 * element in the questionnaire XML file, which used for the sections or the
 * subsections of questions.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Subsection {

	/**
	 * The sid attribute: the section ID of a questionnaire
	 */
	@XmlAttribute(name = "sid")
	private String sid;

	/**
	 * The sortNo attribute: the sorting Number of the section
	 */
	@XmlAttribute(name = "sortNo")
	private int sortNo;

	/**
	 * The title attribute: the title of the section
	 */
	@XmlAttribute(name = "title")
	private String title;

	/**
	 * The detail attribute: the detail description of the section
	 */
	@XmlAttribute(name = "detail")
	private String detail;

	/**
	 * The sectionMode attribute: the section Mode of the section, such as Draw
	 * Mode, Map Mode, Question Mode and More Detail Mode, also see
	 * com.fangda.kass.model.config.SectionMode.
	 */
	@XmlAttribute(name = "sectionMode")
	private String sectionMode;

	/**
	 * The disable attribute: The disable setting of section or subsection, also
	 * see com.fangda.kass.model.config.DisableType.
	 */
	@XmlAttribute(name = "disable")
	private int disable;

	/**
	 * The required attribute: The required setting of the section or
	 * subsection, 1=required section.
	 */
	@XmlAttribute(name = "required")
	private Integer required;

	/**
	 * The en_title attribute: the English Question Title of the section or
	 * subsection.
	 */
	@XmlAttribute(name = "en_title")
	private String en_title;

	/**
	 * The en_detail attribute: the English Question detail description of the
	 * section or subsection.
	 */
	@XmlAttribute(name = "en_detail")
	private String en_detail;

	public Integer getRequired() {
		return required;
	}

	public void setRequired(Integer required) {
		this.required = required;
	}

	public String getEn_title() {
		return en_title;
	}

	public void setEn_title(String enTitle) {
		en_title = enTitle;
	}

	public String getEn_detail() {
		return en_detail;
	}

	public void setEn_detail(String enDetail) {
		en_detail = enDetail;
	}

	@XmlElement(name = "question")
	private List<Question> questions;

	@XmlElement(name = "subsection")
	private List<Subsection> subSections;

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public int getSortNo() {
		return sortNo;
	}

	public void setSortNo(int sortNo) {
		this.sortNo = sortNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getSectionMode() {
		return sectionMode;
	}

	public void setSectionMode(String sectionMode) {
		this.sectionMode = sectionMode;
	}

	public int getDisable() {
		return disable;
	}

	public void setDisable(int disable) {
		this.disable = disable;
	}

	public List<Subsection> getSubSections() {
		if (subSections == null) {
			subSections = new ArrayList<Subsection>();
		}
		if (Constants.PANELFLAG == "IV") {
			for (int i = 0; i < subSections.size(); i++) {
				if (subSections.get(i).disable == 2) {
					subSections.remove(i);
				}
			}
		}
		return subSections;
	}

	public void setSubSections(List<Subsection> subSections) {
		this.subSections = subSections;
	}

	public List<Question> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<Question>();
		}
		if (Constants.PANELFLAG == "IV") {
			for (int i = 0; i < questions.size(); i++) {
				if (questions.get(i).getDisable() == 2) {
					questions.remove(i);
				}
			}
		}

		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
}
