package com.fangda.kass.model.questionnaire;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the CompleteProcedure sub-element of the draw
 * element in the questionnaire XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CompleteProcedure {

	/**
	 * The prompt attribute of the CompleteProcedure element.
	 */
	@XmlAttribute(name = "prompt")
	private String prompt;
	
	/**
	 * The step element: it is a sub-element of the CompleteProcedure element,
	 * which records the steps of automatically inputting all family members of
	 * a person.
	 */
	@XmlElement(name = "step")
	private List<CompleteStep> completeStep = new ArrayList<CompleteStep>();

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public List<CompleteStep> getCompleteStep() {
		return completeStep;
	}

	public void setCompleteStep(List<CompleteStep> completeStep) {
		this.completeStep = completeStep;
	}
}