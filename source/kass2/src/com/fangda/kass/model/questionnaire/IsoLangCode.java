package com.fangda.kass.model.questionnaire;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the isoLangCode sub-element of the
 * languageCountry element in the questionnaire XML file, which is the detail
 * description of the iso language code and country code of questionnaire.
 * Also see com.fangda.kass.model.config.IsoLangCode
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IsoLangCode {
	
	/**
	 * The code Attribute:  the iso language and country code, such as en_EN, zh-CN, de-DE.
	 */
	@XmlAttribute(name = "code")
	private String code;

	/**
	 * The name Attribute:  the iso language and country name, such as English, Chinese (Simplified), German (Germany).
	 */
	@XmlAttribute(name = "name")
	private String name;

	/**
	 * The languageCode Attribute:  the iso language code, such as en, zh, de.
	 */
	@XmlAttribute(name = "languageCode")
	private String languageCode;

	/**
	 * The countryCode Attribute:  the iso country code, such as EN, CN, DE.
	 */
	@XmlAttribute(name = "countryCode")
	private String countryCode;

	/**
	 * The languageName Attribute:  the language name in the ISO 639-1.
	 */
	@XmlAttribute(name = "languageName")
	private String languageName;

	/**
	 * The countryName Attribute:  the country name in the ISO 3166.
	 */
	@XmlAttribute(name = "countryName")
	private String countryName;

	/**
	 * The localName Attribute:  the iso language and country name in local language.
	 */
	@XmlAttribute(name = "localName")
	private String localName;

	/**
	 * The localLanguage Attribute:  the Language name in local language.
	 */
	@XmlAttribute(name = "localLanguage")
	private String localLanguage;
	
	/**
	 * The localCountry Attribute:  the country name in local language.
	 */
	@XmlAttribute(name = "localCountry")
	private String localCountry;

	/**
	 * The isKNQ2 Attribute.
	 */
	@XmlAttribute(name = "isKNQ2")
	private String isKNQ2;

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getLocalLanguage() {
		return localLanguage;
	}

	public void setLocalLanguage(String localLanguage) {
		this.localLanguage = localLanguage;
	}

	public String getLocalCountry() {
		return localCountry;
	}

	public void setLocalCountry(String localCountry) {
		this.localCountry = localCountry;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getIsKNQ2() {
		return isKNQ2;
	}

	public void setIsKNQ2(String isKNQ2) {
		this.isKNQ2 = isKNQ2;
	}
}
