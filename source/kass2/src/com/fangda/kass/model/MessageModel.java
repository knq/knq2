package com.fangda.kass.model;

/**
 * The Class for the message of submitting
 * 
 * @author Fangfang Zhao
 *
 */
public class MessageModel {
	
	public MessageModel() {

	}

	public MessageModel(boolean success, String message) {
		this(success, message, 0);
	}

	public MessageModel(boolean success, String message, int state) {
		this.success = success;
		this.message = message;
		this.state = state;
	}

	private boolean success = true;
	private String message = "";
	private int state = 0;// 0 = closed, 1 = unclosed

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
