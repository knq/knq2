package com.fangda.kass.model;
/**
 * The Basic Class for the retrieves of the XML
 * 
 * @author Fangfang Zhao
 *
 */
public class BaseModel {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
