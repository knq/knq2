package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class stores / retrieves the root element (project) of a .project file
 * in XML format.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	/**
	 * The title attribute: the title of a research project.
	 */
	@XmlAttribute(name = "title")
	private String title;
	/**
	 * The directory attribute: the current directory path in which all files of
	 * project are stored.
	 */
	@XmlAttribute(name = "directory")
	private String directory;
	/**
	 * The country attribute: the country of the field site of the project.
	 */
	@XmlAttribute(name = "country")
	private String country;
	/**
	 * The province attribute: the province of the field site of the project.
	 */
	@XmlAttribute(name = "province")
	private String province;
	/**
	 * The district attribute: the district of the field site of the project.
	 */
	@XmlAttribute(name = "district")
	private String district;
	/**
	 * The county attribute: the county of the field site of the project.
	 */
	@XmlAttribute(name = "county")
	private String county;
	/**
	 * The town attribute: the town of the field site of the project.
	 */
	@XmlAttribute(name = "town")
	private String town;
	/**
	 * The village attribute: the village of the field site of the project.
	 */
	@XmlAttribute(name = "village")
	private String village;
	/**
	 * The placeName attribute: the placeName of the field site of the project.
	 */
	@XmlAttribute(name = "placeName")
	private String placeName;
	/**
	 * The questionnaireDirectory attribute: the current directory of the
	 * questionnaire applied in this project.
	 */
	@XmlAttribute(name = "questionnaireDirectory")
	private String questionnaireDirectory;
	/**
	 * The language attribute: the language of the interview in this project.
	 */
	@XmlAttribute(name = "language")
	private String lan;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getQuestionnaireDirectory() {
		return questionnaireDirectory;
	}

	public void setQuestionnaireDirectory(String questionnaireDirectory) {
		this.questionnaireDirectory = questionnaireDirectory;
	}

	public String getLan() {
		return lan;
	}

	public void setLan(String lan) {
		this.lan = lan;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

}
