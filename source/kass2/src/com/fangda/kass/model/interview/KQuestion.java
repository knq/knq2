package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves all attributes of the question sub element of
 * the questions element for completed status of this question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KQuestion {
	/**
	 * The completed attribute: Completed Status of a question: 1 = completed,
	 * 0=undealt, 2= undone
	 */
	@XmlAttribute(name = "completed")
	private String completed;
	/**
	 * The id attribute: More accurately "Qid" since this stores qid of a
	 * question.
	 */
	@XmlAttribute(name = "id")
	private String id;
	/**
	 * The isMarked attribute: Marked Status of a lead question: true = Marked,
	 * false = unMarked
	 */
	@XmlAttribute(name = "isMarked")
	private Boolean isMarked;

	public Boolean getIsMarked() {
		return isMarked;
	}

	public void setIsMarked(Boolean isMarked) {
		this.isMarked = isMarked;
	}

	public String getCompleted() {
		return completed;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
