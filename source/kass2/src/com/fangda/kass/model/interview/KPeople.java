package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//import com.fangda.kass.model.questionnaire.FollowupQuestion;

/**
 * This class stores / retrieves the people element which is a placeholder
 * element for all person in an interview xml file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KPeople {
	/**
	 * The person element: it is a sub-element of the people element.
	 */
	@XmlElement(name = "person")
	private List<KPPerson> persons = new ArrayList<KPPerson>();

	public List<KPPerson> getPersons() {
		if (persons == null) {
			persons = new ArrayList<KPPerson>();
		}
		return persons;
	}

	public void setPersons(List<KPPerson> persons) {
		this.persons = persons;
	}
}
