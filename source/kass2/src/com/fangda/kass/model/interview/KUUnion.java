package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

//import com.fangda.kass.model.config.Field;

/**
 * This class stores / retrieves the union element, which records any key
 * information of a union.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KUUnion implements java.io.Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9094051014311609465L;
	/**
	 * The uid attribute: the Union ID.
	 */
	@XmlAttribute(name = "uid")
	private int id;
	/**
	 * The isOther attribute: 1 refers to this union is in the kinship network,
	 * 0 refers to this union belongs other important people
	 */
	@XmlAttribute(name = "isOther")
	private Integer isOther;
	/**
	 * The field element: Generic storage element for any attributes of a
	 * question.
	 */
	@XmlElement(name = "field")
	private List<KQQuestionField> fields;
	/**
	 * The coords Element: the X-, and Y- Coordinates of icons of person and
	 * unions.
	 */
	@XmlElement(name = "coords")
	private KPPCoords coords;
	/**
	 * The member Element: members of a union
	 */
	@XmlElement(name = "member")
	private List<KUUMember> members;

	public KPPCoords getCoords() {
		if (coords == null) {
			coords = new KPPCoords();
		}
		return coords;
	}

	public void setCoords(KPPCoords coords) {
		this.coords = coords;
	}

	public List<KQQuestionField> getFields() {
		if (fields == null) {
			fields = new ArrayList<KQQuestionField>();
		}
		return fields;
	}

	public void setFields(List<KQQuestionField> fields) {
		this.fields = fields;
	}

	public List<KUUMember> getMembers() {
		if (members == null) {
			members = new ArrayList<KUUMember>();
		}
		return members;
	}

	public void setMembers(List<KUUMember> members) {
		this.members = members;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getIsOther() {
		return isOther;
	}

	public void setIsOther(Integer isOther) {
		this.isOther = isOther;
	}

	public KUUnion clone() {
		KUUnion p = null;
		try {
			p = (KUUnion) super.clone();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}
}
