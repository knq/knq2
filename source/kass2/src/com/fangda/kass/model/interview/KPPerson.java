package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves attributes of a person element in a interview
 * xml file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KPPerson implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8212796495451168494L;

	/**
	 * The pid Attribute: Person ID
	 */
	@XmlAttribute(name = "pid")
	private Integer pid;

	/**
	 * The sex Attribute: Gender of a person
	 */
	@XmlAttribute(name = "sex")
	private Integer sex;

	/**
	 * The isOther Attribute: Other important person, who is not a relative: 1
	 * is a non-relative important person, 0 is a relative.
	 */
	@XmlAttribute(name = "isOther")
	private Integer isOther;

	/**
	 * The completed Attribute: Completed Status of a person : 1 = completed,
	 * 0=uncompleted
	 */
	@XmlAttribute(name = "completed")
	private Integer completed = 0;
	
	/**
	 * The field Element: Generic storage element for any attributes of a
	 * question.
	 */
	@XmlElement(name = "field")
	private List<KQQuestionField> fields;
	
	/**
	 * The question Element: More accurately "Answer" since this stores data for
	 * the answers from the question files.
	 */
	@XmlElement(name = "question")
	private List<KQQuestion> questions;
	
	/**
	 * The coords Element: the X-, and Y- Coordinates of icons of person and
	 * unions.
	 */
	@XmlElement(name = "coords")
	private KPPCoords coords;

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public KPPCoords getCoords() {
		return coords;
	}

	public void setCoords(KPPCoords coords) {
		this.coords = coords;
	}

	public List<KQQuestion> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<KQQuestion>();
		}
		return questions;
	}

	public void setQuestions(List<KQQuestion> questions) {
		this.questions = questions;
	}

	public List<KQQuestionField> getFields() {
		if (fields == null) {
			fields = new ArrayList<KQQuestionField>();
		}
		return fields;
	}

	public void setFields(List<KQQuestionField> fields) {
		this.fields = fields;
	}

	public Integer getIsOther() {
		return isOther;
	}

	public void setIsOther(Integer isOther) {
		this.isOther = isOther;
	}

	public Integer getCompleted() {
		return completed;
	}

	public void setCompleted(Integer completed) {
		this.completed = completed;
	}

	public KPPerson clone() {
		KPPerson p = null;
		try {
			p = (KPPerson) super.clone();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}

}
