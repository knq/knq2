package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class stores / retrieves the root element (KASS) of an interview XML
 * file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
// @XmlTransient
@XmlRootElement(name = "KASS")
@XmlAccessorType(XmlAccessType.FIELD)
public class KASS {

	/**
	 * The language attribute: language of interview
	 */
	@XmlAttribute(name = "language")
	private String language;
	/**
	 * The fieldsite element: the field site of a project.
	 */
	@XmlElement(name = "fieldSite")
	private KFieldSite fieldSite;
	/**
	 * The pageConfig element: the layout setting of NKK diagram page.
	 */
	@XmlElement(name = "pageConfig")
	private PageConfig pageConfig;
	/**
	 * The interviewInfo element: the background information of an interview.
	 */
	@XmlElement(name = "interviewInfo")
	private KInterviewInfo interviewInfo;
	/**
	 * The people element: it is a placeholder element for all person in an
	 * interview xml file.
	 */
	@XmlElement(name = "people")
	private KPeople people;
	/**
	 * The unions element: it is a placeholder element for all unions in an
	 * interview xml file.
	 */
	@XmlElement(name = "unions")
	private KUnions unions;
	/**
	 * The questions element: it is a placeholder element for all questions in
	 * interview xml file.
	 */
	@XmlElement(name = "questions")
	private KQuestions questions;

	public KFieldSite getFieldSite() {
		return fieldSite;
	}

	public void setFieldSite(KFieldSite fieldSite) {
		this.fieldSite = fieldSite;
	}

	public KInterviewInfo getInterviewInfo() {
		return interviewInfo;
	}

	public void setInterviewInfo(KInterviewInfo interviewInfo) {
		this.interviewInfo = interviewInfo;
	}

	public KPeople getPeople() {
		if (people == null) {
			people = new KPeople();
		}
		return people;
	}

	public void setPeople(KPeople people) {
		this.people = people;
	}

	public KUnions getUnions() {
		if (unions == null) {
			unions = new KUnions();
		}
		return unions;
	}

	public void setUnions(KUnions unions) {
		this.unions = unions;
	}

	public KQuestions getQuestions() {
		if (questions == null) {
			questions = new KQuestions();
		}
		return questions;
	}

	public void setQuestions(KQuestions questions) {
		this.questions = questions;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public PageConfig getPageConfig() {
		if (pageConfig == null) {
			pageConfig = new PageConfig();
		}
		return pageConfig;
	}

	public void setPageConfig(PageConfig pageConfig) {
		this.pageConfig = pageConfig;
	}

}
