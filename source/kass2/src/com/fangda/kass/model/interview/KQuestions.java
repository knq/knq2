package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the questions element which is a placeholder
 * element for all questions in an interview xml file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KQuestions {
	/**
	 * The question element: it is a sub-element of the questions element.
	 */
	@XmlElement(name = "question")
	private List<KQuestion> questions;

	public List<KQuestion> getQuestions() {
		if (questions == null) {
			questions = new ArrayList<KQuestion>();
		}
		return questions;
	}

	public void setQuestions(List<KQuestion> questions) {
		this.questions = questions;
	}
}
