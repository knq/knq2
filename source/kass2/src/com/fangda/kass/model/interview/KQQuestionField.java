package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * This class stores / retrieves the attributes of the field element of the
 * question element, which records any key information of a question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "qid", "label", "value" })
public class KQQuestionField implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1319843227130659353L;
	
	/**
	 * The qid attribute: Question ID
	 */
	@XmlAttribute(name = "qid")
	private String qid;
	/**
	 * The label attribute: Question Label
	 */
	@XmlAttribute(name = "label")
	private String label;
	/**
	 * The value attribute: Question value
	 */
	@XmlAttribute(name = "value")
	private String value;

	public String getValue() {
		// if(StringUtils.equals(Constants.DEFAULT_NA,
		// value)||StringUtils.equals(Constants.DEFAULT_DNA, value)) {
		// return "";
		// }
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}
}
