package com.fangda.kass.model.interview;

/**
 * This class stores / retrieves pages of interview questions.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class InterviewPage {

	public static int count;
	public static int curpage;
	public static int next;

	public static int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCurpage() {
		return curpage;
	}

	public void setCurpage(int curpage) {
		this.curpage = curpage;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}
}
