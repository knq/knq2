package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the question element, which records some key
 * information of a question.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KQQuestion implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5420926908032616983L;
	/**
	 * The qid attribute: Question ID
	 */
	@XmlAttribute(name = "qid")
	private String qid;
	/**
	 * The title attribute: Title of the question
	 */
	@XmlAttribute(name = "title")
	private String title;
	/**
	 * The value attribute: Answer value of the question
	 */
	@XmlAttribute(name = "value")
	private String value;
	/**
	 * The label attribute: Label of the question
	 */
	@XmlAttribute(name = "label")
	private String label;
	/**
	 * The field element: Generic storage element for any attributes of the
	 * question.
	 */
	@XmlElement(name = "field")
	private List<KQQuestionField> fields;

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<KQQuestionField> getFields() {
		if (fields == null) {
			fields = new ArrayList<KQQuestionField>();
		}
		return fields;
	}

	public void setFields(List<KQQuestionField> fields) {
		this.fields = fields;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
