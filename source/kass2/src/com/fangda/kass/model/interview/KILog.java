package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the log element which records the Interview
 * Duration.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class KILog {

	/**
	 * The startTime attribute: Start time
	 */
	@XmlAttribute(name = "startTime")
	private String startTime;
	/**
	 * The endTime attribute: End time
	 */
	@XmlAttribute(name = "endTime")
	private String endTime;

	/**
	 * The duration attribute: Time of duration
	 */
	@XmlAttribute(name = "duration")
	private String duration;

	/**
	 * The break attribute: Break Time
	 */
	@XmlAttribute(name = "break")
	private String breaks;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getBreaks() {
		return breaks;
	}

	public void setBreaks(String breaks) {
		this.breaks = breaks;
	}
}
