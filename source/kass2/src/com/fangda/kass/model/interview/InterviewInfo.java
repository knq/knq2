package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

/**
 * This class stores / retrieves some key information of questions including
 * questions, follow questions and answer option.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
public class InterviewInfo {

	public String qid;
	public String qtype;
	public List<String> optionid = new ArrayList<String>();

	public List<String> getOptionid() {
		return optionid;
	}

	public void setOptionid(List<String> optionid) {
		this.optionid = optionid;
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public String getQtype() {
		return qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

}
