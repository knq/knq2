package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class stores / retrieves the pageConfig element in an interview xml
 * file, which records the layout setting of NKK diagram page.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlRootElement(name = "PageConfig")
@XmlAccessorType(XmlAccessType.FIELD)
public class PageConfig {

	/**
	 * The pageFormat attribute: 0= portrait, 1 = landscape
	 */
	@XmlAttribute(name = "pageFormat")
	private int pageFormat = 0;
	/**
	 * The pageFormat attribute: View Scale of the diagram.
	 */
	@XmlAttribute(name = "pageScale")
	private int pageScale = 10;
	/**
	 * The cellWidth attribute: the horizontal spacing of two person icons
	 */
	@XmlAttribute(name = "cellWidth")
	private int cellWith = 100;
	/**
	 * The cellHeight attribute: the vertical spacing of between person icons
	 * and union icons
	 */
	@XmlAttribute(name = "cellHeight")
	private int cellHeight = 100;
	/**
	 * The showBirthDay attribute: 0 refers to display person icon without the
	 * birth Year of person, 1 refers to display person icon with the birth Year
	 * of person
	 */
	@XmlAttribute(name = "showBirthDay")
	private int showBirthday = 0;

	public int getPageFormat() {
		return pageFormat;
	}

	public void setPageFormat(int pageFormat) {
		this.pageFormat = pageFormat;
	}

	public int getPageScale() {
		return pageScale;
	}

	public void setPageScale(int pageScale) {
		this.pageScale = pageScale;
	}

	public int getCellWith() {
		return cellWith;
	}

	public void setCellWith(int cellWith) {
		this.cellWith = cellWith;
	}

	public int getCellHeight() {
		return cellHeight;
	}

	public void setCellHeight(int cellHeight) {
		this.cellHeight = cellHeight;
	}

	public int getShowBirthday() {
		return showBirthday;
	}

	public void setShowBirthday(int showBirthday) {
		this.showBirthday = showBirthday;
	}
}
