package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the unions element which is a placeholder
 * element for all unions in an interview xml file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KUnions {
	/**
	 * The union element: it is a sub-element of the unions element.
	 */
	@XmlElement(name = "union")
	private List<KUUnion> unions = new ArrayList<KUUnion>();

	public List<KUUnion> getUnions() {
		if (unions == null) {
			unions = new ArrayList<KUUnion>();
		}
		return unions;
	}

	public void setUnions(List<KUUnion> unions) {
		this.unions = unions;
	}

}
