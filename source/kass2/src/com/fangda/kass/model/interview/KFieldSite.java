package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves Attributes of the fieldSite element in a
 * interview XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KFieldSite {
	/**
	 * The projectTitle attribute: the title of a research project
	 */
	@XmlAttribute(name = "projectTitle")
	private String projectTitle;
	/**
	 * The projectDirector attribute: the director of this project
	 */
	@XmlAttribute(name = "projectDirector")
	private String projectDirector;
	/**
	 * The country attribute: the country of the field site
	 */
	@XmlAttribute(name = "country")
	private String country;
	/**
	 * The province attribute: the province of the field site
	 */
	@XmlAttribute(name = "province")
	private String province;
	/**
	 * The district attribute: the district of the field site
	 */
	@XmlAttribute(name = "district")
	private String district;
	/**
	 * The county attribute: the county of the field site
	 */
	@XmlAttribute(name = "county")
	private String county;
	/**
	 * The town attribute: the town of the field site
	 */
	@XmlAttribute(name = "town")
	private String town;
	/**
	 * The village attribute: the village of the field site
	 */
	@XmlAttribute(name = "village")
	private String village;
	/**
	 * The placeName attribute: the place name of the field site
	 */
	@XmlAttribute(name = "placeName")
	private String placeName;

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getProjectDirector() {
		return projectDirector;
	}

	public void setProjectDirector(String projectDirector) {
		this.projectDirector = projectDirector;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

}
