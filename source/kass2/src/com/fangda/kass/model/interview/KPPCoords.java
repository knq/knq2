package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class stores / retrieves the coords Element which records the X-, and Y-
 * Coordinates of icons of person and unions.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KPPCoords implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1437902313243971141L;
	
	/**
	 * The x attribute: X- Coordinates
	 */
	@XmlAttribute(name = "x")
	private double x;
	/**
	 * The y attribute: Y- Coordinates
	 */
	@XmlAttribute(name = "y")
	private double y;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
