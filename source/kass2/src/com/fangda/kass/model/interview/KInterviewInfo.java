package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves elements of background information of an
 * interview.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class KInterviewInfo {
	/**
	 * The sampleNumber attribute: Sample Number
	 */
	@XmlAttribute(name = "sampleNumber")
	private String sampleNumber;
	/**
	 * The serialNumber attribute: Serial Number
	 */
	@XmlAttribute(name = "serialNumber")
	private String serialNumber;
	/**
	 * The version attribute: Version of the KNQ2 software.
	 */
	@XmlAttribute(name = "version")
	private String version;
	/**
	 * The qVersion attribute: The filename of the current version questionnaire
	 * applied in this interview.
	 */
	@XmlAttribute(name = "qVersion")
	private String qVersion;
	/**
	 * The progress attribute: Progress of interview.
	 */
	@XmlAttribute(name = "progress")
	private int progress;
	/**
	 * The interviewer attribute: Name of interviewer
	 */
	@XmlAttribute(name = "interviewer")
	private String interviewer;
	/**
	 * The interviewee attribute: Name of interviewee
	 */
	@XmlAttribute(name = "interviewee")
	private String interviewee;
	/**
	 * The startDate attribute: Start date of this interview
	 */
	@XmlAttribute(name = "startDate")
	private String startDate;
	/**
	 * The endDate attribute: End date of this interview
	 */
	@XmlAttribute(name = "endDate")
	private String endDate;
	/**
	 * The duration attribute: Time of duration of this interview
	 */
	@XmlAttribute(name = "duration")
	private String duration;
	/**
	 * The times attribute: Times
	 */
	@XmlAttribute(name = "times")
	private Integer times;
	/**
	 * The logs element: Logs of interview
	 */
	@XmlElement(name = "logs")
	private KILogs logs;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getQVersion() {
		return qVersion;
	}

	public void setQVersion(String version) {
		qVersion = version;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public String getSampleNumber() {
		return sampleNumber;
	}

	public void setSampleNumber(String sampleNumber) {
		this.sampleNumber = sampleNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getInterviewee() {
		return interviewee;
	}

	public void setInterviewee(String interviewee) {
		this.interviewee = interviewee;
	}

	public String getInterviewer() {
		return interviewer;
	}

	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public KILogs getLogs() {
		if (logs == null) {
			logs = new KILogs();
		}
		return logs;
	}

	public void setLogs(KILogs logs) {
		this.logs = logs;
	}

}
