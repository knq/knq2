package com.fangda.kass.model.interview;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the logs element of an interview XML file.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KILogs {
	/**
	 * The log element: it is a sub-element of the logs element, which records
	 * the Interview Duration.
	 */
	@XmlElement(name = "log")
	private List<KILog> kiLog;

	public List<KILog> getKiLog() {
		if (kiLog == null) {
			kiLog = new ArrayList<KILog>();
		}
		return kiLog;
	}

	public void setKiLog(List<KILog> kiLog) {
		this.kiLog = kiLog;
	}
}
