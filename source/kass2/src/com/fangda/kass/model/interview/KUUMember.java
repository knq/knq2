package com.fangda.kass.model.interview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlElement;

/**
 * This class stores / retrieves the member sub element of the union element,
 * which records any member of a union.
 * 
 * @author Fangfang Zhao, Jing Kong
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KUUMember implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1515763155552830753L;

	/**
	 * The id attribute: More accurately "pid" since this stores pid of a
	 * person.
	 */
	@XmlAttribute(name = "id")
	private Integer id;

	/**
	 * The type attribute: the relationships between the person and the union.
	 */
	@XmlAttribute(name = "type")
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
