KNQ2 (Kinship Network Questionnaire Version 2) is a software for  conducting
face-to-face kinship-network interviews and analyzing  the results.

(C) 2019 the MPI for Social Anthropolog


General 
-------

KNQ2 (Kinship Network Questionnaire 2) is a software system that has  been
designed, and implemented by the KNQ2 programming team, headed  by Dr Kong Jing
at the Chinese Academy of Social Sciences, Beijing.  It builds on and extends
the original KNQ, developed for the KASS  (Kinship and Social Security in
Europe) project by Gordon Milligan and  Christian Kieser �C incorporating major
new capabilities requested by  the KUV team at Max Planck Institute for Social
Anthropology Halle (Saale).  The software system has been tested and refined
jointly by the Beijing  and Halle teams.  This process in still on-going, so the
software system  itself is still at a ��beta�� stage.

Members of the Beijing team are Kong Jing (leader), Zhao Fangfang, Ma Shuang, 
Zhou Jianguo. The Halle KNQ2 development team includes Patrick Heady 
(coordinator and specifier of initial request), Alexander Pashos, Christoph 
Korb (software system testing and documentation); Louise Bechtold (interviewing, 
software system testing and documentation) Barbara Pieta (software system 
testing and documentation). Details of the original KNQ software system and  of
associated analysis software are available from the GESIS data archive  at
https://doi.org/10.4232/1.12266 �C from which the original software systems  can
be downloaded.


License 
--------

It is open source under the AGPL license.  It is written in Java 6.0 (a.k.a.
1.6.0_39 ).


Third-party licenses 
--------------------

1.  The Apache Software Foundation: commons-lang-2.1.jar, log4j-1.2.14.jar are
licensed under the Apache License 2.0

2.  mxGraph (Copyright (c) JGraph Ltd 2006-2017): jgraph-3.5.jar is licensed
under the modern, 3 clause BSD license.

3.  itext: itext-2.1.7.jar and itext-rtf-2.1.7.jar(Copyright (c), iText Group
NV). Since iText5 the iText can be free used under the AGPL license

4.  jdatepicker-1.0.jar is licensed under the BSD open source license.

5.  slf4j-api-1.5.2.jar and slf4j-log4j12-1.5.2.jar are distributed under the
MIT license.


Key Features
------------

1. The KNQ2 software system enables the interviewer to gather information about 
an informant and her/his network of relatives and important others. At the heart 
of every KNQ2 interview are the interactions between ego and the members of 
her/ his network. In addition, background information on the individual persons 
will be collected, which makes it possible to test hypotheses about the
behaviour  within in the network. The KNQ2 software system offers a wide range
of question  types and guides the interviewer through the different stages of
the interview.

2. The scope of the interview and the questions asked can be controlled using 
a number of different settings. It is possible to limit the size of the network 
of relatives by using a stopping rule. This ensures that only the information 
for people within a certain distance from ego is collected. To ensure the data 
quality, relevance conditions and a skip-logic are also an integrated part of 
the KNQ2 software system. Relevance conditions determine which persons can be 
selected for each individual question. The skip logic allows hiding questions 
that are not relevant in certain interview contexts.

3. The entire questionnaire can be adapted to the needs of the researcher. 
Questions can be added, modified and deleted. Questions can be divided into 
sections and the order of the questions can be changed . The software system 
offers a wide range of different question types to support the researcher in 
answering her/his question.

4. With the help of the software system, it is possible to offer questionnaire 
versions in different languages. Every script that is part of the Unicode UTF-8 
charset can be used. Translating a questionnaire to another language is easy as 
you don��t have to start again from the scratch. Using the internationalisation 
module you can just translate the text strings of the questionnaire. This
procedure  also ensures that different language versions of the questionnaire
stay consistent  and mutually interchangeable. 

5. Once an interview is complete, the data can be exported to CSV format for 
further analysis. This format can be used by a wide range of spreadsheet and 
statistic programs. During the export process, the data will be enriched with 
additional variables to facilitate the work of the researcher.


Requirements
------------

* Latest version of JRE (at least 1.6.0 or 6.0)


How to run 
----------

If you have downloaded the binary distribution, the installer should have
created an link to your "Start" menu and (possibly) on your desktop.

In src (source code), the main class is com.fangda.kass.StartMain, run the main
class to start the application.